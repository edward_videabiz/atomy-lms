<?php
require_once '../../config.php';

$controllder = new \report_ubstatistics\Statistics();

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}