<?php
defined('MOODLE_INTERNAL') || die;

$pluginname = 'report_ubstatistics';

// 로그 표시방법
$name = $pluginname.'/printtype';
$title = get_string('setting_printtype', $pluginname);
$description = get_string('setting_printtype_desc', $pluginname);
$default = 0;
$options = array(
    \report_ubstatistics\Statistics::PRINTTYPE_LOG => get_string('setting_printtype_log', $pluginname)
    ,\report_ubstatistics\Statistics::PRINTTYPE_UBSTATS => get_string('setting_printtype_ubstats', $pluginname)
);
$setting = new admin_setting_configselect($name, $title, $description, $default, $options);
$settings->add($setting);

// 상세정보보기
$name = $pluginname.'/isdetail';
$title = get_string('setting_isdetail', $pluginname);
$description = get_string('setting_isdetail_desc', $pluginname);
$default = 0;
$setting = new admin_setting_configselect($name, $title, $description, $default, array(0 => new lang_string('no'), 1 => new lang_string('yes')));
$settings->add($setting);