<?php
require('../../config.php');
require_once $CFG->dirroot.'/course/lib.php';

$reportName = 'report_ubstatistics';
$CCourse = \local_ubion\course\Course::getInstance();
$CUser = \local_ubion\user\User::getInstance();
$CGroup = \local_ubion\group\Group::getInstance();
$CStatistics = new \report_ubstatistics\Statistics();

$id         = required_param('id', PARAM_INT);
$userid     = optional_param('userid', null, PARAM_INT);
$section    = optional_param('section', -1, PARAM_INT);
$groupid = optional_param('groupid', null, PARAM_INT);

$courseFormat = $CCourse->getCourseFormat($id);
$course = $courseFormat->get_course();
$context = context_course::instance($course->id);

$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);


$i8n = get_strings(array(
    'search',
    'pluginname',
    'viewmode_view',
    'viewmode_create',
    'viewmode_comment',
    'btn_viewed',
    'btn_created',
    'btn_comment',
    'excel_down',
    'heading_user_statistics'
), $reportName);

$i8n->group = get_string('groups', 'group');
$i8n->number = get_string('number', 'local_ubion');
$i8n->idnumber = get_string('idnumber');
$i8n->fullnameuser = get_string('fullnameuser');
$i8n->department = get_string('department');

$PAGE->set_url('/report/ubstatistics/index.php', array('id' => $id));
$PAGE->set_pagelayout('report');
$PAGE->set_title($course->fullname .' : '. $i8n->pluginname);
$PAGE->set_heading($course->fullname);


// js call
$PAGE->requires->strings_for_js(array(
    'not_exists_log'
), $reportName);
$PAGE->requires->strings_for_js(array(
    'excel_message'
), 'local_ubion');

$PAGE->requires->js_call_amd('report_ubstatistics/statistics', 'init', array('courseid' =>  $id, 'url' => $CFG->wwwroot.'/report/ubstatistics/action.php'));


echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->pluginname);


// 권한 점검
if (!$isManager = $CStatistics->isManager($context)) {
    echo '<div class="alert alert-danger">'.get_string('nopermissions', 'error', $i8n->pluginname).'</div>';
} else {
    
    // 강좌에서 그룹을 분류해서 사용하는지 확인
    $courseGroup = $CGroup->getCourseGroups($course->id);
    
    list ($courseSections, $usedCmid, $writeModules, $commentModules, $moduleCount, $resourceCount) = $CStatistics->getCourseSections($course);
    
    // 모든 사용자 목록
    // 모바일에서 정렬 순서는 한글이면 firstname 영문이면 lastname
    $sort = (current_language() == 'ko') ? 'firstname' : 'lastname';
    
    $users = $CCourse->getCourseUserAll($course, null, $groupid, $sort);
    
    // 강좌내에 수강생으로 등록되어 있는지 확인
    $userid = isset($users[$userid]) ? $userid : null;
    
    // 조회한 사용자가 없거나, 강좌내에 존재하지 않는 사용자 검색인 경우에는 첫번째 사용자가 조회되어야 함.
    if (empty($userid)) {
        if ($users) {
            $userid = current($users)->id;
        }
    }
    
    
    ?>
	<div class="report_statistics">
		<div class="search well mb-3">
			<form method="get">
				<div class="form-group row">
					<label class="col-form-label col-md-3" for="sections"><?= get_string('sectionname', 'format_'.$course->format); ?></label>
					<div class="col-md-9">
						<select name="section" class="auto_submit">
							<option value="-1"><?= get_string('all')?></option>
    						<?php 
    						// 강좌내 주차
    						foreach ($courseSections as $sectionnum => $cs) {
    						    $selected = ($section == $sectionnum) ? ' selected="selected"' : '';
    						    echo '<option value="'.$sectionnum.'" '.$selected.'>'.$cs->name.'</option>';   
    						}
    						?>
						</select>
					</div>
				</div>
				<?php
                if ($courseGroup->is) {
                ?>
				<div class="form-group row">
					<label class="col-form-label col-sm-3" for="course_group"><?=$i8n->group;?></label>
					<div class="col-sm-9">
						<select name="groupid" class="course_group auto_submit" id="course_group">
							<?php
							echo '<option value="">'.get_string('allparticipants').'</option>';
							echo $CGroup->getOptions($courseGroup, $groupid);
							?>
						</select>
					</div>
				</div>
				<?php
                }
                ?>
				<div class="form-group row">
					<label class="col-form-label col-md-3" for="sections"><?= get_string('user_select', 'local_ubion'); ?></label>
					<div class="col-md-9">
						<input type="hidden" name="id" value="<?= $id; ?>" />
						<select name="userid" class="auto_submit">
							<?php 
							if (!empty($users)) {
							    foreach ($users as $u) {
							        $selected = ($u->id == $userid) ? ' selected="selected"' : '';
							        echo '<option value="'.$u->id.'" '.$selected.'>'.fullname($u).' ('.$CUser->getIdnumberOrUsername($u).')</option>';
							    }
							} else {
							    echo '<option value="">'.get_string('enrolcandidates', 'enrol').'</option>';   
							}
							?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-form-label col-md-3"><?= $i8n->excel_down; ?></label>
					<div class="col-md-9">
						<a href="<?= $CFG->wwwroot.'/report/ubstatistics/excel.php?id='.$course->id; ?>" class="btn btn-success btn-excel">
							<?= $i8n->excel_down; ?>
						</a>
					</div>
				</div>
			</form>
		</div>
		
		
		<div class="user_status">
			<h4 class="heading_status">
				<?= $i8n->heading_user_statistics; ?>
			</h4>
			
			<?php 
			// 상단에서 userid값이 없으면 강좌내 조회되는 첫번째 사용자가 선택되게 되어 있기 때문에 
			// userid값이 null이면 등록된 사용자가 없다고 판단해도됨.
			if (empty($userid)) {
			    echo '<div class="well">';
			    echo     get_string('enrolcandidates', 'enrol');
			    echo '</div>';
			} else {
			    
			    $use_cmid_string = join(',', $usedCmid);
			    
			    // 사용자 로그 가져오기
			    $logs = $CStatistics->getListAll($course->id, $userid, $use_cmid_string);
			    
			    $logdetailClass = (get_config('report_ubstatistics', 'isdetail') == 1) ? ' btn-logdetail' : 'btn-nobtn';
			    
			    foreach ($courseSections as $sectionnum => $csi) {
			        // 주차를 따로 선택한 경우
			        if ($section >= 0 && $sectionnum != $section) {
			            continue;
			        }
			        
                    echo '<div class="well">';
                    echo    '<dl class="sections">';
                    echo        '<dt>'.$csi->name.'</dt>';
                    echo        '<dd>';
                    echo            '<dl class="activities">';
                    if ($csi->activity) {
                        foreach ($csi->activity as $activity) {
                            
                            $logCountViewed = '-';		// view 항목
                            $logCountCreated = '-';	// created 항목
                            $logCountCommented = '-';	// commented 항목
                            
                            if (isset($logs[$activity->id])) {
                                $log = $logs[$activity->id];
                                
                                $default_attribute = 'data-modname="'.$activity->modname.'" data-cmid="'.$activity->id.'" data-userid="'.$userid.'" title="'.$activity->name.'"';
                                
                                if ($log->viewed > 0) {
                                    $attribute = $default_attribute.' data-viewtype="viewed"';
                                    $logCountViewed = '<button type="button" class="btn btn-default btn-xs btn-tooltip '.$logdetailClass.'" '.$attribute.' title="'.$i8n->viewmode_view.'">'.$log->viewed.'</button>';
                                }
                                
                                if ($log->created > 0) {
                                    $attribute = $default_attribute.' data-viewtype="created"';
                                    $logCountCreated = '<button type="button" class="btn btn-default btn-xs btn-tooltip '.$logdetailClass.'" '.$attribute.' title="'.$i8n->viewmode_create.'">'.$log->created.'</button>';
                                }
                                
                                if ($log->commented > 0) {
                                    $attribute = $default_attribute.' data-viewtype="commented"';
                                    $logCountCommented = '<button type="button" class="btn btn-default btn-xs btn-tooltip '.$logdetailClass.'" '.$attribute.' title="'.$i8n->viewmode_comment.'">'.$log->commented.'</button>';
                                }
                            }
                            
                            $moduleIcon = $activity->get_icon_url();
                            if ($activity->modname == 'ubboard') {
                                // 게시판 타일을 알아와서 알맞는 이미지를 출력시켜줘야됨.
                                $boardType = $DB->get_field_sql("SELECT type FROM {ubboard} WHERE id = :id", array('id'=>$activity->instance));
                                
                                if ($boardType == 'qna' || $boardType == 'notice') {
                                    $moduleIcon = $OUTPUT->image_url('ubboard_'.$boardType, 'ubboard');
                                }
                            }
                            echo        '<dt>';
                            echo 		    '<img src="'.$moduleIcon.'" alt="'.$activity->name.'" title="'.$activity->name.'" class="mr-2" />';
                            echo            $activity->name;
                            echo        '</dt>';
                            
                            
                            $colnum = 12;
                            $isWrite = (in_array($activity->modname, $writeModules)) ? true : false;
                            $isComment = (in_array($activity->modname, $commentModules)) ? true : false;
                            
                            // 보기, 쓰기, 댓글 모두이면 4칸
                            // 보기, 쓰기 또는 댓글이면 6칸
                            if ($isWrite && $isComment) {
                                $colnum = 4;
                            } else if ($isWrite || $isComment) {
                                $colnum = 6;   
                            }
                            
                            echo        '<dd class="row">';
                            echo            '<dl class="col-xs-'.$colnum.' counts">';
                            echo                '<dt>'.$i8n->btn_viewed.'</dt>';
                            echo                '<dd>'.$logCountViewed.'</dd>';
                            echo            '</dl>';
                            
                            if ($isWrite) {
                                echo        '<dl class="col-xs-'.$colnum.' counts">';
                                echo            '<dt>'.$i8n->btn_created.'</dt>';
                                echo            '<dd>'.$logCountCreated.'</dd>';
                                echo        '</dl>';
                            }
                            
                            if ($isComment) {
                                echo        '<dl class="col-xs-'.$colnum.' counts">';
                                echo            '<dt>'.$i8n->btn_comment.'</dt>';
                                echo            '<dd>'.$logCountCommented.'</dd>';
                                echo        '</dl>';
                            }
                            echo        '</dd>';
                        }
                    } else {
                        echo            '<dd class="nomodules">'.get_string('no_modules', 'local_ubion').'</dd>';
                    }
                    echo            '</dl>';
                    echo        '</dd>';
                    echo    '</dl>';
                    echo '</div>';
			    }
			}
			?>
		</div>
		<div class="modal modal-log-detail" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h5 class="modal-title"><?=$i8n->heading_user_statistics;?></h5>
					</div>
					<div class="modal-body">
						<?php 
						echo $OUTPUT->render_from_template('report_ubstatistics/log_detail', null);
						?>
					</div>
					<div class="modal-footer">
						<button class="btn btn-default" data-dismiss="modal"><?= get_string('close', 'local_ubion'); ?></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
	
	// Trigger a report viewed event.
	$event = \report_ubstatistics\event\report_viewed::create(array('context' => $context));
	$event->trigger();
}
echo $OUTPUT->footer();