define(['jquery', 'core/templates', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.tableHeadFixer', 'theme_coursemos/jquery.filedownload'], function($, templates, pBootstrap, pTableHeadFixer, pFileDownload) {
	var statistics = {};
	
	statistics.init = function(courseid, url) {
		
		$(".user_statistics .btn-tooltip").tooltip({container:'.user_statistics', trigger:'hover'});
		$(".table-statistics").tableHeadFixer({"head" : false, "left" : 3});
		
		$(".report_statistics .search .auto_submit").change(function() {
			$(".report_statistics .search form").submit();
		});
		
		$(".table-statistics .module_icon").tooltip();
		
		$(".modal-log-detail").modal({show:false, backdrop:true});
		
		$(".report_statistics .btn-logdetail").click(function() {
			module_name = $(this).attr('data-title');
			modname = $(this).attr('data-modname');
			userid = $(this).attr('data-userid');
			cmid = $(this).attr('data-cmid');
			viewtype = $(this).attr('data-viewtype');
			
			target_element = $(this);

			// header 변경
			$(".modal-log-detail .modal-header h3").text(module_name);

			// 학습현황이 담길 테이블 지정
			log_table = $(".modal-log-detail .modal-body table tbody");

			// tbody 내용 삭제
			log_table.empty();

			$(".modal-log-detail").modal('show');
		});
		
		
		$(".modal-log-detail").on('show.bs.modal', function() {
			ajaxparam = coursemostype + '=detail&id='+courseid+'&userid='+userid+'&modname='+modname+'&cmid='+cmid+'&viewtype='+viewtype;
			
			$.ajax({
				data: ajaxparam
				,method : 'post'
				,url : url
				,success:function(data){
					// 학습현황이 담길 테이블 지정
					log_table = $(".modal-log-detail .modal-body table tbody");

					// tbody 내용 삭제
					log_table.empty();

					if(data.code == mesg.success) {
						if(data.logs_count > 0) {
						    
						    /*
							start_num = 1;
							$.each(data.logs, function(index, log) {
								log_table_tr = $("<tr>");

								log_table_tr.append($("<td class='text-center'>").html(start_num));
								log_table_tr.append($("<td>").html(log.eventname));
								log_table_tr.append($("<td class='text-center'>").html(log.count));

								log_table.append(log_table_tr);

								start_num++
							});
							*/
						    
						    var context = { logs : data.logs};
						    templates.render('report_ubstatistics/log_detail', context)
						    
						    // It returns a promise that needs to be resoved.
						            .then(function(html, js) {
						                // Here eventually I have my compiled template, and any javascript that it generated.
						                // The templates object has append, prepend and replace functions.
						                templates.replaceNodeContents('.modal-log-detail .modal-body', html, js);
						            }).fail(function(ex) {
						                // Deal with this exception (I recommend core/notify exception function for this).
						            });
						} else {
							alert(M.util.get_string('not_exists_log', 'report_ubstatistics'));
							$(".modal-log-detail").modal('hide');
						}
					} else {
						alert(data.msg);
						return false;
					}
				}
			});
		});
		
		$(".report_statistics .btn-excel").click(function() {
			$.fileDownload($(this).prop('href'), {
				prepareCallback : function(url) {
					showSubmitProgress();
					$("#ajax_loading_message .save_msg").text(M.util.get_string('excel_message', 'local_ubion'));
				}, 
				successCallback : function(url) {
					hideSubmitProgress();
				},
		    });
			
			return false;
		});
	};
	
	return statistics;
});