<?php
namespace report_ubstatistics\core;

use stdClass;
use local_ubion\base\{
    Parameter,
    Validate,
    Javascript
};

class Statistics extends \local_ubion\controller\Controller {
    private $modules = null;
    protected $isUbstats = false;
    
    const PRINTTYPE_LOG = 0;
    const PRINTTYPE_UBSTATS = 1;
	
	
	function __construct()
	{
	    $this->isUbstats = get_config('report_ubstatistics', 'printtype');
	}
	
	/**
	 * 학습현황을 볼수 있는 사용자인지 확인
	 * 
	 * @param \context $context
	 * @return boolean
	 */
	function isManager($context)
	{
		return has_capability('report/ubstatistics:view', $context);
	}
	
	
	/**
	 * 댓글 현황을 따로 표시해줘야되는 모듈 리턴 
	 * 
	 * @return string[]
	 */
	function getCommentEventModules()
	{
		return array('ubboard', 'forum');
	}
	
	/**
	 * 쓰기 현황을 따로 표시해줘야되는 모듈 리턴
	 * @return string[]
	 */
	function getWriteEventModules()
	{
	    return array('assign', 'ubboard', 'forum', 'quiz', 'wiki', 'choice', 'feedback', 'glossary', 'survey', 'ubpeer');
	}
	
	
	/**
	 * 사용자 로그 기록 가져오기
	 *
	 * @param number $courseid
	 * @param number $userid
	 * @param number|string $cmids
	 * @return multitype:stdClass NULL
	 */
	function getListAll($courseid, $userid, $cmids)
	{
		global $DB;
	
		$logall = array();
		
		// 강좌내에 등록된 학습자원/활동이 없는 경우에는 에러가 발생됨.
		if (!empty($cmids)) {
			// ubstats 통계 테이블을 사용하는 경우와, 기본 조회하는 방법에 따라 데이터 구 
			if ($this->isUbstats) {
				$functionName = 'getStatsActionCount';
				
				$query = "SELECT
								id, courseid, userid, moduleid, target, action, cmid AS contextinstanceid, action_count
						FROM	{logstore_ubstats_activity}
						WHERE	courseid = :courseid
						AND		userid = :userid
						AND		cmid IN (".$cmids.")";
				
				$param = array('courseid'=>$courseid, 'userid'=>$userid);
			} else {
				$functionName = 'getLogActionCount';
					
				$query = "SELECT
							id
							,component
							,eventname
							,action
							,crud
							,contextinstanceid
							,edulevel
					FROM 	{logstore_standard_log}
					WHERE 	userid = :userid
					AND 	contextlevel = :contextlevel
					AND 	contextinstanceid IN (".$cmids.")
					AND		edulevel IN (".\core\event\base::LEVEL_PARTICIPATING.", ".\core\event\base::LEVEL_OTHER.")
					AND 	courseid = :courseid";
		
				$param = array('courseid'=>$courseid, 'userid'=>$userid, 'contextlevel'=>CONTEXT_MODULE);
			}
			
	
			if ($logs = $DB->get_records_sql($query, $param)) {
				foreach ($logs as $l) {
					// 기존에 등록 cmid가 없는 경우에는 0으로 셋팅
					if (!isset($logall[$l->contextinstanceid])) {
						$logall[$l->contextinstanceid] = new stdClass();
						$logall[$l->contextinstanceid]->created = 0;
						$logall[$l->contextinstanceid]->viewed = 0;
						$logall[$l->contextinstanceid]->commented = 0;
						// $logall[$l->contextinstanceid]->updated = 0;
						// $logall[$l->contextinstanceid]->deleted = 0;
					}
						
					$logall[$l->contextinstanceid] = $this->$functionName($l, $logall[$l->contextinstanceid]);
				}
			}
		}
		return $logall;
	}
	
	/**
	 * 사용자 로그 기록 가져오기
	 *
	 * @param number $courseid
	 * @param number $userid
	 * @param number $cmid
	 * @param string $modname
	 * @param string $viewtype
	 * @return multitype:stdClass NULL
	 */
	function getView($courseid, $userid, $cmid, $modname, $viewtype='viewed')
	{
		global $DB;
	
		$logs = array();
		
		// 강좌내에 등록된 학습자원/활동이 없는 경우에는 에러가 발생됨.
		$functionname = 'getCheckEvent'.ucfirst($modname);
		
		if (!empty($cmid)) {
			// ubstats 통계 테이블을 사용하는 경우와, 기본 조회하는 방법에 따라 데이터 조회하는 테이블이 달라짐
			if ($this->isUbstats) {
				
				$query = "SELECT
								id, courseid, userid, moduleid, target, action, cmid AS contextinstanceid, action_count
						FROM	{logstore_ubstats_activity}
						WHERE	courseid = :courseid
						AND		userid = :userid
						AND		cmid = :cmid";
	
				$param = array('courseid'=>$courseid, 'userid'=>$userid, 'cmid'=>$cmid);
			} else {
				$query = "SELECT
									id
									,component
									,eventname
									,action
									,crud
									,contextinstanceid
									,edulevel
									,COUNT(1) cnt
						FROM 		{logstore_standard_log}
						WHERE 		userid = :userid
						AND 		contextlevel = :contextlevel
						AND 		contextinstanceid = :cmid
						AND			edulevel IN (".\core\event\base::LEVEL_PARTICIPATING.", ".\core\event\base::LEVEL_OTHER.")
						AND 		courseid = :courseid
						GROUP BY 	component, eventname, action, crud";
	
				$param = array('courseid'=>$courseid, 'userid'=>$userid, 'contextlevel'=>CONTEXT_MODULE, 'cmid'=>$cmid);
			}
				
	
			if ($activityLogs = $DB->get_records_sql($query, $param)) {
				
				// 모듈별로 허용되는 event 목록 가져오기
				$allowEvents = array();
				if (in_array($modname, $this->getWriteEventModules())) {
					if ($event_list = $this->$functionname()) {
						$allowEvents = $event_list[$viewtype];
					}
				} else {
					$allowEvents[] = "\\mod_".$modname."\\event\\course_module_viewed";
				}
				
				// error_log(print_r($allowEvents, true));
				
				$index = 1;
				foreach ($activityLogs as $l) {
					if ($this->isUbstats) {
						// event명 규칙이 정해져있기 때문에 logstore_ubstats_activity에 전체 이벤트명을 입력할 필요가 없음.
						$eventName = "\\mod_".$modname."\\event\\".$l->target.'_'.$l->action;
						$count = $l->action_count;
					} else {
						$eventName = $l->eventname;
						$count = $l->cnt;
					}
					
					if (in_array($eventName, $allowEvents)) {
						// 불필요한 값 제거
						$explodeEventName = explode("\\", $eventName);
						
						$la_info = new stdClass();
						$la_info->index = $index;
						$la_info->eventname = end($explodeEventName);
						$la_info->count = $count;
						
						//$eventstringid = str_replace('_', '', $eventname);
						//$eventstringid = end(explode("\\", $eventstringid));
						//$la_info->eventstring = get_string('event'.$eventstringid, 'mod_'.$modname);
						
						$logs[] = $la_info;
					}
				}
			}
		}
		
		// error_log(print_r($logs, true));
		return $logs;
	}
	
	/**
	 * 학습이력 현황 출력을 위해 필요한 기본값 리턴
	 * 
	 * @param int|object $courseorid
	 * @return array
	 */
	public function getCourseSections($courseorid)
	{
	    global $DB;
	    
	    $CCourse = \local_ubion\course\Course::getInstance();
	    
	    if (is_number($courseorid)) {
	        $course = $CCourse->getCourse($courseorid);
	    } else {
	        $course = $courseorid;
	    }
	    
	    // 강좌에 설정된 모든 학습활동 내역들 가져오기
	    $modinfo = get_fast_modinfo($course);
	    $mods = $modinfo->get_cms();
	    $modnames = $modinfo->get_used_module_names();
	    
	    
	    $moduleCount = 0; // 학습 활동수
	    $resourceCount = 0; // 강의 자료수 (resource)
	    
	    // 리소스 영역인 학습활동 정보를 담는 배열
	    $resourceModules = array();
	    foreach ($modnames as $modkey => $modname) {
	        if ($modkey != 'label') {
	            $archetype = plugin_supports('mod', $modkey, FEATURE_MOD_ARCHETYPE);
	            if ($archetype == MOD_ARCHETYPE_RESOURCE) {
	                array_push($resourceModules, $modkey);
	            }
	        }
	    }
	    
	    // 익명성 요구되는 항목들은 표시되면 안됨.
	    $passCmids = array();
	    
	    // 전체 모듈에서 학습활동, 강의 자료수 가져오기...
	    foreach ($mods as $modid => $modInfo) {
	        // 익명성을 요구되는 항목에 대해서는 자료 취합이 되면 안됨
	        // 2017-01-31
	        if ($modInfo->modname == 'ubboard') {
	            $ubboard_type = $DB->get_field_sql("SELECT type FROM {ubboard} WHERE id = :id", array(
	                'id' => $modInfo->instance
	            ));
	            
	            if ($ubboard_type == 'anonymous') {
	                array_push($passCmids, $modInfo->id);
	                continue;
	            }
	        } else if ($modInfo->modname == 'courseevaluation') {
	            array_push($passCmids, $modInfo->id);
	        }
	        
	        // 표지는 제외, 숨김 처리된 학습자원/활동은 표시되면 안됨.
	        if ($modInfo->modname != 'label' && $modInfo->visible) {
	            if (in_array($modInfo->modname, $resourceModules)) {
	                $resourceCount ++;
	            } else {
	                $moduleCount ++;
	            }
	        }
	    }
	    
	    
	    // 쓰기항목이 표시되어야 할 모듈들..
	    $writeModules = $this->getWriteEventModules();
	    
	    // 댓글항목이 표시되어야 할 모듈들..
	    $commentModules = $this->getCommentEventModules();
	    
	    
	    // 주차별 학습 활동 내역을 저장.
	    $courseSections = array();
	    
	    
	    // 주차 
	    $numsections = $course->numsections ?? course_get_format($course->id)->get_last_section_number();
	    
	    
	    $usedCmid = array();
	    foreach ($modinfo->get_section_info_all() as $sectionnum => $csi) {
	        // 강좌에 설정된 numsections보다 작은 경우에만 표시되어야 함.
	        if ($sectionnum <= $numsections) {
	            // 주차가 사용중인 경우에만 표시되어야 함.
	            if ($csi->visible) {
	                $sectionInfo = new stdClass();
	                
	                $weekName = $csi->name;
	                
	                // 강좌 관리자가 주차에 대한 이름을 따로 지정하지 않았을 경우..
	                if (empty($weekName)) {
	                    // 0주차는 강좌 개요
	                    if ($csi->section == 0) {
	                        $weekName = get_string('section0name', 'format_' . $course->format);
	                    } else {
	                        $weekName = $CCourse->getSectionName($course, $csi->section);
	                    }
	                }
	                
	                // 주차명
	                $sectionInfo->name = $weekName;
	                
	                // 주차에 추가된 학습 활동 목록...
	                $sectionInfo->activity = array();
	                $sequence = explode(',', $csi->sequence);
	                foreach ($sequence as $s) {
	                    if (empty($mods[$s]) || in_array($s, $passCmids)) {
	                        continue;
	                    }
	                    
	                    $activity = $mods[$s];
	                    
	                    // 표지는 제외, 숨김 처리된 학습자원/활동은 표시되면 안됨.
	                    if ($activity->modname != 'label' && $activity->visible) {
	                        array_push($sectionInfo->activity, $activity);
	                        
	                        // 강좌에 등록된 모든 학습활동의 cmid기록
	                        array_push($usedCmid, $s);
	                    }
	                }
	                
	                array_push($courseSections, $sectionInfo);
	            }
	        } else {
	            // 기본 16주차에서 50주차로 변경 후 다시 16주차로 변경한 경우.. 불필요하게 17~50까지 반복문이 돌기 때문에 break;를 걸어주는게 좋음.
	            break;
	        }
	    }
	    
	    return array($courseSections, $usedCmid, $writeModules, $commentModules, $moduleCount, $resourceCount);
	}
	
	
	/**
	 * logstore_ubstats_activity 테이블에 의해서 가져온 데이터를 crud값에 맞춰서 카운트 수 변경해줌.
	 *
	 * @param object $log
	 * @param object $count
	 * @return string
	 */
	function getStatsActionCount($log, $count)
	{
		global $DB;
		
		// logstore_ubstats_activity는 moduleid로 기록되어 있기 때문에 modules 테이블이 조회해와야됨.
		if (empty($this->modules)) {
			$this->modules = $DB->get_records_sql('SELECT id, name FROM {modules}');
		}
		
		
		if ($log) {
			if (isset($this->modules[$log->moduleid])) {
				$moduleName = $this->modules[$log->moduleid]->name;
				
				// event명 규칙이 정해져있기 때문에 logstore_ubstats_activity에 전체 이벤트명을 입력할 필요가 없음.
				$eventName = "\\mod_".$moduleName."\\event\\".$log->target.'_'.$log->action;
				
				if (in_array($moduleName, $this->getWriteEventModules())) {
					
					$functionname = 'getCheckEvent'.ucfirst($moduleName);
					if ($event_list = $this->$functionname()) {
					    if (isset($event_list['viewed']) && in_array($eventName, $event_list['viewed'])) {
							$count->viewed = $count->viewed + $log->action_count;
					    } else if (isset($event_list['created']) && in_array($eventName, $event_list['created'])) {
							$count->created = $count->created + $log->action_count;
					    } else if (isset($event_list['commented']) && in_array($eventName, $event_list['commented'])) {
							$count->commented = $count->commented + $log->action_count;
						}
					}
				} else {
					if (strpos($eventName, 'course_module_viewed') !== false) {
						$count->viewed = $count->viewed + $log->action_count;
					}
				}
			}
		}
		
		return $count;
	}
	
	
	/**
	 * logstore_standard_log 테이블에 의해서 가져온 데이터를 crud값에 맞춰서 카운트 수 변경해줌.
	 *
	 * @param object $log
	 * @param object $count
	 * @return string
	 */
	function getLogActionCount($log, $count)
	{
		if ($log) {
			
			$eventName = strtolower($log->eventname);
			$component = str_replace('mod_', '', $log->component);
	
			// view, created를 명확하게 구분하는 모듈인 경우
			if (in_array($component, $this->getWriteEventModules())) {
				$functionname = 'getCheckEvent'.ucfirst($component);
				if ($event_list = $this->$functionname()) {
					if (isset($eventName['viewed']) && in_array($eventName, $event_list['viewed'])) {
						$count->viewed = $count->viewed + 1;
					} else if (isset($eventName['created']) && in_array($eventName, $event_list['created'])) {
						$count->created = $count->created + 1;
					} else if (isset($eventName['commented']) && in_array($eventName, $event_list['commented'])) {
						$count->commented = $count->commented + 1;
					}
				}
			} else {
				// 학습자원이기 때문에 viewed에 대해서만 read를 + 1해줘야됨.
				if (strpos($eventName, 'course_module_viewed') !== false) {
					$count->viewed = $count->viewed + 1;
				}
			}
		}
	
		return $count;
	}
	
	
	// 과제 => 최종 제출 : \mod_assign\event\assessable_submitted
	function getCheckEventAssign()
	{
		return array(
				'viewed'=>array(
						'\mod_assign\event\course_module_viewed'
						,'\mod_assign\event\submission_status_viewed'
	
				)
				,'created'=>array(
						'\mod_assign\event\assessable_submitted'
	
				)
		);
	}
	
	function getCheckEventUbboard()
	{
		return array(
				'viewed'=>array(
						'\mod_ubboard\event\article_viewed'
				)
				,'created'=>array(
						'\mod_ubboard\event\article_created'
				),'commented'=>array(
						'\mod_ubboard\event\comment_created'
	
				)
		);
	}
	
	function getCheckEventForum()
	{
		// forum에서는 단독주제형식, 블로그 형식 등 리스트 페이지가 표시되지 않는 포럼 형태는 course_module_viewed로 기록이 남습니다.
		return array(
				'viewed'=>array(
						'\mod_forum\event\course_module_viewed'
						,'\mod_forum\event\discussion_viewed'
				)
				,'created'=>array(
						'\mod_forum\event\discussion_created'
				)
				,'commented'=>array(
						'\mod_forum\event\post_created'
	
				)
		);
	}
	
	
	function getCheckEventQuiz()
	{
		// forum에서는 단독주제형식, 블로그 형식 등 리스트 페이지가 표시되지 않는 포럼 형태는 course_module_viewed로 기록이 남습니다.
		return array(
				'viewed'=>array(
						'\mod_quiz\event\course_module_viewed'
				)
				,'created'=>array(
						'\mod_quiz\event\attempt_submitted'
				)
		);
	}
	
	function getCheckEventWiki()
	{
		// forum에서는 단독주제형식, 블로그 형식 등 리스트 페이지가 표시되지 않는 포럼 형태는 course_module_viewed로 기록이 남습니다.
		return array(
				'viewed'=>array(
						'\mod_wiki\event\course_module_viewed'
						,'\mod_wiki\event\comments_viewed'
						,'\mod_wiki\event\page_viewed'
				)
				,'created'=>array(
						'\mod_wiki\event\comment_created'
						,'\mod_wiki\event\page_created'
						,'\mod_wiki\event\page_updated'
				)
		);
	}
	
	function getCheckEventChoice()
	{
		// forum에서는 단독주제형식, 블로그 형식 등 리스트 페이지가 표시되지 않는 포럼 형태는 course_module_viewed로 기록이 남습니다.
		return array(
				'viewed'=>array(
						'\mod_choice\event\course_module_viewed'
				)
				,'created'=>array(
						'\mod_choice\event\answer_submitted'
				)
		);
	}
	
	function getCheckEventFeedback()
	{
		// forum에서는 단독주제형식, 블로그 형식 등 리스트 페이지가 표시되지 않는 포럼 형태는 course_module_viewed로 기록이 남습니다.
		return array(
				'viewed'=>array(
						'\mod_feedback\event\course_module_viewed'
				)
				,'created'=>array(
						'\mod_feedback\event\response_submitted'
				)
		);
	}
	
	
	function getCheckEventGlossary()
	{
		// forum에서는 단독주제형식, 블로그 형식 등 리스트 페이지가 표시되지 않는 포럼 형태는 course_module_viewed로 기록이 남습니다.
		return array(
				'viewed'=>array(
						'\mod_glossary\event\course_module_viewed'
						,'\mod_glossary\event\entry_created'
				)
				,'created'=>array(
						'\mod_glossary\event\entry_created'
						,'\mod_glossary\event\comment_created'
				)
		);
	}
	
	function getCheckEventSurvey()
	{
		// forum에서는 단독주제형식, 블로그 형식 등 리스트 페이지가 표시되지 않는 포럼 형태는 course_module_viewed로 기록이 남습니다.
		return array(
				'viewed'=>array(
						'\mod_survey\event\course_module_viewed'
				)
				,'created'=>array(
						'\mod_survey\event\response_submitted'
				)
		);
	}
	
	function getCheckEventUbpeer()
	{
	    // forum에서는 단독주제형식, 블로그 형식 등 리스트 페이지가 표시되지 않는 포럼 형태는 course_module_viewed로 기록이 남습니다.
	    return array(
	        'viewed'=>array(
	            '\mod_ubpeer\event\course_module_viewed'
	        )
	        ,'created'=>array(
	            '\mod_ubpeer\event\evaluation_answered'
	        )
	    );
	}
	
	
	function doDetail()
	{
        global $USER;

	    $id = Parameter::post('id', null, PARAM_INT);
	    $modname = Parameter::post('modname', null, PARAM_ALPHANUMEXT);
	    $cmid = Parameter::post('cmid', null, PARAM_INT);
	    $userid = Parameter::post('userid', null, PARAM_INT);
	    $viewtype = Parameter::post('viewtype', 'viewed', PARAM_ALPHANUMEXT);
	    if ($viewtype != 'viewed' && $viewtype != 'created') {
	        $viewtype = 'viewed';
	    }
	    
	    $validate = $this->validate()->make(array(
	        'id' => array(
	            'required', 'number'
	            ,$this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion')
	            ,$this->validate()::PARAMVALUE => $id
	        )
	        ,'modname' => array(
	            'required', 'alphaNumExt'
	            ,$this->validate()::PLACEHOLDER => 'modname'
	            ,$this->validate()::PARAMVALUE => $modname
	        )
	        ,'cmid' => array(
	            'required', 'number'
	            ,$this->validate()::PLACEHOLDER => 'cmid'
	            ,$this->validate()::PARAMVALUE => $cmid
	        )
	        ,'userid' => array(
	            'required', 'number'
	            ,$this->validate()::PLACEHOLDER => get_string('userid', 'local_ubion')
	            ,$this->validate()::PARAMVALUE => $userid
	        )
	    ));
	    
	    if ($validate->fails()) {
	        Javascript::printAlert($validate->getErrorMessages());
	    } else {
	        // 교수자가 아니라면 본인 정보만 확인 가능함.
	        if (!\local_ubion\course\Course::getInstance()->isProfessor($id)) {
	            $userid = $USER->id;
	        }
	        
	        $logs = $this->getView($id, $userid, $cmid, $modname, $viewtype);
	        $logs_count = count($logs);
	        
	        Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'logs'=>$logs, 'logs_count'=>$logs_count));
	    }
	}
}