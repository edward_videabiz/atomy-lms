<?php
require ('../../config.php');
require_once $CFG->dirroot . '/course/lib.php';
use \local_ubion\base\Common;
use \local_ubion\base\Paging;
use \local_ubion\base\Parameter;

$reportName = 'report_ubstatistics';
$CCourse = \local_ubion\course\Course::getInstance();
$CUser = \local_ubion\user\User::getInstance();
$CGroup = \local_ubion\group\Group::getInstance();
$CStatistics = new \report_ubstatistics\Statistics();

$id = required_param('id', PARAM_INT);
$page = optional_param('page', 1, PARAM_INT); // page
$page = ($page <= 0) ? 1 : $page;
$ps = optional_param('ps', 10, PARAM_INT); // 페이지 사이즈
$ls = optional_param('ls', 15, PARAM_INT); // 리스트 갯수
$order = optional_param('order', 'idnumber', PARAM_ALPHA);
$orderType = optional_param('order_type', 'ASC', PARAM_ALPHA);
$section = optional_param('section', - 1, PARAM_INT);
$groupid = optional_param('groupid', null, PARAM_INT);

$keyfield = optional_param('keyfield', null, PARAM_ALPHANUMEXT);
$keyword = optional_param('keyword', null, PARAM_NOTAGS);
$keyword = strtolower(htmlspecialchars($keyword));

$courseFormat = $CCourse->getCourseFormat($id);
$course = $courseFormat->get_course();
$context = context_course::instance($course->id);

$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);

// 모바일이면 모바일 페이지로 이동시켜줘야됨.
if (Common::isMobile()) {
    redirect($CFG->wwwroot . '/report/ubstatistics/index_m.php?id=' . $id);
}

$i8n = get_strings(array(
    'search',
    'pluginname',
    'viewmode_view',
    'viewmode_create',
    'viewmode_comment',
    'btn_viewed',
    'btn_created',
    'btn_comment',
    'excel_down',
    'heading_user_statistics'
), $reportName);

$i8n->group = get_string('groups', 'group');
$i8n->number = get_string('number', 'local_ubion');
$i8n->idnumber = get_string('idnumber');
$i8n->fullnameuser = get_string('fullnameuser');
$i8n->department = get_string('department');

$PAGE->set_url('/report/ubstatistics/index.php', array(
    'id' => $id
));
$PAGE->set_pagelayout('report');
$PAGE->set_title($course->fullname . ' : ' . $i8n->pluginname);
$PAGE->set_heading($course->fullname);

$PAGE->requires->strings_for_js(array(
    'not_exists_log'
), $reportName);
$PAGE->requires->strings_for_js(array(
    'excel_message'
), 'local_ubion');

$PAGE->requires->js_call_amd('report_ubstatistics/statistics', 'init', array('courseid' =>  $id, 'url' => $CFG->wwwroot.'/report/ubstatistics/action.php'));

echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->pluginname);

// 권한 점검
if (! $isManager = $CStatistics->isManager($context)) {
    echo '<div class="alert alert-danger">' . get_string('nopermissions', 'error', $i8n->pluginname) . '</div>';
} else {
    
    // 강좌에서 그룹을 분류해서 사용하는지 확인
    $courseGroup = $CGroup->getCourseGroups($course->id);
    
    // 필요한 정보 가져오기
    list ($courseSections, $usedCmid, $writeModules, $commentModules, $moduleCount, $resourceCount) = $CStatistics->getCourseSections($course);
    
    
    ?>
    <div class="report_statistics">
    	<div class="search well">
    		<form method="get" class="form-horizontal">
    			<div class="form-group row">
    				<label class="control-label col-sm-3"><?php print_string('paging_number_list', 'local_ubion'); ?></label>
    				<div class="col-sm-9">
    					<select name="ls" class="form-control form-control-auto auto_submit">
    						<?php
        					if ($listSizes = Parameter::getListSizes(true)) {
        					    foreach ($listSizes as $lsKey => $lsValue) {
        					        $selected = ($lsKey == $ls) ? ' selected="selected"' : '';
        					        echo '<option value="'.$lsKey.'" '.$selected.'>'.$lsValue.'</option>';
        					    }
        					}
        					?>
    					</select>
    				</div>
    			</div>
    			<div class="form-group row">
    				<label class="control-label col-sm-3" for="sections"><?=get_string('sectionname', 'format_' . $course->format);?></label>
    				<div class="col-sm-9">
    					<select name="section" class="form-control form-control-auto auto_submit">
    						<option value="-1"><?=get_string('all')?></option>
    						<?php
                            // 강좌내 주차
                            foreach ($courseSections as $sectionnum => $cs) {
                                $selected = ($section == $sectionnum) ? ' selected="selected"' : '';
                                echo '<option value="' . $sectionnum . '" ' . $selected . '>' . $cs->name . '</option>';
                            }
                            ?>
                        </select>
					</div>
				</div>
				<?php
				// 강좌내 그룹이 존재하는 경우
                if ($courseGroup->is) {
                ?>
				<div class="form-group row">
					<label class="control-label col-sm-3" for="course_group"><?=$i8n->group;?></label>
					<div class="col-sm-9">
						<select name="groupid" class="form-control form-control-auto auto_submit" id="course_group">
							<?php
							echo $CGroup->getOptions($courseGroup, $groupid);
							?>
						</select>
					</div>
				</div>
				<?php
                }
                ?>
                <div class="form-group row">
                	<label class="control-label col-sm-3" for="search_keyword"><?=$i8n->search;?></label>
                	<div class="col-sm-9 form-inline">
						<input type="hidden" name="id" value="<?=$id;?>" />
						<select name="keyfield" class="form-control form-control-auto">
							<?php
							$selected = array(
							    'idnumber' => '',
							    'fullname' => ''    
							);
							if (isset($selected[$keyfield])) {
							    $selected[$keyfield] = ' selected="selected"';
							}
							?>
							<option value="idnumber" <?=$selected['idnumber'];?>><?=get_string('idnumber');?></option>
							<option value="fullname" <?=$selected['fullname'];?>><?=get_string('fullname');?></option>
						</select>
						
						<input type="text" name="keyword" placeholder="<?=$i8n->search;?>" value="<?=$keyword;?>" class="form-control form-control-auto" />
						
						<input type="submit" value="<?=$i8n->search;?>" class="btn btn-default" />
						<?php
						if (! Common::isNullOrEmpty($keyfield) && ! Common::isNullOrEmpty($keyword)) {
						    echo '<a href="' . $PAGE->url->out() . '" class="btn btn-default">' . get_string('search_cancel', 'local_ubion') . '</a>';
						}
						?>
    				</div>
    			</div>
    			<div class="form-group row">
    				<label class="control-label col-sm-3"><?=$i8n->excel_down;?></label>
    				<div class="col-sm-9">
    					<a href="<?=$CFG->wwwroot . '/report/ubstatistics/excel.php?id=' . $course->id;?>" class="btn btn-success btn-excel">
    					<?=$i8n->excel_down;?>
    					</a>
    				</div>
    			</div>
    		</form>
    	</div>
    	<div class="course_status">
    		<h4 class="heading_status"><?=get_string('heading_course_structure', $reportName);?></h4>
    		<div class="well">
				<div class="row">
					<dl class="col-xs-6 col-sm-4">
						<dt class="text-center"><?=get_string('number_student', $reportName);?></dt>
						<dd class="text-center"><?=number_format(count_role_users($CCourse->getRoleStudent(), $context));?></dd>
					</dl>
					<dl class="col-xs-6 col-sm-4">
						<dt class="text-center"><?=get_string('number_file', $reportName);?></dt>
						<dd class="text-center"><?=number_format($resourceCount);?></dd>
					</dl>
					<dl class="col-xs-6 col-sm-4">
						<dt class="text-center"><?=get_string('number_activity', $reportName);?></dt>
						<dd class="text-center"><?=number_format($moduleCount);?></dd>
					</dl>
				</div>
			</div>
		</div>
		<div class="user_status">
			<h4 class="heading_status">
				<?=$i8n->heading_user_statistics;?>
			</h4>
			<div class="well">
			<?php
            
			
			// 정렬
            $idnumberLink = Common::getSortLink($i8n->idnumber, 'idnumber', 'order', 'order_type');
            $fullnameLink = Common::getSortLink($i8n->fullnameuser, 'fullname', 'order', 'order_type');
            
            // 사용자 정보 가져오기
            if (empty($ls)) {
                $users = $CCourse->getCourseUserAll($course, null, $groupid, $order, $orderType);
                $totalCount = count($users);
            } else {
                list ($totalCount, $users) = $CCourse->getCourseUsers($course, null, $keyfield, $keyword, $groupid, null, $page, $ls, $order, $orderType);
            }
            
            $rowspan = 3;
            
            if ($totalCount > 0) {
            ?>
            	<div class="user_statistics table-responsive">
            		<table class="table table-bordered table-coursemos table-statistics">
                        <thead>
                        	<tr>
                        		<th class="min-wp-50" rowspan="<?=$rowspan;?>"><?=$i8n->number;?></th>
                        		<th class="min-wp-100" rowspan="<?=$rowspan;?>"><?=$idnumberLink;?></th>
                        		<th class="min-wp-120" rowspan="<?=$rowspan;?>"><?=$fullnameLink;?></th>
                        		<th class="min-wp-100 border_line" rowspan="<?=$rowspan;?>"><?=$i8n->department;?></th>
                        		<?php
                                if ($courseGroup->is) {
                                    echo '<th class="min-wp-80 border_line" rowspan="' . $rowspan . '">' . get_string('groups', 'group') . '</th>';
                                }
                                
                                foreach ($courseSections as $sectionnum => $csi) {
                                    // 주차를 따로 선택한 경우
                                    if ($section >= 0 && $sectionnum != $section) {
                                        continue;
                                    }
                                    
                                    // 보기는 무조건 표시이기 때문에 1부터 시작
                                    $colspan = 0;
                                    foreach ($csi->activity as $activity) {
                                        $isWrite = (in_array($activity->modname, $writeModules)) ? true : false;
                                        $isComment = (in_array($activity->modname, $commentModules)) ? true : false;
                                        
                                        // 보기는 모든 항목이 존재하므로 무조건 +1해줘야됨.
                                        $colspan ++;
                                        
                                        if ($isWrite) {
                                            $colspan ++;
                                        }
                                        
                                        if ($isComment) {
                                            $colspan ++;
                                        }
                                    }
                                    $colspanStr = '';
                                    if ($colspan > 1) {
                                        $colspanStr = 'colspan="' . $colspan . '"';
                                    }
                                    
                                    echo '<th ' . $colspanStr . ' class="ellipsis header border_line text-center" title="' . $csi->name . '">' . $csi->name . '</th>';
                                }
                                ?>
                            </tr>
                            <tr>
                            <?php
                            foreach ($courseSections as $sectionnum => $csi) {
                                // 주차를 따로 선택한 경우
                                if ($section >= 0 && $sectionnum != $section) {
                                    continue;
                                }
                                
                                if ($csi->activity) {
                                    $tempi = 0;
                                    foreach ($csi->activity as $activity) {
                                        $borderLineClass = ($tempi == 0) ? ' border_line ' : '';
                                        $activityClass = ($tempi > 0) ? ' activity_line ' : '';
                                        
                                        $isWrite = (in_array($activity->modname, $writeModules)) ? true : false;
                                        $isComment = (in_array($activity->modname, $commentModules)) ? true : false;
                                        
                                        // 보기는 무조건 표시이기 때문에 1부터 시작
                                        $colspan = 1;
                                        
                                        if ($isWrite) {
                                            $colspan ++;
                                        }
                                        
                                        if ($isComment) {
                                            $colspan ++;
                                        }
                                        
                                        $moduleUrl = $CFG->wwwroot . '/mod/' . $activity->modname . '/view.php?id=' . $activity->id;
                                        echo '<th class="ellipsis ' . $borderLineClass . ' th_moduleicon nofixed ' . $activityClass . ' ' . $activity->modname . '" colspan="' . $colspan . '">';
                                        echo    '<a href="' . $moduleUrl . '" target="_blank">';
                                        
                                        $moduleIcon = $activity->get_icon_url();
                                        if ($activity->modname == 'ubboard') {
                                            // 게시판 타일을 알아와서 알맞는 이미지를 출력시켜줘야됨.
                                            $boardType = $DB->get_field_sql("SELECT type FROM {ubboard} WHERE id = :id", array(
                                                'id' => $activity->instance
                                            ));
                                            
                                            if ($boardType == 'qna' || $boardType == 'notice') {
                                                $moduleIcon = $OUTPUT->image_url('ubboard_' . $boardType, 'ubboard');
                                            }
                                        }
                                        echo        '<img src="' . $moduleIcon . '" alt="' . $activity->name . '" title="' . $activity->name . '" data-toggle="tooltip" />';
                                        echo    '</a>';
                                        echo '</th>';
                                        $tempi ++;
                                    }
                                } else {
                                    echo '<th class="ellipsis th_moduleicon border_line nofixed">&nbsp;</th>';
                                }
                            }
                            ?>
                            </tr>
                            <tr>
                            <?php
                            foreach ($courseSections as $sectionnum => $csi) {
                                // 주차를 따로 선택한 경우
                                if ($section >= 0 && $sectionnum != $section) {
                                    continue;
                                }
                                
                                if ($csi->activity) {
                                    $tempi = 0;
                                    foreach ($csi->activity as $activity) {
                                        $borderLineClass = ($tempi == 0) ? ' border_line ' : '';
                                        $activityClass = ($tempi > 0) ? ' activity_line ' : '';
                                        
                                        $isWrite = (in_array($activity->modname, $writeModules)) ? true : false;
                                        $isComment = (in_array($activity->modname, $commentModules)) ? true : false;
                                        
                                        echo '<th class="ellipsis ' . $borderLineClass . ' th_moduleicon th_rw nofixed ' . $activityClass . '">';
                                        echo $i8n->btn_viewed;
                                        echo '</th>';
                                        
                                        if ($isWrite) {
                                            echo '<th class="ellipsis th_moduleicon th_rw nofixed">';
                                            echo $i8n->btn_created;
                                            echo '</th>';
                                        }
                                        
                                        if ($isComment) {
                                            echo '<th class="ellipsis th_moduleicon th_rw nofixed">';
                                            echo $i8n->btn_comment;
                                            echo '</th>';
                                        }
                                        
                                        $tempi ++;
                                    }
                                } else {
                                    echo '<th class="ellipsis th_moduleicon th_rw border_line nofixed">&nbsp;</th>';
                                }
                            }
                            ?>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $printNum = ($page - 1) * $ls;
                        $nullValueString = '-';
                        $use_cmid_string = join(',', $usedCmid);
                        
                        foreach ($users as $user) {
                            $printNum++;
                            
                            $user_fullname = fullname($user);
                            echo '<tr>';
                            echo    '<td class="text-center">' . $printNum . '</td>';
                            echo    '<td class="text-center">' . $CUser->getIdnumber($user->idnumber, true) . '</td>';
                            echo    '<td class="text-center"><div class="fullname ellipsis" title="' . $user_fullname . '">' . $user_fullname . '</div></td>';
                            echo    '<td class="border_line"><div class="department ellipsis" title="' . $user->department . '">' . $user->department . '</div></td>';
                            
                            $groupName = '';
                            if ($courseGroup->is) {
                                // 사용자가 강좌내 여러 그룹에 소속될수 있기 때문에 별도 디비로 조회하는게 효율적입니다.
                                // 효율적 => 다중 DBMS 대응하기 위함
                                if ($user_group = $CGroup->getUserGroups($course->id, $user->id)) {
                                    foreach ($user_group as $ug) {
                                        $groupName .= $CGroup->getBadge($ug->name);
                                    }
                                }
                                echo '<td class="text-center border_line">' . $groupName . '</td>';
                            }
                            
                            // 사용자 로그 기록 가져오기
                            $logs = $CStatistics->getListAll($course->id, $user->id, $use_cmid_string);
                            $logdetailClass = (get_config('report_ubstatistics', 'isdetail') == 1) ? ' btn-logdetail' : 'btn-nobtn';
                            foreach ($courseSections as $sectionnum => $csi) {
                                // 주차를 따로 선택한 경우
                                if ($section >= 0 && $sectionnum != $section) {
                                    continue;
                                }
                                
                                if ($csi->activity) {
                                    $tempi = 0;
                                    foreach ($csi->activity as $activity) {
                                        $borderLineClass = ($tempi == 0) ? ' border_line ' : '';
                                        $activityClass = ($tempi > 0) ? ' activity_line ' : '';
                                        
                                        $logCountViewed = '-'; // view 항목
                                        $logCountCreated = '-'; // created 항목
                                        $logCountCommented = '-'; // commented 항목
                                        
                                        $isWrite = (in_array($activity->modname, $writeModules)) ? true : false;
                                        $isComment = (in_array($activity->modname, $commentModules)) ? true : false;
                                        
                                        // logs에서 cmid값을 검사해서 존재할 경우
                                        if (isset($logs[$activity->id])) {
                                            $log = $logs[$activity->id];
                                            
                                            $default_attribute = 'data-modname="' . $activity->modname . '" data-cmid="' . $activity->id . '" data-userid="' . $user->id . '"';
                                            
                                            if ($log->viewed > 0) {
                                                $attribute = $default_attribute . ' data-viewtype="viewed"';
                                                $logCountViewed = '<button type="button" class="btn btn-default btn-sm btn-tooltip ' . $logdetailClass . '" ' . $attribute . ' title="' . $i8n->viewmode_view . '">' . $log->viewed . '</button>';
                                            }
                                            
                                            if ($log->created > 0) {
                                                $attribute = $default_attribute . ' data-viewtype="created"';
                                                $logCountCreated = '<button type="button" class="btn btn-default btn-sm btn-tooltip ' . $logdetailClass . '" ' . $attribute . ' title="' . $i8n->viewmode_create . '">' . $log->created . '</button>';
                                            }
                                            
                                            if ($log->commented > 0) {
                                                $attribute = $default_attribute . ' data-viewtype="commented"';
                                                $logCountCommented = '<button type="button" class="btn btn-default btn-sm btn-tooltip ' . $logdetailClass . '" ' . $attribute . ' title="' . $i8n->viewmode_comment . '">' . $log->commented . '</button>';
                                            }
                                        }
                                        
                                        echo '<td class="text-center ' . $borderLineClass . $activityClass . '">';
                                        echo $logCountViewed;
                                        echo '</td>';
                                        
                                        if ($isWrite) {
                                            echo '<td class="text-center">';
                                            echo $logCountCreated;
                                            echo '</td>';
                                        }
                                        
                                        if ($isComment) {
                                            echo '<td class="text-center">';
                                            echo $logCountCommented;
                                            echo '</td>';
                                        }
                                        
                                        $tempi ++;
                                    }
                                } else {
                                    echo '<td class="border_line text-center">' . $nullValueString . '</td>';
                                }
                            }
                            
                            echo '</tr>';
                            
                        }
                        ?>
    					</tbody>
    				</table>
    			</div>
    			<?php
                } else {
                    echo get_string('not_found_users', 'local_ubion');
                }
                ?> 
            </div>
            <?= Paging::printHTML($totalCount, $page, $ls); ?>
		</div>
		<div class="modal modal-log-detail" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h5 class="modal-title"><?=$i8n->heading_user_statistics;?></h5>
					</div>
					<div class="modal-body">
						<?php 
						echo $OUTPUT->render_from_template('report_ubstatistics/log_detail', null);
						?>
					</div>
					<div class="modal-footer">
						<button class="btn btn-default" data-dismiss="modal"><?= get_string('close', 'local_ubion'); ?></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
    
    // Trigger a report viewed event.
    $event = \report_ubstatistics\event\report_viewed::create(array(
        'context' => $context
    ));
    $event->trigger();
}
echo $OUTPUT->footer();