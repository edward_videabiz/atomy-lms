<?php
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2018071400;     // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2014050800;     // Requires this Moodle version
$plugin->component = 'report_ubstatistics'; // Full name of the plugin (used for diagnostics)
$plugin->dependencies = array('local_ubion' => 2017010100, 'logstore_ubstats' => 2014070800);