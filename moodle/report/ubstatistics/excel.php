<?php
require('../../config.php');

header("Set-Cookie: fileDownload=true; path=/");

require_once $CFG->dirroot.'/course/lib.php';
require_once $CFG->libdir.'/excellib.class.php';

$pluginname = 'report_ubstatistics';
$CCourse = \local_ubion\course\Course::getInstance();
$CUser = \local_ubion\user\User::getInstance();
$CGroup = \local_ubion\group\Group::getInstance();
$CStatistics = new \report_ubstatistics\Statistics();

$id 		= required_param('id', PARAM_INTEGER);
$page       = optional_param('page', 0, PARAM_INT);     		// page
$ps			= optional_param('ps', 10, PARAM_INT);				// 페이지 사이즈
$ls			= optional_param('ls', 15, PARAM_INT);				// 리스트 갯수
$order		= optional_param('order', 'idnumber', PARAM_ALPHA);
$order_type = optional_param('order_type', 'ASC', PARAM_ALPHA);
$all		= optional_param('all', null, PARAM_ALPHA);

$keyfield = optional_param('keyfield', null, PARAM_ALPHANUMEXT);
$keyword = optional_param('keyword', null, PARAM_NOTAGS);
$keyword = strtolower(htmlspecialchars($keyword));

$course = course_get_format($id)->get_course(); // Needed to have numsections property available.
$context = context_course::instance($course->id);

$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);

$PAGE->set_url('/report/ubstatistics/excel.php', array('id' => $id));

$i8n = get_strings(array(
		'pluginname',
		'viewmode_view',
		'viewmode_create',
		'viewmode_comment'
), $pluginname);

// 권한 점검
if (!$isManager = $CStatistics->isManager($context)) {
	// 학습현황은 관리자(교수, 조교)만 보기 때문에 사용자 페이지로 이동을 시킬수가 없음.
	echo $OUTPUT->header();
	echo $OUTPUT->notification(get_string('nopermissions', 'error', $i8n->pluginname));
	echo $OUTPUT->footer();
	exit;
} else {

    // 강좌에서 그룹을 분류해서 사용하는지 확인
    // 엑셀 다운로드는 권한이 없으면 다운로드가 안되며, 모든 수강생을 다 받기 때문에 별다른 조건 처리 하지 않아도 됨.
    $courseGroup = $CGroup->getCourseGroups($course->id);
    
    list ($courseSections, $usedCmid, $writeModules, $commentModules, $moduleCount, $resourceCount) = $CStatistics->getCourseSections($course);
    $cmCount = count($usedCmid);
	
	// 강좌에서 사용중인 모듈이 존재하는 경우에만 표시
    if ($cmCount > 0) {
		// Excel download 부분 시작...
		// excel layout 잡기.
		$workbook = new MoodleExcelWorkbook('-');
		$filename = $CCourse->getName($course).'_statistics.xls';
		$workbook->send($filename);
	
		$format = new stdClass();
		$format->title = $workbook->add_format(array('bold'=>1, 'v_align'=>'vcenter', 'size'=>12));
		$format->head = $workbook->add_format(array('align'=>'center', 'bold'=>1, 'v_align'=>'vcenter', 'border'=>1, 'bg_color'=>'#54728C', 'color'=>'white', 'text_wrap'=>true));
		$format->head2 = $workbook->add_format(array('align'=>'center', 'bold'=>1, 'v_align'=>'vcenter', 'border'=>1, 'bg_color'=>'#F5F5F5', 'color'=>'black', 'text_wrap'=>true));
		$format->head_right = $workbook->add_format(array('align'=>'right', 'bold'=>1, 'v_align'=>'vcenter'));
		$format->body = $workbook->add_format(array('align'=>'center', 'border'=>1, 'v_align'=>'vcenter', 'text_wrap'=>true));
		$format->body_left = $workbook->add_format(array('align'=>'left', 'border'=>1, 'v_align'=>'vcenter', 'text_wrap'=>true));
		$format->body_right = $workbook->add_format(array('align'=>'right', 'v_align'=>'vcenter', 'text_wrap'=>true));
	
	
		$worksheet = array();
		$sheetnum = 0;
		$rownum = 0;
		$worksheet[$sheetnum] = $workbook->add_worksheet('');
	
		$cellIndex = 0;
	
		// ====================== 엑셀 cell 설정 시작.. ======================
	
		// ROW 0 : 강의실 운영현황
		$worksheet[$sheetnum]->set_row($rownum, 20);
		$worksheet[$sheetnum]->write_string($rownum, 0, get_string('heading_course_structure', $pluginname), $format->title);
		$worksheet[$sheetnum]->merge_cells($rownum, 0, 0, 3);
	
		// ROW 1 : 수강생
		$rownum = 1;
		$worksheet[$sheetnum]->set_row($rownum, 20);
		$worksheet[$sheetnum]->write_string($rownum, 0, get_string('number_student', $pluginname), $format->head_right);
		$worksheet[$sheetnum]->merge_cells($rownum, 0, $rownum, 1);
	
		$worksheet[$sheetnum]->write_number($rownum, 2, count_role_users($CCourse->getRoleStudent(), $context), $format->body_right);
	
	
		// ROW 2 : 강의 자료수
        $rownum++;
		$worksheet[$sheetnum]->set_row($rownum, 20);
		$worksheet[$sheetnum]->write_string($rownum, 0, get_string('number_file', $pluginname), $format->head_right);
		$worksheet[$sheetnum]->merge_cells($rownum, 0, $rownum, 1);
	
		$worksheet[$sheetnum]->write_number($rownum, 2, $resourceCount, $format->body_right);
	
		// ROW 3 : 강의 활동 수
		$rownum++;
		$worksheet[$sheetnum]->set_row($rownum, 20);
		$worksheet[$sheetnum]->write_string($rownum, 0, get_string('number_activity', $pluginname), $format->head_right);
		$worksheet[$sheetnum]->merge_cells($rownum, 0, $rownum, 1);
	
		$worksheet[$sheetnum]->write_number($rownum, 2, $moduleCount, $format->body_right);
	
	
		// 전체 학생
		$totalCount = 0;
		$users = $CCourse->getCourseUserAll($course, array(), null, $order, $order_type);
	
		// ROW 4 ~ 5 : 공란
		// ROW 6 : 강의실 운영현황
		$rownum = 6;
		$worksheet[$sheetnum]->set_row($rownum, 20);
		$worksheet[$sheetnum]->write_string($rownum, 0, get_string('heading_user_statistics', $pluginname), $format->title);
		$worksheet[$sheetnum]->merge_cells($rownum, 0, $rownum, 3);
	
	
		// 구분 및 주차 명
		$rownum = 7;
		$cellIndex = 0;
		$worksheet[$sheetnum]->set_row($rownum, 30);
		$worksheet[$sheetnum]->write_string($rownum, $cellIndex, get_string('category', $pluginname), $format->head);
		$cellIndex++;
		
		$worksheet[$sheetnum]->write_string($rownum, $cellIndex, '', $format->head);	// cell merge시 엑셀 cell에 선이 안그어지는 문제 방지를 위해서 빈값으로 설정
		$cellIndex++;
		
		if ($courseGroup->is) {
			$worksheet[$sheetnum]->write_string($rownum, $cellIndex, '', $format->head);	// cell merge시 엑셀 cell에 선이 안그어지는 문제 방지를 위해서 빈값으로 설정
			$cellIndex++;
		}
		
		$worksheet[$sheetnum]->write_string($rownum, $cellIndex, '', $format->head);	// cell merge시 엑셀 cell에 선이 안그어지는 문제 방지를 위해서 빈값으로 설정
		$cellIndex++;
		
		$worksheet[$sheetnum]->write_string($rownum, $cellIndex, '', $format->head);	// cell merge시 엑셀 cell에 선이 안그어지는 문제 방지를 위해서 빈값으로 설정
		$worksheet[$sheetnum]->merge_cells($rownum, 0, $rownum, $cellIndex);
		$cellIndex++;
		
		// 주차명
		foreach ($courseSections as $csi) {
	
			$activity_count = count($csi->activity);
	
			//echo '<th '.$colspan_str.' class="rellipsis header" title="'.$csi->name.'">'.$csi->name.'</th>';
			$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $csi->name, $format->head);
	
			// 주차내에 등록된 학습자원/활동이 없는 경우
			if ($activity_count > 0) {
				$mergeCount = 0;
				
				foreach ($csi->activity as $activity) {
				    $isWrite = (in_array($activity->modname, $writeModules)) ? true : false;
					$isComment = (in_array($activity->modname, $commentModules)) ? true : false;
				
					// 보기
					$mergeCount++; 
					
					if ($isWrite) {
					    $mergeCount++;
					}
					
					if ($isComment) {
					    $mergeCount++;
					}
				}
				
				
				// cell_index 부터 주차에 등록된 학습자원/활동 수이기 때문에 -1를 해야됨.
				// cell_index가 4이고, 등록된 학습자원/활동이 4개라면 합이 8이지만 index값으로 따지면 4,5,6,7이므로 -1개를 해줘야됨.
				$cellIndex_plus = $cellIndex + $mergeCount - 1;
					
				// 반복은 cell_index + 1부터 시작해야됨.
				// 안그럼 위에서 설정한 $csi->name값도 빈값 처리됨.
				for($i=$cellIndex+1; $i<=$cellIndex_plus; $i++) {
					$worksheet[$sheetnum]->write_string($rownum, $i, '', $format->head);
				}
					
				// 주차내에 등록된 학습자원/활동이 있는 경우 cell merge
				$worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $rownum, $cellIndex_plus);
					
				// 다음 cell_index 계산을 위해서 다시 +1해줘야됨.
				$cellIndex = $cellIndex_plus + 1;
			} else {
				// 등록된 학습/자원 활동이 없으면 다음 cell로 넘어가야됨.
				$cellIndex++;
			}
		}
	

		// 실제 데이터 출력 시작
		$rownum = 8;
		$cellIndex = 0;
		$worksheet[$sheetnum]->set_row($rownum, 25);
		$worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 5);	// 번호
		$worksheet[$sheetnum]->write_string($rownum, $cellIndex, get_string('number', 'local_ubion'), $format->head2);
		
		$merge_rownum = $rownum + 1;
		$worksheet[$sheetnum]->write_string($merge_rownum, $cellIndex, '', $format->head2);
		$worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $merge_rownum, $cellIndex);
		$cellIndex++;
		
		
		$worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 15);	// 학번
		$worksheet[$sheetnum]->write_string($rownum, $cellIndex, get_string('idnumber'), $format->head2);
		$merge_rownum = $rownum + 1;
		$worksheet[$sheetnum]->write_string($merge_rownum, $cellIndex, '', $format->head2);
		$worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $merge_rownum, $cellIndex);
		$cellIndex++;
		
		
		$worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 15);	// 성명
		$worksheet[$sheetnum]->write_string($rownum, $cellIndex, get_string('fullnameuser'), $format->head2);
		$merge_rownum = $rownum + 1;
		$worksheet[$sheetnum]->write_string($merge_rownum, $cellIndex, '', $format->head2);
		$worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $merge_rownum, $cellIndex);
		$cellIndex++;
		
	
		// 부서
		$worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 25);	// 부서
		$worksheet[$sheetnum]->write_string($rownum, $cellIndex, get_string('department'), $format->head2);
		$merge_rownum = $rownum + 1;
		$worksheet[$sheetnum]->write_string($merge_rownum, $cellIndex, '', $format->head2);
		$worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $merge_rownum, $cellIndex);
		$cellIndex++;
		// rw row 출력을 해야되는데, 사용자명 이후에 출력이 되어야 하기 때문에... 0부터 시작되면 안됨. 
		$rowRwCellIndex = $cellIndex;
		
		// 그룹
		if ($courseGroup->is) {
			$worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 20);	// 그룹
			$worksheet[$sheetnum]->write_string($rownum, $cellIndex, get_string('groups', 'group'), $format->head2);
			
			$merge_rownum = $rownum + 1;
			$worksheet[$sheetnum]->write_string($merge_rownum, $cellIndex, '', $format->head2);
			$worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $merge_rownum, $cellIndex);
			
			$cellIndex++;
			// rw row 출력을 해야되는데, 사용자명 이후에 출력이 되어야 하기 때문에... 0부터 시작되면 안됨. 
			$rowRwCellIndex = $cellIndex;
		}
		
		// 학습자원/활동 영역
		foreach ($courseSections as $csi) {
			if ($csi->activity) {
				foreach ($csi->activity as $activity) {
				    $isWrite = (in_array($activity->modname, $writeModules)) ? true : false;
					$isComment = (in_array($activity->modname, $commentModules)) ? true : false;
					
					$worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 8);
					$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $activity->name, $format->head2);
					
					// 셀 추가 갯수
					$mergeCount = 0;
					
					if ($isWrite) {
					    $mergeCount++;
					}
					
					if ($isComment) {
					    $mergeCount++;
					}
					
					if ($mergeCount > 0) {
						for($i=1; $i<=$mergeCount; $i++) {
							$worksheet[$sheetnum]->set_column($cellIndex, $cellIndex + $i, 8);	// 부서
							$worksheet[$sheetnum]->write_string($rownum, $cellIndex + $i, '', $format->head2);
						}
						
						$worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $rownum, $cellIndex + $mergeCount);
						$cellIndex += ($mergeCount + 1);
					} else {
						$cellIndex++;
					}
				}
			} else {
				$worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 15);	// 부서
				$worksheet[$sheetnum]->write_string($rownum, $cellIndex, '-', $format->head2);
				$cellIndex++;
			}
		}
		
		// RW를 표시해줘야됨.
		$cellIndex = $rowRwCellIndex;
		$rownum = $rownum + 1;
		$worksheet[$sheetnum]->set_row($rownum, 25);
		foreach ($courseSections as $csi) {
			if ($csi->activity) {
				foreach ($csi->activity as $activity) {
				    
				    $isWrite = (in_array($activity->modname, $writeModules)) ? true : false;
					$isComment = (in_array($activity->modname, $commentModules)) ? true : false;

					
				    $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $i8n->viewmode_view, $format->head2);
					$cellIndex++;
					
					if ($isWrite) {
						$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $i8n->viewmode_create, $format->head2);
						$cellIndex++;
						
					}
					
					if ($isComment) {
					    $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $i8n->viewmode_comment, $format->head2);
						$cellIndex++;
					}
				}
			} else {
				$worksheet[$sheetnum]->write_string($rownum, $cellIndex, '-', $format->head2);
				$cellIndex++;
			}
		}	
		
	
	
		$nullValueCount = '-';
		$rownum = $rownum + 1;
		$useCmidString = join(',', $usedCmid);
		$printNum = 1;
		foreach ($users as $user) {
		    
			$logs = $CStatistics->getListAll($course->id, $user->id, $useCmidString);
	
			$cellIndex = 0;
			//$worksheet[$sheetnum]->set_row($rownum, 25);
	
			$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $printNum, $format->body);
			$cellIndex++;
	
			$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $user->idnumber, $format->body);
			$cellIndex++;
			
			$worksheet[$sheetnum]->write_string($rownum, $cellIndex, fullname($user), $format->body);
			$cellIndex++;
			
			$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $user->department, $format->body_left);
			$cellIndex++;
			
			if ($courseGroup->is) {
				// 사용자가 강좌내 여러 그룹에 소속될수 있기 때문에 별도 디비로 조회하는게 효율적입니다.
				// 효율적 => 다중 DBMS 대응하기 위함
				$groupName = '';
				if ($user_group = $CGroup->getUserGroups($course->id, $user->id)) {
					$comma = '';
					foreach ($user_group as $ug) {
						if (!empty($ug->name)) {
							$groupName .= $comma.$ug->name;
							$comma = "\n";
						}
					}
				}
				
				$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $groupName, $format->body_left);
				$cellIndex++;
			}
	
	
			// 학습현황 시작
			foreach ($courseSections as $csi) {
				if ($csi->activity) {
					$temp_i = 0;
					foreach ($csi->activity as $activity) {
					    
					    $isWrite = (in_array($activity->modname, $writeModules)) ? true : false;
					    
						// 댓글까지 표현되어야 하는 모듈인 경우 댓글 항목 추가
						$isComment = (in_array($activity->modname, $commentModules)) ? true : false;
						
						$logCountViewed = '-';		// view 항목
						$logCountCreated = '-';	// created 항목
						$logCountCommented = '-';	// created 항목
						
						// logs에서 cmid값을 검사해서 존재할 경우
						if (isset($logs[$activity->id])) {
							$log = $logs[$activity->id];
							
							if ($log->viewed > 0) {
								$logCountViewed = $log->viewed;
							}
							
							if ($log->created > 0) {
								$logCountCreated = $log->created;
							}
							
							if ($log->commented > 0) {
								$logCountCommented = $log->commented;
							}
						}	
							


						$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $logCountViewed, $format->body);
						$cellIndex++;
						
						if ($isWrite) {
							$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $logCountCreated, $format->body);
							$cellIndex++;
						}
						
						if ($isComment) {
							$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $logCountCommented, $format->body);
							$cellIndex++;
						}
					}
				} else {
					$worksheet[$sheetnum]->write_string($rownum, $cellIndex, $nullValueCount, $format->body);
					$cellIndex++;
				}
			}
	
			$rownum++;
			$printNum++;
		}
	
		$workbook->close();
		die;
	
	} else {
		echo $OUTPUT->header();
		echo '<div class="alert alert-error">'.get_string('no_modules', $pluginname).'</div>';
		echo $OUTPUT->footer();
		exit;
	}
}

