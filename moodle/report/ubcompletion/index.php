<?php
use local_ubion\base\Common;
use local_ubion\base\Paging;
use local_ubion\base\Parameter;

require '../../config.php';

$id = required_param('id', PARAM_INT);
$keyfield	= optional_param('keyfield', null, PARAM_ALPHANUMEXT);
$keyword	= Parameter::getKeyword();
$groupid    = optional_param('groupid', null, PARAM_INT);
$order		= optional_param(Parameter::getSortBy(), null, PARAM_ALPHA);
$orderType = optional_param(Parameter::getSortOrder(), 'ASC', PARAM_ALPHA);
$section = optional_param('section', - 1, PARAM_INT);
$page		= optional_param('page', 1, PARAM_INT);
$ls			= optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);

$pluginname = 'report_ubcompletion';


$CCourse = \local_ubion\course\Course::getInstance();
$CUser = \local_ubion\user\User::getInstance();
$CGroup = \local_ubion\group\Group::getInstance();
$CCompletion = new \report_ubcompletion\Completion($id);

$course = $CCourse->getCourse($id);
$context = context_course::instance($course->id);

$PAGE->set_context($context);
$PAGE->set_course($course);

require_login($course);
require_capability('report/ubcompletion:view', $context);

$PAGE->set_url('/report/ubcompletion/index.php', array(
    'id' => $id
));
$PAGE->set_pagelayout('report');



$i8n = new stdClass();
$i8n->pluginname = get_string('pluginname', $pluginname);
$i8n->number = get_string('number', 'local_ubion');
$i8n->grade = get_string('grade', 'local_ubion');
$i8n->fullnameuser = get_string('fullnameuser');

$i8n->group = get_string('groups', 'group');
$i8n->department = get_string('department');
$i8n->idnumber = get_string('idnumber');
$i8n->userpic = get_string('userpic');

$i8n->excel = get_string('excel', 'local_ubion');
$i8n->search = get_string('search');

$PAGE->set_title($course->fullname . ' : ' . $i8n->pluginname);
$PAGE->set_heading($course->fullname);

// 강좌에서 그룹을 분류해서 사용하는지 확인
$courseGroup = $CGroup->getCourseGroups($course->id);


$modinfo = get_fast_modinfo($course);
$sections = $CCompletion->getSections($modinfo);


list($totalCount, $users) = $CCompletion->getCourseUsers($course, array(), $keyfield, $keyword, $groupid, null, $page, $ls, $order, $orderType);


$PAGE->requires->js_call_amd('report_ubcompletion/completion', 'index');
$PAGE->requires->js_call_amd('report_progress/completion_override', 'init', [fullname($USER)]);
$PAGE->requires->string_for_js('excel_message', 'local_ubion');

echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->pluginname);
?>
<div class="report-ubcompletion">
    <form method="get" class="form-horizontal well form-search">
    	<div class="form-group row">
    		<label class="control-label col-sm-3"><?php print_string('paging_number_list', 'local_ubion'); ?></label>
    		<div class="col-sm-9">
    			<select name="ls" class="form-control form-control-auto form-control-listsize">
    				<?php
    				if ($listSizes = Parameter::getListSizes(true)) {
    				    foreach ($listSizes as $lsKey => $lsValue) {
    				        $selected = ($lsKey == $ls) ? ' selected="selected"' : '';
    				        echo '<option value="'.$lsKey.'" '.$selected.'>'.$lsValue.'</option>';
    				    }
    				}
    				?>
    			</select>
    		</div>
    	</div>
    	<div class="form-group row">
    		<label class="control-label col-sm-3" for="sections"><?=get_string('sectionname', 'format_' . $course->format);?></label>
    		<div class="col-sm-9">
    			<select name="section" class="form-control form-control-auto form-control-section">
    				<option value="-1"><?=get_string('all')?></option>
    				<?php
                    // 강좌내 주차
    				foreach ($sections as $sectionnum => $sectionInfo) {
                        $selected = ($section == $sectionnum) ? ' selected="selected"' : '';
                        echo '<option value="' . $sectionnum . '" ' . $selected . '>' . $sectionInfo->name . '</option>';
                    }
                    ?>
                </select>
    		</div>
    	</div>
    	<?php
    	// 강좌내 그룹이 존재하는 경우
        if ($courseGroup->is) {
        ?>
    	<div class="form-group row">
    		<label class="control-label col-sm-3" for="course_group"><?=$i8n->group;?></label>
    		<div class="col-sm-9">
    			<select name="groupid" class="form-control form-control-auto form-control-group" id="course_group">
    				<?php
    				echo $CGroup->getOptions($courseGroup, $groupid);
    				?>
    			</select>
    		</div>
    	</div>
    	<?php
        }
        ?>
        <div class="form-group row">
        	<label class="control-label col-sm-3" for="search_keyword"><?=$i8n->search;?></label>
        	<div class="col-sm-9 form-inline">
    			<input type="hidden" name="id" value="<?=$id;?>" />
    			<select name="keyfield" class="form-control form-control-auto">
    				<?php
    				$selected = array(
    				    'idnumber' => '',
    				    'fullname' => ''    
    				);
    				if (isset($selected[$keyfield])) {
    				    $selected[$keyfield] = ' selected="selected"';
    				}
    				?>
    				<option value="idnumber" <?=$selected['idnumber'];?>><?=get_string('idnumber');?></option>
    				<option value="fullname" <?=$selected['fullname'];?>><?=get_string('fullname');?></option>
    			</select>
    			
    			<input type="text" name="keyword" placeholder="<?=$i8n->search;?>" value="<?=$keyword;?>" class="form-control form-control-auto" />
    			
    			<input type="submit" value="<?=$i8n->search;?>" class="btn btn-default" />
    			<?php
    			if (! Common::isNullOrEmpty($keyfield) && ! Common::isNullOrEmpty($keyword)) {
    			    echo '<a href="' . $PAGE->url->out() . '" class="btn btn-default">' . get_string('search_cancel', 'local_ubion') . '</a>';
    			}
    			?>
    		</div>
    	</div>
    	<div class="form-group row">
    		<label class="control-label col-sm-3"><?=$i8n->excel;?></label>
    		<div class="col-sm-9">
    			<a href="<?=$CFG->wwwroot . '/report/ubcompletion/excel.php?id=' . $course->id;?>" class="btn btn-success btn-excel">
    			<?=$i8n->excel;?>
    			</a>
    		</div>
    	</div>
    </form>
    
    <?php
	if ($totalCount > 0) {
	?>
    <div class="completion-status well">
    	<div class="table-responsive">
    		<table id="completion-progress" class="table table-bordered table-coursemos table-mobile-responsive table-completion mb-0">
                <thead>
                	<tr>
                		<?php
						echo '<th rowspan="2" class="fixedcolumn min-wp-50">'.$i8n->number.'</th>';
						echo '<th rowspan="2" class="fixedcolumn min-wp-100">'.Common::getSortLink($i8n->idnumber, 'idnumber').'</th>';
						echo '<th rowspan="2" class="fixedcolumn min-wp-120 leftlastfixedcolumn">'.Common::getSortLink($i8n->fullnameuser, 'fullname').'</th>';
						echo '<th class="borderline min-wp-120" rowspan="2">'.$i8n->department.'</th>';
						
						if ($courseGroup->is) {
							echo '<th rowspan="2" class="min-wp-80">'.$i8n->group.'</th>';
						}
	
						// 주차 표시
						foreach ($sections as $cs) {
						    // 주차를 따로 선택한 경우
						    if ($section >= 0 && $cs->section != $section) {
						        continue;
						    }
						    
							$cmidsCount = count($cs->cmids);
							$colspan = '';
							
							if ($cmidsCount > 1) {
							    $colspan = ' colspan="'.$cmidsCount.'"';
							}

							echo '<th '.$colspan.' class="borderline text-truncate max-wp-50 min-wp-50" title="'.$cs->name.'" data-toggle="tooltip" data-container="body">'.$cs->name.'</th>';
						}
						?>
                	</tr>
                	<tr>
                		<?php 
						// 학습자원/활동 표시 영역
						foreach ($sections as $cs) {
						    // 주차를 따로 선택한 경우
						    if ($section >= 0 && $cs->section != $section) {
						        continue;
						    }
						    
							if (empty($cs->cmids)) {
								echo '<th class="nofixed text-truncate max-wp-50 min-wp-50 borderline">&nbsp;</th>';
							} else {
							    $tmpI = 0;
								foreach ($cs->cmids as $cmid) {
								    $borderline = ($tmpI == 0) ? 'borderline' : '';
								    
								    $cminfo = $modinfo->get_cm($cmid);
								    
								    $title = $cminfo->name;
								    if ($cminfo->completionexpected) {
								        $title .= '<br/>';
								        $title .= Common::getUserDateShort($cminfo->completionexpected);
								    }
								    
								    echo '<th class="nofixed text-truncate max-wp-50 min-wp-50 text-center '.$borderline.'">';
								    echo	'<img src="'.$cminfo->get_icon_url().'" alt="'.$cminfo->name.'" title="'.$title.'" data-toggle="tooltip" data-container="body" data-html="true" />';
									echo '</th>';
									$tmpI++;
								}
							}
						}
						?>
                	</tr>
                </thead>
                <tbody>
                	<?php
                	$page = empty($page) ? 1 : $page;
                	$number = (($page-1) * $ls) + 1;
                	
                	foreach ($users as $u) {
                	    $fullname = fullname($u);
                	    $idnumber = (!empty($u->idnumber)) ? $u->idnumber : '&nbsp;';
                	    
                	    // 사용자 이수현황정보
                	    $userCompletions = $CCompletion->getUserCompletions($u->id);
                	    
                	    echo '<tr>';
                	    echo     '<td class="text-center td-title-70" data-title="'.$i8n->number.'">'.$number.'</td>';
                	    echo     '<td class="text-center td-title-70" data-title="'.$i8n->idnumber.'">'.$idnumber.'</td>';
                	    echo     '<td class="text-center td-title-70" data-title="'.$i8n->fullnameuser.'">'.$fullname.'</td>';
                	    echo     '<td class="borderline td-title-70" data-title="'.$i8n->department.'">';
                	    echo         $CUser->getDepartment($u, true);
                	    echo     '</td>';
                	    
                	    if ($courseGroup->is) {
                	        $groupName = $CGroup->getUserGroupBadge($course->id, $u->id, null, true);
                	        echo '<td class="text-center td-title-70" data-title="'.$i8n->group.'">'.$groupName.'</td>';
                	    }
                	    
                	    foreach ($sections as $cs) {
                	        // 주차를 따로 선택한 경우
                	        if ($section >= 0 && $cs->section != $section) {
                	            continue;
                	        }
                	        
                	        $cmidsCount = count($cs->cmids);
                	        
                	        $sectionShortName = $CCourse->getSectionNameShort($course, $cs->section);
                	        if ($cmidsCount > 0) {
                	            $tmpI = 0;
                	            foreach($cs->cmids as $cmid) {
                	                $borderline = ($tmpI == 0) ? 'borderline' : '';
                	                
                	                $cminfo = $modinfo->get_cm($cmid);
                	                
                	                $state = COMPLETION_INCOMPLETE;
                	                $overrideby = 0;
                	                $date = '';
                	                
                	                if (isset($userCompletions[$cmid])) {
                	                    $state = $userCompletions[$cmid]->completionstate;
                	                    $overrideby = $userCompletions[$cmid]->overrideby;
                	                    $date = $userCompletions[$cmid]->timemodified;
                	                }
                	                
                	                $icon = $CCompletion->getStateIcon($cminfo, $u, $state, $overrideby, $date);
                	                
                	                $title = $sectionShortName.' - '.$cminfo->name;
                	                echo '<td class="text-center '.$borderline.' td-title-70" data-title="'.$title.'">'.$icon.'</td>';
                	                $tmpI++;
                	            }
                	        } else {
                	            $title = $sectionShortName;
                	            echo '<td class="borderline td-title-70" data-title="'.$title.'">&nbsp;</td>';
                	        }
                	    }
                	    
                	    
                	    echo '</tr>';
                	    
                	    $number++;
                	}
                	?>
                </tbody>
			</table>
		</div>
		<?= Paging::printHTML($totalCount, $page, $ls); ?>
    </div>
    <?php 
	} else {
	    echo '<div class="well">'.get_string('not_found_users', 'local_ubion').'</div>';
	}
    ?>
</div>
<?php 

// 이벤트 실행
$CCompletion->setEvent();
echo $OUTPUT->footer();