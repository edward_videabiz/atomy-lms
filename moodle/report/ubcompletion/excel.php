<?php
use local_ubion\base\Common;
use local_ubion\base\Parameter;

require('../../config.php');

header("Set-Cookie: fileDownload=true; path=/");

require_once $CFG->dirroot.'/course/lib.php';
require_once $CFG->libdir.'/excellib.class.php';

$id = required_param('id', PARAM_INT);
$keyfield	= optional_param('keyfield', null, PARAM_ALPHANUMEXT);
$keyword	= Parameter::getKeyword();
$groupid    = optional_param('groupid', null, PARAM_INT);
$order		= optional_param(Parameter::getSortBy(), null, PARAM_ALPHA);
$orderType = optional_param(Parameter::getSortOrder(), 'ASC', PARAM_ALPHA);
$section = optional_param('section', - 1, PARAM_INT);
$page		= optional_param('page', 1, PARAM_INT);
$ls			= optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);

$pluginname = 'report_ubcompletion';

$CCourse = \local_ubion\course\Course::getInstance();
$CUser = \local_ubion\user\User::getInstance();
$CGroup = \local_ubion\group\Group::getInstance();
$CCompletion = new \report_ubcompletion\Completion($id);

$course = $CCourse->getCourse($id);
$context = context_course::instance($course->id);

$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);
require_capability('report/ubcompletion:view', $context);

$PAGE->set_url('/report/ubcompletion/index.php', array(
    'id' => $id
));


$i8n = new stdClass();
$i8n->pluginname = get_string('pluginname', $pluginname);
$i8n->number = get_string('number', 'local_ubion');
$i8n->grade = get_string('grade', 'local_ubion');
$i8n->fullnameuser = get_string('fullnameuser');

$i8n->group = get_string('groups', 'group');
$i8n->department = get_string('department');
$i8n->idnumber = get_string('idnumber');

// 강좌에서 그룹을 분류해서 사용하는지 확인
$courseGroup = $CGroup->getCourseGroups($course->id);


$modinfo = get_fast_modinfo($course);
$sections = $CCompletion->getSections($modinfo);

// 등록된 학습자원/활동이 존재하는지 확인 해야됨.
$isCompletionModule = false;
foreach ($sections as $cs) {
    
    $cmidsCount = count($cs->cmids);
    
    if ($cmidsCount > 0) {
        $isCompletionModule = true;
        break;
    }
}


// 이수기능을 사용하는 학습자원/활동이 한개라도 존재하는 경우
if ($isCompletionModule) {
    
    $users = $CCompletion->getCourseUserAll($course, array(), $groupid, $order, $orderType);
    $totalCount = count($users);
    
    if ($totalCount > 0) {
    
        // Excel download 부분 시작...
        // excel layout 잡기.
        $workbook = new MoodleExcelWorkbook('-');
        $filename = $CCourse->getName($course).'_'.get_string('pluginname', $pluginname).'.xls';
        $workbook->send($filename);
        
        $format = new stdClass();
        $format->title = $workbook->add_format(array('bold'=>1, 'v_align'=>'vcenter', 'size'=>12));
        $format->head = $workbook->add_format(array('align'=>'center', 'bold'=>1, 'v_align'=>'vcenter', 'border'=>1, 'bg_color'=>'#54728C', 'color'=>'white', 'text_wrap'=>true));
        $format->head2 = $workbook->add_format(array('align'=>'center', 'bold'=>1, 'v_align'=>'vcenter', 'border'=>1, 'bg_color'=>'#F5F5F5', 'color'=>'black', 'text_wrap'=>true));
        $format->body = $workbook->add_format(array('align'=>'center', 'border'=>1, 'v_align'=>'vcenter', 'text_wrap'=>true));
        $format->bodyComplete = $workbook->add_format(array('align'=>'center', 'border'=>1, 'v_align'=>'vcenter', 'text_wrap'=>true, 'color'=>'blue'));
        
        $worksheet = array();
        $sheetnum = 0;
        $rownum = 0;
        $worksheet[$sheetnum] = $workbook->add_worksheet('');
        
        $cellIndex = 0;
        
        
        // 번호, 학번, 이름, 부서, 그룹은 2개 row가 병합되어야 함. 
        $rowspannum = $rownum + 1;
        $sectionStartCellIndex = 4;       // section이 시작되는 index
        $worksheet[$sheetnum]->set_row($rownum, 30);
        
        
        // 번호
        $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $i8n->number, $format->head);
        $worksheet[$sheetnum]->write_string($rowspannum, $cellIndex, $i8n->number, $format->head);
        $worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $rowspannum, $cellIndex);
        $worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 5);
        $cellIndex++;
        
        // 학번
        $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $i8n->idnumber, $format->head);
        $worksheet[$sheetnum]->write_string($rowspannum, $cellIndex, $i8n->idnumber, $format->head);
        $worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $rowspannum, $cellIndex);
        $worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 15);
        $cellIndex++;
        
        // 이름
        $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $i8n->fullnameuser, $format->head);
        $worksheet[$sheetnum]->write_string($rowspannum, $cellIndex, $i8n->fullnameuser, $format->head);
        $worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $rowspannum, $cellIndex);
        $worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 15);
        $cellIndex++;
        
        // 부서
        $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $i8n->department, $format->head);
        $worksheet[$sheetnum]->write_string($rowspannum, $cellIndex, $i8n->department, $format->head);
        $worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $rowspannum, $cellIndex);
        $worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 25);
        $cellIndex++;
        
        // 그룹이 존재하는 경우
        if ($courseGroup->is) {
            $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $i8n->group, $format->head);
            $worksheet[$sheetnum]->write_string($rowspannum, $cellIndex, $i8n->group, $format->head);
            $worksheet[$sheetnum]->merge_cells($rownum, $cellIndex, $rowspannum, $cellIndex);
            $worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 20);
            $cellIndex++;
            
            $sectionStartCellIndex = 5;
        }
        
        // section 표시
        foreach ($sections as $cs) {
            
            // 주차를 따로 선택한 경우
            if ($section >= 0 && $cs->section != $section) {
                continue;
            }
            
            $mergeStart = $cellIndex;
            $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $CCourse->getSectionName($course, $cs->section), $format->head);
            $cellIndex++;
            
            // 등록된 학습활동/자원 갯수
            $cmidsCount = count($cs->cmids);
            for ($i = 1; $i < $cmidsCount; $i++) {
                $worksheet[$sheetnum]->write_string($rownum, $cellIndex, '', $format->head);
                $cellIndex++;
            }
            $mergeEnd = $cellIndex - 1;
            $worksheet[$sheetnum]->merge_cells($rownum, $mergeStart, $rownum, $mergeEnd);
        }
        
        
        // 학습자원/활동
        $rownum++;
        $cellIndex = $sectionStartCellIndex;
        
        foreach ($sections as $cs) {
            // 주차를 따로 선택한 경우
            if ($section >= 0 && $cs->section != $section) {
                continue;
            }
            
            if (empty($cs->cmids)) {
                $worksheet[$sheetnum]->write_string($rownum, $cellIndex, '', $format->head2);
                $worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 8);
                $cellIndex++;
            } else {
                foreach ($cs->cmids as $cmid) {
                    $cminfo = $modinfo->get_cm($cmid);
                    
                    $title = $cminfo->name;
                    if ($cminfo->completionexpected) {
                        $title .= "\n";
                        $title .= Common::getUserDateShort($cminfo->completionexpected);
                    }
                    
                    $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $title, $format->head2);
                    $worksheet[$sheetnum]->set_column($cellIndex, $cellIndex, 8);
                    $cellIndex++;
                }
            }
            
        }
        
        
        // 사용자 데이터 출력
        $rownum++;
        
        
        $number = 1;
        foreach ($users as $u) {
            $cellIndex = 0;
            
            $fullname = fullname($u);
            $idnumber = (!empty($u->idnumber)) ? $u->idnumber : '';
            
            // 사용자 이수현황정보
            $userCompletions = $CCompletion->getUserCompletions($u->id);
            
            $worksheet[$sheetnum]->set_row($rownum, 25);
            
            // 번호
            $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $number, $format->body);
            $cellIndex++;
            
            // 학번
            $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $idnumber, $format->body);
            $cellIndex++;
            
            // 성명
            $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $fullname, $format->body);
            $cellIndex++;
            
            // 부서
            $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $CUser->getDepartment($u), $format->body);
            $cellIndex++;
            
            if ($courseGroup->is) {
                $groupName = '';
                if ($user_group = $CGroup->getUserGroups($course->id, $u->id)) {
                    $comma = '';
                    foreach ($user_group as $ug) {
                        if (!empty($ug->name)) {
                            $groupName .= $comma.$ug->name;
                            $comma = "\n";
                        }
                    }
                }
                
                $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $groupName, $format->body);
                $cellIndex++;
            }
            
            foreach ($sections as $cs) {
                // 주차를 따로 선택한 경우
                if ($section >= 0 && $cs->section != $section) {
                    continue;
                }
                
                $cmidsCount = count($cs->cmids);
                
                if ($cmidsCount > 0) {
                    foreach($cs->cmids as $cmid) {
                        $cminfo = $modinfo->get_cm($cmid);
                        
                        $state = COMPLETION_INCOMPLETE;
                        $overrideby = 0;
                        $date = '';
                        
                        if (isset($userCompletions[$cmid])) {
                            $state = $userCompletions[$cmid]->completionstate;
                            $overrideby = $userCompletions[$cmid]->overrideby;
                            $date = $userCompletions[$cmid]->timemodified;
                        }
                        
                        $text = $CCompletion->getStateText($state, $overrideby);
                        
                        $formatClass = $format->body;
                        if ($state == COMPLETION_COMPLETE) {
                            $formatClass = $format->bodyComplete;
                        }
                        
                        
                        $worksheet[$sheetnum]->write_string($rownum, $cellIndex, $text, $formatClass);
                        $cellIndex++;
                    }
                } else {
                    $worksheet[$sheetnum]->write_string($rownum, $cellIndex, '', $format->body);
                    $cellIndex++;
                }
            }
            
            $rownum++;
            $number++;
        }
        
        
        $workbook->close();
        die;
    } else {
        \local_ubion\base\Javascript::printAlert(get_string('not_found_users', 'local_ubion'));
    }
} else {
    \local_ubion\base\Javascript::printAlert(get_string('no_modules', $pluginname));
}