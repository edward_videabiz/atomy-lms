<?php
$string['eventreportviewed'] = '강좌 이수현황 보기';
$string['no_modules'] = '이수 기능을 사용하는 학습자원/활동이 없습니다.';
$string['pluginname'] = '강좌 이수현황';
$string['progress-title'] = '{$a->state} : {$a->date}';
$string['ubcompletion:view'] = 'View completion reports';