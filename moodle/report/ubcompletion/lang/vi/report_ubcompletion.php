﻿<?php
$string['eventreportviewed'] = 'Xem trạng thái hoàn thành khóa học';
$string['no_modules'] = 'Không có hoạt động/tài nguyên nào sử dụng theo dõi hoàn thành.';
$string['pluginname'] = 'Trạng thái hoàn thành khóa học';
$string['progress-title'] = '{$a->state} : {$a->date}';
$string['ubcompletion:view'] = 'Xem các báo cáo hoàn thành';