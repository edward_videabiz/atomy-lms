﻿<?php
$string['eventreportviewed'] = 'View course completion status';
$string['no_modules'] = 'There is no learning activity/resource using completing tracking.';
$string['pluginname'] = 'Course completion status';
$string['progress-title'] = '{$a->state} : {$a->date}';
$string['ubcompletion:view'] = 'View completion reports';