<?php
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2018080601;     // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2017111301;     // Requires this Moodle version
$plugin->component = 'report_ubcompletion'; // Full name of the plugin (used for diagnostics)
$plugin->dependencies = array('local_ubion' => 2017010100);