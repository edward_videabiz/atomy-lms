<?php
namespace report_ubcompletion\core;

use local_ubion\controller\Controller;
use local_ubion\base\Common;
use local_ubion\course\Course;
use local_ubion\core\traits\TraitCourseUser;

class Completion extends Controller 
{
    use TraitCourseUser;
    
    protected $course = null;
    protected $courseContext = null;
    protected $CCompletionInfo = null;
    protected $pluginname = 'report_ubcompletion';
    protected $overridecompletion = false;
    
    public function __construct($id)
    {
        $this->course = Course::getInstance()->getCourse($id);
        $this->courseContext = \context_course::instance($id);
        $this->CCompletionInfo = new \completion_info($this->course);
        $this->overridecompletion = has_capability('moodle/course:overridecompletion', $this->courseContext);
    }
    
    
    /**
     * 강좌 주차내에 이수기능을 사용하는 학습자원/활동 리턴
     * 
     * @param \course_modinfo $modinfo
     * @return \stdClass[]
     */
    public function getSections($modinfo=null)
    {
        if (empty($modinfo)) {
            $modinfo = get_fast_modinfo($this->course);
        }
        
        $CCourse = Course::getInstance();
        
        $numsections = $CCourse->getNumSections($this->course);
        $sections = array();
        
        foreach ($modinfo->get_section_info_all() as $section) {
            // 주차 보기 권한이 존재하며, 강좌내에 설정된 주차보다 같거나 작은 경우에만 표시되어야 함.
            if ($section->visible && $section->section <= $numsections) {
                
                $sectionInfo = new \stdClass();
                $sectionInfo->id = $section->id;
                $sectionInfo->section = $section->section;
                $sectionInfo->name = $CCourse->getSectionName($this->course, $section->section);
                $sectionInfo->cmids = array();
                
                // 학습자원/활동을 반복 돌면서 이수 항목, 완료된 갯수 파악.
                $cmids = $modinfo->sections[$section->section] ?? null;
                
                if (! empty($cmids)) {
                    foreach ($cmids as $cmid) {
                        $cm = $modinfo->cms[$cmid];
                        
                        // label은 이수여부가 없음.
                        if ($cm->modname == 'label') {
                            // Labels are special (not interesting for students)!
                            continue;
                        }
                        
                        // 해당 모듈을 볼 권한이 있는 경우
                        if ($cm->visible) {
                            if ($this->CCompletionInfo->is_enabled($cm) != COMPLETION_TRACKING_NONE) {
                                $sectionInfo->cmids[$cmid] = $cmid;
                            }
                        }
                    }
                }
                
                $sections[$section->section] = $sectionInfo;
            }
        }
        
        
        return $sections;
    }
    
    /**
     * 모듈별 사용자 이수현황 리턴
     * 
     * @param int $userid
     * @return array
     */
    public function getUserCompletions($userid)
    {
        global $DB;
        
        $query = "SELECT
                				cmc.coursemoduleid
                                ,cmc.completionstate
                                ,cmc.viewed
                                ,cmc.overrideby
                                ,cmc.timemodified
                  FROM			{course_modules} cm
                  LEFT JOIN	    {course_modules_completion} cmc ON cm.id = cmc.coursemoduleid
                  WHERE			cm.course = :courseid
                  AND			cmc.userid = :userid";
        
        $param = array('courseid' => $this->course->id, 'userid' => $userid);
        
        return $DB->get_records_sql($query, $param);
    }
    
    
    /**
     * 모듈의 이수상태를 아이콘으로 리턴시켜줌
     * 
     * @param \cm_info $cminfo
     * @param \stdClass $user
     * @param int $state
     * @param int $overrideby
     * @param int $date
     * @return string
     */
    public function getStateIcon(\cm_info $cminfo, $user, $state, $overrideby, $date)
    {
        global $OUTPUT, $PAGE;
        
        
        // 해당 로직은
        // report/progress/index.php를 참고해서 거의 그대로 코드 사용함.
        
        if ($date > 0) {
            $date = Common::getUserDate($date);
        }
        
        // Work out how it corresponds to an icon
        switch($state) {
            case COMPLETION_INCOMPLETE :
                $completiontype = 'n'.($overrideby ? '-override' : '');
                break;
            case COMPLETION_COMPLETE :
                $completiontype = 'y'.($overrideby ? '-override' : '');
                break;
            case COMPLETION_COMPLETE_PASS :
                $completiontype = 'pass';
                break;
            case COMPLETION_COMPLETE_FAIL :
                $completiontype = 'fail';
                break;
        }
        $completiontrackingstring = $cminfo->completion == COMPLETION_TRACKING_AUTOMATIC ? 'auto' : 'manual';
        $completionicon = 'completion-' . $completiontrackingstring. '-' . $completiontype;
        
        
        if ($overrideby) {
            $overridebyuser = \core_user::get_user($overrideby, '*', MUST_EXIST);
            $describe = get_string('completion-' . $completiontype, 'completion', fullname($overridebyuser));
        } else {
            $describe = get_string('completion-' . $completiontype, 'completion');
        }
        
        $a = new \stdClass();
        $a->state = $describe;
        $a->date = $date;
        $a->user = fullname($user);
        $a->activity = $cminfo->name;
        
        $fulldescribe = get_string('progress-title', $this->pluginname, $a);
        
        // bootstrap-tooltip 을 활용할수 있도록 아이콘 출력 부분 수정함.
        $icon = $OUTPUT->image_url('i/'.$completionicon);
        
        
        $imgTag = '<img src="'.$icon.'" title="'.$fulldescribe.'" data-toggle="tooltip" data-container="body" data-html="true" />';
        
        if ($this->overridecompletion && $state != COMPLETION_COMPLETE_PASS && $state != COMPLETION_COMPLETE_FAIL) {
            
            $newstate = ($state == COMPLETION_COMPLETE) ? COMPLETION_INCOMPLETE : COMPLETION_COMPLETE;
            
            $changecompl = $user->id . '-' . $cminfo->id . '-' . $newstate;
            
            $url = new \moodle_url($PAGE->url, ['sesskey' => sesskey()]);
            $tag = \html_writer::link($url, $imgTag, array('class' => 'changecompl', 'data-changecompl' => $changecompl,
                'data-activityname' => $a->activity,
                'data-userfullname' => $a->user,
                'data-completiontracking' => $completiontrackingstring,
                'aria-role' => 'button'));
        } else {
            $tag = $imgTag;
        }
        
        return $tag;
    }
    
    /**
     * 모듈의 이수상태값을 텍스트 형태로 리턴
     * 
     * @param int $state
     * @param int $overrideby
     * @return string
     */
    public function getStateText($state, $overrideby)
    {
        // Work out how it corresponds to an icon
        switch($state) {
            case COMPLETION_INCOMPLETE :
                $completiontype = 'n'.($overrideby ? '-override' : '');
                break;
            case COMPLETION_COMPLETE :
                $completiontype = 'y'.($overrideby ? '-override' : '');
                break;
            case COMPLETION_COMPLETE_PASS :
                $completiontype = 'pass';
                break;
            case COMPLETION_COMPLETE_FAIL :
                $completiontype = 'fail';
                break;
        }
        
        
        if ($overrideby) {
            $overridebyuser = \core_user::get_user($overrideby, '*', MUST_EXIST);
            $describe = get_string('completion-' . $completiontype, 'completion', fullname($overridebyuser));
        } else {
            $describe = get_string('completion-' . $completiontype, 'completion');
        }
        
        return $describe;
    }
    
    
    public function setEvent()
    {
        
        // Trigger a report viewed event.
        $event = \report_ubcompletion\event\report_viewed::create(array('context' => $this->courseContext));
        $event->trigger();
    }
}