define(['jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate', 'theme_coursemos/jquery.filedownload', 'theme_coursemos/jquery.tableHeadFixer'], function($) {
    var completion = {};
    
    completion.pluginname = 'report_ubcompletion';
    
    // 메인 페이지
    completion.index = function() {
       // 한페이지에 보여줄 사용자 수...
        $(".report-ubcompletion .form-search .form-control-listsize").change(function() {
            location_search('ls', $(this).val());
        });
        
        // 그룹
        $(".report-ubcompletion .form-search .form-control-group").change(function() {
            location_search('groupid', $(this).val());
        });
        
        // 주차
        $(".report-ubcompletion .form-search .form-control-section").change(function() {
            location_search('section', $(this).val());
        });
        
        
        $(".report-ubcompletion .table-completion").tableHeadFixer({"head" : false, "left" : 3});
        
        
        $(".report-ubcompletion .btn-excel").click(function() {
            $.fileDownload($(this).prop('href'), {
                prepareCallback : function(url) {
                    showSubmitProgress();
                    $("#ajax_loading_message .save_msg").text(M.util.get_string('excel_message', 'local_ubion'));
                }, 
                successCallback : function(url) {
                    hideSubmitProgress();
                },
            });
            
            return false;
        });
        
    };
    
    return completion;
});