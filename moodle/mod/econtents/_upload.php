<?php
use \local_ubion\base\Common;
use \local_ubion\base\Parameter;

require_once '../../config.php';

$id = optional_param('id', 0, PARAM_INT);
$keyfield	= optional_param('keyfield', null, PARAM_ALPHANUMEXT);
$keyword	= Parameter::getKeyword();
$page		= optional_param('page', 1, PARAM_INT);
$ls			= optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);

$pluginname = 'econtents';

$PAGE->set_url('/mod/econtetns/_upload.php', array('page'=>$page, 'keyfield'=>$keyfield, 'keyword'=>$keyword));
$baseurl = $PAGE->url;


if (!isloggedin()) {
    Common::printError(get_string('invalidlogin'));
}

// 이곳은 개인화 된 페이지 임.
$PAGE->set_context(context_user::instance($USER->id));

$CUser = \local_ubion\user\User::getInstance();
$CEContents = new \mod_econtents\EContents();
$config = $CEContents->getConfig();

$prevLink = $CFG->wwwroot.'/mod/econtents/_list.php?'.$baseurl->get_query_string();

if (!empty($id)) {
    if ($content = $CEContents->getContentView($id)) {
        // 관지라 또는 업로드한 사용자가 아니라면 수정 불가
        if (!is_siteadmin() && $content->userid != $USER->id) {
         ?>
        <script type="text/javascript">
        	require(["mod_econtents/management"], function(management) {
        		alert('<?= get_string('notedit', $pluginname); ?>');
        		management.list();
        	});
        </script>
        <?php
        }
    }
}

// 새 콘텐츠 등록할 때 디폴트 값 설정
if (empty($content)) {
    $content = new stdClass();
    $content->id = '';
    $content->contenttype = "";
    $content->contentname = "";
    $content->contenturl = "";
    $content->description = "";
    $content->filename = '';
    $content->filesize = 0;
    $content->ispublic = $config->ispublic;
    $content->playtime = $config->default_playtime;
    $content->popupwidth = $config->popupwidth;
    $content->popupheight = $config->popupheight;
    $content->isestream = 0;

}


$i8n = new stdClass();
$i8n->name = get_string('contentname', $pluginname);
$i8n->description = get_string('contentdescription', $pluginname);
$i8n->ispublic = get_string('ispublic', $pluginname);
$i8n->fileupload = get_string('fileupload', $pluginname);
$i8n->filepath_index = get_string('filepath_index', $pluginname);
$i8n->learningtime = get_string('learningtime', $pluginname);
?>

<script type="text/javascript">
    require(["mod_econtents/management"], function(management) {

        $("#add_econtents_form").validate({
            submitHandler : function(form) {

                showSubmitProgress();

                $.ajax({
					 url 	: management.actionurl
					,data	: $("#add_econtents_form").serializeFiles()
					,processData: false
			        ,contentType: false
		            ,enctype: 'multipart/form-data'
					,success: function(data){
						if(data.code == mesg.success) {
						    hideSubmitProgress();

							// 업로드가 완료되면 메인 화면으로 이동하기
						    management.list();
						} else {
							alert(data.msg);
   						return false;
						}
					}
				});
				return false;
            }
        });


        $('#uploadfile').change(function(){
	        var filename = $('#uploadfile').val();
	        var upfile = $('#uploadfile')[0].files[0];//.name

	        if(!upfile.type.match('zip') && !upfile.type.match('html')){
		    	alert('<?= get_string('onlyhtmlzip', $pluginname)?>');

		    	$("#uploadfile").val("");
		    	$("#uploadfile").focus();
	        }else{
		        var orignalFilename = $("#contentname").val();

		        // 제목이 따로 입력되지 않았으면 파일명 그대로 표시
		        if (orignalFilename == "") {
	        		$("#contentname").val(getFileName(upfile.name));
		        }


		        $("input[name='filename']").val(upfile.name);
	        	$("input[name='filesize']").val(upfile.size);

		        if(upfile.type.match('html')){
			    	$("input[name='contenturl']").val(upfile.name);
		        }
	        }
        });

        $("#add_econtents_form .btn-prev").click(function() {
            management.modalMove('<?= $prevLink; ?>');
			return false;
		});


        setRequiredIcon();
    });
</script>

<form method="post" enctype="multipart/form-data" class="form-horizontal form-validate" id="add_econtents_form">
	<div class="form-group">
		<label class="control-label col-sm-3" for="uploadfile"><?= $i8n->fileupload; ?></label>
		<div class="col-sm-9">
			<input type="file" name="uploadfile" id="uploadfile" class="form-control" accept=".zip,.html,.htm" />
			<?php
			if (!empty($content->filename)) {
				$filetext = $content->filename;
				$filetext .=  ($content->filesize)?	' ('.Common::getFileSize($content->filesize).')' :'';
				echo '<div class="form-text mt-2">';
				echo    '<span class="text-danger font-weight-bold">'.get_string('upload_file', $pluginname, $filetext).'</span>';
				echo '</div>';
			}
			?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3"><?= $i8n->name; ?></label>
		<div class="col-sm-9">
            <input type="hidden" name="id" value="<?= $content->id; ?>"/>
            <input type="hidden" name="<?= $CEContents::TYPENAME; ?>" value="upload"/>
            <input type="hidden" name="sesskey" value="<?= sesskey() ?>"/>
			<input type="hidden" name="filename" value="<?= $content->filename; ?>" />
			<input type="hidden" name="filesize" value="<?= $content->filesize; ?>" />

  			<input type="text" id="contentname" name="contentname" class="required form-control" maxlength="100" placeholder="<?= $i8n->name; ?>" value="<?= $content->contentname; ?>"/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3"><?= $i8n->description; ?></label>
		<div class="col-sm-9">
			<textarea id="description" name="description" class="form-control" placeholder="<?= $i8n->description; ?>"><?= $content->description; ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3"><?= $i8n->ispublic; ?></label>
		<div class="col-sm-9">
			<?php
			$checked = array('0' => '', '1' => '');
			if (isset($checked[$content->ispublic])) {
			    $checked[$content->ispublic] = ' checked="checked"';
			}
			?>
			<label class="radio-inline">
				<input type="radio" name="ispublic" value="1" <?= $checked[1]; ?> />
				<?= get_string('public', $pluginname); ?>
			</label>
			<label class="radio-inline">
				<input type="radio" name="ispublic" value="0" <?= $checked[0]; ?> />
				<?= get_string('notpublic', $pluginname); ?>
			</label>
			<div class="form-text mt-2">
				<span class="text-info"><?= get_string('ispublic_desc', $pluginname); ?></span>
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-3" for="contenturl"><?= $i8n->filepath_index; ?></label>
		<div class="col-sm-9">
			<input type="text" id="contenturl" name="contenturl" class="required form-control" maxlength="255" placeholder="<?= $i8n->filepath_index; ?>" value="<?= $content->contenturl; ?>"/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="learningtime"><?= $i8n->learningtime; ?></label>
		<div class="col-sm-9">
			<input type="text" id="learningtime" name="learningtime" class="form-control form-control-auto required number" maxlength="100" placeholder="<?= $i8n->learningtime; ?>" size="5" value="<?= $content->playtime; ?>"/>
			<span class="form-text"><?= get_string('learningtimedescription', $pluginname); ?></span>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3"><?= get_string('popupwindowsize', $pluginname); ?></label>
		<div class="col-sm-9">
			<div class="form-inline">
    			<input type="text" name="popupwidth" class="form-control number required" maxlength="5" placeholder="<?= get_string('popupwidth', $pluginname); ?>" size="5" value="<?= $content->popupwidth; ?>"/> *
    			<input type="text" name="popupheight" class="form-control number required" maxlength="5" placeholder="<?= get_string('popupheight', $pluginname); ?>" size="5" value="<?= $content->popupheight; ?>"/>
			</div>
		</div>
	</div>
	<?php
	/* 사용되지 않는 기능인거 같아서 주석처리함.
	<div class="form-group">
		<label class="control-label col-sm-3"><?= get_string('thumbnail', $pluginname); ?></label>
		<div class="col-sm-9">
			<input type="file" name="thumb" class="form-control" accept="image/*" />

			<?php
			if (is_file($CFG->dataroot.'/'.$CEContents::getSaveFolderName().'/'.$id.'_thumb')) {
			    $thumbUrl = $CFG->wwwroot.'/mod/econtents/img.php?id='.base64_encode($id);
			    echo '<div class="form-text">';
			    echo     '<img src="'.$thumbUrl.'" alt="" width="120" height="90" />';
			    echo '</div>';
			}
			?>
		</div>
	</div>
	*/
	?>
	<div class="form-group">
		<label class="control-label col-sm-3"><?= get_string('estream', $pluginname); ?></label>
		<div class="col-sm-9">
			<?php
			$checked = array('0' => '', '1' => '');
			if (isset($checked[$content->isestream])) {
			    $checked[$content->isestream] = ' checked="checked"';
			}

			if (empty($content->isestream)) {
			    $checked[0] = ' checked="checked"';
			}
			?>
			<label class="radio-inline">
				<input type="radio" name="isestream" value="1" <?= $checked[1]; ?> />
				<?= get_string('yes'); ?>
			</label>
			<label class="radio-inline">
				<input type="radio" name="isestream" value="0" <?= $checked[0]; ?> />
				<?= get_string('no'); ?>
			</label>

			<div class="form-text">
				<span class="text-info"><?= get_string('estream_desc', $pluginname); ?></span>
			</div>
		</div>
	</div>

	<div class="form-group">
    	<div class="col-sm-9 col-sm-offset-3">
    		<button type="submit" class="btn btn-primary"><?= get_string('save', 'local_ubion'); ?></button>
    		<button type="button" class="btn btn-default btn-prev"><?= get_string('prev', $pluginname); ?></button>
    	</div>
	</div>
</form>