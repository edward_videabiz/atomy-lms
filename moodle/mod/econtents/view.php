<?php

use \local_ubion\base\Common;

require_once '../../config.php';

require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/mod/resource/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

$pluginname = 'econtents';

$CCourse = \local_ubion\course\Course::getInstance();
$CEContents = new \mod_econtents\EContents();

$id = optional_param('id', 0, PARAM_INT);  // Course module ID

if (!$cm = get_coursemodule_from_id('econtents', $id)) {
    print_error('invalidcoursemodule');
}


$econtents = $DB->get_record_sql('SELECT * FROM {econtents} WHERE id = :id', array('id' => $cm->instance));

if (empty($econtents)) {
    Common::printNotice(get_string('error_not_exists', $pluginname), $CFG->wwwroot.'/course/view.php?id='.$econtents->course);
}

$course = $CCourse->getCourse($cm->course);
$context = context_module::instance($cm->id);

require_course_login($course, true, $cm);

$PAGE->set_context($context);
$PAGE->set_url('/mod/econtents/view.php', array('id' => $cm->id));
$PAGE->set_title($course->shortname . ' : ' . format_string($econtents->name));
$PAGE->set_heading($course->fullname);

$isProfessor = $CCourse->isProfessor($course->id);

// 열람제한인 경우 교수자가 아니면 error화면이 표시되어야 함.
if (empty($econtents->restricted)) {
    if (! $isProfessor) {
        Common::printNotice(get_string('restrict_message', $pluginname), $CFG->wwwroot.'/course/view.php?id='.$course->id);
    }
}

// 이벤트 실행
$CEContents->setViewEvent($course, $cm, $context);


echo $OUTPUT->header();

if($introMessage = format_module_intro('econtents', $econtents, $cm->id)) {
    echo $OUTPUT->box($introMessage, 'generalbox');
}


$fullurl = $CFG->wwwroot."/mod/econtents/viewer.php?id=".$cm->id;
$wh = \mod_econtents\econtents::getPopupOption($econtents->popupwidth, $econtents->popupheight);
$popupName = \mod_econtents\econtents::getPopupName();


$onclick = "window.open('".$fullurl."', '".$popupName."', '".$wh."'); return false;";

echo '<div class="buttons">';
echo    '<a href="'.$fullurl.'" class="btn btn-default" onclick="'.$onclick.'">'.get_string('viewcontent', $pluginname).'</a>';
echo '</div>';


echo $OUTPUT->footer();