<?php
defined('MOODLE_INTERNAL') || die;

/**
 * List of features supported in URL module
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, false if not, null if doesn't know
 */
function econtents_supports($feature) {
    switch($feature) {
        case FEATURE_GROUPS:                  	return false;
        case FEATURE_GROUPINGS:               	return false;
        case FEATURE_GROUPMEMBERSONLY:        	return true;
        case FEATURE_MOD_INTRO:               	return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS: 	return true;
		case FEATURE_COMPLETION_HAS_RULES: 		return true;
        case FEATURE_GRADE_HAS_GRADE:         	return true;
        case FEATURE_GRADE_OUTCOMES:          	return false;
        case FEATURE_BACKUP_MOODLE2:          	return true;
        case FEATURE_MOD_ARCHETYPE:           	return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_MODEDIT_DEFAULT_COMPLETION: return false;
        case FEATURE_SHOW_DESCRIPTION:        	return true;

        default: return null;
    }
}


/**
 * Add econtents instance.
 * @param object $data
 * @param object $mform
 * @return int new econtents instance id
 */
function econtents_add_instance($data, $mform) {
	global $DB;

	// $cmid = $data->coursemodule;
	$data->timemodified = time();
	
	
	// econtentscontents 값을 가지고 playtime값을 가져와서 저장해야됨.
	$contents = $DB->get_record_sql('SELECT id, filesize, playtime FROM {econtents_contents} WHERE id = :id', array('id'=>$data->econtentscontents));
	$data->playtime = $contents->playtime * MINSECS;    // econtents는 분단위로 입력하기 때문에 초단위로 변환이 필요함.
	$data->filesize = $contents->filesize;
	

	// 온라인 출석부 관련 로직
	$COnAttendance = \local_ubonattend\Attendance::getInstance($data->course);
	if ($COnAttendance->isAttendanceCourse()) {
	    
	    // 진도처리를 사용하지 않는 영상이면 온라인 출석체크 여부도 아니오로 설정되어야 함.
	    if (empty($data->progress)) {
	        $data->useattend = 0;
	    }
	    
	    // 해당 주차에 설정된 진도처리 일자 가져오기
	    $modinfo = get_fast_modinfo($data->course);
	    if ($sectionInfo = $modinfo->get_section_info($data->section)) {
	        
	        if ($onlineAttendanceSection = $COnAttendance->getSection($sectionInfo->id)) {
	            $data->timeopen = $onlineAttendanceSection->time_start;
	            
	            // 지각 기능 사용여부에 따라서 종료일이 달라짐
	            if ($COnAttendance->isLateCourse()) {
	                $data->timeclose = $onlineAttendanceSection->time_lateness;
	            } else {
	                $data->timeclose = $onlineAttendanceSection->time_end;
	            }
	            
	            $data->completionprogress = $onlineAttendanceSection->attendance_percent;
	            $data->tardinessprogress = $onlineAttendanceSection->lateness_percent;
	        }
	    }
	}
	
	
	if (!$data->id = $DB->insert_record('econtents', $data)) {
	    return false;
	}

	// 성적항목 추가 여부
	$configAddGradeItem = get_config('econtents', 'grade');
	$addGradeItem = $data->add_grade_item ?? null;
	
	// 동영상 관련 성적 항목 추가해야됨
	// econtents 관리자 설정에서 성적항목 추가 옵션 + econtents 설정에서 성적항목 추가 옵션이 사용함으로 설정된 경우에만 성적 항목 추가
	if ($configAddGradeItem && !empty($addGradeItem)) {
	    $data->cmidnumber = $data->coursemodule;
	    econtents_grade_item_update($data);
	}
	
		
	// 이벤트(일정) 추가/삭제로직
	econtents_update_calendar($data);

	return $data->id;
}

/**
 * Update econtents instance.
 * @param object $data
 * @param object $mform
 * @return bool true
 */
function econtents_update_instance($data, $mform) {
	global $DB;

	$data->timemodified = time();
	$data->id           = $data->instance;

	$result = true;
	if($olddata = $DB->get_record_sql('SELECT * FROM {econtents} WHERE id = :id', array('id'=> $data->id))) {
	    
	    // 동영상이 변경이 되었으면 변경된 동영상의 playtime으로 재반영
	    if ($data->econtentscontents != $olddata->econtentscontents) {
	        // econtents 테이블에 playtime 시간 저장하기
	        $contents = $DB->get_record_sql('SELECT id, filesize, playtime FROM {econtents_contents} WHERE id = :id', array('id'=>$data->econtentscontents));
	        
	        $data->playtime = $contents->playtime * MINSECS;    // econtents는 분단위로 입력하기 때문에 초단위로 변환이 필요함.
	        $data->filesize = $contents->filesize;
	    }
	    
	    // 성적항목 추가 여부
	    $configAddGradeItem = get_config('econtents', 'grade');
	    $oldAddGradeItem = $olddata->add_grade_item ?? null;
	    $addGradeItem = $data->add_grade_item ?? null;
	    
	    // 성적항목을 추가했다가 취소한 경우
	    if ($configAddGradeItem && $oldAddGradeItem != $addGradeItem && empty($addGradeItem)) {
	        // Delete grade items, outcome items and grades attached to modules.
	        if ($grade_items = grade_item::fetch_all(array('itemtype' => 'mod', 'itemmodule' => 'econtents', 'iteminstance' => $olddata->id, 'courseid' => $olddata->course))) {
	            foreach ($grade_items as $grade_item) {
	                $grade_item->delete('moddelete');
	            }
	        }
	    }
		// econtents 관련 성적 항목 추가해야됨
		// econtents 관리자 설정에서 성적항목 추가 옵션 + econtents 설정에서 성적항목 추가 옵션이 사용함으로 설정된 경우에만 성적 항목 추가
	    if($data->add_grade_item && $configAddGradeItem) {
			$data->cmidnumber = $data->coursemodule;
			
			// econtents 관련 성적 항목 변경해야됨,
			econtents_grade_item_update($data);
		}
		
		// 성적 항목 추가 사용 => 사용안함으로 변경시 autograde(자동성적반영) 값도 사용안함으로 변경되어야 함.
		if (empty($data->add_grade_item)) {
			$data->autograde = 0;
		}
		
		$modinfo = get_fast_modinfo($data->course);
		$isProgressReCalculate = false;
		$COnAttendance = \local_ubonattend\Attendance::getInstance($data->course);
		// 온라인 출석부를 사용하는 강좌인 경우 timeopen, timeclose, 출석인정률등의 정보를 온라인 출석부 설정에서 가져와야됨.
		if ($COnAttendance->isAttendanceCourse()) {
		    
		    // 진도처리를 사용하지 않는 영상이면 온라인 출석체크 여부도 아니오로 설정되어야 함.
		    if (empty($data->progress)) {
		        $data->useattend = 0;
		    }
		    
		    // 해당 주차에 설정된 진도처리 일자 가져오기
		    if ($sectionInfo = $modinfo->get_section_info($data->section)) {
		        
		        if ($onlineAttendanceSection = $COnAttendance->getSection($sectionInfo->id)) {
		            $data->timeopen = $onlineAttendanceSection->time_start;
		            
		            // 지각 기능 사용여부에 따라서 종료일이 달라짐
		            if ($COnAttendance->isLateCourse()) {
		                $data->timeclose = $onlineAttendanceSection->time_lateness;
		            } else {
		                $data->timeclose = $onlineAttendanceSection->time_end;
		            }
		            
		            $data->completionprogress = $onlineAttendanceSection->attendance_percent;
		            $data->tardinessprogress = $onlineAttendanceSection->lateness_percent;
		        }
		    }
		} else {
		    // 온라인 출석부가 아닌 경우
		    // 전달된 데이터와 이전 데이터의 timeopen, timeclose가 다른 경우 진도율 재계산이 이루어져야됨.
		    if ($olddata->timeopen != $data->timeopen || $olddata->timeclose != $data->timeclose) {
		        $isProgressReCalculate = true;
		    }
		}
		
		
		$DB->update_record('econtents', $data);
		
		// 진도 재계산을 해야되는 경우
		if ($isProgressReCalculate) {
		    // modedit.php에서 rebuild_course_cache하는데, 이곳에서 먼저 캐시가 삭제되어야지만 진도 재계산이 정상적으로 이루어짐
		    rebuild_course_cache($data->course, true);
		    
		    // 새로 갱신된 캐시로 다시 읽어와야되므로 재선언
		    $modinfo = get_fast_modinfo($data->course);
		    
		    if ($cm = $modinfo->get_cm($data->coursemodule)) {
		        $CCourse = \local_ubion\course\Course::getInstance();
		        
		        $roleid = $CCourse->getStudentRoleID();
		        if ($users = $CCourse->getCourseUserAll($data->course, array(), null, null, null, $roleid)) {
		            $CProgress = new \local_ubonattend\Progress($data->course);
		            $CProgress->setModuleProgress($cm, $users, $cm->section);
		        }
		        
		    }
		}

		
		// 이벤트(일정) 추가/삭제로직
		econtents_update_calendar($data);
		
		return $result;
	} else {
	    return false;
	}
	

}

/**
 * Delete econtents instance.
 * @param int $id
 * @return bool true
 */
function econtents_delete_instance($id) {
	global $DB;

	if ($econtents = $DB->get_record('econtents', array('id'=>$id))) {

	    // 트랙 관련된 데이터 모두 삭제 해야됨.
	    if ($tracks = $DB->get_records_sql('SELECT * FROM {econtents_track} WHERE econtentsid = :econtentsid', array('econtentsid' => $econtents->id))) {
	        foreach ($tracks as $t){
	            if (!$DB->delete_records('econtents_track_detail', array('track' => $t->id))) {
	                return false;
	            }
	        }
	        
	        if (!$DB->delete_records('econtents_track', array('econtentsid' => $econtents->id))) {
	            return false;
	        }
	    }
	    
	    $DB->delete_records('econtents', array('id'=>$econtents->id));
	    
	    
	    // 등록된 이벤트(일정) 삭제
	    $param = array('courseid'=>$econtents->course, 'modulename'=>'econtents', 'instance'=>$econtents->id);
	    if($event = $DB->get_record_sql('SELECT * FROM {event} WHERE courseid = :courseid AND modulename = :modulename AND instance = :instance', $param)) {
	        $badevent = calendar_event::load($event);
	        $badevent->delete();
	    }
	    
	    // 삭제시 진도 재계산은
	    // observer에서 처리하기 때문에 따로 이곳에서 처리할 필요가 없습니다.
	}

	return true;
}


/**
 * Create/update grade item for given econtents
 * 모듈 성적 추가 및 수정시 성적관련 테이블 정보 변경 함수
 *
 * @category grade
 * @uses GRADE_TYPE_NONE
 * @uses GRADE_TYPE_VALUE
 * @uses GRADE_TYPE_SCALE
 * @param stdClass $econtents econtents object with extra cmidnumber
 * @param mixed $grades Optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return int 0 if ok
 */
function econtents_grade_item_update($econtents, $grades=NULL) {
    global $CFG;
    if (!function_exists('grade_update')) { //workaround for buggy PHP versions
        require_once($CFG->libdir.'/gradelib.php');
    }
    
    $params = array('itemname'=>$econtents->name, 'idnumber'=>$econtents->cmidnumber);
    
    
    // 성적 관련 항목이 전달 된 경우
    if(isset($econtents->grade)) {
        if ($econtents->grade >= 0) {
            $params['gradetype'] = GRADE_TYPE_VALUE;
            $params['grademax']  = $econtents->grade;
            $params['grademin']  = 0;
        } else if ($econtents->grade < 0) {
            $params['gradetype'] = GRADE_TYPE_SCALE;
            $params['scaleid']   = -$econtents->grade;
        } else {
            $params['gradetype'] = GRADE_TYPE_TEXT; // allow text comments only
        }
    }
    
    if ($grades === 'reset') {
        $params['reset'] = true;
        $grades = NULL;
    }
    
    return grade_update('mod/econtents', $econtents->course, 'mod', 'econtents', $econtents->id, 0, $grades, $params);
}




function econtents_print_overview($courses, &$htmlarray) {
    global $CFG, $DB;
    
    if (empty($courses) || !is_array($courses) || count($courses) == 0) {
        return array();
    }
    
    require_once $CFG->libdir.'/resourcelib.php';
    
    $modulename = get_string('pluginname', 'econtents');
    foreach ($courses as $c) {
        // 강좌 마지막 접근 시간 이외에 신규로 추가된 econtents항목만 가져오기.
        $query = "SELECT
						*
			FROM		{econtents}
			WHERE		course = :course
			AND			timecreated > :lastaccess";
        
        if ($modules = $DB->get_records_sql($query, array('lastaccess'=>$c->lastaccess, 'course'=>$c->id))) {
            foreach ($modules as $m) {
                $return = html_writer::start_tag('div', array('class'=>'overview econtents'));
                
                $cm = get_coursemodule_from_instance('econtents', $m->id);
                
                $extra = '';
                if ($m->display == RESOURCELIB_DISPLAY_POPUP) {
                    $width = $m->popupwidth;
                    $height = $m->popupheight;
                    $wh = \mod_econtents\EContents::getPopupOption($width, $height);
                    $fullurl = "$CFG->wwwroot/mod/econtents/viewer.php?id=$cm->id";
                    $popupName = \mod_econtents\EContents::getPopupName();
                    
                    $extra = "onclick=\"window.open('$fullurl', '".$popupName."', '$wh'); return false;\" ";
                }
                //$link_extra = econtents_get_coursemodule_info($cm);
                
                $link = '<a href='.$CFG->wwwroot.'/mod/econtents/view.php?id='.$cm->id.' '.$extra.'>'.$m->name.'</a>';
                $return .= html_writer::tag('div', $modulename.' : '.$link, array('class'=>'name'));
                
                
                if (!empty($return)) {
                    if (!array_key_exists($m->course, $htmlarray)) {
                        $htmlarray[$m->course] = array();
                    }
                    if (!array_key_exists('econtents',$htmlarray[$m->course])) {
                        $htmlarray[$m->course]['econtents'] = ''; // initialize, avoid warnings
                    }
                    $htmlarray[$m->course]['econtents'] .= $return;
                }
            }
        }
        
    }
}







/**
 * Return a list of page types
 * @param string $pagetype current page type
 * @param stdClass $parentcontext Block's parent context
 * @param stdClass $currentcontext Current context of block
 */
function econtents_page_type_list($pagetype, $parentcontext, $currentcontext) {
    $module_pagetype = array('mod-econtents-*'=>get_string('page-mod-econtents-x', 'econtents'));
    return $module_pagetype;
}



/**
 * Given a course_module object, this function returns any
 * "extra" information that may be needed when printing
 * this activity in a course listing.
 *
 * See {@link get_array_of_activities()} in course/lib.php
 *
 * @param object $coursemodule
 * @return object info
 */
function econtents_get_coursemodule_info($coursemodule) {
    global $CFG, $DB;
    
    require_once $CFG->libdir.'/resourcelib.php';

    if (!$econtents = $DB->get_record_sql('SELECT * FROM {econtents} WHERE id = :id', array('id'=>$coursemodule->instance))) {
    	return NULL;
    }

    
    $info = new cached_cm_info();
    $info->name = $econtents->name;
    if ($coursemodule->showdescription) {
    	// Convert intro to html. Do not filter cached version, filters run at display time.
    	$info->content = format_module_intro('econtents', $econtents, $coursemodule->id, false);
    }

    $display = $econtents->display;
    
    if ($display == RESOURCELIB_DISPLAY_POPUP) {
        
        $fullurl = "$CFG->wwwroot/mod/econtents/viewer.php?id=$coursemodule->id";
        
        $width = $econtents->popupwidth;
        $height = $econtents->popupheight;
        $wh = \mod_econtents\EContents::getPopupOption($width, $height);
        $popupName = \mod_econtents\EContents::getPopupName();
        
        $info->onclick = "window.open('$fullurl', '".$popupName."', '$wh'); return false;";
    } else {
        $fullurl = "$CFG->wwwroot/mod/econtents/view.php?id=$coursemodule->id";
        $info->onclick = "window.location.href ='$fullurl';return false;";
    }
    
    // If any optional extra details are turned on, store in custom data
    
    // econtens같은 경우에는 무조건 열람시간으로 체크함.
    $econtents->isOpenTimeCheck = true;
    
    $info->customdata = \mod_econtents\EContents::getCustomData($econtents, $coursemodule);
    
    return $info;
}

/**
 * Called when viewing course page. Shows extra details after the link if
 * enabled.
 *
 * @param cm_info $cm Course module information
 */
function econtents_cm_info_view(cm_info $cm) {
    $customdata = $cm->customdata;
	
	$details = null;
	if (is_object($customdata)) {
	    $details = $customdata->details;
	} else if (!empty($customdata)){
	    $details = $customdata;
	}
	
	$cm->set_after_link(' ' . html_writer::tag('span', $details, array('class' => 'displayoptions')));
}



/**
 * Export URL resource contents
 *
 * @return array of file content
 
function econtents_export_contents($cm, $baseurl) {
    global $CFG, $DB;
    $contents = array();
    $context = context_module::instance($cm->id);

    $course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);
    $econtents = $DB->get_record('econtents', array('id'=>$cm->instance), '*', MUST_EXIST);

    $fullurl = str_replace('&amp;', '&', econtents_get_full_url($econtents, $cm, $course));
    $isurl = clean_param($fullurl, PARAM_URL);
    if (empty($isurl)) {
        return null;
    }

    $econtents = array();
    $econtents['type'] = 'econtents';
    $econtents['filename']     = $econtents->name;
    $econtents['filepath']     = null;
    $econtents['filesize']     = 0;
    $econtents['fileurl']      = $fullurl;
    $econtents['timecreated']  = null;
    $econtents['timemodified'] = $econtents->timemodified;
    $econtents['sortorder']    = null;
    $econtents['userid']       = null;
    $econtents['author']       = null;
    $econtents['license']      = null;
    $contents[] = $econtents;

    return $contents;
}
*/



function econtents_get_completion_state ($course, $cm, $userid, $type, $isCompletion=true) {
    global $DB;
    
    // econtents 정보 가져오기
    if($econtents = $DB->get_record_sql('SELECT * FROM {econtents} WHERE id = :id', array('id'=>$cm->instance))) {
        
        // 성적항목 추가 여부
        $configAddGradeItem = get_config('econtents', 'grade');
        
        // 이수체크를 하는 경우에는 학습시간을 준수해야되는 경우에만 진행하고,
        // 이수체크를 하지 않는 경우에는 진도율에 따라서 성적을 반영해줘야됨.
        if (($isCompletion && $econtents->completiontimeenabled) || empty($isCompletion)) {
            
            // local_ubonattend_module에서 진도율 정보 가져오기
            // 참고로 state 출석 상태값은 온라인 출석부를 사용하지 않더라도 진도율 기반으로 출석, 결석값을 입력해줍니다.
            
            $query = "SELECT status FROM {local_ubonattend_module} WHERE courseid = :courseid AND cmid = :cmid AND userid = :userid";
            $param = array('courseid' => $course->id, 'cmid' => $cm->id, 'userid' => $userid);
            $status = $DB->get_field_sql($query, $param);
            
            
            // 상태값이 없으면 동영상 자체를 시청하지 않은 상태라서 성적을 매길 필요가 없음
            if (empty($status)) {
                return false;
            } else {
                $rawgrade = 0;
                $return = false;
                
                // 만약 지각인 경우 점수를 따로 지정하고 싶으면 if문으로 분기 처리하시면 됩니다.
                if ($status == \local_ubonattend\Attendance::CODE_ATTENDANCE || $status == \local_ubonattend\Attendance::CODE_LATE) {
                    $rawgrade = 100;
                    $return = true;
                }
                
                
                // 성적항목 추가가 활성화 된 경우
                // 이수 체크를 위해서 성적 반영여부를 상단으로 조건문을 걸면 이수 조건(학습시간준수)을 타지 못하는 관계로 성적은 반드시 이곳에서 조건문이 걸려야 합니다.
                if ($configAddGradeItem) {
                    // 성적항목이 추가이며, 자동 성적 반영 로직인 경우...
                    if ($econtents->autograde == 1 && $econtents->add_grade_item) {
                        // 성적이 잠가져있지 않는 경우에만 성적 입력되도록 구현
                        if (!econtents_grading_disabled($course->id, $econtents->id, $userid)) {
                            $econtents->cmidnumber = $cm->id;
                            
                            $grade_time = time();
                            $grades = array();
                            $grades['rawgrade'] = $rawgrade;
                            $grades['userid'] = $userid;
                            // 고정적인 값을 넣어야될거 같은데... USER->id값을 넣으면 본인이 성적을 매기게될거 같은데..
                            $grades['usermodified'] = 2;	// 일단은 기본적으로 생성되는 admin 계정이 변경한걸로 셋팅
                            $grades['datesubmitted'] = $grade_time;
                            $grades['dategraded'] = $grade_time;
                            
                            // 성적 입력 로직 추가
                            econtents_grade_item_update($econtents, $grades);
                        }
                    }
                }
                
                return $return;
            }
            
        } else {
            return $type;
        }
    } else {
        return false;
    }
}

function econtents_grading_disabled($courseid, $moduleid, $userid) {
    global $CFG;
    
    if (!function_exists('grade_get_grades')) { //workaround for buggy PHP versions
        require_once($CFG->libdir.'/gradelib.php');
    }
    
    $gradinginfo = grade_get_grades($courseid, 'mod', 'econtents', $moduleid, array($userid));
    if (!$gradinginfo) {
        return false;
    }
    
    if (!isset($gradinginfo->items[0]->grades[$userid])) {
        return false;
    }
    $gradingdisabled = $gradinginfo->items[0]->grades[$userid]->locked || $gradinginfo->items[0]->grades[$userid]->overridden;
    return $gradingdisabled;
}




/*****************
 * 초기화 관련 함수
 *****************/

/**
 * 모든 econtents에 대해서 성적 초기화
 *
 * @param int $courseid
 * @param string $type
 */
function econtents_reset_gradebook($courseid, $type='') {
    global $DB;
    
    $sql = "SELECT s.*, cm.idnumber as cmidnumber, s.course as courseid
              FROM {econtents} s, {course_modules} cm, {modules} m
             WHERE m.name=:moduletype AND m.id=cm.module AND cm.instance=s.id AND s.course=:courseid";
    $params = array('moduletype'=>'econtents', 'courseid'=>$courseid);
    
    if ($econtents = $DB->get_records_sql($sql, $params)) {
        foreach ($econtents as $econtent) {
            econtents_grade_item_update($econtent, 'reset');
        }
    }
}

function econtents_reset_course_form_defaults($course) {
    return array('reset_econtents_track'=>0);
}


function econtents_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'econtetnsheader', get_string('modulenameplural', 'econtents'));
    $mform->addElement('checkbox', 'reset_econtens_track', get_string('reset_econtents_track','econtents'));
}

/**
 * This function is used by the reset_course_userdata function in moodlelib.
 *
 * @param $data  : the data submitted from the reset course.
 * @return array status array
 */
function econtents_reset_userdata($data) {
    global $DB;
    
    $componentstr = get_string('modulenameplural', 'econtents');
    $resetallstr    = get_string('reset_econtens_track', 'econtents');
    $status = array();
    $params = array('course'=>$data->courseid);
    
    
    if (!empty($data->reset_econtents_track)) {
        if($econtents = $DB->get_records_sql('SELECT * FROM {econtents} WHERE course = :course', $params)) {
            foreach($econtents as $m) {
                // 해당 econtents에 등록된 Track정보 가져와서 삭제 해야됨.
                if($tracks = $DB->get_records_sql('SELECT * FROM {econtents_track} WHERE econtentsid = :econtentsid', array('econtentsid'=>$m->id))) {
                    // econtents_track_detail 테이블 삭제
                    foreach($tracks as $t) {
                        $DB->delete_records('econtents_track_detail', array('track'=>$t->id));
                    }
                    
                    // track 테이블도 삭제
                    $DB->delete_records('econtents_track', array('id'=>$t->id));
                }
            }
            
            $status[] = array('component'=>$componentstr, 'item'=>$resetallstr, 'error'=>false);
        }
    }
    
    
    return $status;
}



/**
 * List of view style log actions
 * @return array
 */
function econtents_get_view_actions() {
    return array('view', 'view all', 'view viewer');
}

/**
 * List of update style log actions
 * @return array
 */
function econtents_get_post_actions() {
    return array('update', 'add');
}


/**
 * Update the calendar entries for this econtents.
 *
 * @param int $cmid - Required to pass this in because it might not exist in the database yet.
 * @param object $data
 * @return bool
 */
function econtents_update_calendar($data) {
    global $DB, $CFG;
    require_once($CFG->dirroot.'/calendar/lib.php');
    
    /**
     * 현재 event 등록은 무조건 1개의 레코드만 등록이 됨.
     * 만약 퀴즈처럼 5일(설정하기나름임)이상의 반복일정인 경우에는 2개 레코드로 분리해서 등록을 시킬수가 있는데..
     * 이러면 사용자들이 헷갈려할수가 있을거 같음.
     * 레코드를 분리해서 사용하려면 evernote (econtents 시작/종료로 분리해서 일정 등록하는 소스) 에서 소스를 찾으면 됨.
     */
    
    $modulename = 'econtents';
    $params = array('courseid'=>$data->course, 'modulename'=>$modulename, 'instance'=>$data->id);
    $old_event = $DB->get_record_sql('SELECT * FROM {event} WHERE courseid = :courseid AND modulename = :modulename AND instance = :instance', $params);
    
    // 진도처리 시작 또는 종료가 지정된 경우
    if (!empty($data->timeopen) || !empty($data->timeclose)) {
        $event = new stdClass();
        // 등록된 이벤트가 존재하는지 확인
        $event->id = (isset($old_event->id) && !empty($old_event->id)) ? $old_event->id : null;
        $event->name = $data->name;
        
        // Convert the links to pluginfile. It is a bit hacky but at this stage the files
        // might not have been saved in the module area yet.
        $intro = $data->intro;
        if ($draftid = file_get_submitted_draft_itemid('introeditor')) {
            $intro = file_rewrite_urls_to_pluginfile($intro, $draftid);
        }
        
        // We need to remove the links to files as the calendar is not ready
        // to support module events with file areas.
        $intro = strip_pluginfile_content($intro);
        $event->description = array(
            'text' => $intro,
            'format' => $data->introformat
        );
        
        $event->courseid    = $data->course;
        $event->groupid     = 0;
        $event->userid      = 0;
        $event->modulename  = $modulename;
        $event->instance    = $data->id;
        $event->timeduration = 0;
        
        // 시작, 종료가 모두 지정된 경우
        if(!empty($data->timeopen) && !empty($data->timeclose)) {
            $event->eventtype = 'due';
            $event->timestart = $data->timeopen;
            $event->timeduration = max($data->timeclose - $data->timeopen, 0);	// 반복 일정 기간
        } else if(!empty($data->timeopen)) {
            $event->eventtype = 'open';
            $event->timestart = $data->timeopen;
        } else if(!empty($data->timeclose)) {
            $event->eventtype = 'close';
            $event->timestart = $data->timeclose;
        }
        
        if(empty($event->id)) {
            calendar_event::create($event, false);
        } else {
            $calendarevent = calendar_event::load($event->id);
            $calendarevent->update($event, false);
        }
    } else {
        if($old_event) {
            $badevent = calendar_event::load($old_event);
            $badevent->delete();
        }
    }
}


/**
 * Return use outline
 * @param object $course
 * @param object $user
 * @param object $mod
 * @param object $xncommons
 * @return object|null
 */
function econtents_user_outline($course, $user, $mod, $xncommons) {
	global $DB;

	$query = "SELECT action_count FROM {logstore_ubstats_activity} WHERE courseid = :courseid AND userid = :userid AND cmid = :cmid AND action = :action";
	if ($viewCount = $DB->get_field_sql($query, array('courseid' => $course->id, 'userid'=>$user->id, 'cmid'=>$mod->id, 'action'=>'viewed'))) {
	    
	    $result = new stdClass();
	    $result->info = get_string('numviews', '', $viewCount);
	    
	    /* 시간까지는 표시하지 않음.
	     $query = "SELECT timecreated FROM {logstore_standard_log} WHERE courseid = :courseid AND contextlevel = :contextlevel AND contextinstanceid = :cmid AND userid = :userid";
	     $result->time = $DB->get_field_sql($query, array('courseid'=>$course->id, 'contextlevel'=>CONTEXT_MODULE, 'cmid'=>$mod->id, 'userid'=>$user->id));
	     */
	    
	    return $result;
	}
	
	return NULL;
}