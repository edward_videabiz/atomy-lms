<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/econtents/backup/moodle2/restore_econtents_stepslib.php');

class restore_econtents_activity_task extends restore_activity_task {
    
    protected function define_my_settings() {
        
    }
    
    protected function define_my_steps() {
        $this->add_step(new restore_econtents_activity_structure_step('econtents_structure', 'econtents.xml'));
    }
    
    
    static public function define_decode_contents() {
        $contents = array();

        $contents[] = new restore_decode_content('econtents', array('intro'), 'econtents');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    static public function define_decode_rules() {
        $rules = array();
        
        // List of econtents in course
        $rules[] = new restore_decode_rule('ECONTENTSINDEX', '/mod/econtents/index.php?id=$1', 'course');
        // econtents by cm->id and econtents->id
        $rules[] = new restore_decode_rule('ECONTENTSVIEWBYID', '/mod/econtents/view.php?id=$1', 'course_module');
        // econtents by cm->id and econtents->id
        $rules[] = new restore_decode_rule('ECONTENTSVIEWERBYID', '/mod/econtents/viewer.php?id=$1', 'course_module');
        
        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * forum logs. It must return one array
     * of {@link restore_log_rule} objects
     */
    static public function define_restore_log_rules() {
        $rules = array();
        
        $rules[] = new restore_log_rule('econtents', 'add', 'view.php?id={course_module}', '{econtents}');
        $rules[] = new restore_log_rule('econtents', 'update', 'view.php?id={course_module}', '{econtents}');
        $rules[] = new restore_log_rule('econtents', 'view', 'view.php?id={course_module}', '{econtents}');
        $rules[] = new restore_log_rule('econtents', 'view viewer', 'viewer.php?id={course_module}', '{econtents}');
        
        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * course logs. It must return one array
     * of {@link restore_log_rule} objects
     *
     * Note this rules are applied when restoring course logs
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    static public function define_restore_log_rules_for_course() {
        $rules = array();
        
        $rules[] = new restore_log_rule('econtents', 'view all ', 'index.php?id={course}', null);
        
        return $rules;
    }
}

?>
