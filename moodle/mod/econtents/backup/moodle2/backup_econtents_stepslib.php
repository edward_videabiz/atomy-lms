<?php

class backup_econtents_activity_structure_step extends backup_activity_structure_step {
    protected function define_structure() {
        // To know if we are including userinfo
        $userinfo = $this->get_setting_value('userinfo');

        // Define each element separated
        $econtents = new backup_nested_element('econtents', array('id'), array(
            'course', 'name',  
            'intro', 'introformat', 
            'econtentscontents', 'playtime',
            'popupwidth', 'popupheight', 
            'showplaytime',
            'progress',
            'timeopen', 'timeclose',
            'completionprogress', 'tardinessprogress',
            'restricted', 'completiontimeenabled',   
			'autograde', 'add_grade_item', 'useattend',
            'timecreated', 'timemodified'
        ));

        $tracks = new backup_nested_element('tracks');
        $track = new backup_nested_element('track', array('id'), array(
            'econtentsid', 'userid', 'progress', 'totalprogress', 'progress_time', 'totalprogress_time', 'timefirst', 'timelast', 'attempts', 'recognition', 'lastattempts'));

        $trackDetails = new backup_nested_element('details');
        $trackDetail = new backup_nested_element('detail', array('id'), array(
            'track', 'attempt', 'state', 'positionold', 'positionnew', 'timetrack'
		));

        $trackDevices = new backup_nested_element('devices');
        $trackDevice = new backup_nested_element('device', array('id'), array(
            'track', 'attempt', 'device', 'timecreated', 'ip'
		));


        // Build the tree
        $econtents->add_child($tracks);
        $tracks->add_child($track);

		// 진도관리 상세
        $track->add_child($trackDetails);
        $trackDetails->add_child($trackDetail);
		
        $track->add_child($trackDevices);
        $trackDevices->add_child($trackDevice);

        // Define sources
        $econtents->set_source_table('econtents', array('id' => backup::VAR_ACTIVITYID));

        if ($userinfo) {
            $tracks->set_source_table('econtents_track', array('econtentsid'=>backup::VAR_PARENTID));
            $trackDetail->set_source_table('econtents_track_detail', array('track'=>backup::VAR_PARENTID));
            $trackDevice->set_source_table('econtents_track_device', array('track'=>backup::VAR_PARENTID));
        }


        // Define id annotations
        $track->annotate_ids('user', 'userid');
        
        // ubboard에서 사용되는 첨부파일중 mdl_files에 저장되는 값들을 모두 적어줘야됨.
        $econtents->annotate_files('mod_econtents', 'intro', null); // chat_intro area don't use itemid


        // Return the root element (econtents), wrapped into standard activity structure
        return $this->prepare_activity_structure($econtents);
    }
}
?>
