<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/mod/econtents/backup/moodle2/backup_econtents_stepslib.php'); // Because it exists (must)

class backup_econtents_activity_task extends backup_activity_task {
    
    /**
     * Define (add) particular settings this activity can have
     */
    protected function define_my_settings() {
        //
    }

    /**
     * Define (add) particular steps this activity can have
     */
    protected function define_my_steps() {
        $this->add_step(new backup_econtents_activity_structure_step('econtents structure', 'econtents.xml'));
    }
    
    /**
     * Code the transformations to perform in the activity in
     * order to get transportable (encoded) links
     */
    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot,"/");
        
        $search="/(".$base."\/mod\/econtents\/index.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@ECONTENTSINDEX*$2@$', $content);
        
        $search="/(".$base."\/mod\/econtents\/view.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@ECONTENTSVIEWBYID*$2@$', $content);
        
        $search="/(".$base."\/mod\/econtents\/viewer.php\?id\=)([0-9]+)/";
        $content= preg_replace($search, '$@ECONTENTSVIEWERBYID*$2@$', $content);
        
        
        return $content;
    }
}
?>
