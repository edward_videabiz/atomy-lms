<?php

class restore_econtents_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {
        
        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        // econtents 정보
        $paths[] = new restore_path_element('econtents', '/activity/econtents');
        
        // 사용자 정보까지 가져오는 경우. 진도관리 정보 복구
        if ($userinfo) {
            $paths[] = new restore_path_element('econtents_track', '/activity/econtents/tracks/track');
        	$paths[] = new restore_path_element('econtents_track_detail', '/activity/econtents/tracks/track/details/detail');
        	$paths[] = new restore_path_element('econtents_track_device', '/activity/econtents/tracks/track/devices/device');

        }

        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }

    protected function process_econtents($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        $data->timecreated = time();
        $data->timemodified = null;
        
        
        // gradeitem_add => add_grade_item으로 변경된 항목에 대한 처리
        $addGradeItem = $data->add_grade_item ?? $data->gradeitem_add;
        $data->add_grade_item = $addGradeItem;

        $newitemid = $DB->insert_record('econtents', $data);
        $this->apply_activity_instance($newitemid);
    }

    
    protected function process_econtents_track($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;

		$data->econtentsid = $this->get_new_parentid('econtents');
		$data->userid = $this->get_mappingid('user', $data->userid);

    	$data->timefirst = $this->apply_date_offset($data->timefirst);
    	$data->timelast = $this->apply_date_offset($data->timelast);

		$newitemid = $DB->insert_record('econtents_track', $data);
        $this->set_mapping('econtents_track', $oldid, $newitemid);
    }


    protected function process_econtents_track_detail($data) {
    	global $DB;

    	$data = (object)$data;
    	$oldid = $data->id;

    	$data->track = $this->get_new_parentid('econtents_track');
    	$data->timetrack = $this->apply_date_offset($data->timetrack);

    	$newitemid = $DB->insert_record('econtents_track_detail', $data);
    	$this->set_mapping('econtents_track_detail', $oldid, $newitemid);
    }

    protected function process_econtents_track_device($data) {
    	global $DB;

    	$data = (object)$data;
    	$oldid = $data->id;

    	$data->track = $this->get_new_parentid('econtents_track');
    	$data->timecreated = $this->apply_date_offset($data->timecreated);

    	$newitemid = $DB->insert_record('econtents_track_device', $data);
    	$this->set_mapping('econtents_track_device', $oldid, $newitemid);
    }

    protected function after_execute() {
        // Add url related files, no need to match by itemname (just internally handled context)
        $this->add_related_files('mod_econtents', 'intro', null);
    }

}

?>
