<?php
namespace mod_econtents\core;

use local_ubion\controller\Controller;
use local_ubion\base\Javascript;
use local_ubion\base\Parameter;
use local_ubion\base\Common;
use local_ubonattend\TraitProgress;

class EContents extends Controller
{
    use TraitProgress;

    protected $pluginname = 'econtents';
    protected $config = null;

    public function __construct($config = null)
    {
        if (empty($config)) {
            $this->config = get_config('econtents');
        } else {
            $this->config = $config;
        }
    }


    /**
     * 설정 값 가져요기
     *
     * @return void
     */
    public function getConfig()
    {
        return $this->config;
    }


    /**
     * 등록된 contents 목록
     *
     * @param string $keyfield
     * @param string $keyword
     * @param int $page
     * @param int $ls
     * @param int $userid
     * @return array
     */
    public function getLists($keyfield, $keyword, $page = 1, $ls = 15, $userid = null)
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        if ($page <= 0) {
            $page = 1;
        }
        $limitfrom = $ls * ($page - 1);

        $replace = "##REPLACE##";

        $query = "SELECT
						      ".$replace."
        		  FROM        {econtents_contents} c
        		  JOIN	      {user} u ON c.userid = u.id
                  WHERE       isdelete = 0";

        $where = array();
        $param = array();

        $isSiteAdmin = is_siteadmin($userid);

        if (!$isSiteAdmin) {
            $where[] = "userid = :userid";
            $param['userid'] = $userid;
        }

        if (!empty($keyword)) {
            // 관리자인 경우에는 사용자 검색이 가능하고, 관리자 이외에는 콘텐츠 제목으로만 검색이되어야함.
            if ($isSiteAdmin) {
                if ($keyfield == 'idnumber') {
                    $where[] = $DB->sql_like('idnumber', ':keyword');
                } else if ($keyfield == 'fullname') {
                    $fullname = $DB->sql_fullname('u.firstname','u.lastname');
                    $where[] = $DB->sql_like('LOWER('.$fullname.')', ':keyword');

                    $param['keyword'] = '%'.$keyword.'%';
                } else {
                    $where[] = $DB->sql_like('LOWER(contentname)', ':keyword');
                }
            } else {
                $where[] = $DB->sql_like('LOWER(contentname)', ':keyword');
            }

            $param['keyword'] = '%'.$keyword.'%';
        }


        $whereCount = count($where);
        if ($whereCount > 0) {
            $query .= ' AND '.join(' AND ', $where);
        }

        $totalQuery = str_replace($replace, 'COUNT(1)', $query);

        $totalCount = $DB->get_field_sql($totalQuery, $param);


        $usernameField = get_all_user_name_fields(true, 'u');
        $field = " c.*
                   ,u.username
                   ,u.idnumber
                   ,".$usernameField;

        $query = str_replace($replace, $field, $query);
        $lists = $DB->get_records_sql($query.' ORDER BY timecreated DESC', $param, $limitfrom, $ls);

        return array($totalCount, $lists);
    }


    /**
     * contents view (econtents_contents 테이블의 단일 레코드 조회)
     *
     * @param int $id
     * @return \stdClass
     */
    public function getContentView($id)
    {
        global $DB;

        $query = "SELECT * FROM {econtents_contents} WHERE id = :id";
        $param = array('id' => $id);

        return $DB->get_record_sql($query, $param);
    }

    /**
     * econtents 가 저장될 폴더명
     * @return string
     */
    public static function getSaveFolderName()
    {
        // 파일경로 : $CFG->dataroot/입력한 폴더명
        return 'econtents';
    }

    public function getSymbolicFolderName()
    {
        return '_econtents';
    }



    public function doUpload()
    {
        $id             = Parameter::post('id', null, PARAM_INT);
        $contentName 	= Parameter::post('contentname', null, PARAM_NOTAGS);
        $contentUrl 	= Parameter::post('contenturl', null, PARAM_NOTAGS);
        $description	= Parameter::post('description', null, PARAM_NOTAGS);
        $isPublic 	    = Parameter::post('ispublic', $this->config->ispublic, PARAM_INT);
        $playtime       = Parameter::post('learningtime', $this->config->default_playtime, PARAM_INT);
        $popupWidth 	= Parameter::post('popupwidth', $this->config->popupwidth, PARAM_INT);
        $popupHeight 	= Parameter::post('popupheight', $this->config->popupheight, PARAM_INT);
        $isestream 	    = Parameter::post('isestream', 0, PARAM_INT);
        $filename       = Parameter::post('filename', null, PARAM_NOTAGS);
        $filesize       = Parameter::post('filesize', 0, PARAM_INT);


        // 필수값 체크
        $validate = $this->validate()->make(array(
            'contentname' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('contentname', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $contentName
            )
            ,'contenturl' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('filepath_index', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $contentUrl
            )
            ,'learningtime' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('learningtime', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $playtime
            )
            ,'popupwidth' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('popupwidth', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $popupWidth
            )
            ,'popupheight' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('popupheight', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $popupHeight
            )
        ));


        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {


            // 기존에 등록되어있던 항목인지 확인
            if (!empty($id)) {
                if ($econtents = $this->getContentView($id)) {
                    $this->setUpdate($econtents->id, $contentName, $contentUrl, $description, $isPublic, $playtime, $popupWidth, $popupHeight, $filename, $filesize, $isestream);
                    
                    // 재생시간이 변경되었는지 확인해봐야됨.
                    if ($econtents->playtime != $playtime) {
                        // 해당 영상을 사용하는 강좌들의 진도율을 재계산해야됨.
                        // 재생시간이 바뀔 확률은 거의 없는데.. 혹시나 해서 추가해놓음
                        $this->setUpdateProgressRecal($id, $playtime);
                    }
                } else {
                    Javascript::printAlert(get_string('notexist_econtents', $this->pluginname));
                }
            } else {
                $id = $this->setInsert($contentName, $contentUrl, $description, $isPublic, $playtime, $popupWidth, $popupHeight, $filename, $filesize, $isestream);
            }

            // 첨부파일이 존재하는지 확인
            $fileUploadName = 'uploadfile';
            if (isset($_FILES[$fileUploadName]) && !empty($_FILES[$fileUploadName])) {

                // zip 또는 html 파일이 저장될 경로
                // econtents/econtentscontentid
                $tmpDir = self::getEContentsIDFolder($id);

                if (!is_dir($tmpDir)) {
                    mkdir($tmpDir, 0777);
                } else {
                    // 기존 폴더 삭제 후 재 업로드
                    Common::setDirectoryFileDelete($tmpDir);
                    mkdir($tmpDir, 0777);
                }

                // 첨부파일 확장자 검사.
                $extentions = array('zip', 'html', 'htm');

                $path = pathinfo($_FILES[$fileUploadName]['name']);
                $ext = strtolower($path['extension']);

                if (!in_array($ext, $extentions)) {
                    Javascript::printAlert(get_string('onlyhtmlzip', $this->pluginname));
                }


                if ($ext == 'zip') {
                    $zipFile = $tmpDir.'/'.time().'.zip';
                    move_uploaded_file($_FILES[$fileUploadName]['tmp_name'], $zipFile);

                    // 압축해제
                    $fp = get_file_packer('application/zip');
                    $fp->extract_to_pathname($zipFile, $tmpDir);

                } else {
                    $saveFileName = $tmpDir.'/'.$_FILES[$fileUploadName]['name'];
                    move_uploaded_file($_FILES[$fileUploadName]['tmp_name'], $saveFileName);
                }
            }


            // 썸네일
            // 기존 썸네일 제거
            // 현재는 action이 일어날때마다 썸네일 파일 삭제 후 새로 저장하기 때문에
            // 단순정보 수정시 썸네일이 제거되었다라는 문의가 올수 있음.
            // 코스모스 3.0에서 삭제 체크 박스가 따로 존재했으나, 이를 제거하고 무조건 삭제하는 방식으로 처리가 되어 있어서 3.4x에서도 동일한 방법으로 처리함.
            // 상황에 따라 삭제 체크박스를 되살려야될수도 있음.
            /* 썸네일 기능 제거
            $thumbFile = self::getEContentsThumbnail($id);
            if (is_file($thumbFile)) {
                unlink($thumbFile);
            }

            $fileUploadName = 'thumb';
            if (isset($_FILES[$fileUploadName]) && !empty($_FILES[$fileUploadName])) {
                if (getimagesize($_FILES[$fileUploadName]['tmp_name'])) {

                    move_uploaded_file($_FILES[$fileUploadName]['tmp_name'], $thumbFile);
                }
            }
            */

            Javascript::printAlert(get_string('save_complete', 'local_ubion'), true);
        }
    }

    /**
     * econtents 재생시간 변경으로 인한 진도율 재계산
     * 
     * @param int $id
     */
    public function setUpdateProgressRecal($id, $playtime)
    {
        global $DB;
        
        $query = "SELECT 
                            course 
                  FROM      {econtents} 
                  WHERE     econtentscontents = :econtentscontents
                  GROUP BY  course";
        
        $param = [
            'econtentscontents' => $id
        ];
        
        if ($courses = $DB->get_records_sql($query, $param)) {
            foreach ($courses as $c) {
                
                $query = "SELECT
                                    id
                          FROM      {econtents}
                          WHERE     course = :courseid     
                          AND       econtentscontents = :econtentscontents";
                
                $param = [
                    'courseid' => $c->course,
                    'econtentscontents' => $id
                ];
                
                if ($modules = $DB->get_records_sql($query, $param)) {
                
                    foreach ($modules as $m) {
                        $update = new \stdClass();
                        $update->id = $m->id;
                        $update->playtime = $playtime * MINSECS;
                        
                        $DB->update_record('econtents', $update);
                    }
                    
                    // 모듈 한개에 대해서 진도 상태값이 변경이 되면 주차, 강좌 모두 진도 상태가 변경이 되어야 하기 때문에
                    // 강좌 전체에 대해서 진도율 재계산하는것과 크게 다른점이 없음.
                    // 참고로 강좌가 종료된 (학습기간이 종료된 경우) 경우에는 진도율이 재계산되기 않습니다.
                    $CProgress = new \local_ubonattend\Progress($c->course);
                    $CProgress->setReCalculation();
                }
                
            }
        }
    }

    public function setUpdate($id, $contentName, $contentUrl, $description, $isPublic, $playtime, $popupWidth, $popupHeight, $filename, $filesize, $isestream)
    {
        global $DB;

        $econtents = new \stdClass();
        $econtents->id = $id;
        $econtents->contentname = $contentName;
        $econtents->contenturl = $contentUrl;
        $econtents->description = $description;
        $econtents->ispublic = $isPublic;
        $econtents->playtime = $playtime;
        $econtents->popupwidth = $popupWidth;
        $econtents->popupheight = $popupHeight;
        $econtents->filename = $filename;
        $econtents->filesize = $filesize;
        $econtents->isestream = $isestream;
        $econtents->timemodified = time();

        $DB->update_record('econtents_contents', $econtents);
    }


    public function setInsert($contentName, $contentUrl, $description, $isPublic, $playtime, $popupWidth, $popupHeight, $filename, $filesize, $isestream, $userid = null)
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $econtents = new \stdClass();
        $econtents->userid = $userid;
        $econtents->contentname = $contentName;
        $econtents->contenturl = $contentUrl;
        $econtents->description = $description;
        $econtents->ispublic = $isPublic;
        $econtents->playtime = $playtime;
        $econtents->popupwidth = $popupWidth;
        $econtents->popupheight = $popupHeight;
        $econtents->filename = $filename;
        $econtents->filesize = $filesize;
        $econtents->isestream = $isestream;
        $econtents->timecreated = time();

        $econtents->id = $DB->insert_record('econtents_contents', $econtents);

        return $econtents->id;
    }


    /**
     * 썸네일 물리적 경로
     *
     * @return string
     */
    public static function getEContentsFolder()
    {
        global $CFG;

        // 파일 저장 경로
        $dir = $CFG->dataroot.'/'.self::getSaveFolderName();
        if (!is_dir($dir)) {
            mkdir($dir, 0777);
        }

        return $dir;
    }

    /**
     * 특정 contents의 썸네일 물리적 경로
     *
     * @param int $id
     * @return string
     */
    public static function getEContentsIDFolder($id)
    {
        $dir = self::getEContentsFolder();

        $dir .= '/'.$id;

        if (!is_dir($dir)) {
            mkdir($dir, 0777);
        }

        return $dir;
    }

    /**
     * 썸네일 파일 물리적 경로
     *
     * @param int $id
     * @return void
     */
    public static function getEContentsThumbnail($id)
    {
        $dir = self::getEContentsFolder();

        $thumbFile = $dir.'/'.$id.'_thumb';

        return $thumbFile;
    }

    public function doCheckDelete()
    {
        $ids = Parameter::postArray('econtentid', null, PARAM_INT);

        $idsCount = count($ids);

        if ($idsCount > 0) {
            foreach ($ids as $id) {
                $this->setContentDelete($id);
            }

            Javascript::printAlert(get_string('delete_complete', 'local_ubion'), true);
        } else {
            Javascript::printAlert(get_string('no_check_content', $this->pluginname));
        }
    }


    /**
     * contents 삭제
     *
     * @param int $id
     * @return void
     */
    public function setContentDelete($id)
    {
        global $DB;

        // 실제 존재하는 항목인지 확인
        if ($this->getContentView($id)) {
            // econtents의 파일 및 db내용은 실수로 삭제해서 자료 복구 요청을 하는 경우가 있기 때문에
            // 데이터 및 디비에서 삭제하지 않음.
            // $dir = self::getEContentsIDFolder($id);
            // Common::setDirectoryFileDelete($dir);

            // 사용된 contents 는 삭제하지 않고 삭제여부만 기록함. 20150209 hahaa
            if ($DB->get_record_sql('SELECT * FROM {econtents} WHERE econtentscontents = :econtentscontents', array('econtentscontents' => $id))){
                $DB->update_record('econtents_contents', array('id'=>$id, 'isdelete'=>'1'));
            } else {
                $DB->delete_records('econtents_contents', array('id'=>$id));
            }
        }
    }

    /**
     * 팝업창 이름 (팝업창 중복 방지)
     *
     * @return string
     */
    public static function getPopupName()
    {
        $sameWindow = get_config('econtents', 'same_window');

        $name = '';
        if (!empty($sameWindow)) {
            $name = 'econtents_viewer';
        }

        return $name;
    }


    /**
     * view event
     *
     * @param object $course
     * @param object $cm
     * @param object $context
     * @param boolean $isViewPage
     */
    public function setViewEvent($course, $cm, $context, $isViewPage=true)
    {
        global $DB;

        $action = ($isViewPage) ? 'view' : 'viewer';

        $params = array(
            'context' => $context,
            'objectid' => $cm->instance
            ,'other' => array(
                'action'=>$action
            )
        );
        $event = \mod_econtents\event\course_module_viewed::create($params);
        $event->trigger();

        // 뷰어를 오픈한 경우 (view.php 호출시 임베디드 표시 방법이 아니라면 completion 로직은 실행되면 안됨)
        if (!$isViewPage) {
            // Update 'viewed' state if required by completion system
            $completion = new \completion_info($course);
            $completion->set_module_viewed($cm);
        }
    }


    public function doTrackDetail()
    {
        // error_log(print_r($_POST, true));
        $courseid = Parameter::post('courseid', null, PARAM_INT);
        $cmid = Parameter::post('cmid', null, PARAM_INT);
        $state = Parameter::post('state', null, PARAM_INT);
        $positionfrom = Parameter::post('positionfrom', null, PARAM_INT);
        $positionto = Parameter::post('positionto', null, PARAM_INT);
        $sesskey = Parameter::post('sesskey', 'a', PARAM_ALPHANUMEXT);

        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            )
            ,'cmid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('cmid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $cmid
            )
            ,'state' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('track_state', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $state
            )
            ,'positionfrom' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('track_position_from', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $positionfrom
            )
            ,'positionto' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('track_position_to', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $positionto
            )
            ,'sesskey' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => 'sesskey'
                ,$this->validate()::PARAMVALUE => $sesskey
            )
        ));


        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {

            // 로그인 검사
            if (isloggedin()) {
                // sesskey 검사
                if (confirm_sesskey($sesskey)) {

                    $cm = get_fast_modinfo($courseid)->get_cm($cmid);

                    // 소수점으로 전달되기 때문에 내림 처리해야됨.
                    // PARAM_INT에 의해서 내림 처리 되긴 하지만 혹시 몰라서 2중으로 처리
                    $positionfrom = floor($positionfrom);
                    $positionto = floor($positionto);

                    self::setProgressTrackDetail($cm, $state, $positionfrom, $positionto);

                    Javascript::printAlert(get_string('save_complete', 'local_ubion'), true);

                } else {
                    Javascript::getAlertParentReload(get_string('invalidsesskey', 'error'));
                }
            } else {
                Javascript::getAlertParentReload(get_string('invalidlogin'));
            }
        }
    }

}