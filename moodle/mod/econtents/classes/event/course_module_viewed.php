<?php
namespace mod_econtents\event;
defined('MOODLE_INTERNAL') || die();

class course_module_viewed extends \core\event\course_module_viewed {

    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['objecttable'] = 'econtents';
        $this->data['crud'] = 'r';
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
    }
    
    
    /**
     * Custom validation.
     *
     * @throws \coding_exception
     * @return void
     */
    protected function validate_data() {
    	parent::validate_data();
    
    	if (!isset($this->other['action'])) {
    		throw new \coding_exception('The \'action\' value must be set in other.');
    	}
    }
}
