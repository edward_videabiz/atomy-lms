<?php
use \local_ubion\base\Common;
use \local_ubion\base\Paging;
use \local_ubion\base\Parameter;

require_once '../../config.php';

$pluginname = 'econtents';

// 이러닝 콘텐츠 리스트
$page       = optional_param('page', 1, PARAM_INT);     		// page
$keyfield	= optional_param('keyfield', null, PARAM_ALPHANUMEXT);
$keyword	= Parameter::getKeyword();
$ls			= optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);

$PAGE->set_url('/mod/econtetns/_list.php', array('page'=>$page, 'keyfield'=>$keyfield, 'keyword'=>$keyword));
$baseurl = $PAGE->url;

if (!isloggedin()) {
    Common::printError(get_string('invalidlogin'));
}

// 이곳은 개인화 된 페이지 임.
$PAGE->set_context(context_user::instance($USER->id));

$i8n = new stdClass();
$i8n->select = get_string('select', $pluginname);
$i8n->preview = get_string('preview', $pluginname);

$CUser = \local_ubion\user\User::getInstance();
$CEContents = new \mod_econtents\EContents();
list ($totalCount, $lists) = $CEContents->getLists($keyfield, $keyword, $page, $ls);
?>
<script type="text/javascript">
    require(["mod_econtents/management"], function(management) {
        $(".list .pageinfo .refresh").click(function() {
            management.list();
		});


     	// 페이징 클릭시
		$(".list .pagination .page-link").click(function() {
			management.modalMove($(this).attr('href'));
			return false;
		});


		// 동영상 클릭시
		$(".list .a-link").click(function() {
		    management.modalMove($(this).attr('href'));
			return false;
		});

		// 파일 검색
		$(".list .form-search").submit(function() {

			serial = $(this).serialize();
			management.modalMove(M.cfg.wwwroot + '/mod/econtents/_list.php?'+serial);
			return false;
		});


		// 미리보기
		$(".list .btn-preview").click(function() {
			var NewWindow = window.open($(this).attr('data-url'), 'PREVIEWWIN_'+$(this).attr('data-id'), 'width=' + $(this).attr('data-width') + ',height='+ $(this).attr('data-height') + ',toolbar=No,location=No,scrollbars=no,status=No,resizable=yes');
			NewWindow.focus();
		});


		// 콘텐츠 선택
		$(".list .btn-select").click(function() {

			name = $(this).attr('data-name');

			// 제목이 설정되어 있는지 확인
			if (!$("#id_name").val()) {
				$("#id_name").val(name);
			}
			$("#id_econtentscontentsname").val(name);
			$(".mform input[name='econtentscontents']").val($(this).attr('data-id'));

			$("#id_popupwidth").val($(this).attr('data-width'));
			$("#id_popupheight").val($(this).attr('data-height'));

			management.modalHide();
		});


		// 검색 취소
		$(".list .btn-search-cancel").click(function() {
		    management.list();
			return false;
		});

		// 동영상 업로드 버튼 클릭
		$(".list .upload .btn-upload").click(function() {
			management.modalMove($(this).attr('href'));
			return false;
		});


		// 전체선택 / 선택취소
		$(".list .form-list input[name='allchecked']").click(function() {
			var checked = $(this).is(":checked");

			$(".list .form-list input[name='econtentid[]']").each(function() {
				if(!$(this).is(":disabled")) {
					this.checked = checked;
				}
			});
		});


		// 콘텐츠 삭제
		$(".list .form-list tfoot .btn-delete").click(function() {
			count = $(".list .form-list input[name='econtentid[]']:checked").length;
			if(count > 0) {
				if(confirm("<?= get_string('confirm_select_delete', $pluginname); ?>")) {
				    $.ajax({
						 url 	: management.actionurl
						,data	: $(".list .form-list").serialize()
						,success: function(data){
							if(data.code == mesg.success) {
								// 업로드가 완료되면 메인 화면으로 이동하기
							    management.list();
							} else {
								alert(data.msg);
							}
						}
					});

					return false;
				}
			} else {
				alert('<?php echo get_string('no_check_content', $pluginname); ?>');
				return false;
			}
		});
	});
</script>
<div class="list">
	<div class="pageinfo">

		<h3 class="name">
			<?= get_string('uploaded_contents', $pluginname); ?> <span class="count">(<?= number_format($totalCount); ?>)</span>
			<img src="<?= $OUTPUT->image_url('layer/refresh', $pluginname) ?>" alt="refresh" class="refresh" />
		</h3>

	</div>
	<div class="py-3">
    	<div class="upload float-none float-md-right pb-2 pb-md-0">
    		<a href="<?= $CFG->wwwroot.'/mod/econtents/_upload.php?'.$baseurl->get_query_string(); ?>" class="btn btn-info btn-upload">
    			<i class="fa fa-upload mr-1"></i>
    			<?= get_string('upload', $pluginname); ?>
    		</a>
    	</div>
    	<div class="econtents_search ">
    		<form name="search" action="_list.php" method="get" class="form-inline form-search">
    			<?php
    				if(is_siteadmin()) {		// 관리자인 경우에만 사용자 검색이 가능해야됨.
    					$selected = array();
    					$selected['subject'] = ($keyfield == 'subject') ? 'selected="selected"' : '';
    					$selected['fullname'] = ($keyfield == 'fullname') ? 'selected="selected"' : '';
    					$selected['idnumber'] = ($keyfield == 'idnumber') ? 'selected="selected"' : '';

    					echo '<select name="keyfield" class="form-control mb-2 mb-md-0">';
    					echo '	<option value="subject" '.$selected['subject'].'>'.get_string('subject', $pluginname).'</option>';
    					echo '	<option value="fullname" '.$selected['fullname'].'>'.get_string('fullnameuser').'</option>';
    					echo '	<option value="idnumber" '.$selected['idnumber'].'>'.get_string('idnumber').'</option>';
    					echo '</select>';
    				}
    			?>
    			<input type="text" name="keyword" class="form-control required mb-2 mb-md-0" placeholder="<?= get_string('search'); ?>" value="<?= $keyword; ?>" />

    			<button type="submit" class="btn btn-default mb-2 mb-md-0"><?= get_string('search'); ?></button>
    			<?php
    			// 검색어가 존재하는 경우
    			if (!empty($keyword)) {
    				echo '<button type="button" class="btn btn-default btn-search-cancel mb-2 mb-md-0">'.get_string('search_cancel', 'local_ubion').'</button>';
    			}
    			?>

    		</form>
    	</div>
	</div>

	<form action="action.php" method="post" class="form-list">
		<div class="table-responsive">
    		<table class="table table-bordered table-striped table-coursemos">
    			<colgroup>
    				<col class="wp-40" />
    				<col class="wp-50" />
    				<col />
    				<col class="wp-80" />
    				<col class="wp-160" />
    				<col class="wp-90" />
    				<col class="wp-70" />
    			</colgroup>
    			<thead>
    				<tr>
    					<th><input type="checkbox" name="allchecked" /></th>
    					<th><?= get_string('number', 'local_ubion'); ?></th>
    					<th><?= get_string('contentname', $pluginname); ?></th>
    					<th><?= get_string('registuser', $pluginname); ?></th>
    					<th><?= get_string('registdate', $pluginname); ?></th>
    					<th><?= get_string('preview', $pluginname); ?></th>
    					<th><?= get_string('select', $pluginname); ?></th>
    				</tr>
    			</thead>
    			<tbody>
    			<?php
    			if ($totalCount > 0) {
    			    $number = $totalCount - (($page-1) * $ls);

    			    $symbolic = $CFG->wwwroot.'/'.$CEContents->getSymbolicFolderName();  //symbolic link : 수동을 생성 필요
    			    foreach ($lists as $l) {
    			        echo '<tr>';
    			        echo     '<td class="text-center">';
    			        echo         '<input type="checkbox" name="econtentid[]" value="'.$l->id.'" /></td>';
    			        echo     '</td>';

    			        echo     '<td class="text-center">'.number_format($number).'</td>';

    			        echo     '<td>';
    			        echo         '<a href="'.$CFG->wwwroot.'/mod/econtents/_upload.php?id='.$l->id.'" class="a-link">';
    			        echo             $l->contentname;
    			        echo         '</a>';
    			        if (!empty($l->description)) {
    			            echo "<br/><span class='description'>(".nl2br($l->description).")</span>";
    			        }
    			        echo 	'</td>';

    			        echo    '<td class="text-center">'.fullname($l).'</td>';
    					echo    '<td class="text-center">'.Common::getUserDate($l->timecreated).'</td>';


    					// 미리보기
    					$url = $l->contenturl;
    					if (!empty($l->filename)) {
    					   $url = $symbolic.'/'.$l->id.'/'.$l->contenturl;
    					}

    					echo    '<td class="text-center">';
    					echo       '<button type="button" class="btn btn-sm btn-info btn-preview" data-id="'.$l->id.'" data-url="'.$url.'" data-width="'.$l->popupwidth.'" data-height="'.$l->popupheight.'">';
    					echo           $i8n->preview;
    					echo       '</button>';
    					echo    '</td>';

    					// 선택
    					echo    '<td class="text-center">';
    					echo       '<button type="button" class="btn btn-sm btn-success btn-select" data-id="'.$l->id.'" data-name="'.$l->contentname.'" data-width="'.$l->popupwidth.'" data-height="'.$l->popupheight.'">';
    					echo           $i8n->select;
    					echo       '</button>';
    					echo    '</td>';
    			        echo '</tr>';

    			        $number--;
    			    }

    			} else {
    			    echo '<tr><td colspan="7">'.get_string('no_registered_econtents', $pluginname).'</td></tr>';
    			}
    			?>
    			</tbody>
    			<tfoot>
    				<tr>
    					<td colspan="7">
    						<input type="hidden" name="<?= $CEContents::TYPENAME; ?>" value="checkDelete" />
    						<input type="hidden" name="sesskey" value="<?= sesskey(); ?>" />
    						<button type="button" class="btn btn-danger btn-sm btn-delete"><?= get_string('delete'); ?></button>
    					</td>
    				</tr>
    			</tfoot>
    		</table>
		</div>
	</form>
	<?php
	// 페이징 출력
	echo Paging::printHTML($totalCount, $page, $ls);
	?>
</div>