<?php

use \local_ubion\base\Javascript;

require('../../config.php');
require_once $CFG->dirroot.'/course/lib.php';
require_once $CFG->libdir . '/completionlib.php';


$pluginname = 'econtents';

$CCourse = \local_ubion\course\Course::getInstance();

$id = optional_param('id', 0, PARAM_INT);  // Course module ID

if (!$cm = get_coursemodule_from_id('econtents', $id)) {
    print_error('invalidcoursemodule');
}

$econtents = $DB->get_record_sql('SELECT * FROM {econtents} WHERE id = :id', array('id'=>$cm->instance));

$course = $CCourse->getCourse($cm->course);
$context = context_module::instance($cm->id);

require_course_login($course, true, $cm);

$PAGE->set_context($context);
$PAGE->set_url('/mod/econtents/viewer.php', array('id' => $cm->id));
$PAGE->set_title($course->shortname . ' : ' . format_string($econtents->name));
$PAGE->set_heading($course->fullname);
$PAGE->set_pagelayout('popup');

// 헤더 영역 표시하면 안됨.
$PAGE->add_body_class('no-header');

// 교수자 여부 (교수자이면 진도체크 처리 하지 않음)
$isProfessor = $CCourse->isProfessor($course->id);
// 열람제한인 경우 교수자가 아니면 error화면이 표시되어야 함.
if (empty($econtents->restricted)) {
    if (! $isProfessor) {
        Javascript::getAlertClose(get_string('restrict_message', $pluginname));
    }
}

$CEContents = new \mod_econtents\EContents();

$econtentsContents = $CEContents->getContentView($econtents->econtentscontents);

if (empty($econtents) || empty($econtentsContents)) {
    Javascript::getAlertClose(get_string('error_not_exists', $pluginname));
}

// estream인 경우에는 X-UA-Compatible, IE=edge 로 선언이 되면 안됨.
$ieCompatible = '';
//$ieCompatible = '<meta http-equiv="X-UA-Compatible" content="IE=7" />';
if ($econtentsContents->isestream) {
    // estream인경우에는 모바일에서는 뷰어를 볼수가 없음 (activex기반임)
    if (core_useragent::get_device_type() != 'default') {
        Javascript::getAlertClose(get_string('notview_estream_mobile', $pluginname));

    } else {
        // estream은 ie 전용 (activex)
        if (!core_useragent::is_ie()) {
            Javascript::getAlertClose(get_string('notview_estream_onlyie', $pluginname));
        }
    }

    $ieCompatible = '<meta http-equiv="X-UA-Compatible" content="IE=7" />';
}


// ie Compatible 메타 태그를 변경해야되는 경우
if (!empty($ieCompatible)) {
    if (isset($CFG->additionalhtmlhead)) {
        $CFG->additionalhtmlhead .=  "\n".$ieCompatible;
    } else {
        $CFG->additionalhtmlhead = $ieCompatible;
    }
}


// 이러닝 콘텐츠 주소
$url = $econtentsContents->contenturl;
if (!empty($econtentsContents->filename)) {
    $symbolic = $CFG->wwwroot.'/'.$CEContents->getSymbolicFolderName();  //symbolic link : 수동을 생성 필요
    $url = $symbolic.'/'.$econtentsContents->id.'/'.$econtentsContents->contenturl;
}


// 진도처리기간 정보 가져오기
$progressInfo = $CEContents::getProgressPeriod($econtents, $cm->section);

// 진도체크 기간 확인
$CProgress = new \local_ubonattend\Progress($econtents->course);
$isProgressPeriodCheck = $CProgress->isProgressPeriodCheck($progressInfo->timeopen, $progressInfo->timelateness);

// 교수자가 아닌 경우 진도체크 여부를 확인해봐야됨.
if (! $isProfessor && $progressInfo->progress) {
    
    // 기본 1분
    // 재생시간이 5분보다 작다면 30초 단위로 호출되어야 함.
    // 1초 : 1000
    $intervalSecond = $CProgress::getIntervalTime($econtents->playtime);
    
    // 동영상이 진도처리 기능을 사용하는지 체크
    $track = $CEContents->getProgressTrack('econtents', $econtents);
    
    
    $PAGE->requires->strings_for_js(array(
        'track_window_close'
        ,'no_progress_period'
    ), $pluginname);
    
    $PAGE->requires->js_call_amd('mod_econtents/progress', 'progress', array(
        'isProgressPeriodCheck'    => $isProgressPeriodCheck
        ,'courseid'                 => $course->id
        ,'cmid'                     => $cm->id
        ,'intervalSecond'           => $intervalSecond
    ));
}

// 이벤트
$CEContents->setViewEvent($course, $cm, $context, false);

$ctx = new stdClass();
$ctx->name = $econtents->name;
$ctx->period = get_string('progress_check_period', $pluginname).' : '.$CEContents->getProgressPeriodText($econtents, $cm->section);
$ctx->url = $url;

echo $OUTPUT->header();
echo $OUTPUT->render_from_template("mod_econtents/viewer", $ctx);
echo $OUTPUT->footer();