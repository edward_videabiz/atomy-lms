define(['jquery', 'theme_coursemos/bootstrap'], function($, bootstrap) {

    var progress = {};
    
    progress.isLogin = true;
    progress.progressTagID = null
    progress.actionurl = M.cfg.wwwroot + '/mod/econtents/action.php';
    
    progress.progress = function(isProgressPeriodCheck, courseid, cmid, intervalSecond) {
        
        
        var intervalProgress = 0;
        var playerIntervalID = null;
        
        // 창닫기
        $("#viewer .viewer-header .btn-close").click(function() {
            // 약간의 딜레이를 줘야 이벤트가 기록됨
            setTimeout(function() {
                self.close();    
            }, 500);
             
        });
        
        
        $(window).bind('beforeunload', function() {
            
            
            // 일시정지와 창 종료 순서가 변경되서 전달되는 경우가 있기 때문에 아주 약간의 딜레이를 둠
            progress.ajax(courseid, cmid, '99', intervalProgress, intervalProgress);    
            
            
            alertMessage = M.util.get_string('track_window_close', 'econtents');

            if(navigator.userAgent.indexOf('AppleWebKit') !== -1 || navigator.userAgent.indexOf('Firefox') !== -1) {
                return alertMessage;
            } else {
                alert(alertMessage);
            }
        });
        
        
        // econtents는 따로 position값이 없기 때문에 1초마다 강제로 position값 변경
        // 참고로 econtents는 뷰어 열람시간으로 체크하기 때문에 position값은 의미가 없으며, 입력형태를 맞추기 위해서 해당값을 사용함. (즉, 무의미한값임)
        var t = setInterval(function() {
            intervalProgress += 1;
        }, 1000);
        
        
        playerIntervalID = setInterval(function() {
            // console.log('interval : ' + intervalProgress);
            progress.ajax(courseid, cmid, '8', intervalProgress, intervalProgress);
        }, intervalSecond);
        
        
        if (! isProgressPeriodCheck) {
            progress.alertMessage('alert-danger', M.util.get_string('no_progress_period', 'econtents'));
        }
        
        // 딜레이를 주지 않으면 1번 항목이 기록되지 않음
        setTimeout(function() {
         // 뷰어 열람 기록 남김
            progress.ajax(courseid, cmid, 1, 0, 0);
        }, 500);
        
    }
    
    
    progress.alertMessage = function(className, message, timeout) {
        
        timeout = typeof timeout !== 'undefined' ? timeout : 5000;
        
        el = $(".viewer-content .alert-message");
        
        el.removeClass('alert-success alert-info alert-warning alert-danger').addClass(className).html(message).show();
        
        elSetTimeout = setTimeout(function() {
            el.hide();
        }, timeout);
        
        el.click(function() {
           $(this).hide();
           
           clearTimeout(elSetTimeout);
        });
    }
    
    
    progress.ajax = function(courseid, cmid, state, positionfrom, positionto) {
        
        if (cmid > 0 && progress.isLogin) {
            
            param = {
                    courseid : courseid
                    ,cmid : cmid
                    ,state : state
                    ,positionfrom : positionfrom
                    ,positionto : positionto
            };
            
            param[coursemostype] = 'trackDetail';
            
            $.ajax( {
                url: progress.actionurl
               ,data: param
               ,cache : false
               ,type: "post"
               ,success: function(data) {
                   
                   if(data.code == mesg.failure) {
                        alert(data.msg);
                        return false;
                   } else if (data.code == mesg.failureNotLogin) {
                       
                        alert(data.msg);
                        
                        if (opener) {
                            self.close();
                            opener.document.location.href = opener.document.location.href;
                        } else {
                            location.href = location.href;
                        }
                   }
               }
               ,error : function(xhr, msg) {
                   if(xhr.responseText) {
                       alert(msg + " : " + xhr.responseText);
                   }
               }
           });
        }
    }
    
    
    return progress;
});