define(['jquery', 'theme_coursemos/bootstrap'], function($) {
    var management = {};

    management.moduleurl = M.cfg.wwwroot + '/mod/econtents';
    management.actionurl = management.moduleurl + '/action.php';
    management.containerid = 'econtents_container';

    management.mform = function() {

        management.modal();


        $("#id_selectcontentbtn").click(function() {
            management.list();
        });


        // 성적 항목 추가를 예로 설정하면 자동 성적반영도 예로 선택되게 설정함.
        // 2014-08-25 by akddd
        $("#id_add_grade_item").change(function() {
            if ($(this).val() == 1) {
                $("#id_autograde").val(1);
            } else {
                $("#id_autograde").val(0);
            }
        });
    };


    management.list = function() {
        $('#' + management.containerid).modal('show');
        $('#' + management.containerid + ' .modal-body').load(management.moduleurl + '/_list.php');
    };


    management.modal = function() {

        // Modal 태그 존재여부 확인, 없다면 추가해줘야됨.
        if ($('#' + management.containerid).length == 0) {

            var container = $('<div id="' + management.containerid + '" class="modal fade" tabindex="-1" role="dialog"></div>');
            var dialog = $('<div class="modal-dialog modal-lg" role="document"></div>');
            var content = $('<div class="modal-content"></div>');

            var header = $('<div id="econtents-hd" class="modal-header"></div>');
            header.append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
            header.append('<h4 class="modal-title" id="myModalLabel">' + M.util.get_string('select', 'econtents') + '</h4>');

            var body = $('<div id="econtents-bd" class="modal-body">Loading...</div>');

            content.append(header).append(body);
            dialog.append(content);
            container.append(dialog);

            $('body').append(container);
        }
    };

    management.modalMove = function(href) {
        $('#' + management.containerid + ' .modal-body').load(href);
    };

    management.modalHide = function() {
        $('#' + management.containerid).modal('hide');
    };

    return management;
});