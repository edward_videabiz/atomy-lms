<?php

require_once __DIR__.'/../../config.php';

use local_ubion\controller\Controller;
$type 		= optional_param(Controller::TYPENAME, null, PARAM_ALPHAEXT);

$CVod = new \mod_econtents\EContents();

if ($CVod->isSubmit($type)) {
    $CVod->invoke($type);
}