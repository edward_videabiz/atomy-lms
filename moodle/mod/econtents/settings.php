<?php
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    
    $pluginname = 'econtents';
    
    require_once("$CFG->libdir/resourcelib.php");
    
    // econtents는 ie전용인경우와, X-UA-Compatible을 강제로 변경해줘야되는 경우가 있기 때문에 embeded는 사용 불가능합니다.
    $displayoptions = resourcelib_get_displayoptions(array(RESOURCELIB_DISPLAY_POPUP));
    $defaultdisplayoptions = array(RESOURCELIB_DISPLAY_POPUP);
    
    // moduleid
    $name = $pluginname.'/moduleid';
    $title = get_string('setting_moduleid', $pluginname);
    $description = get_string('setting_moduleid_desc', $pluginname);
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT, 5);
    $settings->add($setting);
    
    
    ####################
    ## econtents 설정 ##
    ####################
    $settings->add(new admin_setting_heading('modeditdefaults', get_string('modeditdefaults', 'admin'), get_string('condifmodeditdefaults', 'admin')));
    
    
    // 설명표시
    $name = $pluginname.'/printintro';
    $title = get_string('printintro', $pluginname);
    $description = get_string('printintroexplain', $pluginname);
    $default = 0;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
    
    
    // 공개여부
    $name = $pluginname.'/ispublic';
    $title = get_string('ispublic', $pluginname);
    $description = get_string('ispublic_desc_setting', $pluginname);
    $default = 1;
    $options = array(
        '1' => get_string('public', $pluginname)
        ,'0' => get_string('notpublic', $pluginname)
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $settings->add($setting);
    
    
    
    // 기본 학습시간 (playtime값이 따로 존재하지 않을 경우 사용됨)
    $name = $pluginname.'/default_playtime';
    $title = get_string('setting_default_playtime', $pluginname);
    $description = get_string('setting_default_playtime_desc', $pluginname);
    $default = 10;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT, 5);
    $settings->add($setting);
    
    // 재생 시간 표시여부
    $name = $pluginname.'/showplaytime';
    $title = get_string('showplaytime', $pluginname);
    $description = get_string('showplaytime_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
    
    
    // 가능한 표시 옵션
    $name = $pluginname.'/displayoptions';
    $title = get_string('displayoptions', $pluginname);
    $description = get_string('configdisplayoptions', $pluginname);
    $default = $defaultdisplayoptions;
    $options = $displayoptions;
    $setting = new admin_setting_configmultiselect($name, $title, $description, $default, $options);
    $settings->add($setting);
    
    
    // 표시
    $name = $pluginname.'/display';
    $title = get_string('displayselect', $pluginname);
    $description = get_string('displayselectexplain', $pluginname);
    $default = RESOURCELIB_DISPLAY_POPUP;
    $options = $displayoptions;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $settings->add($setting);
    
    
    // 팝업창 가로 크기
    $name = $pluginname.'/popupwidth';
    $title = get_string('popupwidth', $pluginname);
    $description = get_string('popupwidthexplain', $pluginname);
    $default = 1005;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT, 7);
    $settings->add($setting);
    
    
    // 팝업창 세로 크기
    $name = $pluginname.'/popupheight';
    $title = get_string('popupheight', $pluginname);
    $description = get_string('popupheightexplain', $pluginname);
    $default = 755;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT, 7);
    $settings->add($setting);
    
    
    // 팝업창 중복 여부
    $name = $pluginname.'/same_window';
    $title = get_string('setting_same_window', $pluginname);
    $description = get_string('setting_same_window_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
    
    
    ###############
    ## 진도 설정 ##
    ###############
    $settings->add(new admin_setting_heading('progress', get_string('setting_progress', $pluginname), null));
    
    
    //--- 진도율 처리 여부(출석) -----------------------------------------------------------------------------------
    $name = $pluginname.'/completionprogress';
    $title = get_string('completion_progress', $pluginname);
    $description = get_string('completion_progress_description', $pluginname);
    $default = 90;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT, 7);
    $settings->add($setting);
    
    
    //--- 진도율 처리 여부(지각) -----------------------------------------------------------------------------------
    $name = $pluginname.'/tardinessprogress';
    $title = get_string('tardiness_progress', $pluginname);
    $description = get_string('tardinessprogress_description', $pluginname);
    $default = 50;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT, 7);
    $settings->add($setting);
    
    
    //--- grade item 추가 여부 -----------------------------------------------------------------------------------
    $name = $pluginname.'/grade';
    $title = get_string('setting_grade', $pluginname);
    $description = get_string('setting_grade_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
    
    
}
