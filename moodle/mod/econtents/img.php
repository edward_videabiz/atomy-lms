<?php
require_once '../../config.php';

$id = (isset($_GET['id'])) ? $_GET['id'] : '';

if (empty($id)) {
    exit;
}

$id = base64_decode($id);


// 파일 경로
$imgFile = \mod_econtents\EContents::getEContentsThumbnail($id);
if (!is_file($imgFile)) {
    exit;
}


$imgInfo = getimagesize($imgFile);
if( empty($imgInfo) ){
    return false;
}

if (empty($imgInfo[2]) || $imgInfo[2] < 1 || $imgInfo[2] >16) {
    exit;
}

$filesize = filesize($imgFile);

$pathInfo = pathinfo($imgFile);

Header('Content-type: '.mime_content_type($imgFile));
Header('Content-Disposition: inline; filename="'.$pathInfo['basename'].'"');
header('Content-Length: '.(string)($filesize));
header("content-description: php generated data");
header("pragma: no-cache");
header("expires: 0");

/*
 if( $fp = fopen($img, "rb") ){
 (while(!feof($fp)) {
 print( fread($fp, 1024*10 ) );
 //flush();
 }
 fpassthru($fp);
 fclose ($fp);
 }
 */

readfile($imgFile);