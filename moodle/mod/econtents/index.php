<?php
require('../../config.php');
require_once $CFG->dirroot.'/course/lib.php';
require_once $CFG->libdir.'/resourcelib.php';

$id = required_param('id', PARAM_INT); // course id

$course_format = course_get_format($id);
$course = $course_format->get_course(); // Needed to have numsections property available.

require_course_login($course, true);
$PAGE->set_pagelayout('incourse');

$params = array(
    'context' => context_course::instance($course->id)
);
$event = \mod_econtents\event\course_module_instance_list_viewed::create($params);
$event->add_record_snapshot('course', $course);
$event->trigger();

$moduleName     = get_string('pluginname', 'econtents');
$moduleNames    = get_string('modulenameplural', 'econtents');
$strsectionname  = get_string('sectionname', 'format_'.$course->format);
$strname         = get_string('name');
$strintro        = get_string('moduleintro');
$strlastmodified = get_string('lastmodified');

$PAGE->set_url('/mod/econtents/index.php', array('id' => $course->id));
$PAGE->set_title($course->shortname.': '.$moduleNames);
$PAGE->set_heading($course->fullname);
// $PAGE->navbar->add($moduleNames, $PAGE->url);

echo $OUTPUT->header();
echo $OUTPUT->heading($moduleNames);

if ($econtentss = get_all_instances_in_course('econtents', $course)) {
    $usesections = course_format_uses_sections($course->format);
    
    $table = new html_table();
    $table->attributes['class'] = 'generaltable mod_index';
    
    if ($usesections) {
        $table->head  = array ($strsectionname, $strname, $strintro);
        $table->align = array ('center', 'left', 'left');
    } else {
        $table->head  = array ($strlastmodified, $strname, $strintro);
        $table->align = array ('left', 'left', 'left');
    }
    
    $CEContents = new \mod_econtents\EContents();
    $config = get_config('econtents');
    $modinfo = get_fast_modinfo($course);
    $currentsection = '';
    foreach ($econtentss as $econtents) {
        $cm = $modinfo->cms[$econtents->coursemodule];
        if ($usesections) {
            $printsection = '';
            if ($econtents->section !== $currentsection) {
                if ($econtents->section) {
                    $printsection = get_section_name($course, $econtents->section);
                }
                if ($currentsection !== '') {
                    $table->data[] = 'hr';
                }
                $currentsection = $econtents->section;
            }
        } else {
            $printsection = '<span class="smallinfo">'.userdate($econtents->timemodified)."</span>";
        }
        
        $extra = empty($cm->extra) ? '' : $cm->extra;
        $icon = $cm->get_icon_url();
        if (!empty($icon)) {
            $icon = '<img src="'.$OUTPUT->image_url($icon).'" class="activityicon mr-1" alt="'.get_string('modulename', $cm->modname).'" /> ';
        }
        
        
        $fullurl = $CFG->wwwroot.'/mod/econtents/view.php?id='.$cm->id;
        $onclick = '';
        
        if ($econtents->display == RESOURCELIB_DISPLAY_POPUP) {
            
            $popupName = $CEContents::getPopupName();
            $popupOption = $CEContents::getPopupOption($econtents->popupwidth, $econtents->popupheight);
            
            
            $onclick = "onclick=\"window.open('$fullurl', '$popupName', '$popupOption'); return false;\"";
        }
        
        $class = $econtents->visible ? '' : 'class="dimmed"'; // hidden modules are dimmed
        
        $table->data[] = array (
            $printsection,
            "<a $class $extra href=\"view.php?id=$cm->id\" $onclick>".$icon.format_string($econtents->name)."</a>",
            format_module_intro('econtents', $econtents, $cm->id));
    }
    
    echo html_writer::table($table);
} else {
    $message = '<p>'.get_string('thereareno', 'moodle', $moduleName).'</p>';
    $message .= '<p><a href="'.$CFG->wwwroot.'/course/view.php?id='.$id.'" class="btn btn-default">'.get_string('continue').'</a></p>';
    echo $OUTPUT->notification($message);
}
echo $OUTPUT->footer();
