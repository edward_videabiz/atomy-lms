<?php

function xmldb_econtents_upgrade($oldversion) {
	global $CFG, $DB, $OUTPUT;

	$dbman = $DB->get_manager();

	if ($oldversion < 2018073001) {
	    
	    // Define field progress_type to be dropped from econtents.
	    $table = new xmldb_table('econtents');
	    $field = new xmldb_field('progress_type');
	    
	    // Conditionally launch drop field progress_type.
	    if ($dbman->field_exists($table, $field)) {
	        $dbman->drop_field($table, $field);
	    }
	    
	    // Econtents savepoint reached.
	    upgrade_mod_savepoint(true, 2018073001, 'econtents');
	}
	
	
	if ($oldversion < 2018073002) {
	    
	    // Define index contenttype (not unique) to be dropped form econtents_contents.
	    $table = new xmldb_table('econtents_contents');
	    
	    $index = new xmldb_index('contenttype', XMLDB_INDEX_NOTUNIQUE, array('contenttype'));
	    if ($dbman->index_exists($table, $index)) {
	        $dbman->drop_index($table, $index);
	    }
	    
	    
	    $field = new xmldb_field('contenttype');
	    if ($dbman->field_exists($table, $field)) {
	        $dbman->drop_field($table, $field);
	    }
	    
	    
	    $field = new xmldb_field('filepath');
	    if ($dbman->field_exists($table, $field)) {
	        $dbman->drop_field($table, $field);
	    }
	    
	    // Econtents savepoint reached.
	    upgrade_mod_savepoint(true, 2018073002, 'econtents');
	}
	
	
	
	if ($oldversion < 2018073003) {
	    
	    // Define field display to be added to econtents.
	    $table = new xmldb_table('econtents');
	    $field = new xmldb_field('display', XMLDB_TYPE_INTEGER, '4', null, null, null, '0', 'econtentscontents');
	    
	    // Conditionally launch add field display.
	    if (!$dbman->field_exists($table, $field)) {
	        $dbman->add_field($table, $field);
	    }
	    
	    // Econtents savepoint reached.
	    upgrade_mod_savepoint(true, 2018073003, 'econtents');
	}
	
}