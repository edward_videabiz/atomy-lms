<?php
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2018073003;       // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2017111301;       // Requires this Moodle version
$plugin->component = 'mod_econtents';      // Full name of the plugin (used for diagnostics)
$plugin->dependencies = array('local_ubion' => 2014070101);