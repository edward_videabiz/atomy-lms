<?php
defined('MOODLE_INTERNAL') || die;

require_once $CFG->dirroot.'/course/moodleform_mod.php';
require_once $CFG->libdir.'/resourcelib.php';

class mod_econtents_mod_form extends moodleform_mod {
    
    function definition() {
        
        global $CFG, $DB, $COURSE, $PAGE, $USER;
        $mform = $this->_form;

        $pluginname = 'econtents';
        $config = get_config($pluginname);        
        
        $CEContents = new \mod_econtents\EContents();
        
        $config = $CEContents->getConfig();
        
        $courseid = $this->current->course;
        $CCourse = \local_ubion\course\Course::getInstance();
        $courseUbion = $CCourse->getUbion($courseid);
        
        // 강좌내에서 진도처리 기능을 사용하는지 확인
        $isProgress = $courseUbion->progress;
        
        // 지각 기능을 사용하는지 확인
        $isLate = false;
        
        // 성적 항목 추가
        $isAddGradeItem = $config->grade;
        
        $COnAttendance = \local_ubonattend\Attendance::getInstance($courseid);
        $isOnlineAttendance = $COnAttendance->isAttendanceCourse();
        $settingDesc = '';
        
        
        // 온라인 출석부를 사용하는 경우 미리 사용할 값들 설정
        if ($isOnlineAttendance) {
            $isLate = $COnAttendance->isLateCourse();
            
            $settingURL = $CFG->wwwroot.'/local/ubonattend/setting.php?id='.$courseid;
            $settingButton = '<a href="'.$settingURL.'" target="_blank" class="btn btn-sm btn-default">'.get_string('onlineattendance', 'local_ubonattend').'</a>';
            $settingDesc = '<span class="text-info">'.get_string('notchange_setting', 'local_ubonattend', $settingButton).'</span>';
            
            // 해당 주차에 설정된 진도처리 일자 가져오기
            $modinfo = get_fast_modinfo($courseid);
            $sectionInfo = $modinfo->get_section_info($this->_section);
            
            if ($onlineAttendanceSection = $COnAttendance->getSection($sectionInfo->id)) {
                
                // 온라인출석부를 사용하는 경우, 온라인 출석부 비율을 가져옴
                $config->completionprogress = isset($onlineAttendanceSection->attendance_percent) ? $onlineAttendanceSection->attendance_percent : $config->completionprogress ;
                $config->tardinessprogress = isset($onlineAttendanceSection->lateness_percent) ? $onlineAttendanceSection->lateness_percent : $config->tardinessprogress;
            }
        }
        
        $strrequired = get_string('required');
        
        #####################
        ##### 기본 항목 #####
        #####################
        $mform->addElement('header', 'general', get_string('general', 'form'));
        
        // 제목
        $mform->addElement('text', 'name', get_string('name'), array('size'=>'54'));
        $mform->addRule('name', $strrequired, 'required', null, 'client');
        $mform->setType('name', PARAM_NOTAGS);
        
        // 설명
        $this->standard_intro_elements(get_string('intro', $pluginname));
        
        
        ##########################
        ##### econtents 선택 #####
        ##########################
        $mform->addElement('header', 'select', get_string('select_contents', $pluginname));
        $mform->setExpanded('select');
        
        
        // 전용 스크립트 호출
        $PAGE->requires->js_call_amd('mod_econtents/management', 'mform', array());
        $PAGE->requires->strings_for_js(array(
            'select'
        ), $pluginname);
        
        
        // 콘텐츠 사용 객체 등록
        $ctlarray = array();
        $ctlarrayRules = array();
        
        // text박스
        $ctlarray[] = $mform->createElement('text', 'econtentscontentsname', '', 'maxlength="254" size="30" readonly="readonly"');
        $ctlarrayRules['econtentscontentsname'][] = array($strrequired, 'required', null, 'client');
        
        // 콘텐츠 선택 버튼
        $ctlarray[] = $mform->createElement('button', 'selectcontentbtn', get_string('select', $pluginname), 'value="1"');
        $ctlarray[] = $mform->createElement('hidden', 'econtentscontents');
        $ctlarrayRules['econtentscontents'][] = array($strrequired, 'required', null, 'client');
        
        $mform->addGroup($ctlarray, 'contentselector',  get_string('econtentscontents', $pluginname), '', '');
        
        
        $mform->addGroupRule('contentselector', $ctlarrayRules);
        $mform->setType('econtentscontentsname', PARAM_NOTAGS);
        $mform->setType('econtentscontents', PARAM_INT);
        
        $econtentsid = $this->current->econtentscontents ?? null;
        if (!empty($econtentsid)) {
            $contentname = $DB->get_field_sql('SELECT contentname FROM {econtents_contents} WHERE id = :id', array('id'=>$econtentsid));
            $mform->setDefault('econtentscontentsname', $contentname);
        }

        
        // 열람제한 (진도처리기간과 상관 없이 뷰어 열람 제한기능)
        $restrictOptions = array();
        $restrictOptions[0] = get_string('restrict', $pluginname);
        $restrictOptions[1] = get_string('view', $pluginname);
        
        $mform->addElement('select', 'restricted', get_string('view_restrict', $pluginname), $restrictOptions);
        $mform->addHelpButton('restricted', 'restricted', $pluginname);
        $mform->setDefault('restricted', 1);
        
        
        ##############################
        ##### 진도처리 기간 선택 #####
        ##############################
        $mform->addElement('header', 'progress_management', get_string('progress_management', $pluginname));
        $mform->setExpanded('progress_management');
        
        // 진도관리 사용여부
        $mform->addElement('selectyesno', 'progress',  get_string('progress_check', $pluginname));
        
        // 강좌 진도허용 여부에 따라서 폼 disabled 시키기..
        $mform->addElement('hidden', 'is_progress');
        $mform->setType('is_progress', PARAM_INT);
        
        // 온라인 출석부 사용여부에 따라서 폼 disabled 시키기
        $mform->addElement('hidden', 'is_online_attendance');
        $mform->setType('is_online_attendance', PARAM_INT);
        
        if ($isProgress) {
            $mform->setDefault('is_progress', 1);
            $mform->setDefault('progress', 1);
        } else {
            $mform->setDefault('is_progress', 0);
            $mform->setDefault('progress', 0);
        }
        
        
        // 온라인 출석체크 - 온라인 출석부를 사용하는 경우에만 표시
        if ($isOnlineAttendance) {
            $mform->addElement('selectyesno', 'useattend', get_string('useattend', $pluginname));
            $mform->setDefault('useattend', 1);
            
            // 진도체크도 사용으로 설정해줘야됨.
            $mform->setDefault('progress', 1);
        } else {
            $mform->addElement('hidden', 'useattend');
            $mform->setDefault('useattend', 0);
        }
        $mform->setType('useattend', PARAM_INT);
        
        
        // 온라인 출석부를 사용하는 경우
        if ($isOnlineAttendance) {
            // 진도처리도 무조건 예, 온라인 출석부 사용여부도 예
            $mform->setDefault('is_progress', 1);
            $mform->setDefault('is_online_attendance', 1);
            
            //출석(진도) 설정은 '온라인출석분설정'에서 변경 가능합니다.
            $mform->addElement('static', 'mform_online_desc', '', $settingDesc);
            
            $mform->setDefault('timeopen', $onlineAttendanceSection->time_start);
            
            // 지각 기능을 사용하는 경우
            if ($isLate) {
                $mform->setDefault('timeclose', $onlineAttendanceSection->time_lateness);
            } else {
                $mform->setDefault('timeclose', $onlineAttendanceSection->time_end);
            }
            
        } else {
            
            // 진도 처리 기간
            $usermidnight = usergetmidnight(time());
            $mform->addElement('date_time_selector', 'timeopen', get_string('timeopen', $pluginname), array('optional' => true, 'step' => 1));
            $mform->setDefault('timeopen', $usermidnight);
            
            $mform->addElement('date_time_selector', 'timeclose', get_string('timeclose', $pluginname), array('optional' => true, 'step' => 1));
            $mform->setDefault('timeclose', $usermidnight + WEEKSECS + DAYSECS - MINSECS);
            
        }
        
        
        
        
        // 진도처리를 진행하지 않는 강좌인 경우에는 비활성화
        $mform->disabledIf('progress', 'is_progress', 'eq', 0);
        $mform->disabledIf('timeopen', 'is_progress', 'eq', 0);
        $mform->disabledIf('timeclose', 'is_progress', 'eq', 0);
        
        
        // 온라인 출석부를 사용하는 강좌인 경우에는 진도처리기간 및 인정진도율 항목 비활성화
        // (온라인 출석부에서 설정이 되기 때문에 개별 동영상에 대해서는 수정이 불가능해야됨)
        $mform->disabledIf('timeopen', 'is_online_attendance', 'eq', 1);
        $mform->disabledIf('timeclose', 'is_online_attendance', 'eq', 1);
        $mform->disabledIf('completionprogress', 'is_online_attendance', 'eq', 1);
        $mform->disabledIf('tardinessprogress', 'is_online_attendance', 'eq', 1);
        
        
        // 진도처리 여부에 따라 활성화 여부가 결정되어야 함.
        $mform->disabledIf('timeopen', 'progress', 'eq', 0);
        $mform->disabledIf('timeclose', 'progress', 'eq', 0);
        $mform->disabledIf('useattend', 'progress', 'eq', 0);
        
        
        
        
        ################
        ##### 성적 #####
        ################
        if ($isAddGradeItem) {
            // 성적 부분 시작
            $this->standard_grading_coursemodule_elements();
            
            // 성적 항목 삭제
            $mform->removeElement('grade');
            $mform->addElement('select', 'grade', get_string('grade'), range(0, 100));
            //$mform->addElement('hidden', 'grade', 100);
            $mform->setType('grade', PARAM_NUMBER);
            
            // 성적 필수 항목 삭제
            if ($mform->elementExists('completionusegrade')) {
                $mform->removeElement('completionusegrade');
            }
            
            // gradeitem 추가
            $mform->addElement('selectyesno', 'add_grade_item', get_string('add_grade_item', $pluginname));
            $mform->disabledIf('grade', 'add_grade_item', 'eq', 0);
            $mform->disabledIf('gradecat', 'add_grade_item', 'eq', 0);
            $mform->disabledIf('autograde', 'add_grade_item', 'eq', 0);
        } else {
            $mform->addElement('hidden', 'add_grade_item');
            $mform->setDefault('add_grade_item', 0);
            
            $mform->addElement('header', 'modstandardgrade', get_string('grade'));
        }
        $mform->setType('add_grade_item', PARAM_INT);
        
        
        // 학습 인정 진도율
        $mform->addElement('text', 'completionprogress', get_string('completion_progress', $pluginname), array('maxlength'=>3, 'size'=>7));
        $mform->setType('completionprogress', PARAM_INT);
        $mform->setDefault('completionprogress', $config->completionprogress);
        
        // 지각기능 사용 - 지각 인정 진도율
        if ($isLate) {
            $mform->addElement('text', 'tardinessprogress', get_string('tardiness_progress', $pluginname), array('maxlength'=>3, 'size'=>7));
            $mform->setType('tardinessprogress', PARAM_INT);
            $mform->setDefault('tardinessprogress', $config->tardinessprogress);
        }
        
        if ($isOnlineAttendance) {
            //진도율 설정은 '온라인출석분설정'에서 변경 가능합니다.
            $mform->addElement('static', 'mform_online_progress_rate_desc', '', $settingDesc);
        }
        
        
        // 성적 항목 추가인 경우 자동 성적 반영 항목 추가
        if ($isAddGradeItem) {
            // 자동 성적 반영
            $mform->addElement('selectyesno', 'autograde', get_string('autograde', $pluginname));
        }
        
        
        #####################
        ##### 기타 설정 #####
        #####################
        $mform->addElement('header', 'options', get_string('optionsheader', $pluginname));
        $mform->setExpanded('options');
        
        
        if ($this->current->instance) {
            $options = resourcelib_get_displayoptions(explode(',', $config->displayoptions), $this->current->display);
        } else {
            $options = resourcelib_get_displayoptions(explode(',', $config->displayoptions));
        }
        
        // 화면 표시방법
        if (count($options) == 1) {
            $mform->addElement('hidden', 'display');
            $mform->setType('display', PARAM_INT);
            reset($options);
            $mform->setDefault('display', key($options));
        } else {
            $mform->addElement('select', 'display', get_string('displayselect', 'url'), $options);
            $mform->setDefault('display', $config->display);
        }
        
        // 팝업창 크기 지정
        if (array_key_exists(RESOURCELIB_DISPLAY_POPUP, $options)) {
            // width
            $mform->addElement('text', 'popupwidth', get_string('popupwidth', $pluginname), array('size'=>7));
            if (count($options) > 1) {
                $mform->disabledIf('popupwidth', 'display', 'noteq', RESOURCELIB_DISPLAY_POPUP);
            }
            $mform->setType('popupwidth', PARAM_INT);
            $mform->setDefault('popupwidth', $config->popupwidth);
            
            // height
            $mform->addElement('text', 'popupheight', get_string('popupheight', $pluginname), array('size'=>7));
            if (count($options) > 1) {
                $mform->disabledIf('popupheight', 'display', 'noteq', RESOURCELIB_DISPLAY_POPUP);
            }
            $mform->setType('popupheight', PARAM_INT);
            $mform->setDefault('popupheight', $config->popupheight);
        }
        
        // showplaytime
        $mform->addElement('checkbox', 'showplaytime', get_string('showplaytime', $pluginname));
        $mform->setDefault('showplaytime', $config->showplaytime);
        
        
        // 일반모듈 설정
        $this->standard_coursemodule_elements();
        
        // buttons
        $this->add_action_buttons();
    }

    /**
     * 진도관리 관련해서 추가로 조건을 추가하는 경우.
     *
     * @see moodleform_mod::add_completion_rules()
     */
	function add_completion_rules() {
	    $mform = & $this->_form;
	    
	    $mform->addElement('checkbox', 'completiontimeenabled', get_string('completion_condition', 'econtents'), get_string('completion_condition_desc', 'econtents'));
	    
	    return array('completiontimeenabled');
    }

    function completion_rule_enabled($data) {
        return (!empty($data['completiontimeenabled']));
    }

    
    function get_data() {
        $data = parent::get_data();
        if (!$data) {
            return false;
        }
        
        $autocompletion = !empty($data->completion) && $data->completion == COMPLETION_TRACKING_AUTOMATIC;
        
        // 수정할때 아래 조건을 비교하지 않으면 completiontimeenabled값이 변경이 되지 않음.
        if (empty($data->completiontimeenabled) || !$autocompletion) {
            $data->completiontimeenabled = 0;
        }
        
        if (empty($data->showplaytime)) {
            $data->showplaytime = 0;
        }
        
        return $data;
    }


    function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // 기간 체크
		if (!empty($data['timeopen']) && !empty($data['timeclose'])) {
            if ($data['timeopen'] > $data['timeclose']) {
                $errors['timeclose'] = get_string('timeclosevalidation', 'econtents', userdate($data['timeopen'], get_string('strftimedatetimeshort')));
            }
        }

        if (empty($data['econtentscontents'])) {
            $errors['econtentscontents'] = get_string('required');
        }
        
        return $errors;
    }

    function definition_after_data() {
        global $PAGE, $DB;
        // 성적 추가를 사용하지 않은 상태로 저장을 하는 경우 성적 카테고리가 없다보니, 다음 동영상 수정 페이지 접근시 성적 카테고리 element가 표시되지 않음.
        // vod는 outcome을 사용하지 않기 때문에 임시로 $this->_features->gradecat값을 false로 설정하고
        // parent::definition_after_data();
        // 실행 후 $this->_features->gradecat값을 원상 복귀 시킴
        $original_gradecat = $this->_features->gradecat;
        $this->_features->gradecat = false;
        parent::definition_after_data();
        $this->_features->gradecat = $original_gradecat;
        
        $mform = &$this->_form;
        
        // 성적값이 자동으로 셋팅되게 해줘야됨.
        $query = "SELECT grademax FROM {grade_items} WHERE courseid = :courseid AND itemmodule = :module AND iteminstance = :instance";
        if ($grade_max = $DB->get_field_sql($query, array('courseid'=>$this->current->course, 'module'=>'vod', 'instance'=>$this->current->id))) {
            
            $grade_max = (int)$grade_max;
            
            // $mform->setDefault는 사용하면 안됩니다. (이수가 초기화되지 않는 현상이 있음)
            // $mform->setDefault('grade', $grade_max);
            $mform->getElement('grade')->setValue($grade_max);
        }
    }
}
