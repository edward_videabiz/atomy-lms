<?php
require '_head.php';

$CCourse = \local_ubion\course\Course::getInstance();
?>
<script type="text/javascript">
	$(function() {
		$(".select_course").click(function() {
			if ($(this).is(':checked')) {
				$(this).closest('tr').find('select').removeAttr('disabled').focus();
			} else {
				$(this).closest('tr').find('select').attr('disabled', 'disabled');
			}
		});

		$("#share_confirm_form").submit(function() {
			select_course = $("#share_confirm_form input[type='checkbox']:checked").length;

			if (select_course > 0) {
				$.ajax({
				 	url: 'action.php'
					,data: $(this).serialize()
					,type:'post'
					,success:function(data){
						if (data.code == mesg.success) {
							alert(data.msg);
							location.reload();
						} else {
							alert(data.msg);
						}
					}
				});

				
			} else {
				alert('<?php echo get_string('share_no_select_course_board', 'ubboard'); ?>');
			}

			return false;
		});
	});
</script>

<?php
$id         = optional_param('id', 0, PARAM_INT);       		// Course Module ID
$bwid 		= optional_param('bwid', null, PARAM_INT);
$boardtype = optional_param('boardtype', null, PARAM_ALPHANUMEXT);

if ($ubboard->type == 'anonymous') {
    echo get_string('no_share_anonymous', 'ubboard');
    exit;
}

// 본인이 수강중인 강좌 목록을 가져와서 교수 및 조교권한을 가진 항목만 추려서 뿌려줘야됨.
$myCourses = $CCourse->getMyCourses();


echo '<form id="share_confirm_form" method="post">';
echo	'<input type="hidden" name="'.$CUBBoard::TYPENAME.'" value="share" />';
echo	'<input type="hidden" name="id" value="'.$id.'" />';
echo	'<input type="hidden" name="bwids" value="'.$bwid.'" />';
if ($myCourses) {
	echo '<p class="text-info">'.get_string('share_message', 'ubboard').'</p>';
	echo '<table class="table table-bordered table-coursemos">';
	echo	'<colgroup>';
	echo		'<col class="col-sm-7" />';
	echo		'<col class="col-sm-5" />';
	echo 	'</colgroup>';
	echo	'<thead>';
	echo		'<tr>';
	echo			'<th class="text-center">'.get_string('share_select_course', 'ubboard').'</th>';
	echo			'<th class="text-center">'.get_string('share_select_board', 'ubboard').'</th>';
	echo		'</tr>';
	echo	'</thead>';
	foreach ($myCourses as $c) {
		// 교수 및 조교권한이 존재해야됨.
		//if (local_ubion_course_is_professor($c->id) && $ubboard_class->getCourse()->id != $c->id) {
		// 2017-02-01 동일 강좌내 게시판에도 공유할수 있도록 조치함. by akddd
	    if ($CCourse->isProfessor($c->id)) {
			// 강좌가 기본 셋팅 되어 있는지 확인
			$disableClass = '';
			
			// 2015-06-01
			// session에 담긴 값은 강좌 설정이 변경이 된거에 대해서 반영이 되지 않기 때문에 DB에서 직접 조회하도록 변경
			if ($DB->get_field_sql("SELECT setting FROM {course_ubion} WHERE course = :course", array('course'=>$c->id)) < 1) {
				$disableClass = ' disabled="disabled"';
			}
			
			echo '<tr>';
			echo	'<td>';
			echo		'<div class="checkbox">';
			echo 			'<label>';
			echo 				'<input type="checkbox" class="select_course" name="select_course_'.$c->id.'" value="1" '.$disableClass.' /> ';
			echo				$CCourse->getName($c);
			echo			'</label>';
			echo		'</div>';
			echo 	'</td>';
			echo	'<td>';
			
			// 강좌가 셋팅 되어 있는지 확인
			if (empty($disableClass)) {
			    echo 	'<select name="select_board_'.$c->id.'" class="form-control form-control-board" disabled="disabled">';
				// 강좌에 등록되어 있는 게시판 목록 가져오기
				if ($ubboards = $DB->get_records_sql('SELECT id, name, type FROM {ubboard} WHERE course = :course', array('course'=>$c->id))) {
					$isFirstSelected = false;
					foreach ($ubboards as $board) {
						// 강좌내에 공유하려는 게시판의 type이 중복되는 경우 첫번째로 조회되는 게시판이 선택되도록 설정
						$selected = '';
						if ($board->type == $boardtype && !$isFirstSelected) {
							$selected = ' selected="selected"';
						}
						echo '<option value="'.$board->id.'" '.$selected.'>'.$board->name.'</option>';
					}
				} else {
					echo '<option value="">'.get_string('notfound_created_board', 'ubboard').'</option>';
				}
				echo 	'</select>';
			} else {
				echo	'<span class="text-error">'.get_string('share_no_setting', 'ubboard').'</span>';
			}
			echo 	'</td>';
			echo '</tr>';
			
		}
	}
	
	echo '</table>';
	
} else {
	echo '<p class="text-danger">'.get_string('no_mycourses', 'local_ubion').'</p>';
}


echo 	'<div class="modal-footer">';
echo 		'<input type="submit" class="btn btn-primary" value="'.get_string('btn_confirm', 'ubboard').'" />';
echo 		'<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">'.get_string('close', 'local_ubion').'</button>';
echo 	'</div>';
echo '</form>';