<?php

require_once __DIR__.'/_head.php';


list($totalcount, $articles, $categories) = $CUBBoard->getLists();

$lists = array();

// 게시판에 존재하는 파일 갯수
$filecount = 0;

if ($totalcount > 0) {
    list ($lists, $filecount) = $CUBBoard->_getLists($totalcount, $articles, $categories);
    
    // mustache에서는 key가 부여되어 있으면 안됨.
    $lists = array_values($lists);
}

// 공지사항
$notices = array();
// 모든 페이지에서 가져올 필요는 없고, 1페이지에서만 공지사항이 출력되어야 함.
// 검색이 아닌 경우 표시
if ($page <= 1) {
    $notices = $CUBBoard->getNoticeLists();

    list ($notices, $noticefilecount) = $CUBBoard->_getLists(count($notices), $notices, $categories);
    
    $filecount += $noticefilecount;
    $notices = array_values($notices);
}

// 파일 일괄 다운로드 관련 변수
$alldownload = new stdClass();
$alldownload->is = false;
$alldownload->url = $CFG->wwwroot.'/mod/ubboard/alldownload.php?id='.$cm->id;
$alldownload->title = get_string('alldownload', 'ubboard');


// 교수자 권한이 존재하는 경우에만 일괄 다운로드 버튼이 표시되어야 함.
if ($CUBBoard->isAllDownload()) {
    // 현재 리스트에 파일이 없다면 게시판 전체에 대해서 파일이 존재하는지 재검사 해봐야됨.
    if (empty($filecount)) {
        $alldownload->is = $CUBBoard->getFileCount();
    } else {
        // 현재 목록에 파일이 한개라도 존재하면 일괄다운로드 버튼이 표시되어야 함.
        $alldownload->is = true;
    }
}

// buttons
$buttons = new stdClass();
$buttons->list = $CUBBoard->setButton(get_string('btn_list', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/view.php?id='.$cm->id);
$buttons->write = $CUBBoard->setButton(get_string('btn_write', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/write.php?id='.$cm->id);
$buttons->category_manage = $CUBBoard->setButton(get_string('category_manage', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/category.php?id='.$cm->id);


// template로 넘길 변수 선언
$mustache = new stdClass();
$mustache->lists = $lists;
$mustache->notices = $notices;
$mustache->colspan = $CUBBoard->getColspan();
$mustache->icons = $CUBBoard->getIcons();
$mustache->categories = $CUBBoard->_getCategories($categories);
$mustache->paging = \local_ubion\base\Paging::getPaging($totalcount, $page, $ls);
$mustache->alldownload = $alldownload;
$mustache->buttons = $buttons;
$mustache->groupSelect = groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/ubboard/view.php?id=' . $cm->id, true);
$mustache->keyfieldOption = $CUBBoard->getKeyfieldOptions($keyfield);
$mustache->selectedButtons = $CUBBoard->getSelectedButtons();

// mustache 문법으로 바꿔줘야됨.
$listSizes = array();
foreach (\local_ubion\base\Parameter::getListSizes() as $lsKey => $lsValue) {
    $lss = new stdClass();
    $lss->key = $lsKey;
    $lss->value = $lsValue;
    $lss->selected = ($CUBBoard->getParameter('ls') == $lsKey) ? 'selected="selected"' : '';
    $listSizes[] = $lss;
}
$mustache->listSizes = $listSizes;

$event = \mod_ubboard\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $ubboard);
$event->trigger();

// 관리자 기능
if ($CUBBoard->isPermission('manage')) {
    $PAGE->requires->strings_for_js(array(
        'confirm_select_article_delete'
        ,'selected_noarticle'
        ,'confirm_select_article_open'
        ,'confirm_select_article_open_cancel'
    ), 'ubboard');
    
    $PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'manage');
}

// 카테고리를 사용하면 카테고리 관련된 스크립트를 로드해야됨.
if ($ubboard->category) {
    $PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'category', array(
        'id' => $cm->id
    ));
}


// 게시물 목록관련 스크립트
$PAGE->requires->strings_for_js(array(
    'alldownload_loading'
), 'ubboard');

$PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'list', array(
    'id' => $cm->id
    ,'defaultLS' => $CUBBoard->getDefaultListSize()
));


// Output starts here.
echo $OUTPUT->header();

echo $CUBBoard->renderer('lists', $mustache);
// Finish the page.
echo $OUTPUT->footer();
