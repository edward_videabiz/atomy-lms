define(['jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate', 'theme_coursemos/jquery.filedownload', 'theme_coursemos/jquery-sortable'], function($) {
	var ubboard = {};
	
	ubboard.admin_path = '.path-mod-ubboard ';
	ubboard.action = 'action.php';
	
	ubboard.manage = function() {
		
		// 체크박스
		$("#ubboard-list-form input[name='all_checked']").click(function() {
			var checked = $(this).is(":checked");
			
			$('#ubboard-list-form input[name="manage[]"]').each(function() {
				if(!$(this).is(":disabled")) {
					this.checked = checked;
				}
			});
		});
		
		
		// 다중 삭제
		$("#ubboard-list-form .btn-selected-delete").click(function() {
			
			// type값 변경
			checkCount = $('#ubboard-list-form input[name="manage[]"]:checked').length;
			
			if(checkCount > 0) {
				
				if(confirm(M.util.get_string('confirm_select_article_delete', 'ubboard', checkCount))) {
					$('#ubboard-list-form input[name="'+coursemostype+'"]').val('checkDelete');
					$("#ubboard-list-form").submit();
				}
			} else {
				alert(M.util.get_string('selected_noarticle', 'ubboard'));
			}
			
			return false;
		});

		
		// 공개 
		$("#ubboard-list-form .btn-selected-open").click(function() {

			// type값 변경
			checkCount = $('#ubboard-list-form input[name="manage[]"]:checked').length;
			
			if(checkCount > 0) {
				$(".ubboard .modal-article-open").modal('show');
			} else {
				alert(M.util.get_string('selected_noarticle', 'ubboard'));
			}
			
			return false;
		});
		
		// 공개 저장
		$(".ubboard .modal-article-open .btn-open-save").click(function() {
			
			checkCount = $('#ubboard-list-form input[name="manage[]"]:checked').length;
			openval = $(".ubboard .modal-article-open .form-control-open").val();
			
			confirmMessage = 'confirm_select_article_open';
			if(openval != 1) {
				confirmMessage = 'confirm_select_article_open_cancel';
			}
			
			confirm_message = M.util.get_string(confirmMessage, 'ubboard', checkCount);
			
			if(confirm(confirm_message)) {
				$('#ubboard-list-form input[name="'+coursemostype+'"]').val('checkOpen');
				$('#ubboard-list-form input[name="open"]').val(openval);
				$("#ubboard-list-form").submit();
			}
		});
		
		// 이동
		$("#ubboard-list-form .btn-selected-category").click(function() {

			// type값 변경
			checkCount = $('#ubboard-list-form input[name="manage[]"]:checked').length;
			
			if(checkCount > 0) {
				$(".ubboard .modal-article-move").modal('show');
			} else {
				alert(M.util.get_string('selected_noarticle', 'ubboard'));
			}
			
			return false;
		});
		
		
		// 이동 저장
		$(".ubboard .modal-article-move .btn-move-save").click(function() {
			
			checkCount = $('#ubboard-list-form input[name="manage[]"]:checked').length;
			
			if(checkCount > 0) {
				movecategoryid = $(".ubboard .modal-article-move .form-control-movecategoryid").val();
				
				$('#ubboard-list-form input[name="'+coursemostype+'"]').val('checkMoveArticles');
				$('#ubboard-list-form input[name="categoryid"]').val(movecategoryid);
				$("#ubboard-list-form").submit();
			}
		});
		
		
		// 공유
		$("#ubboard-list-form .btn-selected-share").click(function() {

			// type값 변경
			checkCount = $('#ubboard-list-form input[name="manage[]"]:checked').length;
			
			if(checkCount > 0) {
				$(".ubboard .modal-share").modal('show');
			} else {
				alert(M.util.get_string('selected_noarticle', 'ubboard'));
			}
			
			return false;
		});
		
		$(".ubboard .modal-share").on('show.bs.modal', function () {
			var inputmanage = $('#ubboard-list-form input[name="manage[]"]:checked');
			
			bwids = ''; 
			if($(inputmanage).length > 0) {
				comma = '';
				$(inputmanage).each(function() {
					bwids += comma + $(this).val();
					comma = ',';
				});
			}
			
			$(".ubboard .modal-share .modal-body").load($("#ubboard-list-form .btn-selected-share").data('url') + '&bwids='+bwids);
		});
	}

	ubboard.modform = function() {
		
		// 게시판 type 변경시..
		$(ubboard.admin_path + "#id_type").change(function() {
			getBoardSettings($(this).val());
			return false;
		});
		
		// 위에서는 selectbox change이벤트만 발생시켰기 때문에
		// 게시판 deafult설정값을 셋팅할수가 없어서 강제로 이벤트 발생시킴
		// getBoardSettings('default');
		
		// disable 상태로 두면 해당 element값이 전송이 안됨. submit시 disable 목록 제거 시켜줘야됨.
		$("#mform1").submit(function() {
			
			$("#id_board_setting select:disabled").each(function(i, element) {
				$(element).removeAttr('disabled');
			});
			
			// 조별 게시판 같은 경우에는 무들 기본 컨트롤인 group selectbox를 컨트롤하기 때문에 이곳에도.. disabled항목을 한거를 풀어줘야됨.
			$("#id_modstandardelshdr select:disabled").each(function(i, element) {
				$(element).removeAttr('disabled');
			});
		});
		
		
		function getBoardSettings(boardtype) {
			// 전달된 값이 존재하는 경우
			if (boardtype) {
				param = coursemostype + "=boardSettings&boardtype=" + boardtype;
				
				$.ajax({
					url:M.cfg.wwwroot+"/mod/ubboard/action.php"
					,data: param
					,success:function(data){
						if(data.code == mesg.success) {
							
							// notice, reply, comment, category, secret, sns, anonymous, groupmode
							for(var tc in data.boardsetting){
								el_name = ubboard.admin_path + "#id_"+tc;
								
								// 해당 element가 존재할 경우에만 실행하기
								if($(el_name).length > 0) {
									$(el_name + " option:selected").removeAttr('selected');
									
									selected_value = 0;
									
									if(typeof data.boardsetting[tc].open === 'boolean') {
										if(data.boardsetting[tc].open) {
											selected_value = 1;
										}
									} else {
										selected_value = data.boardsetting[tc].open;
									}
		
									$(el_name).val(selected_value);
									$(el_name + " > option[value="+selected_value+"]").prop('selected', true);
									
									// element disable
									if(data.boardsetting[tc].disable) {
										$(el_name).attr('disabled', 'disabled');
										if(tc != 'anonymous') {
											$(el_name).val(0);
										}
									} else {
										$(el_name).removeAttr('disabled');
									}
									
									if(tc == 'anonymous') {
										if($(el_name).val() == 1) {
											$(ubboard.admin_path + "#id_mailsend").attr('disabled', 'disabled');
											$(ubboard.admin_path + "#id_mailsend").val(0);
										}
									}
								}
							}
							
							// 그룹 게시판에서 그룹 기능을 사용하지 않는 게시판으로 type을 변경하면 groupmode값이 계속 유지 되는 현상이 발생이되서 
							// 그룹 기능을 사용하지 않을 경우에는 무조건 그룹 관련된 내용은 0값으로 셋팅되게 변경함.
							// 2012-09-18 by akddd
							if(data.boardsetting.hasOwnProperty('groupmode') && data.boardsetting['groupmode'].disable) {
								$(ubboard.admin_path + "#id_groupmode").val('0');
								$(ubboard.admin_path + "#id_groupingid").attr('disabled', 'disabled').val('0');
							} else {
								$(ubboard.admin_path + "#id_groupingid").removeAttr('disabled');
							}
							
							/*
							// 답글이 활성화 되어 있는 상태라면 게시판 정렬순서는 변경될수 없음. (게시판 타입이 일반 게시판 => Q&A로 변경되는 경우 게시판 정렬순서가 활성화 되어 있음)
							if ($(ubboard.admin_path + "#id_reply").val() == '1') {
								// 답글이 활성화 되어 있으면 게시판 정렬순서는 변경하지 못하고 작성일 순으로 정렬되도록 처리되어야 함.
								$(ubboard.admin_path + "#id_ordertype").attr('disabled', 'disabled').val('0');
							} else {
								$(ubboard.admin_path + "#id_ordertype").removeAttr('disabled');
							}
							*/
						} else {
							alert(data.msg);
						}
					}
				});
			}
		}
	}

	ubboard.article = function(id, bwid) {
		// 공유 기능
		$(".ubboard .article-buttons .btn-share").click(function() {
			$(".ubboard .modal-article-share").modal('show');
		});
		
		$(".ubboard .modal-article-share").on('show.bs.modal', function () {
			$(".ubboard .modal-article-share .modal-body").load($(".ubboard .article-buttons .btn-share").data('url'));
		});
		
		$(".article-lists .pagination .page-link").click(function() {
			setPagingEventBind($(this), id, bwid);
			
			return false;
		});
		
		
		function setPagingEventBind(el, id, bwid)
		{
			if ($(el).attr('href') != '#') {
				
				param = coursemostype + "=articleLists";// &page=" + $(el).data('pagenum');
				
				// 파라메터를 분해해서 전달.
				var queryParameters = {}, queryString = location.search.substring(1),
				re = /([^&=]+)=([^&]*)/g, m;
			    
				while (m = re.exec(queryString)) {
					pkey = decodeURIComponent(m[1]);
					pvalue = decodeURIComponent(m[2]);
					
					if (queryParameters[pkey] !== undefined) {
						if (!$.isArray(queryParameters[pkey])) {
							queryParameters[pkey] = [queryParameters[pkey]];
			            }
						queryParameters[pkey].push(pvalue);
			            
					} else {
						queryParameters[pkey] = pvalue;	
					}
				}
			    
				queryParameters['page'] = $(el).data('pagenum');
				
				
				$.ajax({
					url:M.cfg.wwwroot+"/mod/ubboard/view.php?" + $.param(queryParameters) 
					,data: param
					,success:function(data){
						if(data.code == mesg.success) {
							$(".article-lists").html(data.html);
							
							// 이벤트 걸어줘야됨
							$(".article-lists .pagination .page-link").click(function() {
								setPagingEventBind($(this), bwid);
								return false;
							});
							
							$(".ubboard .lists .form-control-category").change(function() {
								categoryid = $(this).val();
								
								url = M.cfg.wwwroot + '/mod/ubboard/view.php?id='+id;
								if (categoryid) {
									url += '&categoryid=' + categoryid;
								}
								
								location.href = url;
							});
						} else {
							alert(data.msg);
						}
					}
				});
			}
			
			return false;
		}
		
		// 삭제 버튼
		$(".ubboard .article-buttons .btn-delete").click(function() {
			setDelete();
			return false;
		});
		
		$(".ubboard .article-buttons .btn-delete-shareall").click(function() {
			setDeleteShareAll();
			return false;
		});
		
		$(".ubboard .article-buttons .btn-popover-delete").popover({
			html : true
			,placement : 'top'
			,trigger : 'focus'
			,content : function() {
				return $(".popover-delete-content").html();
			}
		}).on('shown.bs.popover', function (e) {
			$(".ubboard .article-buttons .btn-delete").click(function() {
				setDelete();
				return false;
			});
			
			$(".ubboard .article-buttons .btn-delete-shareall").click(function() {
				setDeleteShareAll();
				return false;
			});
		});
		
		
		function setDelete() {
			if (confirm(M.util.get_string('confirm_article_delete', 'ubboard'))) {
				parameter = coursemostype + '=delete&id='+id+'&bwid=' + bwid;
				formSubmit(ubboard.action, parameter);
			}
		}
		
		function setDeleteShareAll() {
			if (confirm(M.util.get_string('confirm_article_delete_shareall', 'ubboard'))) {
				parameter = coursemostype + '=deleteShareAll&id='+id+'&bwid=' + bwid;
				formSubmit(ubboard.action, parameter);
			}
		}
		
		
		
		// 공개 / 비공개 버튼
		$(".ubboard .article-buttons .btn-open").click(function() {
			openbutton = $(this);
			
			opentype = openbutton.attr('data-open');
			
			confirmMessage = 'confirm_article_open_y';
			if (opentype == '0') {
				// 비공개 전환 
				confirmMessage = 'confirm_article_open_n';
			}
			
			if (confirm(M.util.get_string(confirmMessage, 'ubboard'))) {
				
				parameter = coursemostype + '=open&id='+id+'&bwid=' + bwid+'&open='+opentype;
				
				$.ajax({
					url: ubboard.action 
					,data: parameter
					,success:function(data){
						if(data.code == mesg.success) {
							openbutton.attr('data-open', data.open).text(data.text);
						} else {
							alert(data.msg);
						}
					}
				});
			}
			
			return false;
		});
	}

	ubboard.defaultSetting = function() {
		// 취소 버튼
		$("#mformubboard #id_cancel").click(function() {
			history.back(-1);
			return false;
		});
	}


	ubboard.category = function(id) {
		$(".ubboard .lists .form-control-category").change(function() {
			categoryid = $(this).val();
			
			url = M.cfg.wwwroot + '/mod/ubboard/view.php?id='+id;
			if (categoryid) {
				url += '&categoryid=' + categoryid;
			}
			
			location.href = url;
		});
	}

	ubboard.categoryManage = function(id) {
		
		// delete
		$(".ubboard .btn-category-delete").click(function() {
			title = $(this).data('title');
			categoryid = $(this).data('id');
			
			if (confirm(M.util.get_string('confirm_category_delete', 'ubboard', title))) {
				
				parameter = coursemostype + '=categoryDelete&id='+id+'&categoryid=' + categoryid;
				formSubmit(ubboard.action, parameter);
			}
			
			return false;
			
		});
		
		
		// sortable
		$(".ubboard .coursemos-sortable").sortable({
			containerSelector: 'table'
			,itemPath: '> tbody'
			,itemSelector: 'tr'
			,handle : '.orders'
			,placeholder: '<tr class="placeholder"/>'

			,onDrop: function  ($item, container, _super) {
			    _super($item, container);

			    items = $(".ubboard .coursemos-sortable").sortable("serialize").get();
			    
			    jsonString = JSON.stringify(items, null, ' ');
			    console.log(jsonString);
			    $.ajax({
					 data: param = coursemostype+'=categorySortorder&id='+id+'&json='+jsonString
					,url : ubboard.action
					,success:function(data){
						if(data.code == mesg.success) {
							// location.reload();
						} else {
							alert(data.msg);
						}
					}
					,error: function(xhr, option, error){
				      alert(xhr.status + ' : ' + error);
				  	}
				});
			    
			}
		});
	}

	ubboard.comment = function(id, bwid) {
		$("#ubboard-comment .form-validate").validate();
		
		
		// comment cancel 클릭 이벤트 (textarea 폼 초기화)
		$("#ubboard-comment .well-comment-form .btn-comment-cancel").click(function() {
			$("#ubboard-comment .form-control-comment").val('');
		});
		
		
		// 코멘트 수정
		$("#ubboard-comment .btn-comment-modify").click(function() {
			
			// 작성 내용
			commentContent = $(this).closest('.media-body').find(".comment-content");
			
			setCommentFormModify(id, $(this).data('id'), commentContent);
			
			
			return false;
		});
		
		// 코멘트 삭제
		$("#ubboard-comment .btn-comment-delete").click(function() {
			if (confirm(M.util.get_string('confirm_comment_delete', 'ubboard'))) {
				parameter = coursemostype + '=commentDelete&id='+id+'&bwid=' + bwid + '&bcid=' + $(this).data('id');
				formSubmit(ubboard.action, parameter);
			}
			
			return false;
		});
		
		// 코멘트 답변
		$("#ubboard-comment .btn-comment-reply").click(function() {
			
			commentContent = $(this).closest('.media-body').find(".comment-content");
			setCommentFormReply($(this).data('id'), commentContent);
			
			return false;
		});
		
		
		function setCommentFormReply(commentid, obj) {
			var commentFormClone = getCommentForm();
			
			// type 변경
			$(commentFormClone).find("input[name='"+coursemostype+"']").val("commentInsert");
			
			// 해당 코멘트 번호 추가
			$(commentFormClone).find("input[name='bcid']").val(commentid);
			
			// 기존에 입력폼에 등록된 글이 존재할 수 있기 때문에 내용 삭제 시키기
			// textarea는 한개밖에 없으므로 따로 예외처리 신경 안써두 될듯.
			$(commentFormClone).find('textarea').val('');
			$(obj).after(commentFormClone);
		}


		function setCommentFormModify(id, commentid, obj) {
			var commentFormClone = getCommentForm();
			
			// type 변경
			$(commentFormClone).find("input[name='"+coursemostype+"']").val("commentUpdate");

			// 해당 코멘트 번호 추가
			$(commentFormClone).find("input[name='bcid']").val(commentid);
			
			// html 내용 숨기고 입력폼 로드
			$(obj).hide();
			$(obj).after(commentFormClone);
			
			// 코멘트 내용 가져오기
			// jquery로 html 값 가져와서 뿌려주는데 불필요한 공백들이 많이 존재함..
			// 실제 내용을 가져와서 뿌려주는게 더 좋은 방법인거 같음.
			param = coursemostype + "=comment&id="+id+"&commentid="+commentid;

			$.ajax({
				url: ubboard.action
				,data: param
				,type:'post'
				,success:function(data){
					if(data.code == mesg.success) {
						$(commentFormClone).find('textarea').val(data.comment);
						
						// 익명 기능을 사용하는 경우...
						if($(commentFormClone).find('.form-control-nickname').length > 0) {
							$(commentFormClone).find('.form-control-nickname').val(data.name);
						}
					} else {
						alert(data.msg);
						setCommentCancel();
						return false;
					}
				}
			});
		}

		function getCommentForm() {
			// comment_form 
			var commentFormCloneName = 'form-comment-clone';
			var commentFormClone = $("#"+commentFormCloneName);
			
			if(commentFormClone.length > 0) {
				// 기존에 등록한게 존재하므로.
				// 기존 위치에 있는 comment_conent를 show 시키고 위치 이동을 시켜야됨.
				$(commentFormClone).parent().find('.comment-content').show();
			} else {
				commentFormClone = $("<div id='"+commentFormCloneName+"' />").append($("#ubboard-comment .well-comment-form").clone());	
				$('body').append(commentFormClone);
				
				$("#"+commentFormCloneName+" .form-validate").validate();
			}
			
			$(commentFormClone).find('.btn-comment-cancel').unbind('click').click(function() { setCommentCancel(); });
			
			return commentFormClone;
		}


		function setCommentCancel() {
			var commentFormCloneName = 'form-comment-clone';
			var commentFormClone = $("#"+commentFormCloneName);
			if(commentFormClone.length > 0) {
				// 기존에 등록한게 존재하므로.
				// 기존 위치에 있는 comment_conent를 show 시키고 삭제 되어야 함.
				$(commentFormClone).parent().find('.comment-content').show();
			}
			
			$(commentFormClone).remove();
		}
	}


	ubboard.list = function (id, defaultLS) {	
		
		$(".ubboard .paging-info .btn-alldownload").click(function() {
			$.fileDownload($(this).prop('href'), {
				prepareCallback : function(url) {
					showSubmitProgress();
					$("#ajax_loading_message .save_msg").text(M.util.get_string('alldownload_loading', 'ubboard'));
				}, 
				successCallback : function(url) {
					hideSubmitProgress();
				},
	            failCallback: function (responseHtml, url, error) { 
	            	location.href = url;
	            }

		    });
			
			return false;
		});
		
		
		$(".ubboard .paging-info .form-control-ls").change(function() {
			ls = $(this).val();
			
			redirectls(id, ls);
		});
		
		
		
		function redirectls(id, ls) {
			// 숫자가 아니라면 강제로 기본 크기 지정
			if (!$.isNumeric(ls)) {	
				ls = defaultLS;
			}
			
			location.replace(M.cfg.wwwroot + '/mod/ubboard/view.php?id='+id + '&ls=' + ls);
		}
	}
	
	return ubboard;
});