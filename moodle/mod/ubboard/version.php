<?php
defined('MOODLE_INTERNAL') || die();

$plugin->component = 'mod_ubboard';
$plugin->version = 2018011601;
$plugin->requires = 2017050500;
$plugin->dependencies = array('local_ubion' => 2014070101);
