<?php
$definitions = array(
    // Cache ubboard
    'notices' => array(
        'mode' => cache_store::MODE_APPLICATION,
        'persistent' => true,
        'simplekeys' => true,
        'ttl' => 86400,
    ),
);
