<?php

class restore_ubboard_activity_structure_step extends restore_activity_structure_step {

    private $isFileCnt = true;
    
    /**
     * Defines structure of path elements to be processed during the restore
     *
     * @return array of {@link restore_path_element}
     */
    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');
        
        // 게시판 정보
        $paths[] = new restore_path_element('ubboard', '/activity/ubboard');
        $paths[] = new restore_path_element('ubboard_category', '/activity/ubboard/categorys/category');
        
        // 사용자 정보까지 가져오는 경우.
        if ($userinfo) {
            // 게시글, 코멘트 정보
            $paths[] = new restore_path_element('ubboard_write', '/activity/ubboard/categorys/category/writes/write');
            $paths[] = new restore_path_element('ubboard_comment', '/activity/ubboard/categorys/category/writes/write/comments/comment');
        }
        
        // Return the paths wrapped into standard activity structure
        return $this->prepare_activity_structure($paths);
    }
    
    
    /**
     * ubbpard 추가
     *
     */
    protected function process_ubboard($data) {
        global $DB;
        
        // 허용되는 게시판 타입
        // $allowBoardType = array('anonymous', 'default', 'group', 'notice', 'onetoone', 'qna');
        $allowBoardType = \mod_ubboard\factory::getTypes();
        
        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        
        // 허용되는 게시판 타입이 아닌 경우에는 기본 default로 설정해줘야됨.
        if (!array_key_exists($data->type, $allowBoardType)) {
            $data->type = 'default';
        }
        
        if (empty($data->timecreated)) {
            $data->timecreated = time();
        }
        
        if (empty($data->timemodified)) {
            $data->timemodified = time();
        }
        
        
        // insert the url record
        $newitemid = $DB->insert_record('ubboard', $data);
        // immediately after inserting "activity" record, call this
        $this->apply_activity_instance($newitemid);
    }
    
    
    /**
     * 카테고리 추가
     *
     */
    protected function process_ubboard_category($data) {
        global $DB;
        
        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        $data->ubboardid = $this->get_new_parentid('ubboard');
        
        if (empty($data->timecreated)) {
            $data->timecreated = time();
        }
        
        if (empty($data->timemodified)) {
            $data->timemodified = time();
        }
        
        $newitemid = $DB->insert_record('ubboard_category', $data);
        // ubboard->category->write->comment 와 같이 하위에 자식? 엮여 있을 경우에는 mapping을 반드시 해줘야 정상적으로 작동함
        $this->set_mapping('ubboard_category', $oldid, $newitemid);
    }
    
    
    protected function process_ubboard_write($data) {
        global $DB;
        
        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();
        
        $data->ubboardid = $this->get_new_parentid('ubboard');
        $data->categoryid = $this->get_new_parentid('ubboard_category');
        
        $data->groupid = $this->get_mappingid('group', $data->groupid);
        
        // data->userid가 없다면, 과거 데이터에서 넘어온거기 때문에 uid로 비교를 해야됨.
        if (isset($data->userid)) {
            $data->userid = $this->get_mappingid('user', $data->userid);
            if (isset($data->puserid)) {
                $data->puserid = $this->get_mappingid('user', $data->puserid);
            } else {
                $data->puserid = $this->get_mappingid('user', $data->userid);
            }
        } else {
            // 과거 ubboard 데이터를 현 ubboard 형태로 맞춰주는 작업
            $data->userid = $this->get_mappingid('user', $data->uid);
            
            if (isset($data->puid)) {
                $data->puserid = $this->get_mappingid('user', $data->puid);
            } else {
                $data->puserid = $this->get_mappingid('user', $data->uid);
            }
        }
        
        // file_cnt가 존재하는지 확인
        if (!isset($data->file_cnt)) {
            $this->isFileCnt = false;
        }
        
        
        $newitemid = $DB->insert_record('ubboard_write', $data);
        
        // ubboard->category->write->comment 와 같이 하위에 자식? 엮여 있을 경우에는 mapping을 반드시 해줘야 정상적으로 작동함
        // 파일이랑도 관련이 있으면 4번째 파라메터는 반드시 true를 전달해줘야됨
        $this->set_mapping('ubboard_write', $oldid, $newitemid, true);
    }
    
    protected function process_ubboard_comment($data) {
        global $DB;
        
        $data = (object)$data;
        $data->course = $this->get_courseid();
        
        $data->ubboardid = $this->get_new_parentid('ubboard');
        $data->bwid = $this->get_new_parentid('ubboard_write');
        
        if (isset($data->userid)) {
            $data->userid = $this->get_mappingid('user', $data->userid);
        } else {
            $data->userid = $this->get_mappingid('user', $data->uid);
        }
        
        
        $newitemid = $DB->insert_record('ubboard_comment', $data);
    }
    
    

    /**
     * Post-execution actions
     */
    protected function after_execute() {
        global $DB;
        
        // Add ubboard related files, no need to match by itemname (just internally handled context).
        $this->add_related_files('mod_ubboard', 'intro', null);
        
        // mod명, filearea명, 테이블명
        $this->add_related_files('mod_ubboard', 'attachment', 'ubboard_write');
        
        // 과거 ubboard에서는 file_cnt 컬럼이 존재하지 않았기 때문에 
        // file_cnt 컬럼이 없다면 게시물을 반복 돌면서 파일 갯수 업데이트 해줘야됨.
        // isFileCnt는  process_ubboard_write() 함수에서 검사합니다.
        if (empty($this->isFileCnt)) {
            if ($articles = $DB->get_records_sql("SELECT * FROM {ubboard_write} WHERE ubboardid = :ubboardid", array('ubboardid' => $this->get_new_parentid('ubboard')))) {
                $fs = get_file_storage();
                
                foreach ($articles as $a) {
                    if ($files = $fs->get_area_files($this->task->get_contextid(), 'mod_ubboard', \mod_ubboard\core\ubboard::FILEAREA_ATTACHMENT, $a->id)) {
                        $fileCount = 0;
                        foreach ($files as $f) {
                            // 실제 파일
                            if ($f->get_filename() != '.') {
                                $fileCount++;
                            }
                        }
                        
                        
                        // 등록된 파일이 존재하면 file_cnt 업데이트
                        if ($fileCount > 0) {
                            $update = new stdClass();
                            $update->id = $a->id;
                            $update->file_cnt = $fileCount;
                            $DB->update_record('ubboard_write', $update);
                        }
                    }
                    
                }
            }
        }
        
        
        
    }
}
