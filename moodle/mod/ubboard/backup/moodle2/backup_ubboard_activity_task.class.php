<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/ubboard/backup/moodle2/backup_ubboard_stepslib.php');
require_once($CFG->dirroot . '/mod/ubboard/backup/moodle2/backup_ubboard_settingslib.php');

class backup_ubboard_activity_task extends backup_activity_task {

    /**
     * No specific settings for this activity
     */
    protected function define_my_settings() {
    }

    /**
     * Defines a backup step to store the instance data in the ubboard.xml file
     */
    protected function define_my_steps() {
        $this->add_step(new backup_ubboard_activity_structure_step('ubboard_structure', 'ubboard.xml'));
    }

    /**
     * Encodes URLs to the index.php and view.php scripts
     *
     * @param string $content some HTML text that eventually contains URLs to the activity instance scripts
     * @return string the content with the URLs encoded
     */
    static public function encode_content_links($content) {
        global $CFG;

        $base = preg_quote($CFG->wwwroot, '/');

        // Link to the list of ubboards.
        $search = '/('.$base.'\/mod\/ubboard\/index.php\?id\=)([0-9]+)/';
        $content = preg_replace($search, '$@UBBOARDINDEX*$2@$', $content);

        // Link to ubboard view by moduleid.
        $search = '/('.$base.'\/mod\/ubboard\/view.php\?id\=)([0-9]+)/';
        $content = preg_replace($search, '$@UBBOARDVIEWBYID*$2@$', $content);

        return $content;
    }
}
