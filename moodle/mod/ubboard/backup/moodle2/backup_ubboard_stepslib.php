<?php

class backup_ubboard_activity_structure_step extends backup_activity_structure_step {

    /**
     * Defines the backup structure of the module
     *
     * @return backup_nested_element
     */
    protected function define_structure() {

        // Get know if we are including userinfo.
        $userinfo = $this->get_setting_value('userinfo');

        // Define the root element describing the ubboard instance.
        $ubboard = new backup_nested_element('ubboard', array('id'), array(
            'name', 'type', 'intro', 'introformat',
            'attachment', 'file_count', 'file_size',
            'category', 'ordertype',
            'notice', 'sms', 'reply', 'comment', 'secret', 'open', 'sns', 'anonymous',
            'mailsend', 'mailreceive', 'timecreated', 'timemodified', 'basic'
        ));

        $categorys = new backup_nested_element('categorys');
        $category = new backup_nested_element('category', array('id'), array(
            'name', 'orders', 'timecreated',
            'timemodified', 'basic', 'isdelete'));
        
        
        $writes = new backup_nested_element('writes');
        $write = new backup_nested_element('write', array('id'), array(
            'groupid', 'num', 'reply', 'parent', 'notice',
            'stime', 'etime', 'secret', 'userid', 'puserid',
            'name', 'subject', 'content', 'view_cnt', 'comment_cnt', 'file_cnt', 'timecreated', 'timemodified', 'ip', 'isdelete', 'open'));
        
        
        $comments = new backup_nested_element('comments');
        $comment = new backup_nested_element('comment', array('id'), array(
            'num', 'reply', 'parent', 'userid',
            'name', 'secret', 'comment', 'timecreated', 'timemodified', 'ip', 'isdelete'));
        
        // 카테고리
        $ubboard->add_child($categorys);
        $categorys->add_child($category);
        
        // 게시글
        $category->add_child($writes);
        $writes->add_child($write);
        
        // 코멘트
        $write->add_child($comments);
        $comments->add_child($comment);
        
        
        // ubboard id값은 activitid값이 되어야 함.
        $ubboard->set_source_table('ubboard', array('id' => backup::VAR_ACTIVITYID));
        
        $category->set_source_table('ubboard_category', array('ubboardid' => backup::VAR_PARENTID));
        
        if ($userinfo) {
            $write->set_source_table('ubboard_write', array('ubboardid'=>'../../../../id', 'categoryid'=>backup::VAR_PARENTID));
            $comment->set_source_table('ubboard_comment', array('ubboardid'=>'../../../../../../id', 'bwid' => backup::VAR_PARENTID));
        }
        
        // userid는 실제 db 테이블에서 사용자 id값이 저장되는 컬럼임;
        $write->annotate_ids('user', 'userid');
        $write->annotate_ids('user', 'puserid');
        $write->annotate_ids('group', 'groupid');
        $comment->annotate_ids('user', 'userid');
        
        
        // ubboard에서 사용되는 첨부파일중 mdl_files에 저장되는 값들을 모두 적어줘야됨.
        $ubboard->annotate_files('mod_ubboard', 'intro', null); // chat_intro area don't use itemid
        $write->annotate_files('mod_ubboard', 'attachment', 'id'); // chat_intro area don't use itemid
        
        // Return the root element (ubboard), wrapped into standard activity structure.
        return $this->prepare_activity_structure($ubboard);
    }
}
