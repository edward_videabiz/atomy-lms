<?php
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/ubboard/backup/moodle2/restore_ubboard_stepslib.php');

class restore_ubboard_activity_task extends restore_activity_task {

    /**
     * Define (add) particular settings this activity can have
     */
    protected function define_my_settings() {
        // No particular settings for this activity.
    }

    /**
     * Define (add) particular steps this activity can have
     */
    protected function define_my_steps() {
        // We have just one structure step here.
        $this->add_step(new restore_ubboard_activity_structure_step('ubboard_structure', 'ubboard.xml'));
    }

    /**
     * Define the contents in the activity that must be
     * processed by the link decoder
     */
    static public function define_decode_contents() {
        $contents = array();

        $contents[] = new restore_decode_content('ubboard', array('intro'), 'ubboard');

        return $contents;
    }

    /**
     * Define the decoding rules for links belonging
     * to the activity to be executed by the link decoder
     */
    static public function define_decode_rules() {
        $rules = array();

        $rules[] = new restore_decode_rule('UBBOARDVIEWBYID', '/mod/ubboard/view.php?id=$1', 'course_module');
        $rules[] = new restore_decode_rule('UBBOARDINDEX', '/mod/ubboard/index.php?id=$1', 'course');

        return $rules;

    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * ubboard logs. It must return one array
     * of {@link restore_log_rule} objects
     */
    static public function define_restore_log_rules() {
        $rules = array();

        // 게시판 관련
        $rules[] = new restore_log_rule('ubboard', 'add', 'view.php?id={course_module}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'update', 'view.php?id={course_module}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'view', 'view.php?id={course_module}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'view all', 'index.php?id={course_module}', '{course_module}');
        
        // 작성글 관련
        $rules[] = new restore_log_rule('ubboard', 'view article', 'article.php?id={course_module}&bwid={ubboard_write_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'write article', 'write.php?id={course_module}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'add article', 'article.php?id={course_module}&bwid={ubboard_write_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'modify article', 'modify.php?id={course_module}&bwid={ubboard_write_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'update article', 'article.php?id={course_module}&bwid={ubboard_write_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'reply article', 'write.php?id={course_module}&rnum=[rnum]', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'delete article', 'article.php?id={course_module}&bwid={ubboard_write_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'delete selected article', 'view.php?id={course_module}', '{ubboard}');
        
        // 코멘트 관련
        $rules[] = new restore_log_rule('ubboard', 'add comment', 'article.php?id={course_module}&bwid={ubboard_write_id}&bcid={ubboard_category_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'update comment', 'article.php?id={course_module}&bwid={ubboard_write_id}&bcid={ubboard_category_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'delete comment', 'article.php?id={course_module}&bwid={ubboard_write_id}&bcid={ubboard_category_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'add comment reply', 'article.php?id={course_module}&bwid={ubboard_write_id}&bcid={ubboard_category_id}', '{ubboard}');
        
        // 카테고리
        $rules[] = new restore_log_rule('ubboard', 'manage category', 'category.php?id={course_module}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'write category', 'category_add.php?id={course_module}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'add category', 'category.php?id={course_module}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'modify category', 'category_modify.php?id={course_module}&caid={ubboard_category_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'update category', 'category.php?id={course_module}&caid={ubboard_category_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'delete category', 'category.php?id={course_module}&caid={ubboard_category_id}', '{ubboard}');
        $rules[] = new restore_log_rule('ubboard', 'sortable category', 'category.php?id={course_module}', '{ubboard}');
        
        
        $rules[] = new restore_log_rule('ubboard', 'allDownload', 'view.php?id={course_module}', '{ubboard}');
        
        return $rules;

        return $rules;
    }

    /**
     * Define the restore log rules that will be applied
     * by the {@link restore_logs_processor} when restoring
     * course logs. It must return one array
     * of {@link restore_log_rule} objects
     *
     * Note this rules are applied when restoring course logs
     * by the restore final task, but are defined here at
     * activity level. All them are rules not linked to any module instance (cmid = 0)
     */
    static public function define_restore_log_rules_for_course() {
        $rules = array();

        $rules[] = new restore_log_rule('ubboard', 'view all', 'index.php?id={course}', null);

        return $rules;
    }
}
