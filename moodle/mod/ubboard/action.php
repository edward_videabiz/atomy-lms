<?php
require('../../config.php');
use local_ubion\controller\Controller;
use \local_ubion\base\Javascript;
use local_ubion\base\Parameter;
use mod_ubboard\factory;


$type 		= optional_param(Controller::TYPENAME, null, PARAM_ALPHAEXT);
 
if ($type == 'boardSettings') {
    if (Parameter::isAajx()) {
        $boardtype = optional_param('boardtype', 'default', PARAM_ALPHANUMEXT);
        
        $boardSetting = factory::getSettings($boardtype);
        
        if (!empty($boardSetting)) {
            Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'boardsetting'=>$boardSetting));
        } else {
            Javascript::printAlert(get_string('notfound_boardtype', $pluginname, $boardtype));
        }
    } else {
        Javascript::printAlert(get_string('not_valid_call_method', 'local_ubion'));
    }
} else {
    $id         = required_param('id', PARAM_INT);   // cmid
    
    $CUBBoard = \mod_ubboard\factory::create($id);
    
    if ($CUBBoard->isSubmit($type)) {
        $CUBBoard->invoke($type);
    }
}