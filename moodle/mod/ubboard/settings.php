<?php
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    $pluginname = 'ubboard';
    
    // 모든 첨부파일 다운로드 권한
    $name = $pluginname.'/allDownload';
    $title = get_string('setting_alldownload', $pluginname);
    $infomation = get_string('setting_alldownload_help', $pluginname);
    $default = 1;
    $options = array(1 => get_string('defaultcourseteacher'), 2 => get_string('defaultcoursestudent'));
    $setting = new admin_setting_configselect($name, $title, $infomation, $default, $options);
    $settings->add($setting);
    
    
    // 사이트 팝업 시작 위치 Top
    $name = $pluginname.'/popupTopSite';
    $title = get_string('setting_popup_top_site', $pluginname);
    $infomation = get_string('setting_popup_top_help_site', $pluginname);
    $default = 5;
    $setting = new admin_setting_configtext($name, $title, $infomation, $default, PARAM_INT);
    $settings->add($setting);
    
    
    // 사이트 팝업 시작 위치 Left
    $name = $pluginname.'/popupLeftSite';
    $title = get_string('setting_popup_left_site', $pluginname);
    $infomation = get_string('setting_popup_left_help_site', $pluginname);
    $default = 5;
    $setting = new admin_setting_configtext($name, $title, $infomation, $default, PARAM_INT);
    $settings->add($setting);
    
    
    
    // 강좌내 팝업 사용여부
    $name = $pluginname.'/coursePopup';
    $title = get_string('setting_course_popup', $pluginname);
    $infomation = get_string('setting_course_popup_help', $pluginname);
    $default = 0;
    $options = array(0 => get_string('no'), 1 => get_string('yes'));
    $setting = new admin_setting_configselect($name, $title, $infomation, $default, $options);
    $settings->add($setting);
    
    // 강좌내 팝업 시작 위치 Top
    $name = $pluginname.'/popupTopCourse';
    $title = get_string('setting_popup_top_course', $pluginname);
    $infomation = get_string('setting_popup_top_help_course', $pluginname);
    $default = 70;
    $setting = new admin_setting_configtext($name, $title, $infomation, $default, PARAM_INT);
    $settings->add($setting);
    
    
    // 강좌내 팝업 시작 위치 Left
    $name = $pluginname.'/popupLeftCourse';
    $title = get_string('setting_popup_left_course', $pluginname);
    $infomation = get_string('setting_popup_left_help_course', $pluginname);
    $default = 280;
    $setting = new admin_setting_configtext($name, $title, $infomation, $default, PARAM_INT);
    $settings->add($setting);
    
    
        
    
    ######################
    ###### 공개여부 ######
    ######################
    $settingpage = new admin_setting_heading('ubboard_open', get_string('setting_open', $pluginname), null);
    $settings->add($settingpage);
    
    // 공개여부 사용여부
    $name = $pluginname.'/open';
    $title = get_string('setting_open_use', $pluginname);
    $infomation = null;
    $default = 0;
    $options = array(0 => get_string('no'), 1 => get_string('yes'));
    $setting = new admin_setting_configselect($name, $title, $infomation, $default, $options);
    $settings->add($setting);
    
    
    // 글 등록시 공개여부 기본값
    $name = $pluginname.'/open_default';
    $title = get_string('setting_open_default', $pluginname);
    $infomation = null;
    $default = 0;
    $options = array(0 => get_string('open_0', $pluginname), 1 => get_string('open_1', $pluginname));
    $setting = new admin_setting_configselect($name, $title, $infomation, $default, $options);
    $settings->add($setting);
}