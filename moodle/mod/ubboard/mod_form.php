<?php
use mod_ubboard\factory;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * Module instance settings form
 *
 * @package    mod_ubboard
 * @copyright  2017 akddd <akddd@naddle.net>
 */
class mod_ubboard_mod_form extends moodleform_mod {

    // mod 플러그인인 경우에는 mod_를 생략해도 상관이 없습니다.
    private $pluginname = 'ubboard';
    
    /**
     * Defines forms elements
     */
    public function definition() 
    {
        global $CFG, $COURSE, $DB, $PAGE;
        
        $mform = $this->_form;
        
        $strrequired = get_string('required');

        // Adding the "general" fieldset, where all the common settings are showed.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('ubboardname', $this->pluginname), array('size' => '64'));
        $mform->setType('name', PARAM_TEXT);
        
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        
        // 게시판 타입
        $mform->addElement('select', 'type', get_string('boardtype', $this->pluginname), \mod_ubboard\factory::getTypes());
        
        if (empty($this->current->id)) {
            $mform->addRule('type', $strrequired, 'required', null, 'client');
        }

        // Adding the standard "intro" and "introformat" fields.
        $this->standard_intro_elements(get_string('boardintro', $this->pluginname));       

        
        // ==============
        // upload
        // ==============
        $mform->addElement('header', 'attachment_header', get_string('attachments', $this->pluginname));
        
        $mform->addElement('selectyesno', 'attachment', get_string('enable_attachment', $this->pluginname));
        $mform->setDefault('attachment', 1);
        
        // 첨부파일 갯수
        $maxcount = 50;
        $attachment_count = array_combine(range(1, $maxcount), range(1, $maxcount));
        $mform->addElement('select', 'file_count', get_string('filecount', $this->pluginname), $attachment_count);
        $mform->setDefault('file_count', 5);
        
        // 최대 첨부 용량
        $maxUploadSize = get_max_upload_sizes($CFG->maxbytes, $COURSE->maxbytes, 0);
        $mform->addElement('select', 'file_size', get_string('filemaxsize', $this->pluginname), $maxUploadSize);
        $mform->setDefault('file_size', 0);	// 강좌 업로드 한계로 설정
        
        
        // ==============
        // board setting
        // ==============
        $mform->addElement('header', 'board_setting', get_string('board_setting', $this->pluginname));
        $mform->setExpanded('board_setting', true);
        
        // category
        $mform->addElement('selectyesno', 'category', get_string('board_category', $this->pluginname));
        $mform->setDefault('category', 0);
        
        
        // 게시판 정렬
        $sort_order = array();
        $sort_order[0] = get_string('sortorder_create', $this->pluginname);
        $sort_order[1] = get_string('sortorder_modify', $this->pluginname);
        
        $mform->addElement('select', 'ordertype', get_string('board_sortorder', $this->pluginname), $sort_order);
        $mform->setDefault('ordertype', 0);	// 작성일값으로 설정
        
        
        // 공지사항
        $mform->addElement('selectyesno', 'notice', get_string('board_notice', $this->pluginname));
        $mform->setDefault('notice', 0);
        $mform->addHelpButton('notice', 'board_notice', $this->pluginname);
        
        // reply
        $mform->addElement('selectyesno', 'reply', get_string('board_reply', $this->pluginname));
        $mform->setDefault('reply', 1);
        
        
        // comment
        $mform->addElement('selectyesno', 'comment', get_string('board_comment', $this->pluginname));
        $mform->setDefault('comment', 1);
        
        
        // secret
        $mform->addElement('selectyesno', 'secret', get_string('board_secret', $this->pluginname));
        $mform->setDefault('secret', 0);
        
        
        // 공개글
        $isOpen = get_config('ubboard', 'open');
        if (empty($isOpen)) {
            $isOpen = 0;
        }
        
        if ($isOpen) {
            $mform->addElement('selectyesno', 'open', get_string('board_open', $this->pluginname));
            $mform->setDefault('open', 0);
            $mform->addHelpButton('open', 'board_open', $this->pluginname);
        } else {
            // 모듈 설정에서 공개기능 사용안함으로 설정했기 때문에 open값은 0으로 설정되어야 함.
            $mform->addElement('hidden', 'open');
            $mform->setType('open', PARAM_INT);
            $mform->setDefault('open', 0);	
        }
        
        
        // SNS
        $mform->addElement('selectyesno', 'sns', get_string('board_sns', $this->pluginname));
        $mform->setDefault('sns', 0);
        
        
        // 익명기능
        $mform->addElement('selectyesno', 'anonymous', get_string('board_anonymous', $this->pluginname), array('disabled' => 'disabled'));
        $mform->setDefault('anonymous', 0);
        $mform->addHelpButton('anonymous', 'board_anonymous', $this->pluginname);
        
        
        // 메일전송
        $mform->addElement('selectyesno', 'mailsend', get_string('board_mailsend', $this->pluginname), array('disabled' => 'disabled'));
        $mform->setDefault('mailsend', 0);
        $mform->addHelpButton('mailsend', 'board_mailsend', $this->pluginname);
        
        
        // 메일 받기
        $mform->addElement('selectyesno', 'mailreceive', get_string('board_mailreceive', $this->pluginname), array('disabled' => 'disabled'));
        $mform->setDefault('mailreceive', 0);
        $mform->addHelpButton('mailreceive', 'board_mailreceive', $this->pluginname);
        
        
        // 익명 기능을 사용하면 메일 전송 기능은 비활성화 되어야 함.
        $mform->disabledif ('mailsend', 'anonymous', 'eq', 1);
        $mform->disabledif ('ordertype', 'reply', 'eq', 1);
        
        $this->standard_coursemodule_elements();
        
        // buttons
        $this->add_action_buttons();
        
        $PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'modform');
    }
    
    
    function definition_after_data() {
        global $PAGE, $CFG;
        parent::definition_after_data();
        
        $mform = $this->_form;
        
        // 수정모드인지 판별을 할려면 update값과 instance값 두개가 존재하는지로 판단해야될듯.
        // update만으로도 가능하나... 좀 더 확실하게 하기위해서 instance도 같이 비교하는게 좋을듯 싶음.
        $updateid = $mform->getElementValue('update');
        $instanceid = $mform->getElementValue('instance');
        $boardtype = $mform->getElementValue('type');
        
        // 수정 페이지인지 확인
        $isUpdate = (!empty($updateid) && !empty($instanceid)) ? true : false;
        
        // 게시판 타입
        $boardtypevalue = (!empty($boardtype)) ? current($boardtype) : 'default';
        
        $boardSetting = factory::getSettings($boardtypevalue);
        foreach ($boardSetting as $key=>$value) {
            if ($mform->elementExists($key)) {
                
                // 수정페이지가 아닌 경우 게시판의 기본값을 다시 셋팅해줘야됨.
                // mod_form에 기본값을 셋팅해놨기 때문에 코드상에서 게시판 셋팅값을 변경하면 selected가 제대로 안되는 문제가 있음.
                if (!$isUpdate) {
                    $mform->setDefault($key, $value['open']);
                }
                
                // 모듈이 disabled되지 않은 항목인 경우 disabled attr 제거 해주기
                if (!$value['disable']) {
                    $form_element = $mform->getElement($key);
                    $form_element->removeAttribute('disabled');
                } else {
                    // 2014-07-10 disabled element인 경우에는 disabled 시켜줘야됨.
                    $form_element = $mform->getElement($key);
                    $form_element->updateAttributes(array('disabled', 'disabled'));
                    
                    // form submit될때에 disalbed을 풀기 때문에 disabled로 설정된 element값의 기본값을 0으로 설정해줘야됨.
                    if ($key != 'anonymous') {
                        $mform->setDefault($key, 0);
                    }
                }
            }
        }
        
        // 수정 모드인 경우
        if ($isUpdate) {
            // 익명 게시판이면 몇몇 설정들은 비활성화 되어있어야 함.
            if ($this->current->type == 'anonymous') {
                $mform->getElement('type')->updateAttributes(array('disabled', 'disabled'));
                $mform->getElement('anonymous')->updateAttributes(array('disabled', 'disabled'));
            }
        }
    
        if ($boardtypevalue == 'group') {
            $mform->setDefault('groupmode', $boardSetting['groupmode']['open']);
        } else {
            $mform->setDefault('groupmode', 0);
            $mform->setDefault('groupingid', 0);
            $mform->setDefault('groupmembersonly', 0);
            
            
            // 게시판 추가인 경우에는 groupmode값이 셋팅되면 안됨.
            $mform->getElement('groupmode')->updateAttributes(array('disabled'=>'disabled'));
            $mform->getElement('groupingid')->updateAttributes(array('disabled'=>'disabled'));
            if ($mform->elementExists('groupmembersonly')) {
                $mform->getElement('groupmembersonly')->updateAttributes(array('disabled'=>'disabled'));
            }
        }
        
    }
    
    
    function data_preprocessing(&$default_values) {
        parent::data_preprocessing($default_values);
        
        /*
         * // Set up the completion checkboxes which aren't part of standard data.
        // We also make the default value (if you turn on the checkbox) for those
        // numbers to be 1, this will not apply unless checkbox is ticked.
        $default_values['completiondiscussionsenabled']=
            !empty($default_values['completiondiscussions']) ? 1 : 0;
        if (empty($default_values['completiondiscussions'])) {
            $default_values['completiondiscussions']=1;
        }
        $default_values['completionrepliesenabled']=
            !empty($default_values['completionreplies']) ? 1 : 0;
        if (empty($default_values['completionreplies'])) {
            $default_values['completionreplies']=1;
        }
        // Tick by default if Add mode or if completion posts settings is set to 1 or more.
        if (empty($this->_instance) || !empty($default_values['completionposts'])) {
            $default_values['completionpostsenabled'] = 1;
        } else {
            $default_values['completionpostsenabled'] = 0;
        }
        if (empty($default_values['completionposts'])) {
            $default_values['completionposts']=1;
        }
         */
    }
}