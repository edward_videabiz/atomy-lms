<?php
require '_head.php';


use \local_ubion\base\Common;


// 해당 메뉴를 사용할수 있는 사람인지 검사
if ($CUBBoard->isAllDownload()) {
    
    // 모든 게시물을 반복돌면서 첨부파일 확인
    // files 테이블에서 직접 조회해도 되는데, 문제는 게시물 삭제시 실제 DB에서 삭제되는게 아니고 isdelete 컬럼에 0, 1로 구분하기 때문에
    // files 테이블에 조회하면 삭제된 게시물도 같이 다운받아질수 있음.
    if ($allArticles = $CUBBoard->getAllArticles()) {
        $fs = get_file_storage();
        
        $zipfiles = array();
        $anonymous = get_string('anonymous', 'ubboard');
        
        foreach ($allArticles as $a) {
            
            // 익명 게시판이면 익명으로 출력되어야 함.
            $fullname = $anonymous;
            if ($ubboard->anonymous) {
                if (!empty($a->name)) {
                    $fullname = $a->name;
                }
            } else {
                if (!empty($a->userid)) {
                    $fullname = $CUBBoard->getUserFullname($a);
                }
            }
            $a->fullname = $fullname;
            
            
            // $files = $CUBBoard->getFiles($a->id);
            if ($files = $fs->get_area_files($context->id, 'mod_ubboard', $CUBBoard::FILEAREA_ATTACHMENT, $a->id, 'id', false)) {
                foreach ($files as $file) {
                    
                    $fileKey = clean_filename($fullname . "_" . $file->get_filename());
                    $zipfiles[$fileKey] = $file;
                }
            }
        }
        
        if (!empty($zipfiles)) {
            $zipFilename = str_replace(' ', '_', clean_filename($course->fullname.'-'.$ubboard->name.".zip")); //name of new zip file.
            
            //create path for new zip file.
            $tempzip = tempnam($CFG->dataroot.'/temp/', 'ubboard_');
            
            $useragent = core_useragent::get_user_agent_string();
            if (core_useragent::is_ie() || strpos($useragent, 'Edge/') !== false) {
                $zipFilename = rawurlencode($zipFilename);
            }
            
            
            //zip files
            $zipper = new zip_packer();
            if ($zipper->archive_to_pathname($zipfiles, $tempzip)) {
                // 첨부파일 모두 다운로드
                $params = array(
                    'context' => $context
                    ,'objectid' => $ubboard->id
                );
                
                $event = \mod_ubboard\event\all_downloaded::create($params);
                $event->trigger();
                

                header("Set-Cookie: fileDownload=true; path=/");
                send_temp_file($tempzip, $zipFilename); //send file and delete after sending.
            } else {
                print_error(get_string('zip_failed', 'ubboard'), 'error', $error_url);
            }
            
            
        } else {
            Common::printNotice(get_string('nofiles', 'ubboard'), $ubboardViewUrl);
        }
    } else {
        Common::printNotice(get_string('noarticles', 'ubboard'), $ubboardViewUrl);
    }
} else {
    Common::printNotice(get_string('error_permission_alldownload', 'ubboard'), $ubboardViewUrl);
}