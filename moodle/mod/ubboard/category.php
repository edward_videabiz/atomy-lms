<?php

use local_ubion\base\Common;

require_once __DIR__.'/_head.php';

// 수정권한 확인
$CUBBoard->isPermission('manage', true);

// 해당 게시판이 카테고리 기능을 사용하지 않을 경우에는 오류 메세지.
if (empty($ubboard->category)) {
    Common::printNotice(get_string('category_not_used', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/view.php?id='.$cm->id);
}

// 모든 카테고리 가져오기
if ($categories = $CUBBoard->getCategories()) {
    $strModify = get_string('btn_modify', 'ubboard');
    $strDelete = get_string('btn_delete', 'ubboard');
    
    $modifylink = $CFG->wwwroot.'/mod/ubboard/category_write.php?id='.$cm->id.'&categoryid=';
    $categorylink = $CFG->wwwroot.'/mod/ubboard/view.php?id='.$cm->id.'&categoryid=';

    $categoryQuery = "SELECT COUNT(1) FROM {ubboard_write} WHERE course=:course AND ubboardid = :ubboardid AND categoryid = :categoryid AND isdelete = 0";
    foreach ($categories as $c) {
        $c->fulldate = userdate($c->timecreated);
        $c->date = date('Y-m-d', $c->timecreated);
        
        $c->buttonModify = '<a href="'.$modifylink.$c->id.'" class="btn btn-sm btn-default">'.$strModify.'</a>';
        $c->buttonDelete = '';
        if (!empty($c->basic)) {
            $c->buttonDelete = '<button type="button" class="btn btn-sm btn-danger btn-category-delete" data-title="'.$c->name.'" data-id="'.$c->id.'">'.$strDelete.'</button>';
        }
        
        $articleCount = $DB->get_field_sql($categoryQuery, array('course'=>$ubboard->course, 'ubboardid'=>$ubboard->id, 'categoryid'=>$c->id));
        if ($articleCount > 0) {
            $articleCount = number_format($articleCount);
        }
        $c->cnt = $articleCount;
        
        $c->link = $categorylink.$c->id;
    }
}


// mustache보낼때에는 key값이 별도로 설정되어 있으면 안됨.
if (!empty($categories)) {
    $categories = array_values($categories);
}


$buttons = new stdClass();
$buttons->list = $CUBBoard->setButton(get_string('btn_list', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/view.php?id='.$cm->id);
$buttons->add = $CUBBoard->setButton(get_string('btn_add', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/category_write.php?id='.$cm->id);

// template로 넘길 변수 선언
$mustache = new stdClass();
$mustache->categories = $categories;
$mustache->icons = $CUBBoard->getIcons();
$mustache->buttons = $buttons;


$PAGE->requires->strings_for_js(array(
    'confirm_category_delete'
), 'ubboard');
$PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'categoryManage', array(
    'id' => $cm->id
));

$PAGE->set_url('/mod/ubboard/category.php', array('id' => $cm->id));

// Output starts here.
echo $OUTPUT->header();

echo $CUBBoard->renderer('category', $mustache);
// Finish the page.
echo $OUTPUT->footer();
