<?php
use local_ubion\controller\Controller;
use local_ubion\base\Common;

require_once '_head.php';
require_once($CFG->libdir.'/completionlib.php');

$rnum = optional_param('rnum', 0, PARAM_INT);

$PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'defaultSetting');

$PAGE->set_url('/mod/ubboard/write.php', array(
    'id' => $id,
    'bwid' => $bwid
));


// 그룹 게시판인데 그룹에 소속되지 않으면 에러 메시지를 출력시켜줘야됨.
if ($ubboard->type == 'group') {
    $selectgroup = groups_get_activity_group($cm);
    
    // selectgroup값이 0보다 작으면 소속된 그룹이 없는 상태임
    // 단 관리 권한이 존재하는 사용자라면 전체 그룹에 대한 글을 남길수 있기 때문에 예외처리되어야 함.
    if (!$CUBBoard->isPermission('manage') && $selectgroup <= 0) {
        Common::printError(get_string('error_group_nobelong', 'ubboard'));
    }
    
}

$mformWrite = new mod_ubboard_write_form('action.php', 
    array(
        'CUBBoard'=>$CUBBoard,
        'rnum' => $rnum,
        'bwid' => $bwid
    )
    ,'post'
    ,''
    ,array('id' => 'mformubboard')
);


$typename = Controller::TYPENAME;
$data = new \stdClass();
$data->id = $cm->id;
$data->$typename = 'insert';

// 취소 버튼을 클릭한 경우
if ($mformWrite->is_cancelled()) {
    redirect($CFG->wwwroot.'/mod/ubboard/view.php?id='.$id);
} else if ($fromform = $mformWrite->get_data()) {
    $typename = $CUBBoard::TYPENAME;
    $coursemostype = $fromform->$typename;
    
    if ($CUBBoard->isSubmit($coursemostype)) {
        $CUBBoard->invoke($coursemostype);
    }
} else {
    // 글 수정인 경우
    if (!empty($bwid)) {
        // 권한 체크
        $CUBBoard->isPermission('write', true);
        
        $data = $CUBBoard->getArticle($bwid, true);
        
        // 본인 글인 경우에만 수정이 가능해야됨.
        if (!$CUBBoard->isMyArticle($data)) {
            \local_ubion\base\Common::printNotice(get_string('not_my_article', 'ubboard'), $ubboardViewUrl);
        }
            
        $data->id = $cm->id;        // id값은 반드시 cmid로 전달해줘야됨.
        $data->bwid = $bwid;
        $data->$typename = 'update';
        
        
        // 파일정보 담아두기
        $draftitemid = file_get_submitted_draft_itemid('attachment');
        file_prepare_draft_area($draftitemid, $context->id, 'mod_ubboard', $CUBBoard::FILEAREA_ATTACHMENT, $bwid, $mformWrite::attachment_options($ubboard));
        $data->attachment = $draftitemid;
        
        // 원본글 정보 반영
        $data->content = $CUBBoard->getFormEditor('article', $bwid, $data->content);
        
    } else if (!empty($rnum)) {
        // 답변을 달수 있는 게시판인지 확인
        if ($ubboard->reply) {
            // 권한 체크
            $CUBBoard->isPermission('reply', true);
            
            // 제목하고, 내용 넣어줘야됨.
            // 이곳에서는 getArticle() 호출시 DB 데이터만 가져오면 상위글에서 추가한 에디터 사진들이 표시가 되지 않습니다.
            if ($article = $CUBBoard->getArticle($rnum)) {
                if ($article->ubboardid != $ubboard->id) {
                    Common::printError(get_string('error_notmatch_boardid', 'ubboard'));
                }
                
                // 원본글이 비밀글이면 본인 권한 체크해야됨.
                if (!$CUBBoard->isPermission('manage')) {
                    // 비밀글인 경우
                    if ($article->secret) {
                        // 대상글이 본인과 관련된 글이 아니라면 에러 출력해줘야됨.
                        if (!$CUBBoard->isSecretArticleView($article)) {
                            Common::printNotice(get_string('secret_not_reply', 'ubboard', $rnum), $ubboardViewUrl);
                        }
                    }
                }
                
                $data->rnum = $rnum;
                
                $data->subject = get_string('reply', 'ubboard')." ".$article->subject;
                
                // 원본글 내용 추가해주기
                $data->content = "<br/><br/><blockquote style='margin:0 0 0 1rem; border-left:2px #ccc solid; padding-left:.5rem; font-size:12px;'>".$article->content."</blockquote>";
                $data->content = $CUBBoard->getFormEditor('article', $bwid, $data->content);
                
                // 비밀글 여부를 상위 글값 그대로 유지해줘야됨.
                $data->secret = $article->secret;
                
            } else {
                Common::printError(get_string('error_notexist_article', 'ubboard', $rnum));
            }
        } else {
            Common::printNotice(get_string('reply_not_used', 'ubboard'), $ubboardViewUrl);
        }
    } else {
        // 글 추가
        // 권한 체크
        $CUBBoard->isPermission('write', true);
    }
    
    
    
    // 상황에 따라 mform_write에 기본값으로 셋팅되어야 할 값을 설정해줘야됨.
    $mformWrite->set_data($data);
    
    
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading(format_string($ubboard->name), 2);
    
    $mformWrite->display();
    
    echo $OUTPUT->footer();
}