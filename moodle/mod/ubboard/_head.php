<?php
require('../../config.php');

// ==========================
// 초기 설정
// ==========================
$id         = optional_param('id', 0, PARAM_INT);       		    // Course Module ID
$boardid    = optional_param('ubboardid', 0, PARAM_INT);              // ubboard instance ID - it should be named as the first character of the module.


if (!empty($id)) {
    $CUBBoard = \mod_ubboard\factory::create($id);
} else if (!empty($ubboardid)) {
    $CUBBoard = \mod_ubboard\factory::create($ubboardid, false);
} else {
    print_error('missingparameter');
}

$ls			= optional_param('ls', $CUBBoard->getDefaultListSize(), PARAM_INT);				    // 리스트 갯수

$course = $CUBBoard->getCourse();
$cm = $CUBBoard->getCM();
$context = $CUBBoard->getContext();
$ubboard = $CUBBoard->getUBBoard();

$PAGE->set_url('/mod/ubboard/view.php', array('id' => $cm->id));
$PAGE->set_course($course);
$PAGE->set_cm($cm);
$PAGE->set_context($context);
$PAGE->set_title($course->shortname . ' : ' . format_string($ubboard->name));
$PAGE->set_heading($course->fullname);

// 강좌 홈인 경우에는 표시가되면 안됨.
if ($course->id == SITEID) {
    // $PAGE->set_pagelayout('standard');
    // $PAGE->set_pagetype('site-board');
} else {
    // 반드시 로그인 해야됨
    require_login($course, false, $cm);
}


// 기본 파라메터
$getparam   = $CUBBoard->getParameterAll();
$keyfield   = $getparam['keyfield'];
$keyword 	= $getparam['keyword'];
$categoryid	= $getparam['categoryid'];
$bwid 		= $getparam['bwid'];
$page       = $getparam['page'];
$ls         = $getparam['ls'];


// 카테고리 파라메터가 전달이 되더라도 카테고리 기능을 사용하지 않으면 caid는 빈값이 되어야 함.
if ($ubboard->category != 1) {
    $categoryid = null;
}

$ubboardViewUrl = $CFG->wwwroot.'/mod/ubboard/view.php?id='.$cm->id;