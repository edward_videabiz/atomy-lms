<?php
namespace mod_ubboard\core;

defined('MOODLE_INTERNAL') || die();

class qna_ubboard extends ubboard
{
    public static function getSettings() {
        
        global $CFG;
        
        $ubboard_config = parent::getSettings();
        
        $ubboard_config['mailreceive']  =   array('open' => false, 'disable' => false);
        $ubboard_config['reply']        =   array('open' => true,  'disable' => false);
        $ubboard_config['secret']       =   array('open' => true,  'disable' => false);
        
        return $ubboard_config;
    }
}