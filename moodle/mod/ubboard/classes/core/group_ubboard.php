<?php
namespace mod_ubboard\core;

defined('MOODLE_INTERNAL') || die();

class group_ubboard extends ubboard
{
    public static function getSettings() {
        
        global $CFG;
        
        $ubboard_config = parent::getSettings();
        
        $ubboard_config['groupmode']    =   array('open' => 1,     'disable' => false);
        $ubboard_config['reply']        =   array('open' => true,  'disable' => false);
        
        
        return $ubboard_config;
    }
}