<?php
namespace mod_ubboard\core;

defined('MOODLE_INTERNAL') || die();

use renderable;
use stdClass;
use context_module;
use file_storage;
use context;
use repository;
use local_ubion\course\Course;
use local_ubion\controller\Controller;
use local_ubion\base\Common;
use local_ubion\base\Javascript;
use local_ubion\base\Parameter;
use mod_ubboard\factory;

abstract class ubboard extends Controller implements renderable 
{
    const FILEAREA_ATTACHMENT = 'attachment';
    const FILEAREA_ARTICLE = 'article';
    
    protected $cm;
    protected $ubboard;
    protected $context;
    protected $course;
    protected $pluginname = 'ubboard';
    protected $permissions = null;
        
    
    protected $getParameter = array(
        'keyfield'  => null
        ,'keyword'  => null
        ,'ls'       => 15
        ,'page'     => 1
        ,'categoryid' => null
        ,'bwid'=>null
        ,'id'=>null
    );
    
    public function __construct(stdClass $cm, stdClass $ubboard, $isInvoke = true)
    {
        global $DB;
        
        $this->cm = $cm;
        $this->ubboard = $ubboard;
        $this->context = context_module::instance($cm->id);
        
        // intro 설정
        // view페이지에서만 출력되도록 설정함.
        if ($_SERVER['SCRIPT_NAME'] === '/mod/ubboard/view.php') {
            $this->ubboard->intro = format_module_intro('ubboard', $ubboard, $cm->id);
        } else {
            $this->ubboard->intro = '';
        }
        
        // 게시판 타입에 따라서 작업을 해야되는데, mustache에서는 if문에 조건문을 넣을수가 없기 때문에
        // 이곳에서 게시판 관련 설정을 작업해줘야됨.
        $boardType = factory::getTypes();
        $this->ubboard->isBoardType = array();
        foreach  ($boardType as $bkey => $bname) {
            $this->ubboard->isBoardType[$bkey] = $this->ubboard->type == $bkey;
        }
        
        
        // $this->course = $DB->get_record('course', array('id' => $cm->course));
        $this->course = Course::getInstance()->getCourse($cm->course);
        
        $this->permissions = $this->getPermissions();
        
        
        // 공개 컬럼 표시여부
        $this->ubboard->isOpenColumn = false;
        if ($this->isCourseOpen() && $this->permissions->manage) {
            $this->ubboard->isOpenColumn = true;
        }
        
        
        // 상황에따라 parameter값을 조정해야되는 경우가 발생될수 있음.
        // renderer 페이지에 전달된 파라메터를 설정해줌.
        $this->setGetParameter();
    }
    
    
    /**
     * renderer에 전달된 기본 파라메터를 설정해줍니다.
     * @param array $param
     */
    public function setGetParameter()
    {
        // 기본 파라메터 정의
        $id         = optional_param('id', null, PARAM_INT);
        $keyfield   = trim(optional_param('keyfield', '', PARAM_NOTAGS));   // keyfield
        $keyword    = Parameter::getKeyword();
        $categoryid	= optional_param('categoryid', null, PARAM_INT);		    // 카테고리
        $bwid 		= optional_param('bwid', 0, PARAM_INT);
        $page       = optional_param('page', 1, PARAM_INT);     		    // page
        $ls         = optional_param('ls', $this->getDefaultListSize(), PARAM_INT);
        
        $this->getParameter['keyfield'] = $keyfield;
        $this->getParameter['keyword'] = $keyword;
        $this->getParameter['categoryid'] = $categoryid;
        $this->getParameter['bwid'] = $bwid;
        $this->getParameter['page'] = $page;
        $this->getParameter['ls'] = $ls;
        $this->getParameter['id'] = $id;
    }
    
    
    /**
     * 모든 파라메터 리턴
     * @return NULL[]|number[]
     */
    public function getParameterAll()
    {
        return $this->getParameter;
    }
    
    
    /**
     * 특정 파라메터 리턴
     * @param string $name
     * @param string $default
     * @return string|NULL|number
     */
    public function getParameter($name, $default='')
    {
        return $this->getParameter[$name] ?? $default;
    }
    
    
    /**
     * 검색 여부
     *
     * @return boolean
     */
    public function isSearch()
    {
        return (!empty($this->getParameter('keyfield')) && !empty($this->getParameter('keyword'))) ? true : false;
    }
    
    
    public function getKeyfieldOptions()
    {
        $keyfield = $this->getParameter('keyfield');
        
        $options = array();
        $options[] = array('key'=>'subject', 'name'=>get_string('subject', $this->pluginname), 'selected'=>false);
        $options[] = array('key'=>'content', 'name'=>get_string('content', $this->pluginname), 'selected'=>false);
        $options[] = array('key'=>'writer', 'name'=>get_string('writer', $this->pluginname), 'selected'=>false);
        
        foreach ($options as &$os) {
            if ($os['key'] == $keyfield) {
                $os['selected'] = true;
                break;
            }
        }
        
        return $options;
    }
    
    /**
     * course module 정보 리턴
     * @return stdClass
     */
    final public function getCM()
    {
        return $this->cm;
    }
    
    /**
     * 게시판 정보 리턴
     * @return stdClass
     */
    final public function getUBBoard()
    {
        return $this->ubboard;
    }
    
    /**
     * module context 리턴
     * @return context_module
     */
    final public function getContext()
    {
        return $this->context;
    }
    
    /**
     * 강좌 정보 리턴
     * @return stdClass
     */
    final public function getCourse()
    {
        return $this->course;
    }
    
    
    /**
     * renderer 리턴
     *
     * @return \renderer_base
     */
    final public function getRenderer()
    {
        global $PAGE;
        
        return $PAGE->get_renderer('mod_ubboard');
    }
    
    final public function renderer()
    {
        // 해당 함수 사용하기 위한 주의 사항
        // 첫번째 파라메터는 renderer.php 파일에 함수명이 전달되어야 함
        // 예 : renderer.php에서 renderLists인 경우에는 첫번째 파라메터를 lists(또는 Lists)로 전달해줘야됨
        // 이후 파라메터는 renderLists 함수의 파라메터 순서대로 전달해주면 됩니다.
        
        $argsCount  = func_num_args();
        $args = func_get_args();
        
        if ($argsCount > 0) {
            $renderName = 'render'.ucfirst($args[0]);
            
            // render명은 제외하고 전달해줘야됨.
            unset($args[0]);
            
            
            $renderer = $this->getRenderer();
            $renderer->setCUBBoard($this);
            
            return call_user_func_array(array($renderer, $renderName), $args);
        } else {
            Common::printError(get_string('error_render_argument', $this->pluginname));
        }
    }
    
    /**
     * 한화면에 표시될 게시물 갯수
     * 
     * @return number
     */
    public function getDefaultListSize()
    {
        // 게시판에서만 예외적으로 기본 게시물 표시 갯수 변경 요청이 올수 있을거 같아서 미리 함수로 빼놓음
        return Parameter::getDefaultListSize();
    }
    
    
    ##########################
    ######## Category ########
    ##########################
    
    /**
     * 등록된 카테고리 리턴
     * @return array
     */
    public function getCategories()
    {
        global $DB;
        
        $query = "SELECT
    						*
    			FROM 		{ubboard_category}
    			WHERE 		course = :course
    			AND			ubboardid = :ubboardid
    			AND			isdelete = :isdelete
    			ORDER BY 	orders ASC";
        
        return $DB->get_records_sql($query, array('course'=>$this->course->id, 'ubboardid'=>$this->ubboard->id, 'isdelete'=>0));
    }
    
    public function _getCategories($categories)
    {
        if (!empty($categories)) {
            $categoryid = $this->getParameter('categoryid');
            
            foreach ($categories as $c) {
                $c->isSelected = ($c->id == $categoryid) ? true : false;
            }
            
            // mustache로 전달할때에는 key값을 제거해줘야됨.
            $categories = array_values($categories);
        } else {
            // 파라메터로 전달된 $categories 값이 null이나 빈값이면 강제로 배열로 만들어줘야지
            // array_unshift시 warning이 안떨어짐.
            if (!is_array($categories)) {
                $categories = array();
            }
        }
        
        // 전체 항목 추가해주기
        $total = new stdClass();
        $total->id = null;
        $total->name = get_string('category_all', $this->pluginname);
        
        array_unshift($categories, $total);
        
        return $categories;
    }
    
    
    /**
     * 기본 카테고리 명칭
     * @return string
     */
    public static function getDefaultCategoryName()
    {
        return get_string('category_defaultname', 'ubboard');
    }
    
    
    /**
     * 게시판에 생성된 기본 카테고리 정보를 가져옵니다.
     *
     * @return stdClass
     */
    final function getDefaultCategory() {
        global $DB;
        
        $query = "SELECT
    						*
    			FROM		{ubboard_category}
    			WHERE		course = :course
    			AND			ubboardid = :ubboardid
    			AND			basic = 0";
        
        return $DB->get_record_sql($query, array('course'=>$this->course->id, 'ubboardid'=>$this->ubboard->id, 'basic'=>0));
    }
    
    /**
     * 카테고리 확인
     * 
     * @param number $id
     * @return mixed|boolean
     */
    public function getCategory($id)
    {
        global $DB;
        
        return $DB->get_record_sql("SELECT * FROM {ubboard_category} WHERE id = :id", array('id'=>$id));
    }
    
    
    public function doCategoryInsert()
    {
        global $CFG;
        
        if ($this->isPermission('manage')) {
            $name = Parameter::post('name', null, PARAM_NOTAGS);
            
            $validate = $this->validate()->make(array(
                'name' => array(
                    'required'
                    ,$this->validate()::PLACEHOLDER => get_string('category_name', $this->pluginname)
                    ,$this->validate()::PARAMVALUE => $name
                )
            ));
            
            if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
            } else {    
                if ($this->setCategoryInsert($name)) {
                    redirect($CFG->wwwroot.'/mod/ubboard/category.php?id='.$this->cm->id);
                } else {
                    Javascript::printAlert(get_string('error_fail_category', 'ubboard'));
                }
            
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('manage'), $this->pluginname)));
        }
    }
    
    
    public function setCategoryInsert($name) 
    {
        global $DB;
        
        $category = new stdClass();
        $category->course = $this->course->id;
        $category->ubboardid = $this->ubboard->id;
        $category->name = $name;
        $category->timecreated = time();
        
        $sortorder = $DB->get_field_sql('SELECT MAX(orders) FROM {ubboard_category} WHERE course=:course AND ubboardid = :ubboardid', array('course'=>$category->course, 'ubboardid'=>$category->ubboardid));
        if (empty($sortorder)) {
            $sortorder = 1;
        } else {
            $sortorder += 1;
        }
        $category->orders = $sortorder;
        
        if ($category->id = $DB->insert_record('ubboard_category', $category)) {
        
            $params = array(
                'context' => $this->context
                ,'objectid' => $category->id
            );
            
            $event = \mod_ubboard\event\category_created::create($params);
            $event->add_record_snapshot('ubboard', $this->ubboard);
            //$event->add_record_snapshot('ubboard_category', $category);
            $event->trigger();
            
            return $category->id;
        } 
        
        return false;
    }
    
    
    public function doCategoryUpdate()
    {
        global $CFG;
        
        if ($this->isPermission('manage')) {
            $categoryid = Parameter::post('categoryid', null, PARAM_INT);
            $name = Parameter::post('name', null, PARAM_NOTAGS);
            
            $validate = $this->validate()->make(array(
                'name' => array(
                    'required'
                    ,$this->validate()::PLACEHOLDER => get_string('category_name', $this->pluginname)
                    ,$this->validate()::PARAMVALUE => $name
                )
                ,'categoryid' => array(
                    'required'
                    ,$this->validate()::PLACEHOLDER => get_string('categoryid', $this->pluginname)
                    ,$this->validate()::PARAMVALUE => $categoryid
                )
            ));
            
            if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
            } else {
                $this->setCategoryUpdate($categoryid, $name);
                redirect($CFG->wwwroot.'/mod/ubboard/category.php?id='.$this->cm->id);
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('manage'), $this->pluginname)));
        }
    }
    
    
    public function setCategoryUpdate($categoryid, $name)
    {
        global $DB;
        
        $category = new stdClass();
        $category->id = $categoryid;
        $category->name = $name;
        $category->timemodified = time();
        
        $DB->update_record('ubboard_category', $category);
        
        $params = array(
            'context' => $this->context
            ,'objectid' => $category->id
        );
        $event = \mod_ubboard\event\category_updated::create($params);
        $event->add_record_snapshot('ubboard', $this->ubboard);
        //$event->add_record_snapshot('ubboard_category', $category);
        $event->trigger();
    }
    
    
    public function doCategoryDelete()
    {
        global $CFG;
        
        if ($this->isPermission('manage')) {
            $categoryid = Parameter::post('categoryid', null, PARAM_INT);
            
            $validate = $this->validate()->make(array(
                'categoryid' => array(
                    'required'
                    ,$this->validate()::PLACEHOLDER => get_string('categoryid', $this->pluginname)
                    ,$this->validate()::PARAMVALUE => $categoryid
                )
            ));
        
            if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
            } else {    
                if ($category = $this->getCategory($categoryid)) {
                    if (empty($category->basic)) {
                        Javascript::printAlert(get_string('error_notdelete_category', $this->pluginname));
                    } else {
                        $this->setCategoryDelete($category);
                        
                        redirect($CFG->wwwroot.'/mod/ubboard/category.php?id='.$this->cm->id);
                    }
                } else {
                    Javascript::printAlert(get_string('error_category', $this->pluginname));
                }
            
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('manage'), $this->pluginname)));
        }
    }
    
    
    public function doCategorySortorder()
    {
        global $CFG;
        
        if ($this->isPermission('manage')) {
            $sortorder = Parameter::post('json', null, PARAM_NOTAGS);
            
            if (!empty($sortorder)) {
                $sortorder = json_decode($sortorder);
                
                $categorySortorder = array();
                $order = 1;
                foreach ($sortorder[0] as $sr) {
                    $categorySortorder[$order] = $sr->id;
                    $order++;
                }
                
                $this->setCategorySortorder($categorySortorder);
            }
            
            if (Parameter::isAajx()) {
                Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'msg'=>get_string('update_complete', 'local_ubion')));
            } else {
                redirect($CFG->wwwroot.'/mod/ubboard/category.php?id='.$cm->id);
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('manage'), $this->pluginname)));
        }
        exit;
    }
    
    
    public function setCategorySortorder($sortorder)
    {
        global $DB;
        
        if (!empty($sortorder)) {
            // key : 순서
            // value : categoryid
            foreach ($sortorder as $order => $categoryid) {
                $update = new stdClass();
                $update->id = $categoryid;
                $update->orders = $order;
                $DB->update_record('ubboard_category', $update);
            }
            
            $params = array(
                'context' => $this->context
                ,'objectid' => $categoryid
            );
            $event = \mod_ubboard\event\category_sortabled::create($params);
            $event->add_record_snapshot('ubboard', $this->ubboard);
            $event->trigger();
        }
    }
    
    public function setCategoryDelete(stdClass $category)
    {
        global $DB;
        
        // 기본 카테고리는 무조건 존재함.
        $defaultCategory = $this->getDefaultCategory();
        
        
        // 삭제하려던 카테고리에 속한 게시물 기본 카테고리로 이동시키기.
        $query = "UPDATE    {ubboard_write} SET
						    categoryid = :defaultcategoryid
				WHERE	    course = :course
                AND         ubboardid = :ubboardid
				AND		   categoryid = :oldcategoryid";
        
        
        $DB->execute($query, array('defaultcategoryid'=>$defaultCategory->id, 'course'=>$category->course, 'oldcategoryid'=>$category->id, 'ubboardid'=>$this->ubboard->id));
        
        
        // 카테고리 삭제
        if ($DB->delete_records('ubboard_category', array('id'=>$category->id))) {
            $params = array(
                'context' => $this->context
                ,'objectid' => $category->id
            );
            $event = \mod_ubboard\event\category_deleted::create($params);
            $event->add_record_snapshot('ubboard', $this->ubboard);
            $event->trigger();
            
            return true;
        } else {
            return false;
        }
    }
    
    
    public function doCheckMoveArticles()
    {
        global $CFG, $DB;
        
        // 관리 권한이 존재하는지 확인
        if ($this->isPermission('manage')) {
            // 필수값 검사
            $articleNums = Parameter::postArray('manage', null, PARAM_INT);
            $categoryid = Parameter::post('categoryid', -1, PARAM_INT);
            
            if (empty($categoryid)) {
                $defaultCategory = $this->getDefaultCategory();
                $categoryid = $defaultCategory->id;
            }
            
            $count = count($articleNums);
            
            $changeCount = 0;
            $isParentArticle = false;
            
            if ($count > 0) {
                foreach ($articleNums as $bwid) {
                    if ($article = $this->getArticle($bwid, true)) {
                        if (!$article->parent) {
                            // 최상위 글이므로 하위 글들도 카테고리를 전부 변경시켜줘야됨.
                            $query = "UPDATE	{ubboard_write} SET
        								        categoryid = :categoryid
        							WHERE	    course = :course
        							AND		    ubboardid = :ubboardid
        							AND		    num = :num";
                            
                            $DB->execute($query, array('categoryid'=>$categoryid, 'num'=>$article->num, 'course'=>$article->course, 'ubboardid'=>$article->ubboardid));
                            
                            $changeCount++;
                        } else {
                            $isParentArticle = true;
                        }
                    }
                }
            }
            
            if ($isParentArticle) {
                if (empty($changeCount)) {
                    Javascript::printAlert(get_string('cannot_move_top_article', $this->pluginname));
                } else {
                    Javascript::printAlert(get_string('cannot_move_some_article', $this->pluginname));
                }
            }
            
            if ($changeCount > 0) {
                $params = array(
                    'context' => $this->context,
                    'objectid' => $this->ubboard->id
                );
                $event = \mod_ubboard\event\article_moved::create($params);
                $event->add_record_snapshot('ubboard', $this->ubboard);
                $event->trigger();
            }
            
            
            redirect($CFG->wwwroot.'/mod/ubboard/view.php?id='.$this->cm->id);
            
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('manage'), 'ubboard')));
        }
        
        
    }
    
    
    
    #############################
    ######## 게시물 목록 ########
    #############################
    
    /**
     * 게시판 목록에 사용되는 DB 컬럼명을 리턴해줍니다.
     *
     * @param array $addField
     * @return string
     */
    public function getListFields($addField = array())
    {
        $usernamefield = get_all_user_name_fields(true, 'u');
        
        $field = "uw.id
                ,uw.course
                ,uw.groupid
                ,uw.ubboardid
                ,uw.categoryid
                ,uw.num
                ,uw.reply
                ,uw.parent
                ,uw.notice
                ,uw.secret
                ,uw.userid
                ,uw.puserid
                ,uw.name
                ,uw.subject
                ,uw.content
                ,uw.file_cnt
                ,uw.view_cnt
                ,uw.comment_cnt
                ,uw.timecreated
                ,uw.timemodified
                ,uw.isdelete
                ,uw.open
                ,u.idnumber";
        
        $field .= ','.$usernamefield;
        
        if (!empty($addField)) {
            $field .= join(' ,', $addField);
        }
        
        return $field;
    }
    
    
    /**
     * 공지사항 목록 리턴
     * @return array
     */
    public function getNoticeLists()
    {
        global $DB, $USER;
        
        // 공지사항이기 때문에 따로 페이징 처리하지 않습니다.
        // 필수 쿼리
        $query = "SELECT
							".$this->getListFields()."
				FROM		{ubboard_write} uw
				JOIN		{user} u ON u.id = uw.userid
				WHERE		course 	    = :course
				AND			ubboardid 	= :ubboardid
				AND			notice 	    = :notice
				AND			isdelete    = 0
				AND			u.deleted   = 0";
        
        $param = array(
            'course'        => $this->course->id
            ,'ubboardid'    => $this->ubboard->id
            ,'notice'       => 1
        );
        
        $addQuery = array();
        
        // 카테고리를 사용하고 카테고리 파라메터값이 존재할 경우.
        if (!empty($this->ubboard->category) && !empty($categoryid)) {
            $addQuery[] = " AND categoryid = :categoryid";
            $param['categoryid'] = $categoryid;
        }
        
        
        // 검색어가 존재할 경우.
        if (!empty($keyfield) && !empty($keyword)) {
            if ($keyfield == 'writer') {
                $fullname = $DB->sql_fullname('u.firstname','u.lastname');
                
                $addQuery[] = " AND ".$DB->sql_like('LOWER('.$fullname.')', ':keyword');
            } else {
                $addQuery[] = " AND ".$DB->sql_like('LOWER('.$keyfield.')', ':keyword');
            }
            
            // 검색인 경우 비밀글은 검색되면 안됨..
            if (!is_siteadmin() && !$this->isPermission('manage')) {
                $addQuery[] = " AND uw.secret = 0";
            }
            
            $param['keyword'] = '%'.strtolower($keyword).'%';
        }
        
        // 공개글 허용인 경우
        if ($this->isCourseOpen()) {
            // 관리 권한이 없으면 공개 또는 본인이 작성한 게시물만 보여야됨.
            if (!$this->isPermission('manage')) {
                $addQuery[] = " AND (uw.open = 1 OR uw.userid = :openuserid)";
                $param['openuserid'] = $USER->id;
            }
        }
        
        
        // 전체 게시물 수
        if (!empty($addQuery)) {
            $query .= join(' ', $addQuery);
        }
        
        // 게시판 ordertype에 따라서 정렬 순서가 바뀜.
        // 0 : 작성일.
        // 1 : 수정일.
        if ($this->ubboard->ordertype == 0) {
            $sorttype = ' uw.timecreated ';
        } else {
            $sorttype = ' uw.timemodified ';
        }
        
        // 답변기능을 사용하면 공지사항 게시판 여부를 떠나서 $sorttype(글 정렬 방법) 순서를 사용하면 안됨. 
        if ($this->ubboard->reply == 1) {
            $sortorder = " ORDER BY num DESC, reply, ".$sorttype." DESC";
        } else {
            $sortorder = ' ORDER BY '.$sorttype.' DESC, num DESC, reply';
        }
        
        // error_log($query.$sortorder);
        return $DB->get_records_sql($query.$sortorder, $param);
    }
    
    /**
     * 선택된 게시물 영역에 표시되어야 할 버튼 목록
     * @return stdClass[]
     */
    public function getSelectedButtons()
    {
        global $CFG;
        
        $buttons = array();
        
        $defaultClass = 'btn btn-default btn-sm';
        
        // 카테고리 이동
        if ($this->ubboard->category) {
            $buttons[] = $this->setButton(get_string('btn_move', 'ubboard'), '#', $defaultClass.' btn-selected-category');
        }
        
        // 삭제
        $buttons[] = $this->setButton(get_string('btn_delete', 'ubboard'), '#', $defaultClass.' btn-selected-delete');
        
        // 공유
        if ($this->isShare()) {
            $buttons[] = $this->setButton(get_string('btn_share', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/_share_all.php?id='.$this->cm->id.'&boardtype='.$this->ubboard->type, $defaultClass.' btn-selected-share');
        }
        
        // 공개
        if ($this->isCourseOpen()) {
            $buttons[] = $this->setButton(get_string('btn_open', 'ubboard'), '#', $defaultClass.' btn-selected-open');
        }
        
        
        return $buttons;
    }
    
    
    /**
     * 게시판 목록 리턴 (DB 데이터만 존재합니다.)
     *
     * @param number $categoryid
     * @param string $keyfield
     * @param string $keyword
     * @param number $page
     * @param number $ls
     * @return array
     */
    public function getLists()
    {
        global $DB, $USER;
        
        // 기본 파라메터 (강좌번호, 게시판 번호)
        $param = array(
            'course'       => $this->course->id
            ,'ubboardid'    => $this->ubboard->id
            ,'notice'       => 0
        );
        
        // 필수 쿼리
        $query = "SELECT
							".self::REPLACE."
				FROM		{ubboard_write} uw
				JOIN		{user} u ON u.id = uw.userid
				WHERE		course       = :course
				AND			ubboardid 	 = :ubboardid
				AND			notice       = :notice
				AND			isdelete     = 0
                AND			u.deleted = 0";
        
        $categoryid = $this->getParameter('categoryid');
        $keyfield = $this->getParameter('keyfield');
        $keyword = $this->getParameter('keyword');
        $page = $this->getParameter('page', 1);
        $ls = $this->getParameter('ls', $this->getDefaultListSize());
        
        
        $addQuery = array();
        
        // 카테고리를 사용하고 카테고리 파라메터값이 존재할 경우.
        if (!empty($this->ubboard->category) && !empty($categoryid)) {
            $addQuery[] = " AND categoryid = :categoryid";
            $param['categoryid'] = $categoryid;
        }
        
        
        // 검색어가 존재할 경우.
        if (!empty($keyfield) && !empty($keyword)) {
            if ($keyfield == 'writer') {
                $fullname = $DB->sql_fullname('u.firstname','u.lastname');
                
                $addQuery[] = " AND ".$DB->sql_like('LOWER('.$fullname.')', ':keyword');
            } else {
                $addQuery[] = " AND ".$DB->sql_like('LOWER('.$keyfield.')', ':keyword');
            }
            
            // 검색인 경우 비밀글은 검색되면 안됨..
            if (!is_siteadmin() && !$this->isPermission('manage')) {
                $addQuery[] = " AND uw.secret = 0";
            }
            
            $param['keyword'] = '%'.strtolower($keyword).'%';
        }
        
        // 공개글 허용인 경우
        if ($this->isCourseOpen()) {
            // 관리 권한이 없으면 공개 또는 본인이 작성한 게시물만 보여야됨.
            if (!$this->isPermission('manage')) {
                $addQuery[] = " AND (uw.open = 1 OR uw.userid = :openuserid)";
                $param['openuserid'] = $USER->id;
            }
        }
        
        
        // 전체 게시물 수
        if (!empty($addQuery)) {
            $query .= join(' ', $addQuery);
        }
        $totalquery = str_replace(self::REPLACE, 'COUNT(1)', $query);
        $totalcount = $DB->get_field_sql($totalquery, $param);
        
        
        
        // 게시판 ordertype에 따라서 정렬 순서가 바뀜.
        // 0 : 작성일.
        // 1 : 수정일.
        if ($this->ubboard->ordertype == 0) {
            $sorttype = ' uw.timecreated ';
        } else {
            $sorttype = ' uw.timemodified ';
        }
        
        // 답변글을 사용하는 게시판인 경우에는 입력/수정일에 대해서 정렬 기능을 제공하지 않음.
        if ($this->ubboard->reply == 1) {
            $sortorder = " ORDER BY num DESC, reply, ".$sorttype." DESC";
        } else {
            $sortorder = ' ORDER BY '.$sorttype.' DESC, num DESC, reply';
        }
        
        
        // 가져올 게시물 갯수
        $page = ($page <= 0) ? 1 : $page;
        $limitfrom = $ls * ($page - 1);
        
        $query = str_replace(self::REPLACE, $this->getListFields(), $query);
        
        // error_log($query);
        // error_log(print_r($param, true));
        $lists = $DB->get_records_sql($query.$sortorder, $param, $limitfrom, $ls);
        
        // 게시물 가져올때 카테고리 명칭까지 가져오는것보다 카테고리만 따로 가져오는게 좋을거 같음.
        $categories = null;
        if ($this->ubboard->category) {
            $categories = $this->getCategories();
        }
        
        return array($totalcount, $lists, $categories);
    }
    
    
    public function _getLists($totalcount, $articles, $categories, $bwid=null)
    {
        global $USER;
        
        $filecount = 0;
        
        if (!empty($articles)) {
            $keyfield = $this->getParameter('keyfield');
            $keyword = $this->getParameter('keyword');
            
            $anonymous = get_string('anonymous', $this->pluginname);
            
            $isView = $this->isPermission('view');
            
            // 검색 여부
            $isSearch = $this->isSearch();
            
            // 마지막 로그인 시간
            $currentLogin = $USER->currentlogin ?? 0;
            
            $strOpenY = get_string('open_1', 'ubboard');
            $strOpenN = get_string('open_0', 'ubboard');
            
            $page = $this->getParameter['page'] ?? 1;
            $ls = $this->getParameter['ls'] ?? $this->getDefaultListSize();
            
            $sequentialnum = $totalcount - (($page - 1) * $ls);
            
            // 게시물에 링크를 걸어줘야되는데 $this->getParameter에 반영을 하면 bwid가 반영되기 때문에 $this->getParameter를 복사해서 사용해야됨.
            $defaultParameter = $this->getParameter;
            foreach ($articles as $a) {
                
                // 게시물 번호
                // $a->number = number_format($this->getListArticleNum($a->num, $sequentialnum));
                $a->number = $sequentialnum;
                
                // 링크 및 게시글에 대해서 추가적으로 붙어줘야될 classname
                $a->addclassname = '';
                
                $a->isCurrent = ($bwid == $a->id) ? true : false;
                $a->trclass = ($a->isCurrent) ? 'active' : '';
                
                // 작성일을 y-m-d형식으로
                $a->date = date('Y-m-d', $a->timecreated);
                $a->fulldate = userdate($a->timecreated);
                
                // view 권한이 존재할 경우에만 링크 걸어주기
                $a->link = null;
                
                // 검색이 존재하고 검색 항목이 제목인 경우, 제목에 검색된 단어 강조해주기
                if ($isSearch && $keyfield == 'subject') {
                    $a->subject = $this->getSearchString($keyword, $a->subject);
                }
                
                if ($isView) {
                    $defaultParameter['bwid'] = $a->id;
                    $a->link = new \moodle_url('/mod/ubboard/article.php', $defaultParameter);
                    // $a->subject = '<a href="'.$CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$a->id.'">'.$a->subject.'</a>';
                    // $a->subject = html_writer::link(new \moodle_url('/mod/ubboard/article.php', $defaultParameter), $a->subject);
                }
                
                // 최근에 작성된 글인지 확인
                // $a->is_new = $this::isNewArticle($a->date, self::$is_new_day);
                
                // 카테고리 기본 명칭
                $a->categoryname = self::getDefaultCategoryName();
                // 카테고리를 사용하는 경우에만 해당 게시물에 대해서 실제 카테고리 명칭을 부여해줌.
                if ($this->ubboard->category) {
                    if (!empty($a->categoryid) && array_key_exists($a->categoryid, $categories)) {
                        $a->categoryname = $categories[$a->categoryid]->name;
                    }
                }
                
                // 익명 게시판이면 익명으로 출력되어야 함.
                $fullname = $anonymous;
                if ($this->ubboard->anonymous) {
                    if (!empty($a->name)) {
                        $fullname = $a->name;
                    }
                } else {
                    if (!empty($a->userid)) {
                        $fullname = $this->getUserFullname($a);
                    }
                }
                $a->fullname = $fullname;
                
                // 첨부파일 모두 다운로드 관련 기능때문에 파일이 존재하는지 확인
                $filecount += $a->file_cnt;
                
                
                // 게시글 update 시간 비교하기
                // 로그인 되어 있고, 마지막 로그인 시간 이후에 등록된 글이 존재하는지 확인
                $a->isupdate = false;
                if (isloggedin()) {
                    if ($a->timemodified > $currentLogin) {
                        $a->isupdate = true;
                    }
                }
                
                // 답변글인 경우.
                $a->leftmargin = ($this->ubboard->reply) ? strlen($a->reply) * 15 : 0;
                
                // 비밀글인 경우...
                if ($a->secret) {
                    $isSecretView = $this->isSecretArticleView($a);
                    
                    // 비밀글 볼 권한이 없는 경우.
                    if (!$isSecretView) {
                        $a->subject = get_string('secret_article', $this->pluginname);
                        $a->link = null;
                        $a->addclassname .= " secret ";
                    }
                }
                
                // 그룹 게시판인 경우 (group_ubboard.php에서 override해서 처리하는게 가장 베스트이지만, 관리해야될 항목이 많아져서 이곳에서 처리함.)
                if ($this->ubboard->isBoardType['group']) {
                    if ($a->groupid <= 0) {
                        $a->groupname = get_string('allparticipants');
                    }
                }
                
                // 공개여부
                $a->openname = ($a->open) ? $strOpenY : $strOpenN;
                
                $sequentialnum--;
            }
        }
        
        return array($articles, $filecount);
    }
    
    
    /**
     * 화면에 표시될 게시물 번호
     * 
     * @param number $articlenum  ubboard_write->num
     * @param number $sequentialnum 순차적인 번호
     * @return number
     */
    public function getListArticleNum($articlenum, $sequentialnum)
    {
        return $articlenum;
    }
    
    
    
    public function doArticleLists()
    {
        global $PAGE, $CFG;
        
        // 필수값 검사.
        
        $PAGE->set_context($this->context);
        
        $bwid = $this->getParameter('bwid');
        $page = $this->getParameter('page', 1);
        $ls = $this->getParameter('ls', $this->getDefaultListSize());
        
        // list 정보 가져오기
        $lists = array();
        list($totalcount, $articles, $categories) = $this->getLists();
        if ($totalcount > 0) {
            list ($lists, $filecount) = $this->_getLists($totalcount, $articles, $categories, $bwid);
            
            // mustache에서는 key가 부여되어 있으면 안됨.
            $lists = array_values($lists);
        }
        
        $buttons = new stdClass();
        $buttons->category_manage = $this->setButton(get_string('category_manage', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/category.php?id='.$this->cm->id);
        
        $mustache = new stdClass();
        $mustache->lists = $lists;
        $mustache->icons = $this->getIcons();
        $mustache->paging = \local_ubion\base\Paging::getPaging($totalcount, $page, $ls);
        $mustache->colspan = $this->getColspan();
        $mustache->selectedButtons = $this->getSelectedButtons();
        $mustache->categories = $this->_getCategories($categories);
        $mustache->buttons = $buttons;
        
        
        $html = $this->renderer('articleLists', $mustache);
        
        Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'html'=>$html));
    }
    
    /**
     * 게시판에서 사용되는 아이콘 목록..
     * @return \stdClass
     */
    public function getIcons()
    {
        global $OUTPUT;
        
        // icon
        $icons = new stdClass();
        $icons->notice = $OUTPUT->image_url('icon/notice', 'ubboard');
        $icons->reply = $OUTPUT->image_url('icon/reply', 'ubboard');
        $icons->new = $OUTPUT->image_url('icon/new', 'ubboard');
        $icons->secret = $OUTPUT->image_url('icon/secret', 'ubboard');
        $icons->file = $OUTPUT->image_url('icon/disk', 'ubboard');
        $icons->update = $OUTPUT->image_url('icon/up', 'ubboard');
        $icons->category = $OUTPUT->image_url('icon/category', 'ubboard');
        $icons->sortorder = $OUTPUT->image_url('t/move');
        
        return $icons;
    }
    
        
    
    
    /**
     * 등록된 글 또는 조회된 글이 없을때 셀 병합될 갯수
     * @return number
     */
    public function getColspan()
    {
        // 기본은 5개
        // 번호, 제목, 작성자, 작성일, 조회수
        $colspan = 5;
        
        // 체크박스
        if ($this->isPermission('manage')) {
            $colspan++;
        }
        
        // 카테고리 기능을 사용하는 경우
        if ($this->ubboard->category) {
            $colspan++;
        }
        
        // 그룹 기능을 사용하는 경우
        if ($this->ubboard->isBoardType['group']) {
            $colspan++;
        }
        
        // 공개여부를 사용하는 경우 (단 관리자만 표시되어야 함)
        if ($this->isCourseOpen() && $this->isPermission('manage')) {
            $colspan++;
        }
        
        return $colspan;
    }
    
    
    /**
     * 버튼 객체 생성
     * 
     * @param string $text
     * @param string $url
     * @param string $class
     * @param array $parameter
     * @return \stdClass
     */
    public function setButton($text, $url, $class='', array $parameter = array())
    {
        $button = new stdClass();
        $button->text = $text;
        $button->url = $url;
        $button->class = $class;
        
        if (!empty($parameter)) {
            $symbol = '';
            $addurl = '';
            foreach ($parameter as $key => $value) {
                if (!empty($value)) {
                    $addurl .= $symbol.$key.'='.$value;
                    
                    $symbol = '&';
                }
            }
            
            if (!empty($addurl)) {
                if (strpos($url, '?') !== false) {
                    $url = $url.'&'.$addurl;
                } else {
                    $url = $url.'?'.$addurl;
                }
            }
            
            $button->url = $url;
        }
        
        return $button;
    }
    
    
    public function doInsert()
    {
        global $CFG, $PAGE;
        
        $rnum = Parameter::post('rnum', 0, PARAM_INT);

        // 글쓰기, 답변에따라서 체크해야될 권한이 다름.
        $checkAuth = 'write';
        if ($this->ubboard->reply && !empty($rnum)) {
            $checkAuth = 'reply';
        }
        
        // 권한 검사
        if ($this->isPermission($checkAuth)) {
            if ($this->validateDefaultItem()) {
                
                list ($isError, $errorMessage, $uw) = $this->_getWriteObject();
                
                if ($isError) {
                    Javascript::printAlert($errorMessage);
                } else {
                    $isAllMailSend = Parameter::post('allmailsend', 0, PARAM_INT);
                    
                    if ($bwid = $this->setInsert($uw, $isAllMailSend)) {
                        redirect($CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$bwid);
                    } else {
                        Javascript::printAlert(get_string('error_fail_write', 'ubboard'));
                    }
                }
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName($checkAuth), 'ubboard')));
        }
    }
    
    /**
     * 게시판 타입이 여러개이다보니 setInsert나 doInsert를 override하는 경우 
     * 로직 변경이 이루어질때 수정되어야 할 파일들이 증가되는 문제가 있어서
     * 기본 항목에 대해서 셋팅을 해주고 각 게시판마다 특정 항목에 대해서만 값을 재 설정해주도록 처리함.
     * 
     * @return boolean[]|NULL[]|string[]|\stdClass[]
     */
    protected function _getWriteObject()
    {
        global $USER, $DB, $CFG;
        require_once($CFG->libdir.'/filelib.php');
        
        $isError = false;
        $errorMessage = null;
        
        $subject = Parameter::post('subject', null, PARAM_NOTAGS);
        $content = $this->getParamContent();
        $notice = Parameter::post('notice', 0, PARAM_INT);
        $secret = Parameter::post('secret', 0, PARAM_INT);
        $rnum = Parameter::post('rnum', 0, PARAM_INT);
        $categoryid = Parameter::post('categoryid', 0, PARAM_INT);
        $groupid = Parameter::post('groupid', 0, PARAM_INT);
        // 첨부파일 draftid
        $attachment = Parameter::post('attachment', null, PARAM_ALPHANUMEXT);
        $reply = Parameter::post('reply', 0, PARAM_INT);
        
        // 답변글을 지원하지 않는 게시판인 경우
        $isReply = false;
        if (empty($this->ubboard->reply)) {
            $isReply = false;
        } else {
            if (!empty($rnum)) {
                $isReply = true;
            }
        }
        
        $num = 0;
        // 답글이 아닌 경우에만 num값을 새로 부여해야됨.
        if (!$isReply) {
            // 신규글 등록이기 때문에 새로운 글번호를 할당 받아야됨.
            $num = $this->getArticleNextNum();
        }
        
        
        // 해당 함수는 웹서비스에서도 호출이 될수 있기 때문에 get, post 값을 가져다가 사용하시면 안됩니다.
        $ubboardWrite = new stdClass();
        $ubboardWrite->course = $this->course->id;
        $ubboardWrite->ubboardid = $this->ubboard->id;
        
        // 제목, 내용
        $ubboardWrite->subject = $subject;
        $ubboardWrite->content = $content;
        
        
        // 글 작성자
        $ubboardWrite->userid = $USER->id;
        
        
        // 답변글을 지원하는지 확인해봐야됨.
        if (empty($rnum) || empty($this->ubboard->reply)) {
            
            // 게시판내의 번호
            $ubboardWrite->num = $num;
            
            $ubboardWrite->puserid = $USER->id;
            
            // 게시판에서 그룹 모드를 사용하는 경우
            $groupmode = groups_get_activity_groupmode($this->cm);
            if ($groupmode) {
                // 0값이 나오면 -1값으로 등록해줘야되기 때문에 empty 형식으로 조건 검사하는게 좋을듯.
                if (empty($groupid)) {
                    // 관리 권한이 있는 사용자인 경우에만 -1로 설정, 그 이외에는 본인 그룹으로 작성되어야 함.
                    if ($this->isPermission('manage')) {
                        $groupid = -1;
                    } else {
                        $groupid = groups_get_activity_group($this->cm);
                    }
                }
            } else {
                $groupid = -1;
            }
            $ubboardWrite->groupid = $groupid;
            
            
            // 카테고리 번호가 전달되고, 카테고리를 사용하고 있는 게시판인 경우
            if (!empty($categoryid) && $this->ubboard->category) {
                // 전달된 카테고리 번호가 실제 게시판내에서 사용중인 카테고리 번호인지 확인
                $categoryinfo = $this->getCategory($categoryid);
            } else {
                // 기본 카테고리에 저장
                $categoryinfo = $this->getDefaultCategory();
            }
            
            if (!empty($categoryinfo) && $categoryinfo->ubboardid == $this->ubboard->id) {
                $ubboardWrite->categoryid = $categoryinfo->id;
            } else {
                $isError = true;
                $errorMessage = get_string('error_category', $this->pluginname, $categoryid);
            }
            
        } else {
            if ($parentAticle = $this->getArticle($rnum)) {
                
                // 본인과 관련된 답변글인지 확인 해봐야됨.
                // 원본글이 비밀글이면 본인 권한 체크해야됨.
                if (!$this->isPermission('manage')) {
                    // 비밀글인 경우
                    if ($parentAticle->secret) {
                        // 대상글이 본인과 관련된 글이 아니라면 에러 출력해줘야됨.
                        if (!$this->isSecretArticleView($parentAticle)) {
                            $isError = true;
                            $errorMessage = get_string('secret_not_reply', 'ubboard');
                        }
                    }
                }
                
                // 답변글은 최대 26단계까지 허용 가능함.
                
                $ubboardWrite->num = $parentAticle->num;
                $ubboardWrite->parent = $parentAticle->id;
                $ubboardWrite->puserid = $parentAticle->puserid;
                $ubboardWrite->groupid = $parentAticle->groupid;
                
                $reply = $this->getReplyLevel($parentAticle->num, $parentAticle->reply);
                if ($reply !== false) {
                    $ubboardWrite->reply = $reply;
                } else {
                    $isError = true;
                    $errorMessage = get_string('error_maxreply', $this->pluginname);
                }
                
                // 원본글의 카테고리와 동일하게 맞춰줘야됨.
                $ubboardWrite->categoryid = $parentAticle->categoryid;
            } else {
                $isError = true;
                $errorMessage = get_string('error_parent_article', $this->pluginname);
            }
        }
        
        // 공지사항 => 공지사항은 관리 권한을 가진 사용자만 작성할수 있음.
        if ($this->ubboard->notice && $this->isPermission('manage')) {
            $ubboardWrite->notice = $notice;
            
            // 기본값 설정
            $stime = $etime = 0;
            
            // 공지사항 게시판이고, 공지사항이 체크 된 경우
            if ($this->ubboard->isBoardType['notice'] && $notice) {
                
                // 기본은 무들 폼에의해서 전달되기 때문에 배열 형태임.
                $stime = Parameter::postArray('stime', 0, PARAM_INT);
                $etime = Parameter::postArray('etime', 0, PARAM_INT);
                
                // 시작일
                if (is_array($stime)) {
                    $stime = Common::getArrayToUnixtime($stime);
                } else {
                    $stime = Parameter::post('stime', 0, PARAM_INT);
                }
                
                // 종료일
                if (is_array($etime)) {
                    $etime = Common::getArrayToUnixtime($etime);
                } else {
                    $etime = Parameter::post('etime', 0, PARAM_INT);
                }
            }
            $ubboardWrite->stime = $stime;
            $ubboardWrite->etime = $etime;
        }
        
        // 비밀글
        if ($this->ubboard->secret) {
            $ubboardWrite->secret = $secret;
        }

        
        // 공개여부
        // 기본값은 1로 설정
        $open = 1;  
        if ($this->isCourseOpen()) {
            $openDefault = $this->getOpenDefault();
            
            // 공개여부를 사용하는 강좌인 경우에는 전달된 파라메터로 설정되어야 함.
            $open = Parameter::post('open', $openDefault, PARAM_INT);
        }
        $ubboardWrite->open = $open;
        
        // 익명 게시판인 경우 닉네임을 지정할수 있음.
        if ($this->ubboard->anonymous) {
            $nickname = Parameter::post('nickname', null, PARAM_NOTAGS);
            
            if (!empty($nickname)) {
                $ubboardWrite->name = $nickname;
            }
        }
        
        $ubboardWrite->timecreated = time();
        $ubboardWrite->ip = getremoteaddr();
        
        // db에는 저장되지 않지만 디비에 저장될때 필요한 변수들..
        $ubboardWrite->_attachment = $attachment;
        
        return array($isError, $errorMessage, $ubboardWrite);
    }
    
    
    
    /**
     * 글쓰기
     * 
     * @param stdClass $uw
     * @param boolean $isAllMailSend
     * @return string|boolean
     */
    public function setInsert($uw, $isAllMailSend=false)
    {
        global $DB;
        
        // 글 내용이 에디터에서 작성된 경우
        $editorContent = null;
        if (is_array($uw->content)) {
            $editorContent = $uw->content;
            $uw->content = ''; // 배열을 디비에 저장할수 없으니 빈값 처리 후 글 작성된 id를 토대로 다시 update를 진행해야됨.
        }
        
        if ($uw->id = $DB->insert_record('ubboard_write', $uw)) {
            
            if (!empty($editorContent)) {
                $editorContent = file_save_draft_area_files($editorContent['itemid'], $this->context->id, 'mod_ubboard', self::FILEAREA_ARTICLE, $uw->id, \mod_ubboard_write_form::editor_options($this->context), $editorContent['text']);
                $DB->set_field('ubboard_write', 'content', $editorContent, array('id'=>$uw->id));
            }
            
            // 첨부파일 등록
            if (isset($uw->_attachment) && !empty($uw->_attachment)) {
                // 등록된 파일 갯수 가져오기
                $info = file_get_draft_area_info($uw->_attachment);
                $filecount = isset($info['filecount']) ? $info['filecount'] : 0;
                
                // 파일 저장
                file_save_draft_area_files($uw->_attachment, $this->context->id, 'mod_ubboard', self::FILEAREA_ATTACHMENT, $uw->id, \mod_ubboard_write_form::attachment_options($this->ubboard));
                $DB->set_field('ubboard_write', 'file_cnt', $filecount, array('id'=>$uw->id));
            }
            
            
            // 추가 이벤트
            $params = array(
                'context' => $this->context
                ,'objectid' => $uw->id
            );
            $event = \mod_ubboard\event\article_created::create($params);
            $event->add_record_snapshot('ubboard', $this->ubboard);
            // $event->add_record_snapshot('ubboard_write', $uw);
            $event->trigger();
            
            // 추가적인 검사를 진행하기 위해서 등록된 글정보 가져오기
            $article = $this->getArticle($uw->id);
            
            
            // 모든 사용자에게 메일 전송
            $this->sendMailSend($article, $isAllMailSend);
            
            // 메일 받기 관련 로직 실행
            $this->sendMailReceive($article);
            
            // 푸시 메시지 보내기
            $this->setPush($article);
            
            // 캐시 등록여부를 결정하기 위해 캐시 관련 함수 실행
            $this->setArticleCache($uw->id);
            
            return $uw->id;
        }
        
        return false;
    }
    
    
    /**
     * 신규로 추가될 게시판의 num값이 가져옵니다.
     *
     * @return int
     */
    public function getArticleNextNum() {
        global $DB;
        
        // 쿼리상에서 직접 max + 1 해도 되는데... mysql외 다른 DB를 사용할수 있어서 PHP 단에서 처리합니다.
        $query = "SELECT
						MAX(num)
				FROM 	{ubboard_write}
				WHERE	course	  = :course
				AND		ubboardid = :ubboardid";
        
        $max = $DB->get_field_sql($query, array('course'=>$this->course->id, 'ubboardid'=>$this->ubboard->id));
        
        if (empty($max)) {
            $max = 1;
        } else {
            $max += 1;
        }
        
        return $max;
    }
    
    
    /**
     * 에디터에 반영되어야 할 글 내용
     *
     * @param string $area
     * @param number $bwid
     * @param string $text
     * @param string $editorname
     * @return array[]
     */
    public function getFormEditor($area, $bwid, $text, $editorname = 'content')
    {
        $draftid_editor = file_get_submitted_draft_itemid($editorname);
        $currenttext = file_prepare_draft_area($draftid_editor, $this->context->id, 'mod_ubboard', $area, $bwid, \mod_ubboard_write_form::editor_options($this->context), $text);
        
        return array(
            'text'=>$currenttext,
            'format'=>editors_get_preferred_format(),
            'itemid'=>$draftid_editor
        );
    }
    
    /**
     * 답변글 reply값 구해오기
     *
     * @param object $original
     * @return boolean|string
     */
    public function getReplyLevel($num, $reply) {
        global $DB;
        
        $start = 'A';
        $end = 'Z';
        
        $replyLen = strlen($reply) + 1;
        
        // 답변 글의 바로 하위 단계에 달린 가장 최근 글 reply값 알아오기.
        $query = "SELECT
						MAX(SUBSTRING(reply, $replyLen, 1)) AS reply
				FROM	{ubboard_write}
				WHERE	num			= :num
				AND		course		= :course
				AND		ubboardid	= :ubboardid
				AND		SUBSTRING(reply, $replyLen, 1) <> ''
				AND		reply LIKE :reply";
        
        
        $maxreply = $DB->get_field_sql($query, array('num'=>$num, 'reply'=>$reply."%", 'course'=>$this->ubboard->course, 'ubboardid'=>$this->ubboard->id));
        
        
        // 하위 답변글이 하나도 없는 경우도 있음.
        if (empty($maxreply)) {
            $maxreply = $start;
        } else if ($maxreply == $end) {
            // 마지막 단계이면 답변을 더이상 달수 없음.
            return false;
        } else {
            $maxreply = chr(ord($maxreply) + 1);
        }
        
        
        return $reply.$maxreply;
    }
    
    
    /**
     * 모든 게시물 가져오기
     * 
     * @param boolean $isAll 공지사항 게시물까지 가져오고 싶으면 true로 입력해주시면 됩니다. 
     * @return array
     */
    public function getAllArticles($isAll=true)
    {
        global $DB;
        
        
        // 필수 쿼리
        $query = "SELECT
							".$this->getListFields()."
				FROM		{ubboard_write} uw
				JOIN		{user} u ON u.id = uw.userid
				WHERE		course       = :course
				AND			ubboardid 	 = :ubboardid
				AND			isdelete     = 0
                AND			u.deleted    = 0";
        
        
        // 기본 파라메터 (강좌번호, 게시판 번호)
        $param = array(
            'course'       => $this->course->id
            ,'ubboardid'    => $this->ubboard->id
            ,'notice'       => 0
        );
        
        
        // 일반 게시물만 다운 받고 싶은 경우
        if (!$isAll) {
            $query .= " AND	notice = :notice";
            $param['notice'] = 0;
        }
        
        return $DB->get_records_sql($query, $param);
    }
    
    
    
    /**
     * 게시글 정보 가져오기
     * 
     * @param number $bwid
     * @return mixed|boolean
     */
    public function getArticle($bwid, $isOnlyDBData=false)
    {
        global $DB, $CFG;
        
        $query = "SELECT 		
                            *
    			FROM		{ubboard_write}
    			WHERE		id = :id
                AND         course = :course
    			AND			ubboardid = :ubboardid";
        
        $param = array();
        $param['id'] = $bwid;
        $param['course'] = $this->course->id;
        $param['ubboardid'] = $this->ubboard->id;
        
        
        $article = $DB->get_record_sql($query, $param);
        
        // 간혹 가공되지 않는 DB 데이터가 필요한 경우가 있기 때문에 예외처리함.
        if (!$isOnlyDBData && !empty($article)) {
            $dateformat = get_string('strftimedatetimeshort');
            
            $fullname = get_string('anonymous', 'ubboard'); // userid값이 필수라서 이름이 없을수는 없는데, 혹시 몰라서 공백 처리 해둠
            if ($this->ubboard->anonymous) {
                if (!empty($article->name)) {
                    $fullname = $article->name;
                }
            } else {
                
                if (!empty($article->userid)) {
                    // 사용자 이름 알아오기
                    $usernamefield = get_all_user_name_fields(true);
                    
                    if ($writer = $DB->get_record_sql("SELECT id, ".$usernamefield.", idnumber FROM {user} WHERE id = :id", array('id'=>$article->userid))) {
                        $fullname = $this->getUserFullname($writer);
                    }
                }
            }
            
            // 본문 내용 관련 
            $article->content = format_text(file_rewrite_pluginfile_urls($article->content, 'pluginfile.php', $this->context->id, 'mod_ubboard', self::FILEAREA_ARTICLE, $article->id), FORMAT_MOODLE, array('context' => $this->context->id));
            
            if ($this->isSearch()) {
                $keyword = $this->getParameter('keyword');
                $keyfield = $this->getParameter('keyfield');
                
                if ($keyfield == 'subject') {
                    $article->subject = $this->getSearchString($keyword, $article->subject);
                } else if ($keyfield == 'content') {
                    $article->content = $this->getSearchString($keyword, $article->content);
                }
            }
            
            // 첨부 파일 관련 셋팅
            $article->attachments = $this->getFiles($bwid);
            
            // 필요한 기본 셋팅
            $article->fullname = $fullname;
            $article->date = userdate($article->timecreated, $dateformat);		// 작성시간
            $article->mdate = userdate($article->timemodified, $dateformat);		// 작성일
            
            
            $article->numformat_view = number_format($article->view_cnt);
            $article->cmid = $this->cm->id;
            
            // 강좌 공지사항 등록시 공개여부 기능을 사용하는 경우 캐시 파일에서 게시판 설정을 가져오려면 불필요한 쿼리가 많이 타기 때문에
            // 이곳에서 열람 여부 값을 설정해줌
            $article->isView = true;
            if ($this->isCourseOpen()) {
                // 공개 되지 않은 글인 경우 isView값은 false가 되어야 함.
                if (empty($article->open)) {
                    $article->isView = false;
                }
            }
        }
        
        return $article;
    }
    
    
    public function doUpdate()
    {
        global $CFG;
        
        if ($this->isPermission('write')) {
            if ($this->validateDefaultItem()) {
                
                list ($isError, $errorMessage, $update) = $this->_getUpdateObject();
                
                if ($isError) {
                    Javascript::printAlert($errorMessage);
                } else {
                    // 게시글 수정
                    $isAllMailSend = Parameter::post('allmailsend', 0, PARAM_INT);
                    $this->setUpdate($update, $isAllMailSend);
                    
                    $shareedit = Parameter::post('shareedit', null, PARAM_INT);
                    // 공유된 게시물에 대해서 모두 수정을 해야되는 경우
                    if ($shareedit) {
                        $this->setShareUpdate($update->id, $isAllMailSend);
                    }
                    
                    redirect($CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$update->id);
                }
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('write'), 'ubboard')));
        }
    }
    
    protected function _getUpdateObject()
    {
        global $USER, $DB, $CFG;
        require_once($CFG->libdir.'/filelib.php');
        
        $isError = false;
        $errorMessage = null;
        $update = null;
        
        $bwid = Parameter::post('bwid', null, PARAM_INT);
        $subject = Parameter::post('subject', null, PARAM_NOTAGS);
        $content = $this->getParamContent();
        $notice = Parameter::post('notice', 0, PARAM_INT);
        $secret = Parameter::post('secret', 0, PARAM_INT);
        $rnum = Parameter::post('rnum', 0, PARAM_INT);
        $categoryid = Parameter::post('categoryid', 0, PARAM_INT);
        $groupid = Parameter::post('groupid', 0, PARAM_INT);
        // 첨부파일 draftid
        $attachment = Parameter::post('attachment', null, PARAM_ALPHANUMEXT);
        
        
        // 수정되기 전 글을 가져와서 불필요한 항목들에 대해서는 update되지 않도록 처리해야됨.
        if ($oldArticle = $this->getArticle($bwid, true)) {
        
            if ($this->isMyArticle($oldArticle)) {
                // 해당 함수는 웹서비스에서도 호출이 될수 있기 때문에 get, post 값을 가져다가 사용하시면 안됩니다.
                $update = new stdClass();
                $update->id = $bwid;
                
                // 제목, 내용
                $update->subject = $subject;
                
                if (isset($content['itemid']) && !empty($content['itemid'])) {
                    $update->content = file_save_draft_area_files($content['itemid'], $this->context->id, 'mod_ubboard', self::FILEAREA_ARTICLE, $bwid, \mod_ubboard_write_form::editor_options($this->context), $content['text']);
                } else {
                    $update->content = $content;
                }
                
                // 첨부파일
                if ($this->ubboard->attachment && !empty($attachment)) {
                    $info = file_get_draft_area_info($attachment);
                    $filecount = isset($info['filecount']) ? $info['filecount'] : 0;
                    
                    $update->file_cnt = $filecount;
                }
                
                
                // 카테고리
                if ($this->ubboard->category) {
                    // 답변글이 달린 게시판은 카테고리를 수정할수 없음 (단 최상위 글이라면 가능함)
                    if (empty($oldArticle->reply)) {
                        $update->categoryid = $categoryid;
                    }
                }
                
                // 비밀글, 공지사항 등 셋팅
                // 공지사항은 관리 권한을 가진 사용자만 설정할수 있음.
                if ($this->ubboard->notice && $this->isPermission('manage')) {
                    $update->notice = $notice;
                    
                    // 시작 종료일
                    $stime = $etime = 0;
                    
                    // 공지사항 게시판이고, 공지사항이 체크 된 경우
                    if ($this->ubboard->isBoardType['notice'] && $notice) {
                        
                        // 기본은 무들 폼에의해서 전달되기 때문에 배열 형태임.
                        $stime = Parameter::postArray('stime', 0, PARAM_INT);
                        $etime = Parameter::postArray('etime', 0, PARAM_INT);
                        
                        // 시작일
                        if (is_array($stime)) {
                            $stime = Common::getArrayToUnixtime($stime);
                        } else {
                            $stime = Parameter::post('stime', 0, PARAM_INT);
                        }    
                        
                        // 종료일
                        if (is_array($etime)) {
                            $etime = Common::getArrayToUnixtime($etime);
                        } else {
                            $etime = Parameter::post('etime', 0, PARAM_INT);
                        }
                    }
                    
                    $update->stime = $stime;
                    $update->etime = $etime;
                }
                
                if ($this->ubboard->secret) {
                    $update->secret = $secret;
                }
                
                // 익명 게시판인 경우 닉네임을 지정할수 있음.
                if ($this->ubboard->anonymous) {
                    $nickname = Parameter::post('nickname', null, PARAM_NOTAGS);
                    
                    if (!empty($nickname)) {
                        $update->name = $nickname;
                    }
                }
                
                
                $update->timemodified = time();
                $update->ip = getremoteaddr();
                
                // 디비에는 저장되지 않지만 업데이트 로직에 필요한 변수들
                $update->_attachment = $attachment;
                $update->_oldArticle = $oldArticle;
            } else {
                $isError = true;
                $errorMessage = get_string('not_my_article', 'ubboard');
            }
            
        } else {
            $isError = true;
            $errorMessage = get_string('error_notexist_article', 'ubboard', $bwid);
        }
        
        return array($isError, $errorMessage, $update);
    }
    
    
    public function setUpdate($update, $isAllMailSend=false, $originaBwid=null)
    {
        global $DB;
        
        $DB->update_record('ubboard_write', $update);
        
        // 첨부파일 저장
        if (isset($update->_attachment) && !empty($update->_attachment)) {
            file_save_draft_area_files($update->_attachment, $this->context->id, 'mod_ubboard', self::FILEAREA_ATTACHMENT, $update->id, \mod_ubboard_write_form::attachment_options($this->ubboard));
        }
        
        
        // 최상위 글인경우 답변글까지 모두 수정되어야 함.
        // 상위글이 답변글이라면 update 구문이 실행되면 안됨.
        if (isset($update->categoryid) && $update->categoryid != $update->_oldArticle->categoryid && empty($update->_oldArticle->reply)) {
            $query = "UPDATE {ubboard_write} SET
							categoryid = :categoryid
					WHERE	course = :course
					AND		ubboardid = :ubboardid
					AND		num = :num";
            
            $DB->execute($query, array('categoryid'=>$update->categoryid, 'num'=>$update->_oldArticle->num, 'course'=>$this->ubboard->course, 'ubboardid'=>$this->ubboard->id));
        }
        
        
        // 글 수정 이벤트 실행
        $params = array(
            'context' => $this->context
            ,'objectid' => $update->id
        );
        
        // 공유로 인해 update가 이루어진 경우 other에 정보 남겨주기
        if (!empty($originaBwid)) {
            $params['other'] = array('originalbwid'=>$originaBwid);
        }
        $event = \mod_ubboard\event\article_updated::create($params);
        $event->add_record_snapshot('ubboard', $this->ubboard);
        $event->trigger();
        
        
        // 모든 사용자에게 메일 전송
        $this->sendMailSend($update->id, $isAllMailSend, true);
        
        // 메일 받기 관련 로직 실행
        $this->sendMailReceive($update->id, true);
        
        // 수정된 정보에 대해서는 push메시지를 보내지 않습니다.
        
        
        // 수정된 정보를 반영하기 위해서 캐시 관련 함수를 실행시켜줘야됨.
        $this->setArticleCache($update->id);
    }
    
    /**
     * 공지사항 정보 캐시 저장
     * 공지사항은 파라메터로 object로 받으면 안됩니다. 
     * (캐시에 저장이 되어야 되는데 함수 호출시마다 파라메터의 property가 각각 다르다면 문제가 발생됨)
     * 
     * @param int $bwid
     */
    public function setArticleCache($bwid)
    {
        // 캐시 정보 삭제 및 저장하기
        $cache = \cache::make('mod_ubboard', 'notices');
        $cacheUBBoard = $cache->get($this->course->id);
        
        // 사이트 공지이거나, 강좌내 팝업 공지를 사용하는 경우
        if (factory::isCoursePopup($this->course->id)) {
            
            // 게시판이 공지 게시판인지 확인 해야됨.
            // 공지 게시판이 아니면 캐시 등록할 필요가 없음.
            if ($this->ubboard->isBoardType['notice'] && $this->isPermission('manage')) {
                
                // 캐시 등록하기전에 실제 게시물이 존재하는지 확인 
                // 게시물 추가 및 수정에 의해서 호출이 되는데 입력/수정시 전달되는 객체가 ubboard_write의 모든 정보가 담겨있다라는 보장을 못하기 때문에
                // 캐시 등록전에 디비에서 데이터를 가져옴.
                if ($article = $this->getArticle($bwid)) {
                    // 캐시가 존재하는 경우 해당 글 삭제 시켜줘야됨.
                    if ($cacheUBBoard !== false) {
                        // 공지사항으로 설정되어 있으면 추가 및 업데이트
                        // 일반글이라면 캐시 삭제
                        if ($article->notice) {
                            $cacheUBBoard->articles[$bwid] = $article;
                        } else {
                            // 캐시 정보가 존재하는 경우 캐시 정보 삭제
                            if (isset($cacheUBBoard->articles[$bwid])) {
                                unset($cacheUBBoard->articles[$bwid]);
                            }
                        }
                        
                        $cache->set($this->course->id, $cacheUBBoard);
                    } else {
                        // 기존에 강좌에 등록된 캐시가 없기 때문에 공지글로 등록된 경우에 대해서 캐시 추가만 진행해주면됨.
                        // 공지사항으로 설정된 경우에는 캐시 등록시켜줘야됨.
                        if ($article->notice) {
                            // 캐시가 없기 때문에 신규로 등록 시켜줘야됨.
                            $cacheUBBoard = \mod_ubboard\latest::getDefaultCacheObject();
                            $cacheUBBoard->articles[$bwid] = $this->getArticle($bwid);
                            $cache->set($this->course->id, $cacheUBBoard);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * 본인이 작성한 글인지 확인합니다. (관리 권한이라면 본인이 작성하지 않았어도 true로 리턴)
     * 
     * @param number|object $articleorid
     * @return boolean
     */
    public function isMyArticle($articleorid)
    {
        global $DB, $USER;
        
        if (is_number($articleorid)) {
            $userid = $DB->get_field_sql('SELECT uid FROM {ubboard_write} WHERE id = :id', array('id'=>$bw));
        } else {
            $userid = $articleorid->userid;
        }
        
        
        // 관리 권한이 없는 경우
        if (!$this->isPermission('manage')) {
            if ($USER->id == $userid || is_siteadmin()) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    
    /**
     * 특정게시물에 대해서 비밀글을 볼수 있는지 확인해줍니다.
     * 
     * @param stdClass $article
     * @return boolean
     */
    function isSecretArticleView($article)
    {
        global $USER;
        
        $isView = true;
        
        // 비밀글인 경우
        if ($article->secret) {
        
            // 관리 권한이 없는 경우
            if (!$this->isPermission('manage')) {
            
                // 답변글인 경우
                if (!empty($article->reply)) {                
                    // 답변글이 본인이 작성한거라면 볼수 있어야함.
                    if ($USER->id != $article->userid && $USER->id != $article->puserid) {
                        $isView = false;
                    }
                } else {
                    // 답변 글이 아니라면 글 작성자만 보여야됨.
                    if ($USER->id != $article->userid) {
                        $isView = false;
                    }
                }
                
            }
        }
        
        return $isView;
    }
    
    /**
     * 게시물 보기 권한이 존재하는지 확인
     * @param stdClass $article
     * @return boolean
     */
    function isViewExtend($article)
    {
        $isView = true;
        $message = null;
        
        return array($isView, $message);
    }
    
    /**
     * 게시글에 답변글이 존재하는지 확인합니다.
     *
     * @param object $article
     * @return boolean
     */
    function isArticleChild(stdClass $article) 
    {
        global $DB;
        
        $reply = substr($article->reply, 0, strlen($article->reply));
        
        $query = "SELECT
							COUNT(1)
				FROM 		{ubboard_write}
				WHERE		course = :course
                AND			ubboardid = :ubboardid
                AND         id <> :id
                AND			num = :num
				AND			".$DB->sql_like('reply', ':reply')."
				AND			isdelete = 0";
        
        // 답변 글이 존재할 경우 true
        if ($DB->get_field_sql($query, array('reply'=>$reply.'%', 'id'=>$article->id, 'num'=>$article->num, 'course'=>$this->course->id, 'ubboardid'=>$this->ubboard->id)) > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    
    public function doDelete()
    {
        global $CFG, $DB;
        
        if ($this->isPermission('delete')) {
            $bwid = Parameter::post('bwid', null, PARAM_INT);
            
            $validate = $this->validate()->make(array(
                'bwid' => array(
                    'required'
                    ,$this->validate()::PLACEHOLDER => get_string('articlenum', $this->pluginname)
                    ,$this->validate()::PARAMVALUE => $bwid
                )
            ));
            
            if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
            } else {    
                if ($article = $this->getArticle($bwid, true)) {
                    
                    // 본인이 작성했거나, 관리 권한이 없으면 삭제 불가능해야됨.
                    if (!$this->isMyArticle($article)) {
                        Javascript::printAlert(get_string('not_my_article', $this->pluginname));
                    }
                    
                    // 관리자가 아니라면 답변글이 존재하면 삭제 불가능하도록 해야됨.
                    if (!$this->isPermission('manage')) {
                        if ($this->isArticleChild($article)) {
                            Javascript::printAlert(get_string('not_delete_reply', $this->pluginname));
                        }
                    }
                    
                    // 단일 항목으로 삭제로 인해 더이상 공유 게시물로 엮일 필요가 없는 항목이 존재할수 있음.
                    // setDelete에서 ubboard_share_child 레코드를 삭제하기 때문에 setDelete보다 먼저 실행이 되어야 함.
                    $shareArticleCount = 0;
                    if ($shareid = $this->getShareid($bwid)) {
                        $shareArticles = $DB->get_records_sql('SELECT bwid FROM {ubboard_share_child} WHERE shareid = :shareid', array('shareid' => $shareid));
                        
                        // 삭제 대상글은 setDelete에서 삭제되므로 사전이 미리 제거
                        unset($shareArticles[$bwid]);
                        
                        $shareArticleCount = count($shareArticles);
                    }
                    
                    $this->setDelete($article);
                    
                    
                    // 공유된 글이 1개 밖에 존재하지 않는다면 ubboard_share에서 레코드 삭제 해줘야됨.
                    if ($shareArticleCount == 1 && !empty($shareid)) {
                        $DB->delete_records('ubboard_share_child', array('shareid' => $shareid));
                        $DB->delete_records('ubboard_share', array('id' => $shareid));
                    }
                    
                    
                    redirect($CFG->wwwroot.'/mod/ubboard/view.php?id='.$this->cm->id);
                } else {
                    Javascript::printAlert(get_string('error_notexist_article', 'ubboard', $bwid));
                }
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('delete'), 'ubboard')));
        }
    }
    
    public function setDelete(stdClass $article, $originaBwid=null)
    {
        global $DB;
        
        $bwid = $article->id;
        
        
        // 해당글과 관련된 게시물 삭제 처리
        $query = "UPDATE {ubboard_write} SET
							isdelete = 1
					WHERE	course 	     = :course
					AND		ubboardid    = :ubboardid
					AND		num 	     = :num";
        
        $param = array('course'=>$this->course->id, 'ubboardid'=>$this->ubboard->id, 'num'=>$article->num);
        
        // 만약 최상위 글이 아니고, 답변글이 삭제된 경우라면 해당 답변글 하위에 있는 답변글만 삭제되어야 함.
        if (!empty($article->reply)) {
            $reply = substr($article->reply, 0, strlen($article->reply));
            
            $query .= ' AND '.$DB->sql_like('reply', ':reply');
            
            $param['reply'] = $reply.'%';
        }
        
        $DB->execute($query, $param);
                
        
        // 공유목록에 존재하는 게시물인 경우 삭제 조치
        $query = "DELETE FROM {ubboard_share_child} WHERE bwid = :bwid";
        $DB->execute($query, array('bwid'=>$bwid));
        
        
        // 삭제 이벤트 로그
        $params = array(
            'context' => $this->context
            ,'objectid' => $article->id
        );
        
        // 공유로 인해 update가 이루어진 경우 other에 정보 남겨주기
        if (!empty($originaBwid)) {
            $params['other'] = array('originalbwid'=>$originaBwid);
        }
        
        
        $event = \mod_ubboard\event\article_deleted::create($params);
        $event->add_record_snapshot('ubboard', $this->ubboard);
        $event->add_record_snapshot('ubboard_write', $article);
        $event->trigger();
        
        // 사이트 공지이거나, 강좌내 팝업 공지를 사용하는 경우 진행
        if (factory::isCoursePopup($this->course->id)) {
            // 캐시 삭제
            $cache = \cache::make('mod_ubboard', 'notices');
            $cacheUBBoard = $cache->get($this->course->id);
            
            if ($cacheUBBoard !== false && isset($cacheUBBoard->articles[$article->id])) {
                unset($cacheUBBoard->articles[$article->id]);
                $cache->set($this->course->id, $cacheUBBoard);
            }
        }
        
        // 알림테이블에 삭제여부값 변경해주기
        $this->setPush($bwid);
    }
    
    
    
    public function doCheckDelete()
    {
        global $CFG;
        
        // 관리 권한이 존재하는지 확인
        if ($this->isPermission('manage')) {
            // 필수값 검사
            $articleNums = Parameter::postArray('manage', null, PARAM_INT);
            $count = count($articleNums);
            
            if ($count > 0) {
                foreach ($articleNums as $bwid) {
                    
                    // 해당 글 정보 가져오기
                    if ($article = $this->getArticle($bwid, true)) {
                        
                        // 관리자가 직접 삭제하기 때문에 하위글과 상관없이 삭제됨.
                        $this->setDelete($article);
                    }
                }
                
                redirect($CFG->wwwroot.'/mod/ubboard/view.php?id='.$this->cm->id);
            } else {
                Javascript::printAlert(get_string('selected_noarticle', $this->pluginname));
            }            
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('manage'), 'ubboard')));
        }
    }
    
    
    #############################
    ######## 게시물 공유 ########
    #############################
    
    
    public function doCheckShare()
    {
        $this->_doShare();
    }
    
    
    public function doShare()
    {
        $this->_doShare();
    }
    
    protected function _doShare()
    {
        global $DB;
        
        // 관리 권한이 존재하는지 확인
        if ($this->isPermission('manage')) {
            
            $bwids = Parameter::post('bwids', null, PARAM_NOTAGS);
            
            $validate = $this->validate()->make(array(
                'bwids' => array(
                    'required'
                    ,$this->validate()::PLACEHOLDER => get_string('articlenum', $this->pluginname)
                    ,$this->validate()::PARAMVALUE => $bwids
                )
            ));
            
            if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
            } else {
                // 전달된 파라메터를 전부 반복 돌면서 select_course, select_course_board를 배열에 따로 담아둬야됨.
                $selectCourse = array();
                $selectCourseBoard = array();
                
                foreach (Parameter::getRequestAll() as $ekey => $evalue) {
                    
                    // select_course_board 먼저 검사 후 select_coruse 검사
                    // select_course_, select_course_board_에 붙는 숫자는 강좌 번호 입니다.
                    if (strpos($ekey, 'select_board_') !== false) {
                        // 1줄로 사용하면  Only variables should be passed by reference 메시지가 출력됨.
                        // end(explode('_', $pp_key));
                        $tmpcourseid = explode('_', $ekey);
                        $courseid = end($tmpcourseid);
                        
                        // 게시판이 존재하지 않을수도 있기 때문에 선택된 게시판만 저장해둬야됨.
                        if (!empty($evalue)) {
                            $selectCourseBoard[$courseid] = $evalue;
                        }
                    } else if (strpos($ekey, 'select_course_') !== false) {
                        $tmpcourseid = explode('_', $ekey);
                        $courseid = end($tmpcourseid);
                        
                        $selectCourse[$courseid] = $evalue;
                    }
                }
                
                // 선택되어 있는 강좌를 반복 돌면서 게시판 확인
                if (empty($selectCourse)) {
                    Javascript::printAlert(get_string('share_no_select_course_board', 'ubboard'));
                } else {
                    
                    // 여러 게시글이 선택된 경우
                    if (strpos($bwids, ',')) {
                        // 선택된 게시판 id를 분해해서 글 등록 시켜주기
                        $explodeBwids = explode(',', $bwids);
                    } else {
                        // 단일 게시판물이지만 코드 라인을 줄이기 배열형태로 감싸줌
                        $explodeBwids = array($bwids);
                    }
                    
                    // 선택된 공유게시글
                    $countBwids = count($explodeBwids);
                    if ($countBwids > 0) {
                        
                        // 과거 데이터가 먼저 등록이 되어야 하므로 정렬을 새로 해야됨. (정렬을 하지 않으면 과거 글이 먼저 출력이 됨)
                        asort($explodeBwids);
                        
                        
                        foreach ($explodeBwids as $eb) {
                            // 원본글 파일 목록
                            $query = "SELECT * FROM {files} WHERE contextid = :contextid AND component = :component AND filearea IN (:article, :attachment) AND itemid = :itemid";
                            $fileParam = array(
                                'contextid'     => $this->context->id
                                ,'component'    => 'mod_ubboard'
                                ,'itemid'       => $eb
                                ,'article'      => self::FILEAREA_ARTICLE
                                ,'attachment'   => self::FILEAREA_ATTACHMENT
                            );
                            
                            $files = $DB->get_records_sql($query, $fileParam);
                        
                            
                            // 공유하려는 글에 부모글이 존재하는지 확인
                            $shareid = $this->getShareid($eb);
                            
                            // 부모글이 존재하지 않으면 한번도 공유되지 않은 글이기 때문에 레코드 추가해줘야됨.
                            if (empty($shareid)) {
                                
                                // 부모글 정보 추가
                                $ubboardShare = new stdClass();
                                $ubboardShare->courseid = $this->course->id;
                                $ubboardShare->ubboardid = $this->ubboard->id;
                                $ubboardShare->cmid = $this->cm->id;
                                $ubboardShare->bwid = $eb;
                                $shareid = $DB->insert_record('ubboard_share', $ubboardShare);
                             
                                // 자식 테이블에도 추가해줘야됨.
                                $this->setShareChild($shareid, $this->course->id, $this->ubboard->id, $this->cm->id, $eb);
                            }
                            
                            $cntSuccess = 0;
                            foreach ($selectCourse as $courseid => $checkvalue) {
                                // 강좌를 선택을 하고 게시판이 설정된 경우
                                if ($checkvalue && isset($selectCourseBoard[$courseid])) {
                                    if ($this->setShare($eb, $selectCourseBoard[$courseid], $shareid, $files)) {
                                        $cntSuccess++;
                                    }
                                }
                            }
                        }
                        
                        // 총 {$a}개 강좌에 게시물이 공유되었습니다.
                        // cnt_success를 공유하려는 게시물 마다 새로 초기화 후 셋팅을 하는데..
                        // 메시지 내용이 x개 강좌에 게시물이 공유되었다라고 출력을 하기 때문에 게시물 별로 초기화해도 상관없음.
                        Javascript::printAlert(get_string('share_success', 'ubboard', $cntSuccess), true);
                    } else {
                        Javascript::printAlert(get_string('share_no_select_article', 'ubboard'));
                    }
                }
                
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('manage'), 'ubboard')));
        }
    }
    
    protected function getShareid($bwid)
    {
        global $DB;
       
        return $DB->get_field_sql("SELECT shareid FROM {ubboard_share_child} WHERE bwid = :bwid", array('bwid' => $bwid));
    }
    
    public function setShare($originalArticleNum, $targetBoardID, $shareid, $files = array())
    {
        global $DB, $USER;
        
        
        // 원본글 정보 가져오기
        if ($originalArticle = $this->getArticle($originalArticleNum, true)) {
            
            // 해당 게시판 관련 클래스 생성
            $newCUBBoard = factory::create($targetBoardID, false, false);
            $newContext = $newCUBBoard->getContext();
            $newUBBoard = $newCUBBoard->getUBBoard();
            
            // 답변글에 대해서 공유를 하더라도 최상위 글로 등록이 되어야 함. 
            // 공유하려는 답변글의 부모글이 공유가 안되어 있을수도 있고, 
            // 만약 공유되었더라고 해도 공유할 게시판내에서 답변글의 level이 중복되는 문제가 발생될수 있기 때문에 
            // 최상위글로 등록되는게 좋을듯함.
            
            // 글 등록 로직 시작.
            $bw = new stdClass();
            $bw->course = $newCUBBoard->getCourse()->id;
            $bw->ubboardid = $newCUBBoard->getUBBoard()->id;
            
            $bw->userid = $USER->id;
            $bw->num = $newCUBBoard->getArticleNextNum();
            $bw->puserid = $bw->userid;
            
            // 공유된 글이 다른 강좌일수 있기 때문에 그룹아이디를 따로 지정할수 없음
            $bw->groupid = -1;
            $bw->subject = $originalArticle->subject;
            $bw->content = $originalArticle->content;
            
            // 공유된 글이 다른 강좌일수 있기 때문에 게시판 카테고리를 따로 지정할수 없기 때문에 기본 카테고리로 지정
            $default_category = $newCUBBoard->getDefaultCategory();
            $bw->categoryid = $default_category->id;
            
            // 게시판에서 비밀글을 사용하는 경우
            if ($newUBBoard->secret) {
                $bw->secret = $originalArticle->secret;
            }
            
            // 게시판에서 공지 기능을 사용하는 경우
            if ($newUBBoard->notice) {
                $bw->notice = $originalArticle->notice;
            }
            
            // 공개 기능을 사용하는 경우
            if ($newCUBBoard->isCourseOpen()) {
                $bw->open = $originalArticle->open;
            }
            
            $bw->timecreated = time();
            $bw->ip = Common::getRealIPAddr();
            
            // 파일 갯수
            $bw->file_cnt = $originalArticle->file_cnt;
            
            if ($bw->id = $newCUBBoard->setInsert($bw)) {
                // 파일 반영
                $newCUBBoard->setShareFiles($bw->id, $files);
                
                // 공유된 글 정보 남겨놓기
                $this->setShareChild($shareid, $newCUBBoard->course->id, $newUBBoard->id, $newCUBBoard->cm->id, $bw->id);
                
                return $bw->id;
            }
        }
        
        return false;
    }
    
    
    protected function setShareChild($shareid, $courseid, $ubboardid, $cmid, $bwid)
    {
        global $DB;
        
        // 공유된 글 정보 남겨놓기
        $ubboardShareChild = new stdClass();
        $ubboardShareChild->shareid = $shareid;
        $ubboardShareChild->courseid = $courseid;
        $ubboardShareChild->ubboardid = $ubboardid;
        $ubboardShareChild->cmid = $cmid;
        $ubboardShareChild->bwid = $bwid;
        $ubboardShareChild->id = $DB->insert_record('ubboard_share_child', $ubboardShareChild);
        
        return $ubboardShareChild->id;
    }
    
    
    public function setShareUpdate($bwid, $isAllMailSend=false)
    {
        global $DB;
        
        // 원본글 정보 가져와서 하위글도 모두 수정해야됨.
        if ($shareid = $this->getShareid($bwid)) {
            // 변경되어야 할 게시판 정보 뽑아오기
            $articleIds = array();
            
            // 공유된 글 목록 가져오기
            $query = "SELECT
                    			usc.*
                    FROM		{ubboard_share} us
                    JOIN		{ubboard_share_child} usc ON us.id = usc.shareid
                    JOIN		{ubboard_write} uw ON usc.bwid = uw.id
                    WHERE       us.id = :shareid
                    AND		    uw.isdelete = 0";
            
            if ($articles = $DB->get_records_sql($query, array('shareid' => $shareid))) {
                foreach ($articles as $as) {
                    $articleIds[$as->bwid] = $as;
                }
                
                // 수정이 이루어진 글에 대해서는 재수정이 이루어질 필요가 없기 때문에 제거
                unset($articleIds[$bwid]);
            }
            
            
            if ($articleIds) {
                // 수정된 글 정보 가져와서 공유된 게시물에 변경시켜줘야됨.
                if ($originalArticle = $this->getArticle($bwid, true)) {
                    $time = time();
                    
                    // 등록된 파일
                    $query = "SELECT * FROM {files} WHERE contextid = :contextid AND component = :component AND filearea IN (:article, :attachment) AND itemid = :itemid";
                    $fileParam = array(
                        'contextid'     => $this->context->id
                        ,'component'    => 'mod_ubboard'
                        ,'itemid'       => $bwid
                        ,'article'      => self::FILEAREA_ARTICLE
                        ,'attachment'   => self::FILEAREA_ATTACHMENT
                        
                    );
                    
                    // $files는 에디터에 존재하는 이미지 파일도 포함되어 있기 때문에 
                    // 첨부파일로 등록된 파일 갯수는 따로 확인해야됨.
                    $fileCnt = 0;
                    if ($files = $DB->get_records_sql($query, $fileParam)) {
                        foreach ($files as $f) {
                            if ($f->filepath == '/' && $f->filename == '.') {
                                continue;
                            }
                            
                            if ($f->filearea == self::FILEAREA_ATTACHMENT) {
                                $fileCnt++;
                            }
                        }
                    }
                    
                    foreach ($articleIds as $as) {
                        // 해당 게시판 관련 클래스 생성
                        // !!!!!!! 중요 !!!!!!!
                        // 글 수정관련해서는 $this-> 로 접근하면 안됩니다. ($this-> 는 원본글에 대한 값이 설정되어 있기 때문에 해당 글에 대한 class를 새로 선언해줘야됨)
                        $newCUBBoard = factory::create($as->ubboardid, false, false);
                        $newContext = $newCUBBoard->getContext();
                        $newUBBoard = $newCUBBoard->getUBBoard();
                        
                        // 수정되려는 글이 실제로 존재하는지 확인
                        if ($oldArticle = $newCUBBoard->getArticle($as->bwid, true)) {
                            
                            // $originalArticle를 clone 후 불 필요한 항목 삭제하는것보다 필요한 항목을 입력해주는게 더 효율적임.
                            $update = new stdClass();
                            $update->id = $as->bwid;
                            $update->subject = $originalArticle->subject;
                            $update->content = $originalArticle->content;
                            
                            // 비밀글을 사용하는 경우
                            if ($newUBBoard->secret) {
                                $update->secret = $originalArticle->secret;
                            }
                            
                            // 공지기능을 사용하는 경우
                            if ($newUBBoard->notice) {
                                $update->notice = $originalArticle->notice;
                                $update->stime = $originalArticle->stime;
                                $update->etime = $originalArticle->etime;
                            }
                            
                            // 공개 기능을 사용하는 경우
                            if ($newCUBBoard->isCourseOpen()) {
                                $update->open = $originalArticle->open;
                            }
                            
                            $update->timemodified = $time;
                            $update->ip = Common::getRealIPAddr();
                            $update->file_cnt = $fileCnt;
                            
                            // 실제 디비에는 저장되지 않지만 저장 로직에는 필요한 변수
                            $update->_oldArticle = $oldArticle;
                            
                            // error_log(print_r($update, true));
                            $newCUBBoard->setUpdate($update, $isAllMailSend, $bwid);
                            
                            
                            // 파일 반영
                            $newCUBBoard->setShareFiles($update->id, $files, true);
                        }
                    }
                }
            }
        }
    }
    
    public function doDeleteShareAll()
    {
        global $CFG, $DB;
        
        if ($this->isPermission('delete')) {
            $bwid = Parameter::post('bwid', null, PARAM_INT);
            
            $validate = $this->validate()->make(array(
                'bwid' => array(
                    'required'
                    ,$this->validate()::PLACEHOLDER => get_string('articlenum', $this->pluginname)
                    ,$this->validate()::PARAMVALUE => $bwid
                )
            ));
            
            if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
            } else {
                
                // 부모글이 존재하는지 확인
                // 해당글이 공유된 글인지 확인해봐야됨.
                if ($shareid = $this->getShareid($bwid)) {
                
                    // 공유된 글 목록 가져오기
                    $query = "SELECT
                            			usc.*
                            FROM		{ubboard_share} us
                            JOIN		{ubboard_share_child} usc ON us.id = usc.shareid
                            JOIN		{ubboard_write} uw ON usc.bwid = uw.id
                            WHERE       us.id = :shareid
                            AND		    uw.isdelete = 0";
                    
                    if ($articles = $DB->get_records_sql($query, array('shareid' => $shareid))) {
                        foreach ($articles as $as) {
                            // 해당 게시판 관련 클래스 생성
                            // !!!!!!! 중요 !!!!!!!
                            // 글 수정관련해서는 $this-> 로 접근하면 안됩니다. ($this-> 는 원본글에 대한 값이 설정되어 있기 때문에 해당 글에 대한 class를 새로 선언해줘야됨)
                            $newCUBBoard = factory::create($as->ubboardid, false, false);
                            
                            if ($article = $newCUBBoard->getArticle($as->bwid, true)) {
                                $newCUBBoard->setDelete($article, $bwid);
                            }
                        }
                        
                        // 공유 목록 삭제
                        $DB->execute("DELETE FROM {ubboard_share} WHERE id = :id", array('id' => $shareid));
                    }   
                }
                
                redirect($CFG->wwwroot.'/mod/ubboard/view.php?id='.$this->cm->id);
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('delete'), 'ubboard')));
        }
    }
    
    /**
     * 전달된 파일에 대해서 공유할 게시글에 등록시켜주기 
     * 
     * @param number $bwid
     * @param array $files
     * @param boolean $isUpdate
     */
    protected function setShareFiles($bwid, $files, $isUpdate=false)
    {
        global $DB, $USER;
        
        
        $fs = get_file_storage();
        
        // 원본글 파일 목록
        $newHashesArticle = array();
        $newHashesAttachment = array();
        
        $component = 'mod_ubboard';
        // 게시글 변경이라면 파일 변경전에 기존에 등록된 파일을 배열에 따로 기록해놓고,
        // 원본글과 비교해서 파일 변경 및 삭제에 대한 로직을 구현해야됨.
        if ($isUpdate) {
            $oldArticleFiles = $fs->get_area_files($this->context->id, $component, self::FILEAREA_ARTICLE, $bwid, 'id');
            $oldAttachmentFiles = $fs->get_area_files($this->context->id, $component, self::FILEAREA_ATTACHMENT, $bwid, 'id');
        } else {
            // 신규글이면 빈값 배열로 리턴
            $oldArticleFiles = $oldAttachmentFiles = array();
        }
        
        $fileCount = count($files);
        if ($fileCount > 0) {
            
            // 게시판에서는 서브 폴더 기능을 사용하지 않음.
            // 그리고 공유 대상 게시판에 대해서 파일 업로드 제한(mimetype, maxuploadsize 등)에 대해서는 따로 검사하지 않음.
            // 업로드 제한에 의해서 파일이 안올라가면 해당 조건을 찾아주는게 만만치 않은 일이 될거 같음.
            foreach ($files as $file) {
                if ($file->filepath == '/' && $file->filename == '.') {
                    continue;
                }
                
                // 원본글에 대해서 등록된 파일을 newhash 배열에 따로 등록 시켜놓음
                // 만약 게시글이 수정이 이루어지는 경우 공유될 게시글에 대해서 newhash에 등록되지 않는 파일들에 대해서는 삭제를 시켜줘야됨
                $newhash = $fs->get_pathname_hash($this->context->id, $file->component, $file->filearea, $bwid, $file->filepath, $file->filename);
                
                $f = $fs->get_file_instance($file);
                if ($file->filearea == self::FILEAREA_ARTICLE) {
                    $newHashesArticle[$newhash] = $f;
                } else {
                    $newHashesAttachment[$newhash] = $f;
                }
            }
        }
        
        // 본문 에디터 파일, 첨부파일에 대해 파일 동기화 시켜줘야됨.
        $this->setShareFilesCUD($oldArticleFiles, $newHashesArticle, self::FILEAREA_ARTICLE, $bwid);
        $this->setShareFilesCUD($oldAttachmentFiles, $newHashesAttachment, self::FILEAREA_ATTACHMENT, $bwid);
    }
    
    
    /**
     * 게시물 수정시 기존에 등록되어 있던 파일과, 수정된 원본글간에 파일 동기화
     * 
     * @param array $oldfiles
     * @param array $newhashes
     * @param string $filearea
     * @param number $itemid
     */
    private function setShareFilesCUD($oldfiles, $newhashes, $filearea, $itemid)
    {
        $component = 'mod_ubboard';
        $fs = get_file_storage();
                
        // lib/filelib.php
        // file_save_draft_area_files() 함수 내용 일부 발췌
        foreach ($oldfiles as $oldfile) {
            $oldhash = $oldfile->get_pathnamehash();
            
            if (!isset($newhashes[$oldhash])) {
                // delete files not needed any more - deleted by user
                $oldfile->delete();
                continue;
            }
            
            $newfile = $newhashes[$oldhash];
            // Now we know that we have $oldfile and $newfile for the same path.
            // Let's check if we can update this file or we need to delete and create.
            if ($newfile->is_directory()) {
                // Directories are always ok to just update.
            } else if (($source = @unserialize($newfile->get_source())) && isset($source->original)) {
                // File has the 'original' - we need to update the file (it may even have not been changed at all).
                $original = file_storage::unpack_reference($source->original);
                if ($original['filename'] !== $oldfile->get_filename() || $original['filepath'] !== $oldfile->get_filepath()) {
                    // Very odd, original points to another file. Delete and create file.
                    $oldfile->delete();
                    continue;
                }
            } else {
                // The same file name but absence of 'original' means that file was deteled and uploaded again.
                // By deleting and creating new file we properly manage all existing references.
                $oldfile->delete();
                continue;
            }
            
            // status changed, we delete old file, and create a new one
            if ($oldfile->get_status() != $newfile->get_status()) {
                // file was changed, use updated with new timemodified data
                $oldfile->delete();
                // This file will be added later
                continue;
            }
            
            // Updated author
            if ($oldfile->get_author() != $newfile->get_author()) {
                $oldfile->set_author($newfile->get_author());
            }
            // Updated license
            if ($oldfile->get_license() != $newfile->get_license()) {
                $oldfile->set_license($newfile->get_license());
            }
            
            // Updated file source
            // Field files.source for draftarea files contains serialised object with source and original information.
            // We only store the source part of it for non-draft file area.
            $newsource = $newfile->get_source();
            if ($source = @unserialize($newfile->get_source())) {
                $newsource = $source->source;
            }
            if ($oldfile->get_source() !== $newsource) {
                $oldfile->set_source($newsource);
            }
            
            // Updated sort order
            if ($oldfile->get_sortorder() != $newfile->get_sortorder()) {
                $oldfile->set_sortorder($newfile->get_sortorder());
            }
            
            // Update file timemodified
            if ($oldfile->get_timemodified() != $newfile->get_timemodified()) {
                $oldfile->set_timemodified($newfile->get_timemodified());
            }
            
            // Replaced file content
            if (!$oldfile->is_directory() &&
                    ($oldfile->get_contenthash() != $newfile->get_contenthash() ||
                    $oldfile->get_filesize() != $newfile->get_filesize() ||
                    $oldfile->get_referencefileid() != $newfile->get_referencefileid() ||
                    $oldfile->get_userid() != $newfile->get_userid())) {
                $oldfile->replace_file_with($newfile);
            }
                    
            
            // unchanged file or directory - we keep it as is
            unset($newhashes[$oldhash]);
        }
        
        
        // Add fresh file or the file which has changed status
        // the size and subdirectory tests are extra safety only, the UI should prevent it
        foreach ($newhashes as $file) {
            
            $file_record = array('contextid'=>$this->context->id, 'component'=>$component, 'filearea'=>$filearea, 'itemid'=>$itemid, 'timemodified'=>time());
            if ($source = @unserialize($file->get_source())) {
                // Field files.source for draftarea files contains serialised object with source and original information.
                // We only store the source part of it for non-draft file area.
                $file_record['source'] = $source->source;
            }
            
            if ($file->is_external_file()) {
                $repoid = $file->get_repository_id();
                if (!empty($repoid)) {
                    $context = context::instance_by_id($this->context->id, MUST_EXIST);
                    $repo = repository::get_repository_by_id($repoid, $context);
                    if (!empty($options)) {
                        $repo->options = $options;
                    }
                    $file_record['repositoryid'] = $repoid;
                    // This hook gives the repo a place to do some house cleaning, and update the $reference before it's saved
                    // to the file store. E.g. transfer ownership of the file to a system account etc.
                    $reference = $repo->reference_file_selected($file->get_reference(), $context, $component, $filearea, $itemid);
                    
                    $file_record['reference'] = $reference;
                }
            }
            
            $fs->create_file_from_storedfile($file_record, $file);
        }
    }
    
    
    /**
     * 기본 validate 검사항목
     * 
     * @return boolean
     */
    protected function validateDefaultItem()
    {
        global $CFG, $PAGE;
        
        // 파일 및 editor에서 context값을 가져다가 쓰기 때문에 이곳에서 page->context값이 부여가 안되어 있으면 추가적으로 반영해줘야됨.
        if (empty($PAGE->context)) {
            $PAGE->set_context($this->context);
        }
        
        $subject = Parameter::post('subject', null, PARAM_NOTAGS);
        
        // modform에서 검사를 하긴 하는데 다른 경로로 해당 함수가 호출되었을때
        // 에러 방지를 위해서 2차로 validate 검사를 진행함.
        $validate = $this->validate()->make(array(
            'subject' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('subject', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $subject
            )
        ));
        
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $isnocontent = true;
            
            $content = $this->getParamContent();
            
            // $content = optional_param('content', null, PARAM_RAW);
            // get으로 처리하지 않기 때문에 post로 전달된값으로만 비교함.
            if (is_array($content)) {
                if (isset($content['text'])) {
                    if (!empty($content['text'])) {
                        $isnocontent = false;
                    }
                }
            } else {
                if (!empty($content)) {
                    $isnocontent = false;
                }
            }
            
            
            if ($isnocontent) {
                Javascript::printAlert(get_string('error_emptycontent', $this->pluginname));
            }
            
            return true;
        }
    }
    
    
    private function getParamContent()
    {
        if (is_array($_POST['content'])) {
            return Parameter::postArray('content', null, PARAM_RAW);
        } else {
            return Parameter::post('content', null, PARAM_RAW);
        }
    }
    
    /**
     * 해당 사용자가 일괄다운로드를 사용할수 있는 사용자인지 리턴해줍니다. 
     * 
     * @return boolean
     */
    public function isAllDownload()
    {
        // 기본적으로는 교수자만 다운로드 가능합니다.
        // 서울대는 예외적으로 학생들도 다운로드가 가능하기 때문에... 별도의 설정으로 분리해놓음.
        $alldownload = get_config('ubboard', 'allDownload');
        
        if (empty($alldownload)) {
            $alldownload = 1;
        }
        
        // 교수자인 경우
        if ($alldownload == 1) {
            return Course::getInstance()->isProfessor($this->course->id);
        } else {
            // 게시판을 확인할수 있는 사용자라면 누구나 다운로드 가능
            return true;
        }
    }
    
    /**
     * 게시판내에 등록된 첨부파일 리턴
     * 
     * @param number $bwid
     * @return \stdClass
     */
    public function getFiles($bwid)
    {
        global $OUTPUT;
        
        $fs = get_file_storage();
        
        $files = $fs->get_area_files($this->context->id, 'mod_ubboard', self::FILEAREA_ATTACHMENT, $bwid, 'id', false);
        $filecount = count($files);
        
        $attachments = new \stdClass();
        $attachments->count = $filecount;
        $attachments->files = array();
        $attachments->images = array();
        
        if ($filecount > 0) {
            foreach ($files as $file) {
                
                $mimetype = $file->get_mimetype();
                $filename = $file->get_filename();
                $file_icon = $OUTPUT->image_url(file_mimetype_icon($mimetype));
                
                // 이미지일 경우 화면에 출력.
                if ($file->is_valid_image())	{
                    $image = new \stdClass();
                    $image->icon = $file_icon;
                    $image->alt = $filename;
                    $image->url = \moodle_url::make_file_url('/pluginfile.php', '/'.$this->context->id.'/mod_ubboard/'.self::FILEAREA_ATTACHMENT.'/'.$bwid.'/'.$filename, false);
                    
                    array_push($attachments->images, $image);
                }
                
                
                $temp_file = new \stdClass();
                $temp_file->icon = $file_icon;
                $temp_file->icon_alt = $mimetype;
                $temp_file->text = $filename;
                $temp_file->url = \moodle_url::make_file_url('/pluginfile.php', '/'.$this->context->id.'/mod_ubboard/'.self::FILEAREA_ATTACHMENT.'/'.$bwid.'/'.$filename, true);
                
                array_push($attachments->files, $temp_file);
            }
        }
        
        return $attachments;
    }
    
    /**
     * 게시판 또는 특정 게시물에 존재하는 첨부파일 갯수를 리턴해줍니다.
     * @return number
     */
    public function getFileCount($bwid=null) {
        global $DB;
        
        /*
        $query = "SELECT
    						COUNT(1)
    			FROM 		{files}
    			WHERE		component = :component
    			AND			filearea = :filearea
    			AND			contextid = :contextid
    			AND 		filename != :filename";
        
        $param = array(
            'component'     => 'mod_ubboard'
            ,'filearea'     => 'attachment'
            ,'contextid'    => $this->context->id
            ,'filename'     => '.'
        );
        
        if (!empty($bwid)) {
            $query .= " AND			itemid = :itemid";
            $param['itemid'] = $bwid;
        }
        */
        
        if (empty($bwid)) {
            $query = "SELECT 
                                SUM(file_cnt) 
                    FROM        {ubboard_write}
                    WHERE       course = :course 
                    AND         ubboardid = :ubboardid
                    AND         isdelete = :isdelete";
            
            $param = array('course' => $this->course->id, 'ubboardid' => $this->ubboard->id, 'isdelete' => 0);
            
        } else {
            $query = "SELECT
                                file_cnt
                    FROM        {ubboard_write}
                    WHERE       id = :id";
            
            $param = array('id'=>$bwid);
        }
        
        return $DB->get_field_sql($query, $param);
    }
    
    
    #########################
    ######## Comment ########
    #########################
    
    public function getComment($bcid)
    {
        global $DB;
        
        return $DB->get_record_sql('SELECT * FROM {ubboard_comment} WHERE id = :id', array('id'=>$bcid));
    }
    
    
    public function getComments($bwid, $isOnlyDBData = false)
    {
        global $DB, $OUTPUT, $USER;
        
        $comments = array();
        if ($this->ubboard->comment) {
            $query = "SELECT
        						*
        			FROM		{ubboard_comment}
        			WHERE		course = :course
        			AND			bwid = :bwid
        			AND			isdelete = 0
        			ORDER BY	num ASC, reply, timecreated DESC";
            
            $param = array('course' => $this->course->id, 'bwid'=>$bwid);
            
            $comments = $DB->get_records_sql($query, $param);
            
            // 가공된 데이터 리턴
            if (!$isOnlyDBData) {
                $dateformat = get_string('strftimedate', 'langconfig');
                $isCommentPermission = $this->isPermission('comment');
                
                foreach ($comments as $c) {
                    $c->date = userdate($c->timecreated, $dateformat);
                    $c->fulldate = userdate($c->timecreated);
                    
                    // 작성자 기본 정보는 익명
                    $fullname = get_string('anonymous', 'ubboard');
                    $userpicture = $OUTPUT->image_url('u/f2');
                    $userlink = null;
                    
                    // 익명 기능을 사용하지 않으면 사용자 정보 가져오기
                    if ($this->ubboard->anonymous) {
                        if (!empty($c->name)) {
                            $fullname = $c->name;
                        }
                    } else {
                        if (!empty($c->userid)) {
                            // 사용자 이름 알아오기
                            $usernamefield = get_all_user_name_fields(true);
                            
                            if ($writer = $DB->get_record_sql("SELECT id, ".$usernamefield.", idnumber, picture, imagealt, email FROM {user} WHERE id = :id", array('id'=>$c->userid))) {
                                $fullname = $this->getUserFullname($writer);
                                $userpicture = \local_ubion\user\User::getInstance()->getPicture($writer);
                                
                                $userlink = $this->getUserLink($writer);
                            }
                        }
                    }
                    
                    $c->fullname = $fullname;
                    $c->userpicture = '<img src="'.$userpicture.'" alt="'.$fullname.' Picture" />';
                    $c->userlink = $userlink;
                    
                    $c->isModifyDelete = false;
                    if ($USER->id == $c->userid || $this->isPermission('manage')) {
                        $c->isModifyDelete = true;
                    }

                    // 답변글인 경우.
                    $c->leftmargin = strlen($c->reply) * 15;
                    
                    
                    // 삭제된 코멘트 인 경우.
                    // 원본글 정보는 남겨져있어야 하기 때문에...
                    $c->isReply = true;
                    if ($c->isdelete == 1) {
                        $c->comment = get_string('deleted_comment', 'ubboard');
                        $c->isReply = false;
                    }
                    
                    // 답변/수정,삭제 권한이 존재하고, 코멘트 권한이 존재하는 경우
                    $c->isButtons = (($c->isReply || $c->isModifyDelete) && $isCommentPermission) ? true : false;
                    
                    // format_text에서 nl2br을 해줌.
                    $c->comment = format_text(htmlspecialchars($c->comment));
                }
            }
        }
        
        
        return $comments;
    }
    
    
    public function doCommentInsert()
    {
        global $CFG;
        
        if ($this->isPermission('comment')) {
            // insert, reply
            $validate = $this->validate()->makeRequest(Parameter::getRequestAll(), array(
                'bwid' => array('required', $this->validate()::PLACEHOLDER => get_string('articlenum', $this->pluginname), $this->validate()::PARAMTYPE => PARAM_INT)
                ,'comment' => array('required', $this->validate()::PLACEHOLDER => get_string('comment', $this->pluginname), $this->validate()::PARAMTYPE => PARAM_RAW)
            ));
            
            if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
            } else {
                
                list ($isError, $errorMessage, $comment) = $this->_getCommentWriteObject();
                
                if ($isError) {
                    Javascript::printAlert($errorMessage);
                } else {
                    
                    if ($bcid = $this->setCommentInsert($comment)) {
                        // redirect($CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$comment->bwid.'#comment-'.$bcid);
                        redirect($CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$comment->bwid);
                    } else {
                        Javascript::printAlert(get_string('error_fail_write', 'ubboard'));
                    }
                }
                
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('comment'), 'ubboard')));
        }
    }
    
    public function _getCommentWriteObject()
    {
        global $USER;
        
        $isError = false;
        $errorMessage = null;
        
        $bwid = Parameter::post('bwid', null, PARAM_INT);
        $bcid = Parameter::post('bcid', null, PARAM_INT);
        
        $comment = new \stdClass();
        $comment->course = $this->course->id;
        $comment->ubboardid = $this->ubboard->id;
        $comment->bwid = $bwid;
        $comment->userid = $USER->id;
        
        // 닉네임
        if ($this->ubboard->anonymous) {
            $nickname = Parameter::post('nickname', null, PARAM_NOTAGS);
            
            if (!empty($nickname)) {
                $comment->name = $nickname;
            }
        }
        
        // 답변글인 경우
        if (!empty($bcid)) {
            if ($parentComment = $this->getComment($bcid)) {
                
                $comment->num = $parentComment->num;
                $comment->parent = $parentComment->id;
                
                // 답변글은 최대 26단계까지 허용 가능함.
                $reply = $this->getCommentReplyLevel($bwid, $parentComment->num, $parentComment->reply);
                if ($reply !== false) {
                    $comment->reply = $reply;
                } else {
                    Javascript::printAlert(get_string('error_maxreply', $this->pluginname));
                }
                
            } else {
                $isError = true;
                $errorMessage = get_string('error_parent_article', $this->pluginname);
            }
        } else {
            $comment->num = $this->getCommentNextNum($bwid);
        }
        
        $comment->comment = Parameter::post('comment', null, PARAM_RAW);
        $comment->ip = Common::getRealIPAddr();
        
        $time = time();
        $comment->timecreated = $time;
        
        return array($isError, $errorMessage, $comment);
    }
    
    public function setCommentInsert($comment)
    {
        global $DB;
        
        if ($comment->id = $DB->insert_record('ubboard_comment', $comment)) {
            
            // 코멘트 추가 후 ubboard_write에 코멘트 갯수 수정
            $DB->execute("UPDATE {ubboard_write} SET comment_cnt = comment_cnt + 1 WHERE id	= :id", array('id'=>$comment->bwid));
            
                        
            // 추가 이벤트
            $params = array(
                'context' => $this->context
                ,'objectid' => $comment->id
                ,'other' => array(
                    'bwid'=>$comment->bwid
                )
            );
            $event = \mod_ubboard\event\comment_created::create($params);
            $event->add_record_snapshot('ubboard', $this->ubboard);
            //$event->add_record_snapshot('ubboard_comment', $comment);
            $event->trigger();
            
                        
            return $comment->id;
        }
        
        return false;
    }
    
    public function doCommentUpdate()
    {
        global $CFG;
        
        if ($this->isPermission('comment')) {
            $validate = $this->validate()->makeRequest(Parameter::getRequestAll(), array(
                'bwid' => array('required', $this->validate()::PLACEHOLDER => get_string('articlenum', $this->pluginname), $this->validate()::PARAMTYPE => PARAM_INT)
                ,'bcid' => array('required', $this->validate()::PLACEHOLDER => get_string('commentnum', $this->pluginname), $this->validate()::PARAMTYPE => PARAM_INT)
                ,'comment' => array('required', $this->validate()::PLACEHOLDER => get_string('comment', $this->pluginname), $this->validate()::PARAMTYPE => PARAM_RAW)
            ));
            
            if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
            } else {
                
                list ($isError, $errorMessage, $update) = $this->_getCommentUpdateObject();
                
                if ($isError) {
                    Javascript::printAlert($errorMessage);
                } else {
                    $bwid = Parameter::post('bwid', null, PARAM_INT);
                    
                    $this->setCommentUpdate($update, $bwid);
                    
                    // redirect($CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$bwid.'#comment-'.$update->id);
                    redirect($CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$bwid);
                }
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('comment'), 'ubboard')));
        }
    }
    
    protected function _getCommentUpdateObject()
    {
        global $USER, $DB, $CFG;
        require_once($CFG->libdir.'/filelib.php');
        
        $isError = false;
        $errorMessage = null;
        $update = null;
        
        $bcid = Parameter::post('bcid', null, PARAM_INT);
        
        // 수정되기 전 글을 가져와서 불필요한 항목들에 대해서는 update되지 않도록 처리해야됨.
        if ($oldComment = $this->getComment($bcid)) {
            
            if ($this->isMyComment($oldComment)) {
                // 해당 함수는 웹서비스에서도 호출이 될수 있기 때문에 get, post 값을 가져다가 사용하시면 안됩니다.
                $update = new stdClass();
                $update->id = $bcid;
                $update->comment = Parameter::post('comment', null, PARAM_RAW);
                
                // 닉네임
                if ($this->ubboard->anonymous) {
                    $nickname = Parameter::post('nickname', null, PARAM_NOTAGS);
                    
                    if (!empty($nickname)) {
                        $update->name = $nickname;
                    }
                }
                
            } else {
                $isError = true;
                $errorMessage = get_string('not_my_article', 'ubboard');
            }
            
        } else {
            $isError = true;
            $errorMessage = get_string('error_notexist_comment', 'ubboard', $bcid);
        }
        
        return array($isError, $errorMessage, $update);
    }
    
    
    public function setCommentUpdate($comment, $bwid)
    {
        global $DB;
        
        
        $comment->timemodified = time();
        $comment->ip = Common::getRealIPAddr();
        
        $DB->update_record('ubboard_comment', $comment);
        
        
        $params = array(
            'context' => $this->context
            ,'objectid' => $comment->id
            ,'other' => array(
                'bwid'=>$bwid
            )
        );
        $event = \mod_ubboard\event\comment_updated::create($params);
        $event->add_record_snapshot('ubboard', $this->ubboard);
        //$event->add_record_snapshot('ubboard_comment', $comment);
        $event->trigger();
        
        return false;
    }
    
    /**
     * 본인이 작성한 코멘트인지 확인
     *
     * @param int $bcid
     * @return boolean
     */
    function isMyComment($commentorid) {
        global $DB, $USER;
        
        if (is_number($commentorid)) {
            $userid = $DB->get_field_sql('SELECT userid FROM {ubboard_comment} WHERE id = :id', array('id'=>$commentorid));
        } else {
            $userid = $commentorid->userid;
        }
        
        
        // 관리 권한이 없는 경우
        if (!$this->isPermission('manage')) {
            if ($USER->id == $userid || is_siteadmin()) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    public function doCommentDelete()
    {
        global $CFG;
        
        if ($this->isPermission('comment')) {
            $bwid = Parameter::post('bwid', null, PARAM_INT);
            $bcid = Parameter::post('bcid', null, PARAM_INT);
            
            $validate = $this->validate()->make(array(
                'bwid' => array(
                    'required'
                    ,$this->validate()::PLACEHOLDER => get_string('articlenum', $this->pluginname)
                    ,$this->validate()::PARAMVALUE => $bwid
                )
                ,'bcid' => array(
                    'required'
                    ,$this->validate()::PLACEHOLDER => get_string('commentnum', $this->pluginname)
                    ,$this->validate()::PARAMVALUE => $bcid
                )
            ));
            
            if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
            } else {
                if ($comment = $this->getComment($bcid)) {
                    
                    // 본인이 작성했거나, 관리 권한이 없으면 삭제 불가능해야됨.
                    if (!$this->isMyComment($comment)) {
                        Javascript::printAlert(get_string('not_my_article', $this->pluginname));
                    }
                    
                    // 관리자가 아니라면 답변글이 존재하면 삭제 불가능하도록 해야됨.
                    if (!$this->isPermission('manage')) {
                        if ($this->isCommentChild($comment)) {
                            Javascript::printAlert(get_string('not_delete_comment_reply', $this->pluginname));
                        }
                    }
                    
                    $this->setCommentDelete($comment);
                    
                    redirect($CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$bwid);
                } else {
                    Javascript::printAlert(get_string('error_notexist_article', 'ubboard', $bwid));
                }
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('comment'), 'ubboard')));
        }
    }
    
    
    public function setCommentDelete($comment)
    {
        global $DB;
        
        $bwid = $comment->bwid;
        
        
        // 해당글과 관련된 게시물 삭제 처리
        $query = "UPDATE {ubboard_comment} SET
							isdelete = 1
					WHERE	course 	  = :course
					AND		bwid      = :bwid
					AND		num 	  = :num";
        
        $param = array('course'=>$this->course->id, 'bwid'=>$comment->bwid, 'num'=>$comment->num);
        
        // 만약 최상위 글이 아니고, 답변글이 삭제된 경우라면 해당 답변글 하위에 있는 답변글만 삭제되어야 함.
        if (!empty($comment->reply)) {
            $reply = substr($comment->reply, 0, strlen($comment->reply));
            
            $query .= ' AND '.$DB->sql_like('reply', ':reply');
            
            $param['reply'] = $reply.'%';
        }
        
        $DB->execute($query, $param);
        
        
        // 게시물에 존재하는 코멘트 갯수 (삭제된 코멘트 제외)
        $commentCount = $DB->get_field_sql("SELECT COUNT(1) FROM {ubboard_comment} WHERE course = :course AND bwid = :bwid AND isdelete = 0", array('course'=>$this->course->id, 'bwid'=>$comment->bwid));
        
        $ubboardWrite = new stdClass();
        $ubboardWrite->id = $comment->bwid;
        $ubboardWrite->comment_cnt = $commentCount;
        $DB->update_record('ubboard_write', $ubboardWrite);
        
        
        // 삭제 이벤트 로그
        $params = array(
            'context' => $this->context
            ,'objectid' => $comment->id
            ,'other' => array(
                'bwid'=>$comment->bwid
            )
        );
        $event = \mod_ubboard\event\comment_deleted::create($params);
        $event->add_record_snapshot('ubboard', $this->ubboard);
        $event->add_record_snapshot('ubboard_comment', $comment);
        $event->trigger();
    }
    
    
    /**
     * 신규로 추가될 게시판의 num값이 가져옵니다.
     *
     * @return int
     */
    public function getCommentNextNum($bwid) {
        global $DB;
        
        // 쿼리상에서 직접 max + 1 해도 되는데... mysql외 다른 DB를 사용할수 있어서 PHP 단에서 처리합니다.
        $query = "SELECT
						MAX(num)
				FROM 	{ubboard_comment}
				WHERE	course	  = :course
				AND		bwid      = :bwid";
        
        $max = $DB->get_field_sql($query, array('course'=>$this->course->id, 'bwid'=>$bwid));
        
        if (empty($max)) {
            $max = 1;
        } else {
            $max += 1;
        }
        
        return $max;
    }
    
    /**
     * 코멘트 답변 레벨
     * 
     * @param number $bwid
     * @param number $num
     * @param string $reply
     * @return boolean|string
     */
    function getCommentReplyLevel($bwid, $num, $reply) {
        global $DB;
        
            
        $start = 'A';
        $end = 'Z';
        
        $replyLen = strlen($reply) + 1;
        
        // 답변 글의 바로 하위 단계에 달린 가장 최근 글 reply값 알아오기.
        $query = "SELECT
                            MAX(SUBSTRING(reply, $replyLen, 1)) as reply
				FROM	    {ubboard_comment}
				WHERE	    course	= :course
				AND		    bwid	= :bwid
                AND         num     = :num
				AND		    SUBSTRING(reply, $replyLen, 1) <> ''
				AND		    reply LIKE :reply";
        
        
        $maxreply = $DB->get_field_sql($query, array('num'=>$num, 'reply'=>$reply."%", 'course'=>$this->ubboard->course, 'bwid'=>$bwid));
        
        
        // 하위 답변글이 하나도 없는 경우도 있음.
        if (empty($maxreply)) {
            $maxreply = $start;
        } else if ($maxreply == $end) {
            // 마지막 단계이면 답변을 더이상 달수 없음.
            return false;
        } else {
            $maxreply = chr(ord($maxreply) + 1);
        }
            
        return $reply.$maxreply;
    }
    
    
    public function doComment()
    {
        $commentid = Parameter::post('commentid', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'commentid' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('commentnum', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $commentid
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {    
            if ($comment = $this->getComment($commentid)) {
                Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'comment'=>$comment->comment, 'name'=>$comment->name));
            } else {
                Javascript::printJSON(array('code'=>Javascript::getFailureCode(), 'msg'=>get_string('notfound_comment', $this->pluginname)));
            }
        }
    }
    
    
    /**
     * 게시글에 답변글이 존재하는지 확인합니다.
     *
     * @param object $comment
     * @return boolean
     */
    function isCommentChild(stdClass $comment)
    {
        global $DB;
        
        $reply = substr($comment->reply, 0, strlen($comment->reply));
        
        $query = "SELECT
							COUNT(1)
				FROM 		{ubboard_comment}
				WHERE		course = :course
                AND			bwid = :bwid
                AND         id <> :id
                AND			num = :num
				AND			".$DB->sql_like('reply', ':reply')."
				AND			isdelete = 0";
        
        // 답변 글이 존재할 경우 true
        if ($DB->get_field_sql($query, array('reply'=>$reply.'%', 'id'=>$comment->id, 'num'=>$comment->num, 'course'=>$this->course->id, 'bwid'=>$comment->bwid)) > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    
    /**
     * 사용자 이름에 추가될 링크
     * 
     * @param stdClass $user
     * @return NULL|string
     */
    protected function getUserLink($user)
    {
        global $CFG;
        
        $userlink = null;
        
        // 기본적으로 링크가 제공되지 않고,
        // 관리자라면 aslogin 할수 있도록 링크 제공해줘야됨.
        // 필요에 따라 교수자도 추가해주면 될듯함.
        if (is_siteadmin()) {
            $userlink = $CFG->wwwroot.'/user/view.php?id='.$user->id.'&course='.$this->course->id;
        }
        
        return $userlink;
    }
    
    /**
     * 게시판 추가 
     * 
     * @param stdClass $ubboard
     * @return number
     */
    public static function setAddInstance(stdClass $ubboard)
    {
        global $DB;
        
        // 게시판 추가 시간 저장
        $ubboard->timecreated = time();
        
        if (! $ubboard->id = $DB->insert_record('ubboard', $ubboard)) {
            return false;
        } else {
            // 게시판 추가시 자동으로 기본 카테고리 생성
            $category = new stdClass();
            $category->course = $ubboard->course;
            $category->ubboardid = $ubboard->id;
            $category->name = self::getDefaultCategoryName();
            $category->timecreated = time();
            $category->basic = 0;
            $category->orders = 1;
            $DB->insert_record('ubboard_category', $category);
        }
        
        return $ubboard->id;
    }

    /**
     * 게시판 수정
     * 
     * @param stdClass $ubboard
     * @return boolean
     */
    public static function setUpdateInstance(stdClass $ubboard)
    {
        global $DB;
        
        $ubboard->timemodified = time();
        $ubboard->id = $ubboard->instance;
        
        return $DB->update_record('ubboard', $ubboard);
    }
    
    
    /**
     * 게시판 삭제
     * @param stdClass $ubboard
     */
    public static function setDeleteInstance(stdClass $ubboard) {
        global $DB;
        
        if (!$cm = get_coursemodule_from_instance('ubboard', $ubboard->id)) {
            return false;
        }
        
        // 덧글 삭제
        $DB->delete_records('ubboard_comment', array('course'=>$ubboard->course, 'ubboardid'=>$ubboard->id));
        
        $context = context_module::instance($cm->id);
        
        // 첨부파일 삭제
        // 무들 로직상 사용자가 등록한 파일을 따로 관리하기 때문에 실제 파일은 삭제가 되지 않을수도 있습니다.
        $fs = get_file_storage();
        $fs->delete_area_files($context->id);
        
        // 게시판 내용 삭제
        $DB->delete_records('ubboard_write', array('course'=>$ubboard->course, 'ubboardid'=>$ubboard->id));
        
        // 카테고리 삭제
        $DB->delete_records('ubboard_category', array('course'=>$ubboard->course, 'ubboardid'=>$ubboard->id));
        
        // 게시판 정보 삭제
        $DB->delete_records('ubboard', array('id' => $ubboard->id));
        
        return true;
    }
    
    /**
     * 게시판 속성값 리턴
     * @return array
     */
    public static function getSettings() {
        global $CFG;
        
        $ubboard_config = array();
        $ubboard_config['anonymous']    =   array('open' => false,  'disable' => true);
        $ubboard_config['attachment']   =   array('open' => true,   'disable' => false);
        $ubboard_config['category']     =   array('open' => false,  'disable' => false);
        $ubboard_config['comment']      =   array('open' => true,   'disable' => false);
        $ubboard_config['groupmode']    =   array('open' => 0,      'disable' => true);	// 0, false, true 각기 다르게 사용됩니다; 0값이라고 false로 수정하심 안됩니다;
        $ubboard_config['mailreceive']  =   array('open' => false,  'disable' => true);
        $ubboard_config['mailsend']     =   array('open' => false,  'disable' => true);
        $ubboard_config['notice']       =   array('open' => false,  'disable' => false);
        $ubboard_config['open']         =   array('open' => false,  'disable' => false);
        $ubboard_config['reply']        =   array('open' => false,  'disable' => false);
        $ubboard_config['secret']       =   array('open' => false,  'disable' => false);
        $ubboard_config['sns']          =   array('open' => false,  'disable' => false);
        
        return $ubboard_config;
    }
    
    /**
     * 검색한 문자열에 대해서 강좌 태그를 추가해줍니다.
     * 
     * @param string $search
     * @param string $string
     * @return string
     */
    protected function getSearchString($search, $string)
    {
        // 검색어가 존재하는 경우에만 진행되어야 함.
        if (!empty($search)) {
            // 문자앞에 \ 를 붙입니다.
            $src = array("/", "|");
            $dst = array("\/", "\|");
            
            // 검색어 전체를 공란으로..
            $s = explode(" ", $search);
            $s_count = count($s);
            
            // "/(검색1|검색2)/i" 와 같은 패턴을 만듬
            $pattern = "";
            $bar = "";
            for ($m=0; $m<$s_count; $m++) {
                if (empty($s[$m])) {
                    continue;
                }
                
                $tmp_str = quotemeta($s[$m]);
                $tmp_str = str_replace($src, $dst, $tmp_str);
                $pattern .= $bar . $tmp_str . "(?![^<]*>)";
                $bar = "|";
            }
            
            // 지정된 검색 폰트의 색상, 배경색상으로 대체
            $replace = "<span class='search'>\\1</span>";
            return preg_replace("/($pattern)/i", $replace, $string);
        } else {
            return $string;
        }
    }
    
    
    
    
    /**
     * isPermission과 동일한 기능을 합니다.
     * 
     * @param string $type
     * @param string $isPrint
     * @deprecated isPermission 함수를 사용해주시기 바랍니다. => 기존 게시판 소스 기반으로 변경이 이루어지기 때문에 임시적으로만 존재하는 함수입니다.
     * @return boolean
     */
    final public function isAuth($type, $isPrint=false) 
    {
        return $this->isPermission($type, $isPrint);
    }
    
    /**
     * 권한 검사
     *
     * @param string $type
     * @param boolean $isPrint
     * @return boolean
     */
    final public function isPermission($type, $isPrint=false) 
    {
        global $CFG;
        
        if (empty($this->permissions)) {
            $this->permissions = $this->getPermissions();
        }
        
        // view => article을 사용하시기 바랍니다.
        if ($type == 'view') {
            $type = 'article';
        }
        
        
        $isPermission = false;
        if (!empty($this->permissions)) {
            if (isset($this->permissions->$type)) {
                $isPermission = $this->permissions->$type;
            }
        }
        
        if (!$isPermission) {
            if ($isPrint) {
                Common::printNotice(get_string('error_access', $this->pluginname, get_string($this->getAuthName($type), $this->pluginname)), $CFG->wwwroot.'/mod/ubboard/view.php?id='.$this->cm->id);
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    /**
     * 권한 목록
     * @return string[]
     */
    final private function getPermissionList()
    {
        // 해당 부분은 지금까지 추가된 적이 없기 때문에 별도의 예외처리는 하지 않았으니,
        // 존재하지 않는 권한이 포함되지 않도록 주의해주시기 바랍니다. 
        return array('list', 'article', 'write', 'reply', 'delete', 'comment', 'manage');
    }
    
    /**
     * 게시판에 대한 권한 정보를 리턴해줍니다.
     * @return \stdClass
     */
    public function getPermissions()
    {
        if (empty($this->permissions)) {
            $ps = new stdClass();
            
            $permissions = $this->getPermissionList();
            
            foreach ($permissions as $p) {
                
                // 게시판 설정에 사용하지 않는 기능인 경우에는 권한체크를 진행할 필요가 없음
                $isCheckPermission = true;
                if ($p == 'reply') {
                    $isCheckPermission = $this->ubboard->reply;
                } else if ($p == 'comment') {
                    $isCheckPermission = $this->ubboard->comment;
                }
                
                if ($isCheckPermission) {
                    $ps->$p = $this->_isPermission($p);
                } else {
                    $ps->$p = false;
                }
            }
            
            $this->permissions = $ps;
        }
        
        return $this->permissions;
    }
    
    /**
     * 권한 검사
     * 해당 함수는 특별한 경우가 아니라면 호출하지 말고 isPermision함수를 호출해주시기 바랍니다.
     *
     * @param string $type
     * @param boolean $isPrint
     * @return boolean
     */
    final private function _isPermission($type)
    {
        global $CFG;
        
        // 무들 권한 검사 할때 쓰이는 형태로 변수 선언
        $authName = $this->getAuthName($type);
        
        $isPermission = false;
        
        // role_capabilities에 레코드가 많이 쌓이면 로그인시 느려지는 문제가 발생됨.
        // 공지사항 게시판 인 경우에는 하드코딩으로 글쓰기, 수정, 답변, 삭제 기능은 교수자 권한이 있는 사용자만 가능해야됨.
        if ($this->ubboard->type == 'notice') {
            $checkAuth = array('ubboard:write', 'ubboard:reply', 'ubboard:delete');
            if (in_array($authName, $checkAuth)) {
                $isPermission = Course::getInstance()->isProfessor($this->course->id);
            } else {
                $isPermission = has_capability('mod/'.$authName, $this->context);
            }
        } else {
            $isPermission = has_capability('mod/'.$authName, $this->context);
        }
        
        if (!$isPermission) {
            return false;
        } else {
            // 그룹 권한 검사도 진행이 되어야 함.
            if ($this->_isGroupPermission($type)) {
                return true;
            }
            
            return false;
        }
        
        return true;
    }
    
    final private function getAuthName($type)
    {
        // view => article을 사용하시기 바랍니다.
        if ($type == 'view') {
            $type = 'article';
        }
        
        return 'ubboard:'.$type;
    }
    
    /**
     * 그룹 관련 권한 권사
     *
     * @param string $type
     */
    final private function _isGroupPermission($type) {
        global $USER;
        
        $accessallgroups = has_capability('moodle/site:accessallgroups', $this->context);
        $groupmode = groups_get_activity_groupmode($this->cm);
        $currentgroup = groups_get_activity_group($this->cm, true);
        
        if (!$groupmode or $accessallgroups) {
            return true;
        } else {
            
            $return = false;
            $isGroupMember = false;
            
            // list가 아닌 경우에는 무조건 groups_is_member 함수로 점검 해야됨.
            if ($type != 'list') {
                $isGroupMember = groups_is_member($currentgroup);
            }
            
            // 열린, 분리 모듬 둘다 list 권한은 제외 시켜야됨.
            // 분리 모듬이라도 전체 사용자를 대상으로 쓴글은 읽을수있어야 되기 때문에..
            switch($type) {
                case 'write' :
                case 'reply' :
                case 'delete' :
                case 'comment' :
                    if ($currentgroup) {
                        $return = $isGroupMember;
                    } else {
                        // no group membership and no accessallgroups means no new discussions
                        // reverted to 1.7 behaviour in 1.9+,  buggy in 1.8.0-1.9.0
                        $return = false;
                    }
                    break;
                default:
                    $return = true;
                    break;
            }
            
            // 분리 모둠인 경우에는 view 권한도 체크 해줘야됨.
            if ($groupmode == SEPARATEGROUPS && $type == 'view') {
                if ($currentgroup) {
                    $return = $isGroupMember;
                } else {
                    // no group membership and no accessallgroups means no new discussions
                    // reverted to 1.7 behaviour in 1.9+,  buggy in 1.8.0-1.9.0
                    $return = false;
                }
            } else if ($groupmode == VISIBLEGROUPS && $type == 'comment') {
                // 열린 그룹이라면, 코멘트는 누구나 볼수 있음.
                $return = true;
            }
            
            return $return;
        }
    }
    
    
    /**
     * 조회수 증가
     * @param number $bwid
     * @return boolean
     */
    final public function setViewCntUp($bwid)
    {
        global $DB;
        
        $isUpdate = false;
        
        // 쿠키값 검사.
        $cookiename = "ubboard_read";
        $cookievalue = $this->ubboard->id."_".$bwid.",";
        
        
        $cookie = Common::getCookie($cookiename);
        
        if ($cookie !== false) {
            // 문자열이 있는지 확인.
            if (strpos($cookie, $cookievalue) === FALSE) {
                $isUpdate = true;
            }
        } else {
            $isUpdate = true;
        }
        
        
        // 등록된 쿠키값이 없을 경우에만 update
        if ($isUpdate) {
            // 임시로 Update 코드 변경함.
            $ubboard_write = $DB->get_record_sql('SELECT id, view_cnt FROM {ubboard_write} WHERE id = :id', array('id'=>$bwid));
            $ubboard_write->view_cnt = $ubboard_write->view_cnt + 1;
            
            $DB->update_record('ubboard_write', $ubboard_write);
            
            Common::setCookie($cookiename, $cookievalue);
        }
        
        return $isUpdate;
    }
    
    /**
     * 사용자 이름 리턴 (교수자이면 학번도 같이 리턴됩니다.)
     * @param stdClass $user
     * @return string
     */
    public function getUserFullname($user)
    {
        $fullname = fullname($user);
        if ($this->isPermission('manage') && isset($user->idnumber) && !empty($user->idnumber)) {
            $fullname .= ' <small>('.$user->idnumber.')</small>';
        }
        
        return $fullname;
    }
    
    /**
     * 디바이스 타입
     * 게시판에서는 일반 / 모바일로만 구별함.
     * 
     * @return string
     */
    public function getDevice()
    {
        $device = \core_useragent::get_device_type();
        
        // 모바일 디바이스가 아니면 무조건 default device가 리턴
        if ($device != \core_useragent::DEVICETYPE_MOBILE) {
            $device = \core_useragent::DEVICETYPE_DEFAULT;
        }
        
        return $device;
    }
    
    ###########################
    ######## push 전송 ########
    ###########################
    public function setPush($articleorid)
    {
        // 숫자로 전달이 되었으면 게시글 정보 가져와야됨.
        if (is_number($articleorid)) {
            $article = $this->getArticle($articleorid);
        } else {
            $article = $articleorid;
        }
        
        // 공지사항 푸시
        $this->setPushNotice($article);
        
        // 일반글에 대한 푸시
        // 아직 일반글에 대한 정의가 내려져있지 않기 때문에 따로 진행하지 않음.
        // 참고로 모든 게시판에 적용을 하면 사용자가 원치않는 게시물에 대해 알림을 전송받기 때문에 어떠한 제약 조건이 필요할듯함.
        // 해당 부분은 논의가 필요함.
    }
    
    public function setPushNotice(stdClass $article)
    {
        global $CFG;
        
        // 등록된 글에 대해서 사용자들한테 push를 보내줘야됨.
        // 해당 함수는 공지사항만 담당합니다.
        if ($this->ubboard->type == 'notice' && !$this->ubboard->isBoardType['anonymous'] && $this->course->id > SITEID) {
            
            // 삭제된 게시물이면 알림에도 삭제처리해줘야됨.
            $isDelete = 0;
            if ($article->isdelete) {
                // 참고로 삭제가 되었으면 공개 기능 사용여부와 상관 없이 무조건 알림 삭제 처리
                $isDelete = 1;
            } else {
                // 공개 기능을 사용하는 경우
                if ($this->isCourseOpen()) {
                    // 비공개이면 삭제된걸로 변경해줘야됨.
                    if (empty($article->open)) {
                        $isDelete = 1;
                    }
                }
            }
            
            
            // 알림 전송
            $CNotification = \local_ubnotification\Notification::getInstance();
            
            $courseid = $this->ubboard->course;
            $cmid = $this->cm->id;
            $instanceid = $this->ubboard->id;
            $etcid = $article->id;
            $action = $CNotification::ACTION_UBBOARD_NOTICE;
            
            // 게시판 같은 경우에는 중복된 레코드가 발생될수 있기 때문에 예외처리 진행해줘야됨.
            if ($CNotification->isNotificationExist($courseid, $cmid, $instanceid, $etcid, $action)) {
                $CNotification->setUpdate($courseid, $cmid, $instanceid, $etcid, $action, $isDelete);
            } else {
                $CNotification->setInsert($courseid, 'mod_ubboard', $action, $cmid, $instanceid, $etcid, $isDelete);
            }
        }
    }
    
    
    
    ###########################
    ######## 메일 전송 ########
    ###########################
    
    public function sendMailSend($articleorid, $isAllMailSend = false, $isUpdate = false) 
    {
        global $CFG;
        
        // 관리자이고, 익명 게시판이 아닌 경우 메일 전송
        if ($this->isPermission('manage') && !$this->ubboard->isBoardType['anonymous'] && $this->ubboard->mailsend) {
            // 모든 사용자에게 메일 전송이 선택되어 있고
            // 강좌번호가 SITEID가 아닌 경우 메일 전송
            if ($isAllMailSend && $this->course->id > SITEID) {
                
                if (is_number($articleorid)) {
                    $article = $this->getArticle($articleorid);
                } else {
                    $article = $articleorid;
                }
                
                
                $isSend = true;
                // 공개여부 기능을 사용중이라면 게시글이 공개일 때에만 전송되어야 함.
                if ($this->isCourseOpen() && empty($article->open)) {
                    $isSend = false;
                }
                
                if ($isSend) {
                    // 강좌 참여자 모두 전송하기 위해서 모든 구성원 찾아와야됨.
                    require_once $CFG->libdir.'/enrollib.php';
                    $courseContext = \context_course::instance($this->course->id);
                    if ($users = get_enrolled_users($courseContext)) {
                        
                        $mailTitle = $this->getMailSendTitle($article->subject);
                        $mailContent = $this->getMailSendContent($article, $isUpdate);
                        
                        $this->sendEmail($users, $mailTitle, $mailContent);
                    }
                }
            }
        }
    }
    
    /**
     * 전체 사용자에게 메일 전송되는 메일 제목을 리턴
     * 
     * @param string $subject
     * @return string
     */
    protected function getMailSendTitle($subject)
    {
        global $SITE;
        
        $coursename = Course::getInstance()->getName($this->course);
        return '['.$SITE->shortname.']['.$coursename.'] '.$subject;
    }
    
    protected function getMailSendContent($article, $isUpdate = false)
    {
        $mailContent = '';
        if ($isUpdate) {
            if (!empty($article->timemodified)) {
                $mailContent .= '<p>'.get_string('mail_receive_modifydate', 'ubboard', userdate($article->timemodified)).'</p>';
                $mailContent .= '<hr/>';
            }
        }
        
        $mailContent .= $article->content;
        
        $mailFiles = '';
        // 파일이 존재하는 경우 링크로 제공해줌
        if ($article->file_cnt > 0) {
            $mailFiles .= '<ul>';
            foreach ($article->attachments->files as $f) {
                $mailFiles .= '<li><a href="'.$f->url.'">'.$f->text.'</a></li>';
            }
            $mailFiles .= '</ul>';
        }
        
        if (!empty($mailFiles)) {
            $mailContent .= '<p>&nbsp;</p>'.$mailFiles;
        }
        
        return $mailContent;
    }
    
    /**
     * 메일 받기시 전송되는 메일 제목을 리턴
     * 
     * @param string $coursename
     * @param string $subject
     * @return string
     */
    protected function getMailReceiveTitle($coursename, $subject)
    {        
        return '['.$coursename.'] '.$subject;
    }
    
    
    /**
     * 메일 받기시 전송되는 메일 내용을 리턴
     * 
     * @param object $article
     * @param string $coursename
     * @param boolean $isUpdate
     * @return string
     */
    protected function getMailReceiveContent($article, $coursename, $isUpdate=false)
    {
        global $CFG;
        
        $url = $CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$article->id;
        
        // 에디터 상에서 첨부파일이 존재할 수 있기 때문에 url 형태로 변경 시켜줘야됨.
        // 강좌명 색상 재조정 문의가 들어올거 같아서 언어팩에서 처리하지 않습니다. (캐시 삭제 문제 때문임.)
        $mailContent = get_string('mail_receive_title', 'ubboard', array('coursename'=>'<span style="color:red">'.$coursename.'</span>', 'boardname'=>$this->ubboard->name));
        $mailContent .= '<p>URL : <a href="'.$url.'" target="_blank">'.$url.'</a></p>';
        
        $mailContent .= '<hr style="margin:40px 0;" />';
        
        if ($isUpdate) {
            if (!empty($article->timemodified)) {
                $mailContent .= get_string('mail_receive_modifydate', 'ubboard', userdate($article->timemodified));
            }
        }
        $mailContent .= get_string('mail_receive_content_title', 'ubboard');
        $mailContent .= $article->content;
        
        return $mailContent;
    }
    
    
    /**
     * 메일 받기 기능
     * @param number|object $articleorid
     * @param boolean $isUpdate
     */
    public function sendMailReceive($articleorid, $isUpdate=false)
    {
        global $CFG, $DB, $USER, $SITE, $PAGE;
        
        // 메일 받기가 활성화 되어 있고, 익명게시판이 아닌 경우
        if ($this->ubboard->mailreceive && !$this->ubboard->isBoardType['anonymous']) {
            
            // 사이트 게시판인 경우에는 게시판에 부여된 역할정보로 가져와야됨.
            if ($this->ubboard->course == SITEID) {
                $courseContext = \context_system::instance();
            } else {
                $courseContext = \context_course::instance($this->ubboard->course);
            }
            
            if (empty($PAGE->context)) {
                $PAGE->set_context($this->context);
            }
            
            // 해당 게시물과 관련된 사용자들 추출
            $users = $this->getRelatedUsers($articleorid, $courseContext);
            
            // 사용자가 존재하는 경우에만 진행
            if (!empty($users)) {
                if ($this->ubboard->course == SITEID) {
                    $coursename = $SITE->shortname;
                } else {
                    $coursename = Course::getInstance()->getName($this->course);
                }
                
                $mailTitle = $this->getMailReceiveTitle($coursename, $article->subject);
                $mailContent = $this->getMailReceiveContent($article, $coursename, $isUpdate);
                
                $this->sendEmail($users, $mailTitle, $mailContent);
            }
        }
    }
    
    /**
     * 특정 게시물과 관련된 사용자 추출
     * 
     * @param int|object $articleorid
     * @param object $courseContext
     * @return array
     */
    public function getRelatedUsers($articleorid, \context $courseContext=null)
    {
        
        if (empty($courseContext)) {
            // 사이트 게시판인 경우에는 게시판에 부여된 역할정보로 가져와야됨.
            if ($this->ubboard->course == SITEID) {
                $courseContext = \context_system::instance();
            } else {
                $courseContext = \context_course::instance($this->ubboard->course);
            }
        }
        
        
        // 게시글이 존재해야됨.
        if (is_number($articleorid)) {
            $article = $this->getArticle($articleorid);
        } else {
            $article = $articleorid;
        }
        
        $isSend = true;
        // 공개여부 기능을 사용중이라면 게시글이 공개일 때에만 전송되어야 함.
        if ($this->isCourseOpen() && empty($article->open)) {
            $isSend = false;
        }
        
        $users = array();
        if ($isSend) {
            // 답변글인지 최초 작성글인지 확인
            $isReply = (empty($article->reply)) ? false : true;
            
            $users = array();
            
            $query = "SELECT
    									u.*
    						FROM		{user} u
    						JOIN 		{role_assignments} ra ON u.id = ra.userid
    						WHERE 		ra.contextid = :contextid
    						AND 		ra.roleid IN (:roleid1, :roleid2)";
            
            
            // 강좌 또는 게시판에 교수, 조교 권한을 가지고 있는 사용자 목록 추출하기
            $profUsers = $DB->get_records_sql($query, array('contextid'=>$courseContext->id, 'roleid1'=>$CFG->UB->ROLE->PROFESSOR, 'roleid2'=>$CFG->UB->ROLE->ASSISTANT));
            
            // 답변글 작성인경우 해당 글에 관련된 사용자들 목록 뽑아 오기
            $articleUsers = array();
            
            if ($isReply) {
                // 해당글과 관련된 사용자 목록 뽑아 오기
                $query = "SELECT
    									u.*
    						FROM 		{ubboard_write} uw
    						JOIN 		{user} u on uw.userid = u.id
    						WHERE 		course = :course
    						AND 		ubboardid = :ubboardid
    						AND 		num = :num
    						AND 		isdelete = :isdelete
    						AND 		u.deleted = :deleted";
                
                $articleUsers = $DB->get_records_sql($query, array('course'=>$article->course, 'ubboardid'=>$article->ubboardid, 'num'=>$article->num, 'isdelete'=>0, 'deleted'=>0));
            } else {
                // 최상위 글을 작성하는 경우라면 본인에게도 메일이 전송이 되어야 함.
                // 사용자 중복을 제거해야되기 때문에 반드시 userid값이 배열 키로 입력이 되어야 합니다.
                $articleUsers = array();
                $articleUsers[$USER->id] = $USER;
            }
            
            // 사용자 중복 제거 하기 (array_merge로 병합시 사용자가 중복 됩니다.)
            $users = $profUsers + $articleUsers;
        }
        
        return $users;
    }
    
    
    /**
     * 메일 전송
     *
     * @param array|object $users
     * @param string $subject
     * @param string $content
     */
    public function sendEmail($users, $subject, $content) {
        global $USER;
        
        $isTest = true;
        
        if ($isTest) {
            error_log('user');
            error_log(print_r($users, true));
            error_log('subject : '.$subject);
            error_log('content : '.$content);
        } else {
            // !!!! SMTP 부하문제로 첨부파일에 대해서는 메일 첨부파일로 전송하지 않습니다. !!!!
            // !!!! 첨부파일을 전송해야되는 경우라면 본문 내용에 링크로 전송시켜주시기 바랍니다. !!!!
            
            // users 형태가 배열이라면 전달된 사용자 모두 전송
            if (is_array($users)) {
                get_mailer('buffer');
                foreach ($users as $u) {
                    if (!empty($u->email) && $u->email != '@') {
                        email_to_user($u, $USER, $subject, strip_tags($content), $content);
                    }
                }
                get_mailer('close');
            } else {
                if (!empty($users->email) && $users->email != '@') {
                    email_to_user($users, $USER, $subject, strip_tags($content), $content);
                }
            }
        }
    }
    
    /**
     * 게시판에서 공유 기능을 사용가능한지 확인
     * 
     * @param boolean $isCheckAnonymous
     * @return boolean
     */
    public function isShare($isCheckAnonymous=true)
    {
        $return = false;
        if ($this->course->id > SITEID) {
            $return = true;
            
            if ($isCheckAnonymous) {
                // 익명 게시판에서는 공유기능이 사용되면 안됨.
                if ($this->ubboard->isBoardType['anonymous']) {
                    $return = false;
                }
            }
        }
        
        return $return;
    }
    
    public function getShareArticles($bwid)
    {
        global $CFG, $DB;
        
        $shareArticles = array();
        
        // 해당글이 공유된 글인지 확인해봐야됨.
        if ($shareid = $this->getShareid($bwid)) {
            
            // 공유된 글 목록 가져오기
            $query = "SELECT
                    			usc.*
                    FROM		{ubboard_share} us
                    JOIN		{ubboard_share_child} usc ON us.id = usc.shareid
                    JOIN		{ubboard_write} uw ON usc.bwid = uw.id
                    WHERE       us.id = :shareid
                    AND		    uw.isdelete = 0";
            
            if ($articles = $DB->get_records_sql($query, array('shareid' => $shareid))) {
                $CCourse = Course::getInstance();
                
                foreach ($articles as $as) {
                    $articleInfo = new stdClass();
                    $articleInfo->url = $CFG->wwwroot.'/mod/ubboard/article.php?id='.$as->cmid.'&bwid='.$as->bwid;
                    $articleInfo->coursename = $CCourse->getName($as->courseid);
                    
                    $shareArticles[$as->bwid] = $articleInfo;
                }
                
                
                // 공유된 글 목록에서 현재 보고 있는 게시물에 대해서는 제거해줘야됨.
                unset($shareArticles[$bwid]);
            }
        }
        
        return $shareArticles;
    }
    
    
    ##########################
    ######## 공개기능 ########
    ##########################
    
    /**
     * UBBOARD 공개글 사용여부
     * 
     * @return boolean
     */
    public function isOpen()
    {
        $isOpen = get_config('ubboard', 'open');
        
        // null인 경우를 대비
        if (empty($isOpen)) {
            $isOpen = 0;
        }
        
        return $isOpen;
    }
    
    /**
     * 글 등록시 공개여부 기본값
     * 
     * @return number
     */
    public function getOpenDefault()
    {
        $openDefault = get_config('ubboard', 'open_default');
        
        // null인 경우를 대비
        if (empty($openDefault)) {
            $openDefault = 0;
        }
        
        return $openDefault;
    }
    
    /**
     * 게시판내 공개글 사용여부
     * 
     * @return boolean
     */
    public function isCourseOpen()
    {
        return ($this->isOpen() && $this->ubboard->open) ? true : false;
    }
    
    
    public function doOpen()
    {
        global $CFG;
        
        $bwid = Parameter::post('bwid', null, PARAM_INT);
        $open = Parameter::post('open', 1, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'bwid' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('articlenum', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $bwid
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            if ($article = $this->getArticle($bwid, true)) {
                
                // 본인 게시물인지 확인해야됨.
                // 본인이 작성했거나, 관리 권한이 없으면 삭제 불가능해야됨.
                if ($this->isMyArticle($article)) {
                    
                    // 공개 여부기능을 사용하는 게시판인지 확인
                    if ($this->isCourseOpen()) {
                    
                        $this->setOpen($article, $open);
                        
                        if (Parameter::isAajx()) {
                            // 변경된 값에 따라서 화면에 표시되는 버튼을 변경조치해야됨.
                            if (empty($open)) {
                                // 공개 => 비공개로 전환된 상태임
                                $buttonText = get_string('open_conversion_1', $this->pluginname);
                                $buttonVal = 1;
                            } else {
                                // 비공개 => 공개로 전환된 상태임
                                $buttonText = get_string('open_conversion_0', $this->pluginname);
                                $buttonVal = 0;
                            }
                            
                            Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'msg'=>get_string('update_complete', 'local_ubion'), 'open'=>$buttonVal, 'text'=>$buttonText));
                        } else {
                            redirect($CFG->wwwroot.'/mod/ubboard/article.php?id='.$this->cm->id.'&bwid='.$bwid);
                        }
                    } else {
                        Javascript::printAlert(get_string('open_not_used', $this->pluginname));
                    }
                } else {
                    Javascript::printAlert(get_string('not_my_article', $this->pluginname));
                }
            } else {
                Javascript::printAlert(get_string('error_notexist_article', 'ubboard', $bwid));
            }
        }
    }
    
    
    public function setOpen(stdClass $article, $open)
    {
        global $DB, $PAGE;
        
        // 게시판내에서 공개여부 사용여부는 doOpen, doCheckOpen에서 체크해주시기 바랍니다.
        
        // 해당글과 관련된 게시물 공개/비공개 처리
        $query = "UPDATE {ubboard_write} SET
							open = :open
					WHERE	course 	     = :course
					AND		ubboardid    = :ubboardid
					AND		num 	     = :num";
        
        $param = array('course'=>$this->course->id, 'ubboardid'=>$this->ubboard->id, 'num'=>$article->num, 'open'=>$open);
        
        // 변경될 값이 공개 / 비공개이냐에 따라 로직이 달라짐.
        if (empty($open)) {
            // 공개 => 비공개로 전환이고, 답변글이라면 하위 답변글도 모두 비공개로 전환되어야 함.
            if (!empty($article->reply)) {
                $reply = substr($article->reply, 0, strlen($article->reply));
                
                $query .= ' AND '.$DB->sql_like('reply', ':reply');
                
                $param['reply'] = $reply.'%';
            }
        } else {
            // 비공개 => 공개 전환이기 때문에 선택된 게시물만 변경처리되어야 함.
            $query .= " AND id = :id";
            $param['id'] = $article->id;
            
        }
        
        $DB->execute($query, $param);
        
        // 변경된 정보를 반영해줘야지 이후 로직에서 문제 없이 기능 작동을 함.
        $article->open = $open;
        
        
        // 중요!!!! => 정보 업데이트 이후에 메일 받기 기능이 실행되어야 함.
        // 공개여부가 활성화된 상태라면 해당 게시물에 대해서 메일 발송이 이루어져야됨.
        // 공개여부에 대한 예외처리는 sendMailReceive에서 진행되기 때문에 별다른 예외처리 없이 함수 호출을 함.
        $this->sendMailReceive($article);
        
        // push 전송
        $this->setPush($article);
        
        // 기존에 등록되어 있던 캐시에는 open값 변경전 값이 저장되어 있기 때문에
        // 변경된 open값으로 캐시를 새로 등록시켜줘야됨.
        $this->setArticleCache($article->id);
    }
    
    public function doCheckOpen()
    {
        global $CFG;
        
        // 관리 권한이 존재하는지 확인
        if ($this->isPermission('manage')) {
            // 필수값 검사
            $articleNums = Parameter::postArray('manage', null, PARAM_INT);
            $count = count($articleNums);
            
            if ($count > 0) {
                if ($this->isCourseOpen()) {
                    $open = Parameter::post('open', 1, PARAM_INT);
                    foreach ($articleNums as $bwid) {
                        // 해당 글 정보 가져오기
                        if ($article = $this->getArticle($bwid, true)) {
                            $this->setOpen($article, $open);
                        }
                    }
                    
                    redirect($CFG->wwwroot.'/mod/ubboard/view.php?id='.$this->cm->id);
                } else {
                    Javascript::printAlert(get_string('open_not_used', $this->pluginname));
                }
            } else {
                Javascript::printAlert(get_string('selected_noarticle', $this->pluginname));
            }
        } else {
            Javascript::printAlert(get_string('error_access', $this->pluginname, get_string($this->getAuthName('manage'), 'ubboard')));
        }
    }
}