<?php
namespace mod_ubboard\core;

defined('MOODLE_INTERNAL') || die();

use stdClass;

class onetoone_ubboard extends ubboard
{
    public static function getSettings() {
        
        global $CFG;
        
        $ubboard_config = parent::getSettings();
        
        $ubboard_config['category']     =   array('open' => false,  'disable' => true);
        $ubboard_config['reply']        =   array('open' => true,   'disable' => false);
        $ubboard_config['secret']       =   array('open' => false,  'disable' => true);
        $ubboard_config['sns']          =   array('open' => false,  'disable' => true);
        
        return $ubboard_config;
    }
    
    public function getLists()
    {
        global $DB, $USER;
        
        // 기본 파라메터 (강좌번호, 게시판 번호)
        $param = array(
            'course'       => $this->course->id
            ,'ubboardid'    => $this->ubboard->id
            ,'notice'       => 0
        );
        
        // 필수 쿼리
        $query = "SELECT
							".self::REPLACE."
				FROM		{ubboard_write} uw
				JOIN		{user} u ON u.id = uw.userid
				WHERE		course       = :course
				AND			ubboardid 	 = :ubboardid
				AND			notice       = :notice
				AND			isdelete     = 0
                AND			u.deleted = 0";
        
        $categoryid = $this->getParameter('categoryid');
        $keyfield = $this->getParameter('keyfield');
        $keyword = $this->getParameter('keyword');
        $page = $this->getParameter('page', 1);
        $ls = $this->getParameter('ls', $this->getDefaultListSize());
        
        
        $addQuery = array();
        
        // 카테고리를 사용하고 카테고리 파라메터값이 존재할 경우.
        if (!empty($this->ubboard->category) && !empty($categoryid)) {
            $addQuery[] = " AND categoryid = :categoryid";
            $param['categoryid'] = $categoryid;
        }
        
        
        // 검색어가 존재할 경우.
        if (!empty($keyfield) && !empty($keyword)) {
            if ($keyfield == 'writer') {
                $fullname = $DB->sql_fullname('u.firstname','u.lastname');
                
                $addQuery[] = " AND ".$DB->sql_like('LOWER('.$fullname.')', ':keyword');
            } else {
                $addQuery[] = " AND ".$DB->sql_like('LOWER('.$keyfield.')', ':keyword');
            }
            
            // 검색인 경우 비밀글은 검색되면 안됨..
            if (!is_siteadmin() && !$this->isPermission('manage')) {
                $addQuery[] = " AND uw.secret = 0";
            }
            
            $param['keyword'] = '%'.strtolower($keyword).'%';
        }
        
        // 공개글 허용인 경우
        if ($this->isCourseOpen()) {
            // 관리 권한이 없으면 공개 또는 본인이 작성한 게시물만 보여야됨.
            if (!$this->isPermission('manage')) {
                $addQuery[] = " AND (uw.open = 1 OR uw.userid = :openuserid)";
                $param['openuserid'] = $USER->id;
            }
        }
        
        
        if (!$this->isPermission('manage')) {
            // manage권한을 가지고 있는 회원 정보 가져오기
            $addQuery[] = " AND puserid = :puserid";
            
            $param['puserid'] = $USER->id;
        }
        
        // 전체 게시물 수
        if (!empty($addQuery)) {
            $query .= join(' ', $addQuery);
        }
        $totalquery = str_replace(self::REPLACE, 'COUNT(1)', $query);
        $totalcount = $DB->get_field_sql($totalquery, $param);
        
        
        
        // 게시판 ordertype에 따라서 정렬 순서가 바뀜.
        // 0 : 작성일.
        // 1 : 수정일.
        if ($this->ubboard->ordertype == 0) {
            $sorttype = ' uw.timecreated ';
        } else {
            $sorttype = ' uw.timemodified ';
        }
        
        // 답변글을 사용하는 게시판인 경우에는 입력/수정일에 대해서 정렬 기능을 제공하지 않음.
        if ($this->ubboard->reply == 1) {
            $sortorder = " ORDER BY num DESC, reply, ".$sorttype." DESC";
        } else {
            $sortorder = ' ORDER BY '.$sorttype.' DESC, num DESC, reply';
        }
        
        
        // 가져올 게시물 갯수
        $page = ($page <= 0) ? 1 : $page;
        $limitfrom = $ls * ($page - 1);
        
        $query = str_replace(self::REPLACE, $this->getListFields(), $query);
        
        // error_log($query);
        // error_log(print_r($param, true));
        $lists = $DB->get_records_sql($query.$sortorder, $param, $limitfrom, $ls);
        
        // 게시물 가져올때 카테고리 명칭까지 가져오는것보다 카테고리만 따로 가져오는게 좋을거 같음.
        $categories = null;
        if ($this->ubboard->category) {
            $categories = $this->getCategories();
        }
        
        return array($totalcount, $lists, $categories);
    }
    
    
    /**
     * 게시물 보기 권한이 존재하는지 확인
     * @param stdClass $article
     * @return boolean
     */
    function isViewExtend($article)
    {
        global $USER;
        
        $isView = true;
        $message = null;
        
        // 관리자가 아니라면..
        // 본인이 등록한 글만 확인할수 있어야됨.
        if (!$this->isPermission('manage')) {
            if ($USER->id != $article->puserid) {
                $isView = false;
                $message = get_string('not_my_article', 'ubboard');
            }
        }
        
        return array($isView, $message);
    }
    
    /**
     * 화면에 표시될 게시물 번호
     *
     * @param number $articlenum  ubboard_write->num
     * @param number $sequentialnum 순차적인 번호
     * @return number
     */
    public function getListArticleNum($articlenum, $sequentialnum)
    {
        return $sequentialnum;
    }
}