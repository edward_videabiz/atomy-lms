<?php
namespace mod_ubboard\core;

use mod_ubboard\factory;

defined('MOODLE_INTERNAL') || die();

class latest
{
    public static function getCourseNotices($courseid)
    {
        
    }
    
    
    public static function getLatest($moduleid, $courseid, $cmid, $count=5)
    {
        global $DB, $CFG;
        
        $query = "SELECT
						uw.id
						,uw.subject
						,uw.content
						,uw.timecreated
						,uw.comment_cnt
						,uw.notice
						,uc.name
						,cm.id AS cmid
						,uw.ubboardid
			FROM		{ubboard} u
			JOIN		{ubboard_write} uw ON uw.ubboardid = u.id
			JOIN		{ubboard_category} uc ON uc.id = uw.categoryid
			JOIN		{course_modules} cm ON cm.instance = u.id AND module = :moduleid
			WHERE		u.course = :courseid
			AND			cm.id = :cmid
			AND			uw.isdelete = 0
			ORDER BY 	timecreated DESC";
        
        
        $param = array('courseid'=>$courseid, 'cmid'=>$cmid, 'moduleid'=>$moduleid);
        
        return $DB->get_records_sql($query, $param, 0, $count);
    }
    
    
    public static function getCacheCoursNotices($courseid)
    {
        global $CFG, $DB, $PAGE;
        
        $notices = array();
        
        // 팝업 공지를 사용하는 경우에만 진행
        if (factory::isCoursePopup($courseid)) {
            $cache = \cache::make('mod_ubboard', 'notices');
            
            $cacheUBBoard = $cache->get($courseid);
            
            
            if ($cacheUBBoard === false) {
                $PAGE->set_context(\context_course::instance($courseid));
                
                // 등록된 항목이 없기 때문에 먼저 캐시파일먼저 등록
                $cacheUBBoard = self::getDefaultCacheObject();
                
                // 캐시파일 자체가 없기 때문에 디비에서 조회해와야됨.
                $articleQuery = "SELECT id FROM {ubboard_write} WHERE course = :courseid AND ubboardid = :ubboardid AND notice = 1 AND isdelete = 0";
                
                if ($boards = $DB->get_records_sql("SELECT id FROM {ubboard} WHERE course = :courseid AND type = :type", array('courseid'=>$courseid, 'type'=>'notice'))) {
                    foreach ($boards as $bs) {
                        // 해당 게시판에 공지사항으로 등록된 글 가져오기
                        if ($articles = $DB->get_records_sql($articleQuery, array('courseid'=>$courseid, 'ubboardid'=>$bs->id))) {
                            $CUBBoard = \mod_ubboard\factory::create($bs->id, false, false);
                            
                            foreach ($articles as $as) {
                                $cacheUBBoard->articles[$as->id] = $CUBBoard->getArticle($as->id);
                            }
                        }
                    }
                }
                
                // 캐시 저장
                $cache->set($courseid, $cacheUBBoard);
            }
            
            
            // 등록된 공지글이 존재하는 경우
            if ($cacheUBBoard->articles) {
                $time = time();
                
                $zindex = 300;
                $top = get_config('ubboard', 'popupTopSite');
                $left = get_config('ubboard', 'popupLeftSite');
                
                if ($courseid > SITEID) {
                    $top = get_config('ubboard', 'popupTopCourse');
                    $left = get_config('ubboard', 'popupLeftCourse');
                }
                
                foreach ($cacheUBBoard->articles as $cd) {
                    // 공지 체크가 되어 있는지 다시 확인
                    if ($cd->notice) {
                        // 더이상 보이지 않기를 선택한 경우에는 화면에 표시되면 안됨.
                        // article->isView가 false이면 팝업으로 출력되면 안됨.
                        if (!isset($_COOKIE['ubboard-notice-'.$cd->id]) && $cd->isView) {
                            // 공지 시작일이 지정이 되었고, 현재 시간이 공지 시간보다 작은 경우는 표시되지 않음.
                            if ($cd->stime > 0 && $cd->stime > $time){
                                continue;
                            }
                            
                            // 공지 종료일이 지정이 되었고, 현재 시간이 공지 시간보다 큰 경우는 표시되지 않음.
                            if ($cd->etime > 0 && $time > $cd->etime){
                                continue;
                            }
                            
                            $notices[$cd->id] = $cd;
                            
                            $cd->inlineStyle = "top:".$top."px; left:".$left."px;z-index:".$zindex;
                            $cd->articlelink = $CFG->wwwroot.'/mod/ubboard/article.php?id='.$cd->cmid.'&bwid='.$cd->id;
                            
                            $zindex++;
                            $left+=20;
                            $top+=50;
                        }
                    }
                }
            }
        }
        
        return $notices;
    }
    
    /**
     * 강좌내 공지사항 기본 object
     * @return \stdClass
     */
    public static function getDefaultCacheObject()
    {
        $cacheUBBoard = new \stdClass();
        $cacheUBBoard->timecreated = time();    // 캐시가 등록된 시간을 알기 위해서 추가함. (파일 생성시간으로 알수도 있긴한데...)
        $cacheUBBoard->articles = array();
        
        return $cacheUBBoard;
    }
}