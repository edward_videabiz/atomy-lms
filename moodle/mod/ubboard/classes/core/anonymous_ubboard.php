<?php
namespace mod_ubboard\core;

defined('MOODLE_INTERNAL') || die();

class anonymous_ubboard extends ubboard
{
    public static function getSettings() {
        
        $ubboard_config = parent::getSettings();
        
        // 익명게시판인 경우 익명 허용은 무조건 허용되어야 하기 때문에 disable 시켜놓음.
        $ubboard_config['anonymous']    = array('open' => true,     'disable' => true);
        $ubboard_config['reply']        = array('open' => true,     'disable' => false);
        $ubboard_config['sns']          = array('open' => false,    'disable' => true);
        
        
        return $ubboard_config;
    }
}