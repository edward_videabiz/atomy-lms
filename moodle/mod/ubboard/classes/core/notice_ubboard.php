<?php
namespace mod_ubboard\core;

defined('MOODLE_INTERNAL') || die();

class notice_ubboard extends ubboard
{
    public static function getSettings() {
        
        global $CFG;
        
        $ubboard_config = $ubboard_config = parent::getSettings();
        
        $ubboard_config['notice']      =   array('open' => true,   'disable' => false);
        $ubboard_config['comment']      =   array('open' => false,   'disable' => false);
        $ubboard_config['reply']        =   array('open' => false,  'disable' => true);
        $ubboard_config['secret']       =   array('open' => false,  'disable' => true);
        $ubboard_config['mailsend']     =   array('open' => false,  'disable' => false);
        
        return $ubboard_config;
    }
}