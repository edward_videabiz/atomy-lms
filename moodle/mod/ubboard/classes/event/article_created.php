<?php

namespace mod_ubboard\event;
defined('MOODLE_INTERNAL') || die();

class article_created extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
        $this->data['objecttable'] = 'ubboard_write';
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "The user with id '$this->userid' has created the article with id '".$this->objectid."' in the ubboard with the course module id '$this->contextinstanceid'.";
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_article_created', 'mod_ubboard');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
    	return new \moodle_url('/mod/ubboard/article.php', array('id' => $this->contextinstanceid, 'bwid'=>$this->objectid));
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'ubboard_write', 'restore' => 'ubboard_write');
    }
}
