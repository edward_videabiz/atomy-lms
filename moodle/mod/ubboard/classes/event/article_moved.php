<?php
namespace mod_ubboard\event;
defined('MOODLE_INTERNAL') || die();

class article_moved extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
        $this->data['objecttable'] = 'ubboard_write';
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "The user with id '$this->userid' has moved the article with id ".$this->objectid." in the ubboard with the course module id '$this->contextinstanceid'.";
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_article_moved', 'mod_ubboard');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
    	// 게시물 한개만 이동하는게 아니기 때문에 view.php 로 이동해야됨.
        return new \moodle_url('/mod/ubboard/view.php', array('id'=>$this->contextinstanceid));
    }
    
    
    public static function get_objectid_mapping() {
        return array('db' => 'ubboard_write', 'restore' => 'ubboard_write');
    }
}
