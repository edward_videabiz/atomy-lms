<?php
namespace mod_ubboard\event;
defined('MOODLE_INTERNAL') || die();

class article_viewed extends \core\event\base {

    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'r';
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
        $this->data['objecttable'] = 'ubboard_write';
    }
    
    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
    	return get_string('event_article_viewed', 'mod_ubboard');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
        return new \moodle_url('/mod/ubboard/article.php', array('id' => $this->contextinstanceid, 'bwid'=>$this->objectid));
    }
    
    
    public static function get_objectid_mapping() {
        return array('db' => 'ubboard_write', 'restore' => 'ubboard_write');
    }
}

