<?php
namespace mod_ubboard\event;
defined('MOODLE_INTERNAL') || die();

class comment_deleted extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'd';
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
        $this->data['objecttable'] = 'ubboard_comment';
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "The user with id '$this->userid' has deleted the comment with id ".$this->objectid." in the ubboard with the course module id '$this->contextinstanceid'.";
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_comment_deleted', 'mod_ubboard');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
    	return new \moodle_url('/mod/ubboard/article.php', array('id' => $this->contextinstanceid, 'bwid'=>$this->other['bwid']));
    }

    /**
     * Custom validation.
     *
     * @throws \coding_exception
     * @return void
     */
    protected function validate_data() {
        parent::validate_data();

        if (!isset($this->other['bwid'])) {
        	throw new \coding_exception('The \'bwid\' value must be set in other.');
        }
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'ubboard_comment', 'restore' => 'ubboard_comment');
    }
    
    public static function get_other_mapping() {
        $othermapped = array();
        $othermapped['bwid'] = array('db' => 'ubbowrd_write', 'restore' => 'ubbowrd_write');
        
        return $othermapped;
    }
}
