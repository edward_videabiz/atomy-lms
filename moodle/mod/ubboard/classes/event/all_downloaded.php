<?php
namespace mod_ubboard\event;
defined('MOODLE_INTERNAL') || die();

class all_downloaded extends \core\event\base {

    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'r';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
        $this->data['objecttable'] = 'ubboard';
    }
    
    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
    	return get_string('event_all_downloaded', 'mod_ubboard');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
        return new \moodle_url('/mod/ubboard/view.php', array('id' => $this->contextinstanceid));
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'ubboard', 'restore' => 'ubboard');
    }
}

