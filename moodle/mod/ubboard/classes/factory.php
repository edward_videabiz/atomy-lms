<?php

/**
 * 해당 페이지 수정시 반드시 akddd에서 문의해주시기 바랍니다.
 */

namespace mod_ubboard;

use mod_ubboard\core\ubboard;
defined('MOODLE_INTERNAL') || die();

class factory
{

    /**
     *
     * @param number $id
     * @param string $isCmid
     * @return ubboard
     */
    public static function create($id, $isCmid = true)
    {
        global $CFG, $DB;
        
        if ($isCmid) {
            $cm = get_coursemodule_from_id('ubboard', $id);
        } else {
            $cm = get_coursemodule_from_instance('ubboard', $id);
        }
        
        if (empty($cm)) {
            print_error('invalidcoursemodule');
        }
        
        if ($ubboard = $DB->get_record_sql("SELECT * FROM {ubboard} WHERE id = :id", array('id' => $cm->instance))) {
            $classname = self::getClassName($ubboard->type);
            
            return new $classname($cm, $ubboard);
        } else {
            print_error('notfound_board', 'ubboard', null, array('cmid'=>$cm->id, 'ubboardid'=>$cm->instance));
        }
    }
    
    /**
     * 사용되는 게시판 타입
     * @return string[]
     */
    public static function getTypes()
    {
        $boardType = get_config('ubboard', 'type');
        
        // 설정된 게시판 타입이 없다면 기본적으로 제공해주는 게시판 타입만 리턴이 되어야 함. 
        if (empty($boardType)) {
            $boardType = array('default', 'notice', 'qna', 'group', 'onetoone', 'anonymous');
        } else {
            $boardType = explode(',', $boardType);
        }
        
        $return = array();
        foreach ($boardType as $bt) {
            $bt = trim($bt);
            
            if (!empty($bt)) {
                $return[$bt] = get_string('boardtype_'.$bt, 'ubboard');
            }
            
        }
        
        return $return;
    }
    
    /**
     * 게시판 설정값 리턴
     * 
     * @param string $boardtype
     * @return array
     */
    public static function getSettings($boardtype)
    {
        $className = self::getClassName($boardtype);
        
        $boardSetting = false;
        if (class_exists($className)) {
            $boardSetting = $className::getSettings();
        }
        
        return $boardSetting;
    }
    
    /**
     * 네임스페이스를 포함한 클래스 이름을 리턴해줍니다.
     * 
     * @param string $boardtype
     * @return string
     */
    public static function getClassName($boardtype)
    {
        return "\\mod_ubboard\\".$boardtype . '_ubboard';
    }
    
    
    final static function isCoursePopup($courseid)
    {
        $return = false;
        
        // 사이트에 추가된 공지사항인 경우에는 무조건 true
        if ($courseid == SITEID) {
            $return = true;
        } else {
            // 강좌 팝업 공지를 사용하는 경우에만 true
            $isCoursePopup = get_config('ubboard', 'coursePopup');
            if ($isCoursePopup) {
                $return = true;
            }
        }
        
        return $return;
    }
}