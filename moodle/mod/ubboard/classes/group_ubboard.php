<?php
namespace mod_ubboard;

defined('MOODLE_INTERNAL') || die();

class group_ubboard extends core\group_ubboard
{
    /**
     * 게시판 목록 리턴 (DB 데이터만 존재합니다.)
     *
     * @param number $categoryid
     * @param string $keyfield
     * @param string $keyword
     * @param number $page
     * @param number $ls
     * @return array
     */
    public function getLists($categoryid = null, $keyfield = null, $keyword = null, $page = 0, $ls = 20)
    {
        global $DB;
        
        // 기본 파라메터 (강좌번호, 게시판 번호)
        $param = array(
            'course'       => $this->course->id
            ,'ubboardid'    => $this->ubboard->id
            ,'notice'       => 0
        );
        
        // 필수 쿼리
        $query = "SELECT
							".self::REPLACE."
                            ,g.name AS groupname
				FROM		{ubboard_write} uw
				JOIN		{user} u ON u.id = uw.userid
                LEFT JOIN	{groups} g ON uw.groupid = g.id
				WHERE		course       = :course
				AND			ubboardid 	 = :ubboardid
				AND			notice       = :notice
				AND			isdelete     = 0
                AND			u.deleted = 0";
        
        
        $addQuery = array();
        
        // 카테고리를 사용하고 카테고리 파라메터값이 존재할 경우.
        if (!empty($this->ubboard->category) && !empty($categoryid)) {
            $addQuery[] = " AND categoryid = :categoryid";
            $param['categoryid'] = $categoryid;
        }
        
        // 그룹 게시판이므로 그룹 관련 파라메터가 추가되어야 함.
        $groupmode    = groups_get_activity_groupmode($this->cm);
        $currentgroup = groups_get_activity_group($this->cm, true);
        
        // 그룹기능을 사용하는 경우
        if ($groupmode) {
            if ($groupmode == VISIBLEGROUPS or has_capability('moodle/site:accessallgroups', $this->context)) {
                if ($currentgroup) {
                    $addQuery[] = " AND groupid IN (-1, :groupid)";
                    
                    $param['groupid'] = $currentgroup;
                }
            } else {
                //seprate groups without access all
                if ($currentgroup) {
                    $addQuery[] = " AND groupid IN (-1, :groupid)";
                    
                    $param['groupid'] = $currentgroup;
                } else {
                    $addQuery[] = " AND groupid = -1";
                }
            }
        }
        
        
        // 검색어가 존재할 경우.
        if (!empty($keyfield) && !empty($keyword)) {
            if ($keyfield == 'writer') {
                $fullname = $DB->sql_fullname('u.firstname','u.lastname');
                
                $addQuery[] = " AND ".$DB->sql_like('LOWER('.$fullname.')', ':keyword');
            } else {
                $addQuery[] = " AND ".$DB->sql_like('LOWER('.$keyfield.')', ':keyword');
            }
            
            // 검색인 경우 비밀글은 검색되면 안됨..
            if (!is_siteadmin() && !$this->isPermission('manage')) {
                $addQuery[] = " AND uw.secret = 0";
            }
            
            $param['keyword'] = '%'.strtolower($keyword).'%';
        }
        
        
        // 전체 게시물 수
        if (!empty($addQuery)) {
            $query .= join(' AND ', $addQuery);
        }
        $totalquery = str_replace(self::REPLACE, 'COUNT(1)', $query);
        $totalcount = $DB->get_field_sql($totalquery, $param);
        
        
        
        // 게시판 ordertype에 따라서 정렬 순서가 바뀜.
        // 0 : 작성일.
        // 1 : 수정일.
        if ($this->ubboard->ordertype == 0) {
            $sorttype = ' uw.timecreated ';
        } else {
            $sorttype = ' uw.timemodified ';
        }
        
        // 답변글을 사용하는 게시판인 경우에는 입력/수정일에 대해서 정렬 기능을 제공하지 않음.
        if ($this->ubboard->reply == 1) {
            $sortorder = " ORDER BY num DESC, reply, ".$sorttype." DESC";
        } else {
            $sortorder = ' ORDER BY '.$sorttype.' DESC, num DESC, reply';
        }
        
        
        // 가져올 게시물 갯수
        $page = ($page <= 0) ? 1 : $page;
        $limitfrom = $ls * ($page - 1);
        
        $query = str_replace(self::REPLACE, $this->getListFields(), $query);
        $lists = $DB->get_records_sql($query.$sortorder, $param, $limitfrom, $ls);
        
        // 게시물 가져올때 카테고리 명칭까지 가져오는것보다 카테고리만 따로 가져오는게 좋을거 같음.
        $categories = null;
        if ($this->ubboard->category) {
            $categories = $this->getCategories();
        }
        
        return array($totalcount, $lists, $categories);
        
    }
}