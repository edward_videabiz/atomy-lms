<?php
namespace mod_ubboard\output;

defined('MOODLE_INTERNAL') || die;

/**
 * TODO 게시판 타입별로 mustache 파일이 존재하면 해당 게시판 타입 파일이 선택되어야 함.
 * 이곳에서는 프로그램 통해서 구별할수 있을거 같긴 한데 테마에서 override가 어떻게 될지 모르겠음?
 * 
 * 일단은 기본 게시판에 대해서 먼저 개발을 완료한 뒤 고민해봐야될듯함. 
 * 2017-09-01 
 */

use plugin_renderer_base;
use stdClass;


class renderer extends plugin_renderer_base {
    protected $pluginname = 'mod_ubboard';
    protected $CUBBoard = null;
    
    /**
     * 게시판 클래스 설정
     * 
     * @param \mod_ubboard\core\ubboard $CUBBoard
     */
    public function setCUBBoard(\mod_ubboard\core\ubboard $CUBBoard) {
        $this->CUBBoard = $CUBBoard;
    }
    
    public function renderTest($data)
    {
        error_log('module renderer');
        return parent::render_from_template($this->pluginname.'/test', $data);
    }
    
    public function renderLists($data)
    {
        $data = $this->getBaseData($data);
        
        return $this->render_from_template($this->getTemplateFile('lists'), $data);
    }
    
    public function renderArticleLists($data)
    {
        $data = $this->getBaseData($data);
        
        return $this->render_from_template($this->getTemplateFile('list'), $data);
    }
    
    
    public function renderArticle($data)
    {
        $data = $this->getBaseData($data);
        
        return $this->render_from_template($this->getTemplateFile('article'), $data);
    }
    
    public function renderCategory($data)
    {
        $data = $this->getBaseData($data);
        
        return $this->render_from_template($this->getTemplateFile('category'), $data);
    }
    
    
    protected function getBaseData($data)
    {
        /*
        if (!empty($_GET)) {
            foreach ($_GET as $key => $value) {
                $this->getParameter[$key] = $value;
            }
        }
        */
        $data->ubboard = $this->CUBBoard->getUBBoard();
        $data->permissions = $this->CUBBoard->getPermissions();
        $data->getParameter = $this->CUBBoard->getParameterAll();
        $data->formtypename = $this->CUBBoard::TYPENAME;
        $data->sesskey = sesskey();
        
        // 검색 여부
        $data->issearch = false;
        if (!empty($this->CUBBoard->getParameter('keyfield')) && !empty($this->CUBBoard->getParameter('keyword'))) {
            $data->issearch = true;
        }
        
        return $data;
    }
    
    private function getTemplateFile($templatename)
    {
        global $CFG;
        
        // 기본 템플릿
        $defaultPath = 'default_'.$templatename;
        
        // mustache는 templates 폴더 안에 서브디렉토리가 존재하면 안됨
        // moodle3.3 20170608 기준
        // 기본값 : devicetype_templatename
        // 게시판별로 template 파일을 다르게 하고 싶다면 devicetype_boardtype_templatename으로 입력하면됨.
        $device = $this->CUBBoard->getDevice();
        
        // 현재 디바이스
        $devicePath = $device.'_'.$templatename;
        $deviceBoardTypePath = $device.'_'.$this->CUBBoard->getUBBoard()->type.'_'.$templatename;
        
        $path = $devicePath;
        if (file_exists($CFG->dirroot.'/mod/ubboard/templates/'.$deviceBoardTypePath.'.mustache')) {
            $path = $deviceBoardTypePath;
        }
        
        if (!file_exists($CFG->dirroot.'/mod/ubboard/templates/'.$path.'.mustache')) {
            $path = $defaultPath;
        }
        
        return $this->pluginname.'/'.$path;
    }
    
    
}