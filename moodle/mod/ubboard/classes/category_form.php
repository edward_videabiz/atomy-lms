<?php
use local_ubion\controller\Controller;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/repository/lib.php');

class mod_ubboard_category_form extends moodleform {
    
    
    /**
     * Form definition
     *
     * @return void
     */
    function definition() {
        global $CFG, $OUTPUT;
        
        $mform =& $this->_form;
        
        $CUBBoard = $this->_customdata['CUBBoard'];
        
        $cm = $CUBBoard->getCM();
        $modcontext = $CUBBoard->getContext();
        $ubboard = $CUBBoard->getUBBoard();
        
        
        $mform->addElement('header', 'general', get_string('default_setting', 'ubboard'));//fill in the data depending on page params later using set_data
        
        // 제목
        $mform->addElement('text', 'name', get_string('category_name', 'ubboard'), 'size="48"');
        $mform->setType('name', PARAM_NOTAGS);
        $mform->addRule('name', get_string('required'), 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
                
        //-------------------------------------------------------------------------------
        $this->add_action_buttons(true, get_string('btn_save', 'ubboard'));
        
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('hidden', 'categoryid');
        $mform->setType('categoryid', PARAM_INT);
        
        $typename = Controller::TYPENAME;
        $mform->addElement('hidden', $typename);
        $mform->setType($typename, PARAM_ALPHANUMEXT);
    }
    
    
    /**
     * Form validation
     *
     * @param array $data data from the form.
     * @param array $files files uploaded.
     * @return array of errors.
     */
    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        
        if (empty($data['name'])) {
            $errors['name'] = get_string('error_emptycategoryname', 'ubboard');
        }
        
        return $errors;
    }
}
