<?php
use local_ubion\controller\Controller;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/formslib.php');
require_once($CFG->dirroot . '/repository/lib.php');

class mod_ubboard_write_form extends moodleform {
    
    /**
     * Returns the options array to use in filemanager for forum attachments
     *
     * @param stdClass $forum
     * @return array
     */
    public static function attachment_options($ubboard) {
        global $COURSE, $PAGE, $CFG;
        
        $maxbytes = get_user_max_upload_file_size($PAGE->context, $CFG->maxbytes, $COURSE->maxbytes, $ubboard->file_size);
        return array(
            'subdirs' => 0,
            'maxbytes' => $maxbytes,
            'maxfiles' => $ubboard->file_count,
            'accepted_types' => '*',
            'return_types' => FILE_INTERNAL | FILE_CONTROLLED_LINK
        );
    }
    
    /**
     * Returns the options array to use in forum text editor
     *
     * @param context_module $context
     * @param int $bwid
     * @return array
     */
    public static function editor_options(context_module $context) {
        global $COURSE, $PAGE, $CFG;
        
        $maxbytes = get_user_max_upload_file_size($context, $CFG->maxbytes, $COURSE->maxbytes);
        return array(
            'maxfiles' => EDITOR_UNLIMITED_FILES,
            'maxbytes' => $maxbytes,
            'trusttext'=> true,
            'return_types'=> FILE_INTERNAL | FILE_EXTERNAL
        );
    }
    
    /**
     * Form definition
     *
     * @return void
     */
    function definition() {
        global $CFG, $OUTPUT, $USER, $DB;
        
        $mform =& $this->_form;
        
        $CUBBoard = $this->_customdata['CUBBoard'];
        $bwid = $this->_customdata['bwid'];
        
        $cm = $CUBBoard->getCM();
        $modcontext = $CUBBoard->getContext();
        $ubboard = $CUBBoard->getUBBoard();
        
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        
        $typename = Controller::TYPENAME;
        $mform->addElement('hidden', $typename);
        $mform->setType($typename, PARAM_ALPHANUMEXT);
        
        $mform->addElement('hidden', 'bwid');
        $mform->setType('bwid', PARAM_INT);
        
        $mform->addElement('hidden', 'rnum');
        $mform->setType('rnum', PARAM_INT);
        
        
        $mform->addElement('header', 'general', get_string('default_setting', 'ubboard'));//fill in the data depending on page params later using set_data
        
        // 그룹 관련 항목
        if ($ubboard->isBoardType['group']) {
            $groupinfo = array();
            
            // hack alert
            $groupdata = groups_get_activity_allowed_groups($cm);
            
            $selectgroup = groups_get_activity_group($cm);
            
            // 관리 권한이 존재하는 사용자이면 모든 그룹을 볼수 있고,
            // 그이외의 사용자들은 본인이 소속된 그룹만 표시되어야 함.
            if ($CUBBoard->isPermission('manage')) {
                $groupinfo = array('0' => get_string('allparticipants'));
            }
            
            if (!empty($groupdata)) {
                foreach ($groupdata as $grouptemp) {
                    $groupinfo[$grouptemp->id] = $grouptemp->name;
                }
            }
            
            $mform->addElement('select', 'groupid', get_string('group'), $groupinfo);
            $mform->setDefault('groupid', $selectgroup);
        }
        
        // 익명 게시판인 경우
        if ($ubboard->anonymous) {
            $ctlarray = array();
            
            $ctlarray[] = $mform->createElement('text', 'nickname', get_string('nickname', 'ubboard'), 'size="15"');
            $ctlarray[] = $mform->createElement('static', 'nickname_static', '', '<span class="text-info">'.get_string('nickname_input', 'ubboard').'</span>');
            
            $mform->setType('nickname', PARAM_NOTAGS);
            $mform->addGroup($ctlarray, 'nicknamectrl',  get_string('nickname', 'ubboard'), '', '');
        }
        
        // 카테고리
        if ($ubboard->category) {
            $options = array();
            
            if ($categories = $CUBBoard->getCategories()) {
                foreach ($categories as $cs) {
                    $options[$cs->id] = $cs->name;
                }
            }
            $mform->addElement('select', 'categoryid', get_string('category', 'ubboard'), $options);
        }
        
        
        // 제목
        $mform->addElement('text', 'subject', get_string('subject', 'ubboard'), 'size="48"');
        $mform->setType('subject', PARAM_NOTAGS);
        $mform->addRule('subject', get_string('required'), 'required', null, 'client');
        $mform->addRule('subject', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        
        // 공지사항
        if ($ubboard->notice && $CUBBoard->isPermission('manage')) {
            $mform->addElement('checkbox', 'notice', get_string('notice', 'ubboard'), get_string('notice', 'ubboard'));
            
            // 시작일/종료일 설정
            if ($ubboard->isBoardType['notice'] && \mod_ubboard\factory::isCoursePopup($ubboard->course)) {
                $usermidnight = usergetmidnight(time());
                $mform->addElement('date_time_selector', 'stime', get_string('notice_time_start', 'ubboard'), array('optional' => true, 'step' => 1));
                $mform->setDefault('stime', $usermidnight);
                
                $mform->addElement('date_time_selector', 'etime', get_string('notice_time_end', 'ubboard'), array('optional' => true, 'step' => 1));
                $mform->setDefault('etime', $usermidnight + WEEKSECS + DAYSECS - MINSECS);
                
                $mform->disabledif ('stime', 'notice', 'notchecked', 1);
                $mform->disabledif ('etime', 'notice', 'notchecked', 1);
                
                $mform->addElement('static', 'noticedatehelp', '', get_string('notice_time_help', 'ubboard'));
            }
        }
        
        // 비밀글
        if ($ubboard->secret) {
            $mform->addElement('checkbox', 'secret', get_string('secret', 'ubboard'), get_string('secret', 'ubboard'));
        }
        
        // 공개글
        if ($CUBBoard->isCourseOpen()) {
            $ctlarray = array();
            
            $ctlarray[] = $mform->createElement('radio', 'open', '', get_string('open_0', 'ubboard'), 0);
            $ctlarray[] = $mform->createElement('radio', 'open', '', get_string('open_1', 'ubboard'), 1);
            
            $mform->setDefault('open', $CUBBoard->getOpenDefault());
            $mform->addGroup($ctlarray, 'contentselector',  get_string('open_yn', 'ubboard'), '', '');
        }
        
        // 공유 게시글 수정인 경우 입력 폼 추가
        if ($CUBBoard->isShare(false) && $CUBBoard->isPermission('manage') && !empty($bwid)) {
            // 실제 공유된 게시글인지 확인
            if ($shareArticle = $CUBBoard->getShareArticles($bwid)) {
                $shareEditOption = array();
                $shareEditOption[0] = get_string('share_edit_option_0', 'ubboard');
                $shareEditOption[1] = get_string('share_edit_option_1', 'ubboard');
                
                $mform->addElement('select', 'shareedit', get_string('share_edit', 'ubboard'), $shareEditOption);
                
                // 기본적으로는 공유된 게시물 모두 수정. => 개별적으로 수정하는 경우는 그리 많지 않을거라 판단됨.
                $mform->setDefault('shareedit', 1);
            }
        }
        
        // 내용
        $mform->addElement('editor', 'content', get_string('content', 'ubboard'), null, self::editor_options($modcontext));
        $mform->setType('content', PARAM_RAW);
        $mform->addRule('content', get_string('required'), 'required', null, 'client');
        
        
        // 첨부파일 
        $mform->addElement('filemanager', 'attachment', get_string('attachments', 'ubboard'), null, self::attachment_options($ubboard));
        
        // 전체사용자에게 메일 보내기
        if ($ubboard->mailsend && validate_email($USER->email) && !$ubboard->isBoardType['anonymous']) {
            $mform->addElement('checkbox', 'allmailsend', get_string('send', 'ubboard'), get_string('allmailsend', 'ubboard'));
        }
        
        // 공개글을 쓰는데 메일 보내기 기능이 활성화 된 경우에는 "공개"로 설정했을 때에만 활성화 되어 야함.
        if ($ubboard->mailsend && validate_email($USER->email) && !$ubboard->isBoardType['anonymous'] && $CUBBoard->isCourseOpen()) {
            $mform->disabledif ('allmailsend', 'open', 'eq', 0);
            $mform->addElement('static', 'allmailsend_help', '', get_string('allmailsend_open_help', 'ubboard'));
        }
                
        //-------------------------------------------------------------------------------
        $this->add_action_buttons(true, get_string('btn_save', 'ubboard'));
        
    }
    
    function definition_after_data() {
        global $PAGE, $CFG;
        parent::definition_after_data();
        
        $mform = &$this->_form;
        
        $bwid = $mform->getElementValue('bwid');
        
        // 수정인 경우
        if (!empty($bwid)) {
            // 이미 등록된 글에 대해서는 그룹 변경이 불가능함.
            if ($mform->elementExists('groupid')) {
                $groupelement = $mform->getElement('groupid')->updateAttributes(array('disabled', 'disabled'));
            }
        }
    }
    
    /**
     * Form validation
     *
     * @param array $data data from the form.
     * @param array $files files uploaded.
     * @return array of errors.
     */
    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        
        if (empty($data['subject'])) {
            $errors['subject'] = get_string('error_emptysubject', 'ubboard');
        }
        
        if (empty($data['content']['text'])) {
            $errors['content'] = get_string('error_emptycontent', 'ubboard');
        }
        
        return $errors;
    }
}
