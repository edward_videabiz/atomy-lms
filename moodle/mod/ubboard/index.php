<?php

require_once(__DIR__ . '/../../config.php');

$id = required_param('id', PARAM_INT); // Course.

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST);

require_course_login($course);

$params = array(
    'context' => context_course::instance($course->id)
);
$event = \mod_ubboard\event\course_module_instance_list_viewed::create($params);
$event->add_record_snapshot('course', $course);
$event->trigger();

$i8n = get_strings(array(
    'articlecount'
    ,'modulenameplural'
    ,'ubboardname'
), 'mod_ubboard');

// 강좌 번호가 1이면 강좌 포맷이 따로 지정될수 없기 때문에
// 무들 기본 포맷인 weeks값이 사용되도록 변경
$courseFormatName = $course->format;
if ($id == SITEID) {
    $courseFormatName = 'weeks';
}
$i8n->sectionname = get_string('sectionname', 'format_'.$courseFormatName);


$PAGE->set_url('/mod/ubboard/index.php', array('id' => $id));
$PAGE->navbar->add($i8n->modulenameplural);
$PAGE->set_title("$course->shortname: $i8n->modulenameplural");
$PAGE->set_heading($course->fullname);
$PAGE->set_pagelayout('incourse');

echo $OUTPUT->header();
echo $OUTPUT->heading($course->fullname);

if (! $ubboards = get_all_instances_in_course('ubboard', $course)) {
    notice(get_string('noboards', 'mod_ubboard'), new moodle_url('/course/view.php', array('id' => $course->id)));
}

$usesections = course_format_uses_sections($course->format);

$table = new html_table();
$table->attributes['class'] = 'generaltable mod_index';

$table->head = array();
if ($usesections) {
    $strsectionname = get_string('sectionname', 'format_'.$course->format);
    $table->head[] = $i8n->sectionname;
}

$table->head[] = $i8n->ubboardname;
$table->head[] = $i8n->articlecount;

$modinfo = get_fast_modinfo($course);
$currentsection = '';

$query = "SELECT COUNT(1) FROM {ubboard_write} WHERE ubboardid = :ubboardid AND isdelete = 0";
foreach ($modinfo->instances['ubboard'] as $cm) {
    $row = array();
    if ($usesections) {
        if ($cm->sectionnum !== $currentsection) {
            if ($cm->sectionnum) {
                $row[] = get_section_name($course, $cm->sectionnum);
            }
            if ($currentsection !== '') {
                $table->data[] = 'hr';
            }
            $currentsection = $cm->sectionnum;
        }
    }

    $class = $cm->visible ? null : array('class' => 'dimmed');

    $row[] = html_writer::link(new moodle_url('view.php', array('id' => $cm->id)), $cm->get_formatted_name(), $class);
    
    // 게시글 총 갯수
    $row[] = $DB->get_field_sql($query, array('ubboardid'=>$cm->instance));
    
    $table->data[] = $row;
}

echo html_writer::table($table);

echo $OUTPUT->footer();
