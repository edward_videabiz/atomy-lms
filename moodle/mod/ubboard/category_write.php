<?php
use local_ubion\controller\Controller;

require_once __DIR__.'/_head.php';


$PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'defaultSetting');

$mformCategory = new mod_ubboard_category_form('category_write.php',
    array(
        'CUBBoard'=>$CUBBoard,
    )
    ,'post'
    ,''
    ,array('id' => 'mformubboard')
);


$typename = Controller::TYPENAME;
$data = new \stdClass();
$data->id = $cm->id;
$data->$typename = 'categoryInsert';

// 취소 버튼을 클릭한 경우
if ($mformCategory->is_cancelled()) {
    redirect($CFG->wwwroot.'/mod/ubboard/category.php?id='.$id);
} else if ($fromform = $mformCategory->get_data()) {
    $typename = $CUBBoard::TYPENAME;
    $coursemostype = $fromform->$typename;
    
    if ($CUBBoard->isSubmit($coursemostype)) {
        $CUBBoard->invoke($coursemostype);
    }
} else {
    // 글 수정인 경우
    if (!empty($categoryid)) {
        $data = $CUBBoard->getCategory($categoryid, true);
        $data->id = $cm->id;        // id값은 반드시 cmid로 전달해줘야됨.
        $data->categoryid = $categoryid;
        $data->$typename = 'categoryUpdate';
    }
    
    
    // 상황에 따라 mform_write에 기본값으로 셋팅되어야 할 값을 설정해줘야됨.
    $mformCategory->set_data($data);
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading(format_string($ubboard->name), 2);
    
    $mformCategory->display();
    
    echo $OUTPUT->footer();
}