<?php
defined('MOODLE_INTERNAL') || die();

/* Moodle core API */

/**
 * Returns the information on whether the module supports a feature
 *
 * See {@link plugin_supports()} for more info.
 *
 * @param string $feature
 *            FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function ubboard_supports($feature)
{
    switch ($feature) {
        case FEATURE_GROUPS:
            return true;
        case FEATURE_GROUPINGS:
            return true;
        case FEATURE_GROUPMEMBERSONLY:
            return true;
        case FEATURE_MOD_INTRO:
            return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS:
            return false;
        case FEATURE_COMPLETION_HAS_RULES:
            return false;
        case FEATURE_GRADE_HAS_GRADE:
            return false;
        case FEATURE_GRADE_OUTCOMES:
            return false;
        case FEATURE_BACKUP_MOODLE2:
            return true;
        case FEATURE_MODEDIT_DEFAULT_COMPLETION:
            return false;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the ubboard into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param stdClass $ubboard
 *            Submitted data from the form in mod_form.php
 * @param mod_ubboard_mod_form $mform
 *            The form instance itself (if needed)
 * @return int The id of the newly inserted ubboard record
 */
function ubboard_add_instance(stdClass $ubboard, mod_ubboard_mod_form $mform = null)
{
    // 게시판 추가 당시에는 cmid값이 존재하지 않기 때문에 게시판 class에 곧바로 접근해야됨.
    $ubboard->id = \mod_ubboard\factory::getClassName($ubboard->type)::setAddInstance($ubboard);
    
    // ubboard_grade_item_update($ubboard);
    
    return $ubboard->id;
}

/**
 * Updates an instance of the ubboard in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param stdClass $ubboard
 *            An object from the form in mod_form.php
 * @param mod_ubboard_mod_form $mform
 *            The form instance itself (if needed)
 * @return boolean Success/Fail
 */
function ubboard_update_instance(stdClass $ubboard, mod_ubboard_mod_form $mform = null)
{
    global $DB;
    
    $oldUbboard = $DB->get_record_sql('SELECT * FROM {ubboard} WHERE id = :id', array('id' => $ubboard->instance));
    
    if (isset($ubboard->type)) {
        
        $boardtype = $ubboard->type;
        
        // 익명게시판이면 다른 게시판으로 설정값이 변경되어서는 안됨.
        if ($oldUbboard->type == 'anonymous') {
            unset($ubboard->type);
            $boardtype = $oldUbboard->type;
        }
    } else {
        $boardtype = $oldUbboard->type;
    }
    
    
    
    $result = \mod_ubboard\factory::getClassName($boardtype)::setUpdateInstance($ubboard);
    
    // ubboard_grade_item_update($ubboard);
    
    return $result;
}

/**
 * Removes an instance of the ubboard from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id
 *            Id of the module instance
 * @return boolean Success/Failure
 */
function ubboard_delete_instance($id)
{
    global $DB;
    
    if (! $ubboard = $DB->get_record('ubboard', array('id' => $id))) {
        return false;
    }
    
    // Delete any dependent records here.
    $result = \mod_ubboard\factory::getClassName($ubboard->type)::setDeleteInstance($ubboard);
    
    if (!$result) {
        return false;
    }
    
    // ubboard_grade_item_delete($ubboard);
    
    return true;
}

/**
 * Returns a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 *
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @param stdClass $course
 *            The course record
 * @param stdClass $user
 *            The user record
 * @param cm_info|stdClass $mod
 *            The course module info object or record
 * @param stdClass $ubboard
 *            The ubboard instance record
 * @return stdClass|null
 */
function ubboard_user_outline($course, $user, $mod, $ubboard)
{
    global $CFG, $DB;
    
    $result = new stdClass();
    $result->info = null;
    $result->time = null;
    
    $sql = 'SELECT
					COUNT(1) AS cnt
					,MAX(timecreated) AS lastarticle
			FROM	{ubboard_write}
			WHERE	course = :course
            AND		ubboardid = :ubboardid
			AND		userid = :userid
			AND		isdelete = 0';
    
    $writeInfo = null;
    $writeTime = 0;
    if ($write = $DB->get_record_sql($sql, array('course'=>$course->id, 'userid'=>$user->id, 'ubboardid'=>$ubboard->id))) {
        $writeInfo = get_string("numarticle", "ubboard", $write->cnt);
        $writeTime = $write->lastarticle;
    }
    
    
    $sql = 'SELECT
					COUNT(1) AS cnt
					,MAX(timecreated) AS lastcomment
			FROM	{ubboard_comment}
			WHERE	course = :course
			AND		ubboardid = :ubboardid
			AND		userid = :userid
			AND		isdelete = 0';
    
    $commentInfo = null;
    $commentTime = 0;
    if ($comment = $DB->get_record_sql($sql, array('course'=>$course->id, 'userid'=>$user->id, 'ubboardid'=>$ubboard->id))) {
        $commentInfo = get_string("numcomments", "ubboard", $comment->cnt);
        $commentTime = $comment->lastcomment;
    }
    
    
    if ($write->cnt > 0 && $comment->cnt > 0) {
        $result->info = $writeInfo.', '.$commentInfo;
        
        $time = ($writeTime > $commentTime) ? $writeTime : $commentTime;
        $result->time = $time;
    } else if ($write->cnt > 0) {
        $result->info = $writeInfo;
        $result->time = $writeTime;
    } else if ($comment->cnt > 0) {
        $result->info = $commentInfo;
        $result->time = $commentTime;
    } else {
        return null;
    }
    
    
    return $result;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * It is supposed to echo directly without returning a value.
 *
 * @param stdClass $course
 *            the current course record
 * @param stdClass $user
 *            the record of the user we are generating report for
 * @param cm_info $mod
 *            course module info
 * @param stdClass $ubboard
 *            the module instance record
 */
function ubboard_user_complete($course, $user, $mod, $ubboard)
{
    global $CFG, $USER, $OUTPUT, $DB;
    
    // 본인과 관련된 글 정보 가져오기
    $query = "SELECT
						*
			FROM		{ubboard_write}
			WHERE		course = :course
			AND			ubboardid = :ubboardid
			AND			userid = :userid
			AND			isdelete = 0
			ORDER BY 	num DESC, reply, timecreated DESC";
    
    echo '<li>';
    echo 	'<h5>'.get_string('articles', 'ubboard').'</h5>';
    echo 	'<table class="table table-bordered table-coursemos">';
    echo		'<colgroup>';
    echo			'<col class="wp-50" />';
    echo			'<col />';
    echo		'</colgroup>';
    echo		'<thead>';
    echo			'<tr>';
    echo				'<th class="text-center">'.get_string('number', 'local_ubion').'</th>';
    echo				'<th>'.get_string('subject', 'ubboard').'</th>';
    echo 			'</tr>';
    echo		'</thead>';
    echo 		'<tbody>';
    if ($write = $DB->get_records_sql($query, array('course'=>$course->id, 'ubboardid'=>$ubboard->id, 'userid'=>$user->id))) {
        $totalCount = count($write);
        foreach ($write as $w) {
            $url = new moodle_url('/mod/ubboard/article.php', array('id'=>$mod->id, 'bwid'=>$w->id));
            $alink = html_writer::link($url, $w->subject);
            
            echo 	'<tr>';
            echo		'<td class="text-center">'.$totalCount.'</td>';
            echo		'<td>'.$alink.'</td>';
            echo 	'</tr>';
            $totalCount--;
        }
    } else {
        echo 		'<tr><td class="text-center" colspan="2">'.get_string('noarticles', 'ubboard').'</td></tr>';
    }
    echo 		'</tbody>';
    echo 	'</table>';
    echo '</li>';
    
    
    // 코멘트
    $query = "SELECT
						*
			FROM		{ubboard_comment}
			WHERE		course = :course
			AND			ubboardid = :ubboardid
			AND			userid = :userid
			AND			isdelete = 0
			ORDER BY 	num DESC, reply, timecreated DESC";
    
    echo '<li>';
    echo    '<h5>'.get_string('comments', 'ubboard').'</h5>';
    echo 	'<table class="table table-bordered table-coursemos">';
    echo		'<colgroup>';
    echo			'<col class="wp-50" />';
    echo			'<col />';
    echo		'</colgroup>';
    echo		'<thead>';
    echo			'<tr>';
    echo				'<th class="text-center">'.get_string('number', 'local_ubion').'</th>';
    echo				'<th>'.get_string('comments', 'ubboard').'</th>';
    echo 			'</tr>';
    echo		'</thead>';
    echo 		'<tbody>';
    if ($comment = $DB->get_records_sql($query, array('course'=>$course->id, 'ubboardid'=>$ubboard->id, 'userid'=>$user->id))) {
        $totalCount = count($comment);
        foreach ($comment as $c) {
            $url = new moodle_url('/mod/ubboard/article.php', array('id'=>$mod->id, 'bwid'=>$c->bwid));
            $alink = html_writer::link($url, format_text($c->comment));
            
            echo 	'<tr>';
            echo		'<td class="text-center">'.$totalCount.'</td>';
            echo		'<td>'.$alink.'</td>';
            echo 	'</tr>';
            $totalCount--;
        }
    } else {
        echo 		'<tr><td class="text-center" colspan="2">'.get_string('nocomments', 'ubboard').'</td></tr>';
    }
    echo 		'</tbody>';
    echo 	'</table>';
    echo '</li>';
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in ubboard activities and print it out.
 *
 * @param stdClass $course
 *            The course record
 * @param bool $viewfullnames
 *            Should we display full names
 * @param int $timestart
 *            Print activity since this timestamp
 * @return boolean True if anything was printed, otherwise false
 */
function ubboard_print_recent_activity($course, $viewfullnames, $timestart)
{
    // TODO 전달된 시간 이후에 강좌내에서 이뤄진 활동에 대해서 리턴해줘야됨.
    // 게시판 타입 및 권한에 따라서 전달되는 항목들도 달라져야될듯 함.
    
    return false;
}

/**
 * Prepares the recent activity data
 *
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link ubboard_print_recent_mod_activity()}.
 *
 * Returns void, it adds items into $activities and increases $index.
 *
 * @param array $activities
 *            sequentially indexed array of objects with added 'cmid' property
 * @param int $index
 *            the index in the $activities to use for the next record
 * @param int $timestart
 *            append activity since this time
 * @param int $courseid
 *            the id of the course we produce the report for
 * @param int $cmid
 *            course module id
 * @param int $userid
 *            check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid
 *            check for a particular group's activity only, defaults to 0 (all groups)
 */
function ubboard_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid = 0, $groupid = 0)
{
    // TODO 전달된 사용자 및 그룹에 대해서 최근글을 리턴해줘야됨.
    
}

/**
 * Prints single activity item prepared by {@link ubboard_get_recent_mod_activity()}
 *
 * @param stdClass $activity
 *            activity record with added 'cmid' property
 * @param int $courseid
 *            the id of the course we produce the report for
 * @param bool $detail
 *            print detailed report
 * @param array $modnames
 *            as returned by {@link get_module_types_names()}
 * @param bool $viewfullnames
 *            display users' full names
 */
function ubboard_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames)
{
    // TODO 전달된 사용자 및 그룹에 대해서 최근글을 출력 시켜줘야됨.
}

/**
 * Function to be run periodically according to the moodle cron
 *
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * Note that this has been deprecated in favour of scheduled task API.
 *
 * @return boolean
 */
function ubboard_cron()
{
    return true;
}


/* Gradebook API */

/**
 * Is a given scale used by the instance of ubboard?
 *
 * This function returns if a scale is being used by one ubboard
 * if it has support for grading and scales.
 *
 * @param int $ubboardid ID of an instance of this module
 * @param int $scaleid ID of the scale
 * @return bool true if the scale is used by the given ubboard instance
 
function ubboard_scale_used($ubboardid, $scaleid) {
    global $DB;

    if ($scaleid and $DB->record_exists('ubboard', array('id' => $ubboardid, 'grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}
*/

/**
 * Checks if scale is being used by any instance of ubboard.
 *
 * This is used to find out if scale used anywhere.
 *
 * @param int $scaleid ID of the scale
 * @return boolean true if the scale is used by any ubboard instance

function ubboard_scale_used_anywhere($scaleid) {
    global $DB;

    if ($scaleid and $DB->record_exists('ubboard', array('grade' => -$scaleid))) {
        return true;
    } else {
        return false;
    }
}
 */

/**
 * Creates or updates grade item for the given ubboard instance
 *
 * Needed by {@link grade_update_mod_grades()}.
 *
 * @param stdClass $ubboard instance object with extra cmidnumber and modname property
 * @param bool $reset reset grades in the gradebook
 * @return void
 
function ubboard_grade_item_update(stdClass $ubboard, $reset=false) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    $item = array();
    $item['itemname'] = clean_param($ubboard->name, PARAM_NOTAGS);
    $item['gradetype'] = GRADE_TYPE_VALUE;

    if ($ubboard->grade > 0) {
        $item['gradetype'] = GRADE_TYPE_VALUE;
        $item['grademax']  = $ubboard->grade;
        $item['grademin']  = 0;
    } else if ($ubboard->grade < 0) {
        $item['gradetype'] = GRADE_TYPE_SCALE;
        $item['scaleid']   = -$ubboard->grade;
    } else {
        $item['gradetype'] = GRADE_TYPE_NONE;
    }

    if ($reset) {
        $item['reset'] = true;
    }

    grade_update('mod/ubboard', $ubboard->course, 'mod', 'ubboard',
            $ubboard->id, 0, null, $item);
}
*/


/**
 * Delete grade item for given ubboard instance
 *
 * @param stdClass $ubboard instance object
 * @return grade_item

function ubboard_grade_item_delete($ubboard) {
    global $CFG;
    require_once($CFG->libdir.'/gradelib.php');

    return grade_update('mod/ubboard', $ubboard->course, 'mod', 'ubboard',
            $ubboard->id, 0, null, array('deleted' => 1));
}
 */

/**
 * Update ubboard grades in the gradebook
 *
 * Needed by {@link grade_update_mod_grades()}.
 *
 * @param stdClass $ubboard instance object with extra cmidnumber and modname property
 * @param int $userid update grade of specific user only, 0 means all participants
 
function ubboard_update_grades(stdClass $ubboard, $userid = 0) {
    global $CFG, $DB;
    require_once($CFG->libdir.'/gradelib.php');

    // Populate array of grade objects indexed by userid.
    $grades = array();

    grade_update('mod/ubboard', $ubboard->course, 'mod', 'ubboard', $ubboard->id, 0, $grades);
}
*/

/* File API */

/**
 * Returns the lists of all browsable file areas within the given module context
 *
 * The file area 'intro' for the activity introduction field is added automatically
 * by {@link file_browser::get_file_info_context_module()}
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @return array of [(string)filearea] => (string)description
 */
function ubboard_get_file_areas($course, $cm, $context)
{
    return array(
        \mod_ubboard\core\ubboard::FILEAREA_ARTICLE => get_string('areaarticle', 'mod_ubboard')
        ,\mod_ubboard\core\ubboard::FILEAREA_ATTACHMENT => get_string('areaattachment', 'mod_ubboard')
    );
}

/**
 * File browsing support for ubboard file areas
 *
 * @package mod_ubboard
 * @category files
 *          
 * @param file_browser $browser
 * @param array $areas
 * @param stdClass $course
 * @param stdClass $cm
 * @param stdClass $context
 * @param string $filearea
 * @param int $itemid
 * @param string $filepath
 * @param string $filename
 * @return file_info instance or null if not found
 */
function ubboard_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename)
{
    global $CFG, $DB, $USER;
    
    if ($context->contextlevel != CONTEXT_MODULE) {
        return null;
    }
    
    // filearea must contain a real area
    if (!isset($areas[$filearea])) {
        return null;
    }
    
    // Note that forum_user_can_see_post() additionally allows access for parent roles
    // and it explicitly checks qanda forum type, too. One day, when we stop requiring
    // course:managefiles, we will need to extend this.
    if (!has_capability('mod/ubboard:article', $context)) {
        return null;
    }
    
    $fs = get_file_storage();
    
    if ($filearea === \mod_ubboard\core\ubboard::FILEAREA_ATTACHMENT) {
        $filepath = is_null($filepath) ? '/' : $filepath;
        $filename = is_null($filename) ? '.' : $filename;
        
        if (!($storedfile = $fs->get_file($context->id, 'mod_ubboard', $filearea, $itemid, $filepath, $filename))) {
            return null;
        }
        
        $urlbase = $CFG->wwwroot.'/pluginfile.php';
        return new file_info_stored($browser, $context, $storedfile, $urlbase, $filearea, $itemid, true, true, false);
    }
    
    return null;
}

/**
 * Serves the files from the ubboard file areas
 *
 * @package mod_ubboard
 * @category files
 *          
 * @param stdClass $course
 *            the course object
 * @param stdClass $cm
 *            the course module object
 * @param stdClass $context
 *            the ubboard's context
 * @param string $filearea
 *            the name of the file area
 * @param array $args
 *            extra arguments (itemid, path)
 * @param bool $forcedownload
 *            whether or not force download
 * @param array $options
 *            additional options affecting the file serving
 */
function ubboard_pluginfile($course, $cm, $context, $filearea, array $args, $forcedownload, array $options = array())
{
    global $CFG, $DB;
    
    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }
    
    // filearea가 article인 경우에는 editor에서 올린 경우임.
    // 전체 메일 발송시 이미지 링크가 깨지기 때문에 예외처리 해야됨.
    if ($filearea != \mod_ubboard\core\ubboard::FILEAREA_ARTICLE) {
        // 로그인 체크
        require_course_login($course, true, $cm);
    }
    
    // 첫번째로 넘어오는 파라메터가 게시판 아이디 임
    $id = (int)array_shift($args);
    
    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/mod_ubboard/$filearea/$id/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }
    
    send_stored_file($file, 0, 0, true); // download MUST be forced - security!
}