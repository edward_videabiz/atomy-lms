<?php
require_once '_head.php';
use \local_ubion\base\Common;

// bwid가 전달되지 않으면 에러
if (empty($bwid)) {
    \local_ubion\base\Common::printNotice(get_string('error_articlenum', 'ubboard'), $ubboardViewUrl);
}

// 게시물 view 권한 체크
$CUBBoard->isPermission('article', true);

// 게시글
$article = $CUBBoard->getArticle($bwid);

// 게시판 타입별로 권한 체크가 추가적으로 필요한 경우가 있음.
list ($isView, $isViewErrorMessage) = $CUBBoard->isViewExtend($article);

// 게시물을 볼 권한이 없는 경우 에러 내용 출력
if (!$isView) {
    Common::printNotice($isViewErrorMessage, $ubboardViewUrl);
}

// 삭제된 글인지 확인
if ($article->isdelete == 1) {
    Common::printNotice(get_string('deleted_article', 'ubboard'), $ubboardViewUrl);
}

// 공개글인지 확인
if (!empty($CUBBoard->isCourseOpen())) {
    if (!$CUBBoard->isPermission('manage')) {
        // 해당 글이 공개되어 있거나, 본인이 작성한 글이 아니라면 에러표시해줘야됨.
        if (empty($article->open) && $article->userid != $USER->id) {
            Common::printNotice(get_string('not_open_article', 'ubboard'), $ubboardViewUrl);
        }
    }
}


// 조회수 증가
// !!중요!! 권한 및 필수값 체크가 종료된 후 조회수를 증가시켜야됩니다.
$isViewCntChange = $CUBBoard->setViewCntUp($bwid);

// 조회수 증가 이후 화면상에 표시되는 viewcnt도 1증가시켜줘야됨.
if ($isViewCntChange) {
    $article->view_cnt += 1;
    
    $article->numformat_view = number_format($article->view_cnt);
}

// 비밀글 체크
if (!$CUBBoard->isSecretArticleView($article)) {
    \local_ubion\base\Common::printNotice(get_string('secret_not_read', 'ubboard'), $ubboardViewUrl);
}

// 목록 버튼에 불필요한 파라메터 제거
$listparam = \local_ubion\base\Common::getArrayCopy($getparam);
unset($listparam['bwid']);

// 버튼 
$buttons = new stdClass();
$buttons->list =  $CUBBoard->setButton(get_string('btn_list', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/view.php', 'btn btn-default btn-list', $listparam);

if ($CUBBoard->isPermission('write')) {
    $buttons->write = $CUBBoard->setButton(get_string('btn_write', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/write.php?id='.$cm->id, 'btn btn-primary btn-write');    
}

if ($CUBBoard->isPermission('reply')) {
    $buttons->reply = $CUBBoard->setButton(get_string('btn_reply', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/write.php?id='.$cm->id.'&rnum='.$bwid, 'btn btn-default btn-write-reply');
}

// 본인 글인 경우에만 수정 및 삭제 버튼이 표시되어야 함.
if ($CUBBoard->isMyArticle($article)) {
    $buttons->modify = $CUBBoard->setButton(get_string('btn_modify', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/write.php?id='.$cm->id.'&bwid='.$bwid, 'btn btn-default btn-modify');
    
    if ($CUBBoard->isPermission('manage') || $CUBBoard->isPermission('delete')) {
        $buttons->delete = $CUBBoard->setButton(get_string('btn_delete', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/article.php?id='.$cm->id.'&bwid='.$bwid, 'btn btn-danger btn-delete');
    }
}

// 공개여부에 따라 버튼 표시
if ($CUBBoard->isCourseOpen() && ($CUBBoard->isPermission('manage') || $article->userid == $USER->id)) {
    
    $openButtonDataVal = 1;
    $openButtonText = 'open_conversion_1';
    if ($article->open) {
        $openButtonDataVal = 0;
        $openButtonText = 'open_conversion_0';
    }
    
    $buttons->open = $CUBBoard->setButton(get_string($openButtonText, 'ubboard'), $openButtonDataVal, 'btn btn-default btn-open');
}

// 공유 관련항목
$share = new stdClass;
$share->articles = array();
$share->articleCount = 0;

// 공유버튼은 익명 게시판에서는 표시되면 안됨.
if ($CUBBoard->isShare() && $CUBBoard->isPermission('manage')) {
    $buttons->share = $CUBBoard->setButton(get_string('btn_share', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/_share.php?id='.$id.'&boardtype='.$ubboard->type.'&bwid='.$bwid, 'btn btn-default btn-share');
}

// 해당 게시물이 공유된 게시물인지 확인
if ($CUBBoard->isShare(false) && $CUBBoard->isPermission('manage')) {   
    // 공유된 글에서 확인 한 경우 원본글과 관련된 글 찾아오기
    if ($share->articles = $CUBBoard->getShareArticles($bwid)) {
        $share->articleCount = count($share->articles);
        
        // mustache 에서 사용하려면 key를 제거해줘야됨.
        $share->articles = array_values($share->articles);
    }
}

// 카테고리 관련 버튼
if ($CUBBoard->isPermission('manage')) {
    $buttons->category_manage = $CUBBoard->setButton(get_string('category_manage', 'ubboard'), $CFG->wwwroot.'/mod/ubboard/category.php?id='.$cm->id);
}

// list 정보 가져오기
$lists = array();
list($totalcount, $articles, $categories) = $CUBBoard->getLists();
if ($totalcount > 0) {
    list ($lists, $filecount) = $CUBBoard->_getLists($totalcount, $articles, $categories, $bwid);
    
    // mustache에서는 key가 부여되어 있으면 안됨.
    $lists = array_values($lists);
}


// 코멘트 관련 설정 가져오기
$comments = null;
if ($ubboard->comment) {
    $PAGE->requires->strings_for_js(array(
        'confirm_comment_delete'
    ), 'ubboard');
    $PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'comment', array(
        'id' => $id
        ,'bwid' => $bwid
    ));
    $comments = $CUBBoard->getComments($bwid);
    
    // mustache 때문에 index 제거해서 보내줘야됨.
    $comments = array_values($comments);
}

// renderer에 전달될 데이터
$mustache = new stdClass();
$mustache->article = $article;
$mustache->buttons = $buttons;
$mustache->lists = $lists;
$mustache->colspan = $CUBBoard->getColspan();
$mustache->categories = $CUBBoard->_getCategories($categories);
$mustache->selectedButtons = $CUBBoard->getSelectedButtons();
$mustache->icons = $CUBBoard->getIcons();
$mustache->paging = \local_ubion\base\Paging::getPaging($totalcount, $page, $ls);
$mustache->comment = $comments;
$mustache->commentCount = count($comments);
$mustache->share = $share;

$PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'article', array(
    'id' => $id
    ,'bwid' => $bwid
));

// 관리자 기능
if ($CUBBoard->isPermission('manage')) {
    $PAGE->requires->strings_for_js(array(
        'confirm_select_article_delete'
        ,'selected_noarticle'
        ,'confirm_select_article_open'
        ,'confirm_select_article_open_cancel'
    ), 'ubboard');
    
    $PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'manage');
}

$PAGE->requires->strings_for_js(array(
    'confirm_article_delete'
    ,'confirm_article_delete_shareall'
    ,'confirm_article_open_y'
    ,'confirm_article_open_n'
), 'ubboard');

// 카테고리를 사용하면 카테고리 관련된 스크립트를 로드해야됨.
if ($ubboard->category) {
    $PAGE->requires->js_call_amd('mod_ubboard/ubboard', 'category', array(
        'id' => $cm->id
    ));
}

// moodle 2.7 event 실행.
// 더이상 add_to_log는 사용되지 않습니다.
$params = array(
    'context' => $context
    ,'objectid' => $bwid
);
$event = \mod_ubboard\event\article_viewed::create($params);
$event->add_record_snapshot('ubboard', $ubboard);
$event->add_record_snapshot('ubboard_write', $article);
$event->trigger();

echo $OUTPUT->header();
echo $CUBBoard->renderer('article', $mustache);
echo $OUTPUT->footer();