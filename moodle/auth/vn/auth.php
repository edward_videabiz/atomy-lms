<?php
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/authlib.php');

/**
 * 베트남
 */
class auth_plugin_vn extends auth_plugin_base
{	
	/**
	 * Constructor.
	 */
	public function __construct()
	{
	    $this->authtype = 'vn';
	    $this->config = get_config('auth_vn');
	}

	/**
	 * 일반 로그인 기능은 제공하지 않음.
	 *
	 * @see auth_plugin_base::user_login()
	 */ 
	function user_login ($username, $password)
	{
	    // username이 무조건 소문자로 치환되기 때문에 이곳에서 처리 하면 안됩니다.
	    // 만약 atomy가 아이디가 대소문자 구분이 없다면 상관 없음
	    return false;
	}
	
	function loginpage_hook() {
	    $username = $_POST['username'] ?? null;
	    $password = $_POST['password'] ?? null;
	    
	    // 관리자, 테스트 계정은 따로 API를 호출할 필요가 없음
	    if ($username != 'admin' && strpos($username, 'cos') === false && strpos($username, 'atomystudent') === false) {
    	    if (!empty($username) && !empty($password)) {
    	       \local_vn\API::getInstance()->loginIDPW($username, $password);
    	    }
	    }
	}

    /**
	 * Returns true if this authentication plugin is "internal".
	 *
	 * Internal plugins use password hashes from Moodle user table for authentication.
	 *
	 * @return bool
	 */
	function is_internal()
	{
	    return false;
	}
}