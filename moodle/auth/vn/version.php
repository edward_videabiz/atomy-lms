<?php
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2018032200;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2017111301;        // Requires this Moodle version
$plugin->component = 'auth_vn';      // Full name of the plugin (used for diagnostics)
$plugin->dependencies = array('local_ubion' => 2017061500, 'local_vn' => 2018090702);