<?php
$string['default_login'] = '일반 로그인';
$string['go_portal'] = '[전주대학교 바로가기]';
$string['not_login'] = '로그인이 되어 있지 않습니다. 로그인 후 이용해주시기 바랍니다.';
$string['pluginname'] = '대학교 SSO';
$string['user_edit'] = '개인정보수정';
$string['user_edit_message'] = '<p>이메일 등록 및 개인정보수정은 대학교 홈페이지에서만 가능합니다.</p>
<p>사진을 등록하려면 이메일이 먼저 등록되어야 합니다.</p>';