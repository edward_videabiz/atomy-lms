<?php
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/authlib.php');

/**
 * 대학교 SSO
 */
class auth_plugin_univ extends auth_plugin_base {

	private $sso;

	
	/**
	 * Constructor.
	 */
	public function __construct() {
	    $this->authtype = 'univ';
	    $this->config = get_config('auth_univ');
	}

	/**
	 * 일반 로그인 기능은 제공하지 않음.
	 *
	 * @see auth_plugin_base::user_login()
	 */
	function user_login ($username, $password) {
		global $CFG;

		/*
		$univsso = new UnivSso();
		// 로그인 성공했다면 webLogin()함수에서 redirect됨.
		return $univsso->login($username, $password);
		*/
		return false;
	}

	/**
	 * 사용자 변경 페이지 변경
	 * @see auth_plugin_base::edit_profile_url()
	 */
	function edit_profile_url() {
		global $CFG;

		return $CFG->wwwroot."/user/user_edit.php";
	}
}