<?php
namespace format_ubsweeks;


use local_ubion\controller\Controller;
use local_ubion\base\Parameter;
use local_ubion\base\Javascript;

class Holiday extends Controller {

    private static $instance;
    protected $pluginname = 'format_ubsweeks';
    
    /**
     * 강좌 Class
     * @return Holiday
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }

    
    public function getListAll($rebuild = false)
    {
        global $DB;
        
        // 휴일일이 갯수가 많아지지는 않을거 같아서 전체를 연도 구분없이 캐시 생성하도록 되어 있음.
        // 계절학기에만 일자별 포맷을 쓰기 때문에 휴일 갯수가 그리 많지 않을거 같음.
        // 1년에 많아야 10일?? 많아도 20개... 10년동안 써도 200개정도 밖에 되지 않아서 일단은 별다른 조건은 걸지 않음.
        
        $cache = \cache::make('format_ubsweeks', 'holiday');
        
        // 캐시 key값이 필수값이라서 SITEID를 사용함.
        $cacheKey = SITEID;
        
        $holiday = null;
        if($rebuild) {
            $holiday = false;
        } else {
            $holiday = $cache->get($cacheKey);
        }
        
        // 캐시를 재생성하는 경우
        // holiday가 false인 경우게 $rebuld가 true인 경우도 있지만, 캐시에 담긴 항목이 없으면 false로 리턴해줍니다. 
        if($holiday === false) {
            $holiday = $DB->get_records_sql("SELECT * FROM {format_ubsweeks_holiday} ORDER BY repeated DESC, days");
            
            $cache->set($cacheKey, $holiday);
        }
        
        return $holiday;
    }

    public function doInsert() 
    {
        global $CFG;
        
        $day = Parameter::request('day', null, PARAM_ALPHANUMEXT);
        $name = Parameter::request('name', null, PARAM_NOTAGS);
        $repeat = Parameter::request('repeat', 0, PARAM_INT);
        $visible = Parameter::request('visible', 1, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'day' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('holiday_day', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $day
            )
            ,'name' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('holiday_name', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $name
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            if($this->setInsert($day, $name, $repeat, $visible)) {
                if(Parameter::isAajx()) {
                    Javascript::printAlert(get_string('save_complete', 'local_ubion'), true);
                } else {
                    redirect($this->redirectUrl());
                }
            } else {
                Javascript::printAlert(get_string('db_insert_failed', 'local_ubion'));
            }
        }
    }
    
    
    public function setInsert($day, $name, $repeat=0, $visible=1)
    {
        global $DB;
        
        // 사전에 동일한 일자가 등록되었는지 확인해봐야됨.
        if(!is_number($day)) {
            $day = strtotime($day);
        }
        
        $newid = $this->getExistView($day);
        
        if(empty($newid)) {
            $addHoliday = new \stdClass();
            $addHoliday->days = $day;
            $addHoliday->name = $name;
            $addHoliday->repeated = $repeat;
            $addHoliday->visible = $visible;
            $addHoliday->timecreated = time();
            $newid = $DB->insert_record('format_ubsweeks_holiday', $addHoliday);

            $this->purgeCache();
        }
        
        return $newid;
    }
    
    
    public function doUpdate()
    {
        global $CFG;
    
        $id = Parameter::request('id', null, PARAM_INT);
        $day = Parameter::request('day', null, PARAM_ALPHANUMEXT);
        $name = Parameter::request('name', null, PARAM_NOTAGS);
        $repeat = Parameter::request('repeat', 0, PARAM_INT);
        $visible = Parameter::request('visible', 1, PARAM_INT);
        
        
        $validate = $this->validate()->make(array(
            'id' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('holiday_id', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $id
            )
            ,'day' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('holiday_day', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $day
            )
            ,'name' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('holiday_name', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $name
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {    
            if($this->getView($id)) {
                $this->setUpdate($id, $day, $name, $repeat, $visible);
                
                if(Parameter::isAajx()) {
                    Javascript::printAlert(get_string('update_complete', 'local_ubion'), true);
                } else {
                    redirect($this->redirectUrl());
                }
            } else {
                Javascript::printAlert(get_string('holiday_nodata', $this->pluginname));
            }
        }
    }
    
    
    public function setUpdate($id, $day, $name, $repeat=0, $visible=1)
    {
        global $DB;
    
        // 사전에 동일한 일자가 등록되었는지 확인해봐야됨.
        if(!is_number($day)) {
            $day = strtotime($day);
        }
    
        if(!empty($id)) {
            $holiday = new \stdClass();
            $holiday->id = $id;
            $holiday->days = $day;
            $holiday->name = $name;
            $holiday->repeated = $repeat;
            $holiday->visible = $visible;
            $DB->update_record('format_ubsweeks_holiday', $holiday);
            
            $this->purgeCache();
        }
    }
    
    
    public function getExistView($day) 
    {
        global $DB;
        
        if(!is_number($day)) {
            $day = strtotime($day);
        }
        
        return $DB->get_field_sql("SELECT id FROM {format_ubsweeks_holiday} WHERE days = :day", array('day'=>$day));
    }
    
    
    public function doView()
    {
        $id = Parameter::request('id', 0, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'id' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('holiday_id', $this->pluginname)
                ,$this->validate()::PARAMVALUE => $id
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            
            
            if ($holiday = $this->getView($id)) {
                $holiday->day_string = date('Y-m-d', $holiday->days);
                
                unset($holiday->days);
                Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'holiday'=>$holiday));
            } else {
                Javascript::printAlert(get_string('holiday_nodata', $this->pluginname));
            }
        }
    }
    
    public function getView($id)
    {
        global $DB;
        
        return $DB->get_record_sql("SELECT * FROM {format_ubsweeks_holiday} WHERE id = :id", array('id'=>$id));
    }
    
    
    /**
     * 신청 내역 삭제 (실제 DB 작업은 setDelete에서 진행됩니다.)
     */
    function doDelete()
    {
        global $CFG;
        
        $id = Parameter::request('id', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'id' => array(
                'required', 'number'
                , $this->validate()::PLACEHOLDER => get_string('holiday_id', $this->pluginname)
                , $this->validate()::PARAMVALUE => $id
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            
            
            // 전달된 id값이 실제 존재하는 신청 내역인지 확인
            if ($holiday = $this->getView($id)) {
                // 신청 내역 삭제
                $this->setDelete($id);
                
                redirect($this->redirectUrl());
            } else {
                Javascript::printAlert(get_string('holiday_nodata', $this->pluginname));
            }
        }
    }
    
    /**
     * 신청 내역 삭제
     *
     * @param number $id
     * @param boolean $isPurgeCache
     */
    function setDelete($id, $isPurgeCache=true)
    {
        global $DB;
    
        $DB->delete_records('format_ubsweeks_holiday', array('id'=>$id));
        
        if($isPurgeCache) {
            $this->purgeCache();
        }
    }
    
    
    function doCheckDelete()
    {
        global $DB,$CFG;
        
        $selectItems = Parameter::requestArray('holidays', array(), PARAM_INT);
        
        if(!empty($selectItems)) {
            foreach($selectItems as $si) {
                $this->setDelete($si, false);
            }
            
            $this->purgeCache();
        }
        
        redirect($this->redirectUrl());
    }
    
    
    function purgeCache() 
    {
        $cache = \cache::make('format_ubsweeks', 'holiday');
        $cache->purge();
    }
    
    
    
    function getHolidays($start, $end) 
    {
        // 전체 휴일 목록을 가져와서 전달된 날자가 휴일인지를 비교해봐야됨.
        $allHoliday = $this->getListAll();
        
        // y-m-d 형태로 기록될것임.
        $holidays = array();
        if(!empty($allHoliday)) {
            // 시작, 종료일 기준으로 해당 연도에 맞는 휴일일자를 만들어서 리턴해줌.
            $startDate = date('Y-m-d', $start);
            $endDate = date('Y-m-d', $end);
            
            $startYear = date('Y', $start);
            $endYear = date('Y', $end);
            
            foreach($allHoliday as $ah) {
                $checkDate = array();
                
                // 반복 연도인 경우
                if($ah->repeated) {
                    // 월-일만 추출
                    $monDay = date('m-d', $ah->days);
                    
                    // 시작연도에서 종료연도까지 반복하면서 반복일자 검사해야됨. (겨울학기인 경우 2017년 12월 ~ 2018년 1월 형태로 약 2개월 전달됨.)
                    if ($endYear > $startYear) {
                        for ($i = $endYear; $i >= $startYear; $i--) {
                            $checkDate[] = $i.'-'.$monDay;
                        }
                    } else {
                        // 시작연도로만 일자를 계산함.
                        // 시작연도가 종료연도보다 큰 경우는 파라메터 순서가 잘못된것임... 
                        // 에러는 나면 안되기 때문에 시작연도로 계산함.
                        $checkDate[] = $startYear.'-'.$monDay;
                    }
                } else {
                    $checkDate[] = date('Y-m-d', $ah->days);
                }
                
                
                if(!empty($checkDate)) {
                    foreach($checkDate as $cd) {
                        if($cd >= $startDate && $cd <= $endDate) {
                            // key=>value 형태로 담아줌.
                            $holidays[$cd] = $cd;
                        }
                    }
                }
            }
            
            return $holidays;
        }
        
        return false;
    }
    
    function redirectUrl()
    {
        global $CFG;
        
        return $CFG->wwwroot.'/local/ubion/index.php/site/holiday';
    }
}