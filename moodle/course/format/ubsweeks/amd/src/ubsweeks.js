define([ 'jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate', 'theme_coursemos/bootstrap-tabs' ], function ($) {

    var ubsweeks = {};

    ubsweeks.tabs = function (section) {
        $('.nav-course-format-tabs').bootstrapResponsiveTabs({
            minTabWidth : 60,
            maxTabWidth : 80,
            currentTab : section,
            showNav : true
        });
    };

    ubsweeks.courseFilter = function () {
        $("#select-course-filter").change(function () {
            selectModule = $(this).val();
            if (selectModule == '') {
                $(".course-content .total-sections .section .activity").show();
            } else {
                $(".course-content .total-sections .section .activity").each(function () {
                    if ($(this).hasClass(selectModule)) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        });
    };

    ubsweeks.holiday = function (url) {
        $(".format_ubsweeks_holiday .btn-insert").click(function () {
            $("#modal-holiday").modal('show');

            $(".form-modal-holiday").each(function () {
                this.reset();
            });

            $(".form-modal-holiday input[name='" + coursemostype + "']").val('insert');
            $("#modal-holiday .modal-title").text(M.util.get_string('holiday_add', 'format_ubsweeks'));
        });

        $(".format_ubsweeks_holiday .btn-delete").click(function () {
            if (confirm(M.util.get_string('holiday_delete_confirm', 'format_ubsweeks'))) {
                id = $(this).attr('data-id');

                // minify 오류 방지
                ajaxparam = {
                    id : id
                }
                ajaxparam[coursemostype] = 'delete';

                formSubmit(url, ajaxparam);
            }
            return false;
        });

        $(".format_ubsweeks_holiday .all_checked").click(function () {
            checked = $(this).is(':checked');

            $(".format_ubsweeks_holiday .form-holiday .form-holidays").prop('checked', checked);
        });

        $(".format_ubsweeks_holiday .btn-checked-delete").click(function () {
            selectCount = $(".format_ubsweeks_holiday .form-holiday .form-holidays:checked").length;

            if (selectCount > 0) {
                if (confirm(M.util.get_string('selected_item_delete', 'format_ubsweeks', selectCount))) {
                    $(".format_ubsweeks_holiday .form-holiday").submit();
                }
            } else {
                alert(M.util.get_string('selected_noitem', 'format_ubsweeks'));
            }

            return false;
        });

        $(".format_ubsweeks_holiday .btn-update").click(function () {
            id = $(this).attr('data-id');

            // minify 오류 방지
            ajaxparam = {
                id : id
            }
            ajaxparam[coursemostype] = 'view';

            $.ajax({
                data : ajaxparam,
                url : url,
                success : function (data) {
                    if (data.code == mesg.success) {
                        $(".form-modal-holiday input[name='id']").val(id);
                        $(".form-modal-holiday input[name='" + coursemostype + "']").val('update');
                        $(".form-modal-holiday input[name='day']").val(data.holiday.day_string);
                        $(".form-modal-holiday input[name='name']").val(data.holiday.name);

                        checked = (data.holiday.repeated == 1) ? true : false;
                        $(".form-modal-holiday input[name='repeat']").prop('checked', checked);
                        $(".form-modal-holiday input[name='visible'][value='" + data.holiday.visible + "']").prop('checked', true);

                        $("#modal-holiday").modal('show');
                    } else {
                        alert(data.msg);
                    }
                }
            });

            return false;
        });

        $(".form-modal-holiday").validate({
            submitHandler : function (form) {
                $.ajax({
                    data : $(form).serialize(),
                    url : url,
                    success : function (data) {
                        if (data.code == mesg.success) {
                            location.reload();
                        } else {
                            alert(data.msg);
                        }
                    }
                });

                return false;
            }
        });
    }

    return ubsweeks;

});