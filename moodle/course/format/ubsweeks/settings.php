<?php
defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {
    require_once $CFG->dirroot . '/course/format/ubsweeks/lib.php';

    $pluginname = 'format_ubsweeks';

    // 강좌보기타입

    $options = [
        format_ubsweeks::VIEWTYPE_LIST => get_string('courseviewtype_list', $pluginname),
        format_ubsweeks::VIEWTYPE_TAB => get_string('courseviewtype_tab', $pluginname)
    ];

    $name = $pluginname . '/courseviewtype';
    $title = get_string('setting_courseviewtype', $pluginname);
    $description = get_string('setting_courseviewtype_help', $pluginname);
    $default = format_ubsweeks::VIEWTYPE_LIST;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $settings->add($setting);

    // 강좌 출력 방법
    $options = [
        format_ubsweeks::UI_LIST => get_string('ui_list', $pluginname),
        format_ubsweeks::UI_METRO => get_string('ui_metro', $pluginname)
    ];

    $name = $pluginname . '/ui';
    $title = get_string('setting_ui', $pluginname);
    $description = null;
    $default = format_ubsweeks::UI_LIST;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $settings->add($setting);
}