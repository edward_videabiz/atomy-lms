<?php

$definitions = array(
	// 휴일 캐시
	'holiday' => array(
		'mode' => cache_store::MODE_APPLICATION,
		'simplekeys' => true,
		'staticacceleration' => true,
		'ttl' => 86400,   // 하루동인 유지
	),
);
