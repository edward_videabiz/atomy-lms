<?php
function xmldb_format_ubsweeks_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager(); 
    
    if ($oldversion < 2017031600) {
    
        // Define table format_ubsweeks_holiday to be created.
        $table = new xmldb_table('format_ubsweeks_holiday');
        
        // Adding fields to table format_ubsweeks_holiday.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('days', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        $table->add_field('repeated', XMLDB_TYPE_INTEGER, '1', null, null, null, '0');
        $table->add_field('visible', XMLDB_TYPE_INTEGER, '1', null, null, null, '1');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, null);
        
        // Adding keys to table format_ubsweeks_holiday.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        
        // Adding indexes to table format_ubsweeks_holiday.
        $table->add_index('visible', XMLDB_INDEX_NOTUNIQUE, array('visible'));
        $table->add_index('days', XMLDB_INDEX_NOTUNIQUE, array('days'));
        
        // Conditionally launch create table for format_ubsweeks_holiday.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        // Ubsweeks savepoint reached.
        upgrade_plugin_savepoint(true, 2017031600, 'format', 'ubsweeks');
    }
    
}