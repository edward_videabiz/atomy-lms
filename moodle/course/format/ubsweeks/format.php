<?php
/**
 * @category 	format.php
 * @author 		Sung Hoon, Cho (akddd@naddle.net)
 * @since		2016. 11. 20.
 * @version		0.1
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/completionlib.php');

/*
$success_code = null;
$error_message = '';

// 강좌 기본 셋팅
list($success_code, $error_message) = local_ubion_course_setting($course);

// 강좌를 처음 셋팅하는 경우 redirect
if($success_code == 200) {
	redirect($CFG->wwwroot.'/course/view.php?id='.$course->id, get_string('init_course', 'local_ubion'), 0);
} else if($success_code == 300) {
	// 강좌 서브 메뉴 감추기
	$PAGE->requires->js_function_call('M.course.format.nosubmenu');
	
	$error_message = '<div class="error_message">'.$error_message.'</div>';
	$error_message .= '<div><a href="'.$CFG->wwwroot.'" class="btn btn-default">'.get_string('home').'</a></div>';

	echo $OUTPUT->container($error_message, 'alert alert-danger');
	echo '</div>';	// 테마에 따라서 이 테그가 불필요할수도 있습니다.
	echo $OUTPUT->footer();
	exit;
}
*/

// Horrible backwards compatible parameter aliasing..
if ($week = optional_param('week', 0, PARAM_INT)) {
    $url = $PAGE->url;
    $url->param('section', $week);
    debugging('Outdated week param passed to course/view.php', DEBUG_DEVELOPER);
    redirect($url);
}
// End backwards-compatible aliasing..

// make sure all sections are created
$course = course_get_format($course)->get_course();
course_create_sections_if_missing($course, range(0, $course->numsections));

$renderer = $PAGE->get_renderer('format_ubsweeks');

if (!empty($displaysection)) {
    $renderer->print_single_section_page($course, null, null, null, null, $displaysection);
} else {
    $renderer->print_multiple_section_page($course, null, null, null, null);
}

$PAGE->requires->js_call_amd('format_ubsweeks/ubsweeks', 'courseFilter');
$PAGE->requires->js('/course/format/ubsweeks/format.js');

// 탭형 보기인 경우
if($course->courseviewtype == 1) {
    $PAGE->requires->js_call_amd('format_ubsweeks/ubsweeks', 'tabs', array('section'=>$section));
}