<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the topics course format.
 *
 * @package format_ubstopics
 * @copyright 2016 akddd
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since Moodle 2.3
 */
defined('MOODLE_INTERNAL') || die();
require_once ($CFG->dirroot . '/course/format/renderer.php');
require_once ($CFG->dirroot . '/course/format/ubstopics/lib.php');

/**
 * Basic renderer for ubstopics format.
 *
 * @copyright 2016 akddd
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_ubstopics_renderer extends format_section_renderer_base
{

    /**
     * Constructor method, calls the parent constructor
     *
     * @param moodle_page $page
     * @param string $target
     *            one of rendering target constants
     */
    public function __construct(moodle_page $page, $target)
    {
        parent::__construct($page, $target);

        // Since format_topics_renderer::section_edit_controls() only displays the 'Set current section' control when editing mode is on
        // we need to be sure that the link 'Turn editing mode on' is available for a user who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');
    }

    /**
     * Generate the starting container html for a list of sections
     *
     * @return string HTML to output.
     */
    protected function start_section_list()
    {
        return html_writer::start_tag('ul', array(
            'class' => 'topics ubstopics'
        ));
    }

    /**
     * Generate the closing container html for a list of sections
     *
     * @return string HTML to output.
     */
    protected function end_section_list()
    {
        return html_writer::end_tag('ul');
    }

    /**
     * Generate the title for this section page
     *
     * @return string the page title
     */
    protected function page_title()
    {
        return get_string('topicoutline');
    }

    /**
     * Generate the section title, wraps it in a link to the section page if page is to be displayed on a separate page
     *
     * @param stdClass $section
     *            The course_section entry from DB
     * @param stdClass $course
     *            The course entry from DB
     * @return string HTML to output.
     */
    public function section_title($section, $course)
    {
        return $this->render(course_get_format($course)->inplace_editable_render_section_name($section));
    }

    /**
     * Generate the section title to be displayed on the section page, without a link
     *
     * @param stdClass $section
     *            The course_section entry from DB
     * @param stdClass $course
     *            The course entry from DB
     * @return string HTML to output.
     */
    public function section_title_without_link($section, $course)
    {
        return $this->render(course_get_format($course)->inplace_editable_render_section_name($section, false));
    }

    /**
     * Generate next/previous section links for naviation
     *
     * @param stdClass $course
     *            The course entry from DB
     * @param array $sections
     *            The course_sections entries from the DB
     * @param int $sectionno
     *            The section number in the coruse which is being dsiplayed
     * @return array associative array with previous and next section link
     */
    protected function get_nav_links($course, $sections, $sectionno)
    {
        // FIXME: This is really evil and should by using the navigation API.
        $course = course_get_format($course)->get_course();
        $canviewhidden = has_capability('moodle/course:viewhiddensections', context_course::instance($course->id)) or ! $course->hiddensections;

        $links = array(
            'previous' => '',
            'next' => ''
        );
        $back = $sectionno - 1;
        while ($back > 0 and empty($links['previous'])) {
            if ($canviewhidden || $sections[$back]->uservisible) {
                $addClass = '';
                if (! $sections[$back]->visible) {
                    $addClass = 'dimmed_text';
                }

                $previous_name = get_section_name($course, $sections[$back]);
                $previouslink = '<a href="' . course_get_url($course, $back) . '" class="btn btn-default btn-section-nav section-nav-tooltip ' . $addClass . '" title="' . $previous_name . '">';
                $previouslink .= '<img src="' . $this->output->image_url('section_nav_larrow', 'format_ubstopics') . '" alt="' . $previous_name . '" />';
                $previouslink .= '</a>';
                $links['previous'] = $previouslink;
            }
            $back --;
        }

        $forward = $sectionno + 1;
        $numsections = course_get_format($course)->get_last_section_number();
        while ($forward <= $numsections and empty($links['next'])) {
            if ($canviewhidden || $sections[$forward]->uservisible) {
                $addClass = '';
                if (! $sections[$forward]->visible) {
                    $addClass = 'dimmed_text';
                }

                $next_name = get_section_name($course, $sections[$forward]);
                $nextlink = '<a href="' . course_get_url($course, $forward) . '" class="btn btn-default btn-section-nav section-nav-tooltip ' . $addClass . '" title="' . $next_name . '">';
                $nextlink .= '<img src="' . $this->output->image_url('section_nav_rarrow', 'format_ubstopics') . '" alt="' . $next_name . '" />';
                $nextlink .= '</a>';
                $links['next'] = $nextlink;
            }
            $forward ++;
        }

        return $links;
    }

    /**
     * Output the html for a single section page .
     * ubsweeks의 print_single_section_page 함수와 동일합니다. (그대로 복사 해서 사용)
     *
     * @param stdClass $course
     *            The course entry from DB
     * @param array $sections
     *            (argument not used)
     * @param array $mods
     *            (argument not used)
     * @param array $modnames
     *            (argument not used)
     * @param array $modnamesused
     *            (argument not used)
     * @param int $displaysection
     *            The section number in the course which is being displayed
     * @see format_section_renderer_base::print_single_section_page()
     */
    public function print_single_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection)
    {
        global $PAGE, $CFG;

        $CCourse = \local_ubion\course\Course::getInstance();

        if ($course->courseviewtype == $CCourse::VIEWTYPE_TAB) {
            $this->print_tab_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection);
        } else {
            $modinfo = get_fast_modinfo($course);
            $course_format = course_get_format($course);
            $course = $course_format->get_course();

            // Can we view the section in question?
            if (! ($sectioninfo = $modinfo->get_section_info($displaysection))) {
                // This section doesn't exist
                print_error('unknowncoursesection', 'error', null, $course->fullname);
                return;
            }

            if (! $sectioninfo->uservisible) {
                if (! $course->hiddensections) {
                    echo $this->start_section_list();
                    echo $this->section_hidden($displaysection);
                    echo $this->end_section_list();
                }
                // Can't view this section.
                return;
            }

            // Copy activity clipboard..
            echo $this->course_activity_clipboard($course, $displaysection);
            /*
             * 한주차에 한개의 영역 표시인 경우에는 0주차는 표시되지 않도록 함.
             * $thissection = $modinfo->get_section_info(0);
             * if ($thissection->summary or !empty($modinfo->sections[0]) or $PAGE->user_is_editing()) {
             * echo $this->start_section_list();
             * echo $this->section_header($thissection, $course, true, $displaysection);
             * echo $this->courserenderer->course_section_cm_list($course, $thissection, $displaysection);
             * echo $this->courserenderer->course_section_add_cm_control($course, 0, $displaysection);
             * echo $this->section_footer();
             * echo $this->end_section_list();
             * }
             */

            // The requested section page.
            $thissection = $modinfo->get_section_info($displaysection);

            $add_course_box_class = '';
            if ($course_format->is_section_current($thissection)) {
                $add_course_box_class = 'course_box_current';
            }

            // Start single-section div
            echo html_writer::start_tag('div', array(
                'class' => 'single-section '
            ));
            echo '<div class="course_box ' . $add_course_box_class . '">';

            // Title with section navigation links.
            $sectionnavlinks = $this->get_nav_links($course, $modinfo->get_section_info_all(), $displaysection);

            // Title attributes
            $titleattr = 'section-title-one';
            if (! $thissection->visible) {
                $titleattr .= ' dimmed_text';
            }

            $sectionname = html_writer::tag('span', $this->section_title_without_link($thissection, $course));
            echo $this->output->heading($sectionname, 4, $titleattr);

            // Now the list of sections..
            echo $this->start_section_list();

            echo $this->section_header($thissection, $course, true, $displaysection);
            // Show completion help icon.
            $completioninfo = new completion_info($course);
            echo $completioninfo->display_help_icon();

            echo $this->courserenderer->course_section_cm_list($course, $thissection, $displaysection);
            echo $this->courserenderer->course_section_add_cm_control($course, $displaysection, $displaysection);
            echo $this->section_footer();
            echo $this->end_section_list();

            echo '</div>'; // course_box

            // Display section bottom navigation.
            $sectionbottomnav = '';
            $sectionbottomnav .= html_writer::start_tag('div', array(
                'class' => 'section-navigation mdl-bottom'
            ));
            $sectionbottomnav .= html_writer::tag('span', $sectionnavlinks['previous'], array(
                'class' => 'mdl-left'
            ));
            $sectionbottomnav .= html_writer::tag('span', $sectionnavlinks['next'], array(
                'class' => 'mdl-right'
            ));
            $sectionbottomnav .= html_writer::tag('div', $this->section_nav_selection($course, $sections, $displaysection), array(
                'class' => 'mdl-align'
            ));
            $sectionbottomnav .= html_writer::end_tag('div');
            echo $sectionbottomnav;

            // Close single-section div.
            echo html_writer::end_tag('div');
        }
    }

    /**
     * Output the html for a multiple section page
     * ubsweeks의 print_single_section_page 함수와 동일합니다.
     * (그대로 복사 해서 사용)
     *
     * @param stdClass $course
     *            The course entry from DB
     * @param array $sections
     *            (argument not used)
     * @param array $mods
     *            (argument not used)
     * @param array $modnames
     *            (argument not used)
     * @param array $modnamesused
     *            (argument not used)
     */
    public function print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused)
    {
        global $PAGE, $CFG;

        $modinfo = get_fast_modinfo($course);
        $course_format = course_get_format($course);
        $course = $course_format->get_course();

        $CCourse = \local_ubion\course\Course::getInstance();

        $context = context_course::instance($course->id);
        $isEditing = $PAGE->user_is_editing() and has_capability('moodle/course:update', $context);

        // 탭형 보기인 경우에는 다른 함수를 호출해야됨.
        if ($course->courseviewtype == $CCourse::VIEWTYPE_TAB && ! $isEditing) {
            $this->print_tab_section_page($course, $sections, $mods, $modnames, $modnamesused);
        } else {
            // Title with completion help icon.
            $completioninfo = new completion_info($course);
            echo $completioninfo->display_help_icon();

            // Copy activity clipboard..
            echo $this->course_activity_clipboard($course, 0);

            // ui class
            $uiClass = ($this->isUiMetro($course)) ? 'ui-metro' : '';

            // 주차 관련 변수
            $allsections = $modinfo->get_section_info_all();
            $numsections = course_get_format($course)->get_last_section_number();

            // 강좌 요약 정보
            $current_section = null;
            foreach ($allsections as $section => $thissection) {
                if ($course_format->is_section_current($thissection)) {
                    $current_section = $thissection;
                    break;
                }
            }

            // 기본 학습활동
            echo '<div class="course_box course_box0 ' . $uiClass . '">';
            echo '<h2 class="main">' . $course_format->get_section_name(0) . '</h2>';
            $thissection = $modinfo->get_section_info(0);
            if ($thissection->summary or ! empty($modinfo->sections[0]) or $PAGE->user_is_editing()) {
                echo $this->start_section_list();
                echo $this->section_header($thissection, $course, false, 0);
                echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                echo $this->courserenderer->course_section_add_cm_control($course, 0, 0);
                echo $this->section_footer();
                echo $this->end_section_list();
            }
            echo '</div>';

            // 현재 주차가 존재하면 출력해줘야됨.
            // 단, 현재주차가 강좌 설정보다 큰 값인 경우에는 표시가 되면 안됨.
            // 2014-01-16 by akddd
            // 편집모드가 켜져있는 상태라면 현재 주차가 보이면 안됨 (section 이동시 문제가 발생됨.)
            if ($current_section && $current_section->section <= $numsections && ! $PAGE->user_is_editing()) {
                $section = $current_section->section;
                $thissection = $current_section;

                echo '<div class="course_box course_box_current">';
                echo '<h2 class="main">' . get_string('current_section', 'format_' . $course->format) . '</h2>';

                echo $this->start_section_list();

                // Show the section if the user is permitted to access it, OR if it's not available
                // but there is some available info text which explains the reason & should display.
                $showsection = $thissection->uservisible || ($thissection->visible && ! $thissection->available && ! empty($thissection->availableinfo));

                if (! $showsection) {
                    // Hidden section message is overridden by 'unavailable' control
                    // (showavailability option).

                    // 비공개영역을 완전히 볼수 없으로 설정하고, 현재주차가 숨김으로 표시가되면
                    // 프로그램 오류로 인해서 현재주차가 표시되지 않는걸로 오해할수 있기 때문에..
                    // 비공개영역 값과 무관하게 현재주차가 숨김으로 설정이 되어 있으면 사용불가능 메시지를 출력하게 변경함.
                    // 2013-11-20 by akddd
                    // if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section);
                    // }
                } else {
                    if (! $PAGE->user_is_editing() && $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                        // Display section summary only.
                        echo $this->section_summary($thissection, $course, null);
                    } else {
                        echo $this->section_header($thissection, $course, false, 0);
                        if ($thissection->uservisible) {
                            echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                            echo $this->courserenderer->course_section_add_cm_control($course, $section, 0);
                        }
                        echo $this->section_footer();
                    }
                }

                echo $this->end_section_list();

                // 불필요한 변수 삭제
                unset($current_section);
                echo '</div>';
            }

            // 전체 강좌
            echo '<div id="course-all-sections" class="total-sections ' . $uiClass . '">';
            echo '<div class="course_box">';
            if ($used_modules = $modinfo->get_used_module_names()) {
                echo '<div class="course-filter">';
                echo '<select id="select-course-filter" class="form-control form-control-auto">';
                echo '<option value="">' . get_string('all2', 'local_ubion') . '</option>';
                foreach ($used_modules as $um_key => $um_name) {
                    echo '<option value="' . $um_key . '">' . $um_name . '</option>';
                }
                echo '</select>';
                echo '</div>';
            }
            echo '<h2 class="main">' . get_string('course_all', 'format_' . $course->format) . '<span class="icons"></span></h2>';

            // Now the list of sections..
            echo $this->start_section_list();
            foreach ($allsections as $section => $thissection) {
                // 0번째 주차는 상단에서 출력을 했음.
                if ($section == 0) {
                    continue;
                }
                if ($section > $numsections) {
                    // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                    continue;
                }
                // Show the section if the user is permitted to access it, OR if it's not available
                // but there is some available info text which explains the reason & should display.
                $showsection = $thissection->uservisible || ($thissection->visible && ! $thissection->available && ! empty($thissection->availableinfo));
                if (! $showsection) {
                    // Hidden section message is overridden by 'unavailable' control
                    // (showavailability option).
                    if (! $course->hiddensections && $thissection->available) {
                        echo $this->section_hidden($section);
                    }

                    continue;
                }

                if (! $PAGE->user_is_editing() && $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                    // Display section summary only.
                    echo $this->section_summary($thissection, $course, null);
                } else {
                    echo $this->section_header($thissection, $course, false, 0);
                    if ($thissection->uservisible) {
                        echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                        echo $this->courserenderer->course_section_add_cm_control($course, $section, 0);
                    }
                    echo $this->section_footer();
                }
            }

            if ($isEditing) {
                // Print stealth sections if present.
                foreach ($allsections as $section => $thissection) {
                    if ($section <= $numsections or empty($modinfo->sections[$section])) {
                        // this is not stealth section or it is empty
                        continue;
                    }
                    echo $this->stealth_section_header($section);
                    echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                    echo $this->stealth_section_footer();
                }

                echo $this->end_section_list();

                echo $this->change_number_sections($course, 0);
            } else {
                echo $this->end_section_list();
            }

            echo '</div>'; // course_box
            echo '</div>'; // total_sections
        }
    }

    /**
     * 메트로 형태 출력인지 확인
     *
     * @param \stdClass $course
     * @return boolean
     */
    public function isUiMetro($course)
    {
        $ui = $course->ui ?? \format_ubstopics::UI_LIST;

        $return = ($ui == \format_ubstopics::UI_METRO) ? true : false;

        return $return;
    }

    public function print_tab_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection = null)
    {
        global $PAGE, $CFG;

        $modinfo = get_fast_modinfo($course);
        $course_format = course_get_format($course);
        $course = $course_format->get_course();

        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        echo $completioninfo->display_help_icon();

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);

        // 기본 학습활동
        echo '<div class="course_box course_box0">';
        echo '<h2 class="main">' . $course_format->get_section_name(0) . '</h2>';
        $thissection = $modinfo->get_section_info(0);
        if ($thissection->summary or ! empty($modinfo->sections[0]) or $PAGE->user_is_editing()) {
            echo $this->start_section_list();
            echo $this->section_header($thissection, $course, false, 0);
            echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
            echo $this->courserenderer->course_section_add_cm_control($course, 0, 0);
            echo $this->section_footer();
            echo $this->end_section_list();
        }
        echo '</div>';

        // 현재 주차가 존재하는지 확인
        $current_section_num = null;
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($course_format->is_section_current($thissection)) {
                $current_section_num = $thissection->section;
                break;
            }
        }

        // 현재 주차보다 displaysection이 더 우선시 됨
        if (! empty($displaysection)) {
            $current_section_num = $displaysection;
        }

        // 탭 출력
        echo '<div class="responsive-tabs-container">';
        echo '<ul class="nav nav-tabs nav-course-format-tabs" role="tablist">';
        $i = 1;
        $allsections = $modinfo->get_section_info_all();
        $numsections = course_get_format($course)->get_last_section_number();
        foreach ($allsections as $section => $thissection) {
            // 0번째 주차는 상단에서 출력을 했음.
            if ($section == 0) {
                continue;
            }

            if ($section > $numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }
            $panelName = 'course-tabpanel-' . $section;
            $tabName = $course_format->get_section_name($thissection, true);
            $tabFullName = $course_format->get_section_name($section);

            $activeClass = '';
            if (empty($current_section_num)) {
                if ($i == 1) {
                    $activeClass = 'active';
                }
            } else {
                if ($section == $current_section_num) {
                    $activeClass = 'active';
                }
            }

            echo '<li role="presentation" class="' . $activeClass . '" data-sectionnum="' . $section . '">';
            echo '<a href="#' . $panelName . '" aria-controls="' . $panelName . '" role="tab" data-toggle="tab" title="' . $tabFullName . '" class="nav-link">' . $tabName . '</a>';
            echo '</li>';
            $i ++;
        }
        echo '</ul>';
        echo '</div>';

        // 본문 출력
        echo '<div class="tab-content course_box">';
        $i = 1;
        foreach ($allsections as $section => $thissection) {
            // 0번째 주차는 상단에서 출력을 했음.
            if ($section == 0) {
                continue;
            }

            if ($section > $numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }

            $activeClass = '';
            if (empty($current_section_num)) {
                if ($i == 1) {
                    $activeClass = 'active';
                }
            } else {
                if ($section == $current_section_num) {
                    $activeClass = 'active';
                }
            }

            $panelName = 'course-tabpanel-' . $section;
            echo '<ul role="tabpanel" class="topics ubstopics tab-pane ' . $activeClass . '" id="' . $panelName . '">';

            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible || ($thissection->visible && ! $thissection->available && ! empty($thissection->availableinfo));

            if (! $showsection) {
                // Hidden section message is overridden by 'unavailable' control
                // (showavailability option).

                // 비공개영역을 완전히 볼수 없으로 설정하고, 현재주차가 숨김으로 표시가되면
                // 프로그램 오류로 인해서 현재주차가 표시되지 않는걸로 오해할수 있기 때문에..
                // 비공개영역 값과 무관하게 현재주차가 숨김으로 설정이 되어 있으면 사용불가능 메시지를 출력하게 변경함.
                // 2013-11-20 by akddd
                // if (!$course->hiddensections && $thissection->available) {
                echo $this->section_hidden($section);
                // }
            } else {
                // 탭형 보기에는 single, multiple구분없이 무조건 학습자원/활동이 보여줘야됨.
                // if (!$PAGE->user_is_editing() && $course->coursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                // Display section summary only.
                // echo $this->section_summary($thissection, $course, null);
                // } else {
                echo $this->section_header($thissection, $course, false, 0);
                if ($thissection->uservisible) {
                    echo $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                    echo $this->courserenderer->course_section_add_cm_control($course, $section, 0);
                }
                echo $this->section_footer();
                // }
            }

            echo '</ul>';
            $i ++;
        }
        echo '</div>';
    }

    /**
     * Generate the edit control action menu
     *
     * @param array $controls
     *            The edit control items from section_edit_control_items
     * @param stdClass $course
     *            The course entry from DB
     * @param stdClass $section
     *            The course_section entry from DB
     * @return string HTML to output.
     */
    protected function section_edit_control_menu($controls, $course, $section)
    {
        $o = "";
        if (! empty($controls)) {
            $menu = new action_menu();
            // $menu->set_menu_trigger(get_string('edit'));
            $menu->set_menu_trigger($this->output->pix_icon('t/edit', get_string('edit')));
            $menu->attributes['class'] .= ' section-actions';
            foreach ($controls as $value) {
                $url = empty($value['url']) ? '' : $value['url'];
                $icon = empty($value['icon']) ? '' : $value['icon'];
                $name = empty($value['name']) ? '' : $value['name'];
                $attr = empty($value['attr']) ? array() : $value['attr'];
                $class = empty($item['pixattr']['class']) ? '' : $item['pixattr']['class'];
                $alt = empty($item['pixattr']['alt']) ? '' : $item['pixattr']['alt'];
                $al = new action_menu_link_secondary(new moodle_url($url), new pix_icon($icon, $name, null, array(
                    'class' => "smallicon " . $class,
                    'alt' => $alt
                )), $name, $attr);
                $menu->add($al);
            }

            $o .= html_writer::div($this->render($menu), 'section_action_menu', array(
                'data-sectionid' => $section->id
            ));
        }

        return $o;
    }

    /**
     * Generate the edit control items of a section
     *
     * @param stdClass $course
     *            The course entry from DB
     * @param stdClass $section
     *            The course_section entry from DB
     * @param bool $onsectionpage
     *            true if being printed on a section page
     * @return array of edit control items
     */
    protected function section_edit_control_items($course, $section, $onsectionpage = false)
    {
        global $PAGE;

        if (! $PAGE->user_is_editing()) {
            return array();
        }

        $coursecontext = context_course::instance($course->id);

        if ($onsectionpage) {
            $url = course_get_url($course, $section->section);
        } else {
            $url = course_get_url($course);
        }
        $url->param('sesskey', sesskey());

        $controls = array();
        if ($section->section && has_capability('moodle/course:setcurrentsection', $coursecontext)) {
            if ($course->marker == $section->section) { // Show the "light globe" on/off.
                $url->param('marker', 0);
                $markedthistopic = get_string('markedthistopic');
                $highlightoff = get_string('highlightoff');
                $controls['highlight'] = array(
                    'url' => $url,
                    "icon" => 'i/marked',
                    'name' => $highlightoff,
                    'pixattr' => array(
                        'class' => '',
                        'alt' => $markedthistopic
                    ),
                    'attr' => array(
                        'class' => 'editing_highlight',
                        'title' => $markedthistopic,
                        'data-action' => 'removemarker'
                    )
                );
            } else {
                $url->param('marker', $section->section);
                $markthistopic = get_string('markthistopic');
                $highlight = get_string('highlight');
                $controls['highlight'] = array(
                    'url' => $url,
                    "icon" => 'i/marker',
                    'name' => $highlight,
                    'pixattr' => array(
                        'class' => '',
                        'alt' => $markthistopic
                    ),
                    'attr' => array(
                        'class' => 'editing_highlight',
                        'title' => $markthistopic,
                        'data-action' => 'setmarker'
                    )
                );
            }
        }

        $parentcontrols = parent::section_edit_control_items($course, $section, $onsectionpage);

        // If the edit key exists, we are going to insert our controls after it.
        if (array_key_exists("edit", $parentcontrols)) {
            $merged = array();
            // We can't use splice because we are using associative arrays.
            // Step through the array and merge the arrays.
            foreach ($parentcontrols as $key => $action) {
                $merged[$key] = $action;
                if ($key == "edit") {
                    // If we have come to the edit key, merge these controls here.
                    $merged = array_merge($merged, $controls);
                }
            }

            return $merged;
        } else {
            return array_merge($controls, $parentcontrols);
        }
    }

    /**
     * Generate html for a section summary text
     *
     * @param stdClass $section
     *            The course_section entry from DB
     * @return string HTML to output.
     */
    protected function format_summary_text($section)
    {
        $context = context_course::instance($section->course);
        $summarytext = file_rewrite_pluginfile_urls($section->summary, 'pluginfile.php', $context->id, 'course', 'section', $section->id);

        $options = new stdClass();
        // true값으로 되어 있던걸 false로만 변경해줌
        $options->noclean = false;
        $options->overflowdiv = true;
        return format_text($summarytext, $section->summaryformat, $options);
    }
}
