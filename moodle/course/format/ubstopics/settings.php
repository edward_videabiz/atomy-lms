<?php
defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {
    require_once $CFG->dirroot . '/course/format/ubstopics/lib.php';

    $pluginname = 'format_ubstopics';

    // 강좌보기타입

    $options = [
        format_ubstopics::VIEWTYPE_LIST => get_string('courseviewtype_list', $pluginname),
        format_ubstopics::VIEWTYPE_TAB => get_string('courseviewtype_tab', $pluginname)
    ];

    $name = $pluginname . '/courseviewtype';
    $title = get_string('setting_courseviewtype', $pluginname);
    $description = get_string('setting_courseviewtype_help', $pluginname);
    $default = format_ubstopics::VIEWTYPE_LIST;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $settings->add($setting);

    // 강좌 출력 방법
    $options = [
        format_ubstopics::UI_LIST => get_string('ui_list', $pluginname),
        format_ubstopics::UI_METRO => get_string('ui_metro', $pluginname)
    ];

    $name = $pluginname . '/ui';
    $title = get_string('setting_ui', $pluginname);
    $description = null;
    $default = format_ubstopics::UI_LIST;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $settings->add($setting);
}