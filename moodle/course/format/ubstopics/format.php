<?php
/**
 * @category 	format.php
 * @author 		Sung Hoon, Cho (akddd@naddle.net)
 * @since		2016. 11. 20.
 * @version		0.1
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/completionlib.php');

$isProfessor = \local_ubion\course\Course::getInstance()->isProfessor($course->id);

if (empty($isProfessor)) {
    
    $redirect = $CFG->wwwroot.'/local/vn/index.php?id='.$course->id;
    $PAGE->requires->js_function_call('document.location.replace', array($redirect), true, 1);
    
    echo $OUTPUT->notification(get_string('redirect_main', 'local_vn'), 'alert alert-danger');
    echo '<div class="continuebutton">(<a href="'. $redirect.'">'. get_string('continue') .'</a>)</div>';
    
    echo '</div>';	// 테마에 따라서 이 테그가 불필요할수도 있습니다.
    echo $OUTPUT->footer();
    exit;
}

/*
$success_code = null;
$error_message = '';

// 강좌 기본 셋팅
list($success_code, $error_message) = local_ubion_course_setting($course);

// 강좌를 처음 셋팅하는 경우 redirect
if($success_code == 200) {
	redirect($CFG->wwwroot.'/course/view.php?id='.$course->id, get_string('init_course', 'local_ubion'), 0);
} else if($success_code == 300) {
	// 강좌 서브 메뉴 감추기
	$PAGE->requires->js_function_call('M.course.format.nosubmenu');

	$error_message = '<div class="error_message">'.$error_message.'</div>';
	$error_message .= '<div><a href="'.$CFG->wwwroot.'" class="btn btn-default">'.get_string('home').'</a></div>';

	echo $OUTPUT->container($error_message, 'alert alert-danger');
	echo '</div>';	// 테마에 따라서 이 테그가 불필요할수도 있습니다.
	echo $OUTPUT->footer();
	exit;
}
*/

// Horrible backwards compatible parameter aliasing..
if ($topic = optional_param('topic', 0, PARAM_INT)) {
    $url = $PAGE->url;
    $url->param('section', $topic);
    debugging('Outdated topic param passed to course/view.php', DEBUG_DEVELOPER);
    redirect($url);
}
// End backwards-compatible aliasing..

$context = context_course::instance($course->id);
// Retrieve course format option fields and add them to the $course object.
$courseFormat = course_get_format($course);
$course = $courseFormat->get_course();

if (($marker >=0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
    $course->marker = $marker;
    course_set_marker($course->id, $marker);
}

// Make sure section 0 is created.
course_create_sections_if_missing($course, 0);

if ($courseFormat->uses_sections()) {
    $modinfo = get_fast_modinfo($course);
    $allsections = $modinfo->get_section_info_all();
    $allSectionCount = count($allsections);
    $numsections = $courseFormat->get_last_section_number();
    
    // 사전에 등록된 주차가 없다면 추가해줘야됨.
    // 무들 3.2부터는 week/topic에 numsections항목이 사라짐
    if ($numsections > $allSectionCount) {
        for ($i=1; $i<=$numsections; $i++) {
            course_create_sections_if_missing($course, $i);
        }
    }
}

$renderer = $PAGE->get_renderer('format_ubstopics');

if (!empty($displaysection)) {
    $renderer->print_single_section_page($course, null, null, null, null, $displaysection);
} else {
    $renderer->print_multiple_section_page($course, null, null, null, null);
}

// Include course format js module
$PAGE->requires->js_call_amd('format_ubstopics/ubstopics', 'courseFilter');
$PAGE->requires->js('/course/format/ubstopics/format.js');

// 탭형 보기인 경우
if($course->courseviewtype == format_ubstopics::VIEWTYPE_TAB) {
    $PAGE->requires->js_call_amd('format_ubstopics/ubstopics', 'tabs', array('section'=>$section));
}

// metro 형태 보기
if($course->ui == format_ubstopics::UI_METRO) {
    $PAGE->requires->js_call_amd('format_ubstopics/ubstopics', 'metro');
}