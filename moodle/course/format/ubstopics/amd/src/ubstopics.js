define([ 'jquery', 'format_ubstopics/clamp', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate', 'theme_coursemos/bootstrap-tabs' ], function ($, clamp) {
    var ubstopics = {};

    ubstopics.tabs = function (section) {
        $('.nav-course-format-tabs').bootstrapResponsiveTabs({
            minTabWidth : 60,
            maxTabWidth : 80,
            currentTab : section,
            showNav : true
        });
    };

    ubstopics.courseFilter = function () {
        $("#select-course-filter").change(function () {
            var selectModule = $(this).val();

            if (selectModule == '') {
                $(".course-content .total-sections .section .activity").show();
            } else {
                $(".course-content .total-sections .section .activity").each(function () {
                    if ($(this).hasClass(selectModule)) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        });
    };
    
    ubstopics.metro = function () {
    	var lineClamp = 3;
    	if (Y.UA.ie > 0) {
    		lineClamp = 4;
    	}
    	$(".format-ubstopics .course-content .ui-metro li.section li.activity .instancename").each(function(index, element) {
    	    $clamp(element, { clamp: lineClamp });

    	});
    	//$clamp($(".format-ubstopics .course-content .ui-metro li.section li.activity .instancename"), {clamp: '60px'});
    };

    return ubstopics;
});