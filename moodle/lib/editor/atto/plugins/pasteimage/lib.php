<?php
defined('MOODLE_INTERNAL') || die();

/**
 * Initialise this plugin
 * @param string $elementid
 */
function atto_pasteimage_strings_for_js() {
    global $PAGE;

    $strings = array(
        'uploading',
    );

    $PAGE->requires->strings_for_js($strings, 'atto_pasteimage');
}