<?php
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2016012500;        // The current plugin version (Date: YYYYMMDDXX).
$plugin->requires  = 2014051200;        // Requires this Moodle version.
$plugin->release   = '1.0';
$plugin->maturity  = MATURITY_STABLE;
$plugin->component = 'atto_pasteimage';  // Full name of the plugin (used for diagnostics).
