var COMPONENTNAME = 'atto_pasteimage',

    IMAGE_TEMPLATE = '' +
            '<img src="{{url}}" ' +
                '{{#if id}}id="{{id}}" {{/if}}' +
                ' class="img-responsive" ' +
                ' alt="{{text}}">',

    LOADING_IMAGE_TEMPLATE = '' +
            '<img src="{{url}}" ' +
                '{{#if alt}}alt="{{alt}}" {{/if}}' +
                ' style="vertical-align:text-bottom;margin: 0 .5em;" class="img-responsive" ' +
                '{{#if presentation}}role="presentation" {{/if}}' +
                '{{#if id}}id="{{id}}" {{/if}}' +
                '/>';

Y.namespace('M.atto_pasteimage').Button = Y.Base.create('button', Y.M.editor_atto.EditorPlugin, [], {
    /**
     * Add event listeners.
     *
     * @method initializer
     */
    initializer: function() {
        this.editor.on('paste', this._handlePasteImage, this);
    },

    _handlePasteImage: function(e) {
        var self = this,
            host = this.get('host'),
            template = Y.Handlebars.compile(IMAGE_TEMPLATE),
            loadingtemplate = Y.Handlebars.compile(LOADING_IMAGE_TEMPLATE);

        host.saveSelection();

        var isWindowClipboard = false;
        var items = null;
        if (e._event.clipboardData) {
            items = e._event.clipboardData.items;
        } else if (window.clipboardData) {
            isWindowClipboard = true;
            items = window.clipboardData.files;
        }


        if (items !== null && items.length > 0) {

            // 클립보드라서 여러 파일이 전달될일이 없음
            // 만약 여러개의 클립보드를 지원해야되는 경우 for문으로 변경할 필요성이 있음.
            // 일단은 한개만 지원되도록 처리합니다.
            var idx = 0;

            // 이미지인 경우에만 진행
            if (items[idx].type.indexOf('image/') !== -1) {
                // 전송될 파일 (items[i]를 계속 사용하다보면 가끔씩 items[i]가 undefined로 표시되는 경우가 있어서 별도의 변수에 할당 시켜놓음)
                var clipboardFile = items[idx];

                var options = host.get('filepickeroptions').link,
                    savepath = (options.savepath === undefined) ? '/' : options.savepath,
                    formData = new FormData(),
                    timestamp = 0,
                    uploadid = "",
                    loadinghtml = "",
                    keys = Object.keys(options.repositories);

                e.preventDefault();
                e.stopPropagation();

                // 기본 값 설정
                formData.append('itemid', options.itemid);
                formData.append('env', options.env);
                formData.append('sesskey', M.cfg.sesskey);
                formData.append('client_id', options.client_id);
                formData.append('savepath', savepath);
                formData.append('ctx_id', options.context.id);

                // Upload repository로 업로드 되도록 설정
                for (var i = 0; i < keys.length; i++) {
                    if (options.repositories[keys[i]].type === 'upload') {
                        formData.append('repo_id', options.repositories[keys[i]].id);
                        break;
                    }
                }


                // IE(edge제외) 같은 경우에는 클립보드에 존재하는 file이 곧바로 전달이 되지 않음.
                if (isWindowClipboard) {
                    var reader = new FileReader();
                    reader.onload = function(data) {
                        formData.append('repo_upload_file', self._dataURLToBlob(data.target.result), 'image.png');
                    };
                    reader.readAsDataURL(clipboardFile);
                } else {
                    formData.append('repo_upload_file', clipboardFile.getAsFile());
                }

                // Insert spinner as a placeholder.
                timestamp = new Date().getTime();
                uploadid = 'moodlefile_' + Math.round(Math.random() * 100000) + '-' + timestamp;
                host.focus();
                host.restoreSelection();

                // 파일 업로드가 진행되는 동안 loading 이미지 표시
                loadinghtml = loadingtemplate({
                    url: M.util.image_url("i/loading_small", 'moodle'),
                    alt: M.util.get_string('uploading', COMPONENTNAME),
                    id: uploadid
                });
                host.insertContentAtFocusPoint(loadinghtml);
                self.markUpdated();


                // IE에서 setTimeout을 하지 않으면 업로드가 진행되지 않음.
                setTimeout(function() {
                    $.ajax({
                        url: M.cfg.wwwroot + '/repository/repository_ajax.php?action=upload',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function(result) {
                            result = JSON.parse(result);

                            if (result) {
                                if (result.error) {
                                    $("#" + uploadid).remove();
                                    return new M.core.ajaxException(result);
                                }

                                var file = result;
                                if (result.event && result.event === 'fileexists') {
                                    // A file with this name is already in use here - rename to avoid conflict.
                                    // Chances are, it's a different file (stored in a different folder on the user's computer).
                                    // If the user wants to reuse an existing file, they can copy/paste the link within the editor.
                                    file = result.newfile;
                                }

                                // Replace placeholder with actual link.
                                newhtml = template({
                                    url: file.url,
                                    text: file.file || file.filename
                                });

                                // 기존 loading이미지 삭제후 신규 이미지 추가
                                $("#" + uploadid).remove();

                                var newtag = Y.Node.create(newhtml);
                                self.editor.appendChild(newtag);
                                self.markUpdated();
                            }

                            return true;
                        },
                        error: function() {
                            Y.use('moodle-core-notification-alert', function() {
                                new M.core.alert({message: M.util.get_string('servererror', 'moodle')});
                            });

                            $("#" + uploadid).remove();
                        }
                    });

                    self.markUpdated();
                }, 1);
            }
        }

        return false;
    },


    _dataURLToBlob: function(dataURL) {
        var BASE64_MARKER = ';base64,';
        var parts = null,
            contentType = null,
            raw = null;

        if (dataURL.indexOf(BASE64_MARKER) == -1) {
            parts = dataURL.split(',');
            contentType = parts[0].split(':')[1];
            raw = decodeURIComponent(parts[1]);
            return new Blob([raw], {type: contentType});
        }

        parts = dataURL.split(BASE64_MARKER);
        contentType = parts[0].split(':')[1];
        raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);
        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }
        return new Blob([uInt8Array], {type: contentType});
    }
});