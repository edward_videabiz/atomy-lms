<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = '111.111.111.111';
$CFG->dbname    = 'lms_cm';
$CFG->dbuser    = 'lms';
$CFG->dbpass    = 'pass';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
);

// middle DB
$CFG->middle = new stdClass();
$CFG->middle->dbtype    = 'mysqli';
$CFG->middle->dblibrary = 'native';
$CFG->middle->dbhost    = '111.111.111.111';
$CFG->middle->dbname    = 'lms_haksa';
$CFG->middle->dbuser    = 'lms';
$CFG->middle->dbpass    = 'pass';
$CFG->middle->prefix    = '';
$CFG->middle->dboptions = array (

);

// haksa DB
$CFG->haksa = new stdClass();
$CFG->haksa->dbtype    = 'oci';
$CFG->haksa->dbhost    = '111.111.111.111';
$CFG->haksa->dbname    = 'oradb';
$CFG->haksa->dbuser    = 'user';
$CFG->haksa->dbpass    = 'pass';
$CFG->haksa->dboptions = array (
                'dbpersist'=>false,
                'dbport' => '1521'
);


$CFG->wwwroot   = 'http://coursemos.kr';
$CFG->dataroot  = '/data/Contents/moodledata';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0775;


require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!

require_once $CFG->dirroot.'/local/ubion/config.php';
