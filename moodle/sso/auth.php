<?php
use local_ubion\base\Common;

require '../config.php';

$t = required_param('t', PARAM_NOTAGS);
// $t = urldecode($t);

if (!empty($t)) {
    $PAGE->set_url('/sso/auth.php');
    $PAGE->set_context(context_system::instance());
    
    // $t값 앞뒤로 작은 따옴표 처리해줘야됨.
    $t = "'".$t."'";
    
    // 토큰 로그인 로직 실행
    \local_vn\API::getInstance()->loginUserToken($t);
} else {
    Common::printNotice(get_string('not_empty_ssotoken', 'local_vn'), get_login_url());
}