//////////////////////////////////////////////////////////////////////////////////
//													
//		/common/common.js	ver 1.0					
//													
//				
//		최종수정일	:	2013/05/24					
//		최종수정	:	김준영
//													
//////////////////////////////////////////////////////////////////////////////////

var lectureCode="KIRD_Gas";

var lmsMode = true;

var vod_connect;
var vod_path = "";
var chapter;

// 플래시 로드 함수.
function FlashLoadView(chapter, index){
	chapter = chapter;
	
	var fwidth,fheight,varvalues,id,dirNswf;

	var dirNswf = "../common/common.swf"
	var swf_width = "1000px";
	var swf_height = "693px";
	var swf_name = "common";
	var swf_bgcolor = "#ffffff";
	var swf_wmode = "transparent";

	//var sendData = "this_page="+pageData.this_page+"&file_path="+pageData.file_path+"&chapter="+pageData.chapter+"&lectureCode="+lectureCode+"&vod_info="+pageData.vod_info+"&vod_filename="+rt_vodName()+"&xmlURL=lecture.xml"
	var sendData = "currentPage="+index+"&chapter="+chapter+"&lectureCode="+lectureCode+"&lmsMode="+lmsMode+"&vod_path="+vod_path;
	
	var buffer =""
	
	buffer += "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0' width='"+swf_width+"' height='"+swf_height+"' id='"+swf_name+"' swliveconnect='true'>";
	buffer += "<param name='allowScriptAccess' value='sameDomain' />";
	buffer += "<param name='movie' value='"+dirNswf+"' />";
	buffer += "<param name='menu' value='false' />";
	buffer += "<param name='quality' value='high' />";
	buffer += "<param name='FlashVars' VALUE='"+sendData+"'>";
	buffer += "<param name='Base' value='.'>";
	buffer += "<param name='SeamlessTabbing' value='false'>";
	buffer += "<param name='allowFullScreen' value='true'>";
	buffer += "<embed src='"+dirNswf+"?"+sendData+"' menu='false' Base='.' allowFullScreen='true' quality='high' bgcolor='"+swf_bgcolor+"' width='"+swf_width+"' height='"+swf_height+"' name='"+swf_name+"' allowScriptAccess='sameDomain' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer' />";
	buffer += "</object>";

	document.write(buffer);
	
	flashFocus();
}

function flashFocus(){
	window.document["common"].focus();
}


///////////// page 이동 함수.

function goPage(page){
	document.location.href = itostr(page)+".htm";
}

///////////// 메세지 함수.
function firstPg(){
	alert("시작 페이지입니다.");
}

function lastPg(){
	alert("마지막 페이지입니다.");
}
function alertMsg(msg){
	alert(msg);
}
function itostr(num){ // 숫자열을 문자열로 
	var tmpstr =  Number(num)>9?num:"0"+Number(num)
	return tmpstr
}


function PopUp(url, w, h){
 var strURL, strFeature;
    var x, y;

    x = (screen.width - width) / 2;
    y = (screen.height - height) / 2;
    
    var scroll = "no";
    var name = "win_player";
    var width = w;
    var height = h

    strURL = url+"?chapter="+chapter;
    strFeature = "left=" + x + ", top=" + y + ", width=" + width + ", height=" + height + ", menubar=no, status=no, location=no, toolbar=no, resizable=no, scrollbars=" + scroll;
    
    var win = window.open(strURL, name, strFeature);
    
    win.focus();
}

//PopUp("../common/help/help.swf?chapter="+chapter,800,600);


////////////// KIRD LMS API

function StartPage(a,b,c,d){
	if (lmsMode){
		//alert("Start : Chapter :"+a+" / Clause :"+b+" / Page :"+c+" / Name :"+d);
		parent.StudyStart(a,b,c,d);		
	}
}

function FinishPage(a,b,c,d){
	if (lmsMode){
		//alert("Finish : Chapter :"+a+" / Clause :"+b+" / Page :"+c+" / Name :"+d);
		parent.StudyFinish(a,b,c,d);		
	}
}