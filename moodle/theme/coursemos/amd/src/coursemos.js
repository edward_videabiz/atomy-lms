define(['jquery', 'core/event', 'core/str', 'theme_coursemos/headroom', 'theme_coursemos/bootstrap', 'theme_coursemos/bootstrap-datepicker', 'theme_coursemos/scrollert', 'theme_coursemos/jquery.validate', 'theme_coursemos/bootstrap-lightbox', 'theme_coursemos/jquery.touchSwipe'], function($, Event, Str, Headroom) {
	var theme_coursemos = {};
	
	theme_coursemos.action = '/theme/coursemos/action.php';
	
	theme_coursemos.cssName = {
			backdrop : 'coursemos-backdrop'
			,menuBackdrop : 'coursemos-menu-backdrop'
			,headroomPin : 'coursemos-headroom-pin'
			,headroomUnpin : 'coursemos-headroom-unpin'
			,menuHide : 'coursemos-menu-hide'
			,menuPin : 'coursemos-menu-pin'
			,menuUnpin : 'coursemos-menu-unpin'
			,mobileMenuShow : 'coursemos-mobile-menu-show'
			,bodyBlur : 'coursemos-body-blur'
	};

	theme_coursemos.isMobile = function() {
		isMobile = false;
		if (window.orientation > -1) {
			isMobile = true;
		}
		
		return isMobile;
	}
	
	// requirejs에서 사용될수 있도록 별도로 함수 구성
	theme_coursemos.ajaxSetting = function() {
		
		
	};
	
	theme_coursemos.jquerySetting = function() {
		$.fn.center = function () {
			$(this).css({
			    "position": "absolute",
			    "top": ( $(window).scrollTop() + ($(window).height() - $(this).height())/2 ) + "px",
			    "left":  ( $(window).scrollLeft() + ($(window).width() - $(this).width())/2 ) + "px"
			});
			
		    return this;
		}

		$.fn.pagetop = function () {
			$(this).css({
			    "position": "fixed",
			    "top": "5px",
			    "left":  ( $(window).scrollLeft() + ($(window).width() - $(this).width())/2 ) + "px"
			});
			
		    return this;
		}
		

		$.fn.serializeObject = function () {
		    var o = {};
		    var a = this.serializeArray();
		    
		    $.each(a, function () {
		        if (o[this.name] !== undefined) {
		            if (!o[this.name].push) {
		                o[this.name] = [o[this.name]];
		            }      
		            o[this.name].push(this.value || '');
		        } else {
		            o[this.name] = this.value || '';
		        }
		    });
		    return o;
		};
		
		
		$.fn.headroom = function(option) {
		    return this.each(function() {
		      var $this   = $(this),
		        data      = $this.data('headroom'),
		        options   = typeof option === 'object' && option;

		      options = $.extend(true, {}, Headroom.options, options);

		      if (!data) {
		        data = new Headroom(this, options);
		        data.init();
		        $this.data('headroom', data);
		      }
		      if (typeof option === 'string') {
		        data[option]();

		        if(option === 'destroy'){
		          $this.removeData('headroom');
		        }
		      }
		    });
		};
		
		
		$.fn.serializeFiles = function() {
	        var form = $(this),
	            formData = new FormData()
	            formParams = form.serializeArray();
	    
	        $.each(form.find('input[type="file"]'), function(i, tag) {
	          $.each($(tag)[0].files, function(i, file) {
	            formData.append(tag.name, file);
	          });
	        });
	    
	        $.each(formParams, function(i, val) {
	          formData.append(val.name, val.value);
	        });
	    
	        return formData;
	    }
		

        $('[data-headroom]').each(function() {
	        var $this = $(this);
	        $this.headroom($this.data());
	    });
        
            	
    	
    	/** jquery setting **/
		$.ajaxSetup({
			 type:"POST"
			//,contentType:"application/json; charset=utf-8"
			,error:function(xhr, msg) {
				// coursemostype 파라메터 존재여부와 상관 없이 무조건 loading 메시지 숨김
				hideProgress();
				
				console.log(msg);
				console.log(xhr.responseText);
				// alert(msg + "\n" + xhr.responseText);
		    }
			,beforeSend : function(jqXHR, settings) {
			    
				// string 형태로 전달되는 경우에만 해당됨.
				if (settings.contentType != false && settings.contentType != 'application/json') {
				//  ajax 통신시 sesskey가 없다면 자동으로 sesskey를 붙여줘야됨.
					var queryParameters = {}, queryString = settings.data,
					re = /([^&=]+)=([^&]*)/g, m;
					
					while (m = re.exec(queryString)) {
						
						pkey = decodeURIComponent(m[1]);
						pvalue = decodeURIComponent(m[2]);
						
						if (queryParameters[pkey] !== undefined) {
							if (!$.isArray(queryParameters[pkey])) {
								queryParameters[pkey] = [queryParameters[pkey]];
		                    }
							queryParameters[pkey].push(pvalue);
		                    
						} else {
							queryParameters[pkey] = pvalue;	
						}
						
					}
					
					if (!queryParameters.hasOwnProperty('sesskey')) {
						queryParameters['sesskey'] = M.cfg.sesskey;
					}
					
					// coursemostype 파라메터가 존재할 경우에만 loading 메시지 표시해주기
					// TODO showProgress를 원하지 않는 경우가 있음.
					// 해당 부분 구현해줘야됨.
					if (queryParameters.hasOwnProperty(coursemostype) && !queryParameters.hasOwnProperty(coursemostype_noprogress)) {
						showProgress();
					}
					
					
					settings.data = $.param(queryParameters);
				}
			},
			complete : function() {
				// coursemostype 파라메터 존재여부와 상관 없이 무조건 loading 메시지 숨김
				hideProgress();
			}
		});
		
		
		// $(document).ajaxStart(function () { showProgress() }).ajaxStop(function () { hideProgress() });
	};
	
	
	theme_coursemos.init = function() {
	    
		// 멀티랭귀지
		theme_coursemos.langSettings();
		
		// /theme/coursemos/javascript/custom파일에 추가해놓으면 requirejs 사용시 함수를 못찾는 문제가 있어서 이곳에서 작성해줘야됨.
		theme_coursemos.jquerySetting();
		
		// 현재 해상도에 따라서 추가 body class를 입력해줘야됨.
		theme_coursemos.resize();
		
    	
		
		$("#page-header.headroom").headroom({
			tolerance: 10
			,offset: 100		// header bar가 지정한 top위치보다 큰 경우 header가 사라짐
			,onPin : function() {
				$("body").addClass(theme_coursemos.cssName.headroomPin).removeClass(theme_coursemos.cssName.headroomUnpin);
			}
		    ,onUnpin : function() {
		    	$("body").addClass(theme_coursemos.cssName.headroomUnpin).removeClass(theme_coursemos.cssName.headroomPin);
		    }
		});
		
		

		/* ---------- DatePicker ---------- */
		datepickerLang = (M.cfg.lang == 'ko') ? 'kr' : M.cfg.lang;
		
		$(".form-datepicker").datepicker({
			format : 'yyyy-mm-dd'
			,language : datepickerLang
		    ,autoclose:true
		});
		
		/* ---------- Tooltip ---------- */
		$('[data-toggle="tooltip"]').tooltip();
		
		/* ---------- dropdown ---------- */
		$(".dropdown-toggle").dropdown();

		
		// input에 bootstrap class 적용
		$("input, select, textarea").each(function() {
			switch($(this).attr('type')) {
				case 'button' :
				case 'submit' :
				case 'checkbox' :
				case 'radio' :
				case 'hidden' :
				case 'image' :
					
					break;
				default :
					if(!$(this).hasClass('form-control')) {
						$(this).addClass('form-control');
					}
					break;
			}
		});
		
		// required설정
		setRequiredIcon();
		
		
		
		// 테이블에 bootstrap css 씌워주기.
		$('table').each(function() {
			if(!$(this).hasClass('message_user_pictures') && !$(this).is('[border]')) {
				if(!$(this).hasClass('table')) {
					//$(this).addClass('table table-bordered table-striped');
					$(this).addClass('table table-bordered table-coursemos');
				}
			}
		});
		
		// form submit시 layout 띄워주기
		$("form.form-coursemos").submit(function(ev) {
			if($.trim($(this).attr('onsubmit'))) {
				submit_result =  new Function($(this).attr('onsubmit'))();
				if(submit_result) {
					showSubmitProgress();
					return true;
				}	
			} else {
				showSubmitProgress();
				return true; 
			}
			
			return true;
		});
		/*
		$("form.mform, form.ubform").not('.gradingbatchoperationsform, .noshowsubmit').submit(function(ev) {
			
			// 성적 export시에는 로딩 메시지가 출력되면 안됨.
			if($(this).closest('body').hasClass('path-grade-export')) {
				return true;
			} else {
				if($.trim($(this).attr('onsubmit'))) {
					submit_result =  new Function($(this).attr('onsubmit'))();
					if(submit_result) {
						showSubmitProgress();
						return true;
					}	
				} else {
					showSubmitProgress();
					return true; 
				}
			}
			
			return true;
		});
		*/
		
		$('.coursemos-lightbox').click(function() {
			$(this).ekkoLightbox({alwaysShowClose: true, wrapping: false});
			
			return false;
		});
		
		// 페이지 기본 설정
		theme_coursemos.pageinit();
		
		$(window).resize(function() {
			theme_coursemos.resize();
		});
	},
	
	theme_coursemos.pageinit = function() {
		
		$("#page-header .btn-menuonoff").click(function() {
            // backdrop형태는 한개만 출력되어야 함.
            theme_coursemos.userInfoBackdropHide();
            
			isMobileView = $("body").hasClass("coursemos-page-xs") || $("body").hasClass("coursemos-page-sm");
			
			className = theme_coursemos.cssName.menuHide;
			if (isMobileView) {
				className = theme_coursemos.cssName.mobileMenuShow;
			}
			$("body").toggleClass(className);
			
			
			// backdrop 추가
			if (isMobileView) {
				if ($('body').hasClass(theme_coursemos.cssName.mobileMenuShow)) {
				    theme_coursemos.menuBackdropShow();	
				} else {
				    theme_coursemos.menuBackdropHide();
				}	
			}
		});
		
		

		// 사이트 메뉴 클릭시 하위 메뉴 표시해주기
		/*
		$("#page-lnb .site-menus .site-menu-link").click(function(e) {
			if($(this).parent().find('ul > li').length > 0) {
				// 상위 메뉴에 hover 이벤트가 없으면 추가해주기
				if (!$("#page-lnb .site-menus").hasClass('hover')) {
					$("#page-lnb .site-menus").addClass('hover');
				}
				
				// 상위 메뉴 타이틀 강좌 
				$(this).parent().toggleClass('active');
				
				// scroll 영역 update
				theme_coursemos.siteMenuScrollUpdate();
				e.preventDefault();
			}
		});
		*/
		
		$("#page-lnb .site-menus").swipe({
			swipeLeft : function(event, direction, distance, duration, fingerCount, fingerData, currentDirection) {
				// 강좌 페이지인 경우에만 사이트 메뉴가 닫히는 swipeLeft 이벤트가 실행되어야 함.
				if ($("body").hasClass('coursemos-coursepage')) {
					theme_coursemos.siteMenuClose();
				}
			},
			tap : function(event, target) {
				el = $(target);
				
				if (el.hasClass('site-menu-link') || el.hasClass('site-menu-icon')) {
					
					submenu = el.closest('.menu-item');
					
					// 메뉴 클릭 당시 메뉴 오픈 여부
					isToggleClass = true;
					
					// 상위 메뉴에 hover 이벤트가 없으면 추가해주기
					if (!$("#page-lnb .site-menus").hasClass('hover')) {
						$("#page-lnb .site-menus").addClass('hover');
						
					
						// 선택된 메뉴가 active 된 메뉴이면 toggleClass가 실행되면 안됨.
						if (submenu.hasClass('active')) {
							isToggleClass = false;
						}
					}
					
					if (isToggleClass) {
						// 상위 메뉴 타이틀 강좌 
						submenu.toggleClass('active');
					}
					
					// scroll 영역 update
					theme_coursemos.siteMenuScrollUpdate();
					
					event.preventDefault();
				}
			}
		});
		
		
		// 강좌 페이지인 경우
		if ($("body").hasClass('coursemos-coursepage')) {

			// 모바일 화면에서 메뉴 닫기
			$("#page-lnb .site-menus .site-menu-close").click(function() {
				theme_coursemos.siteMenuClose();
			});
			
			// 강좌 하위 메뉴 열닫기
			$(".block-coursemos-menu .topmenu > li > a").click(function() {
				$(this).parent().toggleClass('expand');
				
				theme_coursemos.courseMenuScrollUpdate();
				return false;
			});	
		}
		
		
		
		
		//$('body').on('click', function (e) {
			/* bootstrap v4a6용
			$('[data-toggle="popover"]').each(function () {
		        //the 'is' for buttons that trigger popups
		        //the 'has' for icons within a button that triggers a popup
				if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
					if ($(this).data('bs.popover') !== undefined) {
						$(this).data('bs.popover')._isTransitioning = false;
			        	$(this).popover('hide');
			        }
		        }
		    });
		    */
			/*
			$('[data-toggle="popover"]').each(function () {
		        //the 'is' for buttons that trigger popups
		        //the 'has' for icons within a button that triggers a popup
		        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
		        	if ($(this).data('bs.popover') !== undefined) {
			        	// click 상태값은 무조건 비활성화 상태로 유지해야됨
			            $(this).data('bs.popover').inState['click'] = false;
			            $(this).popover('hide');
		        	}
		        }
		    });
		    */
		//});
		
		
		// We do twice because: https://github.com/twbs/bootstrap/issues/10547
        jQuery('body').popover({
            trigger: 'focus',
            selector: "[data-toggle=popover][data-trigger!=hover]"
        });

        jQuery("html").popover({
            container: "body",
            selector: "[data-toggle=popover][data-trigger=hover]",
            trigger: "hover",
            delay: {
                hide: 500
            }
        });

        // We need to call popover automatically if nodes are added to the page later.
        Event.getLegacyEvents().done(function(events) {
            jQuery(document).on(events.FILTER_CONTENT_UPDATED, function() {
                jQuery('body').popover({
                    selector: '[data-toggle="popover"]',
                    trigger: 'focus'
                });
            });
        });
        
		
		$(".block-coursemos .block-controls .block-hider-hide, .block-coursemos .block-controls .block-hider-show").click(function() {
			block_coursemos = $(this).closest('.block-coursemos');
			block_name = block_coursemos.attr('data-name');
			
			if(block_name) {
				status = 'show';
				if($(this).hasClass('block-hider-hide')) {
					block_coursemos.addClass('hidden');
					status = 'hide';
				} else {
					block_coursemos.removeClass('hidden');
				}
				
				/*
				$.ajax({
					url : '/theme/coursemosv2/action.php'
					,data: 'type=block_show_hide&name='+block_name+'&status='+status+'&sesskey='+M.cfg.sesskey
					,type:'post'
					,success:function(data){
						if(data.code == mesg.success) {
							
						} else {
							alert(data.msg);
							return false;
						}
					}
				});
				*/
			}
		});
		
		
		if ($(".block-coursemos-activity").length > 0) {
			$(".block-coursemos-activity .btn-add-mod").click(function(e) {
				e.stopPropagation();
				
				theme_coursemos.courseSectionModal($(this));
			});
			
			
			$(".block-coursemos-activity .btn-more").click(function() {
				$(".block-coursemos-activity .more-activity").removeClass('hidden');
				
				$(this).parent().addClass('hidden');
				$(".block-coursemos-activity li.extra-modules").toggleClass('hidden');
				
				theme_coursemos.courseMenuScrollUpdate();
			});
		}
		
		
		if ($(".block-coursemos-section").length > 0 && location.pathname == '/course/view.php') {
			$('body').scrollspy({ target: '#coursemos-course-sections' })
			
			$('body').on('activate.bs.scrollspy', function () {
				
                if ($(".block-coursemos-section .scrollert-content .item.active").length > 0) {
					
                	var activePosition = $(".block-coursemos-section .scrollert-content .item.active").offset().top;
                	var sectionPosition = $(".block-coursemos-section .scrollert-content .sections").offset().top;
                	var scrollTopPosition =  activePosition - sectionPosition;
                	
                	$(".block-coursemos-section .scrollert-content").scrollTop(scrollTopPosition);
			    }
			});
		}
		
		
		
		
		// 성취도? 헬프인 경우에는 popover가 left에서 표시되어야 함.
		if ($("#completionprogressid .btn-moodle-help").length > 0) {
			$("#completionprogressid .btn-moodle-help").attr('data-placement', 'left');
		}
		
		$(".btn-moodle-help").popover();
		
		
		$("#coursemos-common-buttons .option-btn").click(function() {
			backdropName = 'coursemos-common-buttons-backdrop';
			
			if ($('#coursemos-common-buttons').hasClass('open')) {
				
				$("body").removeClass('coursemos-body-blur');
				$("."+backdropName).remove();
				$('#coursemos-common-buttons').removeClass('open');
			} else {
				
				$("#coursemos-common-buttons .top-sidebar .btn-round, #coursemos-common-buttons .top-sidebar button[type='submit']").each(function() {
					button = $(this).find('.btn-txt');
					if (button.length == 0) {
						$(this).html('<span class="btn-txt">' + $(this).text() + '</span>');
					}
				});
				
				$("body").addClass('coursemos-body-blur');
				$('#coursemos-common-buttons').addClass('open');
				
				if ($("."+backdropName).length == 0) {
					$("body").append($("<div />").addClass(theme_coursemos.cssName.backdrop).addClass(backdropName));
					
					// 스크롤 잠금
					theme_coursemos.scrollLock($("."+backdropName));
					theme_coursemos.scrollLock($("#coursemos-common-buttons"));
					
					$("."+backdropName).click(function() {
						$("body").removeClass(theme_coursemos.cssName.bodyBlur);
						$('#coursemos-common-buttons').removeClass('open');
						$(this).remove();
					});
				}
			}
			
		});
		
		// 학습자원/활동 추가
		$('#coursemos-common-buttons .button-item-mods .mod-icon').click(function (e) {
			e.stopPropagation();
			
			theme_coursemos.courseSectionModal($(this));
		});
		
		// scroll body to 0px on click
		$('#coursemos-common-buttons .pagetop-btn').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 500);
			return false;
		});
		
		
		// 사용자 정보
		$("#page-header .nav-item-userinfo .btn-userinfo").click(function() {
		    
		    // 사용자 정보 보기 항목이 열려있는지 확인
		    // 만약 열려있다면 사용자 정보 데이터만 새로 가져와서 화면에 표시만 해주면됨.
		    isOpen = $("#page-userinfo").is(':visible');
		    if (!isOpen) {
                // 메뉴가 열려있는 상태에서 사용자 정보 영역을 활성화 시키면 영역 충돌문제가 발생되므로 메뉴는 숨김 처리 되어야 함.
	            theme_coursemos.menuBackdropHide();
	            
	            theme_coursemos.userInfoBackdropShow();
	            
	            // badge가 존재하면 곧바로 notification이 열려야됨.
	            if ($(this).find('.badge').length > 0) {
	                // show.bs.tab에 의해서 자동으로 가져오기 때문에 tab show만 진행하면 됩니다.
	                $('#page-userinfo a[href="#tab-notifications"]').tab('show');
	            } else {
	                theme_coursemos.userInfoMy();
	            }
	            
	            $("#page-userinfo").show();
		    }
		});
		
		
		$("#page-userinfo .userinfo-close").click(function() {
		    theme_coursemos.userInfoBackdropHide();
		});
		
		$('#page-userinfo a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            viewtype = $(e.target).data('type');
            
            if (viewtype == 'my') {
                theme_coursemos.userInfoMy();
            } else {
                theme_coursemos.userInfoNotifications();
            }
        });
		
		
		// panel-collapse
		$(".panel-collapse .panel-heading-link").click(function(e) {
			e.preventDefault();
			$(this).closest('.panel-collapse').toggleClass('panel-body-hidden');
		});
		
		// anchor로 호출되는 경우가 있기 때문에 페이지 로드시 스크롤 위치로 메뉴 고정 여부를 판단해야됨.
		$( window ).scroll(function() {
			theme_coursemos.gradePageScroll();
			
			scrolltop = $(this).scrollTop();
			if (scrolltop > 100) {
				$('#coursemos-common-buttons .pagetop-btn').fadeIn();
			} else {
				$('#coursemos-common-buttons .pagetop-btn').fadeOut();
				
			}
		});
		
		
		// 성적 페이지 관련 로직 실행
		theme_coursemos.gradePageWidth();
		
		// layout이 popup이나 embeded 같은 경우에는 scrollert 실행 방지
		//if (!$("body").hasClass('noscrollert')) {
			$(".scrollert").scrollert({axes:['y'], useNativeFloatingScrollbars:false});
			
			$(window).resize(function() {
				$(".scrollert").scrollert('update');
			});
			
			$( window ).scroll(function() {
				$(".scrollert").scrollert('update');
			});
		// }
			
			
        // family site
		$("#page-footer .family-sites .btn-family-sites").popover({
			html : true
			,placement : 'top'
			,trigger : 'focus'
			,content : function() {
				return $("#page-footer .family-sites .sites").html();
			}
		})
	},
	
	
	// 스크롤 잠금
	theme_coursemos.scrollLock = function(el) {
		$(el).on("mousewheel DOMMouseScroll wheel touchmove",function(H) {H.preventDefault()});
	},
	
	// 주차 선택 모달
	theme_coursemos.courseSectionModal = function(el) {
		
		modname = $(el).data('modname');
		title = $(el).attr('title');
		
		// tooltip에 의해서 title값이 data-original-title로 변경되었을수도 있음
		if (title == '') {
			title = $(el).data('original-title');
		}
		
		$("#modal-coursemos-activity").on('shown.bs.modal', function (e) {
			// 스크롤을 0위치로 이동시켜놔야됨.
			$("#modal-coursemos-activity .modal-body").scrollTop(0);
		});
		
		$("#modal-coursemos-activity .modal-title").text(title);
		$("#modal-coursemos-activity").modal('show');
		
		
		// modal-coursemos-activity가 여러곳에서 사용되기 때문에 btn-modedit 이벤트를 제거한 뒤 다시 할당 시켜줘야됨
		// 해당 코드를 제거하지 않으면 click 이벤트가 여러번 발생될수 있습니다.
		$("#modal-coursemos-activity .btn-modedit").off('click');
		$("#modal-coursemos-activity .btn-modedit").click(function() {
			location.href = $(this).attr('href') + '&add=' + modname;
			return false;
		});
	},
	
	theme_coursemos.siteMenuScrollUpdate = function() {
		$("#page-lnb .site-menus .scrollert").scrollert('update');
	},
	
	theme_coursemos.siteMenuClose = function() {
		$("#page-lnb .site-menus").removeClass('hover');
		
		// mouseleave시 현재페이지에 해당되는 메뉴에 대해서는 메뉴 open이 유지되어야 있어야 함.
		pagecurrent = $(".coursemos-coursepage #page-lnb .site-menus .menu-item .active").closest('.site-menus');
		
		// 하위메뉴들 active class모두 제거해주기
		$(".coursemos-coursepage #page-lnb .site-menus .menu-item").removeClass('active');
		
		// 현재페이지에 해당되는 메뉴 재활성화 시켜주기
		if (pagecurrent.length > 0) {
			pagecurrent.addClass('active');
		}
		
		theme_coursemos.siteMenuScrollUpdate();
	},
	
	theme_coursemos.courseMenuScrollUpdate = function() {
		$("#page-lnb .course-menus .scrollert").scrollert('update');
	},
	
	theme_coursemos.gradePageWidth = function() {
		// 성적 보기 페이지인 경우
		if ($("#page-grade-report-grader-index").length > 0) {
			// 강좌 메뉴가 fixed인 관계로 본문내용 좌우 스크롤시 강좌 메뉴가 계속 표시되는 경우가 있음
			// 이를 방지하기 위해서 navigation 영역 크기를 늘려줘야됨.
			outerwidth = $("#region-main").outerWidth() - parseInt($("#page-content .page-content-navigation").css('marginLeft'));
			$("#page-content .page-content-navigation").width(outerwidth);
			
		}
	},
	
	theme_coursemos.gradePageScroll = function() {
		if( $("#page-grade-report-grader-index .gradeparent .floater.heading").length > 0) {
			
			// 테이블 header영역이 고정되어야 할 위치
			// floatTopPosition = $("#user-grades tr.heading").offset().top;
			
			setTimeout(function() {
				el = $("#page-grade-report-grader-index .gradeparent .floater.heading");
				floaterTop = parseInt(el.css('top'));
				
				if ($("body").hasClass('coursemos-headroom-pin')) {
					headerHeight = $("#page-header").outerHeight();
					
					newTop = floaterTop + headerHeight;
				} else {
					newTop = floaterTop;	
				}
				
				if (newTop > 0) {
					el.css('top', newTop);
				}
			}, 1);
			
		}
	},
	
	theme_coursemos.menuBackdropShow = function() {
	    theme_coursemos.backdrop(true, theme_coursemos.menuBackdropHide);
	    $("." + theme_coursemos.cssName.backdrop).addClass(theme_coursemos.cssName.menuBackdrop);
	},
	
	theme_coursemos.menuBackdropHide = function() {
	    theme_coursemos.backdrop(false, new function() {
	        
	        $("body").removeClass(theme_coursemos.cssName.bodyBlur).removeClass(theme_coursemos.cssName.mobileMenuShow);
	        $("#page-lnb .site-menus").removeClass('hover');
	        theme_coursemos.siteMenuClose();
        });
	},
	
	
	theme_coursemos.userInfoBackdropShow = function() {
	    theme_coursemos.backdrop(true, theme_coursemos.userInfoBackdropHide);
	},
	
	theme_coursemos.userInfoBackdropHide = function() {
	    theme_coursemos.backdrop(false, new function() {
	        $("#page-userinfo").hide();    
	    });
	},
	
	
    theme_coursemos.userInfoMy = function() {
	    $.ajax({
            url : theme_coursemos.action
            ,data: coursemostype + '=userInfoMy'
            ,type:'post'
            ,success:function(data){
                if(data.code == mesg.success) {
                    $("#tab-my .scrollert-content").html(data.html);
                                        
                    // page-userinfo가 show된 다음에 호출되어야 합니다.
                    $("#tab-my .scrollert").scrollert('update');

					// 언어 변경
                    $(".dropdown-lang .a-lang").click(function() {
                    	lang = $(this).data('lang');
                    	
                    	// 언어 설정 정보 저장
                    	$.ajax({
                            url : theme_coursemos.action
                            ,data: coursemostype + '=langChange&lang=' + lang
                            ,type:'post'
                            ,success:function(data){
                                if(data.code == mesg.success) {
                                	location_search('lang', lang);            
                                } else {
                                    alert(data.msg);
                                    return false;
                                }
                            }
                        });
                    	
                    	
                    	return false;
                	});
                } else {
                    alert(data.msg);
                    return false;
                }
            }
        });
    },
    
    
    theme_coursemos.userInfoNotifications = function() {
        $.ajax({
            url : theme_coursemos.action
            ,data: coursemostype + '=userInfoNotifications'
            ,type:'post'
            ,success:function(data){
                if(data.code == mesg.success) {
                    $("#tab-notifications .scrollert-content").html(data.html);
                                        
                    // page-userinfo가 show된 다음에 호출되어야 합니다.
                    $("#tab-notifications .scrollert").scrollert('update');
                    
                    $("#tab-notifications .form-control-type").change(function() {
                        type = $(this).val();
                        
                        $("#tab-notifications .list-item").each(function() {
                            if (type == '') {
                                $(this).show();
                            } else {
                                if ($(this).data('type') == type) {
                                    $(this).show();
                                } else {
                                    $(this).hide();
                                }
                            }
                        });
                        
                        $("#tab-notifications .scrollert").scrollert('update');
                    });
                    
                    // badge 삭제
                    // 알림 항목을 열었을때 자동읽음 처리를 진행하지 않으면 해당 항목을 주석 처리해주시기 바랍니다.
                    $("#page-header .nav-item-userinfo .btn-userinfo .badge").remove();
                } else {
                    alert(data.msg);
                    return false;
                }
            }
        });
    },
	
	theme_coursemos.backdrop = function(isShow, event) {
	    if (isShow) {
            // 기존에 등록된 항목이 있으면 삭제
            if ($("." + theme_coursemos.cssName.backdrop).length > 0) {
                $("." + theme_coursemos.cssName.backdrop).remove();
            }
                        
            if (!$("body").hasClass(theme_coursemos.cssName.bodyBlur)) {
                $("body").addClass(theme_coursemos.cssName.bodyBlur);
            }
            
            $("body").append($("<div />").addClass(theme_coursemos.cssName.backdrop));
            
            $("." + theme_coursemos.cssName.backdrop).click(function() {
                $(this).remove();
            }).bind('click', event);
        } else {
            $("body").removeClass(theme_coursemos.cssName.bodyBlur);
            $("." + theme_coursemos.cssName.backdrop).trigger('click');
            $("." + theme_coursemos.cssName.backdrop).remove();
        }
	}
	
	
		
	theme_coursemos.resize = function() {
		width = $(window).width();

		$('body').removeClass('coursemos-page-xs coursemos-page-sm coursemos-page-md coursemos-page-lg coursemos-page-xl');
		
		if (width < 576) {
			$('body').addClass('coursemos-page-xs');
		} else if (width >= 576 && width < 768) {
			$('body').addClass('coursemos-page-sm');
		} else if (width >= 768 && width < 992) {
			$('body').addClass('coursemos-page-md');
		} else if (width >= 992 && width < 1200) {
			$('body').addClass('coursemos-page-lg');
		} else if (width > 1200) {
			$('body').addClass('coursemos-page-xl');
		}
		
		theme_coursemos.gradePageWidth();
		theme_coursemos.gradePageScroll();
		
	},
	
	
	theme_coursemos.langSettings = function() {

        // 다국어 처리
    	if (M.cfg.lang == 'ko') {
			$.fn.datepicker.dates['kr'] = {
	    		days: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
	    		daysShort: ["일", "월", "화", "수", "목", "금", "토"],
	    		daysMin: ["일", "월", "화", "수", "목", "금", "토"],
	    		months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
	    		monthsShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"]
	    	};
	            	
    		$.extend($.validator.messages, {
    			required		: " 반드시 입력(선택)하셔야 합니다.",
    			remote			: " Please fix this field.",
    			email			: " 이메일 주소가 올바르지 않습니다.",
    			url				: " URL 주소가 올바르지 않습니다.",
    			date			: " 날자 형식이 올바르지 않습니다.",
    			dateISO			: " 날자 형식이 올바르지 않습니다. (ISO).",
    			number			: " 숫자형식으로 입력해주시기 바랍니다.",
    			digits			: " 정수형식으로 입력해주시기 바랍니다.",
    			creditcard		: " 신용카드 형식으로 입력해주시기 바랍니다.",
    			equalTo			: " 동일한 값을 입력해주시기 바랍니다.",
    			accept			: " Please enter a value with a valid extension.",
    			maxlength		: $.validator.format(" {0}글자 이상 입력하실수 없습니다."),
    			minlength		: $.validator.format(" {0}글자 이상 입력해주시기 바랍니다."),
    			rangelength		: $.validator.format(" Please enter a value between {0} and {1} characters long."),
    			range			: $.validator.format(" Please enter a value between {0} and {1}."),
    			max				: $.validator.format(" {0} 이하의 값을 입력해주시기 바랍니다."),
    			min				: $.validator.format(" {0} 이상의 값을 입력해주시기 바랍니다."),
    			alphaLower		: " 알파벳 소문자만 입력하셔야 합니다.",
    			alphaUpper		: " 알파벳 대문자만 입력하셔야 합니다.",
    			alpha			: " 알파벳만 입력하셔야 합니다.",
    		});
    	};

	}
	
	return theme_coursemos;
});