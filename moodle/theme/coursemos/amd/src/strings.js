define(['jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate', 'theme_coursemos/jquery-sortable'], function($) {
	var strings = {};
	
	strings.management = function(url) {

		$("form.form-validate").validate({
			submitHandler : function(form) {
				$(form).find(":disabled").removeAttr('disabled');
			
			
				$.ajax({
					 data: $(form).serialize()
					,url : url
					,success:function(data){
						if(data.code == mesg.success) {
							location.reload();
						} else {
							alert(data.msg);
						}
					}
					,error: function(xhr, option, error){
				      alert(xhr.status + ' : ' + error);
				  	}
				});
			}
		});
	
	    $(".btn-string-add").click(function() {
	    	stringType = $(this).data('type');
	    	
	    	$("#modal-family .modal-title .modal-title-changetype").text('추가');
	    	
	    	$("#modal-family form").each(function() { this.reset(); })
    		$("#modal-family form input[name='"+coursemostype+"']").val('insert');
    		$("#modal-family form input[name='type']").val(stringType);
    		
			$("#modal-family").modal('show');
		});
		
		$(".btn-string-update").click(function() {
			id = $(this).data('id');
			
			$.ajax({
				 data: param = coursemostype+'=view&id='+id
				,url : url
				,success:function(data){
					if(data.code == mesg.success) {
						$("#modal-family .modal-title .modal-title-changetype").text('변경');
	
	
						$("#modal-family form input[name='"+coursemostype+"']").val('update');
						
						coursemosBindElement($("#modal-family form"), data.result);
						
						// value값은 serialize되어 있기 때문에 unserialize해서 수동으로 바인딩 해줘야됨.
						$.each(data.result.valueUnserialize, function(langKey, langValue) {
							$("#modal-family form [name='lang-"+langKey+"']").val(langValue);
						});
						
						
					} else {
						alert(data.msg);
					}
				}
				,error: function(xhr, option, error){
					alert(xhr.status + ' : ' + error);
			  	}
			});
	
			$("#modal-family").modal('show');
		});
		
		
		$(".btn-string-delete").click(function() {
			id = $(this).data('id');
			parentLI = $(this).closest(".sites");
			siteName = parentLI.find('.site-title').text();

			if(confirm('[' + siteName + '] 사이트를 삭제하시겠습니까?')) {
				$.ajax({
					 data: param = coursemostype+'=delete&id='+id
					,url : url
					,success:function(data){
						if(data.code == mesg.success) {
							parentLI.remove();
						} else {
							alert(data.msg);
						}
					}
					,error: function(xhr, option, error){
						alert(xhr.status + ' : ' + error);
				  	}
				});
			}
		});
		
		
		// sortorder
		menus = $(".family-box .family-sites").sortable({
			handle : '.moveicon'
			,containerPath : '.family-sites'
			,containerSelector : '.family-sites'
			,onDrop: function  ($item, container, _super) {
			    _super($item, container);
			    
			    // family-sites가 여러개 항목이 존재하기 때문에 순서를 변경한 type에 대해서만 변경값이 전달되어야 함.
			    targetIdx = null;
			    type = container.target.data('type');
			    $.each($(".family-box .family-sites"), function (idx, el) { 
			    	if (type == $(el).data('type')) {
			    		targetIdx = idx;
			    	}	
			    });
			    
			    
			    data = menus.sortable("serialize").get();
			    // data에 존재하는 key인 경우에만 진행되어야 함.
			    if (data.hasOwnProperty(targetIdx)) {
					jsonString = JSON.stringify(data[targetIdx], null, ' ');
					
					$.ajax({
						 data: param = coursemostype+'=sortorder1&type='+type+'&json='+jsonString
						,url : url
						,success:function(data){
							if(data.code == mesg.success) {
								
							} else {
								alert(data.msg);
							}
						}
						,error: function(xhr, option, error){
					      alert(xhr.status + ' : ' + error);
					  	}
					});
				}
				
			}
			
		});
	}
	
	return strings;
});