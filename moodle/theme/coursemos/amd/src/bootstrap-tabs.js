define(['jquery'], function($) {
	(function($) {
        $.fn.bootstrapResponsiveTabs = function(options) {
	
		    var settings = $.extend({
		    	// These are the defaults.
		    	minTabWidth: "80",
		    	maxTabWidth: "150",
		    	currentTab : 0,
		    	showNav : false,
		    	prevText : '<i class="fa fa-angle-left"></i>',
		    	nextText : '<i class="fa fa-angle-right"></i>'
		    }, options );
		
		    // Helper function to debounce window resize events
		    var wait_for_repeating_events = (function () {
		        var timers = {};
		        return function (callback, timeout, timer_name) {
		        	if (!timer_name) {
		        		timer_name = "default timer"; //all calls without a uniqueID are grouped as "default timer"
		        	}
		        	if (timers[timer_name]) {
		        		clearTimeout(timers[timer_name]);
		        	}
		        	timers[timer_name] = setTimeout(callback, timeout);
		        };
		    })();
		
		    // Helper function to sort tabs base on their original index positions
		    var sort_tabs = function ($tabsContainer) {
		    	var $tabs = $tabsContainer.find(".js-tab");
		    	$tabs.sort(function(a,b){
		    		return +a.getAttribute('tab-index') - +b.getAttribute('tab-index');
		    	});
		    	$tabsContainer.detach(".js-tab").append($tabs);
		    }
		
		
		    // Main functions for each instantiated responsive tabs
		    this.each(function() {
		
		    	$container = $(this);
		      
		    	var ResponsiveTabs;
		    	ResponsiveTabs = (function () {
		    		
		    		function ResponsiveTabs() {
		
		    			TABS_OBJECT = this;
		    			TABS_OBJECT.activeTabId = 1;
		    			
		    			TABS_OBJECT.tabsHorizontalContainer = $container;
		    			TABS_OBJECT.tabsHorizontalContainer.addClass("responsive-tabs").wrap("<div class='responsive-tabs-container clearfix'></div>");
		    			TABS_OBJECT.tabCount = TABS_OBJECT.tabsHorizontalContainer.find(' > li').length;
		    			
		    			TABS_OBJECT.dropDownContainer = null;
	    				TABS_OBJECT.dropDownMenu = null;
	    				
	    				TABS_OBJECT.navPrevButton = $('<button type="button" class="btn btn-nav btn-nav-prev">'+settings.prevText+'</button>');
	    				TABS_OBJECT.navNextButton = $('<button type="button" class="btn btn-nav btn-nav-next">'+settings.nextText+'</button>');
		          
		    			if(settings.currentTab > 0) {
		    				TABS_OBJECT.activeTabId = settings.currentTab;
		    			} else {
		    				// 주차 셋팅하면서 현재주차를 설정하는 경우도 있음.
		    				if(TABS_OBJECT.tabsHorizontalContainer.find('li.active')) {
		    					// tab-id가 아직 부여되지 않은 상황이라서 data-sectionnum으로 순서값으로 activeTabId를 설정함.
		    					// tab-id와 data-sectionnum과는 동일한 값임.
		    					TABS_OBJECT.activeTabId = TABS_OBJECT.tabsHorizontalContainer.find('li.active').data('sectionnum');
		    				}
		    			}
		
		    			// Update tabs
		    			var update_tabs = function () {
		    				var isMobile = (window.innerWidth < 480) ? true : false;
		    				var menuWidth = TABS_OBJECT.tabsHorizontalContainer.width();
		            
		    				// Determine which tabs to show/hide
		    				var $tabs = TABS_OBJECT.tabsHorizontalContainer.children('li');
		    				$tabs.width("100%");
		
		    				// Set min and max widths for tabs
		    				// On mobile devices smaller than 480px wide, remove min/max width restriction
		    				if (isMobile) {
		    					TABS_OBJECT.tabsHorizontalContainer.addClass('responsive-tabs-mobile');
		    					
		    					$tabs.each(function(i) {
		    						$(this)
		    						.css("min-width", 0)
		    						.css("max-width", "none");
		    					});
		    				} else {
		    					TABS_OBJECT.tabsHorizontalContainer.removeClass('responsive-tabs-mobile');
		    					
		    					$tabs.each(function(i) {
		    						$(this)
		    						.css("min-width", settings.minTabWidth + "px")
		    						.css("max-width", settings.maxTabWidth + "px");
		    					});
		    				}
		            
		    				var defaultTabWidth = $tabs.first().width();
		    				var numTabs = $tabs.length;
		
		    				var numVisibleHorizontalTabs = (Math.ceil(menuWidth / defaultTabWidth));
		    				
		    				// 모바일이면 무조건 1개만 표시되어야 함.
		    				if (isMobile) {
		    					numVisibleHorizontalTabs = 1;
		    				}
		    				
		    				var numVisibleVerticalTabs = numTabs - numVisibleHorizontalTabs;
		
		    				for(var i = 0; i < $tabs.length; i++){
		    					var horizontalTab = $tabs.eq(i);
		    					var tabId = horizontalTab.attr("tab-id");
		    					var verticalTab = TABS_OBJECT.dropDownMenu.find(".js-tab[tab-id=" + tabId + "]");
		    					var isVisible = i < numVisibleHorizontalTabs;
		
		    					horizontalTab.toggleClass('hidden', !isVisible);
		    					verticalTab.toggleClass('hidden', isVisible);
		    				}
		
		    				// Set new dynamic width for each tab based on calculation above
		    				var tabWidth = 100 / numVisibleHorizontalTabs;
		    				var tabPercent = tabWidth + "%";
		    				$tabs.width(tabPercent);
		    		
		
		    				// Toggle the Tabs dropdown if there are more tabs than can fit in the tabs horizontal container
		    				var hasVerticalTabs = (numVisibleVerticalTabs > 0)
		    				TABS_OBJECT.dropDownMenu.toggleClass("hidden", !hasVerticalTabs)
		    				TABS_OBJECT.dropDownMenu.siblings(".dropdown-toggle").find(".count").text("Tabs " + "(" + numVisibleVerticalTabs + ")");
		    				
		    				// 화면상에 모든 탭이 표시되는 경우
		    				TABS_OBJECT.tabsHorizontalContainer.parent().toggleClass('responsive-tabs-nav-hide', !hasVerticalTabs);
		    				
		
		    				// Make 'active' tab always visible in horizontal container
		    				// and hidden in vertical container
		
		    				activeTab = TABS_OBJECT.tabsHorizontalContainer.find(".js-tab[tab-id=" + TABS_OBJECT.activeTabId + "]");
		    				activeTabCurrentIndex = activeTab.index();
		    				activeTabDefaultIndex = activeTab.attr("tab-index");
		    				lastVisibleHorizontalTab = TABS_OBJECT.tabsHorizontalContainer.find(".js-tab:visible").last(); 
		    				lastVisibleTabIndex = lastVisibleHorizontalTab.index()
		
		    				lastHiddenVerticalTab = TABS_OBJECT.dropDownMenu.find(".js-tab.hidden").last();
		    				activeVerticalTab = TABS_OBJECT.dropDownMenu.find(".js-tab[tab-index=" + activeTabCurrentIndex + "]");
		
		    				if (activeTabCurrentIndex >= numVisibleHorizontalTabs) {
		    					activeTab.insertBefore(lastVisibleHorizontalTab);
		    					activeTab.removeClass("hidden");
		    					lastVisibleHorizontalTab.addClass("hidden");
		
		    					lastHiddenVerticalTab.removeClass("hidden");
		    					activeVerticalTab.addClass("hidden");
		    				}
		
		    				if ((activeTabCurrentIndex < activeTabDefaultIndex) && (activeTabCurrentIndex < lastVisibleTabIndex)) {
		    					activeTab.insertAfter(lastVisibleHorizontalTab);  
		    				}
		    				
		    				// prev, next 버튼 표시여부	
		    				$(TABS_OBJECT.navPrevButton).toggleClass('disabled', TABS_OBJECT.activeTabId <= 1);
		    				$(TABS_OBJECT.navNextButton).toggleClass('disabled', TABS_OBJECT.activeTabId >= TABS_OBJECT.tabCount);
		    			}
		
		    			// SETUP
		    			var setup = function () {
		    				// Reset all tabs for calc function
		    				var totalWidth = 0;
		    				var $tabs      = TABS_OBJECT.tabsHorizontalContainer.children('li');
		
		    				// Stop function if there are no tabs in container
		    				if ($tabs.length === 0) {
		    					return;
		    				}
		
		    				// Mark each tab with a 'tab-id' for easy access
		    				$tabs.each(function(i) {
		    					tabIndex = $(this).index();
		    					$(this)
		    					.addClass("js-tab")
		    					.attr("tab-id", i+1)
		    					.attr("tab-index", tabIndex);
		    				});
		
		    				// Attach a dropdown to the right of the tabs bar
		    				// This will be toggled if tabs can't fit in a given viewport size
		    				/* bootstrap v4용
		    				TABS_OBJECT.tabsHorizontalContainer.after("<div class='nav navbar-nav navbar-right tabs-dropdown js-tabs-dropdown'> \
		    						<div class='dropdown'> \
		    						<a href='#' role='button' class='dropdown-toggle nav-link' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><span class='count'>Tabs </span></a> \
		    						<ul class='dropdown-menu dropdown-menu-right'> \
		    						</ul> \
		    						</div> \
		    				</div>");
		    				*/
		            
				            // bootstrap v3용
				            TABS_OBJECT.tabsHorizontalContainer.after("<div class='tabs-dropdown js-tabs-dropdown'>\
					                    	<a class='dropdown-toggle btn btn-default' data-toggle='dropdown'><span class='count'>Tabs </span><b class='caret'></b></a> \
					                    	<ul class='dropdown-menu dropdown-menu-right' role='menu'> \
					                    		<div class='dropdown-header'>\
				                      				<p class='count'>Tabs</p> \
				                      				<button type='button' class='close' data-dismiss='dropdown'><span aria-hidden='true'>&times;</span></button> \
				                      				<div class='divider visible-xs'></div> \
				                    			</div> \
					                    	</ul>\
					                    </div>");
		
		    				// Clone each tab into the dropdown
		    				TABS_OBJECT.dropDownContainer = TABS_OBJECT.tabsHorizontalContainer.siblings(".tabs-dropdown");
		    				TABS_OBJECT.dropDownMenu = TABS_OBJECT.dropDownContainer.find(".dropdown-menu");
		    				$tabs.clone().appendTo(TABS_OBJECT.dropDownMenu);
		

			    			if (settings.showNav) {
			    				TABS_OBJECT.tabsHorizontalContainer.parent().addClass('responsive-tabs-nav')
			    				
			    				TABS_OBJECT.navPrevButton.click(function() {
			    					// 이전 항목이 선택되어야 하기 때문에 0보다 커야됨.
			    					if (TABS_OBJECT.activeTabId > 1 && !$(this).hasClass('disabled')) {
			    						// 선택되어야 할 탭 id
			    						TABS_OBJECT.activeTabId = TABS_OBJECT.activeTabId - 1;
			    						TABS_OBJECT.tabsHorizontalContainer.find('.js-tab[tab-id="'+TABS_OBJECT.activeTabId+'"] > .nav-link').trigger('click');
				    					
				    					// TABS_OBJECT.activeTabId
				    					update_tabs();	
			    					}
			    					
			    				});
			    				
			    				TABS_OBJECT.navNextButton.click(function() {
			    					if (TABS_OBJECT.activeTabId < TABS_OBJECT.tabCount) {
			    						// 선택되어야 할 탭 id
			    						TABS_OBJECT.activeTabId = parseInt(TABS_OBJECT.activeTabId) + 1;
			    						TABS_OBJECT.tabsHorizontalContainer.find('.js-tab[tab-id="'+TABS_OBJECT.activeTabId+'"] > .nav-link').trigger('click');
			    						
			    						update_tabs();
			    					}
			    				});
			    				
			    				TABS_OBJECT.tabsHorizontalContainer.before(TABS_OBJECT.navPrevButton);
			    				TABS_OBJECT.tabsHorizontalContainer.after(TABS_OBJECT.navNextButton);
			    			}
			    			
		    				// Update tabs
		    				update_tabs();
		    			}()
		
		
		    			/** 
		    			 * Change Tab
		    			 */
		    			change_tab = function (e) {
		    				TABS_OBJECT.tabsHorizontalContainer.parents(".responsive-tabs-container").on("click", ".js-tab", function(e) {
		
		    					// Set 'activeTabId' property from clicked tab
		    					var target = $(e.target);
		    					TABS_OBJECT.activeTabId = $(this).attr("tab-id");
		
		    					// Update tab 'active' class for horizontal container if tab is clicked
		    					// from dropdown. Otherwise Bootstrap handles the normal 'active' class placement.
		    					var verticalTabSelected = target.parents(".dropdown-menu").length > 0
		    					if (verticalTabSelected) {
		    						// bootstrap v4용
		    						// TABS_OBJECT.tabsHorizontalContainer.find(".js-tab .nav-link").removeClass("active");
		    						// TABS_OBJECT.tabsHorizontalContainer.find(".js-tab[tab-id=" + TABS_OBJECT.activeTabId + "] .nav-link").addClass("active");
		            	  
		    						// bootstrap v3 용
		    						TABS_OBJECT.tabsHorizontalContainer.find(".js-tab").removeClass("active");
		    						TABS_OBJECT.tabsHorizontalContainer.find(".js-tab[tab-id=" + TABS_OBJECT.activeTabId + "]").addClass("active");
		    					}
		
		    					// bootstrap v4 용
		    					// TABS_OBJECT.dropDownMenu.find(".js-tab .nav-link").removeClass("active");
		              
		    					// bootstrap v3 용
		    					TABS_OBJECT.dropDownMenu.find(".js-tab").removeClass("active");
		              
		    					// Call 'sort_tabs' to re-arrange tabs based on their original index positions
		    					// Call 'update_tabs' to resize tabs and determine which one to show/hide 
		    					sort_tabs(TABS_OBJECT.tabsHorizontalContainer);
		    					sort_tabs(TABS_OBJECT.dropDownMenu);
		    					update_tabs();
		    				});
		    			}()
		
		    			// Update tabs on window resize
		    			$(window).resize(function() {
		    				wait_for_repeating_events(function(){
		    					update_tabs();
		    				}, 100, "Resize Tabs");
		    			});
		    		}
		
		    		return ResponsiveTabs();
		    	})();
		    });
        };
	})(jQuery);
});