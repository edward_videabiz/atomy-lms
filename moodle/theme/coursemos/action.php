<?php
require_once '../../config.php';

$controllder = new \theme_coursemos\Controllder();

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}