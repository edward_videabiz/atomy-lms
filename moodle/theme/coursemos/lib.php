<?php

function theme_coursemos_page_init(moodle_page $page) {
	$page->requires->jquery();
}


/**
 * Inject additional SCSS.
 * 동적으로 css name이 지정되어야 할 항목들이 존재하면 이곳에 추가해주면됩니다.
 * 
 * @param theme_config $theme The theme config object.
 * @return string
 */
function theme_coursemos_get_extra_scss($theme) {
    $content = '';
    
    $cssTemplate = ".##background-name## { background-image : url(##background-url##); } ";
    
    $CThemeLogin = \theme_coursemos\Login::getInstance();
    
    $maxCount = $CThemeLogin->getBackgroundImageCount();
    for ($i = 1; $i <= $maxCount; $i++) {
        $filearea = $CThemeLogin->getBackgroundFileAreaPrefix().$i;
        $backgroundImageCssName = $CThemeLogin->getBackgroundCSSNamePrefix().$i;
        $uploadBackgroundImage = $theme->setting_file_url($filearea, $filearea);
        
        // 등록된 이미지가 존재하지 않는 경우
        if (empty($uploadBackgroundImage)) {
            $uploadBackgroundImage = '[[pix:theme|login/background-coursemos]]';
        } else {
            // url주소에 '' 감싸주기
            $uploadBackgroundImage = "'".$uploadBackgroundImage."'";
        }
        
        $backgroundCss = str_replace("##background-name##", $backgroundImageCssName, $cssTemplate);
        $backgroundCss = str_replace("##background-url##", $uploadBackgroundImage, $backgroundCss);
        
        $content .= $backgroundCss;
    }
    
    // Always return the background image with the scss when we have it.
    return !empty($theme->settings->scss) ? $theme->settings->scss . ' ' . $content : $content;
}

/**
 * Parses CSS before it is cached.
 *
 * This function can make alterations and replace patterns within the CSS.
 *
 * @param string $css The CSS
 * @param theme_config $theme The theme config object.
 * @return string The parsed CSS The parsed CSS.
 */
function theme_coursemos_process_css($css, $theme) {

    // Set color
    $tag = '[[setting:fontcolor]]';
    $css = str_replace($tag, $theme->settings->fontcolor, $css);
    
    $tag = '[[setting:linkcolor]]';
    $css = str_replace($tag, $theme->settings->linkcolor, $css);
    
    $tag = '[[setting:headercolor]]';
    $css = str_replace($tag, $theme->settings->headercolor, $css);
    
    $tag = '[[setting:headercolor_rgba]]';
    $css = str_replace($tag, $theme->settings->headercolor_rgba, $css);
    
    //$tag = '[[setting:headercolor_logo]]';
    //$css = str_replace($tag, $theme->settings->headercolor_logo, $css);
    
    $tag = '[[setting:editbutton_color]]';
    $css = str_replace($tag, $theme->settings->editbutton_color, $css);
    
    $tag = '[[setting:badge_color]]';
    $css = str_replace($tag, $theme->settings->badge_color, $css);
    
    $tag = '[[setting:quickbutton_color]]';
    $css = str_replace($tag, $theme->settings->quickbutton_color, $css);
    
    $tag = '[[setting:loginoutcolor]]';
    $css = str_replace($tag, $theme->settings->loginoutcolor, $css);

    return $css;
}

/**
 * Adds the logo to CSS.
 *
 * @param string $css The CSS.
 * @param string $logo The URL of the logo.
 * @return string The parsed CSS
 */
function theme_coursemos_set_logo($css, $logoname, $logo) {
    $tag = '[[setting:'.$logoname.']]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_coursemos_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    
    $allowFileArea = array(
        'logo'
        ,'logo_small'
        ,'logo_login'
        ,'favicon'
    );
    
    // 배경 이미지 관련처리
    $CThemeLogin = \theme_coursemos\Login::getInstance();
    $maxCount = $CThemeLogin->getBackgroundImageCount();
    for ($i=1; $i <= $maxCount; $i++) {
        $allowFileArea[] = $CThemeLogin->getBackgroundFileAreaPrefix().$i;
    }
    
    if ($context->contextlevel == CONTEXT_SYSTEM and (in_array($filearea, $allowFileArea))) {
        $theme = theme_config::load('coursemos');
        // By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}


/**
 * Returns the main SCSS content.
 *
 * @param theme_config $theme The theme config object.
 * @return string
 */
function theme_coursemos_get_main_scss_content($theme) {
    global $CFG;
    
    // 프리셋은 일단 한개
    $scss = file_get_contents($CFG->dirroot . '/theme/coursemos/scss/default.scss');
    
    
    return $scss;
}


/**
 * Get SCSS to prepend.
 *
 * @param theme_config $theme The theme config object.
 * @return array
 */
function theme_coursemos_get_pre_scss($theme) {
    global $CFG;
    
    $scss = '';
    
    // 로고 파일 셋팅
    $logs = ['logo', 'logo_small', 'logo_login'];
    
    foreach ($logs as $logo) {
     
        // $logopath = '/theme/coursemos/pix/layout/default_'.$logo.'.png';
        $logopath = '[[pix:theme|layout/default_'.$logo.']]';
        
        if (isset($theme->settings->$logo) && !empty($theme->settings->$logo)) {
            $logopath = $theme->setting_file_url($logo, $logo);
        }
        
        $scss .= '$coursemos-'.$logo.": '".$logopath."';\n";
    }
    
    $configurable = [
         'fontcolor'
        ,'linkcolor'
        ,'headercolor'
        ,'headercolor_rgba'
        ,'editbutton_color'
        ,'badge_color'
        ,'quickbutton_color'
        ,'loginoutcolor'
        ,'privacycolor'
        
        ,'loginform_background_rgba'
        ,'loginform_login_btncolor'
        ,'loginform_sso_btncolor_sso'
        ,'loginform_sso_btncolor_person'
    ];
    
    // Prepend variables first.
    foreach ($configurable as $configkey) {
        $targets = 'coursemos-'.$configkey;
        
        $value = isset($theme->settings->{$configkey}) ? $theme->settings->{$configkey} : null;
        if (empty($value)) {
            continue;
        }
        array_map(function($target) use (&$scss, $value) {
            $scss .= '$' . $target . ': ' . $value . ";\n";
        }, (array) $targets);
    }
    
    // Prepend pre-scss.
    if (!empty($theme->settings->scsspre)) {
        $scss .= $theme->settings->scsspre;
    }
    
    return $scss;
}