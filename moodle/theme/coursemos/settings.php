<?php
defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    $pluginname = 'theme_coursemos';
    
    $ADMIN->add('themes', new admin_category('themesettingcoursemos', get_string('pluginname', 'theme_coursemos')));
    
    
    ############################
    ###### 기본 테마 설정 ######
    ############################
    $temp = new admin_settingpage('themecoursemos_default', get_string('settingpage_color', $pluginname));
        
    // Logo file setting.
    $name = $pluginname.'/logo';
    $title = get_string('setting_logo','theme_coursemos');
    $description = get_string('setting_logo_desc', 'theme_coursemos');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Small logo file setting.
    $name = 'theme_coursemos/logo_small';
    $title = get_string('setting_logo_small', 'theme_coursemos');
    $description = get_string('setting_logo_small_desc', 'theme_coursemos');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo_small');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // favicon
    $name = 'theme_coursemos/favicon';
    $title = get_string('setting_favicon', 'theme_coursemos');
    $description = null;
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /////////////////
    // 색상 설정
    /////////////////
    $name = $pluginname.'/color';
    $title = new lang_string('setting_color', $pluginname);
    $description = '';
    $setting = new admin_setting_heading($name, $title, $description);
    $temp->add($setting);
    
    // Main theme color
    $name = $pluginname.'/fontcolor';
    $title = new lang_string('setting_fontcolor', $pluginname);
    $description = '';
    $default = '#068053';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // link color
    $name = $pluginname.'/linkcolor';
    $title = new lang_string('setting_linkcolor', $pluginname);
    $description = '';
    $default = '#337ab7';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = $pluginname.'/headercolor';
    $title = new lang_string('setting_headercolor', $pluginname);
    $description = '';
    $default = '#068053';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = $pluginname.'/headercolor_rgba';
    $title = new lang_string('setting_headercolor_rgba', $pluginname);
    $description = new lang_string('setting_headercolor_rgba_desc', $pluginname);
    $default = 'rgba(6, 128, 83, 0.8)';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = $pluginname.'/editbutton_color';
    $title = new lang_string('setting_editbutton_color', $pluginname);
    $description = '';
    $default = '#068053';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    $name = $pluginname.'/badge_color';
    $title = new lang_string('setting_badge_color', $pluginname);
    $description = new lang_string('setting_badge_color_desc', $pluginname);
    $default = '#bfcd18';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    $name = $pluginname.'/quickbutton_color';
    $title = new lang_string('setting_quickbutton_color', $pluginname);
    $description = '';
    $default = '#86bfaa';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = $pluginname.'/loginoutcolor';
    $title = new lang_string('setting_loginoutcolor', $pluginname);
    $description = '';
    $default = '#62b224';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = $pluginname.'/privacycolor';
    $title = new lang_string('setting_privacycolor', $pluginname);
    $description = '';
    $default = '#ffd000';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('themesettingcoursemos', $temp);
     
    
    #########################
    ###### 로그인 화면 ######
    #########################
    $temp = new admin_settingpage('themecoursemos_login', get_string('settingpage_login', $pluginname));
    
    // Logo file setting.
    $name = $pluginname.'/logo_login';
    $title = get_string('setting_logo_login','theme_coursemos');
    $description = get_string('setting_logo_login_desc', 'theme_coursemos');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo_login');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    $CThemeLogin = \theme_coursemos\Login::getInstance();
    $maxCount = $CThemeLogin->getBackgroundImageCount();
    
    // 배경 이미지 갯수
    $name = $pluginname.'/backgroundimage_count';
    $title = get_string('setting_login_viewimage_count', $pluginname);
    $infomation = null;
    $default = 1;
    $options = array();
    for ($i=1; $i <= $maxCount; $i++) {
        $options[$i] = $i;
    }
    $setting = new admin_setting_configselect($name, $title, $infomation, $default, $options);
    $temp->add($setting);
    
    // background_image
    for ($i=1; $i <= $maxCount; $i++) {
        
        $filearea = $CThemeLogin->getBackgroundFileAreaPrefix().$i;
        
        $name = 'theme_coursemos/'.$filearea;
        $title = get_string('setting_backgroundimage_'.$i,'theme_coursemos');
        $infomation = null;
        $setting = new admin_setting_configstoredfile($name, $title, $infomation, $filearea);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $temp->add($setting);
    }
    
    
    /////////////////
    // 로그인 폼 설정
    /////////////////
    $name = $pluginname.'/loginform';
    $title = new lang_string('setting_loginform', $pluginname);
    $description = '';
    $setting = new admin_setting_heading($name, $title, $description);
    $temp->add($setting);

    // 로그인 폼 위치
    $name = $pluginname.'/loginform_position';
    $title = new lang_string('setting_loginform_position', $pluginname);
    $description = null;
    $default = $CThemeLogin::LOGINFORMLEFT;
    $positionOption = array($CThemeLogin::LOGINFORMLEFT => get_string('setting_loginform_position_left', $pluginname), $CThemeLogin::LOGINFORMRIGHT => get_string('setting_loginform_position_right', $pluginname));
    $setting = new admin_setting_configselect($name, $title, $description, $default, $positionOption);
    $temp->add($setting);
    
    // 로그인 폼 배경 색상 
    $name = $pluginname.'/loginform_background_rgba';
    $title = new lang_string('setting_loginform_background_rgba', $pluginname);
    $description = null;
    $default = 'rgba(15, 67, 131, .9)';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // 로그인 버튼 색상
    $name = $pluginname.'/loginform_login_btncolor';
    $title = new lang_string('setting_loginform_login_btncolor', $pluginname);
    $description = '';
    $default = '#2464b2';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    // 비밀번호 찾기 주소
    $name = $pluginname.'/loginform_findpwd_url';
    $title = new lang_string('setting_loginform_findpwd_url', $pluginname);
    $description = null;
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    
    
    // 사용, 미사용 option (공통적으로 사용되서 상단에 위치 시킴)
    $options = array(0 => get_string('not_use', 'local_ubion'), 1 => get_string('use', 'local_ubion'));
    
    
    // sso 버튼
    $name = $pluginname.'/loginform_sso';
    $title = new lang_string('setting_loginform_sso', $pluginname);
    $description = '';
    $setting = new admin_setting_heading($name, $title, $description);
    $temp->add($setting);
    
    
    // sso 버튼 사용여부
    $name = $pluginname.'/loginform_sso_use';
    $title = get_string('setting_loginform_sso_use', $pluginname);
    $infomation = get_string('setting_loginform_sso_use_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $infomation, $default, $options);
    $temp->add($setting);
    
    // sso 버튼 클릭 주소
    $name = $pluginname.'/loginform_sso_url';
    $title = get_string('setting_loginform_sso_url', $pluginname);
    $infomation = get_string('setting_loginform_sso_url_desc', $pluginname);
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $infomation, $default);
    $temp->add($setting);
    
    // sso 버튼 색상
    $name = $pluginname.'/loginform_sso_btncolor_sso';
    $title = new lang_string('setting_loginform_sso_btncolor_sso', $pluginname);
    $description = '';
    $default = '#05264d';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // 일반 로그인 버튼 색상
    $name = $pluginname.'/loginform_sso_btncolor_person';
    $title = new lang_string('setting_loginform_sso_btncolor_person', $pluginname);
    $description = '';
    $default = '#30abd0';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    // 별도 계정
    $name = $pluginname.'/loginform_separate';
    $title = new lang_string('setting_loginform_separate', $pluginname);
    $description = '';
    $setting = new admin_setting_heading($name, $title, $description);
    $temp->add($setting);
    
    // 별도 계정 사용여부
    $name = $pluginname.'/loginform_separate_use';
    $title = get_string('setting_loginform_separate_use', $pluginname);
    $infomation = get_string('setting_loginform_separate_use_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $infomation, $default, $options);
    $temp->add($setting);
    
    // 별도 계정 전송 주소
    $name = $pluginname.'/loginform_separate_url';
    $title = get_string('setting_loginform_separate_url', $pluginname);
    $infomation = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $infomation, $default);
    $temp->add($setting);
    
    // 별도 계정 사용자 아이디 input name
    $name = $pluginname.'/loginform_separate_username';
    $title = get_string('setting_loginform_separate_username', $pluginname);
    $infomation = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $infomation, $default);
    $temp->add($setting);
    
    // 별도 계정 사용자 비밀번호 input name
    $name = $pluginname.'/loginform_separate_password';
    $title = get_string('setting_loginform_separate_password', $pluginname);
    $infomation = '';
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $infomation, $default);
    $temp->add($setting);
    
    
    
    $ADMIN->add('themesettingcoursemos', $temp);
    
    
    #####################################################
    ###### 학교 copyright 및 개인정보처리방침 주소 ######
    #####################################################
    $temp = new admin_settingpage('themecoursemos_univ', get_string('settingpage_univ', $pluginname));
    
    // copyright
    $name = $pluginname.'/copyright';
    $title = new lang_string('setting_copyright', $pluginname);
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, null, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    $name = 'theme_coursemos/privacyurl';
    $title = get_string('setting_privacyurl', 'theme_coursemos');
    $setting = new admin_setting_configtext($name, $title, null, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('themesettingcoursemos', $temp);
    
    
    #######################
    ###### 강좌 설정 ######
    #######################
    $temp = new admin_settingpage('themecoursemos_course', get_string('settingpage_course', $pluginname));
    
    // resource 출력 순서 조정
    $name = $pluginname.'/resources_sortorder';
    $title = get_string('setting_resources_sortorder', 'theme_coursemos');
    $description = get_string('setting_resources_sortorder_desc', 'theme_coursemos');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_NOTAGS, 50);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // activity 출력 순서 조정
    $name = $pluginname.'/activities_sortorder';
    $title = get_string('setting_activities_sortorder', 'theme_coursemos');
    $description = get_string('setting_activities_sortorder_desc', 'theme_coursemos');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_NOTAGS, 50);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // 빠른 모듈 추가 사용여부
    $name = $pluginname.'/is_favorite_mod';
    $title = get_string('setting_favorite_mod_use', 'theme_coursemos');
    $description = get_string('setting_favorite_mod_use_desc', 'theme_coursemos');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // 빠른 모듈 추가 순서
    $name = $pluginname.'/favorite_mod';
    $title = get_string('setting_favorite_mod', 'theme_coursemos');
    $description = get_string('setting_favorite_mod_desc', 'theme_coursemos');
    $default = 'label,assign,resource,quiz';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('themesettingcoursemos', $temp);

    
    ###########################
    ###### 공지사항 설정 ######
    ###########################
    $temp = new admin_settingpage('themecoursemos_ubboard', get_string('settingpage_ubboard', $pluginname));
    
    // mdl_modules의 id값
    $name = $pluginname.'/ubboard_id';
    $title = get_string('setting_ubboard_id', 'theme_coursemos');
    $description = get_string('setting_ubboard_id_desc', 'theme_coursemos');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $temp->add($setting);
    
    // 공지사항 게시판 cmid
    $name = $pluginname.'/ubboard_cmid';
    $title = get_string('setting_ubboard_cmid', 'theme_coursemos');
    $description = get_string('setting_ubboard_cmid_desc', 'theme_coursemos');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $temp->add($setting);
    
    $ADMIN->add('themesettingcoursemos', $temp);
    
}

/*
 * localplugin은 settings 관련 설정들이 localplugin/settings.php 에서 모두 처리하기 때문에 $settings라는 항목 자체가 존재하지 않음.
 * 
 * 그런데 테마같은 경우에는 /admin/settings/appearance.php에서
 * 
 * $settings = new admin_settingpage('themesetting'.$theme, new lang_string('pluginname', 'theme_'.$theme));
 * include($settings_path);
 * if ($settings) {
 *    $ADMIN->add('themes', $settings);
 * }
 * 
 * $settings를 추가해주는 코드가 존재함
 * 
 * 이를 방지하기 위해서 $settings값을 초기화 시켜서 전달을 해줘야됨.
 * 
 * unset을 하지 않으면 테마 설정이 한개 더 추가되는 문제가 있습니다.
 */ 
unset($settings);
$settings = null;