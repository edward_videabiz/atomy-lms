// ajax, php 간에 통신시 사용되는 규칙
var mesg = new Object();
mesg.success = "100";
mesg.failure = "300";
mesg.failureSesskey = "310";
mesg.failureNotLogin = "399";

var coursemostype = 'coursemostype';
var coursemostype_noprogress = 'coursemostype_noprogress';

/*
$(function() { 

	$(".scrollert").scrollert({axes:['y'], useNativeFloatingScrollbars:false});
	
	$(window).on("resize", function(event) {
		$(".scrollert").scrollert('update');	
	});

});
*/

function location_search2(key, value) {
	var queryParameters = {}, queryString = location.search.substring(1),
	re = /([^&=]+)=([^&]*)/g, m;
    
	while (m = re.exec(queryString)) {
		pkey = decodeURIComponent(m[1]);
		pvalue = decodeURIComponent(m[2]);
		
		if (queryParameters[pkey] !== undefined) {
			if (!$.isArray(queryParameters[pkey])) {
				queryParameters[pkey] = [queryParameters[pkey]];
            }
			queryParameters[pkey].push(pvalue);
            
		} else {
			queryParameters[pkey] = pvalue;	
		}
	}
    
	
    // ls 변경으로 인해서 page값이 변경이 되는데 기존 page갯수보다 작아지면..
    // 문제가 생기기 때문에 ls값을 변경 하면 무조건 page 1로 이동으로 시켜야됨.
	if(queryParameters.hasOwnProperty('page')) {
		queryParameters['page'] = 1;	
	}
	
	queryParameters[key] = value;
    
    location.search =  $.param(queryParameters);
}

function location_search(key, value) {
	var queryParameters = {}, queryString = location.search.substring(1),
	re = /([^&=]+)=([^&]*)/g, m;
    
	while (m = re.exec(queryString)) {
		pkey = decodeURIComponent(m[1]);
		pvalue = decodeURIComponent(m[2]);
		
		if (queryParameters[pkey] !== undefined) {
			if (!$.isArray(queryParameters[pkey])) {
				queryParameters[pkey] = [queryParameters[pkey]];
            }
			queryParameters[pkey].push(pvalue);
            
		} else {
			queryParameters[pkey] = pvalue;	
		}
	}
    
	queryParameters[key] = value;
    // ls 변경으로 인해서 page값이 변경이 되는데 기존 page갯수보다 작아지면..
    // 문제가 생기기 때문에 ls값을 변경 하면 무조건 page 1로 이동으로 시켜야됨.
    
    location.search =  $.param(queryParameters);
}

function showProgress() {
	$("body").append('<div id="ajax_loading"><div id="ajax_loading_img"><img src="/theme/coursemos/pix/ajax-loader-small.gif" alt="loading..." /> Loading...</div></div>');
    $("#ajax_loading").css({height:$(document).height()});
    $("#ajax_loading_img").pagetop();
}

function hideProgress() {
	if($("#ajax_loading").length > 0) {
		$("#ajax_loading").remove();
	}
}

function showSubmitProgress() {
	$("#ajax_loading_submit").show().css({height:$(document).height()});
    $("#ajax_loading_container").center().show();
}

function hideSubmitProgress() {
	if($("#ajax_loading_container").length > 0) {
		$("#ajax_loading_container").hide();
		$("#ajax_loading_submit").hide();
	}
}


function formSubmit(url, parameter) {
	// 임시로 form을 만들어서 submit시킴
	// parameter는 반드시 object형태여야 합니다.
	
	dynamicForm = $("<form method='post' action='"+url+"' />");
	
	if(typeof parameter === 'string') {
		stringParameter = parameter;
		
		var parameter = {}, re = /([^&=]+)=([^&]*)/g, m;
	    
		while (m = re.exec(stringParameter)) {
			pkey = decodeURIComponent(m[1]);
			pvalue = decodeURIComponent(m[2]);
			
			if (parameter[pkey] !== undefined) {
				if (!$.isArray(parameter[pkey])) {
					parameter[pkey] = [parameter[pkey]];
                }
				parameter[pkey].push(pvalue);
                
			} else {
				parameter[pkey] = pvalue;	
			}
		}
	}
	
	isSesskey = false;
	for(key in parameter) {
		if(key == 'sesskey') {
			isSesskey = true;
		}
		
		
		if(parameter.hasOwnProperty(key)) {
			if ($.isArray(parameter[key])) {
				for (ar in parameter[key]) {
					dynamicForm.append('<input type="hidden" name="'+key+'" value="'+parameter[key][ar]+'" />');	
				}
			} else {
				dynamicForm.append('<input type="hidden" name="'+key+'" value="'+parameter[key]+'" />');
			}
			
		}
	}
	
	// sesskey가 등록되지 않았다면 강제로 sesskey 추가
	if (! isSesskey) {
		dynamicForm.append('<input type="hidden" name="sesskey" value="'+M.cfg.sesskey+'" />');
	}
	
	dynamicForm.appendTo('body').submit();
}



function coursemosBindElement(form, data, excepts) {
	form.each(function() { this.reset(); })
	
	for(el in data) {
		// 제외할 대상인 경우에는 pass
		if($.inArray(el, excepts) >= 0) {
			continue;
		}
		
		element = $(form).find('[name='+el+']');

		if(element.length > 0) {
			tagname = element.get(0).tagName;

			if (tagname == 'INPUT') {
				switch(element.attr('type')) {
					default :
						element.val(data[el]);
					break;
					case 'checkbox' :
					case 'radio' :
						element.filter(':input[value="'+data[el]+'"]').prop('checked', true);
						break;
					case 'file' :
						break;		
				}
			} else if(tagname == 'SELECT') {
				element.find('option[value="'+data[el]+'"]').prop('selected', true);
			}
		}
	} 
}


function getURLParameter(url, name) {
    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
}


function getFileName(filename, stype) {
    pathHeader = filename.lastIndexOf("\\");
    pathMiddle = filename.lastIndexOf(".");
    pathEnd = filename.length;
    onlyFileName = filename.substring(pathHeader + 1, pathMiddle);
    extName = filename.substring(pathMiddle + 1, pathEnd);
    allFilename = onlyFileName + "." + extName;
    
    if(stype == "all") {
        return allFilename; // 확장자 포함 파일명
    } else if(stype == "ext") {
        return extName; // 확장자
    } else {
        return onlyFileName; // 순수 파일명만(확장자 제외)
    }
}


function setRequiredIcon() {
    $(".form-validate input.required, .form-validate select.required, .form-validate textarea.required").each(function(idx, el) {
        formgroup = $(el).closest('.form-group');
        
        if (formgroup.length > 0) {
            if (formgroup.find('.control-label i.fa-exclamation-circle').length == 0) {
                formgroup.find('.control-label').append(' <i class="fa fa-exclamation-circle text-danger" title="required"></i> ');
            }
        }
    });
}


$.fn.pagetop = function () {
	$(this).css({
	    "position": "fixed",
	    "top": "5px",
	    "left":  ( $(window).scrollLeft() + ($(window).width() - $(this).width())/2 ) + "px"
	});
	
    return this;
}

$.fn.center = function () {
	$(this).css({
	    "position": "absolute",
	    "top": ( $(window).scrollTop() + ($(window).height() - $(this).height())/2 ) + "px",
	    "left":  ( $(window).scrollLeft() + ($(window).width() - $(this).width())/2 ) + "px"
	});
	
    return this;
}