<?php
namespace theme_coursemos;


class Login extends \theme_coursemos\core\Login 
{
    private static $instance;
    
    /**
     * 강좌 Class
     * @return Login
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
}