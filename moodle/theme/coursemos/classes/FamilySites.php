<?php
namespace theme_coursemos;


class FamilySites extends \theme_coursemos\core\FamilySites
{
    private static $instance;
    
    /**
     * 관련 사이트 Class
     * @return FamilySites
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
}