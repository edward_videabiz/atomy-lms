<?php
namespace theme_coursemos\output;

defined('MOODLE_INTERNAL') || die();

use coding_exception;
use moodle_page;


class core_renderer_maintenance extends core_renderer {

    /**
     * Initialises the renderer instance.
     *
     * @param moodle_page $page
     * @param string $target
     * @throws coding_exception
     */
    public function __construct(moodle_page $page, $target) {
        if ($target !== RENDERER_TARGET_MAINTENANCE || $page->pagelayout !== 'maintenance') {
            throw new coding_exception('Invalid request for the maintenance renderer.');
        }
        parent::__construct($page, $target);
    }
}
