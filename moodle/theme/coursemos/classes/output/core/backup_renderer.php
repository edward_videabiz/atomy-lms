<?php
namespace theme_coursemos\output\core;

use html_writer;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/backup/util/ui/renderer.php');

class backup_renderer extends \core_backup_renderer {
    /**
     * Creates a detailed pairing (key + value)
     *
     * @staticvar int $count
     * @param string $label
     * @param string $value
     * @return string
     */
    protected function backup_detail_pair($label, $value) {
        static $count = 0;
        $count ++;
        $html  = html_writer::start_tag('div', array('class' => 'detail-pair row form-horizontal'));
        $html .= html_writer::tag('label', $label, array('class' => 'detail-pair-label control-label col-sm-3', 'for' => 'detail-pair-value-'.$count));
        if (! \local_ubion\base\Str::startsWith($value, '<') || \local_ubion\base\Str::startsWith($value, '<i')) {
            $value = '<div class="form-control-plaintext">'.$value.'</div>';
        }
        $html .= html_writer::tag('div', $value, array('class' => 'detail-pair-value col-sm-9', 'name' => 'detail-pair-value-'.$count));
        $html .= html_writer::end_tag('div');
        return $html;
    }
}