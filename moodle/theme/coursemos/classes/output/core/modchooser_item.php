<?php
namespace theme_coursemos\output\core;
defined('MOODLE_INTERNAL') || die();

use context;
use lang_string;
use pix_icon;

/**
 * The modchooser_item renderable class.
 *
 * @package    core_course
 * @copyright  2016 Frédéric Massart - FMCorz.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class modchooser_item extends \core\output\chooser_item {

    /**
     * Constructor.
     *
     * @param stdClass $module The module.
     * @param context $context The relevant context.
     */
    public function __construct($module, context $context) {
        global $PAGE;
        
        // The property 'name' may contain more than just the module, in which case we need to extract the true module name.
        $modulename = $module->name;
        if ($colon = strpos($modulename, ':')) {
            $modulename = substr($modulename, 0, $colon);
        }
        // $icon = new pix_icon('icon', '', $modulename, ['class' => 'icon']);
        $icon = new pix_icon('course_format/mod_icon/'.$modulename, '', 'theme_'.$PAGE->theme->name);
        $help = isset($module->help) ? $module->help : new lang_string('nohelpforactivityorresource', 'moodle');

        parent::__construct($module->name, $module->title, $module->link->out(false), $icon, $help, $context);
    }

}
