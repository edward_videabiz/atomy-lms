<?php
namespace theme_coursemos\output\core;
defined('MOODLE_INTERNAL') || die();

use core\output\chooser;
use core\output\chooser_section;
use context_course;
use lang_string;
use moodle_url;
use stdClass;
use renderer_base;

class modchooser extends chooser {
    /**
     * Constructor.
     *
     * @param stdClass $course The course.
     * @param stdClass[] $modules The modules.
     */
    public function __construct(stdClass $course, array $modules) {
        $this->course = $course;
    
        $sections = [];
        $context = context_course::instance($course->id);
        
        // 강의자료 | 학습활동 
        // 형태로 출력되도록 강의자료가 먼저 출력되도록 변경함.

        // Resources
        $resources = array_filter($modules, function($mod) {
            return ($mod->archetype === MOD_ARCHETYPE_RESOURCE);
        });
        
        if($activityOrder = $this->getSortorderModules('resources_sortorder', $resources)) {
            $sections[] = new chooser_section('resources', new lang_string('resources'),
                array_map(function($module) use ($context) {
                    return new modchooser_item($module, $context);
                }, $activityOrder)
            );
        }


        // Activities.
        $activities = array_filter($modules, function($mod) {
            return ($mod->archetype !== MOD_ARCHETYPE_RESOURCE && $mod->archetype !== MOD_ARCHETYPE_SYSTEM);
        });
        
        
        if($activityOrder = $this->getSortorderModules('activities_sortorder', $activities)) {
            $sections[] = new chooser_section('activities', new lang_string('activities'),
                array_map(function($module) use ($context) {
                    return new modchooser_item($module, $context);
                }, $activityOrder)
            );
        }
    
    
        $actionurl = new moodle_url('/course/jumpto.php');
        $title = new lang_string('addresourceoractivity');
        parent::__construct($actionurl, $title, $sections, 'jumplink');

        $this->set_instructions(new lang_string('selectmoduletoviewhelp'));
        $this->add_param('course', $course->id);
    }
    

    /**
     * Export for template.
     *
     * @param renderer_base  The renderer.
     * @return stdClass
     */
    public function export_for_template(renderer_base $output) {
        $data = parent::export_for_template($output);
        $data->courseid = $this->course->id;
        return $data;
    }
    
    
    /**
     * 모듈 출력순서가 지정된 경우 지정된 순서대로 배열이 리턴됩니다.
     * 모듈 출력순서가 지정되지 않았으면 무들 기본 순서에 따릅니다. 
     * 
     * @param string $configname
     * @param array $activities
     * @return unknown|unknown[]
     */
    protected function getSortorderModules($configname, array $activities)
    {
        $activityOrder = array();
        $activityCount = count($activities);
        if ($activityCount > 0) {
            // 출력하고 싶은 순서가 저장되어 있는지 확인
            $moduleSortorder = get_config('theme_coursemos', $configname);
        
            // 따로 설정된 값이 없으면 무들 기본 방식으로 출력
            if(empty($moduleSortorder)) {
                $activityOrder = $activities;
            } else {
                $activityOrder = array();
        
                // 출력순서를 가져와서 순서대로 반복
                $explodeSortorder = explode(',', $moduleSortorder);
                foreach ($explodeSortorder as $es) {
                    // 공백이 있을수도 있음.
                    $es = trim($es);
        
                    if(isset($activities[$es])) {
                        $activityOrder[$es] = $activities[$es];
        
                        unset($activities[$es]);
                    }
                }
        
                if(!empty($activities)) {
                    foreach($activities as $modname => $modinfo) {
                        $activityOrder[$modname] = $modinfo;
                    }
                }
            }
        }
        
        return $activityOrder;
    }
}