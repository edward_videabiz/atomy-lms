<?php
namespace theme_coursemos\output\core;

defined('MOODLE_INTERNAL') || die();

use html_writer;
use action_link;
use stdClass;
use moodle_url;
use action_menu;
use action_menu_link;
use url_select;
use context_course;
use pix_icon;
use moodle_page;
use cm_info;
use completion_info;
use core_text;
require_once ($CFG->dirroot . '/course/renderer.php');

class course_renderer extends \core_course_renderer
{

    protected $isUBFormat = false;

    // $page->course는 mdl_course 테이블 정보만 담겨져있기 때문에 course_format_options 항목까지 담겨져있는 객체가 필요할 때가 있음
    protected $course = null;

    /**
     * Override the constructor so that we can initialise the string cache
     *
     * @param moodle_page $page
     * @param string $target
     */
    public function __construct(moodle_page $page, $target)
    {
        parent::__construct($page, $target);

        if (isset($page->course->id) && $page->course->id > SITEID) {
            if (strpos($page->course->format, 'ubs') !== false) {
                $this->isUBFormat = true;
            }
        }
    }

    /**
     * Renders HTML for displaying the sequence of course module editing buttons
     *
     * @see course_get_cm_edit_actions()
     *
     * @param action_link[] $actions
     *            Array of action_link objects
     * @param cm_info $mod
     *            The module we are displaying actions for.
     * @param array $displayoptions
     *            additional display options:
     *            ownerselector => A JS/CSS selector that can be used to find an cm node.
     *            If specified the owning node will be given the class 'action-menu-shown' when the action
     *            menu is being displayed.
     *            constraintselector => A JS/CSS selector that can be used to find the parent node for which to constrain
     *            the action menu to when it is being displayed.
     *            donotenhance => If set to true the action menu that gets displayed won't be enhanced by JS.
     * @return string
     */
    public function course_section_cm_edit_actions($actions, cm_info $mod = null, $displayoptions = array())
    {
        global $CFG;

        if (empty($actions)) {
            return '';
        }

        if (isset($displayoptions['ownerselector'])) {
            $ownerselector = $displayoptions['ownerselector'];
        } else if ($mod) {
            $ownerselector = '#module-' . $mod->id;
        } else {
            debugging('You should upgrade your call to ' . __FUNCTION__ . ' and provide $mod', DEBUG_DEVELOPER);
            $ownerselector = 'li.activity';
        }

        if (isset($displayoptions['constraintselector'])) {
            $constraint = $displayoptions['constraintselector'];
        } else {
            $constraint = '.course-content';
        }

        $menu = new action_menu();
        $menu->set_owner_selector($ownerselector);
        $menu->set_constraint($constraint);
        $menu->set_alignment(action_menu::TR, action_menu::BR);
        $menu->set_menu_trigger($this->output->pix_icon('t/edit', get_string('edit'))); // "수정" 글자보다는 아이콘으로 출력하는게 보기 좋음.

        foreach ($actions as $action) {
            if ($action instanceof action_menu_link) {
                $action->add_class('cm-edit-action');
            }
            $menu->add($action);
        }
        $menu->attributes['class'] .= ' section-cm-edit-actions commands';

        // Prioritise the menu ahead of all other actions.
        $menu->prioritise = true;

        return $this->render($menu);
    }

    /**
     * Renders HTML to display a list of course modules in a course section
     * Also displays "move here" controls in Javascript-disabled mode
     *
     * This function calls {@link core_course_renderer::course_section_cm()}
     *
     * @param stdClass $course
     *            course object
     * @param int|stdClass|\section_info $section
     *            relative section number or section object
     * @param int $sectionreturn
     *            section number to return to
     * @param int $displayoptions
     * @return void
     */
    public function course_section_cm_list($course, $section, $sectionreturn = null, $displayoptions = array())
    {
        global $USER;

        // 무들에서 제공하는 강좌 포맷에서는 기본 형태 그대로 출력되어야 함.
        if (! $this->isUBFormat) {
            return parent::course_section_cm_list($course, $section, $sectionreturn, $displayoptions);
        }

        $output = '';
        $modinfo = get_fast_modinfo($course);
        if (is_object($section)) {
            $section = $modinfo->get_section_info($section->section);
        } else {
            $section = $modinfo->get_section_info($section);
        }
        $completioninfo = new completion_info($course);

        // check if we are currently in the process of moving a module with JavaScript disabled
        $ismoving = $this->page->user_is_editing() && ismoving($course->id);
        if ($ismoving) {
            $movingpix = new pix_icon('movehere', get_string('movehere'), 'moodle', array(
                'class' => 'movetarget'
            ));
            $strmovefull = strip_tags(get_string("movefull", "", "'$USER->activitycopyname'"));
        }

        $moduleshtml = array();
        $exclude_modname = array(
            'label'
        );
        $exclude_modulehtml = array();

        if (! empty($modinfo->sections[$section->section])) {

            // 0번째 주차에 대해서는 아이콘화 할수 있는 mod 밑에 출력이 되도록 해야됨.
            if ($section->section == 0) {
                foreach ($modinfo->sections[$section->section] as $modnumber) {
                    $mod = $modinfo->cms[$modnumber];

                    if ($ismoving and $mod->id == $USER->activitycopy) {
                        // do not display moving mod
                        continue;
                    }

                    if ($modulehtml = $this->course_section_cm_list_item($course, $completioninfo, $mod, $sectionreturn, $displayoptions)) {
                        if (in_array($mod->modname, $exclude_modname)) {
                            $exclude_modulehtml[$modnumber] = $modulehtml;
                        } else {
                            $moduleshtml[$modnumber] = $modulehtml;
                        }
                    }
                }
            } else {
                foreach ($modinfo->sections[$section->section] as $modnumber) {
                    $mod = $modinfo->cms[$modnumber];

                    if ($ismoving and $mod->id == $USER->activitycopy) {
                        // do not display moving mod
                        continue;
                    }

                    if ($modulehtml = $this->course_section_cm_list_item($course, $completioninfo, $mod, $sectionreturn, $displayoptions)) {
                        $moduleshtml[$modnumber] = $modulehtml;
                    }
                }
            }
        }

        // $exclude_modname 이외의 학습자원/활동에 대해서는 정상적으로 출력
        if (! empty($moduleshtml) || $ismoving) {
            $sectionoutput = '';
            foreach ($moduleshtml as $modnumber => $modulehtml) {
                if ($ismoving) {
                    $movingurl = new moodle_url('/course/mod.php', array(
                        'moveto' => $modnumber,
                        'sesskey' => sesskey()
                    ));
                    $sectionoutput .= html_writer::tag('li', html_writer::link($movingurl, $this->output->render($movingpix), array(
                        'title' => $strmovefull
                    )), array(
                        'class' => 'movehere'
                    ));
                }

                $sectionoutput .= $modulehtml;
            }

            if ($ismoving) {
                $movingurl = new moodle_url('/course/mod.php', array(
                    'movetosection' => $section->id,
                    'sesskey' => sesskey()
                ));
                $sectionoutput .= html_writer::tag('li', html_writer::link($movingurl, $this->output->render($movingpix), array(
                    'title' => $strmovefull
                )), array(
                    'class' => 'movehere'
                ));
            }

            // Always output the section module list.
            $output .= html_writer::tag('ul', $sectionoutput, array(
                'class' => 'section img-text'
            ));
        }

        // 2016-09-28 이대 요청으로 인해 exclude_modulehtml에 대해서 하단으로 위치 변경
        // 0번째 주차에 대해서 아이콘화 할수 없는 학습자원/활동에 대해서 무들 기본 방식으로 출력
        if ($section->section == 0 && $exclude_modulehtml) {
            $output .= '<hr class="exclude_hr" />';

            $output .= html_writer::start_tag('ul', array(
                'class' => 'exclude_mod'
            ));
            foreach ($exclude_modulehtml as $modnumber => $modulehtml) {
                $output .= $modulehtml;
            }

            // Always output the section module list.
            $output .= html_writer::end_tag('ul'); // .section
        }

        return $output;
    }

    /**
     * Renders html to display a name with the link to the course module on a course page
     *
     * If module is unavailable for user but still needs to be displayed
     * in the list, just the name is returned without a link
     *
     * Note, that for course modules that never have separate pages (i.e. labels)
     * this function return an empty string
     *
     * @param cm_info $mod
     * @param array $displayoptions
     * @return string
     */
    public function course_section_cm_name_title(cm_info $mod, $displayoptions = array())
    {

        // 무들에서 제공하는 강좌 포맷에서는 기본 형태 그대로 출력되어야 함.
        if (! $this->isUBFormat) {
            return parent::course_section_cm_name_title($mod, $displayoptions);
        }

        $output = '';
        if (! $mod->uservisible && empty($mod->availableinfo)) {
            // Nothing to be displayed to the user.
            return $output;
        }
        $url = $mod->url;
        if (! $url) {
            return $output;
        }

        // Accessibility: for files get description via icon, this is very ugly hack!
        $instancename = $mod->get_formatted_name();
        $altname = $mod->modfullname;
        // Avoid unnecessary duplication: if e.g. a forum name already
        // includes the word forum (or Forum, etc) then it is unhelpful
        // to include that in the accessible description that is added.
        if (false !== strpos(core_text::strtolower($instancename), core_text::strtolower($altname))) {
            $altname = '';
        }
        // File type after name, for alphabetic lists (screen reader).
        if ($altname) {
            $altname = get_accesshide(' ' . $altname);
        }

        // For items which are hidden but available to current user
        // ($mod->uservisible), we show those as dimmed only if the user has
        // viewhiddenactivities, so that teachers see 'items which might not
        // be available to some students' dimmed but students do not see 'item
        // which is actually available to current student' dimmed.
        $linkclasses = '';
        $accesstext = '';
        $textclasses = '';
        if ($mod->uservisible) {
            $conditionalhidden = $this->is_cm_conditionally_hidden($mod);
            $accessiblebutdim = (! $mod->visible || $conditionalhidden) && has_capability('moodle/course:viewhiddenactivities', $mod->context);
            if ($accessiblebutdim) {
                $linkclasses .= ' dimmed';
                $textclasses .= ' dimmed_text';
                if ($conditionalhidden) {
                    $linkclasses .= ' conditionalhidden';
                    $textclasses .= ' conditionalhidden';
                }
                // Show accessibility note only if user can access the module himself.
                $accesstext = get_accesshide(get_string('hiddenfromstudents') . ':' . $mod->modfullname);
            }
        } else {
            $linkclasses .= ' dimmed';
            $textclasses .= ' dimmed_text';
        }

        // Get on-click attribute value if specified and decode the onclick - it
        // has already been encoded for display (puke).
        $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);

        $groupinglabel = $mod->get_grouping_label($textclasses);

        // Display link itself.
        return $this->course_section_cm_name_output($mod, $instancename, $altname, $url, $onclick, $groupinglabel, $linkclasses, $accesstext, $textclasses);
    }

    /**
     * 강좌 포맷 마다 학습자원/활동이 출력되는 형태가 달라지기 때문에 별도 함수를 따로 구성함.
     *
     * @param cm_info $mod
     * @param string $instancename
     * @param string $altname
     * @param string $url
     * @param string $onclick
     * @param string $groupinglabel
     * @param string $linkclasses
     * @param string $accesstext
     * @param string $textclasses
     * @return string
     */
    protected function course_section_cm_name_output(cm_info $mod, $instancename, $altname, $url, $onclick, $groupinglabel, $linkclasses, $accesstext, $textclasses)
    {
        global $CFG, $DB;

        $output = '';

        if (empty($this->course)) {
            $this->course = \local_ubion\course\Course::getInstance()->getCourse($mod->course);
        }

        $newIcon = ''; // 마지막 접속 시간을 기준으로 신규로 학습자원/활동이 추가가되었으면 new icon을 표시해줘야됨.

        // if(local_ubion_course_is_new_activity($mod->course, $mod->sectionnum, $mod->id)) {
        // $newIcon = '&nbsp;'.$this->output->pix_icon('icons/new', 'new', 'theme_'.$this->page->theme->name, array('class'=>'new_modules'));
        // }

        // course_section_cm_name_output 함수 자체가 ubs로 시작되는 강좌 포맷만 접근하기 때문에
        // UI 관련 const는 무조건 존재함.
        // 만약 이곳에서 에러가 발생된다면 UI 관련 const를 추가해주시면 됩니다.
        $className = 'format_' . $this->course->format;
        $ui = $this->course->ui ?? $className::UI_LIST;

        if ($ui == $className::UI_METRO) {

            // 활동자원/모듈 이미지
            // modname이 resource이면 해당 파일 확장자를 찾아서 표시해줘야됨.
            if ($mod->modname == 'resource' || $mod->modname == 'ubfile') {
                // 파일아이콘이 -로 구분되는일은 없기 때문에 별도 예외처리 없음.
                $modIcon = current(explode('-', $mod->icon));
                $modIcon = $this->output->image_url('metro_icon/' . $modIcon . '-64', 'theme_couresmos');
            } else {

                $modIcon = $mod->modname;

                // 게시판이면 분기 처리해서 아이콘을 변경해야됨.
                if ($mod->modname == 'ubboard') {
                    $query = "SELECT type FROM {ubboard} WHERE id = :id";
                    $param = [
                        'id' => $mod->instance
                    ];

                    // ubboard 테이블에서 게시판 type값을 알아와야됨.
                    if ($ubboardType = $DB->get_field_sql($query, $param)) {
                        $ubboardIcon = $mod->modname . '_' . $ubboardType;

                        // 게시판 타입에 맞는 이미지 파일이 없으면 모듈명 이미지로 출력 해야됨.
                        if (file_exists($CFG->dirroot . '/theme/coursemos/pix/course_format/metro_icon/' . $ubboardIcon . '.png')) {
                            $modIcon = $ubboardIcon;
                        }
                    }
                }

                $modIcon = $this->output->image_url('course_format/metro_icon/' . $modIcon, 'theme_coursemos');
            }

            $activitylink = '<div class="activity-image">';
            $activitylink .= '<img src="' . $modIcon . '" alt="' . $mod->modname . '" />';
            $activitylink .= '</div>';
            $activitylink .= '<div class="activity-title">';
            $activitylink .= $accesstext . '<span class="instancename">' . $instancename . $altname . $newIcon . '</span>';
            $activitylink .= '</div>';
        } else {
            // local_ubion_course_renderer를 상속 받은 강좌 포맷은 유비온에서 생성한 강좌 포맷으로 판단해도 무관함.
            $isSection0 = ($mod->sectionnum == 0) ? true : false;

            // 학습자원/활동 아이콘
            $modIcon = $mod->get_icon_url();

            // 0주차(강좌개요) 인 경우에는 큰 아이콘으로 표시해줘야됨.
            if ($isSection0) {
                if ($mod->modname == 'resource' || $mod->modname == 'ubfile') {
                    // 파일아이콘이 -로 구분되는일은 없기 때문에 별도 예외처리 없음.
                    $modIcon = current(explode('-', $mod->icon));
                    $modIcon = $this->output->image_url($modIcon . '-64');
                } else {
                    $imageName = $mod->modname;

                    // 게시판이면 분기 처리해서 아이콘을 변경해야됨.
                    if ($mod->modname == 'ubboard') {
                        // ubboard 테이블에서 게시판 type값을 알아와야됨.
                        if ($ubboard_type = $DB->get_field_sql('SELECT type FROM {ubboard} WHERE id = :id', array(
                            'id' => $mod->instance
                        ))) {
                            $imageName = $mod->modname . '_' . $ubboard_type;

                            // 게시판 type에 대한 이미지 파일이 없으면 기본 아이콘(게시판)로 출력 해야됨.
                            if (! file_exists($CFG->dirroot . '/theme/' . $this->page->theme->name . '/pix/course_format/mod_icon/' . $imageName . '.png')) {
                                $imageName = $mod->modname;
                            }
                        }
                    }

                    // resource 이외항목들은 해당 코스 포맷에서 아이콘을 찾아야됨.
                    $modIcon = $this->output->image_url('course_format/mod_icon/' . $imageName, 'theme_' . $this->page->theme->name);
                }
            }

            $activitylink = '<img src="' . $modIcon . '" alt="' . $mod->modfullname . '" class="activityicon" />';
            $activitylink .= $accesstext . html_writer::tag('span', $instancename . $altname . $newIcon, array(
                'class' => 'instancename'
            ));
        }

        $linkclasses .= ' activity-link';
        if ($mod->uservisible) {
            $output = html_writer::link($url, $activitylink, array(
                'class' => $linkclasses,
                'onclick' => $onclick
            )) . $groupinglabel;
        } else {
            // We may be displaying this just in order to show information
            // about visibility, without the actual link ($mod->uservisible)
            $output .= html_writer::tag('div', $activitylink, array(
                'class' => $textclasses
            )) . $groupinglabel;
        }

        return $output;
    }

    /**
     * Build the HTML for the module chooser javascript popup
     *
     * @param array $modules
     *            A set of modules as returned form @see
     *            get_module_metadata
     * @param object $course
     *            The course that will be displayed
     * @return string The composed HTML for the module
     */
    public function course_modchooser($modules, $course)
    {
        if (! $this->page->requires->should_create_one_time_item_now('core_course_modchooser')) {
            return '';
        }
        // $modchooser = new \core_course\output\modchooser($course, $modules);
        // return $this->render($modchooser);

        // 모듈 출력 순서를 재정하기 위해서 테마단에서 재정의합니다.
        $modchooser = new modchooser($course, $modules);
        return $this->render($modchooser);
    }

    /**
     * Render a modchooser.
     *
     * @param \renderable $modchooser
     *            The chooser.
     * @return string
     */
    public function render_modchooser(\renderable $modchooser)
    {
        return $this->render_from_template('theme_coursemos/course_modchooser', $modchooser->export_for_template($this));
    }

    /**
     * Renders HTML for the menus to add activities and resources to the current course
     *
     * @param stdClass $course
     * @param int $section
     *            relative section number (field course_sections.section)
     * @param int $sectionreturn
     *            The section to link back to
     * @param array $displayoptions
     *            additional display options, for example blocks add
     *            option 'inblock' => true, suggesting to display controls vertically
     * @return string
     */
    function course_section_add_cm_control($course, $section, $sectionreturn = null, $displayoptions = array())
    {
        global $CFG;

        // 무들에서 제공하는 강좌 포맷에서는 기본 형태 그대로 출력되어야 함.
        if (! $this->isUBFormat) {
            return parent::course_section_add_cm_control($course, $section, $sectionreturn, $displayoptions);
        }

        $vertical = ! empty($displayoptions['inblock']);

        // check to see if user can add menus and there are modules to add
        if (! has_capability('moodle/course:manageactivities', context_course::instance($course->id)) || ! $this->page->user_is_editing() || ! ($modnames = get_module_types_names()) || empty($modnames)) {
            return '';
        }

        $modules = get_module_metadata($course, $modnames, $sectionreturn);

        // 등록 가능한 학습자원/활동
        $activities = \theme_coursemos\Html::getInstance()->getMods($course, $modnames, $modules, $section, $sectionreturn);

        $straddactivity = get_string('addactivity');
        $straddresource = get_string('addresource');
        $sectionname = get_section_name($course, $section);
        $strresourcelabel = get_string('addresourcetosection', null, $sectionname);
        $stractivitylabel = get_string('addactivitytosection', null, $sectionname);

        $output = html_writer::start_tag('div', array(
            'class' => 'section_add_menus',
            'id' => 'add_menus-section-' . $section
        ));

        if (! $vertical) {
            $output .= html_writer::start_tag('div', array(
                'class' => 'horizontal'
            ));
        }

        if (! empty($activities[MOD_CLASS_RESOURCE])) {
            $select = new url_select($activities[MOD_CLASS_RESOURCE], '', array(
                '' => $straddresource
            ), "ressection$section");
            $select->set_help_icon('resources');
            $select->set_label($strresourcelabel, array(
                'class' => 'accesshide'
            ));
            $output .= $this->output->render($select);
        }

        if (! empty($activities[MOD_CLASS_ACTIVITY])) {
            $select = new url_select($activities[MOD_CLASS_ACTIVITY], '', array(
                '' => $straddactivity
            ), "section$section");
            $select->set_help_icon('activities');
            $select->set_label($stractivitylabel, array(
                'class' => 'accesshide'
            ));
            $output .= $this->output->render($select);
        }

        if (! $vertical) {
            $output .= html_writer::end_tag('div');
        }

        $output .= html_writer::end_tag('div');

        // 이부분만 수정함.. 원 소스와 동일하므로 무들 업그레이드시 이 영역만 따로 관리하시면 됩니다.
        if (course_ajax_enabled($course) && $course->id == $this->page->course->id) {
            // 빠른 모듈 추가 사용여부
            $useFavoriteMod = get_config('theme_coursemos', 'is_favorite_mod');
            $favorite_class = ($useFavoriteMod) ? 'usefavorite' : '';

            $modchooser = '<div class="favorite_modchooser ' . $favorite_class . '">';

            // 빠른 모듈 추가를 사용하는 경우
            if ($useFavoriteMod) {

                $configFavoriteMods = get_config('theme_coursemos', 'favorite_mod');
                $favoriteMods = array();

                // ,로 구분되어 있음.
                if (! empty($configFavoriteMods)) {
                    $explodeFavorite = explode(',', $configFavoriteMods);
                    $explodeFavoriteCount = count($explodeFavorite);

                    if ($explodeFavoriteCount == 4) {
                        foreach ($explodeFavorite as $ef) {
                            $favoriteMods[] = trim($ef);
                        }
                    }
                }

                // 설정된 값이 없으면, 정상적인 규칙으로 저장되지 않은 상태이므로 기본값이 설정되어야 함.
                if (empty($favoriteMods)) {
                    $favoriteMods = array(
                        'label',
                        'assign',
                        'resource',
                        'quiz'
                    );
                }

                $modchooser .= '<ul>';
                foreach ($favoriteMods as $fs) {
                    $modchooser .= '<li>';
                    $modchooser .= $this->getAddActivityHtml($fs, $course->id, $section);
                    $modchooser .= '</li>';
                }
                $modchooser .= '</ul>';
            }

            $addresourceoractivity = get_string('addresourceoractivity', 'theme_coursemos');
            $modchooser .= '<div class="modchooser-link">';
            $modchooser .= '<span class="section-modchooser-link"><span class="instancename btn btn-default" title="' . $addresourceoractivity . '">';
            $modchooser .= $addresourceoractivity;
            $modchooser .= '</span></span>';
            $modchooser .= '</div>';
            $modchooser .= '</div>';

            // Wrap the normal output in a noscript div
            $usemodchooser = get_user_preferences('usemodchooser', $CFG->modchooserdefault);
            if ($usemodchooser) {
                $output = html_writer::tag('div', $output, array(
                    'class' => 'hiddenifjs addresourcedropdown'
                ));
                $modchooser = html_writer::tag('div', $modchooser, array(
                    'class' => 'visibleifjs addresourcemodchooser'
                ));
            } else {
                // If the module chooser is disabled, we need to ensure that the dropdowns are shown even if javascript is disabled
                $output = html_writer::tag('div', $output, array(
                    'class' => 'show addresourcedropdown'
                ));
                $modchooser = html_writer::tag('div', $modchooser, array(
                    'class' => 'hide addresourcemodchooser'
                ));
            }
            $output = $this->course_modchooser($modules, $course) . $modchooser . $output;
        }

        return $output;
    }

    /**
     * 빠른 학습자원/활동 모듈 추가 html 리턴
     *
     * @param string $modname
     * @param int $courseid
     * @param int $sectionnum
     * @return string
     */
    public function getAddActivityHtml($modname, $courseid, $sectionnum)
    {
        global $CFG;

        $str_modname = get_string('pluginname', $modname);

        $modchooser = '<a href="' . $CFG->wwwroot . '/course/modedit.php?add=' . $modname . '&course=' . $courseid . '&section=' . $sectionnum . '" title="' . $str_modname . '">';
        $modchooser .= '<img src="' . $this->output->image_url('icon', $modname) . '" alt="' . $str_modname . '" />';
        $modchooser .= '<div class="modulename">' . $str_modname . '</div>';
        $modchooser .= '</a>';

        return $modchooser;
    }
}