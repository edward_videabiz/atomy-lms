<?php
namespace theme_coursemos\output;

use context_header;
use html_writer;
use action_menu;
use navigation_node;
use action_link;
use moodle_url;
use help_icon;
use single_button;
use block_contents;
use stdClass;
use tabtree;
use tabobject;
use paging_bar;
use preferences_groups;
use coding_exception;
defined('MOODLE_INTERNAL') || die();

/**
 * coursemos core renderers.
 *
 * @package theme_coursemos
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class core_renderer extends \core_renderer
{

    /*
     * This renders the navbar.
     * Uses bootstrap compatible html.
     */
    public function navbar()
    {
        global $OUTPUT, $CFG;

        $items = $this->page->navbar->get_items();
        if (empty($items)) { // MDL-46107.
            return '';
        }

        $breadcrumbs = '<ol class=breadcrumb>';

        // 첫번째 항목은 icon 형태로 출력되어야 함.
        // 첫번째 항목은 text가 아닌 이미지로 표시되어야 함.
        // 기존 항목 삭제후 이미지 링크 제공
        unset($items[0]);
        /* 애터미에서는 홈 버튼이 필요하지 않음
        $breadcrumbs .= '<li class="breadcrumb-item breadcrumb-home">';
        $breadcrumbs .= '<a href="' . $CFG->wwwroot . '"><img src="' . $OUTPUT->image_url('layout/course-home', 'theme_coursemos') . '" alt="HOME" class="icon-home" /></a>';
        $breadcrumbs .= '</li>';
        */

        foreach ($items as $item) {
            $item->hideicon = true;
            if (! empty($item->action)) {
                $breadcrumbs .= '<li class="breadcrumb-item">' . $this->render($item) . '</li>';
            }
        }

        $breadcrumbs .= '</ol>';

        return $breadcrumbs;
    }

    /**
     * Renders a breadcrumb navigation node object.
     *
     * @param \breadcrumb_navigation_node $item
     *            The navigation node to render.
     * @return string HTML fragment
     */
    protected function render_breadcrumb_navigation_node(\breadcrumb_navigation_node $item)
    {
        if ($item->action instanceof moodle_url) {
            $content = $item->get_content();
            $title = $item->get_title();
            $attributes = array();
            $attributes['itemprop'] = 'url';

            /*
             * title attribute는 모듈이름이 아닌 등록된 모듈명이여야함.
             * if ($title !== '') {
             * $attributes['title'] = $title;
             * }
             */
            $attributes['title'] = $content;

            if ($item->hidden) {
                $attributes['class'] = 'dimmed_text';
            }
            $content = html_writer::tag('span', $content, array(
                'itemprop' => 'title'
            ));
            $content = html_writer::link($item->action, $content, $attributes);

            $attributes = array();
            $attributes['itemscope'] = '';
            $attributes['itemtype'] = 'http://data-vocabulary.org/Breadcrumb';
            $content = html_writer::tag('span', $content, $attributes);
        } else {
            $content = $this->render_navigation_node($item);
        }
        return $content;
    }

    /**
     * This code renders the navbar button to control the display of the custom menu
     * on smaller screens.
     *
     * Do not display the button if the menu is empty.
     *
     * @return string HTML fragment
     */
    public function navbar_button()
    {
        global $CFG;

        if (empty($CFG->custommenuitems) && $this->lang_menu() == '') {
            return '';
        }

        $iconbar = html_writer::tag('span', '', array(
            'class' => 'icon-bar'
        ));
        $button = html_writer::tag('a', $iconbar . "\n" . $iconbar . "\n" . $iconbar, array(
            'class' => 'btn btn-navbar',
            'data-toggle' => 'collapse',
            'data-target' => '.nav-collapse'
        ));
        return $button;
    }

    /**
     * Renders tabtree
     *
     * @param tabtree $tabtree
     * @return string
     */
    protected function render_tabtree(tabtree $tabtree)
    {
        if (empty($tabtree->subtree)) {
            return '';
        }

        $data = $tabtree->export_for_template($this);
        return $this->render_from_template('core/tabtree', $data);
    }

    /**
     * Renders tabobject (part of tabtree)
     *
     * This function is called from {@link core_renderer::render_tabtree()}
     * and also it calls itself when printing the $tabobject subtree recursively.
     *
     * @param tabobject $tabobject
     * @return string HTML fragment
     */
    protected function render_tabobject(tabobject $tab)
    {
        throw new coding_exception('Tab objects should not be directly rendered.');
    }

    /**
     * Prints a nice side block with an optional header.
     *
     * @param block_contents $bc
     *            HTML for the content
     * @param string $region
     *            the region the block is appearing in.
     * @return string the HTML to be output.
     */
    public function block(block_contents $bc, $region)
    {
        $bc = clone ($bc); // Avoid messing up the object passed in.
        if (empty($bc->blockinstanceid) || ! strip_tags($bc->title)) {
            $bc->collapsible = block_contents::NOT_HIDEABLE;
        }

        $id = ! empty($bc->attributes['id']) ? $bc->attributes['id'] : uniqid('block-');
        $context = new stdClass();
        $context->skipid = $bc->skipid;
        $context->blockinstanceid = $bc->blockinstanceid;
        $context->dockable = $bc->dockable;
        $context->id = $id;
        $context->hidden = $bc->collapsible == block_contents::HIDDEN;
        $context->skiptitle = strip_tags($bc->title);
        $context->showskiplink = ! empty($context->skiptitle);
        $context->arialabel = $bc->arialabel;
        $context->ariarole = ! empty($bc->attributes['role']) ? $bc->attributes['role'] : 'complementary';
        $context->type = $bc->attributes['data-block'];
        $context->title = $bc->title;
        $context->content = $bc->content;
        $context->annotation = $bc->annotation;
        $context->footer = $bc->footer;
        $context->hascontrols = ! empty($bc->controls);
        if ($context->hascontrols) {
            $context->controls = $this->block_controls($bc->controls, $id);
        }

        return $this->render_from_template('core/block', $context);
    }

    /**
     * Renders preferences groups.
     *
     * @param preferences_groups $renderable
     *            The renderable
     * @return string The output.
     */
    public function render_preferences_groups(preferences_groups $renderable)
    {
        return $this->render_from_template('core/preferences_groups', $renderable);
    }

    /**
     * Renders an action menu component.
     *
     * @param action_menu $menu
     * @return string HTML
     */
    public function render_action_menu(action_menu $menu)
    {

        // We don't want the class icon there!
        foreach ($menu->get_secondary_actions() as $action) {
            if ($action instanceof \action_menu_link && $action->has_class('icon')) {
                $action->attributes['class'] = preg_replace('/(^|\s+)icon(\s+|$)/i', '', $action->attributes['class']);
            }
        }

        if ($menu->is_empty()) {
            return '';
        }
        $context = $menu->export_for_template($this);

        return $this->render_from_template('core/action_menu', $context);
    }

    /**
     * Implementation of user image rendering.
     *
     * @param help_icon $helpicon
     *            A help icon instance
     * @return string HTML fragment
     */
    protected function render_help_icon(help_icon $helpicon)
    {
        $context = $helpicon->export_for_template($this);
        return $this->render_from_template('core/help_icon', $context);
    }

    /**
     * Renders a single button widget.
     *
     * This will return HTML to display a form containing a single button.
     *
     * @param single_button $button
     * @return string HTML fragment
     */
    protected function render_single_button(single_button $button)
    {
        return $this->render_from_template('core/single_button', $button->export_for_template($this));
    }

    /**
     * Renders a paging bar.
     *
     * @param paging_bar $pagingbar
     *            The object.
     * @return string HTML
     */
    protected function render_paging_bar(paging_bar $pagingbar)
    {
        // Any more than 10 is not usable and causes wierd wrapping of the pagination in this theme.
        $pagingbar->maxdisplay = 10;
        return $this->render_from_template('core/paging_bar', $pagingbar->export_for_template($this));
    }

    /**
     * Take a node in the nav tree and make an action menu out of it.
     * The links are injected in the action menu.
     *
     * @param action_menu $menu
     * @param navigation_node $node
     * @param boolean $indent
     * @param boolean $onlytopleafnodes
     * @return boolean nodesskipped - True if nodes were skipped in building the menu
     */
    private function build_action_menu_from_navigation(action_menu $menu, navigation_node $node, $indent = false, $onlytopleafnodes = false)
    {
        $skipped = false;
        // Build an action menu based on the visible nodes from this navigation tree.
        foreach ($node->children as $menuitem) {
            if ($menuitem->display) {
                if ($onlytopleafnodes && $menuitem->children->count()) {
                    $skipped = true;
                    continue;
                }
                if ($menuitem->action) {
                    if ($menuitem->action instanceof action_link) {
                        $link = $menuitem->action;
                        // Give preference to setting icon over action icon.
                        if (! empty($menuitem->icon)) {
                            $link->icon = $menuitem->icon;
                        }
                    } else {
                        $link = new action_link($menuitem->action, $menuitem->text, null, null, $menuitem->icon);
                    }
                } else {
                    if ($onlytopleafnodes) {
                        $skipped = true;
                        continue;
                    }
                    $link = new action_link(new moodle_url('#'), $menuitem->text, null, [
                        'disabled' => true
                    ], $menuitem->icon);
                }
                if ($indent) {
                    $link->add_class('m-l-1');
                }
                if (! empty($menuitem->classes)) {
                    $link->add_class(implode(" ", $menuitem->classes));
                }

                $menu->add_secondary_action($link);
                $skipped = $skipped || $this->build_action_menu_from_navigation($menu, $menuitem, true);
            }
        }
        return $skipped;
    }

    /**
     * Do not call this function directly.
     *
     * To terminate the current script with a fatal error, call the {@link print_error}
     * function, or throw an exception. Doing either of those things will then call this
     * function to display the error, before terminating the execution.
     *
     * @param string $message
     *            The message to output
     * @param string $moreinfourl
     *            URL where more info can be found about the error
     * @param string $link
     *            Link for the Continue button
     * @param array $backtrace
     *            The execution backtrace
     * @param string $debuginfo
     *            Debugging information
     * @return string the HTML to output.
     */
    public function fatal_error($message, $moreinfourl, $link, $backtrace, $debuginfo = null, $errorcode = "")
    {
        global $CFG;

        // 오류용 화면으로 변경
        // $PAGE->set_pagelayout('fatal');

        $output = '';
        $obbuffer = '';

        if ($this->has_started()) {
            // we can not always recover properly here, we have problems with output buffering,
            // html tables, etc.
            $output .= $this->opencontainers->pop_all_but_last();
        } else {
            // It is really bad if library code throws exception when output buffering is on,
            // because the buffered text would be printed before our start of page.
            // NOTE: this hack might be behave unexpectedly in case output buffering is enabled in PHP.ini
            error_reporting(0); // disable notices from gzip compression, etc.
            while (ob_get_level() > 0) {
                $buff = ob_get_clean();
                if ($buff === false) {
                    break;
                }
                $obbuffer .= $buff;
            }
            error_reporting($CFG->debug);

            // Output not yet started.
            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
            if (empty($_SERVER['HTTP_RANGE'])) {
                @header($protocol . ' 404 Not Found');
            } else {
                // Must stop byteserving attempts somehow,
                // this is weird but Chrome PDF viewer can be stopped only with 407!
                @header($protocol . ' 407 Proxy Authentication Required');
            }

            $this->page->set_context(null); // ugly hack - make sure page context is set to something, we do not want bogus warnings here
            $this->page->set_url('/'); // no url
                                       // $this->page->set_pagelayout('base'); //TODO: MDL-20676 blocks on error pages are weird, unfortunately it somehow detect the pagelayout from URL :-(
            $this->page->set_title(get_string('error'));
            $this->page->set_heading($this->page->course->fullname);
            $output .= $this->header();
        }

        $message = '<p class="errormessage">' . $message . '</p>' . '<p class="errorcode"><a href="' . $moreinfourl . '">' . get_string('moreinformation') . '</a></p>';

        if (empty($CFG->rolesactive)) {
            $message .= '<p class="errormessage">' . get_string('installproblem', 'error') . '</p>';
            // It is usually not possible to recover from errors triggered during installation, you may need to create a new database or use a different database prefix for new installation.
        }
        $output .= $this->box($message, 'errorbox', null, array(
            'data-rel' => 'fatalerror'
        ));

        if ($CFG->debugdeveloper) {
            if (! empty($debuginfo)) {
                $debuginfo = s($debuginfo); // removes all nasty JS
                $debuginfo = str_replace("\n", '<br />', $debuginfo); // keep newlines
                $output .= $this->notification('<strong>Debug info:</strong> ' . $debuginfo, 'notifytiny');
            }
            if (! empty($backtrace)) {
                $output .= $this->notification('<strong>Stack trace:</strong> ' . format_backtrace($backtrace), 'notifytiny');
            }
            if ($obbuffer !== '') {
                $output .= $this->notification('<strong>Output buffer:</strong> ' . s($obbuffer), 'notifytiny');
            }
        }

        if (empty($CFG->rolesactive)) {
            // continue does not make much sense if moodle is not installed yet because error is most probably not recoverable
        } else if (! empty($link)) {
            $output .= $this->continue_button($link);
        }

        $output .= $this->footer();

        // Padding to encourage IE to display our error page, rather than its own.
        $output .= str_repeat(' ', 512);

        return $output;
    }

    /**
     * Construct a user menu, returning HTML that can be echoed out by a
     * layout file.
     *
     * @param stdClass $user
     *            A user object, usually $USER.
     * @param bool $withlinks
     *            true if a dropdown should be built.
     * @return string HTML fragment.
     */
    public function user_menu($user = null, $withlinks = null)
    {
        global $USER, $CFG, $OUTPUT, $PAGE;

        if (is_null($user)) {
            $user = $USER;
        }

        $output = '<ul class="nav nav-pills" role="tablist">';
        
        $output .= '<li role="presentation" class="nav-item nav-item-lang">';
        // 언어 선택
        $currlang = current_language();
        $langs = get_string_manager()->get_list_of_translations();
        
        // lang 관련 파라메터가 존재하면 lang 파라메터를 삭제하고 아래 language쪽에서 다시 셋팅해줘야됨.
        if (isset($_GET['lang'])) {
            unset($_GET['lang']);
        }
        
        $url = $CFG->wwwroot.$_SERVER['SCRIPT_NAME'];
        if ($_GET) {
            $url .= '?' . http_build_query($_GET) . '&amp;lang=';
        } else {
            $url .= '?lang=';
        }
        
        // $langName = (isset($langs[$currlang]) ? $langs[$currlang] : $langs['en']);
        if (isset($langs[$currlang])) {
            $flag = $OUTPUT->image_url('flag/' . $currlang, 'theme_coursemos');
            $langName = '<img src="' . $flag . '" alt="" />' . '&nbsp;&nbsp;' . $langs[$currlang];
        } else {
            $flag = $OUTPUT->image_url('flag/en', 'theme_coursemos');
            $langName = '<img src="' . $flag . '" alt="" />' . '&nbsp;&nbsp;' . $langs['en'];
        }
        
        $output .= '<div class="dropdown dropdown-lang">';
        $output .=    '<a id="user-info-lang" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $langName . ' <span class="caret"></span></a>';
        
        $output .=    '<div class="dropdown-menu" aria-labelledby="user-info-lang">';
        
        $courseLang = $PAGE->course->lang ?? null;
        
        if (! empty($courseLang)) {
            $output .= '<div class="force-lang dropdown-item">' . get_string('fixed_course_lang', 'theme_coursemos', $this->page->course->lang) . '</div>';
        } else {
            foreach ($langs as $key => $value) {
                $flag = $this->image_url('flag/' . $key, 'theme_coursemos');
                $output .= '<a href="' . $url . $key . '" title="' . $value . '" class="a-lang dropdown-item" data-lang="' . $key . '"><img src="' . $flag . '" alt="' . $key . '" class="lang-flag mr-2" /> ' . $value . '</a>';
            }
        }
        $output .=    '</div>';
        $output .= '</div>';
        $output .= '</li>';
        
        if (isloggedin()) {

            /*
            // 읽지 않은 공지 및 메시지 갯수
            $noReadCount = \theme_coursemos\Html::getInstance()->getNoReadCount();

            // 사용자 정보
            $CUser = \local_ubion\user\User::getInstance();
            $output .= '<li class="nav-item nav-item-userinfo">';
            $output .= '<a class="btn-userinfo">';
            $output .= '<img src="' . $CUser->getPicture($USER) . '" alt="' . get_string('pictureof', '', fullname($USER)) . '" class="userpicture" />';
            if ($noReadCount > 0) {
                $output .= '<span class="badge">' . $noReadCount . '</span>';
            }
            $output .= '</a>';

            $output .= '</li>';

            // 로그아웃
            $output .= '<li class="nav-item nav-item-loginout d-none d-sm-block">';
            $output .= '<a href="' . $CFG->wwwroot . '/login/logout.php?sesskey=' . sesskey() . '" class="btn btn-sm btn-loginout">' . get_string('logout') . '</a>';
            $output .= '</li>';
            */
        } else {
            $output .= '<li role="presentation" class="nav-item nav-item-loginout">';
            // $output .= '<div class="btn-group" role="group">';
            // $output .= '<a href="'.$CFG->UB->SSO_URL.'" class="btn btn-sso">'.get_string('ewha_login', 'auth_ewha').'</a>';
            // $output .= '<a href="'.get_login_url().'" class="btn btn-person">'.get_string('default_login', 'auth_ewha').'</a>';
            // $output .= '</div>';
            $output .= '<a href="' . get_login_url() . '" class="btn btn-sm btn-loginout">' . get_string('login') . '</a>';
            $output .= '</li>';
        }
        $output .= '</ul>';

        return $output;
    }

    /**
     * Output a notification (that is, a status message about something that has just happened).
     *
     * Note: \core\notification::add() may be more suitable for your usage.
     *
     * @param string $message
     *            The message to print out.
     * @param string $type
     *            The type of notification. See constants on \core\output\notification.
     * @return string the HTML to output.
     */
    public function notification($message, $type = null)
    {

        // error => danger로 변경
        $typemappings = [
            // Valid types.
            'success' => \core\output\notification::NOTIFY_SUCCESS,
            'info' => \core\output\notification::NOTIFY_INFO,
            'warning' => \core\output\notification::NOTIFY_WARNING,
            // 'error' => \core\output\notification::NOTIFY_ERROR,
            'error' => 'danger',

            // Legacy types mapped to current types.
            /*
             * 'notifyproblem' => \core\output\notification::NOTIFY_ERROR,
             * 'notifytiny' => \core\output\notification::NOTIFY_ERROR,
             * 'notifyerror' => \core\output\notification::NOTIFY_ERROR,
             */
            'notifyproblem' => 'danger',
            'notifytiny' => 'danger',
            'notifyerror' => 'danger',
            'notifysuccess' => \core\output\notification::NOTIFY_SUCCESS,
            'notifymessage' => \core\output\notification::NOTIFY_INFO,
            'notifyredirect' => \core\output\notification::NOTIFY_INFO,
            'redirectmessage' => \core\output\notification::NOTIFY_INFO
        ];

        $extraclasses = [];

        if ($type) {
            if (strpos($type, ' ') === false) {
                // No spaces in the list of classes, therefore no need to loop over and determine the class.
                if (isset($typemappings[$type])) {
                    $type = $typemappings[$type];
                } else {
                    // The value provided did not match a known type. It must be an extra class.
                    $extraclasses = [
                        $type
                    ];
                }
            } else {
                // Identify what type of notification this is.
                $classarray = explode(' ', self::prepare_classes($type));

                // Separate out the type of notification from the extra classes.
                foreach ($classarray as $class) {
                    if (isset($typemappings[$class])) {
                        $type = $typemappings[$class];
                    } else {
                        $extraclasses[] = $class;
                    }
                }
            }
        }

        $notification = new \core\output\notification($message, $type);

        // 닫기 버튼 표시 되지 않도록 설정
        $notification->set_show_closebutton(false);
        if (count($extraclasses)) {
            $notification->set_extra_classes($extraclasses);
        }

        // Return the rendered template.
        return $this->render_from_template($notification->get_template_name(), $notification->export_for_template($this));
    }

    /**
     * Gets HTML for the page heading.
     *
     * @since Moodle 2.5.1 2.6
     * @param string $tag
     *            The tag to encase the heading in. h1 by default.
     * @return string HTML.
     */
    public function page_heading($tag = 'h1')
    {
        return html_writer::tag($tag, $this->page->heading, array(
            'class' => 'page-title'
        ));
    }

    /**
     * Renders theme links for switching between default and other themes.
     *
     * @return string
     */
    protected function theme_switch_links()
    {
        $actualdevice = \core_useragent::get_device_type();
        $currentdevice = $this->page->devicetypeinuse;
        $switched = ($actualdevice != $currentdevice);

        if (! $switched && $currentdevice == 'default' && $actualdevice == 'default') {
            // The user is using the a default device and hasn't switched so don't shown the switch
            // device links.
            return '';
        }

        if ($switched) {
            $linktext = get_string('switchdevicerecommended');
            $devicetype = $actualdevice;
        } else {
            $linktext = get_string('switchdevicedefault');
            $devicetype = 'default';
        }
        $linkurl = new moodle_url('/theme/switchdevice.php', array(
            'url' => $this->page->url,
            'device' => $devicetype,
            'sesskey' => sesskey()
        ));

        $content = html_writer::start_tag('div', array(
            'id' => 'theme_switch_link',
            'class' => 'mt-3'
        ));
        $content .= html_writer::link($linkurl, $linktext, array(
            'rel' => 'nofollow',
            'class' => 'btn btn-default btn-theme-switch'
        ));
        $content .= html_writer::end_tag('div');

        return $content;
    }

    /**
     * Print a message along with button choices for Continue/Cancel
     *
     * If a string or moodle_url is given instead of a single_button, method defaults to post.
     *
     * @param string $message
     *            The question to ask the user
     * @param single_button|moodle_url|string $continue
     *            The single_button component representing the Continue answer. Can also be a moodle_url or string URL
     * @param single_button|moodle_url|string $cancel
     *            The single_button component representing the Cancel answer. Can also be a moodle_url or string URL
     * @return string HTML fragment
     */
    public function confirm($message, $continue, $cancel)
    {
        if ($continue instanceof single_button) {
            // ok
            $continue->primary = true;
        } else if (is_string($continue)) {
            $continue = new single_button(new moodle_url($continue), get_string('continue'), 'post', true);
        } else if ($continue instanceof moodle_url) {
            $continue = new single_button($continue, get_string('continue'), 'post', true);
        } else {
            throw new coding_exception('The continue param to $OUTPUT->confirm() must be either a URL (string/moodle_url) or a single_button instance.');
        }

        if ($cancel instanceof single_button) {
            // ok
        } else if (is_string($cancel)) {
            $cancel = new single_button(new moodle_url($cancel), get_string('cancel'), 'get');
        } else if ($cancel instanceof moodle_url) {
            $cancel = new single_button($cancel, get_string('cancel'), 'get');
        } else {
            throw new coding_exception('The cancel param to $OUTPUT->confirm() must be either a URL (string/moodle_url) or a single_button instance.');
        }

        $data = new \stdClass();
        $data->message = $message;
        $data->continue = $this->render($continue);
        $data->cancel = $this->render($cancel);

        return $this->render_from_template('theme_coursemos/confirm', $data);
    }

    /**
     * Renders the login form.
     *
     * @param \core_auth\output\login $form
     *            The renderable.
     * @return string
     */
    public function render_login(\core_auth\output\login $form)
    {
        $context = $form->export_for_template($this);

        // 로그인 폼이 학교마다 달라질수 있기 때문에 Login 클래스에서 처리되도록 수정함.
        $context = \theme_coursemos\Login::getInstance()->getExportForTemplate($this, $context);

        // 무들 기본은 core/loginform 을 호출하기 때문에
        // theme/coursemos/template/core/loginform 파일을 생성해서 수정하면 되지만,
        // layout 관련 파일은 한곳에서 관리를 위해서 theme/coursemos/template/layout-loginform으로 위치 변경시킴
        return $this->render_from_template('theme_coursemos/layout-loginform', $context);
    }

    /**
     * Renders the header bar.
     *
     * @param context_header $contextheader
     *            Header bar object.
     * @return string HTML for the header bar.
     */
    protected function render_context_header(context_header $contextheader)
    {
        // parent::render_context_header()와 다른점이라면
        // page-context-header 항목에 well 추가
        // btn-group header-button-group에서 btn-group 항목 제거

        // All the html stuff goes here.
        $html = html_writer::start_div('page-context-header well');

        // Image data.
        if (isset($contextheader->imagedata)) {
            // Header specific image.
            $html .= html_writer::div($contextheader->imagedata, 'page-header-image');
        }

        // Headings.
        if (! isset($contextheader->heading)) {
            $headings = $this->heading($this->page->heading, $contextheader->headinglevel);
        } else {
            $headings = $this->heading($contextheader->heading, $contextheader->headinglevel);
        }

        $html .= html_writer::tag('div', $headings, array(
            'class' => 'page-header-headings'
        ));

        // Buttons.
        if (isset($contextheader->additionalbuttons)) {
            $html .= html_writer::start_div('header-button-group');
            foreach ($contextheader->additionalbuttons as $button) {
                if (! isset($button->page)) {
                    // Include js for messaging.
                    if ($button['buttontype'] === 'togglecontact') {
                        \core_message\helper::togglecontact_requirejs();
                    }
                    $image = $this->pix_icon($button['formattedimage'], $button['title'], 'moodle', array(
                        'class' => 'iconsmall',
                        'role' => 'presentation'
                    ));
                    $image .= html_writer::span($button['title'], 'header-button-title');
                } else {
                    $image = html_writer::empty_tag('img', array(
                        'src' => $button['formattedimage'],
                        'role' => 'presentation'
                    ));
                }

                if (isset($button['linkattributes']['class'])) {
                    if (strpos($button['linkattributes']['class'], 'btn-') === false) {
                        $button['linkattributes']['class'] .= ' btn-default';
                    }
                }
                $html .= html_writer::link($button['url'], html_writer::tag('span', $image), $button['linkattributes']);
            }
            $html .= html_writer::end_div();
        }
        $html .= html_writer::end_div();

        return $html;
    }
    
    
    /**
     * Get the HTML for blocks in the given region.
     *
     * @since Moodle 2.5.1 2.6
     * @param string $region The region to get HTML for.
     * @return string HTML.
     */
    public function blocks($region, $classes = array(), $tag = 'aside') {
        $displayregion = $this->page->apply_theme_region_manipulations($region);
        $classes = (array)$classes;
        $classes[] = 'block-region';
        $attributes = array(
            'id' => 'block-region-'.preg_replace('#[^a-zA-Z0-9_\-]+#', '-', $displayregion),
            'class' => join(' ', $classes),
            'data-blockregion' => $displayregion,
            'data-droptarget' => '1'
        );
        
        if ($this->page->blocks->region_has_content($displayregion, $this)) {
            $content = $this->blocks_for_region($displayregion);
        } else {
            $content = '';
        }
        
        // 블록 content 내용이 존재할 경우에만 리턴
        if (!empty($content)) {
            return html_writer::tag($tag, $content, $attributes);
        } else {
            return null;
        }
    }
    
    
    /**
     * Returns the URL for the favicon.
     *
     * @since Moodle 2.5.1 2.6
     * @return string The favicon URL
     */
    public function favicon()
    {
        // 설정에 파비콘이 업로드 되어있는지 확인
        if (!empty($this->page->theme->settings->favicon)) {
            $url = $this->page->theme->setting_file_url('favicon', 'favicon');
        } else {
            $url = $this->image_url('favicon', 'theme');
        }
        
        return $url;
    }
}
