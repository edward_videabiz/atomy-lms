<?php
namespace theme_coursemos\output;
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/blocks/settings/renderer.php');

use moodle_url;


class block_settings_renderer extends \block_settings_renderer {
    
    public function search_form(moodle_url $formtarget, $searchvalue) {
        $data = [
            'action' => $formtarget->out(false),
            'label' => get_string('searchinsettings', 'admin'),
            'searchvalue' => $searchvalue
        ];
        return $this->render_from_template('block_settings/search_form', $data);
    }
    
}
