<?php
namespace theme_coursemos\core;

use local_ubion\base\Common;
use local_ubion\base\Javascript;
use local_ubion\base\Parameter;

class FamilySites extends \local_ubion\controller\Controller
{

    const TYPE_HEADER_TOP = 'headerFamilyTop';

    const TYPE_HEADER_MIDDLE = 'headerFamilyMiddle';

    const TYPE_HEADER_BOTTOM = 'headerFamilyBottom';

    const TYPE_LOGIN_FAMILY = 'loginFamily';

    const TYPE_LOGIN_BUTTON = 'loginButton';

    protected $cacheKey = 0;

    protected $pluginname = 'theme_coursemos';

    /**
     * SITE관리자 여부
     *
     * siteadmin이 아니면 alert 출력
     */
    private function authCheck()
    {
        if (! is_siteadmin()) {
            Javascript::printAlert(get_string('no_permissions', 'local_ubion'));
        }
    }

    public function getLists($type)
    {
        global $DB;

        $query = "SELECT * FROM {theme_coursemos_strings} WHERE type = :type";
        $order = ' ORDER BY sortorder, timecreated';

        return $DB->get_records_sql($query . $order, array(
            'type' => $type
        ));
    }

    public function doView()
    {
        $id = Parameter::request('id', 0, PARAM_INT);

        $result = null;
        if (! empty($id)) {
            $result = $this->getView($id);
        }

        if (Parameter::isAajx()) {
            Javascript::printJSON(array(
                'code' => Javascript::getSuccessCode(),
                'msg' => get_string('view_complete', 'local_ubion'),
                'result' => $result
            ));
        } else {
            return $result;
        }
    }

    public function getView($id)
    {
        global $DB;

        if ($result = $DB->get_record_sql("SELECT * FROM {theme_coursemos_strings} WHERE id = :id", array(
            'id' => $id
        ))) {
            // serialize된값을 풀어서 전달해줌
            if (! empty($result)) {
                $result->valueUnserialize = unserialize($result->value);
            }
        }

        return $result;
    }

    /**
     * string 추가
     */
    function doInsert()
    {
        // 권한 체크
        $this->authCheck();

        $type = Parameter::request('type', null, PARAM_ALPHAEXT);
        $stringid = Parameter::request('stringid', null, PARAM_ALPHANUMEXT);
        $icon = Parameter::request('icon', null, PARAM_ALPHANUMEXT);
        $url = Parameter::request('url', null, PARAM_URL);

        // ko는 필수
        // 나머지는 선택사항이기 때문에 따로 유효성 검사 진행하지 않음.
        $langKO = Parameter::request('lang-ko', null, PARAM_NOTAGS);

        $validate = $this->validate()->make(array(
            'type' => array(
                'required',
                $this->validate()::PLACEHOLDER => '구분',
                $this->validate()::PARAMVALUE => $type
            ),

            'stringid' => array(
                'required',
                $this->validate()::PLACEHOLDER => 'stringid',
                $this->validate()::PARAMVALUE => $stringid
            ),

            'lang-ko' => array(
                'required',
                $this->validate()::PLACEHOLDER => '한글명',
                $this->validate()::PARAMVALUE => $langKO
            ),

            'url' => array(
                'required',
                $this->validate()::PLACEHOLDER => '주소',
                $this->validate()::PARAMVALUE => $url
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {

            // stringid값이 중복된게 있는지 확인해봐야됨.
            if ($this->isStringIDOverlap($type, $stringid)) {
                Javascript::printAlert(get_string('family_overlap', $this->pluginname, $stringid));
            }

            // 언어 명칭 serialize 시켜서 저장시켜야됨.
            $value = array();
            if ($langs = Common::getLangs()) {
                foreach ($langs as $l) {
                    $value[$l] = Parameter::request('lang-' . $l, null, PARAM_NOTAGS);
                }
            }
            $value = serialize($value);

            $this->setInsert($type, $stringid, $value, $icon, $url);

            Javascript::printJSON(array(
                'code' => Javascript::getSuccessCode(),
                'msg' => get_string('save_complete', 'local_ubion')
            ));
        }
    }

    /**
     * 패밀리 사이트 stringid값이 중복되었는지 확인해줍니다.
     *
     * @param string $name
     * @return mixed|boolean
     */
    function isStringIDOverlap($type, $stringid)
    {
        global $DB;

        return $DB->get_field_sql("SELECT COUNT(1) FROM {theme_coursemos_strings} WHERE type = :type AND stringid = :stringid", array(
            'type' => $type,
            'stringid' => $stringid
        ));
    }

    function setInsert($type, $stringid, $value, $icon, $url)
    {
        global $DB;

        $strings = new \stdClass();
        $strings->type = $type;
        $strings->stringid = $stringid;
        $strings->value = $value;
        $strings->icon = $icon;
        $strings->url = $url;
        $strings->timecreated = time();

        $strings->id = $DB->insert_record('theme_coursemos_strings', $strings);

        // 캐시 초기화 (개발 편의성을 위해서 패밀리 사이트 영역 모두 삭제 처리함)
        $this->setCacheDelete($type);

        return $strings;
    }

    /**
     * string 변경
     */
    function doUpdate()
    {
        // 권한 체크
        $this->authCheck();

        $id = Parameter::request('id', null, PARAM_INT);
        $type = Parameter::request('type', null, PARAM_ALPHAEXT);
        $stringid = Parameter::request('stringid', null, PARAM_ALPHANUMEXT);
        $icon = Parameter::request('icon', null, PARAM_ALPHANUMEXT);
        $url = Parameter::request('url', null, PARAM_URL);

        // ko는 필수
        // 나머지는 선택사항이기 때문에 따로 유효성 검사 진행하지 않음.
        $langKO = Parameter::request('lang-ko', null, PARAM_NOTAGS);

        $validate = $this->validate()->make(array(
            'type' => array(
                'required',
                $this->validate()::PLACEHOLDER => '구분',
                $this->validate()::PARAMVALUE => $type
            ),

            'stringid' => array(
                'required',
                $this->validate()::PLACEHOLDER => 'stringid',
                $this->validate()::PARAMVALUE => $stringid
            ),

            'lang-ko' => array(
                'required',
                $this->validate()::PLACEHOLDER => '한글명',
                $this->validate()::PARAMVALUE => $langKO
            ),

            'url' => array(
                'required',
                $this->validate()::PLACEHOLDER => '주소',
                $this->validate()::PARAMVALUE => $url
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {

            if ($oldData = $this->getView($id)) {

                // stringid 또는 type 값이 변경이 되는 경우 type, stringid값 중복 검사를 진행해야됨.
                if ($oldData->stringid != $stringid || $oldData->type != $type) {
                    // stringid값이 중복된게 있는지 확인해봐야됨.
                    if ($this->isStringIDOverlap($type, $stringid)) {
                        Javascript::printAlert(get_string('family_overlap', $this->pluginname, $stringid));
                    }
                }

                // 언어 명칭 serialize 시켜서 저장시켜야됨.
                $value = array();
                if ($langs = Common::getLangs()) {
                    foreach ($langs as $l) {
                        $value[$l] = Parameter::request('lang-' . $l, null, PARAM_RAW);
                    }
                }
                $value = serialize($value);

                $this->setUpdate($id, $type, $stringid, $value, $icon, $url);

                Javascript::printJSON(array(
                    'code' => Javascript::getSuccessCode(),
                    'msg' => get_string('update_complete', 'local_ubion')
                ));
            } else {
                Javascript::printJSON(array(
                    'code' => Javascript::getFailureCode(),
                    'msg' => '등록된 사이트 정보가 없습니다. id : ' . $id
                ));
            }
        }
    }

    function setUpdate($id, $type, $stringid, $value, $icon, $url)
    {
        global $DB;

        $family = new \stdClass();
        $family->id = $id;
        $family->type = $type;
        $family->stringid = $stringid;
        $family->value = $value;
        $family->icon = $icon;
        $family->url = $url;
        $family->timemodified = time();
        $DB->update_record('theme_coursemos_strings', $family);

        // 캐시 초기화 (개발 편의성을 위해서 패밀리 사이트 영역 모두 삭제 처리함)
        $this->setCacheDelete($type);

        return $family;
    }

    /**
     * 패밀리 사이트 삭제(post)
     *
     * @return mixed
     */
    function doDelete()
    {
        $id = Parameter::request('id', null, PARAM_INT);

        // 패밀리 사이트가 존재하는지 확인
        if ($this->getView($id)) {

            // 권한 체크
            $this->authCheck();

            // 패밀리 사이트 삭제
            $this->setDelete($id);

            return Javascript::printJSON(array(
                'code' => Javascript::getSuccessCode(),
                'msg' => get_string('delete_complete', 'local_ubion')
            ));
        } else {
            Javascript::printJSON(array(
                'code' => Javascript::getFailureCode(),
                'msg' => '등록된 사이트 정보가 없습니다. id : ' . $id
            ));
        }
    }

    /**
     * 패밀리 사이트 삭제
     *
     * @param number $id
     */
    function setDelete($id)
    {
        global $DB;

        // 패밀리 사이트가 존재하는지 확인
        if ($site = $this->getView($id)) {
            $DB->delete_records('theme_coursemos_strings', array(
                'id' => $id
            ));

            // 캐시 초기화 (개발 편의성을 위해서 패밀리 사이트 영역 모두 삭제 처리함)
            $this->setCacheDelete($site->type);
        }
    }

    /**
     * 메뉴 순서 변경 (post)
     */
    function doSortorder()
    {
        global $DB;

        $type = Parameter::request('type', null, PARAM_ALPHAEXT);
        $json = Parameter::request('json', null, PARAM_NOTAGS);

        // 관리자 권한체크
        $this->authCheck();

        $validate = $this->validate()->make(array(
            'type' => array(
                'required',
                $this->validate()::PLACEHOLDER => '구분',
                $this->validate()::PARAMVALUE => $type
            ),

            'json' => array(
                'required',
                $this->validate()::PLACEHOLDER => '정렬순서값',
                $this->validate()::PARAMVALUE => $json
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $json = json_decode($json);

            $order = 1;
            foreach ($json as $site) {
                $update = new \stdClass();
                $update->id = $site->id;
                $update->sortorder = $order;
                $DB->update_record('theme_coursemos_strings', $update);

                $order ++;
            }

            // 캐시 초기화
            $this->setCacheDelete($type);

            Javascript::printJSON(array(
                'code' => Javascript::getSuccessCode(),
                'msg' => get_string('save_complete', 'local_ubion')
            ));
        }
    }

    /**
     * 패밀리 사이트 캐시
     *
     * @return \cache
     */
    function getCache()
    {
        return \cache::make('theme_coursemos', 'family');
    }

    /**
     * 패밀리 사이트 특정 영역 캐시 삭제
     *
     * @param string $type
     */
    function setCacheDelete($type)
    {
        $this->getCache()->delete($type);
    }

    /**
     * 패밀리 사이트 특정 영역 목록
     *
     * @param string $type
     * @param boolean $isReset
     * @return string
     */
    function getCacheList($type, $isReset = false)
    {
        $cache = $this->getCache();

        $familys = $cache->get($type);

        if ($familys === false || $isReset) {
            $familys = $this->getLists($type);

            // 캐시 등록
            $cache->set($type, $familys);
        }

        // 등록된 패밀리 사이트에 대해서 현재 언어와 비교해서 최종적인 데이터만 리턴해줘야됨.
        $functionName = 'getCacheList' . ucfirst($type);

        // 일부러 property_exists 검사를 진행하지않음
        // 일차적으로 로컬에서 테스트 하기 때문에 오류 확인을 위해서 별다른 예외처리를 진행하지 않음.
        return $this->$functionName($familys);
    }

    /**
     * 패밀리 사이트 상단
     *
     * @param array $familys
     * @return string
     */
    protected function getCacheListHeaderFamilyTop($familys)
    {
        return $this->getCacheListHeaderFamilyHTML($familys, 'family-top');
    }

    protected function getCacheListHeaderFamilyMiddle($familys)
    {
        return $this->getCacheListHeaderFamilyHTML($familys, 'family-middle');
    }

    protected function getCacheListHeaderFamilyBottom($familys)
    {
        return $this->getCacheListHeaderFamilyHTML($familys, 'family-bottom');
    }

    protected function getCacheListHeaderFamilyHTML($familys, $class)
    {
        $html = '';

        if (! empty($familys)) {
            $currentLang = current_language();

            $html = '<ul class="' . $class . '">';
            foreach ($familys as $fs) {
                $icon = (! empty($fs->icon)) ? '<i class="family-icon mr-2 fa ' . $fs->icon . '"></i>' : '';
                $unserialize = unserialize($fs->value);

                $html .= '<li class="family-item">';
                $html .= '<a href="' . $fs->url . '" target="_blank" class="family-link">';
                $html .= $icon;
                $html .= $this->getFamilySiteName($unserialize, $currentLang);
                $html .= '</a>';
                $html .= '</li>';
            }
            $html .= '</ul>';
        }

        return $html;
    }

    public function getCacheListHeaderFamily()
    {
        $html = '';
        $html .= $this->getCacheList(self::TYPE_HEADER_TOP);
        $html .= $this->getCacheList(self::TYPE_HEADER_MIDDLE);
        $html .= $this->getCacheList(self::TYPE_HEADER_BOTTOM);

        return $html;
    }

    /**
     * 로그인 페이지 패밀리 사이트
     *
     * @param array $familys
     * @return string
     */
    protected function getCacheListLoginFamily($familys)
    {
        $html = '';

        if (! empty($familys)) {
            $html = '<div class="row">';
            $currentLang = current_language();

            foreach ($familys as $fs) {
                // 로그인 페이지에서는 사용자가 로그인을 안했기 때문에 따로 권한 체크 해야될 필요성이 없음.
                // if ($this->isView($fs->type, $fs->stringid)) {
                $icon = (! empty($fs->icon)) ? '<i class="family-icon fa ' . $fs->icon . '"></i>' : '';
                $unserialize = unserialize($fs->value);
                $siteName = $this->getFamilySiteName($unserialize, $currentLang);

                $html .= '<div class="col-xs-6 col-md-12">';
                $html .= '<a class="family-link" href="' . $fs->url . '" target="_blank" title="' . $siteName . '">';
                $html .= $icon;
                $html .= $siteName;
                $html .= '</a>';
                $html .= '</div>';
                // }
            }
            $html .= '</div>';
        }

        return $html;
    }

    /**
     * 로그인 페이지 버튼
     *
     * @param array $familys
     * @return string
     */
    protected function getCacheListLoginButton($familys)
    {
        $html = '';

        if (! empty($familys)) {
            $currentLang = current_language();

            $addClass = ' col-buttonset-' . count($familys);

            $html .= '<div class="col-buttonset ' . $addClass . '">';
            foreach ($familys as $fs) {
                // 로그인 페이지에서는 사용자가 로그인을 안했기 때문에 따로 권한 체크 해야될 필요성이 없음.
                // if ($this->isView($fs->type, $fs->stringid)) {
                $unserialize = unserialize($fs->value);
                $siteName = $this->getFamilySiteName($unserialize, $currentLang);

                $html .= '<div class="col-button">';
                $html .= '<a class="btn btn-buttonset" href="' . $fs->url . '" title="' . $siteName . '">';
                $html .= $siteName;
                $html .= '</a>';
                $html .= '</div>';
                // }
            }
            $html .= '</div>';
        }

        return $html;
    }

    /**
     * 사용자 언어에 맞는 패밀리 사이트 명칭이 리턴됨.
     *
     * @param array $unserialize
     * @param string $currentLang
     * @return string
     */
    protected function getFamilySiteName(array $unserialize, $currentLang = null)
    {
        return Common::getUnserializeName($unserialize, $currentLang);
    }

    /**
     * 메뉴를 볼수 있는지 확인
     *
     * @param string $type
     * @param string $stringid
     * @return boolean
     */
    public function isView($type, $stringid)
    {
        $isView = true;

        // type값이 headerFamilyTop이고 stringid값이 corecampus라면
        // functionName은 isViewHeaderFamilyTopCoreCampus로 지정해주면 됩니다.
        // 이 부분은 향후에 화면 설정에서 표시되어야 할 신분을 지정할수 있게 개선될수 있습니다.
        $functionName = 'isView' . ucfirst($type) . ucfirst($stringid);

        if (method_exists($this, $functionName)) {
            $isView = $this->$functionName();
        }

        return $isView;
    }

    /**
     * isView 관련 샘플 함수입니다.
     *
     * @return boolean
     */
    public function isViewHeaderFamilyCoursemosSample()
    {
        $CUser = \local_ubion\user\User::getInstance();

        if ($userUbion = $CUser->getUbion()) {
            if ($userUbion->user_type == $CUser::TYPE_STUDENT) {
                return true;
            }
        }

        return false;
    }
}