<?php
namespace theme_coursemos\core;

use local_ubion\base\Javascript;
use local_ubion\base\Parameter;

class Controllder extends \local_ubion\controller\Controller
{

    /**
     * 사용자 정보 출력
     *
     * @return string
     */
    public function doUserInfoMy()
    {
        $CHtml = \theme_coursemos\Html::getInstance();

        $html = $CHtml->getUserInfoMy();

        if (Parameter::isAajx()) {
            Javascript::printJSON(array(
                'code' => Javascript::getSuccessCode(),
                'msg' => get_string('view_complete', 'local_ubion'),
                'html' => $html
            ));
        } else {
            return $html;
        }
    }

    /**
     * 사용자 알림(메시지, notifications) 출력
     *
     * @return string
     */
    public function doUserInfoNotifications()
    {
        $CHtml = \theme_coursemos\Html::getInstance();

        $html = $CHtml->getUserInfoNotifications();

        if (Parameter::isAajx()) {
            Javascript::printJSON(array(
                'code' => Javascript::getSuccessCode(),
                'msg' => get_string('view_complete', 'local_ubion'),
                'html' => $html
            ));
        } else {
            return $html;
        }
    }

    public function doMyCoursesHakgi()
    {
        $year = Parameter::post('year', null, PARAM_INT);
        $semester = Parameter::post('semester', null, PARAM_NOTAGS);
        $coursetype = Parameter::post('coursetype', null, PARAM_ALPHANUMEXT);

        // semester 유효성 검사
        $semester = \local_ubion\course\Course::getInstance()->getSemesterValidation($semester);

        $validate = $this->validate()->make(array(
            'year' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('year', 'local_ubion'),
                $this->validate()::PARAMVALUE => $year
            ),
            'semester' => array(
                'required',
                $this->validate()::PLACEHOLDER => get_string('semester', 'local_ubion'),
                $this->validate()::PARAMVALUE => $semester
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {

            $CHtml = \theme_coursemos\Html::getInstance();
            $html = $CHtml->getMyCourseHakgi($coursetype, $year, $semester);

            if (Parameter::isAajx()) {
                Javascript::printJSON(array(
                    'code' => Javascript::getSuccessCode(),
                    'msg' => get_string('view_complete', 'local_ubion'),
                    'html' => $html
                ));
            } else {
                return $html;
            }
        }
    }

    public function doLayout()
    {
        global $USER;

        $CUser = \local_ubion\user\User::getInstance();

        $layout = Parameter::post('layout', null, PARAM_ALPHANUMEXT);

        $validate = $this->validate()->make(array(
            'layout' => array(
                'required',
                $this->validate()::PLACEHOLDER => 'layout',
                $this->validate()::PARAMVALUE => $layout
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {

            // 정해진 layout값이 전달되지 않았으면 기본값으로 설정
            if ($layout != $CUser::LAYOUT_PROGRESS && $layout != $CUser::LAYOUT_HAKGI) {
                $layout = $CUser->getMyCourseLayout();
            }

            // 설정값 저장
            set_user_preference($CUser->getMyCourseLayoutName(), $layout);

            // 레이아웃 변경으로 인해 세션에 2개 레이아웃에 대한 강좌 정보가 담겨있다보니
            // 세션크기가 커지는 상황이 발생될수 있기 때문에
            // 레이아웃 변경시에는 강좌정보를 담고 있는 세션 부분을 초기화 시켜줘야됨.
            if (isset($USER->ubion->course)) {
                $USER->ubion->course->search = false;
                $USER->ubion->course->courses = array();
                $USER->ubion->course->time = time();
            }

            if (isset($USER->ubion->courseall)) {
                $USER->ubion->courseall->search = false;
                $USER->ubion->courseall->courses = array();
                $USER->ubion->courseall->time = time();
            }

            if (Parameter::isAajx()) {
                Javascript::printJSON(array(
                    'code' => Javascript::getSuccessCode(),
                    'msg' => get_string('view_complete', 'local_ubion')
                ));
            } else {
                return true;
            }
        }
    }

    public function doLangChange()
    {
        $lang = Parameter::post('lang', null, PARAM_LANG);

        $validate = $this->validate()->make(array(
            'lang' => array(
                'required',
                $this->validate()::PLACEHOLDER => 'lang',
                $this->validate()::PARAMVALUE => $lang
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {

            // 전달된 lang으로 DB 업데이트
            // 학교측 요구로.. 언어 변경시마다 user에 lang값을 변경하기로 함.
            // 전달된 값이 실제로 존재하는 언어인지 확인이 필요함.
            $langs = get_string_manager()->get_list_of_translations();

            // 설치된 언어팩인 경우에만 저장
            if (isset($langs[$lang])) {
                $this->setLangChange($lang);
            }

            Javascript::printAlert(get_string('save_complete', 'local_ubion'), true);
        }
    }

    /**
     * 언어 변경
     *
     * @param string $lang
     */
    public function setLangChange($lang)
    {
        global $USER, $DB;

        // 로그인 되어 있고, 게스트 사용자가 아닌 경우
        if (isloggedin() && ! isguestuser()) {
            $update = new \stdClass();
            $update->id = $USER->id;
            $update->lang = $lang;

            $DB->update_record('user', $update);
        }
    }
}