<?php
namespace theme_coursemos\core;

use context_course;
use stdClass;

class Login
{
    const LOGINFORMLEFT = 'left';       // 로그인 폼 정렬 위치 - 좌측
    const LOGINFORMRIGHT = 'right';     // 로그인 폼 정렬 위치 - 우측
    
    // 로그인 배경 이미지 갯수를 늘려주려면 $maxBackgroundImageCount 숫자를 증가시켜주면 됩니다.
    protected $maxBackgroundImageCount = 5;
    protected $pluginname = 'theme_coursemos';
    
    
    function getExportForTemplate(\core_renderer $renderer, $context)
    {
        global $CFG, $SITE;
        
        // Override because rendering is not supported in template yet.
        $context->cookieshelpiconformatted = $renderer->help_icon('cookiesenabled');
        $context->errorformatted = $renderer->error_text($context->error);
        $url = $renderer->get_logo_url();
        if ($url) {
            $url = $url->out(false);
        }
        $context->logourl = $url;
        $context->sitename = format_string($SITE->fullname, true, ['context' => context_course::instance(SITEID), "escape" => false]);
        
        
        // 언어
        $context->langHTML = $this->getLangHTML($renderer);
        
        
        $themeConfig = get_config('theme_coursemos');
        
        // 로그인 폼 위치
        $context->loginContainerPosition = '';
        if ($themeConfig->loginform_position == $this::LOGINFORMRIGHT) {
            $context->loginContainerPosition = 'login-container-right';
        }
        
        // 아이디 비밀번호 찾기 주소
        $context->findpwd_url = $themeConfig->loginform_findpwd_url;
        
        // SSO 로그인 버튼 표시여부
        $context->isSSO = $themeConfig->loginform_sso_use;
        $context->ssoUrl = $themeConfig->loginform_sso_url;
        
        
        // 별도계정
        $context->isSeparate = $themeConfig->loginform_separate_use;
        
        $context->loginform = new stdClass();
        $context->loginform->person = new stdClass();
        $context->loginform->person->action = $context->loginurl;
        $context->loginform->person->username = 'username';
        $context->loginform->person->password = 'password';
        $context->loginform->person->findpwd_url = $context->findpwd_url;
        
        $context->loginform->sso = new stdClass();
        $context->loginform->sso->action = $themeConfig->loginform_separate_url;
        $context->loginform->sso->username = $themeConfig->loginform_separate_username;
        $context->loginform->sso->password = $themeConfig->loginform_separate_password;
        $context->loginform->sso->findpwd_url = $context->findpwd_url;
        
        // 별도 기능을 사용안하면 무들 입력폼으로 설정
        // 별도 기능을 사용하면 sso 사이트 입력폼에 맞는 설정값 설정
        $context->loginform->default = $context->loginform->person;
        if ($context->isSeparate) {
            $context->loginform->default = $context->loginform->sso;
        }
        
        // SSO 로그인 사용시 외부인 비밀번호 찾기는 무들 기본 값이여야함.
        // 미 사용시에는 sso 사용자가 우선시 되어야 하기 때문에 학내 비밀번호찾기가 우선시 되어야 함. 
        if ($context->isSSO) {
            $context->loginform->person->findpwd_url = $context->forgotpasswordurl;
        }
        
        // family site
        $CFamilySites = \theme_coursemos\FamilySites::getInstance();
        $context->familySiteHTML = $CFamilySites->getCacheList($CFamilySites::TYPE_LOGIN_FAMILY);
        
        // button
        $context->buttonHTML = $CFamilySites->getCacheList($CFamilySites::TYPE_LOGIN_BUTTON);
        
        
        // 공지사항
        $CThemeHTML = \theme_coursemos\Html::getInstance();
        if ($notices = $CThemeHTML->getNotices(5)) {
            // key값을 제거해줘야됨.
            $context->notices = array_values($notices);
        }
        // 공지사항 게시판 주소
        $noticecmid = get_config('theme_coursemos', 'ubboard_cmid');
        $context->noticeBoardURL = "#";
        if (!empty($noticecmid)) {
            $context->noticeBoardURL = $CFG->wwwroot.'/mod/ubboard/view.php?id='.$noticecmid;
        }
        
        
        // 배너
        $banners = array();
        $bannerCount = count($banners);
        
        $context->banner = new stdClass();
        $context->banner->is = ($bannerCount > 0) ? true : false;
        $context->banner->lists = $banners;
        
        return $context;
    }
    
    
    protected function getLangHTML($renderer)
    {
        global $CFG;
        
        
        $pluginname = 'theme_coursemos';
        
        // langmenu
        $currlang = current_language();
        $langs = get_string_manager()->get_list_of_translations();
        
        // lang 관련 파라메터가 존재하면 lang 파라메터를 삭제하고 아래 language쪽에서 다시 셋팅해줘야됨.
        if(isset($_GET['lang'])) {
            unset($_GET['lang']);
        }
        
        $langUrl = $CFG->wwwroot.$_SERVER['SCRIPT_NAME'];
        if($_GET) {
            $langUrl .= '?'.http_build_query($_GET).'&amp;lang=';
        } else {
            $langUrl .= '?lang=';
        }
        
        if(isset($langs[$currlang])) {
            $icon = $renderer->pix_icon('flag/'.$currlang, '', $pluginname);
            $langName = $icon.$langs[$currlang];
        } else {
            $icon = $renderer->pix_icon('flag/en', '', $pluginname);
            $langName = $icon.$langs['en'];
        }
        
        $langHTML = '';
        $langHTML .= '<div class="dropdown dropdown-lang">';
        $langHTML .= 	'<a id="user-info-lang" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">'.$langName.' <span class="caret"></span> </a>';
        
        $langHTML .= 	'<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="user-info-lang">';
        foreach($langs as $key=>$value) {
            $icon = $renderer->pix_icon('flag/'.$key, '', $pluginname);
            $langHTML .=  '<li><a href="'.$langUrl.$key.'" title="'.$value.'">'.$icon.' '.$value.'</a></li>';
        }
        $langHTML .= 	'</ul>';
        $langHTML .=  '</div>';
        
        return $langHTML;
    }
    
    /**
     * 업로드 가능한 배경 이미지 갯수
     * @return number
     */
    function getBackgroundImageCount()
    {
        return $this->maxBackgroundImageCount;
    }
    
    
    /**
     * 배경 이미지 css Name
     * @return string
     */
    function getBackgroundCSSNamePrefix()
    {
        return 'coursemos-backgroundimage-';
    }
    
    /**
     * 배경 이미지 filearea
     * @return string
     */
    function getBackgroundFileAreaPrefix()
    {
        return 'backgroundimage_';
    }
    
    /**
     * 로그인 화면에 배경 이미지 표시 갯수 
     * 
     * 배경 이미지 업로드와는 관계가 없습니다.
     * 
     * @return number
     */
    function getBackgroundViewCount()
    {
        $count = get_config('theme_coursemos', 'backgroundimage_count');
        
        // 설정된 값이 따로 없다면 1개로 설정
        if (empty($count)) {
            $count = 1;
        }
        
        return $count;
    }
}