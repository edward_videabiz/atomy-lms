<?php
namespace theme_coursemos\core;

class Html
{

    /**
     * 읽지 않은 메시지 + 알림 갯수
     *
     * @var int
     */
    private static $noReadCount = null;

    protected $pluginname = 'theme_coursemos';

    protected $courseSections = null;

    /**
     * 역할 변경 버튼
     *
     * @param number $courseid
     * @param boolean $isProfessor
     */
    function getRoleChange($courseid, $isProfessor)
    {
        global $CFG, $PAGE;

        $html = '';

        if ($isProfessor || is_role_switched($courseid)) {
            $sesskey = sesskey();

            $class = 'btn-switch';

            if ($isProfessor) {
                $returnurl = $PAGE->url;

                $url = $CFG->wwwroot . '/course/switchrole.php?id=' . $PAGE->course->id . '&sesskey=' . $sesskey . '&switchrole=5&returnurl=' . $returnurl->out(false);
                $text = get_string('screen_student', $this->pluginname);
            } else {
                $url = $CFG->wwwroot . '/course/view.php?id=' . $PAGE->course->id . '&sesskey=' . $sesskey . '&switchrole=0';
                $text = get_string('screen_professor', $this->pluginname);
                $class .= ' btn-switch-prof';
            }

            $html = '<a href="' . $url . '" class="' . $class . '">' . $text . '</a>';
        }

        return $html;
    }

    /**
     * 강좌에 등록된 주차 목록
     *
     * @param int $courseid
     * @param boolean $isCourseMain
     * @return string
     */
    public function getCourseSections($courseid, $isCourseMain=false)
    {
        global $CFG;

        $courseFormat = course_get_format($courseid);
        $courseContext = \context_course::instance($courseid);

        if (empty($this->courseSections)) {
            if ($courseFormat->uses_sections()) {
                if ($sections = $courseFormat->get_sections()) {
                    foreach ($sections as $section) {

                        // section => section_info에 peroperty 추가가 안되도록 되어 있음.
                        $sectionInfo = new \stdClass();
                        $sectionInfo->id = $section->id;
                        $sectionInfo->section = $section->section;
                        $sectionInfo->courseid = $courseid;
                        
                        // 강좌 메인 페이지에서는 scrollspy가 작동해야되므로 anchor로 링크 생성해줘야됨.
                        if ($isCourseMain) {
                            $url = '#section-'.$section->section;
                        } else {
                            $url = $courseFormat->get_view_url($section);
                        }
                        $sectionInfo->url = $url;

                        if (! empty($section->name)) {
                            $title = format_string($section->name, true, array(
                                'context' => $courseContext
                            ));
                        } else {
                            $title = $courseFormat->get_section_name($section);
                        }

                        // 주차명
                        $sectionInfo->title = $title;
                        $sectionInfo->stripTitle = strip_tags($title);

                        // 주차 강좌 class
                        $sectionInfo->class = '';
                        if ($courseFormat->is_section_current($section)) {
                            $sectionInfo->class = 'active';
                        }

                        $sectionInfo->sectionInfo = $section;

                        $this->courseSections[] = $sectionInfo;
                    }
                }
            }
        }

        return $this->courseSections;
    }

    /**
     * 모바일 주소줄 배경색상
     *
     * @return string
     */
    public function getMobileThemeColor()
    {
        global $PAGE;

        return (empty($PAGE->theme->settings->headercolor)) ? '#068053' : $PAGE->theme->settings->headercolor;
    }

    public function getUpcommingPrintCount()
    {
        return 5;
    }

    /**
     * 예정된 할일
     *
     * @return string
     */
    public function getUpcommings($page = 1, $ls = 10, $recent_period_time = 1209600)
    {
        global $CFG;
        require_once $CFG->dirroot . '/calendar/lib.php';

        $CCourse = \local_ubion\course\Course::getInstance();
        $CCalendar = \local_ubion\calendar\Calendar::getInstance();

        $blockShowHideClass = (get_user_preferences('coursemos_block_upcomming', 'show') == 'hide') ? 'hidden' : '';

        /*
         * 로그인 시간 기준으로 하지 않고, 무조건 현재 시간으로 설정
         * 장시간 로그인 안하고 로그인했을 경우 currentlogin값이 너무 오래되서, 지난 일정을 가져오는 경우가 발생됨.
         * if(!empty($USER->currentlogin)) {
         * $current_login = $USER->currentlogin;
         * } else {
         * $current_login = time();
         * }
         */
        $time = time();

        $myCourses = $CCourse->getMyCourses();

        // 일정가져오기
        list ($courses, $group, $user) = calendar_set_filters($myCourses);

        $timestart = $time;

        // 종료일이 따로 지정된 경우
        $recentPeriod = 14; // 기본 값은 2주일
        $recentPeriodTime = $recentPeriod * DAYSECS;

        // 로그인 시간 + 설정된 기간
        $timeend = $timestart + $recentPeriodTime;

        // 가져올 갯수
        $maxLimit = $this->getUpcommingPrintCount();

        list ($totalCount, $events) = $CCalendar->getLists($courses, $group, $user, $timestart, $timeend, $page, $maxLimit);

        // 날자 포맷 정의
        $dateFormatShort = get_string('strftimedateshort');
        $dateFormatDate = get_string('strftimedate');
        $dateFormatFull = get_string('strftimedatetime');

        // 블록명 및 class 정의
        $blockName = 'upcomming';
        $blockClass = 'block-' . $blockName;
        if (! empty($blockShowHideClass)) {
            $blockClass .= ' ' . $blockShowHideClass;
        }
        $blockTitle = get_string('block_upcomming', $this->pluginname, array(
            'start' => userdate($timestart, $dateFormatShort),
            'end' => userdate($timeend, $dateFormatShort)
        ));

        $blockContent = '<ul class="timeline">';
        if ($totalCount > 0) {
            $totalPage = ceil($totalCount / $ls);

            foreach ($events as $ce) {
                $ce = $CCalendar->getMetadataSetting($ce, $ce->courseid, false);

                // 종료시간이 지정 되어 있는 경우도 있음.
                $eventTime = userdate($ce->timestart, $dateFormatDate);
                $eventTimeFull = userdate($ce->timestart, $dateFormatFull);

                // 종료일이 지정된 경우
                if ($ce->timeduration > 0) {
                    $duration_time = $ce->timestart + $ce->timeduration;

                    $eventTime .= ' ~ ' . userdate($duration_time, $dateFormatDate);
                    $eventTimeFull .= ' ~ ' . userdate($duration_time, $dateFormatFull);
                }

                $event_link = $ce->link;
                // 모듈 링크가 있다면
                if (isset($ce->refererlink)) {
                    $event_link = $ce->refererlink;
                }

                $blockContent .= '<li>';
                $blockContent .= '<a href="' . $event_link . '">';
                $blockContent .= '<div class="image">';
                $blockContent .= $ce->timeline_icon;
                $blockContent .= '</div>';
                $blockContent .= '<div class="title">';
                $blockContent .= '<h5 title="' . $ce->name . '">' . $ce->name . '</h5>';
                $blockContent .= '<p class="date upcomming_date" title="' . $eventTimeFull . '">' . $eventTime . '</p>';
                $blockContent .= '</div>';
                $blockContent .= '</a>';
                $blockContent .= '</li>';
            }

            // 조회된 갯수가 화면에 표시될 갯수보다 많을 경우 more 표시
            if ($totalCount > 1) {
                $nextPage = $page + 1;

                if ($nextPage <= $totalPage) {
                    $blockContent .= '<li class="more">';
                    /*
                     * 버튼을 클릭해서 다음 페이지 일정을 가져오고 싶은 경우.. 아직 미구현상태임
                     * $blockContent .= '<button type="button" class="btn btn-default btn-more" data-nextpage="'.$nextPage.'">';
                     * $blockContent .= get_string('more', $this->pluginname).'</span>';
                     * $blockContent .= '</button>';
                     */
                    $blockContent .= '<a href="' . $CFG->wwwroot . '/calendar/view.php?view=month&cal_m=' . date('m') . '" class="btn btn-default btn-more">';
                    $blockContent .= get_string('more_upcoming_event', $this->pluginname, $totalCount);
                    $blockContent .= '</a>';
                    $blockContent .= '</li>';
                }
            }
        } else {
            $blockContent .= '<li>' . get_string('noupcomingevents', 'calendar') . '</li>';
        }
        $blockContent .= '</ul>';

        $html = $this->getBlockBaseHTML();
        $html = str_replace('##block-name##', $blockName, $html);
        $html = str_replace('##block-class-name##', $blockClass, $html);
        $html = str_replace('##block-title##', $blockTitle, $html);
        $html = str_replace('##block-content##', $blockContent, $html);

        return $html;
    }

    /**
     * 알림
     *
     * @deprecated header 알림과 중복되서 더이상 표시하지 않습니다.
     * @return string
     */
    public function getNotifications()
    {
        // header 알림과 중복되서 더이상 표시하지 않음.
        // $blockShowHideClass = (get_user_preferences('coursemos_block_upcomming', 'show') == 'hide') ? 'hidden' : '';

        // // 블록명 및 class 정의
        // $blockName = 'notification';
        // $blockClass = 'block-' . $blockName;
        // if (! empty($blockShowHideClass)) {
        // $blockClass .= ' ' . $blockShowHideClass;
        // }

        // $blockTitle = get_string('block_notification', $this->pluginname);

        // list ($notificationCount, $blockContent) = \local_ubnotification\Notification::getInstance()->getBlockHTML();

        // if ($notificationCount > 0) {
        // $blockTitle .= '<span class="badge">' . $notificationCount . '</span>';
        // }

        // $html = $this->getBlockBaseHTML();
        // $html = str_replace('##block-name##', $blockName, $html);
        // $html = str_replace('##block-class-name##', $blockClass, $html);
        // $html = str_replace('##block-title##', $blockTitle, $html);
        // $html = str_replace('##block-content##', $blockContent, $html);
        $html = '';
        return $html;
    }

    /**
     * frontpage 블록 기본 html
     *
     * @return string
     */
    protected function getBlockBaseHTML()
    {
        $helper = $this->getBlockHelper();

        $html = '<div class="block block-coursemos ##block-class-name##" data-name="##block-name##">';
        $html .= '<div class="block-header">';
        $html .= '<div class="block-controls">';
        $html .= '<img class="block-hider-hide" alt="' . $helper['str_hide'] . '" src="' . $helper['img_show'] . '" title="' . $helper['str_hide'] . '" />';
        $html .= '<img class="block-hider-show" alt="' . $helper['str_show'] . '" src="' . $helper['img_hide'] . '" title="' . $helper['str_show'] . '" />';
        $html .= '</div>';
        $html .= '<h2 class="block-title">##block-title##</h2>';
        $html .= '</div>';
        $html .= '<div class="block-content">';
        $html .= '##block-content##';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * 블록 열/닫기 문자열, 이미지 값 리턴
     *
     * @return string[]
     */
    protected function getBlockHelper()
    {
        global $OUTPUT;

        return array(
            'str_show' => get_string('block_show', $this->pluginname),
            'str_hide' => get_string('block_hide', $this->pluginname),
            'img_show' => $OUTPUT->image_url('t/switch_minus'),
            'img_hide' => $OUTPUT->image_url('t/switch_plus')
        );
    }

    /**
     * 공지사항
     *
     * @param number $count
     * @return NULL|array
     */
    public function getNotices($count = 5)
    {
        $moduleid = get_config($this->pluginname, 'ubboard_id');
        $cmid = get_config($this->pluginname, 'ubboard_cmid');

        if (empty($moduleid) || empty($cmid)) {
            return null;
        } else {
            if ($notices = \mod_ubboard\latest::getLatest($moduleid, SITEID, $cmid)) {

                $time = time();
                $newPeriod = get_config('local_ubion', 'new_days');
                if (empty($newPeriod)) {
                    $newPeriod = 1;
                }
                $newPeriod = $newPeriod * DAYSECS;

                foreach ($notices as $ns) {
                    $ns->icon = null;
                    $ns->class = null;

                    // newicon 표시
                    if ($time < $ns->timecreated + $newPeriod) {
                        $ns->icon = '<span class="new-icon">N</span>';
                        $ns->class = 'bold';
                    }
                }
            }

            return $notices;
        }
    }

    /**
     * 사용자 정보 출력
     *
     * @return string|boolean
     */
    public function getUserInfoMy()
    {
        global $OUTPUT, $USER, $PAGE;

        $CCourse = \local_ubion\course\Course::getInstance();
        $CUser = \local_ubion\user\User::getInstance();

        if (empty($PAGE->context)) {
            $PAGE->set_context(\context_system::instance());
        }

        $context = new \stdClass();
        $context->isLogin = isloggedin();
        $context->isSiteAdmin = is_siteadmin();
        $context->userpicture = $CUser->getPicture($USER, 50);
        $context->fullname = fullname($USER);
        $context->department = $CUser->getDepartment($USER, true);
        $context->langHTML = $this->getLangChangeHTML();

        // 수강중인 강좌
        if ($courses = $CCourse->getMyCourses()) {

            foreach ($courses as $c) {
                $c->fullname = $CCourse->getName($c);
                $c->courseLabel = $CCourse->getTypeLabel($c->course_type, $c->course_gubun_code);
            }
        }

        // mustache로 전달할때에는 key값을 제거해줘야됩니다.
        $courses = array_values($courses);
        $context->courses = $courses;
        $context->courseCount = count($courses);

        return $OUTPUT->render_from_template('theme_coursemos/coursemos-userinfo-my', $context);
    }

    /**
     * 사용자 알림(메시지, notifications) 출력
     *
     * @return string
     */
    public function getUserInfoNotifications()
    {
        global $OUTPUT, $PAGE, $CFG;

        $CNotifications = \local_ubnotification\Notification::getInstance();
        $CMessage = \local_ubsend\message\Message::getInstance();

        if (empty($PAGE->context)) {
            $PAGE->set_context(\context_system::instance());
        }

        // url
        // text
        // icon
        // timecreated
        $lists = array();

        if ($notifications = $CNotifications->getNewNotifications()) {
            $notifications = $CNotifications->getNotificationSetting($notifications);

            foreach ($notifications as $ns) {
                $lists[] = $this->getNotificationItem($ns->message, $ns->url, $ns->icon, $ns->timecreated, 'notification');
            }

            // 읽지 않은 알림 항목에 대해서 읽음으로 처리할 경우 모두 읽음 함수 실행직
            if ($this->isNotificationRead()) {
                $CNotifications->setAllRead();
            }
        }

        if ($messages = $CMessage->getNoReadLists()) {
            $messages = $CMessage->getListsSetting($messages);

            foreach ($messages as $ms) {
                $lists[] = $this->getNotificationItem($ms->message, $ms->readurl, $ms->userpicture, $ms->timecreated, 'message');
            }

            // 모든 메시지 읽기
            if ($this->isMessageRead()) {
                $CMessage->setAllRead();
            }
        }

        // timecreated로 정렬 시켜주기
        usort($lists, function ($a, $b) {
            return $b->timecreated - $a->timecreated;
        });

        $context = new \stdClass();
        $context->lists = $lists;
        $context->listCount = count($lists);
        $context->messageUrl = $CFG->wwwroot . '/local/ubsend/message/';
        $context->notificationUrl = $CFG->wwwroot . '/local/ubnotification/';

        return $OUTPUT->render_from_template('theme_coursemos/coursemos-userinfo-notifications', $context);
    }

    /**
     * 읽지 않은 메시지 + 알림 갯수
     *
     * @return number
     */
    public function getNoReadCount()
    {
        // null인 경우에는 디비에서 조회
        // noreadcount를 static으로 선언한 이유가 여러 화면에서 noreadcount를 표시해야되는 경우가 있음.
        // 이때마다 디비에서 조회하기 보다 메모리에 존재하는 값을 리턴하는게 좋음.
        if (is_null(self::$noReadCount)) {
            $CNotifications = \local_ubnotification\Notification::getInstance();
            $CMessage = \local_ubsend\message\Message::getInstance();

            $noticiationCount = $CNotifications->getCount();
            $messageCount = $CMessage->getNoReadCount();

            self::$noReadCount = $noticiationCount + $messageCount;
        }
        return self::$noReadCount;
    }

    /**
     * doUserInfoNotifications 호출시 읽지 않은 알림에 대해서 읽음 처리 여부
     *
     * @return boolean
     */
    protected function isNotificationRead()
    {
        return true;
    }

    /**
     * doUserInfoNotifications 호출시 읽지 않은 메시지에 대해서 읽음 처리 여부
     *
     * @return boolean
     */
    protected function isMessageRead()
    {
        return true;
    }

    /**
     * 알림 + 메시지 항목 표시를 위한 object 리턴
     *
     * @param string $text
     * @param string $url
     * @param string $icon
     * @param int $timecreated
     * @param string $type
     * @return \stdClass
     */
    private function getNotificationItem($text, $url, $icon, $timecreated, $type)
    {
        $item = new \stdClass();
        $item->text = $text;
        $item->url = $url;
        $item->icon = $icon;
        $item->timecreated = $timecreated;
        $item->date = date('Y-m-d H:i:s', $timecreated);
        $item->type = $type;

        return $item;
    }

    public function getMyCourseHakgi($coursetype, $year, $semester, $semesters = null)
    {
        global $OUTPUT, $PAGE;

        if (empty($PAGE->context)) {
            $PAGE->set_context(\context_system::instance());
        }

        $CCourse = \local_ubion\course\Course::getInstance();

        // mustache에 전달할 내 강좌 목록
        $myCourses = array();

        if (empty($coursetype)) {
            if ($coursesAll = $CCourse->getMyCourseHakgi($year, $semester)) {
                foreach ($coursesAll as $cType => $courses) {
                    $mc = $CCourse->getMyCourseHakgiSetting($cType, $courses);

                    $myCourses[] = $mc;
                }
            }
        } else {
            // 선택한 coursetype에 속한 강좌 목록 가져오기
            $courseAll = $CCourse->getMyCoursesType($coursetype, $year, $semester);

            // 화면에 표시하기 위한 강좌 기본 셋팅
            $mc = $CCourse->getMyCourseHakgiSetting($coursetype, $courseAll);

            $myCourses[] = $mc;
        }

        $ctx = new \stdClass();
        $ctx->myCourses = $myCourses;

        if (empty($semesters)) {
            $semesters = \local_ubion\base\Common::getArrayKeyValueObject($CCourse->getSemesters(), $semester);
        }

        $ctx->semesters = $semesters;

        return $OUTPUT->render_from_template('theme_coursemos/coursemos-index-mycourses-hakgi', $ctx);
    }

    public function getMyCourses()
    {
        global $OUTPUT, $PAGE;

        if (empty($PAGE->context)) {
            $PAGE->set_context(\context_system::instance());
        }

        $CCourse = \local_ubion\course\Course::getInstance();

        // mustache에 전달할 내 강좌 목록
        $myCourses = array();

        if ($courses = $CCourse->getMyCourses()) {
            $myCourses = $CCourse->getMyCourseSetting($courses);
        }

        $ctx = new \stdClass();
        $ctx->myCourses = $myCourses;

        return $OUTPUT->render_from_template('theme_coursemos/coursemos-index-mycourses', $ctx);
    }

    /**
     * 사용가능한 학습자원/활동
     *
     * @param object $course
     * @param array $modnames
     * @param array $modules
     * @param int $section
     *            강좌 주차 번호 (x 주차)
     * @param string $sectionreturn
     *            모듈 등록 후 이동할 페이지
     * @param boolean $isKeyUrl
     *            배열의 키 값을 url로 리턴 (false시 modname으로 리턴시켜줌)
     * @return array[]
     */
    public function getMods($course, $modnames = null, $modules = null, $section = null, $sectionreturn = null, $isKeyUrl = true)
    {
        global $PAGE;

        if (empty($PAGE->context)) {
            $PAGE->set_context(\context_course::instance($course->id));
        }

        if (empty($modnames)) {
            $modnames = get_module_types_names();
        }

        if (empty($modules)) {
            $modules = get_module_metadata($course, $modnames, $sectionreturn);
        }

        $urlparams = null;
        if (! empty($section)) {
            $urlparams = array(
                'section' => $section
            );
        }

        // We'll sort resources and activities into two lists
        $activities = array(
            MOD_CLASS_ACTIVITY => array(),
            MOD_CLASS_RESOURCE => array()
        );

        // 설정에서 resources_sortorder, activities_sortorder 중에 한개라도 설정이 되어 있으면 설정된 순서대로 모듈이 표시되어야 함.
        $resourcesSortorder = get_config($this->pluginname, 'resources_sortorder');
        $activitiesSortorder = get_config($this->pluginname, 'activities_sortorder');

        // 설정된 값이 없으면 무들 기본 그대로 출력
        if (empty($resourcesSortorder) && empty($activitiesSortorder)) {
            foreach ($modules as $module) {
                $archetype = MOD_CLASS_ACTIVITY;
                if ($module->archetype == MOD_ARCHETYPE_RESOURCE) {
                    $archetype = MOD_CLASS_RESOURCE;
                } else if ($module->archetype === MOD_ARCHETYPE_SYSTEM) {
                    // System modules cannot be added by user, do not add to dropdown.
                    continue;
                }

                $link = ($isKeyUrl) ? $module->link->out(true, $urlparams) : $module->name;
                $activities[$archetype][$link] = $module->title;
            }
        } else {
            $tempActivities = array(
                MOD_CLASS_ACTIVITY => array(),
                MOD_CLASS_RESOURCE => array()
            );
            foreach ($modules as $module) {
                $archetype = MOD_CLASS_ACTIVITY;
                if ($module->archetype == MOD_ARCHETYPE_RESOURCE) {
                    $archetype = MOD_CLASS_RESOURCE;
                } else if ($module->archetype === MOD_ARCHETYPE_SYSTEM) {
                    // System modules cannot be added by user, do not add to dropdown.
                    continue;
                }
                $link = $module->link->out(true, $urlparams);

                $stdClass = new \stdClass();
                $stdClass->link = $link;
                $stdClass->title = $module->title;
                $stdClass->name = $module->name;

                $tempActivities[$archetype][$module->name] = $stdClass;
            }

            // 학습자원, 학습활동이 동일한 로직이기 때문에 익명함수로 처리
            $closureModuleSortorder = function ($activitiySortorder, $archetype) use (&$activities, &$tempActivities, $isKeyUrl) {
                if (! empty($activitiySortorder)) {
                    $explodeSortorder = explode(',', $activitiySortorder);
                    foreach ($explodeSortorder as $es) {
                        // 공백이 있을수도 있음.
                        $es = trim($es);

                        // 실제 모듈이 존재하는지 확인 존재한다면 $activities에 순서대로 반입시켜줘야됨.
                        if (isset($tempActivities[$archetype][$es])) {
                            $moduleInfo = $tempActivities[$archetype][$es];

                            $link = ($isKeyUrl) ? $moduleInfo->link : $moduleInfo->name;

                            $activities[$archetype][$link] = $moduleInfo->title;

                            // 입력된 항목보다 실제 설치된 모듈이 더 많을수도 있기 때문에 사용된 모듈은 배열에서 제거해야됨.
                            unset($tempActivities[$archetype][$es]);
                        }
                    }
                }

                // 설정에 실제 입력된 항목보다 설치된 항목이 많다면 정의된 순서 뒤에 모듈이 추가되면됨.
                if (! empty($tempActivities[$archetype])) {
                    foreach ($tempActivities[$archetype] as $moduleInfo) {
                        $link = ($isKeyUrl) ? $moduleInfo->link : $moduleInfo->name;

                        $activities[$archetype][$link] = $moduleInfo->title;
                    }

                    unset($tempActivities[$archetype]);
                }
            };

            $closureModuleSortorder($resourcesSortorder, MOD_ARCHETYPE_RESOURCE);
            $closureModuleSortorder($activitiesSortorder, MOD_CLASS_ACTIVITY);
        }

        return $activities;
    }

    /**
     * 페이지 하단 + 버튼 클릭시 표시되어야 학습자원/활동 목록
     *
     * @param array $mods
     * @return string
     */
    public function getCommonButtonModsHTML($mods = null)
    {
        global $OUTPUT;

        $html = '';
        $resources = null;
        $activities = null;

        if (isset($mods[MOD_ARCHETYPE_RESOURCE])) {
            foreach ($mods[MOD_ARCHETYPE_RESOURCE] as $modName => $modTitle) {
                $icon = $OUTPUT->image_url('course_format/mod_icon/' . $modName, $this->pluginname);
                $resources .= '<div class="mod ml-1 ml-md-2 mt-2">';
                $resources .= '<img src="' . $icon . '" alt="' . $modTitle . '" data-modname="' . $modName . '" title="' . $modTitle . '" class="mod-icon" data-toggle="tooltip" data-placement="top" />';
                $resources .= '<div class="mod-name mt-1 d-none d-md-block">' . $modTitle . '</div>';
                $resources .= '</div>';
            }
        }

        if (isset($mods[MOD_CLASS_ACTIVITY])) {
            foreach ($mods[MOD_CLASS_ACTIVITY] as $modName => $modTitle) {
                $icon = $OUTPUT->image_url('course_format/mod_icon/' . $modName, $this->pluginname);
                $activities .= '<div class="mod ml-1 ml-md-2 mt-2">';
                $activities .= '<img src="' . $icon . '" alt="' . $modTitle . '" data-modname="' . $modName . '" title="' . $modTitle . '" class="mod-icon" data-toggle="tooltip" data-placement="top" />';
                $activities .= '<div class="mod-name mt-1 d-none d-md-block">' . $modTitle . '</div>';
                $activities .= '</div>';
            }
        }

        if (! empty($resources)) {
            $html .= '<div class="button-item button-item-mods mt-3">';
            $html .= $resources;
            $html .= '</div>';
        }

        if (! empty($activities)) {
            $html .= '<div class="button-item button-item-mods mt-3">';
            $html .= $activities;
            $html .= '</div>';
        }

        return $html;
    }

    public function getLangChangeHTML()
    {
        global $CFG, $OUTPUT, $PAGE;

        $html = '';

        $currlang = current_language();
        $langs = get_string_manager()->get_list_of_translations();

        // lang 관련 파라메터가 존재하면 lang 파라메터를 삭제하고 아래 language쪽에서 다시 셋팅해줘야됨.
        if (isset($_GET['lang'])) {
            unset($_GET['lang']);
        }

        $url = $CFG->wwwroot;
        if ($_GET) {
            $url .= '?' . http_build_query($_GET) . '&amp;lang=';
        } else {
            $url .= '?lang=';
        }

        // $langName = (isset($langs[$currlang]) ? $langs[$currlang] : $langs['en']);
        if (isset($langs[$currlang])) {
            $flag = $OUTPUT->image_url('flag/' . $currlang, $this->pluginname);
            $langName = '<img src="' . $flag . '" alt="" />' . '&nbsp;&nbsp;' . $langs[$currlang];
        } else {
            $flag = $OUTPUT->image_url('flag/en', $this->pluginname);
            $langName = '<img src="' . $flag . '" alt="" />' . '&nbsp;&nbsp;' . $langs['en'];
        }

        $html .= '<div class="dropdown dropdown-lang mt-3">';
        $html .= '<a id="user-info-lang" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $langName . ' <span class="caret"></span> </a>';

        $html .= '<ul class="dropdown-menu" aria-labelledby="user-info-lang">';

        $courseLang = $PAGE->course->lang ?? null;

        if (! empty($courseLang)) {
            $html .= '<li class="nochange"><div class="force_lang">' . get_string('fixed_course_lang', $this->pluginname, $this->page->course->lang) . '</div></li>';
        } else {
            foreach ($langs as $key => $value) {
                $flag = $OUTPUT->image_url('flag/' . $key, $this->pluginname);
                $html .= '<li><a href="' . $url . $key . '" title="' . $value . '" class="a-lang" data-lang="' . $key . '"><img src="' . $flag . '" alt="' . $key . '" class="lang_flag" /> ' . $value . '</a></li>';
            }
        }
        $html .= '</ul>';
        $html .= '</div>';

        return $html;
    }
}