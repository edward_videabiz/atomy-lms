<?php
namespace theme_coursemos;

class Html extends \theme_coursemos\core\Html 
{
    private static $instance;
    
    /**
     * html Class
     * @return Html
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
}