<?php
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2018033000;
$plugin->requires  = 2017111301;
$plugin->component = 'theme_coursemos';
$plugin->dependencies = array('local_ubion' => 2017061500);