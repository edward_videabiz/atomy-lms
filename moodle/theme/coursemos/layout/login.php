<?php
defined('MOODLE_INTERNAL') || die();

$pluginname = 'theme_coursemos';

$CThemeHtml = \theme_coursemos\Html::getInstance();
$CThemeLogin = \theme_coursemos\Login::getInstance();

$loginimageCount = $CThemeLogin->getBackgroundViewCount();
if ($loginimageCount > 1) {
    $loginimage = $CThemeLogin->getBackgroundCSSNamePrefix() . rand(1, $loginimageCount) . ' ';
} else {
    // 따로 설정된 값이 없다면 1번째 항목이 표시
    $loginimage = $CThemeLogin->getBackgroundCSSNamePrefix() . '1 ';
}

$extraclasses = [
    'coursemos-loginpage',
    $loginimage
];

// 모바일 브라우저 주소줄 색상 변경 (예 : 나무위키)
$mobileBrowserThemeColor = $CThemeHtml->getMobileThemeColor();

$bodyattributes = $OUTPUT->body_attributes($extraclasses);

// footer 영역에 전달될 내용
$footer = new \stdClass();
$footer->copyright = $PAGE->theme->settings->copyright ?? '';
$footer->privacyurl = $PAGE->theme->settings->privacyurl ?? '';

$templatecontext = [
    'sitename' => $SITE->shortname,
    'output' => $OUTPUT,
    'bodyattributes' => $bodyattributes,
    'footer' => $footer,
    'mobileBrowserThemeColor' => $mobileBrowserThemeColor
];

echo $OUTPUT->render_from_template('theme_coursemos/layout-login', $templatecontext);