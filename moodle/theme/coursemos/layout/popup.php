<?php
defined('MOODLE_INTERNAL') || die();

$CThemeHtml = \theme_coursemos\Html::getInstance();

// 모바일 브라우저 주소줄 색상 변경 (예 : 나무위키)
$mobileBrowserThemeColor = $CThemeHtml->getMobileThemeColor();

$extraclasses = ['no-header'];
$bodyattributes = $OUTPUT->body_attributes($extraclasses);

$templatecontext = [
    // We cannot pass the context to format_string, this layout can be used during
    // installation. At that stage database tables do not exist yet.
    'sitename' => $SITE->shortname,
    'output' => $OUTPUT,
    'bodyattributes' => $bodyattributes,
    'mobileBrowserThemeColor' => $mobileBrowserThemeColor
];

echo $OUTPUT->render_from_template('theme_coursemos/layout-popup', $templatecontext);
