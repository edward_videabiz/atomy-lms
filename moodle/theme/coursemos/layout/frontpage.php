<?php
defined('MOODLE_INTERNAL') || die();

$CThemeHtml = \theme_coursemos\Html::getInstance();
$CFamilySites = \theme_coursemos\FamilySites::getInstance();

$extraclasses = array();
$bodyattributes = $OUTPUT->body_attributes($extraclasses);

// front 페이지는 강좌 페이지가 아니므로 false로 전달 (해당 값은 layout-menus.mustache에서 사용됩니다.)
$isCoursePage = false;

// 메뉴 가져오기
$CMenu = \local_ubmenu\Menu::getInstance();

// 사이트 메뉴는 모든 페이지에서 호출됨.
$siteMenu = $CMenu->getSiteMenus();


// 강좌 공지사항
$UBBoardNotices = array();

// 게시판 자체를 숨기거나 사용하지 않는 경우는 지금까지 없었기 때문에 별도의 조건문 처리하지 않음.
// 게시판을 사용하지 않는 다면 강좌 공지사항 가져오는 부분을 주석처리하시면 됩니다.
if ($UBBoardNotices = \mod_ubboard\latest::getCacheCoursNotices($PAGE->course->id)) {
    // key를 제거해야 mustache에서 반복문을 돌릴수 있음.
    $UBBoardNotices = array_values($UBBoardNotices);
}

$blockshtml = '';
if ($PAGE->blocks->is_known_region('side-pre')) {
    $blockshtml .= $OUTPUT->blocks('side-pre');
}

if ($PAGE->blocks->is_known_region('side-post')) {
    $blockshtml .= $OUTPUT->blocks('side-post');
}

$col = new \stdClass();
$col->dashboard = 'col-lg-8 col-xl-9 ';
$col->notifications = 'col-lg-4 col-xl-3 ';

if (empty($blockshtml)) {
    $col->dashboard = 'col-xs-12';
    $col->notifications = '';
}

// footer 영역에 전달될 내용
$footer = new \stdClass();
$footer->copyright = $PAGE->theme->settings->copyright ?? '';
$footer->privacyurl = $PAGE->theme->settings->privacyurl ?? '';
$footer->familysites = $CFamilySites->getCacheListHeaderFamily();

$templatecontext = [
    'output' => $OUTPUT,
    'sitename' => $SITE->shortname,
    'bodyattributes' => $bodyattributes,
    'isCoursePage' => $isCoursePage,
    'siteMenu' => $siteMenu,
    'blockshtml' => $blockshtml,
    'col' => $col,
    'UBBoardNotices' => $UBBoardNotices,
    'footer' => $footer
];

echo $OUTPUT->render_from_template('theme_coursemos/layout-frontpage', $templatecontext);