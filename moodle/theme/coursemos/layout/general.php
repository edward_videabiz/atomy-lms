<?php
defined('MOODLE_INTERNAL') || die();

$pluginname = 'theme_coursemos';

$CCourse = \local_ubion\course\Course::getInstance();
$CThemeHtml = \theme_coursemos\Html::getInstance();
$CFamilySites = \theme_coursemos\FamilySites::getInstance();

// user_preference_allow_ajax_update('drawer-open-nav', PARAM_ALPHA);
// require_once($CFG->libdir . '/behat/lib.php');

$PAGE->set_popup_notification_allowed(false);

if (isloggedin()) {
    $navdraweropen = (get_user_preferences('drawer-open-nav', 'true') == 'true');
} else {
    $navdraweropen = false;
}

// 교수 여부
$isProfessor = $CCourse->isProfessor($PAGE->course->id);

$extraclasses = [
    'coursemos-layout-course',
    'coursemos-headroom-pin'
];

// 브라우저 정보 표시
// ie인 경우에는 예외처리 해줘야되는 경우가 있음.
if (core_useragent::is_ie()) {
    $extraclasses[] = 'ie';
    $extraclasses[] = 'ie' . core_useragent::check_ie_properties()['version'];
} else {
    if (core_useragent::is_edge()) {
        $extraclasses[] = 'ie-edge';
    }
}

$blockshtml = $OUTPUT->blocks('side-pre');
$hasblocks = strpos($blockshtml, 'data-block=') !== false;

$isCoursePage = ($PAGE->course->id > SITEID) ? true : false;
$scriptName = $_SERVER['SCRIPT_NAME'];
$isCourseMain = (strpos($scriptName, '/course/view.php') !== false) ? true : false;
$isCourseHeader = ($isCoursePage && $isCourseMain) ? true : false;
$isNavCourseSections = false;

$courseEditButton = new \stdClass();
$courseEditButton->is = false;
$courseEditButton->headingButton = $OUTPUT->page_heading_button();
$courseEditButton->modButtons = null;

// 현재는 편집 버튼만 존재하기 때문에 편집 버튼 여부에 따라서 courseEditButton 표시 여부가 결정됨.
// 포럼 페이지에서는 검색 폼으로 인해 화면이 깨지는 문제가 발생됨.
// 해당 페이지에서 에디터 버튼이 표시되지 않아도 사용하는데 문제가 없기 때문에 포럼 페이지에서는 editbutton이 표시되지 않도록 수정함.
if (! empty($courseEditButton->headingButton) && strpos($scriptName, '/mod/forum/') === false) {
    if ($isProfessor) {
        $courseEditButton->is = true;
    }
}

if ($isCoursePage && $isCourseMain) {
    $courseFormat = $CCourse->getCourseFormat($PAGE->course->id);
    $course = $courseFormat->get_course();

    // sections가 여러개 존재하는 하는 경우
    if ($courseFormat->uses_sections()) {
        // courseviewtype (section형태, tab 형태)가 section형태로 출력되는 경우에만 출력
        if (isset($course->courseviewtype) && $course->courseviewtype == $CCourse::VIEWTYPE_SECTION) {
            $isNavCourseSections = true;
        }
    }

    $mods = $CThemeHtml->getMods($PAGE->course, null, null, null, null, false);
    $courseEditButton->modButtons = $CThemeHtml->getCommonButtonModsHTML($mods);
}


// 강좌 메뉴 관련해서 필요한 정보 전달해줘야됨.
$menuHelper = new \stdClass();
$menuHelper->courseurl = $CFG->wwwroot . '/course/view.php?id=' . $PAGE->course->id;

$menuHelper->show = new \stdClass();
$menuHelper->show->title = get_string('block_show', $pluginname);
$menuHelper->show->icon = $OUTPUT->image_url('t/switch_minus');

$menuHelper->hide = new \stdClass();
$menuHelper->hide->title = get_string('block_hide', $pluginname);
$menuHelper->hide->icon = $OUTPUT->image_url('t/switch_plus');

// 학습활동 블록
$menuHelper->roleChangeHTML = $CThemeHtml->getRoleChange($PAGE->course->id, $isProfessor);

// 메뉴 가져오기
$CMenu = \local_ubmenu\Menu::getInstance();

// 사이트 메뉴는 모든 페이지에서 호출됨.
$siteMenu = $CMenu->getSiteMenus();

$courseurl = $CFG->wwwroot;
$coursename = $SITE->fullname;

// 강좌에서만 사용되는 변수
// 강좌 메뉴
$courseMenu = null;

// 주차정보
$courseSections = null;

// 메뉴 표시 여부 
// 강좌 메뉴 숨김 (2019-05-20)사용자는 무들 페이지처럼 느껴지면 안되기 때문에 메뉴 정보 모두 숨김처리
// 관리자는 메뉴를 표시해야될수도 있기 때문에 css로 메뉴 표시 여부 결정
$is = new stdClass();
$is->lnb = \local_vn\Course::getInstance()->isLnb($PAGE->course->id);
$is->section = $is->lnb && $isCoursePage; // 강좌 주차 출력여부
$is->courseMenu = false;    // 강좌 메뉴 출력여부

if ($isCoursePage) {
    $extraclasses[] = 'coursemos-coursepage';

    if ($is->lnb) {
        $courseMenu = $CMenu->getCourseMenus($PAGE->course->id);
        if (!empty($courseMenu)) {
            $is->courseMenu = true;
        }
    }
    $coursename = $CCourse->getName($PAGE->course);
    $courseurl = $CFG->wwwroot . '/course/view.php?id=' . $PAGE->course->id;

    // 현재 편집모드 활성화 상태이면 off 비 활성화 상태이면 on
    $courseEditParamValue = 'on';
    if (isset($USER->editing) && $USER->editing) {
        $courseEditButton->text = get_string('turneditingoff');
        $courseEditParamValue = 'off';
    }
    $courseEditButton->url = $courseurl . '&edit=' . $courseEditParamValue . '&sesskey=' . sesskey();

    $courseSections = $CThemeHtml->getCourseSections($PAGE->course->id, $isCourseMain);
    
}

// 퀴즈 페이지에서는 시간 및 퀴즈 네비게이션이 출력되어야 함.
// 메뉴 표시여부에 따라서 강좌명 및 강좌 주소 또는 홈(베트남 전용홈) 명칭이 출력될지 결정됨.
if (empty($is->lnb)) {
    $isAllowBlockPage = [
        '/mod/quiz/attempt.php',
        '/mod/quiz/review.php',
    ];
    
    // 특정 페이지에서는 fake block이 표시되어야 하므로 예외처리 해줘야됨.
    if (in_array($scriptName, $isAllowBlockPage)) {
        $is->lnb = true;
    }
    
    if (empty($is->lnb)) {
        $extraclasses[] = 'coursemos-menu-none';
    }
    
    $courseurl = $CFG->wwwroot.'/local/vn/index.php?id='.$PAGE->course->id;
    $coursename = get_string('home');
}

$devicetype = core_useragent::get_device_type();

if ($devicetype == core_useragent::DEVICETYPE_MOBILE) {
    $extraclasses[] = 'coursemos-device-mobile';
} else if ($devicetype == core_useragent::DEVICETYPE_TABLET) {
    $extraclasses[] = 'coursemos-device-tablet';
}

// 모바일 브라우저 주소줄 색상 변경 (예 : 나무위키)
$mobileBrowserThemeColor = $CThemeHtml->getMobileThemeColor();

// 강좌 공지사항
$UBBoardNotices = array();
// 강좌 메인에서만 표시되어야 함.
if ($isCourseMain) {
    // 게시판 자체를 숨기거나 사용하지 않는 경우는 지금까지 없었기 때문에 별도의 조건문 처리하지 않음.
    // 게시판을 사용하지 않는 다면 강좌 공지사항 가져오는 부분을 주석처리하시면 됩니다.
    if ($UBBoardNotices = \mod_ubboard\latest::getCacheCoursNotices($PAGE->course->id)) {
        // key를 제거해야 mustache에서 반복문을 돌릴수 있음.
        $UBBoardNotices = array_values($UBBoardNotices);
    }
}

$bodyattributes = $OUTPUT->body_attributes($extraclasses);
// $regionmainsettingsmenu = $OUTPUT->region_main_settings_menu();

// footer 영역에 전달될 내용
$footer = new \stdClass();
$footer->copyright = $PAGE->theme->settings->copyright ?? '';
$footer->privacyurl = $PAGE->theme->settings->privacyurl ?? '';
$footer->familysites = $CFamilySites->getCacheListHeaderFamily();

$templatecontext = [
    'sitename' => $SITE->shortname,
    'output' => $OUTPUT,
    'sidepreblocks' => $blockshtml,
    'hasblocks' => $hasblocks,
    'bodyattributes' => $bodyattributes,
    'navdraweropen' => $navdraweropen,
    // 'regionmainsettingsmenu' => $regionmainsettingsmenu,
    // 'hasregionmainsettingsmenu' => !empty($regionmainsettingsmenu),
    'isCourseHeader' => $isCourseHeader,
    'isCoursePage' => $isCoursePage,
    'isCourseMain' => $isCourseMain,
    'isNavCourseSections' => $isNavCourseSections,
    'siteMenu' => $siteMenu,
    'courseMenu' => $courseMenu,
    'courseSections' => $courseSections,
    'menuHelper' => $menuHelper,
    'isProfessor' => $isProfessor,
    'coursename' => $coursename,
    'courseurl' => $courseurl,
    'courseEditButton' => $courseEditButton,
    'mobileBrowserThemeColor' => $mobileBrowserThemeColor,
    'UBBoardNotices' => $UBBoardNotices,
    'footer' => $footer,
    'is' => $is
];

echo $OUTPUT->render_from_template('theme_coursemos/layout-general', $templatecontext);

