<?php
// /local/ubion/Asite/index.php에 의해서 페이지 호출이 되기때문에 이곳에서 별도로 config를 호출하거나, locallib.php을 호출할 필요가 없습니다.

global $SITECFG;

$pluginname = 'theme_coursemos';

$CAsite = \local_ubion\asite\Asite::getInstance();
$CCourse = \local_ubion\course\Course::getInstance();
$CThemeHtml = \theme_coursemos\Html::getInstance();


if (!is_siteadmin()) {
    exit;
}

// 모바일 브라우저 주소줄 색상 변경 (예 : 나무위키)
$mobileBrowserThemeColor = $CThemeHtml->getMobileThemeColor();

// 메뉴 정의
$siteMenu = $CAsite->geSitetMenus();
$siteMenuSub = $CAsite->geSitetMenuSub();
$contentBreadcrumb = $CAsite->getBreadcrumb();

$extraclasses = ['coursemos-admin'];
$bodyattributes = $OUTPUT->body_attributes($extraclasses);

$headAddHTML = $CAsite->sPrintJs();
$headAddHTML .= $CAsite->sPrintCss();



$langHTML = '';

$langHTML .= '<li role="presentation" class="nav-item nav-item-lang">';
// 언어 선택
$currlang = current_language();
$langs = get_string_manager()->get_list_of_translations();

// lang 관련 파라메터가 존재하면 lang 파라메터를 삭제하고 아래 language쪽에서 다시 셋팅해줘야됨.
if (isset($_GET['lang'])) {
    unset($_GET['lang']);
}

$url = $CFG->wwwroot . $_SERVER['SCRIPT_NAME'];
if ($_GET) {
    $url .= '?' . http_build_query($_GET) . '&amp;lang=';
} else {
    $url .= '?lang=';
}

// $langName = (isset($langs[$currlang]) ? $langs[$currlang] : $langs['en']);
if (isset($langs[$currlang])) {
    $flag = $OUTPUT->image_url('flag/' . $currlang, 'theme_coursemos');
    $langName = '<img src="' . $flag . '" alt="" />' . '&nbsp;&nbsp;' . $langs[$currlang];
} else {
    $flag = $OUTPUT->image_url('flag/en', 'theme_coursemos');
    $langName = '<img src="' . $flag . '" alt="" />' . '&nbsp;&nbsp;' . $langs['en'];
}

$langHTML = '';

$courseLang = $PAGE->course->lang ?? null;

if (!empty($courseLang)) {
    $langHTML .= '<div class="force-lang dropdown-item">' . get_string('fixed_course_lang', 'theme_coursemos', $this->page->course->lang) . '</div>';
} else {
    foreach ($langs as $key => $value) {
        $flag = $this->image_url('flag/' . $key, 'theme_coursemos');
        $langHTML .= '<a href="' . $url . $key . '" title="' . $value . '" class="a-lang dropdown-item" data-lang="' . $key . '"><img src="' . $flag . '" alt="' . $key . '" class="lang-flag mr-2" /> ' . $value . '</a>';
    }
}

$templatecontext = [
    'output' => $OUTPUT,
    'SITECFG' => $SITECFG, 
    'siteMenu' => $siteMenu,
    'contentBreadcrumb' => $contentBreadcrumb,
    'siteMenuSub' => $siteMenuSub,
    'mobileBrowserThemeColor' => $mobileBrowserThemeColor,
    'bodyattributes' => $bodyattributes,
    'headAddHTML' => $headAddHTML,
    'langHTML' => $langHTML
];

echo $OUTPUT->render_from_template('theme_coursemos/layout-coursemosadmin', $templatecontext);