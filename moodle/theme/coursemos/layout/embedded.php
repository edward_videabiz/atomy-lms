<?php
defined('MOODLE_INTERNAL') || die();

$templatecontext = [
    'sitename' => $SITE->shortname,
    'output' => $OUTPUT
];

echo $OUTPUT->render_from_template('theme_coursemos/embedded', $templatecontext);
