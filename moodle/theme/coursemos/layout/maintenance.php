<?php
defined('MOODLE_INTERNAL') || die();

$templatecontext = [
    // We cannot pass the context to format_string, this layout can be used during
    // installation. At that stage database tables do not exist yet.
    'sitename' => $SITE->shortname,
    'output' => $OUTPUT,
    'bodyattributes' => $OUTPUT->body_attributes(array(
        'noscrollert'
    ))
];

echo $OUTPUT->render_from_template('theme_coursemos/maintenance', $templatecontext);
