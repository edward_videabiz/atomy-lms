<?php

function xmldb_theme_coursemos_upgrade($oldversion) {
    global $DB;
    
    $dbman = $DB->get_manager();
    
    if ($oldversion < 2018032300) {
        
        // Define table theme_coursemos_strings to be created.
        $table = new xmldb_table('theme_coursemos_strings');
        
        // Adding fields to table theme_coursemos_strings.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('type', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null);
        $table->add_field('stringid', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('value', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('icon', XMLDB_TYPE_CHAR, '1000', null, null, null, null);
        $table->add_field('url', XMLDB_TYPE_CHAR, '1000', null, null, null, null);
        $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '4', null, null, null, '100');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '8', null, null, null, null);
        
        // Adding keys to table theme_coursemos_strings.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('gubun', XMLDB_KEY_UNIQUE, array('type', 'stringid'));
        
        // Conditionally launch create table for theme_coursemos_strings.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        
        // Coursemos savepoint reached.
        upgrade_plugin_savepoint(true, 2018032300, 'theme', 'coursemos');
    }
 
    if ($oldversion < 2018032301) {
        upgrade_plugin_savepoint(true, 2018032301, 'theme', 'coursemos');
    }
    
    
    if ($oldversion < 2018033000) {
        
        // Define table theme_coursemos_login_banner to be created.
        $table = new xmldb_table('theme_coursemos_login_banner');
        
        // Adding fields to table theme_coursemos_login_banner.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('image_ko', XMLDB_TYPE_CHAR, '1000', null, XMLDB_NOTNULL, null, null);
        $table->add_field('image_en', XMLDB_TYPE_CHAR, '1000', null, XMLDB_NOTNULL, null, null);
        $table->add_field('url_ko', XMLDB_TYPE_CHAR, '1000', null, null, null, null);
        $table->add_field('url_en', XMLDB_TYPE_CHAR, '1000', null, null, null, null);
        $table->add_field('newwindow_ko', XMLDB_TYPE_INTEGER, '1', null, null, null, '0');
        $table->add_field('newwindow_en', XMLDB_TYPE_INTEGER, '1', null, null, null, '0');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, null);
        $table->add_field('sortorder', XMLDB_TYPE_INTEGER, '8', null, null, null, '100');
        $table->add_field('visible', XMLDB_TYPE_INTEGER, '1', null, null, null, '1');
        
        // Adding keys to table theme_coursemos_login_banner.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        
        // Adding indexes to table theme_coursemos_login_banner.
        $table->add_index('visible', XMLDB_INDEX_NOTUNIQUE, array('visible'));
        
        // Conditionally launch create table for theme_coursemos_login_banner.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Coursemos savepoint reached.
        upgrade_plugin_savepoint(true, 2018033000, 'theme', 'coursemos');
    }
    
}