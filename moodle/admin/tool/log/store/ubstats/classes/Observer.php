<?php
namespace logstore_ubstats;

/**
 * Event observer for local_ubion.
 */
class Observer
{

    /**
     * Triggered via course_module_created event.
     *
     * @param \core\event\course_module_created $event
     */
    public static function course_module_created(\core\event\course_module_created $event)
    {

        /**
         * $event sample data
         * core\\event\\course_module_created Object
         * (
         * [data:protected] => Array
         * (
         * [eventname] => \\core\\event\\course_module_created
         * [component] => core
         * [action] => created
         * [target] => course_module
         * [objecttable] => course_modules
         * [objectid] => 73
         * [crud] => c
         * [level] => 1
         * [contextid] => 6980
         * [contextlevel] => 70
         * [contextinstanceid] => 73
         * [userid] => 2
         * [courseid] => 2
         * [relateduserid] =>
         * [other] => Array
         * (
         * [modulename] => forum
         * [name] => bbbbbbbbbbbbb
         * [instanceid] => 18
         * )
         *
         * [timecreated] => 1386222977
         * )
         *
         * [logextra:protected] =>
         * [context:protected] => context_module Object
         * (
         * [_id:protected] => 6980
         * [_contextlevel:protected] => 70
         * [_instanceid:protected] => 73
         * [_path:protected] => /1/190/191/194/15/6980
         * [_depth:protected] => 6
         * )
         *
         * [triggered:core\\event\\base:private] => 1
         * [dispatched:core\\event\\base:private] =>
         * [restored:core\\event\\base:private] =>
         * [recordsnapshots:core\\event\\base:private] => Array
         * (
         * )
         *
         * )
         */
        $cmid = $event->objectid;
        $courseid = $event->courseid;

        // 강좌 번호가 SITEID 인 경우에는 제외해야됨.
        if ($courseid != SITEID) {

            // course_modules에서 필요한 정보 가져오기
            if ($sectionInfo = self::get_course_module_sections($cmid)) {

                $CCourse = \local_ubion\course\Course::getInstance();

                $year = date('Y');
                $semester = $CCourse::CODE_99;

                // 강좌의 year, semester값 가져오기
                if ($courseUbion = $CCourse->getUbion($courseid)) {
                    $year = $courseUbion->year;
                    $semester = $courseUbion->semester_code;
                }

                // 연도, 학기가 빈값이 아닌 경우 통계 데이터 입력
                if (! empty($year) && ! empty($semester)) {
                    $CStats = new \logstore_ubstats\Stats();
                    $CStats->setModuleCountExecute($year, $semester, $courseid, $sectionInfo->section, $event->other['modulename']);
                }
            }
        }
    }

    /**
     * Triggered via course_module_updated event.
     *
     * @param \core\event\course_module_updated $event
     */
    public static function course_module_updated(\core\event\course_module_updated $event)
    {

    /**
     * $event sample data
     * core\\event\\course_module_updated Object
     * (
     * [data:protected] => Array
     * (
     * [eventname] => \\core\\event\\course_module_updated
     * [component] => core
     * [action] => updated
     * [target] => course_module
     * [objecttable] => course_modules
     * [objectid] => 79
     * [crud] => u
     * [level] => 1
     * [contextid] => 6986
     * [contextlevel] => 70
     * [contextinstanceid] => 79
     * [userid] => 2
     * [courseid] => 2
     * [relateduserid] =>
     * [other] => Array
     * (
     * [modulename] => forum
     * [name] => qqqqq
     * [instanceid] => 24
     * )
     *
     * [timecreated] => 1386226341
     * )
     *
     * [logextra:protected] =>
     * [context:protected] => context_module Object
     * (
     * [_id:protected] => 6986
     * [_contextlevel:protected] => 70
     * [_instanceid:protected] => 79
     * [_path:protected] => /1/190/191/194/15/6986
     * [_depth:protected] => 6
     * )
     *
     * [triggered:core\\event\\base:private] => 1
     * [dispatched:core\\event\\base:private] =>
     * [restored:core\\event\\base:private] =>
     * [recordsnapshots:core\\event\\base:private] => Array
     * (
     * )
     *
     * )
     */

        // 별다른 행위 없음
    }

    /**
     * Triggered via user_enrolment_deleted event.
     *
     * @param \core\event\course_module_deleted $event
     */
    public static function course_module_deleted(\core\event\course_module_deleted $event)
    {
        global $DB;

        /**
         * $event sample data
         * core\\event\\course_module_deleted Object
         * (
         * [data:protected] => Array
         * (
         * [eventname] => \\core\\event\\course_module_deleted
         * [component] => core
         * [action] => deleted
         * [target] => course_module
         * [objecttable] => course_modules
         * [objectid] => 69
         * [crud] => d
         * [level] => 1
         * [contextid] => 6976
         * [contextlevel] => 70
         * [contextinstanceid] => 69
         * [userid] => 2
         * [courseid] => 2
         * [relateduserid] =>
         * [other] => Array
         * (
         * [modulename] => forum
         * [instanceid] => 14
         * )
         *
         * [timecreated] => 1386208049
         * )
         *
         * [logextra:protected] =>
         * [context:protected] => context_module Object
         * (
         * [_id:protected] => 6976
         * [_contextlevel:protected] => 70
         * [_instanceid:protected] => 69
         * [_path:protected] => /1/190/191/194/15/6976
         * [_depth:protected] => 6
         * )
         *
         * [triggered:core\\event\\base:private] => 1
         * [dispatched:core\\event\\base:private] =>
         * [restored:core\\event\\base:private] =>
         * [recordsnapshots:core\\event\\base:private] => Array
         * (
         * [course_modules] => Array
         * (
         * [69] => stdClass Object
         * (
         * [id] => 69
         * [course] => 2
         * [module] => 9
         * [instance] => 14
         * [section] => 1 // course_sections 테이블 고유 아이디 임(강좌 주차가 아닙니다.)
         * [idnumber] =>
         * [added] => 1386208038
         * [score] => 0
         * [indent] => 0
         * [visible] => 1
         * [visibleold] => 1
         * [groupmode] => 0
         * [groupingid] => 0
         * [groupmembersonly] => 0
         * [completion] => 1
         * [completiongradeitemnumber] =>
         * [completionview] => 0
         * [completionexpected] => 0
         * [availablefrom] => 0
         * [availableuntil] => 0
         * [showavailability] => 1
         * [showdescription] => 0
         * )
         *
         * )
         *
         * )
         *
         * )
         */

        // coursemodule id
        $cmid = $event->objectid;
        $courseid = $event->courseid;

        // 강좌번호가 SITEID인 경우에는 제외해야됨.
        if ($courseid > SITEID) {

            $cm = $event->get_record_snapshot('course_modules', $cmid);

            // $cm->section값은 {course_sections}->id값이므로 section값을 재 조회해야됨.
            // 참고로 course_modules가 삭제된 후 호출되기 때문에 self::get_course_module_sections($cmid) 리턴 값은 null이 전달됩니다.
            $query = "SELECT
                                section
                      FROM      {course_sections}
                      WHERE     id = :id";

            $param = [
                'id' => $cm->section
            ];

            $section = $DB->get_field_sql($query, $param);

            // 주차 정보가 없으면 패스
            if (! empty($section)) {
                $CCourse = \local_ubion\course\Course::getInstance();

                $year = date('Y');
                $semester = $CCourse::CODE_99;

                // 강좌의 year, semester값 가져오기
                if ($courseUbion = $CCourse->getUbion($courseid)) {
                    $year = $courseUbion->year;
                    $semester = $courseUbion->semester_code;
                }

                // 연도, 학기가 빈값이 아닌 경우 통계 데이터 입력
                if (! empty($year) && ! empty($semester)) {
                    $CStats = new \logstore_ubstats\Stats();
                    $CStats->setModuleCountUpdate($year, $semester, $courseid, $section, $event->other['modulename']);
                }
            }
        }
    }

    /**
     * Triggered via local_ubion_course_module_visible event.
     *
     * @param \local_ubion\event\course_module_visible $event
     */
    public static function course_module_visible(\local_ubion\event\course_module_visible $event)
    {

    /**
     * core\\event\\local_ubion_course_module_visible Object
     * (
     * [data:protected] => Array
     * (
     * [eventname] => \\core\\event\\local_ubion_course_module_visible
     * [component] => core
     * [action] => visible
     * [target] => local_ubion_course_module
     * [objecttable] => course_modules
     * [objectid] => 79
     * [crud] => d
     * [level] => 1
     * [contextid] => 6986
     * [contextlevel] => 70
     * [contextinstanceid] => 79
     * [userid] => 2
     * [courseid] => 2
     * [relateduserid] =>
     * [other] => Array
     * (
     * [modulename] => forum
     * [instanceid] => 24
     * )
     *
     * [timecreated] => 1386229458
     * )
     *
     * [logextra:protected] =>
     * [context:protected] => context_module Object
     * (
     * [_id:protected] => 6986
     * [_contextlevel:protected] => 70
     * [_instanceid:protected] => 79
     * [_path:protected] => /1/190/191/194/15/6986
     * [_depth:protected] => 6
     * )
     *
     * [triggered:core\\event\\base:private] => 1
     * [dispatched:core\\event\\base:private] =>
     * [restored:core\\event\\base:private] =>
     * [recordsnapshots:core\\event\\base:private] => Array
     * (
     * )
     *
     * )
     */

        // 숨김시 따로 처리할 내용이 없음
    }

    /**
     * Triggered via local_ubion_course_module_move event.
     *
     * @param \local_ubion\event\course_module_move $event
     */
    public static function course_module_move(\local_ubion\event\course_module_move $event)
    {
        global $DB;

        /**
         * [data:protected] => Array
         * (
         * [eventname] => \local_ubion\event\course_module_move
         * [component] => local_ubion
         * [action] => move
         * [target] => course_module
         * [objecttable] => course_modules
         * [objectid] => 13
         * [crud] => u
         * [edulevel] => 1
         * [contextid] => 116083
         * [contextlevel] => 70
         * [contextinstanceid] => 13
         * [userid] => 2
         * [courseid] => 2
         * [relateduserid] =>
         * [anonymous] => 0
         * [other] => Array
         * (
         * [modulename] => assign
         * [instanceid] => 9
         * [oldsectionid] => 2
         * [newsectionid] => 35
         * )
         *
         * [timecreated] => 1535348069
         * )
         *
         * [logextra:protected] =>
         * [context:protected] => context_module Object
         * (
         * [_id:protected] => 116083
         * [_contextlevel:protected] => 70
         * [_instanceid:protected] => 13
         * [_path:protected] => /1/3/23/116083
         * [_depth:protected] => 4
         * )
         *
         * [triggered:core\event\base:private] => 1
         * [dispatched:core\event\base:private] =>
         * [restored:core\event\base:private] =>
         * [recordsnapshots:core\event\base:private] => Array
         * (
         * )
         */

        // 진도처리 부분은 local/ubonattend에서 진행되기 때문에
        // 이곳에서는 통계 테이블에 대해서만 처리하면 됩니다.
        $courseid = $event->courseid;
        $cmid = $event->objectid;

        // 강좌 번호가 SITEID 가 아니고 주차 이동된 경우
        if ($courseid != SITEID && $event->other['oldsectionid'] != $event->other['newsectionid']) {

            // course_modules에서 필요한 정보 가져오기
            if ($sectionInfo = self::get_course_module_sections($cmid)) {

                $CCourse = \local_ubion\course\Course::getInstance();

                $year = date('Y');
                $semester = $CCourse::CODE_99;

                // 강좌의 year, semester값 가져오기
                if ($courseUbion = $CCourse->getUbion($courseid)) {
                    $year = $courseUbion->year;
                    $semester = $courseUbion->semester_code;
                }

                // 연도, 학기가 빈값이 아닌 경우 통계 데이터 입력
                if (! empty($year) && ! empty($semester)) {
                    $CStats = new \logstore_ubstats\Stats();

                    $query = "SELECT
                                section
                      FROM      {course_sections}
                      WHERE     id = :id";

                    $param = [
                        'id' => $event->other['oldsectionid']
                    ];

                    if ($oldSection = $DB->get_field_sql($query, $param)) {
                        // 이동전 주차에서 모듈 등록 갯수 빼줘야됨.
                        $CStats->setModuleCountUpdate($year, $semester, $courseid, $oldSection, $event->other['modulename']);
                    }

                    // 이동된 주차에 모듈 등록 갯수 추가
                    $CStats->setModuleCountExecute($year, $semester, $courseid, $sectionInfo->section, $event->other['modulename']);

                    // 사용자별 학습이력현황의 section값 변경해줘야됨.
                    $CStats->setActivityChangeSection($cmid, $sectionInfo->section);
                }
            }
        }
    }

    private static function get_course_module_sections($cmid)
    {
        global $DB;

        // course_modules 테이블에는 주차 id값만 넘어오기 때문에 실제 강좌 주차를 알아올수 있도록 course_sections 테이블이랑 조인해서
        // 필요한 정보 가져와야됨.
        $query = "SELECT
						cm.section AS sectionid
						,cs.section AS section
						,cm.module
						,cm.instance
						,cm.visible
			FROM		{course_modules} cm
			JOIN		{course_sections} cs ON cm.section = cs.id
			WHERE		cm.id = :cmid";

        $param = [
            'cmid' => $cmid
        ];

        return $DB->get_record_sql($query, $param);
    }
}