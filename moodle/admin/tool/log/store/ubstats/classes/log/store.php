<?php
namespace logstore_ubstats\log;

use Exception;
use stdClass;
defined('MOODLE_INTERNAL') || die();

class store implements \tool_log\log\writer, \core\log\sql_internal_table_reader
{
    use \tool_log\helper\store,
        \tool_log\helper\buffered_writer,
        \tool_log\helper\reader;

    /** @var string $logguests true if logging guest access */
    protected $logguests;

    public function __construct(\tool_log\log\manager $manager)
    {
        $this->helper_setup($manager);

        $this->buffersize = $this->get_config('buffersize', 50);
        $this->logguests = $this->get_config('logguests', 0);
    }

    /**
     * Should the event be ignored (== not logged)?
     *
     * @param \core\event\base $event
     * @return bool
     */
    protected function is_event_ignored(\core\event\base $event)
    {
        if ((! CLI_SCRIPT or PHPUNIT_TEST) and ! $this->logguests) {
            // Always log inside CLI scripts because we do not login there.
            if (! isloggedin() or isguestuser()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Finally store the events into the database.
     *
     * @param array $evententries
     *            raw event data
     */
    protected function insert_event_entries($evententries)
    {

        // error_log(print_r($evententries, true));

        // ubion stats 테이블에 데이터 입력해줘야됨.
        // $evententries 는 배열형태로 여러 레코드가 전달될수 있음.
        if (! empty($evententries)) {
            foreach ($evententries as $es) {
                $es = (object) $es;

                // as login 한 경우에는 해당 사용자 계정에 대해서는 로그를 남기면 안됨.
                if (empty($es->realuserid)) {
                    $component = $es->component;
                    $action = $es->action;
                    $target = $es->target;
                    $courseid = $es->courseid;

                    // 로그인인 경우
                    // target : user, action : loggedin 일반 로그인
                    // target : webservice_token, action : send 웹서비스 로그인
                    if (($action == 'loggedin' && $target == 'user') || ($action == 'sent' && $target == 'webservice_token')) {
                        $isAppLogin = ($action == 'sent' && $target == 'webservice_token') ? true : false;
                        $this->insert_event_user_login($isAppLogin);

                        // 강좌 view 화면인 경우 (section페이지 접근시에도 동일하게 action값이 viewed로 남음)
                        // 단 강좌 번호가 1인 경우에는 제외해야됨.
                    } else if ($component == 'core' && $target == 'course' && $action == 'viewed' && $courseid != SITEID) {
                        $this->insert_event_course_view($courseid);

                        // component가 mod로 시작하는 경우에는 학습자원/활동 접속 기록임
                    } else if (strpos($component, 'mod_') === 0) {
                        $this->insert_event_user_activity($es);
                    }
                }

                // 로그인 기록 남기기
                $allowLog = $this->getPrivacyLoginEvent();
                if (in_array($es->eventname, $allowLog)) {
                    $this->setLoginLog($es);
                }

                // 개인정보 취급이력(crud)은 as login 했을 때에도 남겨야됨.
                $allowLog = $this->getPrivacyAllowEvent();
                if (in_array($es->eventname, $allowLog)) {
                    $this->setPrivacyLog($es);
                }
            }
        }
    }

    /**
     * 로그인 기록 남기기
     *
     * @param string $time
     */
    private function insert_event_user_login($isAppLogin = false)
    {
        global $USER, $DB;

        $CCourse = \local_ubion\course\Course::getInstance();
        $CUser = \local_ubion\user\User::getInstance();

        $deviceType = \core_useragent::get_device_type();
        if ($isAppLogin) {
            $deviceType = 'app';
        }

        // 연도, 학기의 기본값 설정
        $year = date('Y');
        $semester = $CCourse::CODE_99;

        // 현재학기가 설정되어 있으면 해당 학기로 로그인 기록이 남아야됨.
        if ($currentSemester = $CCourse->getCurrentSemester()) {
            $year = $currentSemester->year;
            $semester = $currentSemester->semester_code;
        }

        $userid = $USER->id;
        $time = time();

        $param = array();
        $param['dates'] = date('Y-m-d', $time); // 날자
        $param['month'] = date('m', $time); // 월
        $param['day'] = date('d', $time); // 일
        $param['week'] = date('W', $time); // 연도의 주차
        $param['dayofweek'] = date('w', $time); // 요일(0~6)
        $param['userid'] = $userid; // 사용자 아이디
        $param['quarter'] = ceil($param['month'] / 3); // 분기
        $param['year'] = $year; // 학기의년도
        $param['semester'] = $semester; // 학기

        $param['group_code'] = null; // 소속그룹
        $param['etcgroup_code'] = null; // 기타 코드

        // 손님이 아닌 경우.
        $userType = $CUser::TYPE_GUEST;
        if (! isguestuser($userid)) {
            // $lastLoginDate = 0;
            // if (! empty($USER->lastlogin)) {
            // $lastLoginDate = date('Y-m-d', $USER->lastlogin);
            // }

            // unique_login 은 1일 기준 중복 접속 제외한 카운트
            // if (empty($lastLoginDate) || $param['day'] > $lastLoginDate) {
            // $param['unique_login'] = 1;
            // }

            // user_type값 가져오기
            if (is_siteadmin($userid)) {
                $userType = $CUser::TYPE_ADMIN;
                // 관리자는 group_code가 없음
            } else {
                // userubion이 존재하는 경우
                if ($userubion = $CUser->getUbion()) {
                    $userType = $userubion->user_type;
                } else {
                    $userType = null;
                }

                // usertype이 빈값인 경우에는 기타값으로 설정
                if (empty($userType)) {
                    $userType = $CUser::TYPE_ETC;
                }
            }
        }
        $param['user_type'] = $userType;

        $query = "INSERT INTO {logstore_ubstats_user_day}
				(
					dates
					,month
					,day
					,week
					,dayofweek
					,quarter
					,userid
					,user_type
					,group_code
					,etcgroup_code
					,total_login
					,first_device
					,pc
					,mobile
					,tablet
					,app
					,etc_device
					,year
					,semester
				) VALUES (
					:dates
					,:month
					,:day
					,:week
					,:dayofweek
					,:quarter
					,:userid
					,:user_type
					,:group_code
					,:etcgroup_code
					,1
					,:first_device
					,:pc
					,:mobile
					,:tablet
					,:app
					,:etc_device
					,:year
					,:semester
				)
				ON DUPLICATE KEY UPDATE
					total_login = total_login + 1";

        // 디바이스에 따른 업데이트
        $param['pc'] = 0;
        $param['mobile'] = 0;
        $param['tablet'] = 0;
        $param['app'] = 0;
        $param['etc_device'] = 0;

        // ////2.시간대 통계 저장 - inser로 누적 시킴
        $ptime = array();
        $ptime['dates'] = date('Y-m-d', $time); // 날짜
        $ptime['time'] = date('H:i:s', $time); // 시간
        $ptime['userid'] = $userid; // 사용자 아이디
        $ptime['user_type'] = $userType;
        $ptime['device'] = $deviceType; // 디바이스타입
        $ptime['ip'] = sprintf('%u', ip2long(getremoteaddr())); // 사용자 아이피
        $ptime['year'] = $year; // 학기의년도
        $ptime['semester'] = $semester; // 학기

        $query_time = "INSERT INTO {logstore_ubstats_user_time}
				(
					dates
					,time
					,userid
					,user_type
					,device
					,ip
					,year
					,semester
				) VALUES (
					:dates
					,:time
					,:userid
					,:user_type
					,:device
					,:ip
					,:year
					,:semester
				)";

        // ///3.집계 통계 저장
        $pcount = array();
        $pcount['dates'] = date('Y-m-d', $time); // 날짜
        $pcount['hour'] = date('H', $time); // 시간
        $pcount['random'] = mt_rand(1, 10); // 랜덤값
        $pcount['total_login'] = 1; // 첫 1회 방문
        $pcount['pc'] = 0;
        $pcount['mobile'] = 0;
        $pcount['tablet'] = 0;
        $pcount['app'] = 0;
        $pcount['etc_device'] = 0;
        $pcount['year'] = $year; // 학기의년도
        $pcount['semester'] = $semester; // 학기

        $query_cnt = "INSERT INTO {logstore_ubstats_login}
				(
					dates
					,hour
					,`random`
					,total_login
					,pc
					,mobile
					,tablet
					,app
					,etc_device
					,year
					,semester
				) VALUES (
					:dates
					,:hour
					,:random
					,:total_login
					,:pc
					,:mobile
					,:tablet
					,:app
					,:etc_device
					,:year
					,:semester
				)
				ON DUPLICATE KEY UPDATE
					total_login = total_login + 1";

        // 공통 쿼리 및 파라메터 설정
        // 디바이스에 따른 업데이트
        $device_query = '';
        $device_param = array();
        $device_name = '';
        switch ($deviceType) {
            case 'mobile':
                $device_query .= ', mobile = mobile + 1';
                $device_param['mobile'] = 1;
                $device_name = 'mobile';
                break;
            case 'tablet':
                $device_query .= ', tablet = tablet + 1';
                $device_param['tablet'] = 1;
                $device_name = 'tablet';
                break;
            case 'app':
                $device_query .= ', app = app + 1';
                $device_param['app'] = 1;
                $device_name = 'app';
                break;
            case 'etc_device':
                $device_query .= ', etc_device = etc_device + 1';
                $device_param['etc_device'] = 1;
                $device_name = 'etc_device';
                break;
            default:
                $device_query .= ', pc = pc + 1';
                $device_param['pc'] = 1;
                $device_name = 'pc';
                break;
        }
        $device_param['first_device'] = $device_name; // 최초 1회 접속시 디바이스명

        try {
            // transaction
            $transaction = $DB->start_delegated_transaction();

            // 1.날짜통계-ubstats_countdate
            $countdate_param = array_merge($param, $device_param);
            // error_log($query.$device_query);
            // error_log(print_r($countdate_param, true));
            $DB->execute($query . $device_query, $countdate_param);

            // 2.시간대통계-ubstats_counttime
            // error_log($query_time);
            // error_log(print_r($ptime, true));
            $DB->execute($query_time, $ptime);

            // 3.집계통계-ubstats_count
            $count_param = array_merge($pcount, $device_param);
            // error_log($query_cnt.$device_query);
            // error_log(print_r($count_param, true));
            $DB->execute($query_cnt . $device_query, $count_param);

            $transaction->allow_commit();
        } catch (Exception $e) {
            $transaction->rollback($e);
        }
    }

    /**
     * 강좌 접근 기록 남기긱
     *
     * @param int $courseid
     */
    private function insert_event_course_view($courseid)
    {
        global $USER, $DB;

        $userid = $USER->id;
        $CCourse = \local_ubion\course\Course::getInstance();
        $CUser = \local_ubion\user\User::getInstance();

        if ($course = $CCourse->getCourse($courseid)) {
            // 2015-12-09 연도/학기 정보를 강좌 idnumber에서 가져오지 말고 course_ubion에서 가져옴.
            // course_ubion에 데이터가 없다면 통계를 반영하지 않음.
            // 정규 강좌라면 반드시 course_ubion이 존재함.
            // 그리고 코스모스에서 사용하는 강좌 포맷인 경우에는 반드시 강좌 접근시에 course_ubion을 insert하게 되어 있음.
            // course_ubion에서 가져오는 이유는 idnumber를 잘못 입력한 강좌인 경우에는 강좌 접근시 오류가 발생되어서 문제가 발생됨.
            if ($courseUbion = $CCourse->getUbion($course->id)) {
                // 연도, 학기 정보
                $year = $courseUbion->year;
                $semester_code = $courseUbion->semester_code;

                $time = time();

                $param = array();
                $param['day'] = date('Y-m-d', $time);
                $param['month'] = date('m', $time); // 월
                $param['week'] = date('W', $time); // 연도의 주차
                $param['dayofweek'] = date('w', $time); // 요일(0~6)
                $param['timezone'] = date('H', $time); // 시간
                $param['quarter'] = ceil($param['month'] / 3); // 분기
                $param['section'] = $param['week']; // 연도의 주차와 동일하게 입력

                // 강좌 연도/학기
                $param['year'] = $year;
                $param['semester'] = $semester_code;

                // 1일 기준 중복 로그인 제외한 카운트
                $param['unique_login'] = 0;
                // 강좌 전체 접속 횟수
                $param['total_login'] = 1;

                $param['courseid'] = $courseid;
                $param['userid'] = $userid;

                if (isloggedin()) {
                    // $USER->currentcourseaccess[$courseid] 값이 빈값이라면 1일 기준 카운트수는 +1
                    if (empty($USER->currentcourseaccess[$courseid])) {
                        $param['unique_login'] = 1;
                    } else {
                        // 1일 1번만 기록이 되어야 함.
                        $current_connect_day = date('Y-m-d', $USER->currentcourseaccess[$courseid]);

                        if ($param['day'] > $current_connect_day) {
                            $param['unique_login'] = 1;
                        }
                    }

                    // 손님이 아닌 경우.
                    $userType = $CUser::TYPE_GUEST;
                    if (! isguestuser($userid)) {
                        // user_type값 가져오기
                        if (is_siteadmin($userid)) {
                            $userType = $CUser::TYPE_ADMIN;
                        } else {
                            // userubion이 존재하는 경우
                            if ($userubion = $CUser->getUbion()) {
                                $userType = $userubion->user_type;
                            } else {
                                $userType = null;
                            }

                            // usertype이 빈값인 경우에는 기타값으로 설정
                            if (empty($userType)) {
                                $userType = $CUser::TYPE_ETC;
                            }
                        }
                    }
                    $param['user_type'] = $userType;

                    $query = "INSERT INTO {logstore_ubstats_course} (
							userid
							,courseid
							,year
							,user_type
							,quarter
							,week
							,dayofweek
							,semester
							,unique_login
							,total_login
						) VALUES (
							 :userid
							,:courseid
							,:year
							,:user_type
							,:quarter
							,:week
							,:dayofweek
							,:semester
							,1
							,1
						) ON DUPLICATE KEY UPDATE
							 unique_login= unique_login + :unique_login
							,total_login= total_login + :total_login";

                    $DB->execute($query, $param);
                }
            }
        }
    }

    private function insert_event_user_activity($es)
    {
        global $DB, $USER;

        $userid = $USER->id;
        $courseid = $es->courseid;
        $action = $es->action;
        $target = $es->target;
        $cmid = $es->contextinstanceid;
        $crud = $es->crud;
        $CCourse = \local_ubion\course\Course::getInstance();

        if ($course = $CCourse->getCourse($courseid)) {
            // 2015-12-09 연도/학기 정보를 강좌 idnumber에서 가져오지 말고 course_ubion에서 가져옴.
            // course_ubion에 데이터가 없다면 통계를 반영하지 않음.
            // 정규 강좌라면 반드시 course_ubion이 존재함.
            // 그리고 코스모스에서 사용하는 강좌 포맷인 경우에는 반드시 강좌 접근시에 course_ubion을 insert하게 되어 있음.
            // course_ubion에서 가져오는 이유는 idnumber를 잘못 입력한 강좌인 경우에는 강좌 접근시 오류가 발생되어서 문제가 발생됨.
            if ($courseUbion = $CCourse->getUbion($course->id)) {
                $year = $courseUbion->year;
                $semester_code = $courseUbion->semester_code;

                $course_section = null;
                $moduleid = null;

                // section 정보를 가져와야됨.
                $query = "SELECT
									cm.id
									,cm.module
									,cs.section
							FROM 	{course_modules} cm
							JOIN	{course_sections} cs ON cs.id = cm.section
							WHERE 	cm.course = :courseid
							AND  	cm.id = :cmid";

                $param = [
                    'courseid' => $courseid,
                    'cmid' => $cmid
                ];

                if ($course_modules = $DB->get_record_sql($query, $param)) {
                    $course_section = $course_modules->section;
                    $moduleid = $course_modules->module;
                }

                // course_section값은 0값이 전달이 될수 있음. (강좌개요인 경우 0값임)
                // moduleid값은 null, 0값이 나오면 안됨.
                if (! is_null($course_section) && ! empty($moduleid)) {

                    if ($courseid == SITEID) {
                        $year = date('Y');
                        $semester = $CCourse::CODE_99;
                    } else {
                        $year = $year;
                        $semester = $semester_code;
                    }

                    // 다중 DBMS를 위해서 INSERT ON DUPLICATE KEY UPDATE 쿼리는 사용하지 않습니다.
                    $query = "SELECT 
    									id, action_count
    						FROM 		{logstore_ubstats_activity} 
    						WHERE 		year = :year
    						AND			userid = :userid
    						AND			semester = :semester
    						AND			moduleid = :moduleid
    						AND			action = :action
    						AND			target = :target
    						AND			courseid = :courseid
    						AND			section = :section
    						AND			cmid = :cmid";

                    $param = array(
                        'year' => $year,
                        'semester' => $semester,
                        'courseid' => $courseid,
                        'section' => $course_section,
                        'moduleid' => $moduleid,
                        'cmid' => $cmid,
                        'action' => $action,
                        'target' => $target,
                        'userid' => $userid
                    );

                    // 기존에 등록된 데이터가 존재하는 경우
                    if ($user_activity = $DB->get_record_sql($query, $param)) {
                        $param['id'] = $user_activity->id;
                        $param['action_count'] = $user_activity->action_count + 1;

                        $DB->update_record_raw('logstore_ubstats_activity', $param);
                    } else {
                        $param['action_count'] = 1;
                        $param['crud'] = $crud;

                        $DB->insert_record_raw('logstore_ubstats_activity', $param);
                    }
                }
            }
        }
    }

    public function get_events_select($selectwhere, array $params, $sort, $limitfrom, $limitnum)
    {
        global $DB;

        $sort = self::tweak_sort_by_id($sort);

        $events = array();
        $records = $DB->get_recordset_select('logstore_standard_log', $selectwhere, $params, $sort, '*', $limitfrom, $limitnum);

        foreach ($records as $data) {
            if ($event = $this->get_log_event($data)) {
                $events[$data->id] = $event;
            }
        }

        $records->close();

        return $events;
    }

    /**
     * Fetch records using given criteria returning a Traversable object.
     *
     * Note that the traversable object contains a moodle_recordset, so
     * remember that is important that you call close() once you finish
     * using it.
     *
     * @param string $selectwhere
     * @param array $params
     * @param string $sort
     * @param int $limitfrom
     * @param int $limitnum
     * @return \core\dml\recordset_walk|\core\event\base[]
     */
    public function get_events_select_iterator($selectwhere, array $params, $sort, $limitfrom, $limitnum)
    {
        global $DB;

        $sort = self::tweak_sort_by_id($sort);

        $recordset = $DB->get_recordset_select('logstore_standard_log', $selectwhere, $params, $sort, '*', $limitfrom, $limitnum);

        return new \core\dml\recordset_walk($recordset, array(
            $this,
            'get_log_event'
        ));
    }

    /**
     * Returns an event from the log data.
     *
     * @param stdClass $data
     *            Log data
     * @return \core\event\base
     */
    public function get_log_event($data)
    {
        $extra = array(
            'origin' => $data->origin,
            'ip' => $data->ip,
            'realuserid' => $data->realuserid
        );
        $data = (array) $data;
        $data['other'] = unserialize($data['other']);
        if ($data['other'] === false) {
            $data['other'] = array();
        }
        unset($data['origin']);
        unset($data['ip']);
        unset($data['realuserid']);
        unset($data['id']);

        if (! $event = \core\event\base::restore($data, $extra)) {
            return null;
        }

        return $event;
    }

    public function get_events_select_count($selectwhere, array $params)
    {
        global $DB;
        return $DB->count_records_select('logstore_standard_log', $selectwhere, $params);
    }

    public function get_internal_log_table_name()
    {
        return 'logstore_standard_log';
    }

    /**
     * Are the new events appearing in the reader?
     *
     * @return bool true means new log events are being added, false means no new data will be added
     */
    public function is_logging()
    {
        // Only enabled stpres are queried,
        // this means we can return true here unless store has some extra switch.
        return false;
    }

    /**
     * 개인정보 취급이력으로 인정될 이벤트 이름
     */
    protected function getPrivacyLoginEvent()
    {
        return array(
            "\\core\\event\\user_loggedin",
            "\\core\\event\\user_loggedinas",
            "\\core\\event\\webservice_token_sent"
        );
    }

    /**
     * 로그인 기록
     *
     * @param object $es
     * @param boolean $isAsLogin
     * @return number
     */
    protected function setLoginLog($es)
    {
        global $DB;

        $login = new \stdClass();
        $login->userid = $es->userid;
        $login->other = $es->other;
        $login->ip = $es->ip;
        $login->timecreated = $es->timecreated;

        // 앱 로그인이 아니면 pc, mobile, tablet 구분해서 넣어줘야됨.
        if ($es->eventname == '\\core\\event\\webservice_token_sent') {
            $device = 'app';
        } else {
            $device = \core_useragent::get_device_type();
        }
        $login->device = $device;

        // as login 여부
        if ($es->eventname == '\\core\\event\\user_loggedinas') {
            $login->aslogin = 1;
        }

        $login->id = $DB->insert_record('logstore_ubstats_login_log', $login);

        return $login->id;
    }

    /**
     * 개인정보 취급이력으로 인정될 이벤트 이름
     */
    protected function getPrivacyAllowEvent()
    {
        return array(
            "\\core\\event\\user_profile_viewed",
            "\\core\\event\\user_updated",
            "\\core\\event\\user_created",
            "\\core\\event\\user_deleted"
        );
    }

    /**
     * 개인정보 취급 이력 기록
     *
     * @param object $es
     * @return number
     */
    protected function setPrivacyLog($es)
    {
        global $DB;

        // 작업자 => $es->userid
        // 대상자 => $es->relateduserid
        $privacy = new \stdClass();
        $privacy->userid = $es->userid;
        $privacy->eventname = $es->eventname;
        $privacy->crud = $es->crud;
        $privacy->other = $es->other;
        $privacy->targetid = $es->relateduserid;
        $privacy->ip = $es->ip;
        $privacy->timecreated = $es->timecreated;
        $privacy->realuserid = $es->realuserid;

        $privacy->id = $DB->insert_record('logstore_ubstats_privacy_log', $privacy);

        return $privacy->id;
    }
}
