<?php
namespace logstore_ubstats\core;

use stdClass;
defined('MOODLE_INTERNAL') || die();

class Stats
{

    /**
     * 강좌 복구 및 학습자원/복구시 logstore_ubstats_module에 등록된 학습자원/활동 갯수 맞춰줌
     *
     * @param int $lastrun
     */
    function cronModuleCount($lastrun = null)
    {
        global $DB, $CFG;
        require_once ($CFG->dirroot . '/backup/util/interfaces/checksumable.class.php');
        require_once $CFG->dirroot . '/backup/backup.class.php';

        mtrace("activity start Time: " . date('r', time()) . "\n\n");

        // select * from mdl_backup_controllers where operation = 'restore' and status = 1000 and timemodified > 0
        // type값은 조건에서 빼야됨 (강좌만 복구 기능이 존재하는게 아니고 학습활동/자원도 복구를 할수 있음)

        // 가장 마지막에 연동된시간이 빈값이면 현재시간으로 측정
        if (empty($lastrun)) {
            $time = time();
        } else {
            $time = $lastrun;
        }

        // backup_controller 테이블 index가 잡힌게 없어서 별도로 강좌 정보 구해와야될듯 함.
        $query = "SELECT
							itemid
				FROM 		{backup_controllers}
				WHERE 		operation = :operation
				AND 		status = :status
				AND			timemodified >= :timemodified
				GROUP BY 	itemid";

        $param = [
            'operation' => 'restore',
            'status' => \backup::STATUS_FINISHED_OK,
            'timemodified' => $time
        ];

        // backup_controllers
        if ($bcs = $DB->get_records_sql($query, $param)) {

            $modules = $DB->get_records_sql('SELECT id, name, 0 AS cnt FROM {modules}');

            $select_query = "SELECT COUNT(1) FROM {logstore_ubstats_module} WHERE courseid = :courseid AND section = :section AND module = :module";

            $transaction = $DB->start_delegated_transaction();
            foreach ($bcs as $bcs_course) {
                $courseid = $bcs_course->itemid;

                // 강좌 정보 가져오기
                if ($course = $DB->get_record_sql('SELECT id, startdate, idnumber FROM {course} WHERE id = :id', array(
                    'id' => $courseid
                ))) {

                    $CCourse = \local_ubion\course\Course::getInstance();

                    $year = date('Y');
                    $semester = $CCourse::CODE_99;

                    if ($courseUbion = $CCourse->getUbion($course->id)) {
                        $year = $courseUbion->year;
                        $semester = $courseUbion->semester_code;

                        // course_modules가서 등록된 모든 학습 활동 가져오기
                        $query = "SELECT
									cm.id
									,cm.module
									,cs.section AS course_section
							FROM 	{course_modules} cm
							JOIN	{course_sections} cs ON cs.id = cm.section
							WHERE 	cm.course = :course";

                        $param = array(
                            'course' => $courseid
                        );

                        if ($course_modules = $DB->get_records_sql($query, $param)) {
                            // 사이트에 등록된 모든 학습 활동 모듈값 복사해서 등록된 학습활동 갯수 파악
                            $addModules = array();

                            foreach ($course_modules as $cm) {
                                if (array_key_exists($cm->module, $addModules)) {
                                    if (array_key_exists($cm->course_section, $addModules[$cm->module]->count)) {
                                        $addModules[$cm->module]->count[$cm->course_section] ++;
                                    } else {
                                        $addModules[$cm->module]->count[$cm->course_section] = 1;
                                    }
                                } else {
                                    $am = new stdClass();
                                    $am->name = $modules[$cm->module]->name;
                                    $am->count = array(
                                        $cm->course_section => 1
                                    );

                                    $addModules[$cm->module] = $am;
                                }
                            }

                            if ($addModules) {
                                foreach ($addModules as $am) {
                                    if ($am->count) {
                                        foreach ($am->count as $section => $count) {
                                            $select_param = array(
                                                'courseid' => $courseid,
                                                'section' => $section,
                                                'module' => $am->name
                                            );
                                            if ($DB->get_field_sql($select_query, $select_param)) {
                                                $this->setModuleCountUpdate($year, $semester, $courseid, $section, $am->name, $count, true);
                                            } else {
                                                $this->setModuleCountAdd($year, $semester, $courseid, $section, $am->name, $count);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $transaction->allow_commit();
        }

        mtrace("activity end Time: " . date('r', time()) . "\n\n");
    }

    /**
     * 학습자원/활동 추가시 logstore_ubstats_module에 모듈별 등록현황을 업데이트 해줍니다.
     *
     * @param int $year
     * @param string $semester
     * @param int $courseid
     * @param int $courseSection
     *            : 강좌 주차(sectionid가 아닌 강좌 주차 번호임)
     * @param string $moduleName
     * @param number $count
     */
    function setModuleCountExecute($year, $semester, $courseid, $courseSection, $moduleName, $count = 1)
    {
        global $DB;

        $query = "INSERT INTO {logstore_ubstats_module} (
				year, courseid, semester, section, module, sequence_count
			) VALUES (
				:year, :courseid, :semester, :section, :module, :sequence_count
			) ON DUPLICATE KEY UPDATE
				sequence_count= sequence_count + :sequence_count2";

        // add mod에 의해서 등록되기 때문에 sequence_counts값은 무조건 1임.
        // 무들 기능 중 동시에 모듈을 여러개 생성하는 기능은 없음.
        // 만약 존재한다고 해도, 모듈 추가되는 로직을 반복해서 돌리기 때문에 이 함수는 반드시 모듈 1건에 대해서 1번씩은 반드시 실행됨.
        $param = array(
            'year' => $year,
            'semester' => $semester,
            'courseid' => $courseid,
            'section' => $courseSection,
            'module' => $moduleName,
            'sequence_count' => $count,
            'sequence_count2' => $count
        );

        // error_log($query);
        // error_log(print_r($param, true));
        $DB->execute($query, $param);
    }

    /**
     * 연도/학기/강좌별 등록된 학습자원/활동 갯수 증가
     *
     * @param number $year
     * @param string $semester
     * @param number $courseid
     * @param number $courseSection
     * @param string $moduleName
     * @param number $count
     */
    function setModuleCountAdd($year, $semester, $courseid, $courseSection, $moduleName, $count = 1)
    {
        global $DB;

        $query = "INSERT INTO {logstore_ubstats_module} (
				year, courseid, semester, section, module, sequence_count
			) VALUES (
				:year, :courseid, :semester, :section, :module, :sequence_count
			) ON DUPLICATE KEY UPDATE
				sequence_count= sequence_count + :sequence_count2";

        // add mod에 의해서 등록되기 때문에 sequence_counts값은 무조건 1임.
        // 무들 기능 중 동시에 모듈을 여러개 생성하는 기능은 없음.
        // 만약 존재한다고 해도, 모듈 추가되는 로직을 반복해서 돌리기 때문에 이 함수는 반드시 모듈 1건에 대해서 1번씩은 반드시 실행됨.
        $param = array(
            'year' => $year,
            'semester' => $semester,
            'courseid' => $courseid,
            'section' => $courseSection,
            'module' => $moduleName,
            'sequence_count' => $count,
            'sequence_count2' => $count
        );

        $DB->execute($query, $param);
    }

    /**
     * 연도/학기/강좌별 등록된 학습자원/활동 갯수 변경
     *
     * @param number $year
     * @param string $semester
     * @param number $courseid
     * @param number $courseSection
     * @param string $moduleName
     * @param number $count
     * @param string $isChange
     */
    function setModuleCountUpdate($year, $semester, $courseid, $courseSection, $moduleName, $count = 1, $isChange = false)
    {
        global $DB;

        $query = "UPDATE {logstore_ubstats_module} SET";

        // 전달된 값으로 변경시
        if ($isChange) {
            $query .= " sequence_count = :sequence_count ";
        } else { // 기존 값에서 전달된 값만큼 변경시.
            $query .= " sequence_count = IF(sequence_count > 0 , sequence_count - :sequence_count, 0)";
        }

        $query .= " 	WHERE	year = :year
				AND		semester = :semester
		 		AND		courseid = :courseid
				AND		section = :section
				AND		module = :module";

        // add mod에 의해서 등록되기 때문에 sequence_counts값은 무조건 1임.
        // 무들 기능 중 동시에 모듈을 여러개 생성하는 기능은 없음.
        // 만약 존재한다고 해도, 모듈 추가되는 로직을 반복해서 돌리기 때문에 이 함수는 반드시 모듈 1건에 대해서 1번씩은 반드시 실행됨.
        $param = array(
            'year' => $year,
            'semester' => $semester,
            'courseid' => $courseid,
            'section' => $courseSection,
            'module' => $moduleName,
            'sequence_count' => $count
        );

        // error_log($query);
        // error_log(print_r($param, true));

        $DB->execute($query, $param);
    }

    /**
     * 모듈 이동에 의해 주차가 변경된 경우 강좌별 사용자 활동의 section값을 변경시켜줌
     *
     * @param int $cmid
     * @param int $section
     */
    public function setActivityChangeSection($cmid, $section)
    {
        global $DB;

        // logstore_ubstats_activity 에도 강좌 주차를 변경해줘야됨.
        $query = "UPDATE
						  {logstore_ubstats_activity}
				  SET
						  section  = :course_section
				  WHERE   cmid 	   = :cmid";

        $param = [
            'course_section' => $section,
            'cmid' => $cmid
        ];

        // error_log($query);
        // error_log(print_r($param, true));

        $DB->execute($query, $param);
    }

    public function setManageLog($value)
    {
        global $USER, $DB;

        $log = new \stdClass();
        $log->userid = $USER->id;
        $log->value = $value;
        $log->timecreated = time();
        $log->ip = \local_ubion\base\Common::getRealIPAddr();
        $log->id = $DB->insert_record('logstore_ubstats_manage_log', $log);
    }
}