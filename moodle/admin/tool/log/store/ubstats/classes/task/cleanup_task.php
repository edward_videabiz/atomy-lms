<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle. If not, see <http://www.gnu.org/licenses/>.

/**
 * Standard log reader/writer.
 *
 * @package logstore_standard
 * @copyright 2014 Petr Skoda {@link http://skodak.org}
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace logstore_ubstats\task;

defined('MOODLE_INTERNAL') || die();

class cleanup_task extends \core\task\scheduled_task
{

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name()
    {
        return get_string('taskcleanup', 'logstore_ubstats');
    }

    /**
     * Do the job.
     * Throw exceptions on errors (the job will be retried).
     */
    public function execute()
    {
        global $DB;

        $loglifetime = (int) get_config('logstore_ubstats', 'loglifetime');

        if ($loglifetime > 0) {
            $loglifetime = time() - ($loglifetime * 3600 * 24); // Value in days.

            $param = [
                $loglifetime
            ];

            // loglifetime 이전 로그에 대해서는 삭제되어야 함.
            $DB->delete_records_select("logstore_ubstats_login_log", "timecreated < ?", $param);
            $DB->delete_records_select("logstore_ubstats_privacy_log", "timecreated < ?", $param);
            $DB->delete_records_select("logstore_ubstats_manage_log", "timecreated < ?", $param);
        }
    }
}
