<?php
namespace logstore_ubstats\task;

defined('MOODLE_INTERNAL') || die();

class activitycount_task extends \core\task\scheduled_task
{

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name()
    {
        return get_string('taskactivitycount', 'logstore_ubstats');
    }

    /**
     * Do the job.
     * Throw exceptions on errors (the job will be retried).
     */
    public function execute()
    {
        $lastrun = $this->get_last_run_time();

        // 통계 테이블에 복구된 학습자원/활동 반영시켜줘야됨.
        $ubstats = new \logstore_ubstats\Stats();
        $ubstats->cronModuleCount($lastrun);
    }
}
