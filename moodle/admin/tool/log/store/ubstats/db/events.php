<?php
$observers = array(

    // 학습 자원/활동 관련(추가, 수정, 삭제, 숨김, 이동 등)
    array(
        'eventname' => '\core\event\course_module_created',
        'callback' => '\logstore_ubstats\Observer::course_module_created'
    ),
    array(
        'eventname' => '\core\event\course_module_updated',
        'callback' => '\logstore_ubstats\Observer::course_module_updated'
    ),
    array(
        'eventname' => '\core\event\course_module_deleted',
        'callback' => '\logstore_ubstats\Observer::course_module_deleted'
    ),
    array(
        'eventname' => '\local_ubion\event\course_module_visible',
        'callback' => '\logstore_ubstats\Observer::course_module_visible'
    ),
    array(
        'eventname' => '\local_ubion\event\course_module_move',
        'callback' => '\logstore_ubstats\Observer::course_module_move'
    )
);
