<?php

function xmldb_logstore_ubstats_upgrade($oldversion)
{
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2018082702) {

        // 해당 테이블은 더이상 사용되지 않습니다.
        $table = new xmldb_table('logstore_ubstats_cms');

        // Conditionally launch drop table for logstore_ubstats_cms.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Standard savepoint reached.
        upgrade_plugin_savepoint(true, 2018082702, 'logstore', 'ubstats');
    }

    return true;
}
