<?php
require ('config.php');

$utoken = required_param('utoken', PARAM_RAW);
$url = required_param('modurl', PARAM_URL);
// $userid = required_param('userid', PARAM_USERNAME);
// $servicename= required_param('', PARAM_ALPHANUMEXT);

// if($tokenuser = $DB->get_record_sql('SELECT * FROM {external_tokens} WHERE token = :token', array('token' => $utoken))) {
$userdata = get_complete_user_data('id', 2);

if ($USER = complete_user_login($userdata)) {

    // 무조건 모바일 테마를 타도록 변경
    if (strpos($url, '?') !== false) {
        $url = $url . '&theme=coursemos_mobile';
    } else {
        $url = $url . '?theme=coursemos_mobile';
    }

    // 단순 gateway 페이지이기 때문에 모바일 테마의 header, footer는 표시되지 않도록 처리 해야됨.
    $USER->is_webview = true;

    header("Location: $url");
} else {
    throw new moodle_exception('notifyloginfailuresmessage', 'moodle');
}
//} else {
    //throw new moodle_exception('invalidtoken', 'webservice');
//}