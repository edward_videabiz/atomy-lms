<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2018080700;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2017051500;        // Requires this Moodle version
$plugin->component = 'local_ubsend';
$plugin->dependencies = array('local_ubion' => 2017010100);