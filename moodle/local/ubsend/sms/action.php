<?php
require_once '../../../config.php';

$id = required_param('id', PARAM_INT);

$controllder = new \local_ubsend\sms\SMS($id);

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}