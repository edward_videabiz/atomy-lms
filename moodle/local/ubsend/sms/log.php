<?php
use local_ubion\base\Common;
use local_ubion\base\Paging;

require_once '../../../config.php';

$id           = required_param('id', PARAM_INTEGER);
$page         = optional_param('page', 0, PARAM_INT);
$ls           = optional_param('ls', 15, PARAM_INT);        // how many per page

$CUser = \local_ubion\user\User::getInstance();
$CCourse = \local_ubion\course\Course::getInstance();
$course = $CCourse->getCourse($id);

$context = \context_course::instance($course->id);

// 언어 설정
$pluginname = 'local_ubsend';
$i8n = get_strings(array(    
    'sms_send'
    ,'sms_log'
    ,'resend'
), $pluginname);

## 페이지 셋팅
$context= context_course::instance($id);
$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);

// 주소 셋팅
$urlParam = array('id' => $id);
if (!empty($rid)) {
    $urlParam['rid'] = $rid;
}

$basePath = $CFG->wwwroot.'/local/ubsend/sms';
$PAGE->set_url($basePath.'/log.php', $urlParam);
$PAGE->set_pagelayout('incourse');
$PAGE->set_heading($course->fullname);
$PAGE->set_title($course->fullname.' : '.$i8n->sms_log);
$PAGE->navbar->add($i8n->sms_log, $PAGE->url->out());


$isProfessor = $CCourse->isProfessor($course->id, $context);
$CSMS = new \local_ubsend\sms\SMS($id);

// 메일 전송 권한이 없는 경우 에러 페이지 출력
if (!$CSMS->isSendPermission()) {
    Common::printNotice(get_string('no_permissions', 'local_ubion'), $CFG->wwwroot.'/course/view.php?id='.$id);
}

echo $OUTPUT->header();
?>
<div class="local_ubsend local_ubsend_sms">
	<?= $CSMS->getTab('log'); ?>
	
	<div class="well">
		<table class="table table-bordered table-coursemos table-responsive-sm">
			<colgroup>
				<col class="wp-180 max-wp-270" />
				<col class="wp-60" />
				<col />
				<col class="wp-200 hidden-sm-down" />
				<col class="wp-80" />
			</colgroup>
			<thead>
				<tr>
					<th><?= get_string('date'); ?></th>
					<th class="min-wp-60"><?= get_string('sendcount', $pluginname); ?></th>
					<th class="min-wp-150"><?= get_string('to', $pluginname); ?></th>
					<th class="hidden-sm-down"><?= get_string('sms_content', $pluginname); ?></th>
					<th><?= get_string('etc', 'local_ubion'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$usernamefield = get_all_user_name_fields(true);
				list ($totalCount, $lists) = $CSMS->getLogs($page, $ls);
				
				if ($totalCount > 0) {
				    foreach ($lists as $log) {
				        // 전송일
				        $timecreated = $CSMS->getDate($log->timecreated);
				        
				        // 받는 사람
				        if (!empty($log->smsto)) {
				            $users = '';
				            $smsto = explode(',', $log->smsto);
				            
				            $userCount = count($smsto);
				            
				            if ($userCount > 0) {
				                foreach ($smsto as $uid) {
				                    $u = $DB->get_record_sql('SELECT id, idnumber, '.$usernamefield.' FROM {user} WHERE id = :id', array('id'=>$uid));
				                    
				                    $fullname = fullname($u);
				                    $tooltip = $fullname.' ('.$CUser->getIdnumber($u->idnumber, $isProfessor).')';
				                    $users .= '<span class="label label-default mt-1 mx-1 max-wp-100 ellipsis" data-toggle="tooltip" title="'.$tooltip.'">'.$fullname.'</span>';
				                }
				            }
				            
				        } else {
				            $users = '&nbsp;';
				        }
				        				        
				        // 재전송주소
				        $resendUrl = $CFG->wwwroot.'/local/ubsend/sms/send.php?id='.$id.'&amp;rid='.$log->id;
				        
				        ?>
				        <tr>
				        	<td class="text-center"><?= $timecreated ?></td>
				        	<td class="text-center"><?= $log->smsto_cnt; ?></td>
				        	<td><?= $users ?></td>
				        	<td class="hidden-sm-down"><?= nl2br($log->content); ?></td>
				        	<td class="text-center">
				        		<a href="<?= $resendUrl; ?>" class="btn btn-xs btn-default"><?= $i8n->resend; ?></a>
				        	</td>
				        </tr>
				        <?php 
				    }
				} else {
				    echo '<tr><td colspan="5">'.get_string('sms_not_send', $pluginname).'</td></tr>';
				}
				?>
			</tbody>
		</table>
		
		<?= Paging::printHTML($totalCount, $page, $ls); ?>
	</div>
</div>
<?php 
echo $OUTPUT->footer();