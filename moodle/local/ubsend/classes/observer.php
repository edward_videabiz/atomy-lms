<?php
namespace local_ubsend;

class observer {
    
    public static function message_sent(\core\event\message_sent $event) {
        global $DB;
        
        /*
         core\event\message_sent Object
         (
         [data:protected] => Array
         (
         [eventname] => \core\event\message_sent
         [component] => core
         [action] => sent
         [target] => message
         [objecttable] =>
         [objectid] =>
         [crud] => c
         [edulevel] => 0
         [contextid] => 1
         [contextlevel] => 10
         [contextinstanceid] => 0
         [userid] => 1128053
         [courseid] => 0
         [relateduserid] => 2
         [anonymous] => 0
         [other] => Array
         (
         [messageid] => 731
         )
         
         [timecreated] => 1473329993
         )
         
         [logextra:protected] =>
         [context:protected] => context_system Object
         (
         [_id:protected] => 1
         [_contextlevel:protected] => 10
         [_instanceid:protected] => 0
         [_path:protected] => /1
         [_depth:protected] => 1
         )
         
         [triggered:core\event\base:private] => 1
         [dispatched:core\event\base:private] =>
         [restored:core\event\base:private] =>
         [recordsnapshots:core\event\base:private] => Array
         (
         )
         
         )
         */
        
        /*
        // 2016-09-08 메시지 푸시를 message output 기능을 사용하지 않기 때문에 local_ubmessage에서 보내는 메시지를 다시 message_send할 필요가 없어짐.
        // 즉, message_send로 보내지는 메시지는 무조건 local_ubmessage 로 인서트 해주면 됨.
        // other[messageid]로 실제 메시지 내용 가져오기
        if (isset($event->other['messageid']) && !empty($event->other['messageid'])) {
            if ($message = $DB->get_record_sql("SELECT * FROM {message} WHERE id = :id", array('id'=>$event->other['messageid']))) {
                
                // 턴인잇 모듈에서 알림 메시지(답변이 불필요한 메시지)인 경우 useridfrom값을 -10 또는 -20으로 전달함
                // \core_user::get_noreply_user();
                // 해당 메시지는 불필요하므로 local_ubmessage에는 기록되지 않도록 처리함.
                // 2016-10-27 by akddd
                if ($message->useridfrom > 0) {
                    $class_ubmessage = new local_ubmessage_ubmessage();
                    $class_ubmessage->sendMessage($message->useridfrom, $message->useridto, $message->fullmessage, $message->timecreated, $message->contexturl);
                }
                
                // mdl_message 에 등록된 메시지 삭제 (불필요하게 데이터를 쌓고 있을 필요가 없음)
                $DB->delete_records('message', array('id'=>$event->other['messageid']));
            }
        }
        */      
    }
}