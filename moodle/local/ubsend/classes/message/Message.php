<?php
namespace local_ubsend\message;


class Message extends \local_ubsend\core\message\Message {
    private static $instance;
    
    /**
     * 메시지 Class
     * @return Message
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
}