<?php
namespace local_ubsend\sms;

use \local_ubion\base\Javascript;
use \local_ubion\base\Parameter;
use \local_ubion\base\Common;

class SMS extends \local_ubsend\core\sms\AbstractSMS {
    
    public function doSend()
    {
        global $CFG;
        
        // 권한 검사
        if ($this->isSendPermission()) {
            $context = \context_course::instance($this->course->id);
            
            // 중복된 사용자 제거해야됨.
            $users = Parameter::postArray('tousers', null, PARAM_INT);
            $uniqueUsers = array_unique($users);
            $uniqueUserCount = count($uniqueUsers);
            
            if ($uniqueUserCount > 0) {
                
                $subject = Parameter::post('subject', null, PARAM_NOTAGS);
                $content = Parameter::post('sms_content', null, PARAM_NOTAGS);
                $smsfrom = Parameter::post('sms_from', null, PARAM_ALPHANUMEXT);
                
                $validate = $this->validate()->make(array(
                    'sms_content' => array(
                        'required'
                        ,$this->validate()::PLACEHOLDER => get_string('sms_content', $this->pluginname)
                        ,$this->validate()::PARAMVALUE => $content
                    )
                    ,'sms_from' => array(
                        'required'
                        ,$this->validate()::PLACEHOLDER => get_string('sms_from', $this->pluginname)
                        ,$this->validate()::PARAMVALUE => $smsfrom
                    )
                ));
                
                if ($validate->fails()) {
                    Javascript::printAlert($validate->getErrorMessages());
                } else {
                    // 메시지 전송
                    list ($isSuccess, $errorMessage, $smsid) = $this->setSend($smsfrom, $uniqueUsers, $subject, $content);
                    
                    if ($isSuccess) {
                        redirect($CFG->wwwroot.'/local/ubsend/sms/log.php?id='.$this->course->id);
                    } else {
                        Javascript::printAlert($errorMessage);
                    }
                }
            } else {
                Javascript::printAlert(get_string('min_user', 'local_ubion'));
            }
        } else {
            Common::printNotice(get_string('no_permissions'), $CFG->wwwroot.'/course/view.php?id='.$this->course->id);
        }
        
        exit;
    }
    
    
    /**
     * SMS 전송
     * 
     * @see \local_ubsend\core\sms\AbstractSMS::setSend()
     */
    public function setSend($phone, array $users, $subject, $content, $options=array())
    {
        global $USER, $DB;
        
        // 추가적인 파라메터가 필요한 경우 $options 항목을 이용하시면 됩니다.
        
        $smsid = false;
        $isSuccess = true;
        $errorMessage = null;
        
        // 전달된 내용이 maxbyte가 오버되었는지 확인
        $content = $this->cutString($content);
        
        // TODO 각 학교에 맞게 실제 전송 로직 구현하면 됨
        /*
        foreach ($users as $userid) {
            if ($phone = $this->getToPhone($userid)) {
                
            }
        }
        */
        
        if ($isSuccess) {
            $sms = new \stdClass();
            $sms->courseid = $this->course->id;
            $sms->userid = $USER->id;
            $sms->smsto = join(",", $users);
            $sms->smsto_cnt = count($users);
            $sms->subject = $subject;
            $sms->content = $content;
            $sms->timecreated = time();
            $sms->phone = $phone;
            if ($smsid = $DB->insert_record('local_ubsend_sms', $sms)) {
                
                // event 기록
                $event = \local_ubsend\event\sms_sent::create(array(
                    'objectid' => $smsid,
                    'userid' => $USER->id, // Using the user who read the message as they are the ones performing the action.
                    'context'  => \context_course::instance($this->course->id)
                ));
                $event->trigger();
            }
        } else {
            
        }
        
        
        return array($isSuccess, $errorMessage, $smsid);
    }
}