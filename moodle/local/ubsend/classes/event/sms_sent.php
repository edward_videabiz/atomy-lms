<?php
namespace local_ubsend\event;

defined('MOODLE_INTERNAL') || die();

class sms_sent extends \core\event\base {

    /**
     * Init method.
     */
    protected function init() {
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'local_ubsend_sms';
    }

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_sms_sent', 'local_ubsend');
    }

    /**
     * Returns relevant URL.
     *
     * @return \moodle_url
     */
    public function get_url() {
    	return new \moodle_url('/local/ubsend/sms/log.php', array('id' => $this->other['touser']));
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "A sms was sent by the system to the user with id '".$this->other['touser']."'.";
    }

    /**
     * Custom validation.
     *
     * @throws \coding_exception
     * @return void
     */
    protected function validate_data() {
        parent::validate_data();
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'local_ubsend_sms', 'restore' => 'local_ubsend_sms');
    }
}
