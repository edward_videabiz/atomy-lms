<?php
namespace local_ubsend\event;

defined('MOODLE_INTERNAL') || die();

class email_sent extends \core\event\base {

    /**
     * Init method.
     */
    protected function init() {
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'local_ubsend_email';
    }

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_email_sent', 'local_ubsend');
    }

    /**
     * Returns relevant URL.
     *
     * @return \moodle_url
     */
    public function get_url() {
    	return new \moodle_url('/local/ubsend/email/log.php', array('id' => $this->other['touser']));
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "A email was sent by the system to the user with id '".$this->other['touser']."'.";
    }

    /**
     * Custom validation.
     *
     * @throws \coding_exception
     * @return void
     */
    protected function validate_data() {
        parent::validate_data();
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'local_ubsend_email', 'restore' => 'local_ubsend_email');
    }
}
