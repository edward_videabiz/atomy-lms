<?php
namespace local_ubsend\event;

defined('MOODLE_INTERNAL') || die();

class message_visibled extends \core\event\base {

    /**
     * Init method.
     */
    protected function init() {
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'local_ubsend_message_users';
    }

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_message_visibled', 'local_ubsend');
    }

    /**
     * Returns relevant URL.
     *
     * @return \moodle_url
     */
    public function get_url() {
    	return new \moodle_url('/local/ubsend/message/index.php');
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "message ".$this->other['send_type']." visibled lmf_id : ".$this->objectid;
    }
    
    /**
     * Custom validation.
     *
     * @throws \coding_exception
     * @return void
     */
    protected function validate_data() {
    	parent::validate_data();
    
    	if (!isset($this->other['send_type'])) {
    		throw new \coding_exception('The \'send_type\' value must be set in other.');
    	}
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'local_ubsend_message_users', 'restore' => 'local_ubsend_message_users');
    }
    
    public static function get_other_mapping() {
        // 메시지는 복구 대상이 아니기 때문에 따로 구현하지 않음
        return false;
    }
}
