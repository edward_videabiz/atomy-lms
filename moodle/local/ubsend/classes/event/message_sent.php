<?php
namespace local_ubsend\event;

defined('MOODLE_INTERNAL') || die();

class message_sent extends \core\event\base {

    /**
     * Init method.
     */
    protected function init() {
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_OTHER;
        $this->data['objecttable'] = 'local_ubsend_message';
    }

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_message_sent', 'local_ubsend');
    }

    /**
     * Returns relevant URL.
     *
     * @return \moodle_url
     */
    public function get_url() {
    	// group_key값이 넘어오면(0보다 큰 경우) 그룹메시지로 판단해야됨.
    	if ($this->other['group_key'] > 0) {
    		return new \moodle_url('/local/ubsend/message/gmessage.php', array('id' => $this->other['group_key']));
		} else {
    		return new \moodle_url('/local/ubsend/message/message.php', array('id' => $this->other['touser']));
    	}
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        return "A message was sent by the system to the user with id '".$this->other['touser']."'.";
    }

    /**
     * Custom validation.
     *
     * @throws \coding_exception
     * @return void
     */
    protected function validate_data() {
        parent::validate_data();

    	if (!isset($this->other['touser'])) {
    		throw new \coding_exception('The \'touser\' value must be set in other.');
    	}
    	
    	if (!isset($this->other['group_key'])) {
    		throw new \coding_exception('The \'group_key\' value must be set in other.');
    	}
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'local_ubsend_message', 'restore' => 'local_ubsend_message');
    }
    
    public static function get_other_mapping() {
        // 메시지는 복구 대상이 아니기 때문에 따로 구현하지 않음
        return false;
    }
}
