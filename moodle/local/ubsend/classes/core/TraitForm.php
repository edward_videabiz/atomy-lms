<?php
namespace local_ubsend\core;

use context_course;
use local_ubion\core\traits\TraitEditor;
use local_ubion\core\traits\TraitFileForm;
use local_ubion\core\traits\TraitCourseUser;

trait TraitForm {
    use TraitEditor;
    use TraitFileForm;
    use TraitCourseUser;
    
    /**
     * 사용자 선택 폼
     * 
     * @param number $courseid
     * @param string $propertyName
     * @param array $userChecked 재전송인 경우 선택되어야 할 사용자 목록
     * @return string
     */
    public function getUserForm($courseid, $propertyName, $sortorder = 'idnumber', array $userChecked = array())
    {
        global $PAGE;
        
        
        $context = context_course::instance($courseid);
        $rolenames = role_fix_names(get_profile_roles($context), $context, ROLENAME_ALIAS, true);
        
        $users = $this->getCourseUserAll($courseid, array('(SELECT GROUP_CONCAT(roleid) FROM {role_assignments} WHERE contextid = '.$context->id.' AND userid = u.id) AS roleids'), null, $sortorder);
        $isProfessor = \local_ubion\course\Course::getInstance()->isProfessor($courseid);
        
        $html = '';
        if ($users) {
            $html = $this->getUserHtml($users, $propertyName, $rolenames, $isProfessor, $userChecked);
        } else {
            $html = '<div class="form-control-static strong">'.get_string('no_user_course', $pluginname).'</div>';
        }
        
        return $html;
    }
    
    
    public function isStudentRadio()
    {
        return true;
    }
    
    /**
     * 실제 사용자 관련 html을 리턴 해줌.
     * @param array $users
     * @param string $propertyName
     * @param array $rolenames
     * @param boolean $isProfessor
     * @param array $userChecked
     * @param array $allUsers
     * @return string
     */
    protected function getUserHtml($users, $propertyName, array $rolenames, $isProfessor = false, array $userChecked = array(), array &$allUsers = array())
    {   
        $CUser = \local_ubion\user\User::getInstance();
        $CCourse = \local_ubion\course\Course::getInstance();
        
        $roleIDProf = $CCourse->getRoleProfessor();
        
        $html = '<ul class="users row">';
        
        foreach ($users as $u) {
            $u->fullname = fullname($u);
            
            $isDisabled = false;
            if (!empty($propertyName)) {
                // 항목이 없거나, 빈값이라면 dislabed
                if (!isset ($u->$propertyName) && empty($u->$propertyName)) {
                    $isDisabled = true;
                } else {
                    // 만약 이메일 인 경우에는 @에 대한 예외처리 해야됨.
                    if (!$this->isUserExtendCheck($u->$propertyName)) {
                        $isDisabled = true;
                    }
                }
            }
            
            $labelClass = $disabled = '';
            if ($isDisabled) {
                $labelClass = 'disabled';
                $disabled = 'disabled="disabled"';
            }
            
            $checked = '';
            // 체크대상자 중에서도 disabled 되는 사용자라면 체크가 되면 안됨.
            if (in_array($u->id, $userChecked) && !$isDisabled) {
                $checked = 'checked="checked"';
            }
            
            
            // 사진
            $title = $u->fullname;
            if (!empty($propertyName)) {
                $title .= ' ('.$u->$propertyName.')';
            }
            
            $inputType = 'checkbox';
            if (!$isProfessor && $this->isStudentRadio()) {
                $inputType = 'radio';
            }
            
            $html .= '<li class="col-md-6">';
            $html .=    '<div class="'.$inputType.'">';
            $html .=        '<label class="'.$labelClass.'" title="'.$title.'">';
            $html .=            '<input type="'.$inputType.'" name="tousers[]" value="'.$u->id.'" class="minlength[1] form-check-input" '.$checked.' '.$disabled.' />';
            $html .=	        '<img src="'.$CUser->getPicture($u, 30).'" alt="'.$u->fullname.'" class="mr-2 userpicture" />';
            $html .= 	        '<div class="userinfo">';
            
            // 역할 표시
            // roleids를 분해해서 사용해야됨.
            if (!empty($u->roleids)) {
                $roles = explode(',', $u->roleids);
                
                foreach ($roles as $userRole) {
                    $userRole = trim($userRole);
                    
                    $labelClass = 'label';
                    if ($userRole == $roleIDProf) {
                        $labelClass .= ' label-primary';
                    } else {
                        $labelClass .= ' label-default';
                    }
                    
                    $html .= '<span class="'.$labelClass.'">'.$rolenames[$userRole].'</span> ';
                }
                
                $html .= '<br/>';
            }
            
            $html .=                '<span class="username">'.$u->fullname.' ('.$CUser->getIdnumber($u->idnumber, $isProfessor).')</span>';
            $html .=		        '<br/>';
            
            $value = (empty($propertyName) || empty($u->$propertyName)) ? "&nbsp;" : $u->$propertyName;
            $html .=                '<small>'.$value.'</small>';
            $html .=	        '</div>';
            $html .=        '</label>';
            $html .=    '</div>';
            $html .= '</li>';
            
            if ($allUsers && array_key_exists($u->id, $allUsers)) {
                unset($allUsers[$u->id]);
            }
        }
        
        $html .= '</ul>';
        
        return $html;
    }
    
    /**
     * 그룹 선택 폼
     * @param number $courseid
     * @param string $propertyName
     * @param string $sortorder
     * @param array $userChecked
     * @return string
     */
    public function getGroupForm($courseid, $propertyName, $sortorder = 'idnumber', array $userChecked = array())
    {
        global $PAGE;
        
        $CUser = \local_ubion\user\User::getInstance();
        
        $context = context_course::instance($courseid);
        
        $rolenames = role_fix_names(get_profile_roles($context), $context, ROLENAME_ALIAS, true);
        
        $allUsers = $this->getCourseUserAll($courseid, array('(SELECT GROUP_CONCAT(roleid) FROM {role_assignments} WHERE contextid = '.$context->id.' AND userid = u.id) AS roleids'), null, $sortorder);
        $isProfessor = \local_ubion\course\Course::getInstance()->isProfessor($courseid);
        
        $html = '';
        
        if ($allUsers) {
            $html .= '<div class="groupusers">';
            
            // 강좌내에 그룹분류가 존재하는지 확인해야됨.
            $groupInfo = groups_get_course_data($courseid);
            
            // 그룹분류가 존재하는 경우
            if ($groupInfo->groupings) {
                foreach ($groupInfo->groupings as $gg) {
                    $html .= '<fieldset class="fieldset fieldset-grouping">';
                    $html .=    '<legend>'.$gg->name.'</legend>';
                    $html .=    $this->getGroupFormChecked();
                    
                    // 그룹분류내 존재하는 그룹
                    if ($groups = groups_get_all_groups($courseid, null, $gg->id)) {
                        foreach ($groups as $g) {
                            $html .= '<fieldset class="fieldset fieldset-group">';
                            $html .=    '<legend>'.$g->name.'</legend>';
                            $html .=    $this->getGroupFormChecked();
                            
                            if ($users = groups_get_members($g->id, 'u.*', $sortorder)) {
                                $html .= $this->getUserHtml($users, $propertyName, $rolenames, $isProfessor, $userChecked, $allUsers);
                            } else {
                                $html .= '<div class="text-error">'.get_string('no_user_group', 'local_ubion').'</div>';
                            }
                            $html .= '</fieldset>';
                
                            // 그룹 분류에 소속되지 않은 그룹도 존재하기 때문에 전체 그룹 목록에서 
                            // 출력된 그룹은 제외시켜줌
                            unset($groupInfo->groups[$g->id]);
                        }
                    } else {
                        $html .= '<div class="text-error">'.get_string('groupsnone', 'group').'</div>';
                    }
                    $html .= '</fieldset>';
                }
                
                // 그룹 분류에 소속되지 않은 그룹도 존재함
                foreach ($groupInfo->groups as $g) {
                    $html .= '<fieldset class="fieldset fieldset-group">';
                    $html .=    '<legend>'.$g->name.'</legend>';
                    $html .=    $this->getGroupFormChecked();
                    
                    if ($users = $this->getGroupMembers($g->id, $context->id, $sortorder)) {
                        $html .= $this->getUserHtml($users, $propertyName, $rolenames, $isProfessor, $userChecked, $allUsers);
                    } else {
                        $html .= '<div class="text-error">'.get_string('no_user_group', 'local_ubion').'</div>';
                    }
                    $html .= '</fieldset>';
                }
            } else {
                foreach ($groupInfo->groups as $g) {
                    $html .= '<fieldset class="fieldset fieldset-group">';
                    $html .=    '<legend>'.$g->name.'</legend>';
                    $html .=    $this->getGroupFormChecked();
                    
                    if ($users = $this->getGroupMembers($g->id, $context->id, $sortorder)) {
                        $html .= $this->getUserHtml($users, $propertyName, $rolenames, $isProfessor, $userChecked, $allUsers);
                    } else {
                        $html .= '<div class="text-error">'.get_string('no_user_group', 'local_ubion').'</div>';
                    }
                    $html .= '</fieldset>';
                }
            }
            
            // 그룹에 포함되지 않은 사용자도 존재할수 있음.
            $html .=    '<fieldset class="fieldset fieldset-group">';
            $html .=        '<legend>기타</legend>';
            $html .=        $this->getGroupFormChecked();
            $html .=        $this->getUserHtml($allUsers, $propertyName, $rolenames, $isProfessor, $userChecked);
            $html .=    '</fieldset>';
            $html .= '</div>';
        }
        
        return $html;
    }
    
    /**
     * 그룹내 구성원 리턴 (사용자별 권한 정보도 같이 리턴해줍니다.)
     * groups_get_members()와 비슷하지만 추가적으로 사용자별 역할 정보도 같이 리턴해줍니다. 
     * 역할 정보가 필요없다면 groups_get_members() 함수를 호춣해서 사용하시면 됩니다.
     * 
     * @param number $groupid
     * @param number $contextid
     * @param string $sortorder
     * @return array
     */
    protected function getGroupMembers($groupid, $contextid, $sortorder)
    {
        global $DB;
        
        $query = "SELECT 
                            u.*
                            ,(SELECT GROUP_CONCAT(roleid) FROM {role_assignments} WHERE contextid = '.$contextid.' AND userid = u.id) AS roleids
                FROM        {user} u
                JOIN        {groups_members} gm
                WHERE       u.id = gm.userid 
                AND         gm.groupid = :groupid
                ORDER BY    ".$sortorder;
        
        return $DB->get_records_sql($query, array('groupid' => $groupid));
    }
    
    
    /**
     * 그룹내 전체선택/선택해제 html 내용을 리턴해줍니다.
     * @return string
     */
    protected function getGroupFormChecked()
    {   
        $html  = '<div class="btn-group btn-group-checked">';
        $html .= 	'<button type="button" class="btn btn-default btn-xs btn-group-checked btn-group-tooltip" title="'.get_string('allchecked', 'local_ubion').'">A</button>';
        $html .= 	'<button type="button" class="btn btn-default btn-xs btn-group-unchecked btn-group-tooltip" title="'.get_string('unchecked', 'local_ubion').'">C</button>';
        $html .= '</div>';
        
        return $html;
    }
    
    
    /**
     * 전달된 값에 대해서 추가적으로 검사가 필요한 경우 override 해서 validation 검사해주시면 됩니다.
     * 
     * 예 : 이메일 값이 @인 경우에 false를 리턴
     * @param string $value
     * @return boolean
     */
    public function isUserExtendCheck($value)
    {
        return true;
    }
    
}