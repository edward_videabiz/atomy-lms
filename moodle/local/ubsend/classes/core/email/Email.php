<?php
namespace local_ubsend\core\email;

use context_course;
use local_ubsend\core\TraitForm;
use local_ubion\base\Common;
use local_ubion\base\Parameter;
use local_ubion\base\Javascript;

class Email extends \local_ubion\controller\Controller {
    
    use TraitForm;
    
    const PERMISSION_STUDENT = 1;
    const PERMISSION_PROFESSOR = 2;
    
    public $CCourse = null;
    public $course = null;
    protected $pluginname = 'local_ubsend';
    
    public function __construct($courseid)
    {
        $this->CCourse = \local_ubion\course\Course::getInstance();
        $this->course = $this->CCourse->getCourse($courseid);
    }
    
    public function doSend()
    {
        global $CFG;
        
        // 권한 검사
        if ($this->isSendPermission()) {
            $context = \context_course::instance($this->course->id);
            
            // 중복된 사용자 제거해야됨.
            $users = Parameter::postArray('tousers', null, PARAM_INT);
            
            $uniqueUsers = array_unique($users);
            $uniqueUserCount = count($uniqueUsers);
            
            if ($uniqueUserCount > 0) {
                $subject = Parameter::post('subject', null, PARAM_NOTAGS);
                $attachement = Parameter::post('email_attachments', null, PARAM_ALPHANUMEXT);
                
                $validate = $this->validate()->make(array(
                    'subject' => array(
                        'required'
                        ,$this->validate()::PLACEHOLDER => get_string('email_subject', $this->pluginname)
                        ,$this->validate()::PARAMVALUE => $subject
                    )
                ));
                
                if ($validate->fails()) {
                    Javascript::printAlert($validate->getErrorMessages());
                } else {
                    
                    // email 보내기는 따로 앱에서 전달하지 않기 때문에 에디터를 사용하지 않는 부분에 대해서 예외처리 없습니다
                    $content = Parameter::postArray('email_content', null, PARAM_RAW);
                    
                    if (empty($content['text'])) {
                        Javascript::printAlert(get_string('error_emptycontent', $this->pluginname));
                    }
                    
                    if ($this->setSend($uniqueUsers, $subject, $content, $attachement)) {
                        redirect($CFG->wwwroot.'/local/ubsend/email/log.php?id='.$this->course->id);
                    } else {
                        Javascript::printAlert(get_string('error_email_fail', $this->pluginname));
                    }
                }
            } else {
                Javascript::printAlert(get_string('min_user', 'local_ubion'));
            }
        } else {
            Common::printNotice(get_string('no_permissions'), $CFG->wwwroot.'/course/view.php?id='.$this->course->id);
        }
        
        exit;
    }
    
    
    public function setSend(array $users, $subject, array $content, $attachement = null)
    {
        global $USER, $DB, $PAGE;
        
        if (empty($PAGE->context)) {
            $PAGE->set_context(context_course::instance($this->course->id));
        }
        
        $context = \context_course::instance($this->course->id);
        
        $send = new \stdClass();
        $send->courseid = $this->course->id;
        $send->userid = $USER->id;
        $send->mailto = join(",", $users);
        $send->subject = $subject;
        // content는 임시로 데이터 저장해놓음
        $send->content = $content['text']; 
        $send->timecreated = time();
        
        if ($send->id = $DB->insert_record('local_ubsend_email', $send)) {
            
            // 에디터에서 작성된 내용이면 추가적인 작업이 필요함.
            $editorContent = file_save_draft_area_files($content['itemid'], $context->id, 'local_ubsend', $this->getEditorAreaName(), $send->id, $this->getMoodleEditorOption(), $content['text']);
            $DB->set_field('local_ubsend_email', 'content', $editorContent, array('id'=>$send->id));
            
            // 메일 보낼때에는 첨부파일에 대해서 링크를 다시 걸어줘야됨.
            $editorContent = format_text(file_rewrite_pluginfile_urls($editorContent, 'pluginfile.php', $context->id, 'local_ubsend', $this->getEditorAreaName(), $send->id), FORMAT_MOODLE, array('noclean'=>true));
            
            
            $mailFileContent = '';
            // 첨부파일이 존재하는 경우
            if (!empty($attachement)) {
                $info = file_get_draft_area_info($attachement);
                $filecount = isset($info['filecount']) ? $info['filecount'] : 0;
                
                // 파일 저장
                file_save_draft_area_files($attachement, $context->id, 'local_ubsend', $this->getFileAreaName(), $send->id, $this->getMoodleFileFormOption());
                
                if ($filecount > 0) {
                    $fs = get_file_storage();
                    $files = $fs->get_area_files($context->id, 'local_ubsend', $this->getFileAreaName(), $send->id, 'id');
                    
                    $comma = '';
                    $uploadFileName = '';
                    $mailFileContent = '<ul>';
                    foreach ($files as $file) {
                        // 디렉토리가 아닐 경우.
                        if ($file->is_directory() and $file->get_filename() == '.') {
                            continue;
                        }
                        
                        $filename = $file->get_filename();
                        $fileurl = \moodle_url::make_file_url('/pluginfile.php', '/'.$context->id.'/local_ubsend/'.$this->getFileAreaName().'/'.$send->id.'/'.$filename, true);
                        $mailFileContent.= '<li><a href="'.$fileurl.'">'.$filename.'</a></li>';
                        
                        
                        $uploadFileName .= $comma.$filename;
                        $comma = ', ';
                    }
                    
                    $mailFileContent .= '</ul>';
                }
                
                $DB->set_field('local_ubsend_email', 'filecnt', $filecount, array('id'=>$send->id));
            }
            
            // 첨부파일이 존재하는 경우
            if (!empty($mailFileContent)) {
                $editorContent .= '<p>&nbsp;</p>'.$mailFileContent;
            }
            
            // error_log($editorContent);
            
            get_mailer('buffer');
            foreach ($users as $uid) {
                // 사용자 아이디만 존재하기 때문에 user정보를 다시 가져와야됨.
                if ($toUser = $DB->get_record_sql("SELECT * FROM {user} WHERE id = :id", array("id"=>$uid))) {
                    email_to_user($toUser, $USER, $send->subject, strip_tags($editorContent, '<a><p>'), $editorContent);
                }
            }
            get_mailer('close');
            
            
            // event 기록
            $event = \local_ubsend\event\email_sent::create(array(
                'objectid' => $send->id,
                'userid' => $USER->id, // Using the user who read the message as they are the ones performing the action.
                'context'  => $context
            ));
            $event->trigger();
            
            return $send->id;
        }
        
        return false;
    }
    
    public function getEditorAreaName()
    {
        return 'email_content';
    }
    
    public function getFileAreaName()
    {
        return 'email_attachments';
    }
    
    private function getEmailParamContent()
    {
        if (is_array($_POST['email_content'])) {
            return Parameter::postArray('email_content', null, PARAM_RAW);
        } else {
            return Parameter::post('email_content', null, PARAM_RAW);
        }
    }
    
    public function isSendPermission()
    {
        // 이메일 보내기를 사용할수 있는 사용자 체크
        $permission = get_config($this->pluginname, 'email_send_permission');
        
        // 설정값이 존재하지 않으면 교수로 기본 설정
        if (empty($permission)) {
            $permission = self::PERMISSION_PROFESSOR;
        }
        
        // 교수자인 경우 강좌내 교수권한이 존재하는지 확인
        if ($permission == self::PERMISSION_PROFESSOR) {
            if ($this->CCourse->isProfessor($this->course->id)) {
                return true;
            } else {
                return false;
            }
        }
        
        // 교수 이외에는 무조건 true로 전달해도 무방할듯함.
        return true;
    }
    
    public function isUserExtendCheck($value)
    {
        // @인 경우에는 disabled 시켜줘야됨.
        if ($value == '@') {
            return false;
        }
        
        return true;
    }
    
    
    /**
     * 기본 에디터 설정
     * 기본값 그대로 사용해도 되지만 상황에 따라 Trait를 상속 받은 곳에서 재정의가 필요할수 있습니다.
     *
     * @param array $addOpeion
     * @return array
     */
    public function getMoodleEditorOption(array $addOpeion = array())
    {
        global $CFG, $CFG;
        require_once $CFG->dirroot.'/repository/lib.php';
        
        // 에디터는 사진만 업로드 되기 때문에 따로 제한을 걸어두지 않음.
        $maxbytes = get_user_max_upload_file_size(\context_course::instance($this->course->id), $CFG->maxbytes, $this->course->maxbytes);
        
        $options = array(
            'noclean'=>true,
            'maxfiles' => $this->getEmailEditorFileCount(),
            'maxbytes' => $maxbytes,
            'trusttext'=> true,
            'return_types'=> FILE_INTERNAL | FILE_EXTERNAL,
        );
        
        if (!empty($param_options)) {
            $options = array_merge($options, $addOpeion);
        }
        
        return $options;
    }
    
    protected function getEmailEditorFileCount()
    {
        return get_config($this->pluginname, 'email_editor_filecount');
    }
    
    
    /**
     * 기본 파일폼 설정
     * 기본값 그대로 사용해도 되지만 상황에 따라 Trait를 상속 받은 곳에서 재정의가 필요할수 있습니다.
     *
     * @param array $addOpeion
     * @return array
     */
    public function getMoodleFileFormOption(array $addOpeion = array())
    {
        global $CFG;
        require_once $CFG->dirroot.'/repository/lib.php';
        
        // $maxbytes = get_user_max_upload_file_size(\context_course::instance($this->course->id), $CFG->maxbytes, $this->course->maxbytes);
        $maxbytes = get_config($this->pluginname, 'email_filesize');
        
        $options = array(
            'subdirs' => false
            ,'maxfiles' => $this->getEmailFileCount()
            ,'maxbytes' => $maxbytes
        );
        
        if (!empty($param_options)) {
            $options = array_merge($options, $addOpeion);
        }
        
        return $options;
    }
    
    protected function getEmailFileCount()
    {
        return get_config($this->pluginname, 'email_editor_filecount');
    }
    
    
    public function getTab($currentTab = 'send')
    {
        global $CFG;
        
        $basePath = $CFG->wwwroot.'/local/ubsend/email';
        
        // 탭 관련 설정
        $tabs = array();
        $tabs[] = new \tabobject('send', $basePath.'/send.php?id='.$this->course->id, get_string('email_send', $this->pluginname));
        $tabs[] = new \tabobject('log', $basePath.'/log.php?id='.$this->course->id, get_string('email_log', $this->pluginname));
        
        return print_tabs(array($tabs), $currentTab, null, null, true);
    }
    
    
    /**
     * 단일 로그 기록 가져오기
     * @param number $id
     * @return mixed|boolean
     */
    public function getLog($id)
    {
        global $DB;
        
        // 다른 강좌의 기록을 가져올수 있기 때문에 courseid 조건문을 추가함.
        return $DB->get_record_sql("SELECT * FROM {local_ubsend_email} WHERE id = :id AND courseid = :courseid", array('id' => $id, 'courseid' => $this->course->id));
    }
    
    /**
     * 로그 기록 가져오기
     * 
     * @param number $page
     * @param number $ls
     * @return array()
     */
    public function getLogs($page = 1, $ls = 15)
    {
        global $DB;
        
        // 가져올 게시물 갯수
        $page = ($page <= 0) ? 1 : $page;
        $limitfrom = $ls * ($page - 1);
        
        $query = "SELECT
    						COUNT(1)
    			FROM		{local_ubsend_email}
    			WHERE		courseid = :courseid";
        
        $param = array('courseid' => $this->course->id);
        
        $totalCount = $DB->get_field_Sql($query, $param);
        
        
        $query = "SELECT
    						*
    			FROM		{local_ubsend_email}
    			WHERE		courseid = :courseid
    			ORDER BY	timecreated DESC";
        
        $lists = $DB->get_records_sql($query, $param, $limitfrom, $ls);
        
        
        return array($totalCount, $lists);
    }
    
    
    public function getDate($unixtime)
    {
        return date('Y-m-d h:i:s', $unixtime);
    }
}