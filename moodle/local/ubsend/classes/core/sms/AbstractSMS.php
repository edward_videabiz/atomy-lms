<?php
namespace local_ubsend\core\sms;

use local_ubsend\core\TraitForm;
use local_ubion\base\Common;
use local_ubion\base\Parameter;
use local_ubion\base\Javascript;

abstract class AbstractSMS extends \local_ubion\controller\Controller {
    use TraitForm;
    
    const PERMISSION_STUDENT = 1;
    const PERMISSION_PROFESSOR = 2;
    
    public $CCourse = null;
    public $course = null;
    protected $pluginname = 'local_ubsend';
    
    public function __construct($courseid)
    {
        $this->CCourse = \local_ubion\course\Course::getInstance();
        $this->course = $this->CCourse->getCourse($courseid);
    }
    
    
    abstract public function doSend();
    abstract public function setSend($phone, array $users, $subject, $content, $options = array());
    
    /**
     * SMS 최대 전송 Bytes
     * @return number
     */
    public function getMaxBytes()
    {
        $maxbytes = get_config($this->pluginname, 'sms_maxbytes');
        
        // SMS 관련 설정이 안되어 있을 경우를 대비한 기본값 셋팅
        if (empty($maxbytes)) {
            $maxbytes = 80;
        }
        
        return $maxbytes;
    }
    
    /**
     * 한글 바이트 수
     * 
     * @return number
     */
    public function getHangleBytes()
    {
        $hanglebytes = get_config($this->pluginname, 'sms_hanglebytes');
        
        // SMS 관련 설정이 안되어 있을 경우를 대비한 기본값 셋팅
        if (empty($hanglebytes)) {
            $hanglebytes = 2;
        }
        
        return $hanglebytes;
    }
    
    public function isSendPermission()
    {
        // SMS 보내기를 사용할수 있는 사용자 체크
        $permission = get_config($this->pluginname, 'sms_send_permission');
        
        // 설정값이 존재하지 않으면 교수로 기본 설정
        if (empty($permission)) {
            $permission = self::PERMISSION_PROFESSOR;
        }
        
        // 교수자인 경우 강좌내 교수권한이 존재하는지 확인
        if ($permission == self::PERMISSION_PROFESSOR) {
            if ($this->CCourse->isProfessor($this->course->id)) {
                return true;
            } else {
                return false;
            }
        }
        
        // 교수 이외에는 무조건 true로 전달해도 무방할듯함.
        return true;
    }
    
    
    public function getTab($currentTab = 'send')
    {
        global $CFG;
        
        $basePath = $CFG->wwwroot.'/local/ubsend/sms';
        
        // 탭 관련 설정
        $tabs = array();
        $tabs[] = new \tabobject('send', $basePath.'/send.php?id='.$this->course->id, get_string('sms_send', $this->pluginname));
        $tabs[] = new \tabobject('log', $basePath.'/log.php?id='.$this->course->id, get_string('sms_log', $this->pluginname));
        
        return print_tabs(array($tabs), $currentTab, null, null, true);
    }
    
    
    public function isUserExtendCheck($value)
    {
        // @인 경우에는 disabled 시켜줘야됨.
        if (empty($value)) {
            return false;
        }
        
        return true;
    }
    
    
    /**
     * 단일 로그 기록 가져오기
     * @param number $id
     * @return mixed|boolean
     */
    public function getLog($id)
    {
        global $DB;
        
        // 다른 강좌의 기록을 가져올수 있기 때문에 courseid 조건문을 추가함.
        return $DB->get_record_sql("SELECT * FROM {local_ubsend_sms} WHERE id = :id AND courseid = :courseid", array('id' => $id, 'courseid' => $this->course->id));
    }
    
    /**
     * 로그 기록 가져오기
     *
     * @param number $page
     * @param number $ls
     * @return array()
     */
    public function getLogs($page = 1, $ls = 15)
    {
        global $DB;
        
        // 가져올 게시물 갯수
        $page = ($page <= 0) ? 1 : $page;
        $limitfrom = $ls * ($page - 1);
        
        $query = "SELECT
    						COUNT(1)
    			FROM		{local_ubsend_sms}
    			WHERE		courseid = :courseid";
        
        $param = array('courseid' => $this->course->id);
        
        $totalCount = $DB->get_field_Sql($query, $param);
        
        
        $query = "SELECT
    						*
    			FROM		{local_ubsend_sms}
    			WHERE		courseid = :courseid
    			ORDER BY	timecreated DESC";
        
        $lists = $DB->get_records_sql($query, $param, $limitfrom, $ls);
        
        
        return array($totalCount, $lists);
    }
    
    /**
     * maxbyte보다 문자열이 긴 경우 해당 길이 만큼 문자열을 잘라서 리턴해줍니다.
     *  
     * @param string $string
     * @param number $maxbytes
     * @return string
     */
    public function cutString($string, $maxbytes=null)
    {
        // 2byte 체크 : mb_strwidth, mb_strimwidth
        // 3byte 체크 : strlen,  mb_strcut
        
        // 전달된 바이트값이 없으면 기본 설정값으로 체크
        // SMS 내용 이외로 제목이나 다른 항목에 의해서 maxbytes가 달라질수도 있기 때문에 예외항목을 둠.
        if (empty($maxbytes)) {
            $maxBytes = $this->getMaxBytes();
        }
        
        $hangleBytes = $this->getHangleBytes();
        
        $stringBytes = mb_strwidth($string, 'UTF-8');
        if ($hangleBytes == 3) {
            $stringBytes = strlen($string);
        }
        
        // 최대 전송 크기보다 큰 값이 전송되었더라면 문자열을 잘라서 보내야됨.
        if ($stringBytes > $maxBytes) {
            if ($hangleBytes == 3) {
                $string = mb_strcut ($string, 0, $maxBytes);
            } else {
                $string = mb_strimwidth ($string, 0, $maxBytes, null, 'UTF-8');
            }
        }
        
        return $string;
    }
    
    /**
     * 발신자 핸드폰 번호
     * 
     * @return string
     */
    public function getFromPhone()
    {
        global $USER;
        
        return $USER->phone2;
    }
    
    
    public function getToPhone($userid)
    {
        global $DB;
        
        $phone = null;
        if ($userInfo = $DB->get_record_sql("SELECT idnumber, firstname, phone, phone2 FROM {user} WHERE id = :id", array("id"=>$userid))) {
            $phone = preg_replace("/[^0-9]*/s", "", $userInfo->phone2);
        }
        
        return $phone;
    }
    
    public function getDate($unixtime)
    {
        return date('Y-m-d h:i:s', $unixtime);
    }
}