<?php
namespace local_ubsend\core\message;

use local_ubion\base\Common;
use local_ubion\base\Javascript;
use local_ubion\base\Parameter;
use local_ubion\base\Paging;
use local_ubsend\core\TraitForm;
use local_ubion\core\traits\TraitPush;

class Message extends \local_ubion\controller\Controller
{
    use TraitForm;
    use TraitPush;

    protected $pluginname = 'local_ubsend';

    public function isSendPermission()
    {
        // 메시지는 모든 사용자가 보낼수 있기 때문에 따로 설정으로 빼놓지 않음.
        // 향후 필요시 설정으로 빼놓겠습니다.
        return true;
    }

    /**
     * 메시지 전송 이벤트
     *
     * @param \stdClass $message
     * @param array $tousers
     * @param number $to
     * @param number $group_key
     */
    function eventSend($message, $tousers, $to = 0, $groupKey = 0)
    {
        global $USER;

        // Trigger event for reading a message.
        // 메시지 내용은 logstore_standard_log 테이블 용량 커지는 문제로 포함시키지 않습니다.
        // 만약 필요하다면, objectid를 사용해서 local_ubsend_message 테이블에 조회하시기 바랍니다.
        $event = \local_ubsend\event\message_sent::create(array(
            'objectid' => $message->id,
            'userid' => $USER->id, // Using the user who read the message as they are the ones performing the action.
            'context' => \context_user::instance($USER->id),
            'other' => array(
                'touser' => $to,
                'group_key' => $groupKey
            )
        ));
        $event->trigger();

        // groupuserids : 그룹 메시지에 본인이 포함되는 경우
        // userids : 본인을 제외한 push를 전달받은 사용자
        $groupuserids = $userids = $comma = null;

        foreach ($tousers as $ts) {
            // 그룹 메시지인데, 본인이 포함되어 있으면, 푸시 전달 파라메터가 달라져야됨.
            // 단일 메시지를 본인한테도 보낼수도 있기 때문에 그룹 메시지 여부를 조건문으로 추가해놔야됨.
            if ($ts == $USER->id && $message->group_count > 0) {
                $groupuserids = $ts;
            } else {
                $userids .= $comma . $ts;
                $comma = ',';
            }
        }

        // 메시지 전송과 동시에 푸시를 전달할 경우
        // message에 전달하는 사용자 정보를 추가해줘야됨
        $message->userid = $USER->id;
        $message->idnumber = $USER->idnumber;
        $message->firstname = $USER->firstname;
        $message->lastname = $USER->lastname;

        $isPushUpdate = false;

        // 그룹 메시지인 경우 본인을 제외한
        if (! empty($userids)) {
            $parameter = $this->pushParameter($message, $userids, false);
            list ($isSuccess, $result) = $this->push($parameter);

            // 푸시가 정상적으로 전송이 되었으면 push값을 1로 변경해줘야됨.
            if ($isSuccess) {
                $this->setPushComplete($message->id);

                $isPushUpdate = true;
            }
        }

        // 그룹 메시지이고, 본인이 포함된 경우
        if (! empty($groupuserids)) {

            $parameter = $this->pushParameter($message, $groupuserids, true);
            list ($isSuccess, $result) = $this->push($parameter);

            // 푸시가 정상적으로 전송이 되었으면 push값을 1로 변경해줘야됨.
            // 그룹메시지를 받는 사용자한테만 제대로 전달이 되었다면, 본인한테 푸시가 전달되지 않아도 푸시 전송 완료로 판단해야됨.
            // 그리고 사용자한테 전송이 완료가 되었으면 다시 push값을 1로 업데이트 할 필요가 없음.
            if (! $isPushUpdate && $isSuccess) {
                $this->setPushComplete($message->id);
            }
        }
    }

    /**
     * 푸시 전송완료
     *
     * @param int $messageid
     */
    public function setPushComplete($messageid)
    {
        global $DB;

        $message = new \stdClass();
        $message->id = $messageid;
        $message->push = 1;
        $DB->update_record('local_ubsend_message', $message);
    }

    /**
     * 메시지 목록
     *
     * @param int $courseid
     * @param string $keyfield
     * @param string $keyword
     * @param int $page
     * @param int $ls
     * @return array[]|mixed[]|boolean[]
     */
    public function getLists($courseid = null, $keyfield = null, $keyword = null, $page = 1, $ls = 15)
    {
        global $USER, $DB;

        $userid = $USER->id;

        $replace = self::REPLACE;
        $joinReplace = "##JOIN##";
        $joinQuery = "";

        $query = "SELECT
                			" . $replace . "
                  FROM		{local_ubsend_message} um
                  JOIN 		(
                    			SELECT
                    					MAX(messageid) AS messageid
                    					,MAX(useridfrom) AS useridfrom
                    					,MIN(useridto) AS useridto
                    					,MAX(group_key) AS group_key
						                ,MAX(IF(useridto = :userid_5, useridfrom, useridto)) AS userid
                    					,CASE
                    						WHEN group_key = 0 THEN
                    							CASE
                    								WHEN useridto = :userid_1 THEN useridfrom
                    								ELSE useridto
                    							END
                                            WHEN group_key > 0 THEN group_key
                    					END AS groupbyid
                    			FROM 	{local_ubsend_message_users}
                    			WHERE   (useridfrom = :userid_2 OR useridto = :userid_3)
                    			AND		CASE
                    						WHEN useridto = :userid_4 THEN visible_to
                    						ELSE visible_from
                    					END = 0
                    		   GROUP BY groupbyid
                  ) umu ON umu.messageid = um.id
                  JOIN		{user} u ON u.id = umu.userid
                  " . $joinReplace . "
                  WHERE     u.deleted = 0";

        $param = array(
            'userid_1' => $userid,
            'userid_2' => $userid,
            'userid_3' => $userid,
            'userid_4' => $userid,
            'userid_5' => $userid
        );

        // 강좌 번호가 전달된 경우
        if (! empty($courseid)) {
            $joinQuery = "
				JOIN		{role_assignments} ra ON ra.userid = u.id
				JOIN		{context} context ON context.id = ra.contextid
				JOIN		{course} c ON c.id = context.instanceid AND context.contextlevel = :context_level
			";

            $query .= " AND c.id = :courseid";
            $param['courseid'] = $courseid;
            $param['context_level'] = CONTEXT_COURSE;
        }

        // 검색어가 존재하는 경우
        if (! empty($keyfield) && ! empty($keyword)) {
            $keyfield = strtolower($keyfield);
            $keyword = strtolower($keyword);

            switch ($keyfield) {
                case 'username':
                    $fullname = $DB->sql_fullname();
                    $query .= " AND " . $DB->sql_like($fullname, ':keyword');
                    $param['keyword'] = '%' . $keyword . '%';
                    break;
                case 'message':
                    $query .= " AND " . $DB->sql_like('LOWER(message)', ':keyword');
                    $param['keyword'] = '%' . $keyword . '%';
                    break;
                default:
                    $query .= " AND " . $DB->sql_like('idnumber', ':keyword');
                    $param['keyword'] = $keyword . '%';
                    break;
            }
        }

        // error_log($query);
        // error_log(print_r($param, true));

        // join 관련 쿼리 replacee
        $query = str_replace($joinReplace, $joinQuery, $query);

        // 메시지 갯수
        $totalQuery = str_replace($replace, 'COUNT(1)', $query);
        $totalCount = $DB->get_field_sql($totalQuery, $param);

        $messages = array();
        if ($totalCount > 0) {
            // 메시지
            $page = empty($page) ? 1 : $page;

            // Common::print_r($query);
            // Common::print_r($param);

            $field = "umu.*,
        		      um.message,
                      um.timecreated,
                      um.group_count,
                      " . get_all_user_name_fields(true, 'u') . ",
                      u.picture,
                      u.imagealt,
                      u.email,
                      u.idnumber,
                      u.id AS userid";

            $query = str_replace($replace, $field, $query);
            $query .= ' ORDER BY um.timecreated DESC';
            $limitfrom = $ls * ($page - 1);

            $messages = $DB->get_records_sql($query, $param, $limitfrom, $ls);
        }

        return array(
            $totalCount,
            $messages
        );
    }

    /**
     * 읽지 않은 메시지 확인 쿼리
     *
     * @param int $userid
     * @return string[]|string[][]
     */
    protected function getNoReadQueryParam($userid = null)
    {
        global $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $replace = self::REPLACE;

        $query = "SELECT
                			" . $replace . "
                  FROM		{local_ubsend_message} um
                  JOIN 		{local_ubsend_message_users} umu ON um.id = umu.messageid
                  JOIN		{user} u ON u.id = umu.useridfrom
                  WHERE     u.deleted = 0
                  AND		umu.useridto = :userid
                  AND		umu.timeread IS NULL";

        $param = [
            'userid' => $userid
        ];

        return array(
            $query,
            $param
        );
    }

    /**
     * 읽지 않은 메시지 갯수
     *
     * @return int
     */
    public function getNoReadCount()
    {
        global $DB;

        list ($query, $param) = $this->getNoReadQueryParam();

        $query = str_replace(self::REPLACE, "COUNT(1)", $query);

        return $DB->get_field_sql($query, $param);
    }

    /**
     * 읽지 않은 메시지 목록
     *
     * @return array
     */
    public function getNoReadLists()
    {
        global $DB;

        list ($query, $param) = $this->getNoReadQueryParam();

        $field = "umu.*,
        		  um.message,
                  um.timecreated,
                  um.group_count,
                  " . get_all_user_name_fields(true, 'u') . ",
                  u.picture,
                  u.imagealt,
                  u.email,
                  u.idnumber,
                  u.id AS userid";

        $query = str_replace(self::REPLACE, $field, $query);
        $query .= ' ORDER BY um.timecreated DESC';

        return $DB->get_records_sql($query, $param);
    }

    /**
     * 디비에서 조회한 메시지를 화면 출력에 필요한 변수들 셋팅해서 리턴
     *
     * @param array $lists
     * @param int $courseid
     * @param string $keyfield
     * @param string $keyword
     * @param int $page
     * @return array
     */
    public function getListsSetting($lists, $courseid = null, $keyfield = null, $keyword = null, $page = null)
    {
        global $CFG, $USER, $OUTPUT;

        $CUser = \local_ubion\user\User::getInstance();
        $groupImage = $OUTPUT->image_url('group', $this->pluginname);

        $baseParam = 'courseid=' . $courseid . '&keyfield=' . $keyfield . '&keyword=' . $keyword . '&page=' . $page;
        $readURL = $CFG->wwwroot . '/local/ubsend/message/message.php?' . $baseParam;
        $groupReadURL = $CFG->wwwroot . '/local/ubsend/message/gmessage.php?' . $baseParam;

        foreach ($lists as $m) {
            // 필요한 값 셋팅팅
            $username = fullname($m);

            // 신규 메시지 여부
            $isNew = false;

            $m->mtype = 'user';
            $m->mid = $m->userid;

            // 그룹 메시지인 전송한 경우 본인한테만 그룹메시지로 표시가됨.
            // 즉 그룹메시지인 경우에는 new가 표시 될일이 없지만, 본인을 포함해서 그룹 메시지를 전송할수도 있기 때문에에
            // messageid 기준으로 조회해서 읽지 않은 메시지가 있는지 확인 해봐야됨.
            if ($m->useridfrom == $USER->id && $m->group_count > 0) {
                $m->userpicture = $groupImage;

                // 그룹 메시지 전송 후 전송 취소로 인해서 사용자가 1명 밖에 존재하지 않는 경우를 대비함.
                $groupCount = $m->group_count - 1;
                if ($groupCount > 0) {
                    $username = get_string('group_users', $this->pluginname, array(
                        'username' => $username,
                        'count' => $groupCount
                    ));
                }

                // 그룹 메시지인 경우에는 그룹 메시지내에서 읽지 않은 글이 있는지 판단해봐야됨.
                if ($this->getNotReadGroupCount($m->messageid) > 0) {
                    $isNew = true;
                }

                // 메시지 링크 : 그룹 메시지인 경우에는 gmessage로 이동해야됨.
                $url = $groupReadURL . '&id=' . $m->messageid;

                $m->mtype = 'group';
                $m->mid = $m->messageid;
            } else {
                // 사용자 사진 출력을 위해서 임시 변수 설정
                // $m->id값은 local_ubsend_message_users의 id값이기 때문에 $m값을 그대로 넘기면 엉뚱한 사진이 출력될수 있음.
                $picture = clone $m;
                $picture->id = $m->userid;

                $allnames = get_all_user_name_fields(false);
                foreach ($allnames as $key => $value) {
                    $picture->$key = $m->$value;
                }
                $m->userpicture = $CUser->getPicture($picture);

                // 일반 메시지 인 경우에는 상대방과 나눈 대화중에 안읽은 메시지가 있는지 확인해봐야됨.
                if ($this->getNotReadCount($m->userid) > 0) {
                    $isNew = true;
                }

                // 메시지 링크
                $url = $readURL . '&id=' . $m->userid;
            }

            $m->readurl = $url;
            $m->username = $username;
            $m->date = Common::getUserDate($m->timecreated);
            $m->isNew = $isNew;
        }

        return $lists;
    }

    public function doGroupSend()
    {
        global $CFG, $USER;

        // 권한 검사
        if ($this->isSendPermission()) {

            // 중복된 사용자 제거해야됨.
            $tousers = Parameter::postArray('tousers', null, PARAM_INT);

            $uniqueUsers = array_unique($tousers);
            $uniqueUserCount = count($uniqueUsers);

            if ($uniqueUserCount > 0) {
                $id = Parameter::post('id', null, PARAM_INT);
                $message = Parameter::post('message', null, PARAM_NOTAGS);

                $validate = $this->validate()->make(array(
                    'id' => array(
                        'required',
                        'number',
                        $this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion'),
                        $this->validate()::PARAMVALUE => $id
                    ),
                    'message' => array(
                        'required',
                        $this->validate()::PLACEHOLDER => get_string('message', $this->pluginname),
                        $this->validate()::PARAMVALUE => $message
                    )
                ));

                if ($validate->fails()) {
                    Javascript::printAlert($validate->getErrorMessages());
                } else {

                    $isProfessor = \local_ubion\course\Course::getInstance()->isProfessor($id);
                    $isOnlyOne = false;

                    if ($id == SITEID) {
                        // 사이트 메인이면 사이트 관리자면 여러명한테 메시지 보낼수 있음.
                        if (! is_siteadmin()) {
                            $isOnlyOne = true;
                        }
                    } else {
                        if (! $isProfessor && $this->isStudentRadio()) {
                            $isOnlyOne = true;
                        }
                    }

                    // 한명의 사용자한테만 메시지를 보내줘야되는 경우
                    if ($isOnlyOne && $uniqueUserCount > 1) {
                        Javascript::printAlert(get_string('error_message_maxuser', $this->pluginname));
                    }

                    if ($id = $this->setSend($USER->id, $uniqueUsers, $message, time())) {

                        // 일반, 단일 메시지에 따라서 주소가 달라짐
                        if ($uniqueUserCount > 1) {
                            redirect($CFG->wwwroot . '/local/ubsend/message/gmessage.php?id=' . $id);
                        } else {
                            $touserid = current($uniqueUsers);
                            redirect($CFG->wwwroot . '/local/ubsend/message/message.php?id=' . $touserid);
                        }
                        //
                    } else {
                        Javascript::printAlert(get_string('error_message_fail', $this->pluginname));
                    }
                }
            }
        } else {
            Common::printNotice(get_string('no_permissions'), $CFG->wwwroot . '/course/view.php?id=' . $this->course->id);
        }
    }

    public function setSend($from, array $tousers, $message, $timecreated, $url = null, $notification = 0, $component = 'moodle', $instancename = 'instantmessage', $subject = null, $messagehtml = null)
    {
        global $DB;

        // 메시지를 보낼수 있는지 검사해봐야됨.
        $context = \context_system::instance();
        require_capability('moodle/site:sendmessage', $context);

        // $tousers값이 unique되서 전달되지 않았을수도 있기 때문에 재검사
        $tousers = array_unique($tousers);

        $grouptCount = count($tousers);

        // 2명보다 작으면 1:1 메시지이므로 $grouptCount는 0으로 설정
        $touserid = 0;
        if ($grouptCount < 2) {
            $grouptCount = 0;
            $touserid = current($tousers);
        }

        $days = date('Ymd', $timecreated);

        $ubmessage = new \stdClass();
        $ubmessage->useridfrom = $from;
        $ubmessage->messageformat = 1;
        $ubmessage->group_count = $grouptCount;
        $ubmessage->message = $message;
        $ubmessage->notification = $notification;
        $ubmessage->contexturl = $url;
        $ubmessage->timecreated = $timecreated;
        $ubmessage->days = $days;

        // 메시지 내용 기록
        if ($ubmessage->id = $DB->insert_record('local_ubsend_message', $ubmessage)) {
            $groupKey = 0;
            if ($grouptCount > 0) {
                $groupKey = $ubmessage->id;
            }

            foreach ($tousers as $to) {
                $this->setSendFromTo($ubmessage->id, $from, $to, $days, $groupKey);
            }

            // 메시지 전송 이벤트 실행 (그룹은 to가 null이여야 함.)
            // group_key는 ubmessage->id값 그대로 전달해주면 됨.
            $this->eventSend($ubmessage, $tousers, $touserid, $groupKey);
        }

        return $ubmessage->id;
    }

    public function setSendFromTo($messageid, $from, $to, $days, $groupKey = 0)
    {
        global $DB;

        // fromto에 기록해줘야됨.
        $ubmessage_fromto = new \stdClass();
        $ubmessage_fromto->messageid = $messageid;
        $ubmessage_fromto->group_key = $groupKey;
        $ubmessage_fromto->useridfrom = $from;
        $ubmessage_fromto->useridto = $to;
        $ubmessage_fromto->days = $days;

        return $DB->insert_record('local_ubsend_message_users', $ubmessage_fromto);
    }

    /**
     * 단일 메시지 정보
     *
     * @param int $id
     * @return mixed|boolean
     */
    public function getMessage($id)
    {
        global $DB;

        $query = "SELECT * FROM {local_ubsend_message} WHERE id = :id";

        return $DB->get_record_sql($query, array(
            'id' => $id
        ));
    }

    /**
     * 상대방에게 전송된 메시지 중 읽지 않은 메시지가 있는지 확인
     *
     * @param int $targetid
     * @return mixed
     */
    function getNotReadCount($targetid)
    {
        global $DB, $USER;

        // 로그인 계정 기준으로
        // 받는 사용자가 로그인한 계정
        // 보낸 사용자가 $targetid로 판단해야됨.

        $query = "SELECT
						    COUNT(1)
				  FROM 	    {local_ubsend_message_users}
				  WHERE     useridto = :fromuserid
				  AND       useridfrom = :touserid
				  AND 	    timeread IS NULL";

        $param = [
            'fromuserid' => $USER->id,
            'touserid' => $targetid
        ];

        return $DB->get_field_sql($query, $param);
    }

    /**
     * 그룹 메시지중에 본인이 읽지 않은 메시지가 있는지 확인
     * => 본인을 포함한 그룹메시지를 전송하는 경우가 있음.
     *
     * @param number $messageid
     * @param number $userid
     * @return mixed
     */
    function getNotReadGroupCount($messageid, $userid = null)
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $query = "SELECT
						COUNT(1)
				FROM 	{local_ubsend_message_users}
				WHERE 	messageid = :messageid
				AND 	useridto = :userid
				AND 	timeread IS NULL";

        return $DB->get_field_sql($query, array(
            'messageid' => $messageid,
            'userid' => $userid
        ));
    }

    /**
     * 상대방과 나눈 대화 모두 읽음 처리
     *
     *
     * @param int $userid
     */
    function setMessageRead($targetid)
    {
        global $DB, $USER;

        $query = "SELECT
                            *
                  FROM      {local_ubsend_message_users}
                  WHERE     useridto = :fromuserid
                  AND       useridfrom = :touserid
                  AND       timeread IS NULL";

        // 받은 메시지에 대해서 읽음 처리 해야되기 때문에 from은 전달된 파라메터 to는 현재 로그인 사용자 아이디로 반영되어야 함.
        $param = [
            'fromuserid' => $USER->id,
            'touserid' => $targetid
        ];

        if ($messages = $DB->get_records_sql($query, $param)) {
            // timeread 일괄로 업데이트
            $query = "UPDATE {local_ubsend_message_users} SET timeread = :timeread WHERE useridto = :fromuserid AND useridfrom = :touserid AND timeread IS NULL";

            $param['timeread'] = time();
            $DB->execute($query, $param);

            // message_read 이벤트 기록
            foreach ($messages as $message) {
                $this->setEventRead($message->id, $USER->id, $targetid, 0);
            }
        }
    }

    /**
     * 그룹 메시지 읽음 처리
     * 그룹 메시지내에 본인이 포함되어 있을수 있기 때문에 gmessage.php에서 이 함수를 실행시켜줘야 합니다.
     *
     * @param number $messageid
     * @param number $userid
     */
    function setGroupMessageRead($messageid, $userid = null)
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        // 그룹 메시지 내에 본인이 포함되어 있을수 있음.
        // 본인한테 메시지를 보냈을 경우 read 해줘야됨.
        $query = "SELECT
                            id,
                            group_key
                  FROM      {local_ubsend_message_users}
                  WHERE     messageid = :messageid
                  AND       useridto = :useridto
                  AND       group_key = :group_key
                  AND       timeread IS NULL";

        $param = [
            'messageid' => $messageid,
            'group_key' => $messageid,
            'useridto' => $userid
        ];

        if ($message = $DB->get_record_sql($query, $param)) {

            // timeread 일괄로 업데이트
            $query = "UPDATE {local_ubsend_message_users} SET timeread = :timeread WHERE messageid = :messageid AND useridto = :to AND group_key = :group_key AND timeread IS NULL";
            $param = [
                'messageid' => $messageid,
                'to' => $userid,
                'group_key' => $messageid,
                'timeread' => time()
            ];
            $DB->execute($query, $param);

            $this->setEventRead($message->id, $userid, 0, $message->group_key);
        }
    }

    public function setEventRead($objectid, $userid, $fromuser, $groupkey)
    {
        $event = \local_ubsend\event\message_read::create(array(
            'objectid' => $objectid,
            'userid' => $userid, // Using the user who read the message as they are the ones performing the action.
            'context' => \context_user::instance($userid),
            'other' => array(
                'fromuser' => $fromuser,
                'group_key' => $groupkey
            )
        ));
        $event->trigger();
    }

    /**
     * 그룹 메시지 가져오기
     *
     * @param number $messageid
     *            : local_ubsend_message id값
     * @return multitype:
     */
    function getGroupMessageUsers($messageid)
    {
        global $DB;

        $allnames = get_all_user_name_fields(true, 'u');

        $query = "SELECT
							umu.*
							,$allnames
							,u.idnumber
							,u.picture
							,u.imagealt
							,u.email
							,u.institution
							,u.department
				FROM		{local_ubsend_message_users} umu
				JOIN		{user} u ON umu.useridto = u.id
				WHERE		umu.messageid = :id
				AND			u.deleted = :deleted";

        $param = array(
            'id' => $messageid,
            'deleted' => 0
        );

        return $DB->get_records_sql($query, $param);
    }

    /**
     * 메시지 목록
     *
     * @param array $messages
     * @param string $keyfield
     * @param string $keyword
     * @param boolean $isGroup
     * @return string|boolean
     */
    public function getMessageListHtml($messages, $keyfield = null, $keyword = null, $isGroup = false)
    {
        global $USER;

        $html = '';

        // $oldDay = null;

        foreach ($messages as $message) {

            /*
             * $day = date('Y-m-d', $message->timecreated);
             *
             * if (empty($oldDay) || $oldDay != $day) {
             * $html .= $this->getMessageDayTitleHtml($message->timecreated, $isGroup);
             * $oldDay = $day;
             * }
             */

            $side = 'from';
            if ($message->useridfrom != $USER->id) {
                $side = 'to';
            }

            $html .= $this->getMessageHtml($message, $side, $keyfield, $keyword, $isGroup);
        }

        return $html;
    }

    /**
     * 메시지 내용 - day title
     *
     * @param int $timecreated
     * @param boolean $isGroup
     * @return string|boolean
     */
    public function getMessageDayTitleHtml($timecreated, $isGroup = false)
    {
        global $OUTPUT;

        $ctx = new \stdClass();
        $ctx->ymd = date('Ymd', $timecreated);
        // $ctx->date = Common::getUserDateShort($timecreated);
        $ctx->date = userdate($timecreated, get_string('strftimedaydate'));
        $ctx->isGroup = $isGroup;

        return $OUTPUT->render_from_template('local_ubsend/template-dayheading', $ctx);
    }

    /**
     * 단일 메시지 출력
     *
     * @param \stdClass $message
     * @param string $side
     * @param string $keyfield
     * @param string $keyword
     * @param boolean $isGroup
     * @return string|boolean
     */
    public function getMessageHtml($message, $side, $keyfield = null, $keyword = null, $isGroup = false)
    {
        global $USER, $OUTPUT;

        $time = Common::getuserDate($message->timecreated);
        $twitterTime = Common::getDateDiffTwitter(null, $message->timecreated);

        $options = new \stdClass();
        $options->para = false;

        $messagetext = $message->message;

        // 태그를 그대로 출력.
        // 2014-03-18 by akddd
        $messagetext = htmlentities($messagetext);

        // contexturl값이 존재하면 url 출력해주는 함수 호출해줘야됨.
        $messagetext .= message_format_contexturl($message);

        if (! empty($keyword) && $keyfield == 'message') {
            $messagetext = highlight($keyword, $messagetext);
        }

        $ctx = new \stdClass();
        $ctx->id = $message->id;
        $ctx->time = $time;
        $ctx->timecreated = $message->timecreated;
        $ctx->twitterTime = $twitterTime;
        $ctx->message = nl2br($messagetext);
        $ctx->isGroup = $isGroup;

        $class = $side;
        $isNotification = $message->notification ?? 0;
        if ($isNotification) {
            $class .= ' notification';
        }
        $ctx->class = $class;

        // 메시지 관리 버튼
        $isSendCancel = (! isset($message->timeread) && ($USER->id == $message->useridfrom)) ? true : false;

        // 그룹 메시지가 아니라면 부가 정보가 더 표시되어야 함.
        $timeReadText = null;
        $buttonHtml = '';

        // isGroup인 경우에는 $message값이 local_ubsend_message 테이블이 아닌 local_ubsend_message_users값이 전달됩니다.
        if (! $isGroup) {
            if (! empty($message->timeread)) {
                $timeread = Common::getUserDate($message->timeread);

                if ($message->useridfrom == $USER->id) {
                    $timeReadText = get_string('readtime_opponent', $this->pluginname, $timeread);
                } else {
                    $timeReadText = get_string('readtime_my', $this->pluginname, $timeread);
                }
            } else {
                $timeReadText = '<span class="notread">' . get_string('no_read', $this->pluginname) . '</span>';
            }

            // 전송 취소소
            if ($isSendCancel) {
                $buttonHtml .= '<button type="button" class="btn btn-xs btn-danger btn-send-cancel mr-1" data-id="' . $message->messageid . '">';
                $buttonHtml .= '<i class="fa fa-remove"></i>';
                $buttonHtml .= '</button>';
            }

            // 삭제제
            $buttonHtml .= '<button type="button" class="btn btn-xs btn-danger btn-delete" data-messageuserid="' . $message->id . '">';
            $buttonHtml .= '<i class="fa fa-trash"></i>';
            $buttonHtml .= '</button>';
        }
        $ctx->timeReadText = $timeReadText;
        $ctx->buttonHtml = $buttonHtml;

        return $OUTPUT->render_from_template('local_ubsend/template-message', $ctx);
    }

    public function doSendCancel()
    {
        $messageid = Parameter::post('id', null, PARAM_INT);
        $touserid = Parameter::post('touserid', null, PARAM_INT);

        $validate = $this->validate()->make(array(
            'messageid' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('messageid', $this->pluginname),
                $this->validate()::PARAMVALUE => $messageid
            ),
            'touserid' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('to', $this->pluginname),
                $this->validate()::PARAMVALUE => $messageid
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            list ($code, $message) = $this->setSendCancel($messageid, $touserid);

            $isSuccess = ($code == Javascript::getSuccessCode()) ? true : false;
            Javascript::printAlert($message, $isSuccess);
        }
    }

    /**
     * 단일 메시지 전송 취소
     *
     * @param int $messageid
     * @param int $touserid
     * @return array
     */
    public function setSendCancel($messageid, $touserid)
    {
        global $DB, $USER;

        $query = "SELECT
								umu.*,
                                um.group_count
					FROM		{local_ubsend_message} um
					JOIN		{local_ubsend_message_users} umu ON um.id = umu.messageid
					WHERE		um.id = :id
					AND			umu.useridto = :useridto";

        $param = [
            'id' => $messageid,
            'useridto' => $touserid
        ];

        // 그룹 메시지, 일반 메시지에 따라서 전송 취소 로직이 조금씩 달라짐.
        if ($message = $DB->get_record_sql($query, $param)) {

            // 삭제하려는 대상이 본인인지 확인.
            if (is_siteadmin() || $USER->id == $message->useridfrom) {
                // 상대방이 메시지를 읽었는지 확인해봐야됨.
                if (empty($message->timeread)) {

                    // messageid로 등록되어 있는 메시지 갯수
                    $count = $DB->get_field_sql("SELECT COUNT(1) FROM {local_ubsend_message_users} WHERE messageid = :messageid", array(
                        'messageid' => $messageid
                    ));

                    $transaction = $DB->start_delegated_transaction();
                    // 전송취소하려는 메시지가 단일사용한테만 보냈거나,
                    // 다른 사용자들도 이전에 전송 취소한 경우 local_ubsend_message자체도 삭제
                    if ($count == 1) {
                        $DB->delete_records('local_ubsend_message', array(
                            'id' => $messageid
                        ));
                    } else {
                        // 여러사람에게 메시지가 전송이 되었기 때문에에
                        // touser에게만 메시지 전송 취소하고, local_ubsend_message 테이블에 group_count값을 -1해줘야됨.
                        // $DB->delete_records('local_ubsend_message_users', array('messageid'=>$messageid, 'useridto'=>$touser));

                        $updateMessage = new \stdClass();
                        $updateMessage->id = $messageid;
                        $updateMessage->group_count = $message->group_count - 1;
                        $DB->update_record('local_ubsend_message', $updateMessage);
                    }

                    // 전송 취소(삭제) 해야됨.
                    $DB->delete_records('local_ubsend_message_users', array(
                        'messageid' => $messageid,
                        'useridto' => $touserid
                    ));

                    // 이벤트 실행
                    $this->setEventCancel($message->messageid, $message->id);

                    $transaction->allow_commit();

                    $code = Javascript::getSuccessCode();
                    $message = get_string('message_cancel', $this->pluginname);
                } else {
                    $code = Javascript::getFailureCode();
                    $message = get_string('message_not_cancel_read', $this->pluginname);
                }
            } else {
                $code = Javascript::getFailureCode();
                $message = get_string('no_permissions', 'local_ubion');
            }
        } else {
            $code = Javascript::getFailureCode();
            $message = get_string('notfound_message', $this->pluginname);
        }

        return array(
            $code,
            $message
        );
    }

    public function doSendAllCancel()
    {
        global $CFG;

        $messageid = Parameter::post('id', null, PARAM_INT);

        $validate = $this->validate()->make(array(
            'messageid' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('messageid', $this->pluginname),
                $this->validate()::PARAMVALUE => $messageid
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            list ($code, $message) = $this->setSendAllCancel($messageid);

            $isSuccess = ($code == Javascript::getSuccessCode()) ? true : false;
            Javascript::printAlertMove($message, $CFG->wwwroot . '/local/ubsend/message/index.php', $isSuccess);
        }
    }

    public function setSendAllCancel($messageid)
    {
        global $DB, $USER;

        $query = "SELECT
								umu.*,
                                um.group_count
					FROM		{local_ubsend_message} um
					JOIN		{local_ubsend_message_users} umu ON um.id = umu.messageid
					WHERE		um.id = :id";

        $param = [
            'id' => $messageid
        ];

        // getGroupMessageUsers 함수를 사용해도 되는데.. 직접 조회하는게 좀 더 빠를듯
        if ($messages = $DB->get_records_sql($query, $param)) {
            $message = current($messages);

            // 삭제하려는 대상이 본인인지 확인. 그룹 메시지중 아무거나 한개만 추출해서 보낸 사람 비교
            if (is_siteadmin() || $USER->id == $message->useridfrom) {

                $transaction = $DB->start_delegated_transaction();

                // 그룹 메시지를 반복 돌면서 전송 취소 해야됨.
                foreach ($messages as $message) {
                    // 읽지 않은 사용자만 삭제
                    if (empty($message->timeread)) {
                        $DB->delete_records('local_ubsend_message_users', array(
                            'messageid' => $messageid,
                            'useridto' => $message->useridto
                        ));

                        unset($messages[$message->id]);

                        $this->setEventCancel($messageid, $message->id);
                    }
                }

                // $messages값이 빈값이면, 읽은 사용자가 없는 상태임
                // 즉 local_ubsend_message도 삭제 해야됨.
                if (empty($messages)) {
                    $DB->delete_records('local_ubsend_message', array(
                        'id' => $messageid
                    ));
                } else {
                    // 존재하는 사용자수
                    $count = count($messages);

                    $updateMessage = new \stdClass();
                    $updateMessage->id = $messageid;
                    $updateMessage->group_count = $count;
                    $DB->update_record('local_ubsend_message', $updateMessage);
                }

                $transaction->allow_commit();

                $code = Javascript::getSuccessCode();
                $message = get_string('message_cancel', $this->pluginname);
            } else {
                $code = Javascript::getFailureCode();
                $message = get_string('no_permissions', 'local_ubion');
            }
        } else {
            $code = Javascript::getFailureCode();
            $message = get_string('notfound_message', $this->pluginname);
        }

        return array(
            $code,
            $message
        );
    }

    /**
     * 메시지 전송 취소 이벤트
     *
     * @param number $messageid
     * @param number $messageuserid
     */
    function setEventCancel($messageid, $messageuserid)
    {
        global $USER;

        // Trigger event for reading a message.
        // 메시지 내용은 logstore_standard_log 테이블 용량 커지는 문제로 포함시키지 않습니다.
        // 만약 필요하다면, objectid를 사용해서 local_ubsend_message 테이블에 조회하시기 바랍니다.
        $event = \local_ubsend\event\message_canceled::create(array(
            'objectid' => $messageuserid,
            'userid' => $USER->id, // Using the user who read the message as they are the ones performing the action.
            'context' => \context_user::instance($USER->id),
            'other' => array(
                'messageid' => $messageid
            )
        ));

        $event->trigger();
    }

    public function getTargetInfo($touserid)
    {
        global $DB;

        return $DB->get_record_sql("SELECT * FROM {user} WHERE id = :id AND deleted = 0", array(
            'id' => $touserid
        ));
    }

    /**
     * 사용자간 메시지 기록
     *
     * @param int $targetid
     * @param int $page
     * @param int $ls
     * @return array[]|mixed[]|boolean[]
     */
    public function getUserMessages($targetid, $page = 1)
    {
        global $USER, $DB;

        $ls = $this->getUserMessageLS();

        $replace = self::REPLACE;

        // 본인이 user에게 보낸 메시지이면서 그룹메시지가 아닌 경우 (내가 그룹 메시지로 보낸 메시지는 일반 메시지 읽기에서 표시되면 안됨 => 그룹메시지(gmessge)에서 표시되어야 함.)
        // user가 본인에게 보낸 메시지
        $query = "SELECT
							" . $replace . "
				FROM		{local_ubsend_message} um
				JOIN		{local_ubsend_message_users} umu ON um.id = umu.messageid
                WHERE       (
							     (
                                    umu.useridfrom = :user1_1
                                    AND umu.useridto = :user2_1
                                    AND umu.group_key = :group_key
                                    AND visible_from = :visible_from
                                 )
							     OR
							     (
                                    umu.useridfrom = :user2_2
                                    AND umu.useridto = :user1_2
                                    AND visible_to = :visible_to
                                )
				            )";

        $param = [
            'user1_1' => $USER->id,
            'user1_2' => $USER->id,
            'user2_1' => $targetid,
            'user2_2' => $targetid,
            'group_key' => 0,
            'visible_from' => 0,
            'visible_to' => 0,
            'group_key' => 0
        ];

        $totalQuery = str_replace($replace, 'COUNT(1)', $query);
        $totalCount = $DB->get_field_sql($totalQuery, $param);

        $lists = array();
        if ($totalCount > 0) {

            $field = "umu.*,
                      um.message,
                      um.notification,
                      um.timecreated,
                      um.contexturl";

            $order = " ORDER BY um.timecreated DESC";

            $limitfrom = $ls * ($page - 1);

            $query = str_replace($replace, $field, $query);

            $lists = $DB->get_records_sql($query . $order, $param, $limitfrom, $ls);

            // 최근글을 조회하기 위해서 desc했는데, 화면상에서는 가장 마지막글이 하단에 위치해야되기 때문에에
            // 배열을 역순으로 정렬시켜야됨.
            $lists = array_reverse($lists);
        }

        return array(
            $totalCount,
            $lists
        );
    }

    /**
     * 사용자간 메시지 내용 페이지에서 표시할 기본 메시지 갯수
     *
     * @return number
     */
    public function getUserMessageLS()
    {
        return Parameter::getDefaultListSize();
    }

    public function doSend()
    {
        global $CFG, $USER;

        $to = Parameter::post('to', null, PARAM_INT);
        $message = Parameter::post('message', null, PARAM_NOTAGS);

        $validate = $this->validate()->make(array(
            'to' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('to', $this->pluginname),
                $this->validate()::PARAMVALUE => $to
            ),
            'message' => array(
                'required',
                $this->validate()::PLACEHOLDER => get_string('message', $this->pluginname),
                $this->validate()::PARAMVALUE => $message
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $uniqueUsers = array(
                $to
            );

            if ($this->setSend($USER->id, $uniqueUsers, $message, time())) {
                redirect($CFG->wwwroot . '/local/ubsend/message/message.php?id=' . $to);
            }
        }
    }

    public function doDelete()
    {
        $messageuserid = Parameter::post('messageuserid', null, PARAM_INT);

        $validate = $this->validate()->make(array(
            'messageuserid' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('messageuserid', $this->pluginname),
                $this->validate()::PARAMVALUE => $messageuserid
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            list ($code, $message) = $this->setDelete($messageuserid);

            $isSuccess = ($code == Javascript::getSuccessCode()) ? true : false;
            Javascript::printAlert($message, $isSuccess);
        }
    }

    public function setDelete($messageuserid)
    {
        global $DB, $USER;

        $query = "SELECT * FROM {local_ubsend_message_users} WHERE id = :id";
        $param = [
            'id' => $messageuserid
        ];

        // 실제 메시지가 존재하는지 확인인
        if ($message = $DB->get_record_sql($query, $param)) {

            // 삭제하려는 대상이 본인인지 확인.
            if ($USER->id == $message->useridfrom || $USER->id == $message->useridto) {

                if ($USER->id == $message->useridfrom) {
                    $field = 'visible_from';
                } else {
                    $field = 'visible_to';
                }

                $transaction = $DB->start_delegated_transaction();
                $update = new \stdClass();
                $update->id = $message->id;
                $update->$field = 1;

                $DB->update_record('local_ubsend_message_users', $update);

                // 숨김(삭제) 이벤트 실행행
                $this->setEventVisible($messageuserid, $field);
                $transaction->allow_commit();

                return array(
                    Javascript::getSuccessCode(),
                    get_string('message_deleted', $this->pluginname)
                );
            } else {
                return array(
                    Javascript::getFailureCode(),
                    get_string('sent_or_from', $this->pluginname)
                );
            }
        } else {
            return array(
                Javascript::getFailureCode(),
                get_string('notfound_message', $this->pluginname)
            );
        }
    }

    /**
     * 메시지 숨김(삭제) 이벤트
     *
     * @param number $messageuserid
     * @param string $type
     */
    function setEventVisible($messageuserid, $type)
    {
        global $USER;

        // Trigger event for reading a message.
        // 메시지 내용은 logstore_standard_log 테이블 용량 커지는 문제로 포함시키지 않습니다.
        // 만약 필요하다면, objectid를 사용해서 local_ubsend_message 테이블에 조회하시기 바랍니다.
        $event = \local_ubsend\event\message_visibled::create(array(
            'objectid' => $messageuserid,
            'userid' => $USER->id, // Using the user who read the message as they are the ones performing the action.
            'context' => \context_user::instance($USER->id),
            'other' => array(
                'send_type' => $type
            )
        ));
        $event->trigger();
    }

    public function doDayDelete()
    {
        $userid = Parameter::post('userid', null, PARAM_INT);
        $day = Parameter::post('day', null, PARAM_INT);

        $validate = $this->validate()->make(array(
            'userid' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('userid', 'local_ubion'),
                $this->validate()::PARAMVALUE => $userid
            ),
            'day' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('day'),
                $this->validate()::PARAMVALUE => $day
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $this->setDayDelete($userid, $day);

            $isSuccess = true;
            $message = get_string('delete_complete', 'local_ubion');
            Javascript::printAlert($message, $isSuccess);
        }
    }

    public function setDayDelete($userid, $day)
    {
        global $DB, $USER;

        // 보낸 메시지 숨김 처리리
        // 단, 본인이 보낸 그룹메시지는 별도로 삭제 해야됨.
        $query = "SELECT
                            *
                  FROM      {local_ubsend_message_users}
                  WHERE     useridfrom = :useridfrom
                  AND       useridto = :useridto
                  AND       days = :days
                  AND       group_key = :group_key";

        $param = [
            'useridfrom' => $USER->id,
            'useridto' => $userid,
            'days' => $day,
            'group_key' => 0
        ];

        if ($messages = $DB->get_records_sql($query, $param)) {

            $field = 'visible_from';

            $transaction = $DB->start_delegated_transaction();
            foreach ($messages as $message) {
                $update = new \stdClass();
                $update->id = $message->id;
                $update->$field = 1;

                $DB->update_record('local_ubsend_message_users', $update);

                // 숨김(삭제) 이벤트 실행행
                $this->setEventVisible($message->id, $field);
            }
            $transaction->allow_commit();
        }

        // 받은 메시지 숨김 처리
        // 받은 메시지는 그룹메시지 여부가 없음음
        $query = "SELECT
                            *
                  FROM      {local_ubsend_message_users}
                  WHERE     useridfrom = :useridfrom
                  AND       useridto = :useridto
                  AND       days = :days";

        $param = [
            'useridfrom' => $userid,
            'useridto' => $USER->id,
            'days' => $day
        ];

        if ($messages = $DB->get_records_sql($query, $param)) {

            $field = 'visible_to';

            $transaction = $DB->start_delegated_transaction();
            foreach ($messages as $message) {
                $update = new \stdClass();
                $update->id = $message->id;
                $update->$field = 1;

                $DB->update_record('local_ubsend_message_users', $update);

                // 숨김(삭제) 이벤트 실행
                $this->setEventVisible($message->id, $field);
            }
            $transaction->allow_commit();
        }
    }

    public function doDeleteAll()
    {
        $id = Parameter::post('id', null, PARAM_INT);
        $mtype = Parameter::post('mtype', null, PARAM_ALPHANUMEXT);

        $validate = $this->validate()->make(array(
            'userid' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => 'id',
                $this->validate()::PARAMVALUE => $id
            ),
            'mtype' => array(
                'required',
                $this->validate()::PLACEHOLDER => 'mtype',
                $this->validate()::PARAMVALUE => $mtype
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            // 그룹 메시지 또는 일반 사용자간의 메시지 여부에 따라 로직이 달라짐짐
            // id값은
            // 그룹이면 messageid
            // 사용자이면 userid
            // 값이 전달됨
            if ($mtype == 'group') {
                $this->setDeleteGroupAll($id);
            } else {
                $this->setDeleteAll($id);
            }

            $isSuccess = true;
            $message = get_string('delete_complete', 'local_ubion');
            Javascript::printAlert($message, $isSuccess);
        }
    }

    /**
     * 사용자별 메시지 모두 삭제
     *
     * @param number $userid
     *            : 상대방 userid
     * @param number $day
     *            : 일자
     */
    function setDeleteAll($userid)
    {
        global $DB, $USER;

        // 보낸 메시지 숨김 처리
        // 단, 본인이 보낸 그룹메시지는 별도로 삭제 해야됨.
        $query = "SELECT
                            *
                  FROM      {local_ubsend_message_users}
                  WHERE     useridfrom = :useridfrom
                  AND       useridto = :useridto
                  AND       group_key = :group_key";

        $param = [
            'useridfrom' => $USER->id,
            'useridto' => $userid,
            'group_key' => 0
        ];

        if ($messages = $DB->get_records_sql($query, $param)) {
            $field = 'visible_from';

            $transaction = $DB->start_delegated_transaction();
            foreach ($messages as $message) {
                $update = new \stdClass();
                $update->id = $message->id;
                $update->$field = 1;

                $DB->update_record('local_ubsend_message_users', $update);

                // 숨김(삭제) 이벤트 실행
                $this->setEventVisible($message->id, $field);
            }
            $transaction->allow_commit();
        }

        // 받은 메시지 숨김 처리
        $query = "SELECT
                            *
                  FROM      {local_ubsend_message_users}
                  WHERE     useridfrom = :useridfrom
                  AND       useridto = :useridto";

        $param = [
            'useridfrom' => $userid,
            'useridto' => $USER->id
        ];

        if ($messages = $DB->get_records_sql($query, $param)) {
            $field = 'visible_to';

            $transaction = $DB->start_delegated_transaction();
            foreach ($messages as $message) {
                $update = new \stdClass();
                $update->id = $message->id;
                $update->$field = 1;

                $DB->update_record('local_ubsend_message_users', $update);

                // 숨김(삭제) 이벤트 실행
                $this->setEventVisible($message->id, $field);
            }
            $transaction->allow_commit();
        }
    }

    /**
     * 그룹 메시지 삭제
     *
     * @param int $messageid
     *            : local_ubsend_message id
     */
    function setDeleteGroupAll($messageid)
    {
        global $DB, $USER;

        $query = "SELECT
							umu.*
				FROM		{local_ubsend_message} um
				JOIN		{local_ubsend_message_users} umu ON um.id = umu.messageid
				WHERE		um.id = :messageid";

        if ($messages = $DB->get_records_sql($query, array(
            'messageid' => $messageid
        ))) {

            // 임의의 메시지 한개를 선택해서 본인이 보낸 메시지인지 확인인
            $message = current($messages);

            if ($message->useridfrom == $USER->id) {
                // 그룹 메시지 전체 삭제는 본인이 보낸 메시지만 삭제 가능함.
                $field = 'visible_from';

                $transaction = $DB->start_delegated_transaction();
                foreach ($messages as $message) {
                    $update = new \stdClass();
                    $update->id = $message->id;
                    $update->$field = 1;

                    // 그룹메시지인데, 본인을 포함해서 전달하는 경우가 있음
                    // useridto가 본인과 동일하다면 visible_to도 1로 바꿔야됨.
                    if ($message->useridto == $USER->id) {
                        $update->visible_to = 1;
                    }

                    $DB->update_record('local_ubsend_message_users', $update);

                    // 숨김(삭제) 이벤트 실행
                    $this->setEventVisible($message->id, $field);
                }
                $transaction->allow_commit();

                return array(
                    Javascript::getSuccessCode(),
                    get_string('message_deleted', $this->pluginname)
                );
            } else {
                return array(
                    Javascript::getFailureCode(),
                    get_string('group_message_delete', $this->pluginname)
                );
            }
        }
    }

    public function doMessages()
    {
        global $PAGE, $USER;

        $PAGE->set_context(\context_user::instance($USER->id));

        $userid = Parameter::post('userid', null, PARAM_INT);
        $page = Parameter::post('page', 1, PARAM_INT);
        $ls = Parameter::post('ls', Parameter::getDefaultListSize(), PARAM_INT);

        $validate = $this->validate()->make(array(
            'userid' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('userid', 'local_ubion'),
                $this->validate()::PARAMVALUE => $userid
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {

            list ($totalCount, $messages) = $this->getUserMessages($userid, $page);

            $html = $this->getMessageListHtml($messages);

            $isLastPage = false;

            // 전달된 page까지 가져온 게시물이 전체 갯수보다 크다면 마지막 페이지가 아니므로 더이상 페이징 처리가 되면 안됨
            $currentNum = $page * $ls;
            if ($totalCount <= $currentNum) {
                $isLastPage = true;
            }

            if (Parameter::isAajx()) {
                return Javascript::printJSON(array(
                    'code' => Javascript::getSuccessCode(),
                    'msg' => get_string('view_complete', 'local_ubion'),
                    'html' => $html,
                    'isLastPage' => $isLastPage
                ));
            } else {
                return $html;
            }
        }
    }

    /**
     * 연락처 목록
     *
     * @param string $keyword
     * @param int $page
     * @param int $ls
     * @return array
     */
    public function getMyContacts($keyword = null, $page = 1, $ls = 5)
    {
        global $DB, $USER;

        $replace = self::REPLACE;
        $query = "SELECT
                            $replace
                  FROM 		{message_contacts} mc
    			  JOIN 		{user} u ON u.id = mc.contactid
    			  WHERE 	mc.userid = :userid";

        $param = array(
            'userid' => $USER->id
        );

        // 검색관련 내용이 존재하는 경우
        if (! empty($keyword)) {
            $fullname = $DB->sql_fullname();
            $query .= " AND " . $DB->sql_like($fullname, ':keyword');

            $param['keyword'] = '%' . $keyword . '%';
        }

        $totalQuery = str_replace($replace, "COUNT(1)", $query);

        // 전체 갯수
        $totalCount = $DB->get_field_sql($totalQuery, $param);

        $userfields = \user_picture::fields('u', array(
            'idnumber',
            'institution',
            'department'
        ));

        $query = str_replace($replace, $userfields, $query);
        $orderby = "u.firstname";

        if (current_language() != 'ko') {
            $orderby = 'u.lastname';
        }

        $query .= ' ORDER BY ' . $orderby;

        $limitfrom = $ls * ($page - 1);

        // error_log($query);
        // error_log(print_r($param, true));
        $list = $DB->get_records_sql($query, $param, $limitfrom, $ls);

        return array(
            $totalCount,
            $list
        );
    }

    /**
     * 연락처 table에 표시될 갯수
     *
     * @return number
     */
    public function getMyContactDefaultListCount()
    {
        return 5;
    }

    public function doMyContacts()
    {
        global $CFG, $OUTPUT, $PAGE, $USER;

        $PAGE->set_context(\context_user::instance($USER->id));

        $CUser = \local_ubion\user\User::getInstance();

        $page = Parameter::post('page', 1, PARAM_INT);
        // $courseid = Parameter::post('courseid', null, PARAM_INT);
        $keyword = Parameter::getKeyword();
        $ls = $this->getMyContactDefaultListCount();

        list ($totalCount, $contacts) = $this->getMyContacts($keyword, $page, $ls);

        $html = '';
        if ($totalCount > 0) {
            $number = ($page - 1) * $ls;

            $baseurl = $CFG->wwwroot . '/local/ubsend/message/message.php?id=';
            foreach ($contacts as $cs) {
                $number ++;

                $cs->number = $number;
                $cs->userpicture = $CUser->getPicture($cs);
                $cs->fullname = fullname($cs);
                $cs->department = $CUser->getDepartment($cs);
                $cs->messageurl = $baseurl . $cs->id;
                $cs->idnumber = $CUser->getIdnumber($cs->idnumber);
            }

            $contacts = array_values($contacts);
        }

        $paging = Paging::getPaging($totalCount, $page, $ls, 5);

        $ctx = new \stdClass();
        $ctx->totalCount = $totalCount;
        $ctx->contacts = $contacts;
        $ctx->paging = Paging::getHTML($paging);
        $ctx->keyword = $keyword;

        $html = $OUTPUT->render_from_template('local_ubsend/mycontacts', $ctx);

        if (Parameter::isAajx()) {
            return Javascript::printJSON(array(
                'code' => Javascript::getSuccessCode(),
                'msg' => get_string('view_complete', 'local_ubion'),
                'html' => $html
            ));
        } else {
            return $html;
        }
    }

    /**
     * 연락처에 추가된 사용자인지 확인
     *
     * @param int $userid
     * @return mixed|boolean
     */
    public function isContactUser($userid)
    {
        global $DB, $USER;

        // 코스모스에서는 blocked를 사용하지 않으므로 쿼리문에서 제거함.
        $query = "SELECT
                            COUNT(1)
                  FROM      {message_contacts}
                  WHERE     userid = :userid
                  AND       contactid = :contactid";

        $param = [
            'userid' => $USER->id,
            'contactid' => $userid
        ];

        return $DB->get_field_sql($query, $param);
    }

    public function doContact()
    {
        $userid = Parameter::post('userid', null, PARAM_INT);
        $type = Parameter::post('type', 'add', PARAM_ALPHANUMEXT);

        $validate = $this->validate()->make(array(
            'userid' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => get_string('userid', 'local_ubion'),
                $this->validate()::PARAMVALUE => $userid
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $isAdd = true;
            if ($type == 'delete') {
                $isAdd = false;
            }

            $this->setContact($userid, $isAdd);

            Javascript::printAlert(get_string('save_complete', 'local_ubion'), true);
        }
    }

    /**
     * 연락처 추가
     *
     * @param int $userid
     * @param boolean $isAdd
     */
    public function setContact($userid, $isAdd = true)
    {
        global $CFG;

        require_once $CFG->dirroot . '/message/lib.php';

        if ($isAdd) {
            message_add_contact($userid);
        } else {
            message_remove_contact($userid);
        }
    }

    /**
     * 모든 메시지 읽기
     */
    public function setAllRead()
    {
        global $DB, $USER;

        $query = 'SELECT
                            COUNT(1)
                  FROM      {local_ubsend_message_users}
                  WHERE     useridto = :to
                  AND       timeread IS NULL';

        $param = [
            'to' => $USER->id
        ];

        $count = $DB->get_field_sql($query, $param);

        if ($count > 0) {

            // timeread 일괄로 업데이트
            $query = "UPDATE {local_ubsend_message_users} SET timeread = :timeread WHERE useridto = :to AND timeread IS NULL";

            $DB->execute($query, array(
                'to' => $USER->id,
                'timeread' => time()
            ));

            $event = \local_ubsend\event\message_read::create(array(
                'objectid' => 0,
                'userid' => $USER->id, // Using the user who read the message as they are the ones performing the action.
                'context' => \context_user::instance($USER->id),
                'other' => array(
                    'fromuser' => 'ALL',
                    'group_key' => 0
                )
            ));
            $event->trigger();
        }
    }

    public function pushParameter($message, $userids, $isGroupMessage)
    {

        // 사용자 사진 정보 추출
        $usercontext = \context_user::instance($message->userid);
        $profileimageurl = \moodle_url::make_pluginfile_url($usercontext->id, 'user', 'icon', NULL, '/', 'f1');
        $profileimageurlsmall = \moodle_url::make_pluginfile_url($usercontext->id, 'user', 'icon', NULL, '/', 'f2');

        $subject = 'New Message';

        // 내용은 json_encode해서 보내줘야됨.
        $content = array();
        $content['id'] = $message->userid;
        $content['idnumber'] = $message->idnumber;
        $content['firstname'] = $message->firstname;
        $content['lastname'] = $message->lastname;
        $content['profileimageurl'] = $profileimageurl->out(false);
        $content['profileimageurlsmall'] = $profileimageurlsmall->out(false);

        // 개인간의 메시지인 경우에는 userids가 곧 useridto가 되지만,
        // 그룹메시지는 ,로 구분되서 넘어옴.
        // 그리고 그룹 메시지 같은 경우에는 touserid를 messageid값으로 전달해주기로 함.
        $content['touserid'] = $userids;
        // json 형태라서 is_groupmessage값인 boolean형태로 출력되서 0, 1형태로 변경함.
        $content['is_groupmessage'] = ($isGroupMessage) ? 1 : 0;
        if ($isGroupMessage) {
            $content['touserid'] = $message->id;
        }

        // 메시지 내용은 50자로 줄여서 전달.
        if (mb_strlen($message->message, 'UTF-8') > 50) {
            $content['message'] = mb_substr($message->message, 0, 50, 'UTF-8') . '...';
        } else {
            $content['message'] = $message->message;
        }

        $parameter = "type=" . $this->getPushMessageCode();
        $parameter .= "&userids=" . urlencode($userids) . "&subject=" . $subject; // ."&content=".json_encode($content);

        $jsonContent = json_encode($content);
        // push서버에서 content값이 빈값으로 전달되는 경우가 있다고 함.
        // 원인을 찾기 위해서 빈값인 경우 error로그를 남기도록 처리함.
        // 2016-10-27 by akddd
        if (empty($jsonContent)) {
            error_log('push content NULL');
            error_log(print_r($message, true));
            error_log(print_r($jsonContent, true));
            return null;
        } else {
            $parameter .= "&content=" . $jsonContent;

            return $parameter;
        }
    }

    public function cron()
    {
        global $DB;

        // 1. 메시지 삭제
        $period = $this->getDeletePeriod();

        if (! empty($period)) {
            mtrace('Message delete period cron start');

            $date = new \DateTime(date('Y-m-d'));
            $date->sub(new \DateInterval('P' . $period . 'M'));

            $oldYmd = $date->format('Ymd');

            // 1년전 날자 메시지는 삭제
            // 보낸시간 기준이 아닌 보낸 날자 기준으로 삭제함.
            // 단점이 23시 59분에 보낸것도 1분후에 무조건 1일로 계산되는 단점이 있지만, 큰 문제는 없을거라고 판단이 됨.
            $query = "SELECT id FROM {local_ubsend_message} WHERE days <= :days";
            $param = [
                'days' => $oldYmd
            ];

            if ($messages = $DB->get_records_sql($query, $param)) {

                foreach ($messages as $msg) {
                    // local_ubsend_message_users 삭제
                    $DB->execute('DELETE FROM {local_ubsend_message_users} WHERE messageid = :messageid', array(
                        'messageid' => $msg->id
                    ));

                    // ubmessage 삭제
                    $DB->execute('DELETE FROM {local_ubsend_message} WHERE id = :id', array(
                        'id' => $msg->id
                    ));
                }

                $count = count($messages);
                mtrace('Message Delete Message count ' . $count);
            }
            mtrace('Message delete period cron end');
        }

        // 2. 전달되지 않은 푸시 전송
        // 디비 부하를 줄이기 위해서.. 최근 1일 전부터.. push.. 가 0인것만 가져옴.
        $datetime = new \DateTime();
        $datetime->sub(new \DateInterval($this->getNotSendPushPeriod()));

        $days = $datetime->format('Ymd');

        $query = "SELECT * FROM {local_ubsend_message} WHERE days >= :days AND push = 0";
        $param = [
            'days' => $days
        ];

        if ($messages = $DB->get_records_sql($query, $param)) {
            mtrace('Message push cron start');

            // 해당 메시지에 존재하는 사용자 가져오기
            // groupcount와 상관 없이...일단 사용자 다가져오고... count를 별도로 해서 1보다 크면 그룹메시지로 판단하는게 좋을듯 함.
            $query = "SELECT
								useridto, useridfrom
				      FROM		{local_ubsend_message_users}
					  WHERE		messageid = :messageid";

            $userQueryField = \user_picture::fields('', array(
                'idnumber'
            ));
            $userQuery = "SELECT " . $userQueryField . " FROM {user} WHERE id = :id AND deleted = 0";
            $userParam = [
                'id' => ''
            ];

            foreach ($messages as $ms) {
                $param = [
                    'messageid' => $ms->id
                ];

                if ($users = $DB->get_records_sql($query, $param)) {

                    // groupuserids : 그룹 메시지에 본인이 포함되는 경우
                    // userids : 본인을 제외한 push를 전달받은 사용자
                    $groupuserids = $userids = $comma = null;

                    foreach ($users as $u) {
                        // 그룹 메시지인데, 본인이 포함되어 있으면, 푸시 전달 파라메터가 달라져야됨.
                        // 단일 메시지를 본인한테도 보낼수도 있기 때문에 그룹 메시지 여부를 조건문으로 추가해놔야됨.
                        if ($u->useridto == $ms->useridfrom && $ms->group_count > 0) {
                            $groupuserids = $ms->useridfrom;
                        } else {
                            $userids .= $comma . $u->useridto;
                            $comma = ',';
                        }
                    }

                    // 보내는 사용자 정보가 존재하는 경우 진행
                    $userParam['id'] = $ms->useridfrom;
                    if ($userInfo = $DB->get_record_sql($userQuery, $userParam)) {

                        // 메시지 전송과 동시에 푸시를 전달할 경우
                        // message에 전달하는 사용자 정보를 추가해줘야됨
                        $ms->userid = $userInfo->id;
                        $ms->idnumber = $userInfo->idnumber;
                        $ms->firstname = $userInfo->firstname;
                        $ms->lastname = $userInfo->lastname;

                        $isPushUpdate = false;

                        // 그룹 메시지인 경우 본인을 제외한
                        if (! empty($userids)) {
                            $parameter = $this->pushParameter($ms, $userids, false);
                            list ($isSuccess, $result) = $this->push($parameter);

                            // 푸시가 정상적으로 전송이 되었으면 push값을 1로 변경해줘야됨.
                            if ($isSuccess) {
                                $this->setPushComplete($ms->id);

                                $isPushUpdate = true;
                            }
                        }

                        // 그룹 메시지이고, 본인이 포함된 경우
                        if (! empty($groupuserids)) {

                            $parameter = $this->pushParameter($ms, $groupuserids, true);
                            list ($isSuccess, $result) = $this->push($parameter);

                            // 푸시가 정상적으로 전송이 되었으면 push값을 1로 변경해줘야됨.
                            // 그룹메시지를 받는 사용자한테만 제대로 전달이 되었다면, 본인한테 푸시가 전달되지 않아도 푸시 전송 완료로 판단해야됨.
                            // 그리고 사용자한테 전송이 완료가 되었으면 다시 push값을 1로 업데이트 할 필요가 없음.
                            if (! $isPushUpdate && $isSuccess) {
                                $this->setPushComplete($ms->id);
                            }
                        }
                    }
                }
            }

            mtrace('Message push cron end');
        }
    }

    /**
     * 메시지 삭제 기간
     *
     * @return int
     */
    public function getDeletePeriod()
    {
        return get_config('local_ubsend', 'message_delete_period');
    }

    /**
     * 푸시가 전송되지 않는 메시지에 대해 재전송할수 있는 기간
     *
     * @return string
     */
    public function getNotSendPushPeriod()
    {
        // DateInterval 규칙에 맞는 값을 입력해주셔야 합니다.
        // http://php.net/manual/en/dateinterval.construct.php
        return 'P1D';
    }
}