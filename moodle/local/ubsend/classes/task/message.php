<?php
namespace local_ubsend\task;

defined('MOODLE_INTERNAL') || die();

class message extends \core\task\scheduled_task
{

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name()
    {
        return get_string('task_message', 'local_ubsend');
    }

    /**
     * Do the job.
     * Throw exceptions on errors (the job will be retried).
     */
    public function execute()
    {
        global $DB;

        // 오래된 메시지 삭제 및 푸시 전송

        $CMessage = \local_ubsend\message\Message::getInstance();
        $CMessage->cron();
    }
}
