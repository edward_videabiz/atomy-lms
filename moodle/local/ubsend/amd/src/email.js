define(['jquery', 'local_ubsend/form', 'theme_coursemos/jquery.validate'], function($, sendForm) {
	var email = {};
	
	email.send = function(editorname) {
		$(".local_ubsend .form-send").validate({
			ignore:null
			,submitHandler:function(form) {
				
				if($(".form-send input[name='tousers[]']:checked").length > 0){
					$(".form-send button[type='submit']").attr('disabled', 'disabled');
					
					showSubmitProgress();
					form.submit();
				} else {
					alert(M.util.get_string('min_user', 'local_ubion'));
					// location.hash = 'form-send';
					return false;
				}
			}
		});
		
		// validate 이 먼저 호출이 된 후 입력되어야 합니다.
		$("#edit-" + editorname).rules("add", "required");
		$("#edit-" + editorname).attr('placeholder', M.util.get_string('email_content', 'local_ubsend'));
	};
	
	return email;
});