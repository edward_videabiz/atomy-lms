define(['jquery', 'local_ubsend/form', 'theme_coursemos/jquery.validate'], function($, sendForm) {
	var sms = {};
	
	sms.hanglebytes = 2;	// 기본 2byte
	
	sms.send = function(maxbytes, isResend, hanglebytes) {
		sms.hanglebytes = parseInt(hanglebytes);
		
		// form validate
		$(".local_ubsend .form-send").validate({
			submitHandler : function(form) {
				if($(".form-send input[name='tousers[]']:checked").length > 0){
					
					tell = $(form.sms_from).val();
					var phoneExp = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
					var tellExp = /^(0(2|3[1-3]|4[1-4]|5[1-5]|6[1-4]|70))-?(\d{3,4})-?(\d{4})$/;

					if(!phoneExp.test(tell) && !tellExp.test(tell)) {
						alert(M.util.get_string('sms_invalid_phone_number', 'local_ubsend'));
						return false
					}

					$(".form-send button[type='submit']").attr('disabled', 'disabled');
					
					showSubmitProgress();
					form.submit();
				} else {
					alert(M.util.get_string('min_user', 'local_ubion'));
					return false;
				}
				
			}
		});
		
		
		// reset
		$(".form-send .btn-reset").click(function() {
			$(".form-send .form-control-sms").val("");
		});
		
		// 재선송인 경우 기존에 전달한 메시지의 byte를 구해서 입력해줘야됨.
		if(isResend) {
			total_byte = sms.checkbytes($(".form-send .form-control-sms"));
			$(".form-send .bytes .now").text(total_byte);
		}
		
		
		$(".form-send .form-control-sms").keyup(function() {
			bytes = sms.checkbytes($(this));
			$(".form-send .bytes .now").text(bytes);
			
			// 입력한 길이가 큰 경우
			if(bytes > maxbytes) {
				// alert(i8n.maxbyte);
				
				var tempStr = new String(message);
				var len = 0;
				for ( var i = 0; i < message.length; i++) {
					var c = escape(message.charAt(i));
					
					if (c.length == 1) {
						len++;
					} else if (c.indexOf("%u") != -1) {
						len += hanglebytes;
					} else if (c.indexOf("%") != -1) {
						len += c.length / 3;
					}
					
					if (len > maxbytes) {
						tempStr = tempStr.substring(0, i);
						break;
					}
				}
				
				$(this).val(tempStr);
				$(".form-send .form-control-sms").trigger('keyup');
			}
		});
		
		// 특수문자
		$(".form-send .btn-char").click(function() {
			
			var sms_message = $(".form-send .form-control-sms");
			sms_message = sms_message[0];
			var insert_text = $(this).text();
			
			if (document.selection) {
				sms_message.focus();
		        sel = document.selection.createRange();
		        sel.text = insert_text;
		        sms_message.focus();
		    } else if (sms_message.selectionStart || sms_message.selectionStart == '0') {
		    	 var startPos = sms_message.selectionStart;
		         var endPos = sms_message.selectionEnd;
		         var scrollTop = sms_message.scrollTop;
		         sms_message.value = sms_message.value.substring(0, startPos) + insert_text + sms_message.value.substring(endPos, sms_message.value.length);
		         sms_message.focus();
		         sms_message.selectionStart = startPos + insert_text.length;
		         sms_message.selectionEnd = startPos + insert_text.length;
		         sms_message.scrollTop = scrollTop;
		    } else {
		    	sms_message.value += insert_text;
		    	sms_message.focus();
			}
			
			$(".form-send .form-control-sms").trigger('keyup');
		});
		
	};
	
	sms.checkbytes = function(el) {
		total_byte = 0;
		
		message = $(el).val();
		for(var i =0; i < message.length; i++) {
			/*
        	var current_byte = message.charCodeAt(i);
        	if(current_byte > 128)  {
        		total_byte += 2;
        	} else { 
        		total_byte++;
        	}
        	*/
			c = escape(message.charAt(i));
			
			if (c.length == 1) {
				total_byte++;
			} else if (c.indexOf("%u") != -1) {
				total_byte += sms.hanglebytes;
			} else if (c.indexOf("%") != -1) {
				total_byte += c.length / 3;
			}
        }
		
		return total_byte;
	};
	
	return sms;
});