define(['jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate'], function($, bootstrap) {
	$(".local_ubsend .form-send .btn-group-checked .btn-group-tooltip").tooltip({container:'body'});
	
	$(".local_ubsend .form-send .btn-group-checked .btn-group-checked").click(function() {
		$(this).closest('fieldset').find("input[name='tousers[]']").not(':disabled').each(function(i) {
			this.checked = true;
		});
	});
	
	$(".local_ubsend .form-send .btn-group-checked .btn-group-unchecked").click(function() {
		$(this).closest('fieldset').find("input[name='tousers[]']").not(':disabled').each(function(i) {
			this.checked = false;
		});
	});
	
	
	$(".local_ubsend .form-send .btn-collapsed-expanded").click(function() {
		$(".local_ubsend .form-send").toggleClass('nomaxheight');
	});
	
	$(".form-search .select-autosubmit").change(function() {
		$(".form-search").submit();
	});

	$(".local_ubsend .form-search .btn-allchecked").click(function() {
		$(".form-send input[name='tousers[]']").not(':disabled').each(function(i) {
			this.checked = true;
		});
	});
		
	$(".local_ubsend .form-search .btn-unchecked").click(function() {
		$(".form-send input[name='tousers[]']").not(':disabled').each(function(i) {
			this.checked = false;
		});
	});
	
	/*
	$(".local_ubsend .form-send").validate({
		submitHandler:function(form) {
			console.log('form validate');
			if($(".form-send input[name='tousers[]']:checked").length > 0){
				$(".form-send button[type='submit']").attr('disabled', 'disabled');
				// showSubmitProgress();
				form.submit();
				return false;
			} else {
				alert(M.util.get_string('min_user', 'local_ubion'));
				// location.hash = 'form-send';
				return false;
			}
		}
	});
	*/
});