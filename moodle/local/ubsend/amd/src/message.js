define(['jquery', 'core/templates', 'local_ubsend/form', 'theme_coursemos/jquery.validate'], function($, templates, sendForm) {
    var message = {};
    
    message.page = 1;
    message.isPageLoad = false;
    message.isLastPage = false;
    
    message.index = function(action) {
        $(".local-ubsend-message .form-search .form-control-auto").change(function() {
            $(".local-ubsend-message .form-search").submit();
        });
        
        
        $(".local-ubsend-message .tools .btn-delete").click(function() {
            username = $(this).data('username');
            
            if (confirm(M.util.get_string('confirm_all_delete', 'local_ubsend', username))) {
                param = coursemostype + '=deleteAll&id=' + $(this).data('id') + '&mtype=' + $(this).data('mtype');
                
                $.ajax({
                    url : action,
                    data : param,
                    success : function (data) {
                        if (data.code == mesg.success) {
                            location.href = location.href;
                        } else {
                            alert(data.msg);
                        }

                        return true;
                    },
                    error : function (xhr, option, error) {
                        alert(xhr.status + ' : ' + error);
                    }
                });
            }
            
            return false;
        });
        
        
        $(".local-ubsend-message .btn-mycontacts").click(function() {
            var courseid = 0;
            if ($(".local-ubsend-message .form-search select[name='courseid']").length > 0) {
                courseid = $(".local-ubsend-message .form-search select[name='courseid']").val();    
            }
            
            // 페이지를 처음 로드하기 때문에 검색어는 빈값으로 전달
            var keyword = '';    
            message.mycontact(action, courseid, 1, keyword);
        });
    };
    
    
    message.mycontact = function(action, courseid, page, keyword) {
                
        param = coursemostype + '=myContacts&courseid=' + courseid + '&page=' + page + '&keyword=' + keyword;
        
        $.ajax({
            url : action,
            data : param,
            success : function (data) {
                if (data.code == mesg.success) {
                    $(".local-ubsend-message .modal-mycontacts .modal-body").html(data.html);
                    $(".local-ubsend-message .modal-mycontacts").modal('show');
                    
                    // submit 이벤트
                    $(".local-ubsend-message .modal-mycontacts .contacts form").submit(function() {
                        // 새로 keyword를 입력했을수도 있기 때문에 해당 el의 값을 전달해줘야됨.
                        message.mycontact(action, courseid, 1, $(".local-ubsend-message .modal-mycontacts .contacts input[name='keyword']").val());
                        
                        return false;
                    });
                    
                    
                    // 이벤트 추가해줘야됨.
                    $(".local-ubsend-message .modal-mycontacts .page-link").click(function() {
                        message.mycontact(action, courseid, $(this).data('pagenum'), keyword);
                        
                        return false;
                    });
                } else {
                    alert(data.msg);
                }

                return true;
            },
            error : function (xhr, option, error) {
                alert(xhr.status + ' : ' + error);
            }
        });
    }
    
    message.send = function(editorname) {
        $(".local_ubsend .form-send").validate({
            ignore:null
            ,submitHandler:function(form) {
                
                if($(".form-send input[name='tousers[]']:checked").length > 0){
                    $(".form-send button[type='submit']").attr('disabled', 'disabled');
                    
                    showSubmitProgress();
                    form.submit();
                } else {
                    alert(M.util.get_string('min_user', 'local_ubion'));
                    // location.hash = 'form-send';
                    return false;
                }
            }
        });
    };
    
    
    message.message = function(targetid, action, isLastPage) {
        
        $(".local-ubsend-message .form-validate").validate();
                
        // 스크롤을 페이지 하단으로 이동시켜줘야됨.
        // scrollto할 필요 없이 textarea에 포커스 위치 시키면됨
        $(".local-ubsend-message .form-control-message").focus();
        
        // 마지막 페이지 여부
        message.isLastPage = isLastPage;
        
        // heading 감싸주기
        message.viewRefresh(targetid, action);

        
        // scroll이 top에 위치할때 페이징 처리해줘야됨.
        var historyTop = $(".local-ubsend-message .message-history").offset().top;
        $(window).scroll(function() {
            // 페이징은 스크롤이 변경되는동안 여러번 호출되면 안됨
            if ($(window).scrollTop() < historyTop && !message.isPageLoad && !message.isLastPage) {

                message.scrollDisabled();
                
                
                message.isPageLoad = true;
                
                // 다음페이지 조회
                message.page++;
                param = coursemostype + '=messages&userid=' + targetid + '&page=' + message.page;
                
                $.ajax({
                    url : action,
                    data : param,
                    success : function (data) {
                        if (data.code == mesg.success) {
                            // firstchild
                            var firstELPosition = $(".local-ubsend-message .message-history .messages .message-fromto").first();
                            
                            $(".local-ubsend-message .message-history .messages").prepend(data.html);
                            
                            message.viewRefresh(targetid, action);
                            
                            $(document).scrollTop(firstELPosition.offset().top);

                            message.isPageLoad = false;
                            message.isLastPage = data.isLastPage;
                            
                            // 스크롤 잠금 해제
                            message.scrollDisabledUnset();
                        } else {
                            alert(data.msg);
                        }
                    },
                    error : function (xhr, option, error) {
                        alert(xhr.status + ' : ' + error);
                    }
                });    
            }
            
            return false;
        });
        
        
        $(".local-ubsend-message .btn-contact").click(function() {
            var param = coursemostype + '=contact&userid=' + targetid + '&type=' + $(this).attr('data-type');
            var messageType = $(this).attr('data-type');
            
            
            el = $(this);
            $.ajax({
                url : action,
                data : param,
                success : function (data) {
                    if (data.code == mesg.success) {
                        // 버튼을 달리 표시해줘야됨.
                        
                        if (messageType == 'add') {
                            el.attr('data-type', 'delete').removeClass('btn-default').addClass('btn-danger');
                            el.text(M.util.get_string('removecontact', 'message'))
                        } else {
                            el.attr('data-type', 'add').addClass('btn-default').removeClass('btn-danger');
                            el.text(M.util.get_string('addcontact', 'message'))
                        }
                        
                    } else {
                        alert(data.msg);
                    }
                     return true;
                },
                error : function (xhr, option, error) {
                    alert(xhr.status + ' : ' + error);
                }
            });
        });
    };
    
    message.scrollDisabled = function() {

        $(document).on("mousewheel.disableScroll DOMMouseScroll.disableScroll touchmove.disableScroll", function(e) {
            e.preventDefault();
            return;
        });
        
        $(document).on("keydown.disableScroll", function(e) {
            var eventKeyArray = [32, 33, 34, 35, 36, 37, 38, 39, 40];
            for (var i = 0; i < eventKeyArray.length; i++) {
                if (e.keyCode === eventKeyArray [i]) {
                    e.preventDefault();
                    return;
                }
            }
        });
  
    };
    
    
    message.scrollDisabledUnset = function() {
        $(document).off(".disableScroll");
    };
    
    message.viewRefresh = function(targetid, action) {
        
        $(".local-ubsend-message .message-history .heading").remove();
        
        var oldDay = '';
        $(".local-ubsend-message .message-history .message-fromto").each(function() {
            var el = $(this);
            var timecreated = $(this).data('timecreated');
            var date = new Date(timecreated * 1000);
            var day = date.getFullYear() + '' + date.getMonth() + '' + date.getDate();
            
            if (oldDay != day) {
                var context = { timecreated : timecreated};
                templates.render('local_ubsend/template-dayheading', context).then(function(html, js) {
                    
                    el.before(html);
                    
                    message.message_event(targetid, action);
                    
                }).fail(function(ex) {
                    // Deal with this exception (I recommend core/notify exception function for this).
                    // console.log(ex);
                });
                
                oldDay = day;
            }
            
        });
        
    };
    
    
    message.message_event = function(targetid, action) {
     // 이벤트 추가시켜줘야됨.
        $(".local-ubsend-message .btn-day-delete").unbind('click');
        $(".local-ubsend-message .btn-day-delete").click(function() {
            
            days = $(this).parent().find('.dayname').text();
            if (confirm(M.util.get_string('confirm_day_delete', 'local_ubsend', days))) {
                
                param = coursemostype + '=dayDelete&userid=' + targetid + '&day=' + $(this).data('day');
               
                el = $(this);
                $.ajax({
                    url : action,
                    data : param,
                    success : function (data) {
                        if (data.code == mesg.success) {
                            location.href = location.href;
                        } else {
                            alert(data.msg);
                        }
                         return true;
                    },
                    error : function (xhr, option, error) {
                        alert(xhr.status + ' : ' + error);
                    }
                });
            }
          
            return false;
        });
        
        
        $(".local-ubsend-message .btn-send-cancel").unbind('click');
        $(".local-ubsend-message .btn-send-cancel").click(function() {
            if (confirm(M.util.get_string('confirm_message_cancel', 'local_ubsend'))) {
                
                param = coursemostype + '=sendCancel&id=' + $(this).data('id') + '&touserid=' + targetid;
                
                el = $(this);
                $.ajax({
                    url : action,
                    data : param,
                    success : function (data) {
                        if (data.code == mesg.success) {
                            location.href = location.href;
                        } else {
                            alert(data.msg);
                        }

                        return true;
                    },
                    error : function (xhr, option, error) {
                        alert(xhr.status + ' : ' + error);
                    }
                });
            }
            
            return false;
        });
        
        
        $(".local-ubsend-message .btn-delete").unbind('click');
        $(".local-ubsend-message .btn-delete").click(function() {
            if (confirm(M.util.get_string('confirm_message_delete', 'local_ubsend'))) {
                param = coursemostype + '=delete&messageuserid=' + $(this).data('messageuserid');
                
                el = $(this);
                $.ajax({
                    url : action,
                    data : param,
                    success : function (data) {
                        if (data.code == mesg.success) {
                            location.href = location.href;
                        } else {
                            alert(data.msg);
                        }

                        return true;
                    },
                    error : function (xhr, option, error) {
                        alert(xhr.status + ' : ' + error);
                    }
                });
            }
            
            return false;
        });
    };
    
    message.gmessage = function(id, action) {
        
        $(".local-ubsend-message .btn-send-cancel").click(function() {
            if (confirm(M.util.get_string('confirm_message_cancel', 'local_ubsend'))) {
                
                param = coursemostype + '=sendCancel&id=' + id + '&touserid=' + $(this).data('touserid');
                
                el = $(this);
                $.ajax({
                    url : action,
                    data : param,
                    success : function (data) {
                        if (data.code == mesg.success) {
                            el.closest('.col-user').remove();
                        } else {
                            alert(data.msg);
                        }

                        return true;
                    },
                    error : function (xhr, option, error) {
                        alert(xhr.status + ' : ' + error);
                    }
                });
            }
            
            return false;
        });
        
        
        $(".local-ubsend-message .btn-send-all-cancel").click(function() {
            if (confirm(M.util.get_string('confirm_message_all_cancel', 'local_ubsend'))) {
                param = coursemostype + '=sendAllCancel&id=' + id;
                
                $.ajax({
                    url : action,
                    data : param,
                    success : function (data) {
                        if (data.code == mesg.success) {
                            location.href = data.href;
                        } else {
                            alert(data.msg);
                        }

                        return true;
                    },
                    error : function (xhr, option, error) {
                        alert(xhr.status + ' : ' + error);
                    }
                });
            }
            
            return false;
        });
    };
    
    return message;
});