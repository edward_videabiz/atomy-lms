<?php
use local_ubion\base\Common;
use local_ubion\base\Paging;

require_once '../../../config.php';

$id           = required_param('id', PARAM_INTEGER);
$page         = optional_param('page', 0, PARAM_INT);
$ls           = optional_param('ls', 15, PARAM_INT);        // how many per page

$CUser = \local_ubion\user\User::getInstance();
$CCourse = \local_ubion\course\Course::getInstance();
$course = $CCourse->getCourse($id);

$context = \context_course::instance($course->id);

// 언어 설정
$pluginname = 'local_ubsend';
$i8n = get_strings(array(    
    'email_send'
    ,'email_log'
    ,'resend'
), $pluginname);

## 페이지 셋팅
$context= context_course::instance($id);
$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);

// 주소 셋팅
$urlParam = array('id' => $id);
if (!empty($rid)) {
    $urlParam['rid'] = $rid;
}

$basePath = $CFG->wwwroot.'/local/ubsend/email';
$PAGE->set_url($basePath.'/log.php', $urlParam);
$PAGE->set_pagelayout('incourse');
$PAGE->set_heading($course->fullname);
$PAGE->set_title($course->fullname.' : '.$i8n->email_log);
$PAGE->navbar->add($i8n->email_log, $PAGE->url->out());


$isProfessor = $CCourse->isProfessor($course->id, $context);
$CEmail = new \local_ubsend\email\Email($id);

// 메일 전송 권한이 없는 경우 에러 페이지 출력
if (!$CEmail->isSendPermission()) {
    Common::printNotice(get_string('no_permissions', 'local_ubion'), $CFG->wwwroot.'/course/view.php?id='.$id);
}

echo $OUTPUT->header();
?>
<div class="local_ubsend local_ubsend_email">
	<?= $CEmail->getTab('log'); ?>
	
	<div class="well">
		<table class="table table-bordered table-coursemos table-responsive-sm">
			<colgroup>
				<col />
				<col />
				<col class="wp-80 hidden-sm-down" />
				<col class="wp-80" />
			</colgroup>
			<thead>
				<tr>
					<th class="min-wp-180"><?= get_string('email_subject', $pluginname); ?></th>
					<th><?= get_string('to', $pluginname); ?></th>
					<th class="hidden-sm-down"><?= get_string('email_attachments', $pluginname); ?></th>
					<th><?= get_string('etc', 'local_ubion'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$usernamefield = get_all_user_name_fields(true);
				list ($totalCount, $lists) = $CEmail->getLogs($page, $ls);
				
				if ($totalCount > 0) {
				    foreach ($lists as $log) {
				        // 전송일
				        $timecreated = $CEmail->getDate($log->timecreated);
				        
				        // 받는 사람
				        if (!empty($log->mailto)) {
				            $users = '';
				            
				            $mailto = explode(',', $log->mailto);
				            
				            $userCount = count($mailto);
				            
				            if ($userCount > 0) {
				                foreach ($mailto as $uid) {
				                    $u = $DB->get_record_sql('SELECT id, idnumber, '.$usernamefield.' FROM {user} WHERE id = :id', array('id'=>$uid));
				                    
				                    $fullname = fullname($u);
				                    $tooltip = $fullname.' ('.$CUser->getIdnumber($u->idnumber, $isProfessor).')';
				                    $users .= '<span class="label label-default mt-1 mx-1 max-wp-100 ellipsis" data-toggle="tooltip" title="'.$tooltip.'">'.$fullname.'</span>';
				                }
				            }
				            
				            
				        } else {
				            $users = '&nbsp;';
				        }
				        
				        // 첨부파일
				        if ($log->filecnt > 0) {
				            $file = '';
				            
				            $fs = get_file_storage();
				            if ($files = $fs->get_area_files($context->id, 'local_ubsend', $CEmail->getFileAreaName(), $log->id, 'id')) {
				                foreach ($files as $f) {
				                    // 디렉토리가 아닐 경우.
				                    if ($f->is_directory() and $f->get_filename() == '.') {
				                        continue;
				                    }
				                    
				                    $filename = $f->get_filename();
				                    $mimetype = $f->get_mimetype();
				                    $fileurl = \moodle_url::make_file_url('/pluginfile.php', '/'.$context->id.'/local_ubsend/'.$CEmail->getFileAreaName().'/'.$log->id.'/'.$filename, true);
				                    $file.= '<a href="'.$fileurl.'" title="'.$filename.'" class="m-1"><img src="'.$OUTPUT->image_url(file_mimetype_icon($mimetype)).'" alt="'.$mimetype.'" /></a>';
				                }
				            }
				        } else {
				            $file = '&nbsp;';
				        }
				        
				        // 재전송주소
				        $resendUrl = $CFG->wwwroot.'/local/ubsend/email/send.php?id='.$id.'&amp;rid='.$log->id;
				        
				        ?>
				        <tr>
				        	<td><?= $log->subject.'<br/><span class="small">'.$timecreated.'</span>'; ?></td>
				        	<td class="max-wp-300"><?= $users ?></td>
				        	<td class="text-center hidden-sm-down"><?= $file ?></td>
				        	<td class="text-center">
				        		<a href="<?= $resendUrl; ?>" class="btn btn-xs btn-default"><?= $i8n->resend; ?></a>
				        	</td>
				        </tr>
				        <?php 
				    }
				} else {
				    echo '<tr><td colspan="4">'.get_string('email_not_send', $pluginname).'</td></tr>';
				}
				?>
			</tbody>
		</table>
		
		<?= Paging::printHTML($totalCount, $page, $ls); ?>
	</div>
</div>
<?php 
echo $OUTPUT->footer();