<?php
use local_ubion\base\Common;

require_once '../../../config.php';

$id = optional_param('id', null, PARAM_INTEGER);
$rid = optional_param('rid', null, PARAM_INT);		// 재전송
$mode = optional_param('mode', null, PARAM_ALPHA);  // 사용자 기준, 그룹 기준 
$order = optional_param('order', 'username', PARAM_ALPHA);  // 정렬 순서 (학번, 이름)

$CCourse = \local_ubion\course\Course::getInstance();
$course = $CCourse->getCourse($id);

// 언어 설정
$pluginname = 'local_ubsend';
$i8n = get_strings(array(
    'email'
    ,'email_send'
    ,'email_subject'
), $pluginname);

## 페이지 셋팅
$context= context_course::instance($id);
$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);

// 주소 셋팅
$urlParam = array('id' => $id);
if (!empty($rid)) {
    $urlParam['rid'] = $rid;
}
 
$basePath = $CFG->wwwroot.'/local/ubsend/email';
$PAGE->set_url($basePath.'/send.php', $urlParam);
$PAGE->set_pagelayout('incourse');
$PAGE->set_heading($course->fullname);
$PAGE->set_title($course->fullname.' : '.$i8n->email_send);
$PAGE->navbar->add($i8n->email_send, $PAGE->url->out());

$editorname = 'email_content';
$CEmail = new \local_ubsend\email\Email($id);

// 메일 전송 권한이 없는 경우 에러 페이지 출력
if (!$CEmail->isSendPermission()) {
    Common::printNotice(get_string('no_permissions', 'local_ubion'), $CFG->wwwroot.'/course/view.php?id='.$id);
}

// 정렬 관련 설정
if ($order == 'username') {
    $order = (current_language() == 'ko') ? 'firstname' : 'lastname';
}

// 강좌내 그룹이 존재하는지 확인
$isGroup = false;
if ($courseGroups = groups_get_all_groups($course->id)) {
    $isGroup = true;
}


$PAGE->requires->strings_for_js(array(
    'min_user'
), 'local_ubion');

$PAGE->requires->strings_for_js(array(
    'email_content'
), $pluginname);

$PAGE->requires->js_call_amd('local_ubsend/email', 'send', array( 'editorname' => $editorname ));

// 기본값 셋팅
$data = new \stdClass();
$data->id = null;
$data->subject = null;
$data->content = null;
$data->users = array();

// 재전송인 경우 기존 내용 반영해줘야됨.
if (!empty($rid)) {
    if ($log = $CEmail->getLog($rid)) {
        $data->id = $log->id;
        $data->subject = $log->subject;
        $data->content = $log->content;
        
        // 메일 대상자
        $mailto = explode(',', $log->mailto);
        foreach ($mailto as $uid) {
            $data->users[$uid] = $uid;
        }
    }
}

// users.php에서 전달된 사용자가 존재하는 경우
if ($post = data_submitted()) {
    foreach ($post as $k => $v) {
        if (preg_match('/^user(\d+)$/',$k,$m)) {
            $data->users[$m[1]] = $m[1];
        }
    }
}

echo $OUTPUT->header();
?>
<div class="local_ubsend local_ubsend_email">
	<?= $CEmail->getTab(); ?>
	
	<div class="well well-search">
		<form class="form-search form-horizontal">
			<?php 
			if ($isGroup) {
			    $selected = array('user'=>'', 'group'=>'');
			    if ($mode == 'group') {
			        $selected['group'] = 'selected="selected"';
			    }  else {
			        $selected['user'] = 'selected="selected"';
			    }
			?>
			<div class="form-group row">
				<label class="control-label col-sm-2"><?= get_string('output_type', 'local_ubion'); ?></label>
				<div class="col-sm-10">
    				<select name="mode" class="form-control form-control-auto select-autosubmit">
    					<option value="user" <?= $selected['user'] ?>><?= get_string('output_type_user', 'local_ubion'); ?></option>
    					<option value="group" <?= $selected['group'] ?>><?= get_string('output_type_group', 'local_ubion'); ?></option>
    				</select>
				</div>
			</div>
			<?php 
			}
			?>
			
			<div class="form-group row">
				<label class="control-label col-sm-2"><?= get_string('sort_by', 'local_ubion'); ?></label>
				<div class="col-sm-10">
					<input type="hidden" name="id" value="<?= $course->id; ?>" />
    				<select name="order" class="form-control form-control-auto select-autosubmit">
    					<?php 
    					$selected = array('idnumber'=>'', 'username'=>'');
    					if ($order == 'firstname' || $order == 'lastname') {
    					    $selected['username'] = 'selected="selected"';
    					}  else {
    					    $selected['idnumber'] = 'selected="selected"';
    					}
    					?>
    					<option value="idnumber" <?= $selected['idnumber'] ?>><?= get_string('idnumber'); ?></option>
						<option value="username" <?= $selected['username'] ?>><?= get_string('fullnameuser'); ?></option>
    				</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-sm-2"><?= get_string('select'); ?></label>
				<div class="col-sm-10">
    				<button type="button" class="btn btn-sm btn-default btn-allchecked"><?= get_string('allchecked', 'local_ubion'); ?></button>
					<button type="button" class="btn btn-sm btn-default btn-unchecked"><?= get_string('unchecked', 'local_ubion'); ?></button>
				</div>
			</div>
		</form>
	</div>
	<div class="well">
    	<form method="post" class="form-send form-horizontal form-validate" id="form-send" action="action.php">
    		<div class="form-group row">
				<label class="control-label col-sm-2">
					<?= get_string('to', $pluginname); ?>
					<button type="button" class="btn btn-default btn-xs btn-collapsed-expanded">&nbsp;</button>
				</label>
				<div class="col-sm-10">
					<?php 
					if ($mode == 'group') {
					    echo $CEmail->getGroupForm($course->id, 'email', $order, $data->users);
					} else {
					    echo $CEmail->getUserForm($course->id, 'email', $order, $data->users);
					}
					?>
					<input type="hidden" name="id" value="<?= $id; ?>" />
					<input type="hidden" name="sesskey" value="<?= sesskey(); ?>" />
					<input type="hidden" name="<?= $CEmail::TYPENAME; ?>" value="send" />
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-sm-2"><?= $i8n->email_subject; ?></label>
				<div class="col-sm-10">
					<input type="text" name="subject" class="required form-control" placeholder="<?= $i8n->email_subject; ?>" value="<?= $data->subject; ?>" />
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-sm-2"><?= get_string('email_content', $pluginname); ?></label>
				<div class="col-sm-10">
					<?php 
					// 메일 보내기에서 에디터에 무들 이미지 첨부 기능은 사용되면 안됨
					// 에디터에서 작성된 이미지가 강좌내 이미지로 판단하기 때문에 반드시 로그인이 되어야 볼수 있음 (무들에 로그인 되어 있다면 상관 없으나, 비 로그인시 이미지가 안보일수 있음)
					// 예방 차원에서 무들 이미지 첨부 기능을 사용안하도록 maxfiles를 0값으로 셋팅함.
					// $editorOptions = array('maxbytes'=>$maxbytes, 'maxfiles'=>0);
					// 2017-11-07 에디터에 첨부된 이미지는 로그인 없이도 볼수 있도록 조치했기 때문에 파일 첨부 가능
					$editor = $CEmail->getMoodleEditor($editorname);
					
					// 재전송인 경우 기존에 전달했던 내용 반영해줘야됨.
					if (!empty($data->content)) {
					    $CEmail->setMoodleEditorContent($editor, $data->content, $context->id, 'local_ubsend', $CEmail->getEditorAreaName(), $data->id);
					}
					echo $editor->toHtml();
					?>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-sm-2"><?= get_string('email_attachments', $pluginname); ?></label>
				<div class="col-sm-10">
					<?php 
					$fileForm = $CEmail->getMoodleFileForm('email_attachments');
					
					// 파일은 itemid값 여부로 재전송 확인
					if (!empty($data->id)) {
					    $CEmail->setMoodleFileFormContent($fileForm, $context->id, 'local_ubsend', $CEmail->getFileAreaName(), $data->id);
					}
					echo $fileForm->toHtml();
					?>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary"><?= get_string('send', $pluginname); ?></button>
				</div>
			</div>
    	</form>
	</div>
</div>
<?php 
echo $OUTPUT->footer();