<?php
defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $pluginname = 'local_ubsend';

    $ADMIN->add('localplugins', new admin_category('local_ubsend', get_string('pluginname', $pluginname)));

    // #############
    // # Message 설정 ##
    // #############
    $temp = new admin_settingpage('local_ubsend_message', get_string('setting_message_title', $pluginname));

    // 메시지 삭제 기간
    $months = array(
        0 => get_string('not_use', 'local_ubion'),
        3 => get_string('nummonths', null, 3),
        6 => get_string('nummonths', null, 6),
        12 => get_string('numyears', null, 1)
    );

    $name = $pluginname . '/message_delete_period';
    $title = new lang_string('setting_message_delete_period', $pluginname);
    $description = new lang_string('setting_message_delete_period_desc', $pluginname);
    $default = 12;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $months);
    $temp->add($setting);

    $ADMIN->add('local_ubsend', $temp);

    // ###############
    // # Email 설정 ##
    // ###############
    $temp = new admin_settingpage('local_ubsend_email', get_string('setting_email_title', $pluginname));

    // 사용 권한
    $permissions = array(
        \local_ubsend\email\Email::PERMISSION_PROFESSOR => get_string('permission_prof', 'local_ubion'),
        \local_ubsend\email\Email::PERMISSION_STUDENT => get_string('permission_stud', 'local_ubion')
    );

    $name = $pluginname . '/email_send_permission';
    $title = new lang_string('setting_permission_send', $pluginname);
    $description = '';
    $default = \local_ubsend\email\Email::PERMISSION_PROFESSOR;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $permissions);
    $temp->add($setting);

    // 에디터 첨부 갯수
    $filecount = array(
        - 1 => get_string('unlimited')
    );
    for ($i = 0; $i <= 20; $i ++) {
        $filecount[$i] = $i;
    }

    $name = $pluginname . '/email_editor_filecount';
    $title = new lang_string('setting_email_editor_filecount', $pluginname);
    $description = '';
    $default = 5;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $filecount);
    $temp->add($setting);

    // 첨부파일 갯수
    $name = $pluginname . '/email_filecount';
    $title = new lang_string('setting_email_filecount', $pluginname);
    $description = '';
    $default = 5;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $filecount);
    $temp->add($setting);

    // 첨부파일 용량
    $maxbytes = get_max_upload_sizes($CFG->maxbytes, 0, 0);

    $name = $pluginname . '/email_filesize';
    $title = new lang_string('setting_email_filesize', $pluginname);
    $description = '';
    $default = 104857600; // 100M정도로 제한
    $setting = new admin_setting_configselect($name, $title, $description, $default, $maxbytes);
    $temp->add($setting);

    $ADMIN->add('local_ubsend', $temp);

    // #############
    // # SMS 설정 ##
    // #############
    $temp = new admin_settingpage('local_ubsend_sms', get_string('setting_sms_title', $pluginname));

    // 사용 권한
    $name = $pluginname . '/sms_send_permission';
    $title = new lang_string('setting_permission_send', $pluginname);
    $description = '';
    $default = \local_ubsend\email\Email::PERMISSION_PROFESSOR;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $permissions);
    $temp->add($setting);

    // 한글 Byte
    $name = $pluginname . '/sms_hanglebytes';
    $title = new lang_string('setting_sms_hanglebytes', $pluginname);
    $description = '';
    $default = 2;
    $setting = new admin_setting_configselect($name, $title, $description, $default, array(
        2 => '2 Byte',
        3 => '3 Byte'
    ));
    $temp->add($setting);

    // 최대 전송 bytes수
    $name = $pluginname . '/sms_maxbytes';
    $title = new lang_string('setting_sms_maxbytes', $pluginname);
    $description = '';
    $default = 80;
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $ADMIN->add('local_ubsend', $temp);
}