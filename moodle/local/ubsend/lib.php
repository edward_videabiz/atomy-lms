<?php

use local_ubsend\email\Email;

function local_ubsend_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload) {
    global $CFG, $DB;
    
    $CEmail = new email($course->id);
    
    if ($context->contextlevel != CONTEXT_COURSE) {
        return false;
    }
    
    // filearea가 메일로 전송된 항목이면 로그인 체크를 하면 안됨.
    if ($filearea != $CEmail->getFileAreaName() && $filearea != $CEmail->getEditorAreaName()) {
        // 로그인 체크
        require_course_login($course, true, $cm);
    }
    
    // 첫번째로 넘어오는 파라메터가 고유번호임
    $id = (int)array_shift($args);
    
    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/local_ubsend/$filearea/$id/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }
    
    send_stored_file($file, 0, 0, true); // download MUST be forced - security!
}
