<?php

function xmldb_local_ubsend_upgrade($oldversion) {
	global $CFG, $DB, $OUTPUT;

	$dbman = $DB->get_manager();

    if ($oldversion < 2017110800) {

        // Define table local_ubsend_email to be created.
        $table = new xmldb_table('local_ubsend_email');

        // Adding fields to table local_ubsend_email.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('mailto', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('subject', XMLDB_TYPE_CHAR, '500', null, XMLDB_NOTNULL, null, null);
        $table->add_field('content', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('filecnt', XMLDB_TYPE_INTEGER, '4', null, null, null, '0');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table local_ubsend_email.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Adding indexes to table local_ubsend_email.
        $table->add_index('courseid', XMLDB_INDEX_NOTUNIQUE, array('courseid'));

        // Conditionally launch create table for local_ubsend_email.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Ubsend savepoint reached.
        upgrade_plugin_savepoint(true, 2017110800, 'local', 'ubsend');
    }
    
    if ($oldversion < 2017112101) {
        
        // Define table local_ubsend_sms to be created.
        $table = new xmldb_table('local_ubsend_sms');
        
        // Adding fields to table local_ubsend_sms.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('smsto', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('smsto_cnt', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('subject', XMLDB_TYPE_CHAR, '500', null, null, null, null);
        $table->add_field('content', XMLDB_TYPE_CHAR, '500', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, null);
        $table->add_field('msgid', XMLDB_TYPE_CHAR, '1000', null, null, null, null);
        $table->add_field('phone', XMLDB_TYPE_CHAR, '100', null, null, null, null);
        
        // Adding keys to table local_ubsend_sms.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        
        // Adding indexes to table local_ubsend_sms.
        $table->add_index('courseid', XMLDB_INDEX_NOTUNIQUE, array('courseid'));
        $table->add_index('userid', XMLDB_INDEX_NOTUNIQUE, array('userid'));
        $table->add_index('timecreated', XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
        
        // Conditionally launch create table for local_ubsend_sms.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Ubsend savepoint reached.
        upgrade_plugin_savepoint(true, 2017112101, 'local', 'ubsend');
    }
    
    
    if ($oldversion < 2018080700) {
        
        // Define table local_ubsend_message to be created.
        $table = new xmldb_table('local_ubsend_message');
        
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('useridfrom', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('messageformat', XMLDB_TYPE_INTEGER, '1', null, null, null, '1');
        $table->add_field('group_count', XMLDB_TYPE_INTEGER, '10', null, null, null, '0');
        $table->add_field('message', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('notification', XMLDB_TYPE_INTEGER, '1', null, null, null, '0');
        $table->add_field('contexturl', XMLDB_TYPE_CHAR, '500', null, null, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('days', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, null);
        $table->add_field('push', XMLDB_TYPE_INTEGER, '1', null, null, null, '0');
        
        // Adding keys to table local_ubsend_message.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        
        $table->add_index('useridfrom', XMLDB_INDEX_NOTUNIQUE, array('useridfrom'));
        $table->add_index('day-push', XMLDB_INDEX_NOTUNIQUE, array('days', 'push'));
        
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        
        // 수신자 목록
        $table = new xmldb_table('local_ubsend_message_users');
        
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('messageid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('group_key', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('useridfrom', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('useridto', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('visible_from', XMLDB_TYPE_INTEGER, '1', null, null, null, '0');
        $table->add_field('visible_to', XMLDB_TYPE_INTEGER, '1', null, null, null, '0');
        $table->add_field('timeread', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->add_field('days', XMLDB_TYPE_INTEGER, '8', null, XMLDB_NOTNULL, null, null);
        
        // Adding keys to table local_ubsend_message_users.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        
        $table->add_index('messageid', XMLDB_INDEX_NOTUNIQUE, array('messageid'));
        $table->add_index('useridfrom', XMLDB_INDEX_NOTUNIQUE, array('useridfrom'));
        $table->add_index('useridto', XMLDB_INDEX_NOTUNIQUE, array('useridto'));
        $table->add_index('days', XMLDB_INDEX_NOTUNIQUE, array('days'));
        
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Ubsend savepoint reached.
        upgrade_plugin_savepoint(true, 2018080700, 'local', 'ubsend');
    }
    
}