<?php
/*
$handlers = array(
		'mod_created' => array(
				'handlerfile'      => '/local/ubion/events.php',
				'handlerfunction'  => 'mod_created',
				'schedule'         => 'instant',
				'internal'         => 1,
		),

		'mod_updated' => array(
				'handlerfile'      => '/local/ubion/events.php',
				'handlerfunction'  => 'mod_updated',
				'schedule'         => 'instant',
				'internal'         => 1,
		),

		'mod_deleted' => array(
				'handlerfile'      => '/local/ubion/events.php',
				'handlerfunction'  => 'mod_deleted',
				'schedule'         => 'instant',
				'internal'         => 1,
		),

		'mod_move' => array(
				'handlerfile'      => '/local/ubion/events.php',
				'handlerfunction'  => 'mod_move',
				'schedule'         => 'instant',
				'internal'         => 1,
		),
);
*/

$observers = array(
		// 메시지
		array(
				'eventname'   => '\core\event\message_sent',
				'callback'    => '\local_ubsend\observer::message_sent',
		),
);
