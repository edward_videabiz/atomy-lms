<?php
require_once '../../../config.php';


$controllder = \local_ubsend\message\Message::getInstance();

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}