<?php
use local_ubion\base\Parameter;
use local_ubion\base\Common;

require_once '../../../config.php';

$pluginname = 'local_ubsend';

$courseid = optional_param('courseid', null, PARAM_INT);
$page = optional_param('page', 0, PARAM_INT);
$ls = optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);
$keyfield = optional_param('keyfield', null, PARAM_ALPHAEXT);
$keyword = Parameter::getKeyword();

// 반드시 로그인 되어야 함.
require_login(SITEID, false);

$i8n = new stdClass();
$i8n->message = get_string('messages', 'message');
$i8n->delete = get_string('delete');
$i8n->sendmessage = get_string('sendmessage', 'message');
$i8n->mycontacts = get_string('mycontacts', $pluginname);

if (isguestuser()) {
    redirect($CFG->wwwroot);
}

if (empty($CFG->messaging)) {
    Common::printError(get_string('disabled', 'message'));
}

$baseurl = $CFG->wwwroot . '/local/ubsend/message';

$PAGE->set_url($baseurl . '/index.php');
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->navbar->add($i8n->message, $baseurl . '/');
$PAGE->set_title($i8n->message);

// Disable message notification popups while the user is viewing their messages
$PAGE->set_popup_notification_allowed(false);

$CCourse = \local_ubion\course\Course::getInstance();
$CMessage = \local_ubsend\message\Message::getInstance();

$ctx = new stdClass();
if ($ctx->mycourses = array_values($CCourse->getMyCourses())) {
    foreach ($ctx->mycourses as $mc) {
        $mc->fullname = $CCourse->getName($mc);
        $mc->selected = ($mc->id == $courseid);
    }
}
$ctx->isSearch = (! empty($keyfield) && ! empty($keyword)) ? true : false;
$ctx->baseurl = $baseurl;

$ctx->keyword = $keyword;
$ctx->courseid = $courseid;
$ctx->keyfields = Common::getArrayKeyValueObject([
    'username' => get_string('fullnameuser'),
    // 'idnumber' => get_string('idnumber'), // 학번 조회는 개인정보이기 때문에 불가능
    'message' => get_string('keyfield_message', $pluginname)
], $keyfield);

// 메시지 목록 가져오기
list ($totalCount, $messages) = $CMessage->getLists($courseid, $keyfield, $keyword, $page, $ls);
if ($totalCount > 0) {
    // 디비에서 조회한 값에 화면 출력에 필요한 값 세팅해서 리턴
    $messages = $CMessage->getListsSetting($messages, $courseid, $keyfield, $keyword, $page);
    $messages = array_values($messages);
}

$ctx->totalCount = $totalCount;
$ctx->messages = $messages;

$deleteMessage = null;
$period = $CMessage->getDeletePeriod();
if (! empty($period)) {

    $stringid = 'nummonths';
    if ($period % 12 == 0) {
        $stringid = 'numyears';
        $period = $period / 12;
    }

    $strPeriod = get_string($stringid, null, $period);
    $deleteMessage = get_string('message_delete_period', $pluginname, $strPeriod);
}
$ctx->deleteMessage = $deleteMessage;

$PAGE->requires->js_call_amd('local_ubsend/message', 'index', array(
    'action' => $baseurl . '/action.php'
));
$PAGE->requires->strings_for_js(array(
    'confirm_all_delete'
), $pluginname);

echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->message);

echo $OUTPUT->render_from_template('local_ubsend/message-index', $ctx);
echo $OUTPUT->footer();