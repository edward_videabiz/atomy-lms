<?php
use local_ubion\base\Common;
use local_ubion\base\Parameter;

require_once '../../../config.php';

require_login(0, false);

if (isguestuser()) {
    redirect($CFG->wwwroot);
}

if (empty($CFG->messaging)) {
    Common::printError(get_string('disabled', 'message'));
}

$pluginname = 'local_ubsend';

$i8n = new stdClass();
$i8n->message = get_string('messages', 'message');
$i8n->confirm_message_cancel = get_string('confirm_message_cancel', $pluginname);
$i8n->confirm_message_delete = get_string('confirm_message_delete', $pluginname);
$i8n->confirm_message_delete_tonoread = get_string('confirm_message_delete_tonoread', $pluginname);
$i8n->message_day_delete = get_string('confirm_day_delete', $pluginname);

$targetid = required_param('id', PARAM_INT);

// 리스트로 돌아가기 위한 파라메터
$keyfield = optional_param('keyfield', null, PARAM_ALPHAEXT);
$keyword = Parameter::getKeyword();
$courseid = optional_param('courseid', null, PARAM_INT);
$page = optional_param('page', null, PARAM_INT);

$mainurl = $CFG->wwwroot . '/local/ubsend/message/';
$url = new moodle_url('/local/ubsend/message/message.php');
foreach ($_GET as $key => $value) {
    if (! empty($value)) {
        $url->param($key, $value);
    }
}

$PAGE->set_url($url);
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->navbar->add($i8n->message, $mainurl);

// Disable message notification popups while the user is viewing their messages
// $PAGE->set_popup_notification_allowed(false);
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_title($i8n->message);

$CUser = \local_ubion\user\User::getInstance();
$CMessage = \local_ubsend\message\Message::getInstance();

// 사용자가 존재하는지 확인
if ($targetInfo = $CMessage->getTargetInfo($targetid)) {

    // 대상자 화면 출력용 설정
    $targetInfo->userpicture = $CUser->getPicture($targetInfo, 80);
    $targetInfo->fullname = fullname($targetInfo);
    // 사용자간 메시지에서는 학번을 노출 시키면 안됨.
    $targetInfo->idnumber = $CUser->getIdnumber($targetInfo->idnumber);
    $PAGE->navbar->add($targetInfo->fullname, $url);

    // 본인 화면 출력용 설정
    $userInfo = clone $USER;
    $userInfo->userpicture = $CUser->getPicture($userInfo, 80);
    $userInfo->fullname = fullname($userInfo);

    /*
     * 코스모스에서는 user1id값을 따로 지정할수 없게 코딩되어 있으므로 해당 코드는 필요가 없음
     * $systemcontext = context_system::instance();
     *
     * // Is the user involved in the conversation?
     * // Do they have the ability to read other user's conversations?
     * if (!has_capability('moodle/site:readallmessages', $systemcontext)) {
     * print_error('accessdenied', 'admin');
     * }
     */

    // 상대방 대화중에 안읽은 메시지가 있다면 전부 읽음으로 처리해줘야됨.
    $countunread = $CMessage->getNotReadCount($targetInfo->id);
    if ($countunread > 0) {
        $CMessage->setMessageRead($targetInfo->id);
    }

    // 첫페이지 로딩시에는 가장 최근글이 표시되어야 하기 때문에
    // page, ls는 전달해줄필요가 없음
    list ($totalCount, $messages) = $CMessage->getUserMessages($targetid);

    // 화면에 페이지당 표시될 메시지 갯수가 사용자간에 전달된 메시지 갯수보다 크다면
    // 페이징 처리를 할 필요가 없음
    $isLastPage = ($totalCount < $CMessage->getUserMessageLS()) ? true : false;

    $html = '';
    if ($totalCount > 0) {
        $html = $CMessage->getMessageListHtml($messages, $keyfield, $keyword);
    } else {
        $html = '<div class="py-3">' . get_string('no_message', $pluginname) . '</div>';
    }

    // 메시지 전송 폼 출력여부
    $isSend = has_capability('moodle/site:sendmessage', context_system::instance());

    $ctx = new stdClass();
    $ctx->targetInfo = $targetInfo;
    $ctx->userInfo = $userInfo;
    $ctx->html = $html;
    $ctx->isSend = $isSend;
    $ctx->coursemostype = $CMessage::TYPENAME;
    $ctx->mainurl = $mainurl;

    $myContacts = new stdClass();
    $myContacts->is = false;
    $myContacts->type = 'add';
    $myContacts->class = 'btn-default';
    $myContacts->text = get_string('addcontact', 'message');

    if ($CMessage->isContactUser($targetid)) {
        $myContacts->is = true;
        $myContacts->type = 'delete';
        $myContacts->class = 'btn-danger';
        $myContacts->text = get_string('removecontact', 'message');
    }
    $ctx->myContacts = $myContacts;

    $PAGE->requires->js_call_amd('local_ubsend/message', 'message', array(
        'targetid' => $targetid,
        'action' => $mainurl . '/action.php',
        'isLastPage' => $isLastPage
    ));
    $PAGE->requires->strings_for_js(array(
        'confirm_message_cancel',
        'confirm_message_delete',
        'confirm_message_delete_tonoread',
        'confirm_day_delete'
    ), $pluginname);

    $PAGE->requires->strings_for_js(array(
        'removecontact',
        'addcontact'
    ), 'message');

    echo $OUTPUT->header();
    echo $OUTPUT->heading($i8n->message);

    echo $OUTPUT->render_from_template('local_ubsend/message-message', $ctx);
    echo $OUTPUT->footer();
} else {
    Common::printNotice(get_string('invaliduserid', 'error'), $CFG->wwwroot);
}