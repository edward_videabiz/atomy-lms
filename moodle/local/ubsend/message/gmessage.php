<?php
use local_ubion\base\Common;
use local_ubion\base\Javascript;
use local_ubion\base\Parameter;

require_once '../../../config.php';

require_login(0, false);

if (isguestuser()) {
    redirect($CFG->wwwroot);
}

if (empty($CFG->messaging)) {
    Common::printError(get_string('disabled', 'message'));
}

$pluginname = 'local_ubsend';

$i8n = new stdClass();
$i8n->message = get_string('messages', 'message');
$i8n->confirm_message_all_cancel = get_string('confirm_message_all_cancel', $pluginname);
$i8n->confirm_message_cancel = get_string('confirm_message_cancel', $pluginname);


// 그룹메시지 local_ubsend_message id값임
$id = required_param('id', PARAM_INT);


// 리스트로 돌아가기 위한 파라메터
$keyfield 	= optional_param('keyfield', null, PARAM_ALPHAEXT);
$keyword 	= Parameter::getKeyword();
$courseid	= optional_param('courseid', null, PARAM_INT);
$page		= optional_param('page', null, PARAM_INT);


$url = new moodle_url('/local/ubsend/message/gmessage.php');
foreach($_GET as $key => $value) {
    if (!empty($value)) {
        $url->param($key, $value);
    }
}

$PAGE->set_url($url);

$PAGE->set_context(context_user::instance($USER->id));
$PAGE->navbar->add($i8n->message, $CFG->wwwroot.'/local/ubsend/message/');
$PAGE->navbar->add(get_string('group_message', $pluginname), $url);

// Disable message notification popups while the user is viewing their messages
$PAGE->set_popup_notification_allowed(false);
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_title($i8n->message);


$mainurl = $CFG->wwwroot.'/local/ubsend/message/';
$CMessage = \local_ubsend\message\Message::getInstance();
$CUser = \local_ubion\user\User::getInstance();

// 메시지가 실제 존재하는지 확인
if ($message = $CMessage->getMessage($id)) {
    
    // 그룹 메세지는 발송한 나만 볼 수 있다.
    if ($USER->id != $message->useridfrom) {
        Common::printNotice(get_string('no_permissions', 'local_ubion'), $mainurl);
    }
    
    
    // 실제 그룹메시지인지 확인
    // 그룹 메시지였다가 전송 취소하여서 1명만 남는 경우가 있음, 이런 경우 메시지 확인 자체가 되지 않음.
    // 메시지 전송 자체를 그룹으로 시작하였기 때문에 1명만 남아도 그룹메시지로 판단시켜버림
    if ($message->group_count < 1) {
        Common::printNotice(get_string('not_group_message', $pluginname), $mainurl);
    }
    
    
    // 그룹 대화중에 본인이 안읽은 메시지가 존재하는지 확인
    // 그룹 메시지에 본인을 포함 시키는 경우가 있음.
    $countunread = $CMessage->getNotReadGroupCount($id);
    if ($countunread > 0) {
        $CMessage->setGroupMessageRead($id);
    }
    
    
    
    
    // 디비에서 컨트롤 하지 않는 이상 사용자는 무조건 존재함.
    $users = $CMessage->getGroupMessageUsers($id);
    
    // 읽지 않은 사용자가 존재하면 읽지 않은 사용자 전송 취소 버튼 노출 시켜줘야됨.
    $isNoReadUser = false;
    
    foreach ($users as $u) {
        
        $readDate = $readDateShort = null;
        
        if (empty($u->timeread)) {
            $isNoReadUser = true;
        } else {
            $readDate = Common::getUserDate($u->timeread);
            $readDateShort = Common::getUserDateShort($u->timeread);
        }
        
        // 2015-09-02 by akddd
        // gm->id값은 local_ubmessage_fromto의 id값이기 때문에 사진 출력용으로 별도로 객체 생성해줘야됨.
        $picture = clone $u;
        $picture->id = $u->useridto;
        $u->userpicture = $CUser->getPicture($picture);
        $u->fullname = fullname($u);
        $u->readDate = $readDate;
        $u->readDateShort = $readDateShort;
    }
    
    
    // 그룹메시지는 메시지 목록이 여러개 나올수가 없기 때문에 강제로 array로 감싸줌
    $messages = array($message);
    $messageHTML = $CMessage->getMessageListHtml($messages, $keyfield, $keyword, true);
    
    
    $ctx = new stdClass();
    $ctx->users = array_values($users);
    $ctx->html = $messageHTML;
    $ctx->isNoReadUser = $isNoReadUser;
    
    
    $PAGE->requires->js_call_amd('local_ubsend/message', 'gmessage', array(
        'id' => $id,
        'action' => $mainurl.'/action.php'
    ));
    $PAGE->requires->strings_for_js(array(
        'confirm_message_cancel',
        'confirm_message_all_cancel'
    ), $pluginname);
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading($i8n->message);
    echo $OUTPUT->render_from_template('local_ubsend/message-gmessage', $ctx);
    
    echo $OUTPUT->footer();
} else {
    Javascript::printAlertMove(get_string('no_message', $pluginname), $mainurl);
}