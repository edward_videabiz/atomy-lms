<?php
use local_ubion\base\Common;

require_once '../../../config.php';

$id = required_param('id', PARAM_INTEGER);
$mode = optional_param('mode', null, PARAM_ALPHA);  // 사용자 기준, 그룹 기준 
$order = optional_param('order', 'username', PARAM_ALPHA);  // 정렬 순서 (학번, 이름)

$CCourse = \local_ubion\course\Course::getInstance();
$course = $CCourse->getCourse($id);

// 언어 설정
$pluginname = 'local_ubsend';
$i8n = get_strings(array(
    'message'
    ,'message_send'
), $pluginname);

## 페이지 셋팅
$context= context_course::instance($id);
$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);

// 주소 셋팅
$urlParam = array('id' => $id);
$basePath = $CFG->wwwroot.'/local/ubsend/message';
$PAGE->set_url($basePath.'/send.php', $urlParam);
$PAGE->set_pagelayout('incourse');
$PAGE->set_heading($course->fullname);
$PAGE->set_title($course->fullname.' : '.$i8n->message_send);
$PAGE->navbar->add($i8n->message_send, $PAGE->url->out());


$CMessage = \local_ubsend\message\Message::getInstance();

// 메일 전송 권한이 없는 경우 에러 페이지 출력
if (!$CMessage->isSendPermission()) {
    Common::printNotice(get_string('no_permissions', 'local_ubion'), $CFG->wwwroot.'/course/view.php?id='.$id);
}

// 정렬 관련 설정
if ($order == 'username') {
    $order = (current_language() == 'ko') ? 'firstname' : 'lastname';
}

// 강좌내 그룹이 존재하는지 확인
$isGroup = false;
if ($courseGroups = groups_get_all_groups($course->id)) {
    $isGroup = true;
}


$PAGE->requires->strings_for_js(array(
    'min_user'
), 'local_ubion');


// 기본값 셋팅
$data = new \stdClass();
$data->id = null;
$data->subject = null;
$data->content = null;
$data->users = array();


// users.php에서 전달된 사용자가 존재하는 경우
if ($post = data_submitted()) {
    foreach ($post as $k => $v) {
        if (preg_match('/^user(\d+)$/',$k,$m)) {
            $data->users[$m[1]] = $m[1];
        }
    }
}

$PAGE->requires->js_call_amd('local_ubsend/message', 'send', array());

echo $OUTPUT->header();
?>
<div class="local_ubsend local_ubsend_message">
	
	
	<div class="well well-search">
		<form class="form-search form-horizontal">
			<?php 
			if ($isGroup) {
			    $selected = array('user'=>'', 'group'=>'');
			    if ($mode == 'group') {
			        $selected['group'] = 'selected="selected"';
			    }  else {
			        $selected['user'] = 'selected="selected"';
			    }
			?>
			<div class="form-group row">
				<label class="control-label col-sm-3"><?= get_string('output_type', 'local_ubion'); ?></label>
				<div class="col-sm-9">
    				<select name="mode" class="form-control form-control-auto select-autosubmit">
    					<option value="user" <?= $selected['user'] ?>><?= get_string('output_type_user', 'local_ubion'); ?></option>
    					<option value="group" <?= $selected['group'] ?>><?= get_string('output_type_group', 'local_ubion'); ?></option>
    				</select>
				</div>
			</div>
			<?php 
			}
			?>
			
			<div class="form-group row">
				<label class="control-label col-sm-3"><?= get_string('sort_by', 'local_ubion'); ?></label>
				<div class="col-sm-9">
					<input type="hidden" name="id" value="<?= $course->id; ?>" />
    				<select name="order" class="form-control form-control-auto select-autosubmit">
    					<?php 
    					$selected = array('idnumber'=>'', 'username'=>'');
    					if ($order == 'firstname' || $order == 'lastname') {
    					    $selected['username'] = 'selected="selected"';
    					}  else {
    					    $selected['idnumber'] = 'selected="selected"';
    					}
    					?>
    					<option value="idnumber" <?= $selected['idnumber'] ?>><?= get_string('idnumber'); ?></option>
						<option value="username" <?= $selected['username'] ?>><?= get_string('fullnameuser'); ?></option>
    				</select>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-sm-3"><?= get_string('select'); ?></label>
				<div class="col-sm-9">
    				<button type="button" class="btn btn-sm btn-default btn-sm btn-allchecked"><?= get_string('allchecked', 'local_ubion'); ?></button>
					<button type="button" class="btn btn-sm btn-default btn-sm btn-unchecked"><?= get_string('unchecked', 'local_ubion'); ?></button>
				</div>
			</div>
		</form>
	</div>
	<div class="well">
    	<form method="post" class="form-send form-validate form-horizontal" id="form-send" action="action.php">
    		<div class="form-group row">
				<label class="control-label col-sm-3">
					<?= get_string('to', $pluginname); ?>
					<button type="button" class="btn btn-default btn-sm btn-xs btn-collapsed-expanded">&nbsp;</button>
				</label>
				<div class="col-sm-9">
					<?php 
					if ($mode == 'group') {
					    echo $CMessage->getGroupForm($course->id, '', $order, $data->users, 'radio');
					} else {
					    echo $CMessage->getUserForm($course->id, '', $order, $data->users, 'radio');
					}
					?>
					<input type="hidden" name="id" value="<?= $id; ?>" />
					<input type="hidden" name="sesskey" value="<?= sesskey(); ?>" />
					<input type="hidden" name="<?= $CMessage::TYPENAME; ?>" value="groupSend" />
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-sm-3"><?= $i8n->message; ?></label>
				<div class="col-sm-9">
					<textarea class="form-control required" name="message" placeholder="<?=$i8n->message;?>" rows="8"></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-offset-3 col-sm-9">
					<button type="submit" class="btn btn-primary"><?= get_string('send', $pluginname); ?></button>
				</div>
			</div>
    	</form>
	</div>
</div>
<?php 
echo $OUTPUT->footer();