<?php
use local_ubion\base\Common;
use local_ubion\base\Parameter;
use local_ubion\base\Javascript;

require_once '../../../config.php';

$pluginname = 'local_ubsend';

if (Parameter::isAajx()) {

    $courseid = optional_param('courseid', null, PARAM_INT);
    $page = optional_param('page', 0, PARAM_INT);
    $ls = optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);
    $keyfield = optional_param('keyfield', null, PARAM_ALPHAEXT);
    $keyword = Parameter::getKeyword();

    require_login();

    $i8n = new stdClass();
    $i8n->mycontacts = get_string('mycontacts', $pluginname);

    $baseurl = $CFG->wwwroot . '/local/ubsend/message';
    $PAGE->set_url($baseurl . '/mycontacts.php');
    $PAGE->set_context(context_user::instance($USER->id));
    $PAGE->set_title($i8n->mycontacts);
    $PAGE->set_pagelayout('popup');

    // 헤더 영역 표시하면 안됨.
    $PAGE->add_body_class('no-header');

    // Disable message notification popups while the user is viewing their messages
    $PAGE->set_popup_notification_allowed(false);

    $ctx = new stdClass();

    echo $OUTPUT->header();
    echo $OUTPUT->heading($i8n->mycontacts);

    echo $OUTPUT->render_from_template('local_ubsend/mycontacts', $ctx);
    echo $OUTPUT->footer();
} else {
    Javascript::printAlert(get_string('ajax_only', 'local_ubion'));
}