<?php

function xmldb_local_ubion_upgrade($oldversion)
{
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2019051000) {

        
        // Define field jisacode to be added to user_ubion.
        $table = new xmldb_table('user_ubion');
        $field = new xmldb_field('jisacode', XMLDB_TYPE_CHAR, '50', null, null, null, null, 'sex');
        
        // Conditionally launch add field jisacode.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        $field = new xmldb_field('lmsedufinishdate', XMLDB_TYPE_CHAR, '100', null, null, null, null, 'jisacode');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        
        $field = new xmldb_field('lmseducertdate', XMLDB_TYPE_CHAR, '100', null, null, null, null, 'lmsedufinishdate');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        $field = new xmldb_field('atomyusertoken', XMLDB_TYPE_CHAR, '1000', null, null, null, null, 'lmseducertdate');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        
        // Ubion savepoint reached.
        upgrade_plugin_savepoint(true, 2019051000, 'local', 'ubion');
    }
    
    
    if ($oldversion < 2019052000) {
        
        // Define field onoff to be added to user_ubion.
        $table = new xmldb_table('user_ubion');
        $field = new xmldb_field('onoff', XMLDB_TYPE_INTEGER, '1', null, null, null, '0', 'atomyusertoken');
        
        // Conditionally launch add field onoff.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        // Ubion savepoint reached.
        upgrade_plugin_savepoint(true, 2019052000, 'local', 'ubion');
    }
    

    return true;
}