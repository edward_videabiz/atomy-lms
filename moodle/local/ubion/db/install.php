<?php
function xmldb_local_ubion_install() {
	global $DB, $CFG;

	$dbman = $DB->get_manager();

	/*
	 * course, course_categories 테이블 인덱스 추가
	*/
	$table = new xmldb_table('course');

	// lang
	$lang_index = new xmldb_index('cour_sto_lng_idx', XMLDB_INDEX_NOTUNIQUE, array('lang'));
	if (!$dbman->index_exists($table, $lang_index)) {
		$dbman->add_index($table, $lang_index);
	}


	# 강좌 카테고리
	$table = new xmldb_table('course_categories');
	// sortorder
	$sortorder_index = new xmldb_index('courcate_sto_ix', XMLDB_INDEX_NOTUNIQUE, array('sortorder'));
	if (!$dbman->index_exists($table, $sortorder_index)) {
		$dbman->add_index($table, $sortorder_index);
	}

	// course_count
	$course_count_index = new xmldb_index('courcate_csc_ix', XMLDB_INDEX_NOTUNIQUE, array('coursecount'));
	if (!$dbman->index_exists($table, $course_count_index)) {
		$dbman->add_index($table, $course_count_index);
	}

	// path
	$path_index = new xmldb_index('courcate_pat_ix', XMLDB_INDEX_NOTUNIQUE, array('path'));
	if (!$dbman->index_exists($table, $path_index)) {
		$dbman->add_index($table, $path_index);
	}


	// external_tokens 테이블에 token 컬럼에 인덱스 추가
	$table = new xmldb_table('external_tokens');
	$index = new xmldb_index('token', XMLDB_INDEX_NOTUNIQUE, array('token'));
	if (!$dbman->index_exists($table, $index)) {
		$dbman->add_index($table, $index);
	}
	
}