<?php
$definitions = array(
    'course_ubion' => array(
        'mode' => cache_store::MODE_APPLICATION,
        'simplekeys' => true,
        'staticacceleration' => true,
        'ttl' => 3600
    ),
    'course_professor' => array(
        'mode' => cache_store::MODE_APPLICATION,
        'simplekeys' => true,
        'staticacceleration' => true,
        'ttl' => 3600
    ),
    // 휴일 캐시
    'holiday' => array(
        'mode' => cache_store::MODE_APPLICATION,
        'simplekeys' => true,
        'staticacceleration' => true,
        'ttl' => 86400
    )
);
