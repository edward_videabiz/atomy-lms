<?php
$observers = array(
    // 강좌 삭제 (course_ubion에 레코드 추가는 강좌 포맷에서 이뤄지기 때문에 따로 이벤트 처리 하지 않습니다.)
    array(
        'eventname' => '\core\event\course_deleted',
        'callback' => 'local_ubion_observer::course_deleted'
    )
);
