<?php
defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $pluginname = 'local_ubion';

    $ADMIN->add('localplugins', new admin_category('local_ubion', get_string('pluginname', $pluginname)));

    // ##############
    // # 기본 설정 ##
    // ##############
    $temp = new admin_settingpage('local_ubion_default', get_string('setting_default', $pluginname));

    $name = $pluginname . '/use_favorite';
    $title = get_string('setting_favorite', $pluginname);
    $description = get_string('setting_favorite_help', $pluginname);
    $default = 0;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $temp->add($setting);

    // new 표시 기간 설정
    $options = array();
    for ($i = 1; $i <= 10; $i ++) {
        $options[$i] = $i;
    }
    $name = $pluginname . '/new_days';
    $title = get_string('setting_new_days', $pluginname);
    $description = get_string('setting_new_days_help', $pluginname);
    $default = 1;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $temp->add($setting);

    // 세션에 저장된 강좌 목록 재조회 시간
    $options = array();
    for ($i = 1; $i <= 300; $i ++) {
        $options[$i] = get_string('numseconds', 'core', $i);
    }
    $name = $pluginname . '/progress_course_sessiontime';
    $title = get_string('setting_progress_session_time', $pluginname);
    $description = get_string('setting_progress_session_time_help', $pluginname);
    $default = 60;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $temp->add($setting);

    // 진행강좌 - 현재시간 기준으로 몇일전
    $options = array();
    for ($i = 1; $i <= 31; $i ++) {
        $options[$i] = get_string('numday', 'core', $i);
    }
    $name = $pluginname . '/progress_course_prev';
    $title = get_string('setting_progress_course_prev', $pluginname);
    $description = get_string('setting_progress_course_prev_help', $pluginname);
    $default = 14;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $temp->add($setting);

    // 강좌 정렬 기능 사용
    $name = $pluginname . '/progress_course_next';
    $title = get_string('setting_progress_course_next', $pluginname);
    $description = get_string('setting_progress_course_next_help', $pluginname);
    $default = 14;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $temp->add($setting);

    // 역할 roleid 설정
    $temp->add(new admin_setting_heading('local_ubion_role', get_string('roles'), ''));

    $name = $pluginname . '/role_professor';
    $title = get_string('role_professor', $pluginname);
    $description = '';
    $default = 3;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $temp->add($setting);

    $name = $pluginname . '/role_student';
    $title = get_string('role_student', $pluginname);
    $description = '';
    $default = 5;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $temp->add($setting);

    $name = $pluginname . '/role_assistant';
    $title = get_string('role_assistant', $pluginname);
    $description = '';
    $default = 9;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $temp->add($setting);

    $name = $pluginname . '/role_auditor';
    $title = get_string('role_auditor', $pluginname);
    $description = '';
    $default = 10;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $temp->add($setting);

    // 그룹 설정
    $temp->add(new admin_setting_heading('local_ubion_group', get_string('setting_group', $pluginname), ''));

    $options = array(
        0 => get_string('setting_groupoption_group', $pluginname),
        1 => get_string('setting_groupoption_grouping', $pluginname)
    );
    $name = $pluginname . '/output_groupoption';
    $title = get_string('setting_groupoption', $pluginname);
    $description = '';
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $temp->add($setting);

    // LMS 시작/종료 설정
    $temp->add(new admin_setting_heading('setting_year_semester', get_string('setting_lms_year_semester', $pluginname), ''));

    $name = $pluginname . '/lms_start_year';
    $title = get_string('setting_lms_start_year', $pluginname);
    $description = '';
    $default = date('Y');
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $temp->add($setting);

    $name = $pluginname . '/lms_start_semester';
    $title = get_string('setting_lms_start_semester', $pluginname);
    $description = get_string('setting_semester_help', $pluginname);
    $default = \local_ubion\course\Course::CODE_1;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_ALPHANUMEXT);
    $temp->add($setting);

    // 코스모스 시작연도
    $name = $pluginname . '/csms_start_year';
    $title = get_string('setting_csms_start_year', $pluginname);
    $description = '';
    $default = date('Y');
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $temp->add($setting);

    $name = $pluginname . '/csms_start_semester';
    $title = get_string('setting_csms_start_semester', $pluginname);
    $description = get_string('setting_semester_help', $pluginname);
    $default = \local_ubion\course\Course::CODE_1;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_ALPHANUMEXT);
    $temp->add($setting);

    $ADMIN->add('local_ubion', $temp);

    // ##################
    // # 학사연동 설정 ##
    // ##################
    $temp = new admin_settingpage('local_ubion_haksa', get_string('setting_haksa', $pluginname));

    $name = $pluginname . '/location_php';
    $title = get_string('setting_haksa_location_php', $pluginname);
    $description = get_string('setting_haksa_location_php_desc', $pluginname);
    $default = '/usr/local/php/bin/php';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_NOTAGS);
    $temp->add($setting);

    $name = $pluginname . '/location_phpini';
    $title = get_string('setting_haksa_location_phpini', $pluginname);
    $description = get_string('setting_haksa_location_phpini_desc', $pluginname);
    $default = '/usr/local/apache2/conf/php.ini';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_NOTAGS);
    $temp->add($setting);

    $name = $pluginname . '/haksa_auth';
    $title = get_string('setting_haksa_auth', $pluginname);
    $description = get_string('setting_haksa_auth_desc', $pluginname);
    $default = 'univ';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_NOTAGS);
    $temp->add($setting);

    $name = $pluginname . '/haksa_webhook';
    $title = get_string('setting_haksa_webhook', $pluginname);
    $description = get_string('setting_haksa_webhook_desc', $pluginname);
    $default = 'https://wh.jandi.com/connect-api/webhook/11159602/e6a425753dc52b06e79d4878d7dafaed';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_NOTAGS);
    $temp->add($setting);


    $ADMIN->add('local_ubion', $temp);

    // ##############
    // # push 설정 ##
    // ##############
    $temp = new admin_settingpage('local_ubion_push', get_string('setting_push', $pluginname));

    // 푸시 서버 주소
    $name = $pluginname . '/push_host';
    $title = get_string('setting_push_host', $pluginname);
    $description = get_string('setting_push_host_help', $pluginname);
    $default = 'http://push2.coursemos.kr';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    // 푸시 서버 port
    $name = $pluginname . '/push_port';
    $title = get_string('setting_push_port', $pluginname);
    $description = get_string('setting_push_port_help', $pluginname);
    $default = '7444';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    // 푸시 schoolid
    $name = $pluginname . '/push_school';
    $title = get_string('setting_push_school', $pluginname);
    $description = get_string('setting_push_school_help', $pluginname);
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $ADMIN->add('local_ubion', $temp);
}