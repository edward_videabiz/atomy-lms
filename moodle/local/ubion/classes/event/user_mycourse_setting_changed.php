<?php
namespace local_ubion\event;

defined('MOODLE_INTERNAL') || die();

/**
 * Class user_mycourse_setting_changed
 *
 * @package local/ubion
 */
class user_mycourse_setting_changed extends \core\event\base
{

    /**
     * Set basic properties for the event.
     */
    protected function init()
    {
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_OTHER;
    }

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name()
    {
        return get_string('event_user_mycourse_setting_changed', 'local_ubion');
    }

    /**
     * Returns non-localised event description with id's for admin use only.
     *
     * @return string
     */
    public function get_description()
    {
        if ($this->other['type'] == 'sortorder') {
            return 'User Course sortorder changed';
        } else {
            return 'User Course visible changed, courseid => ' . $this->other['courseid'] . ', old => ' . $this->other['old'] . ', new => ' . $this->other['new'];
        }
    }

    /**
     * custom validations
     *
     * Throw \coding_exception notice in case of any problems.
     */
    protected function validate_data()
    {
        if (! isset($this->other['type'])) {
            throw new \coding_exception("Field other['type'] cannot be empty");
        }
    }
    
    public static function get_other_mapping() {
        return false;
    }
}

