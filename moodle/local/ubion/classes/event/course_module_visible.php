<?php
namespace local_ubion\event;

defined('MOODLE_INTERNAL') || die();

/**
 * course_module_visible
 *
 * 학습자원/활동이 보기/숨김되었을때 발생되는 이벤트
 *
 * @property-read array $other {
 *                Extra information about event.
 *               
 *                - string modulename : 모듈명.
 *                - int instanceid : 모듈id
 *                }
 */
class course_module_visible extends \core\event\base
{

    /**
     * Set basic properties for the event.
     */
    protected function init()
    {
        $this->data['objecttable'] = 'course_modules';
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
    }

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name()
    {
        return get_string('event_coursemodulevisible', 'local_ubion');
    }

    /**
     * Returns non-localised event description with id's for admin use only.
     *
     * @return string
     */
    public function get_description()
    {
        return 'The module ' . $this->other['modulename'] . ' with instance id ' . $this->other['instanceid'] . ' was visible by
                user with id ' . $this->userid;
    }

    /**
     * custom validations
     *
     * Throw \coding_exception notice in case of any problems.
     */
    protected function validate_data()
    {
        if (! isset($this->other['modulename'])) {
            throw new \coding_exception("Field other['modulename'] cannot be empty");
        }
        if (! isset($this->other['instanceid'])) {
            throw new \coding_exception("Field other['instanceid'] cannot be empty");
        }
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'course_modules', 'restore' => 'course_module');
    }
    
    
    public static function get_other_mapping() {
        $othermapping = array();
        $othermapping['instanceid'] = \core\event\base::NOT_MAPPED;
        
        return $othermapping;
    }
}

