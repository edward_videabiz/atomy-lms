<?php
namespace local_ubion\event;

defined('MOODLE_INTERNAL') || die();

class course_settinged extends \core\event\base
{

    /**
     * Set basic properties for the event.
     */
    protected function init()
    {
        $this->data['objecttable'] = 'course';
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
    }

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name()
    {
        return get_string('event_coursesettinged', 'local_ubion');
    }

    /**
     * Returns non-localised event description with id's for admin use only.
     *
     * @return string
     */
    public function get_description()
    {
        return 'The course  with id ' . $this->courseid . ' was settinged by
                user with id ' . $this->userid;
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url()
    {
        return new \moodle_url('/local/ubion/course/setting.php', array(
            'id' => $this->courseid
        ));
    }
    
    
    public static function get_objectid_mapping() {
        return array('db' => 'course', 'restore' => 'course');
    }
}