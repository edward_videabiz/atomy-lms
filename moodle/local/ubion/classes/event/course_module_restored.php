<?php
namespace local_ubion\event;

defined('MOODLE_INTERNAL') || die();

/**
 * course_module_restored
 *
 * 학습자원/활동 복구
 */
class course_module_restored extends \core\event\base
{

    /**
     * Set basic properties for the event.
     */
    protected function init()
    {
        $this->data['objecttable'] = 'course';
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
    }

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name()
    {
        return get_string('event_coursemodulerestored', 'local_ubion');
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'course', 'restore' => 'course');
    }
}

