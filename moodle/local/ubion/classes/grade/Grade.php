<?php
namespace local_ubion\grade;

use local_ubion\core\grade\AbstractGrade;

class Grade extends AbstractGrade
{

    private static $instance;

    /**
     * 강좌 Class
     *
     * @return Grade
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c();
        }

        return self::$instance;
    }
}