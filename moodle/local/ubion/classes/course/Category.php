<?php
namespace local_ubion\course;

use local_ubion\core\course\AbstractCategory;

class Category extends AbstractCategory
{

    private static $instance;

    /**
     * Course Category Class
     *
     * @return Category
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c();
        }
        return self::$instance;
    }
}