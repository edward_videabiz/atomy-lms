<?php
namespace local_ubion\course;

use local_ubion\core\course\AbstractCourse;

class Course extends AbstractCourse
{

    private static $instance;

    /**
     * 강좌 Class
     *
     * @return Course
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c();
        }
        return self::$instance;
    }
}