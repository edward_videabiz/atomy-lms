<?php
namespace local_ubion\course;

class Controller extends \local_ubion\core\course\Controller
{

    private static $instance;

    /**
     * 강좌 Controller
     *
     * @return Controller
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c();
        }
        return self::$instance;
    }
}