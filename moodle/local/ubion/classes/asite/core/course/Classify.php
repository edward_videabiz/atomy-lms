<?php
namespace local_ubion\asite\core\course;

use \local_ubion\controller\Controller;
use \local_ubion\base\Parameter;
use \local_ubion\base\Javascript;
use \local_ubion\base\Common;
use \local_ubion\course\Course;
use local_ubion\core\traits\TraitClassify;

class Classify extends Controller
{
    use TraitClassify;
    
    public function getCourseTypes()
    {
        // 필요시 항목을 더 추가해주시면 됩니다.
        return [
            Course::TYPE_IRREGULAR => get_string('course_type_'.strtolower(Course::TYPE_IRREGULAR), 'local_ubion')
            ,Course::TYPE_ECLASS => get_string('course_type_'.strtolower(Course::TYPE_ECLASS), 'local_ubion')
            ,Course::TYPE_MIS => get_string('course_type_'.strtolower(Course::TYPE_MIS), 'local_ubion')
        ];
    }
    
    protected function getDefaultClassifyName()
    {
        return '기타';
    }
    
    public function checkDefaultClassify($courseTypes = null)
    {
        if (empty($courseTypes)) {
            $courseTypes = $this->getCourseTypes();
        }
        
        // 정의된 강좌 타입을 반복돌면서 기본항목이 추가되어있는지 확인해봐야됨.
        foreach ($courseTypes as $ctKey => $ctText) {
            $defaultClassify = $this->getDefault($ctKey);
            
            // 기본항목이 없다면 기본 항목 추가해주기
            if (empty($defaultClassify)) {
                
                // 언어 명칭 serialize 시켜서 저장시켜야됨.
                $langValue = array();
                if ($langs = Common::getLangs()) {
                    foreach ($langs as $l) {
                        // 한국어는 필수이기 떄문에 강제로 할당
                        if ($l == 'ko') {
                            $langValue[$l] = $this->getDefaultClassifyName();
                        } else {
                            $langValue[$l] = '';
                        }
                        
                    }
                }
                
                $langValue = serialize($langValue);
                
                $this->setInsert($ctKey, $langValue, 1, 1);
            }
        }
    }
    
    public function doInsert()
    {
        // ko는 필수
        // 나머지는 선택사항이기 때문에 따로 유효성 검사 진행하지 않음.
        $langKO = Parameter::request('lang-ko', null, PARAM_NOTAGS);
        $courseType = Parameter::post('course_type', null, PARAM_NOTAGS);
        $visible = Parameter::post('visible', 0, PARAM_INT);
        
        
        $validate = $this->validate()->make(array(
            'course_type' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => '강좌 분류'
                ,$this->validate()::PARAMVALUE => $courseType
            )
            ,'lang-ko' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => '한글명'
                ,$this->validate()::PARAMVALUE => $langKO
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            
            // 정의되어 있지 않는 강좌 분류가 전달될수 있기 때문에 확인해봐야됨.
            $courseTypes = $this->getCourseTypes();
            
            if (!isset($courseTypes[$courseType])) {
                Javascript::printAlert('정의되어 있지 않는 강좌 분류가 전달되었습니다. ['.$courseType.']');
            }
            
            // 언어 명칭 serialize 시켜서 저장시켜야됨.
            $langValue = array();
            if ($langs = Common::getLangs()) {
                foreach ($langs as $l) {
                    $langValue[$l] = Parameter::request('lang-'.$l, null, PARAM_NOTAGS);
                }
            }
            
            $langValue = serialize($langValue);
            
            $this->setInsert($courseType, $langValue, $visible);
            
            
            $returnurl = \local_ubion\asite\Asite::getInstance()->sGetUrl('', 'mod', 'site/classify');
            redirect($returnurl);
        }
    }
    
    
    public function setInsert($courseType, $name, $visible, $basic=0, $sortorder = 1000)
    {
        global $DB;
        
        $classify = new \stdClass();
        $classify->course_type = $courseType;
        $classify->value = $name;
        $classify->visible = $visible;
        $classify->basic = $basic;
        $classify->sortorder = $sortorder;
        $classify->timecreated = time();
        $classify->id = $DB->insert_record('local_ubion_classify', $classify);
        
        return $classify;
    }
    
    
    public function doUpdate()
    {
        // ko는 필수
        // 나머지는 선택사항이기 때문에 따로 유효성 검사 진행하지 않음.
        $id = Parameter::request('id', null, PARAM_INT);
        $langKO = Parameter::request('lang-ko', null, PARAM_NOTAGS);
        $visible = Parameter::post('visible', 0, PARAM_INT);
        
        
        $validate = $this->validate()->make(array(
            'id' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => '분류 고유번호'
                ,$this->validate()::PARAMVALUE => $id
            )
            
            ,'lang-ko' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => '한글명'
                ,$this->validate()::PARAMVALUE => $langKO
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            if ($oldData = $this->getView($id)) {
                // 언어 명칭 serialize 시켜서 저장시켜야됨.
                $langValue = array();
                if ($langs = Common::getLangs()) {
                    foreach ($langs as $l) {
                        $langValue[$l] = Parameter::request('lang-'.$l, null, PARAM_NOTAGS);
                    }
                }
                
                $langValue = serialize($langValue);
                $this->setUpdate($id, $langValue, $visible);
                
                $returnurl = \local_ubion\asite\Asite::getInstance()->sGetUrl('', 'mod', 'site/classify');
                redirect($returnurl);
            } else {
                Javascript::printJSON(array('code'=>Javascript::getFailureCode(), 'msg'=>'등록된 분류 정보가 없습니다. id : '.$id));
            }
        }
    }
    
    
    public function setUpdate($id, $name, $visible)
    {
        global $DB;
        
        $classify = new \stdClass();
        $classify->id = $id;
        $classify->value = $name;
        $classify->visible = $visible;
        $classify->timemodified = time();
        
        $DB->update_record('local_ubion_classify', $classify);
    }
    
    
    public function doView()
    {
        $id = Parameter::request('id', 0, PARAM_INT);
        
        $result = null;
        if (!empty($id)) {
            $result = $this->getView($id);
        }
        
        if (Parameter::isAajx()) {
            Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'msg'=>get_string('view_complete', 'local_ubion'), 'result'=>$result));
        } else {
            return $result;
        }
    }
    
    public function getView($id)
    {
        global $DB;
        
        if ($result = $DB->get_record_sql("SELECT * FROM {local_ubion_classify} WHERE id = :id", array('id' => $id))) {
            // serialize된값을 풀어서 전달해줌
            if (!empty($result)) {
                $result->valueUnserialize = unserialize($result->value);
            }
        }
        
        return $result;
    }
    
    public function getDefault($coursetype)
    {
        global $DB;
        
        return $DB->get_record_sql("SELECT * FROM {local_ubion_classify} WHERE course_type = :course_type AND basic = 1", array('course_type' => $coursetype));
    }
    
    
    /**
     * 패밀리 사이트 삭제(post)
     *
     * @return mixed
     */
    function doDelete()
    {
        global $DB;
        
        $id = Parameter::request('id', null, PARAM_INT);
        
        // 패밀리 사이트가 존재하는지 확인
        if ($classify = $this->getView($id)) {
            
            // 기본 분류는 삭제되면 안됨.
            if ($classify->basic) {
                Javascript::printAlert(get_string('classify_not_delete', 'local_ubion'));
            } else {
                
                // 패밀리 사이트 삭제
                list ($isSuccess, $message) = $this->setDelete($classify);
                
                if ($isSuccess) {
                    $code = Javascript::getSuccessCode();
                    $message = get_string('delete_complete', 'local_ubion');
                } else {
                    $code = Javascript::getFailureCode();
                }
                
                return Javascript::printJSON(array('code'=>$code, 'msg'=>$message));
            }
        } else {
            Javascript::printJSON(array('code'=>Javascript::getFailureCode(), 'msg'=>'등록된 분류 정보가 없습니다. id : '.$id));
        }
    }
    
    
    
    
    /**
     * 패밀리 사이트 삭제
     *
     * @param number $id
     */
    function setDelete($classifyorid)
    {
        global $DB;
        
        $isSuccess = true;
        $message = null;
        
        if (is_number($classifyorid)) {
            $classify = $this->getView($classifyorid);
        } else {
            $classify = $classifyorid;
        }
        
        
        // 기본 분류이면 pass
        if (empty($classify->basic)) {
            // 해당 레코드 삭제 후 강좌에 속한 강좌들 기본 분류로 이동
            if ($default = $this->getDefault($classify->course_type)) {
                
                $query = "UPDATE    {course_ubion} SET
                                    course_gubun_code = :new_course_gubun_code
                          WHERE     course_type = :course_type
                          AND       course_gubun_code = :old_course_gubun_code";
                
                $param = array(
                    'course_type' => $classify->course_type
                    ,'new_course_gubun_code' => $default->id
                    ,'old_course_gubun_code' => $classify->id
                );
                
                $DB->execute($query, $param);
                
                $DB->delete_records('local_ubion_classify', array('id' => $classify->id));
                
            } else {
                $isSuccess = false;
                $message = get_string('classify_notfound_default', 'local_ubion');
            }
        } else {
            $isSuccess = false;
            $message = get_string('classify_not_delete', 'local_ubion');
        }
        
        
        return array($isSuccess, $message);
    }
    
    
    public function doCheckDelete()
    {
        $selectItems = Parameter::requestArray('clsf', array(), PARAM_INT);
        
        if (!empty($selectItems)) {
            foreach ($selectItems as $si) {
                $this->setDelete($si);
            }
        }
        
        $returnurl = \local_ubion\asite\Asite::getInstance()->sGetUrl('', 'mod', 'site/classify');
        redirect($returnurl);
    }
    
    
    /**
     * 메뉴 순서 변경 (post)
     */
    function doSortorder()
    {
        global $DB;
        
        
        $json = Parameter::request('json', null, PARAM_NOTAGS);
        
        $validate = $this->validate()->make(array(
            'json' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => '정렬순서값'
                ,$this->validate()::PARAMVALUE => $json
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $json = json_decode($json);
            
            $order = 1;
            foreach ($json as $clsf) {
                $update = new \stdClass();
                $update->id = $clsf->id;
                $update->sortorder = $order;
                $DB->update_record('local_ubion_classify', $update);
                
                $order++;
            }
            
            Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'msg'=>get_string('save_complete', 'local_ubion')));
        }
    }
}