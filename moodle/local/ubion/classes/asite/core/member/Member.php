<?php
namespace local_ubion\asite\core\member;

use context_course;
use context_system;
use moodle_exception;
use stdClass;
use Exception;
use core_user;
use core_text;
use moodle_url;
use html_writer;
use context_user;
use \local_ubion\base\Parameter;
use \local_ubion\base\Javascript;
use \local_ubion\controller\Controller;

class Member extends Controller 
{
    use \local_ubion\core\traits\TraitDB;
    
    /**
     * 코스모스에 등록되어 있는 사용자의 소속 목록
     * @return array
     */
    public function getInstitutions()
    {
        global $DB;
        
        $query = "SELECT
                            uu.institution_code
                            ,u.institution
                FROM        {user} u
                JOIN        {user_ubion} uu ON u.id = uu.userid
                WHERE       u.deleted = 0
                GROUP BY    uu.institution_code, u.institution
                ORDER BY    u.institution";
        
        return $DB->get_records_sql($query);
    }
    
    
    /**
     * 코스모스에 등록되어 있는 사용자의 하위 부서 목록
     * 
     * @param string $institutionCode
     * @return array
     */
    public function getDepartments($institutionCode)
    {
        global $DB;
        
        $query = "SELECT
                            uu.dept_code
                            ,u.department
                FROM        {user} u
                JOIN        {user_ubion} uu ON u.id = uu.userid
                WHERE       u.deleted = 0
                AND         uu.institution_code = :institution_code
                GROUP BY    uu.dept_code, u.department
                ORDER BY    uu.dept_code";
        
        $param = array('institution_code' => $institutionCode);
        
        return $DB->get_records_sql($query, $param);
    }
    
    /**
     * 하위 부서 목록 가져오기
     * @return array
     */
    public function doDepartments()
    {
        $institutionCode = Parameter::post('institution', null, PARAM_NOTAGS);
        
        /* 부서코드가 빈값인 경우가 있기 때문에 유효성 검사를 진행하면 안됨.
        $validate = $this->validate()->make(array(
           'institution' => array(
               'required'
               ,$this->validate()::PLACEHOLDER => '소속'
               ,$this->validate()::PARAMVALUE => $institutionCode
           ) 
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
        */  
            $depts = $this->getDepartments($institutionCode);
            
            if (Parameter::isAajx()) {
                Javascript::printJSON(array('code' => Javascript::getSuccessCode(), 'msg' => get_string('view_complete', 'local_ubion'), 'depts' => $depts));
            } else {
                return $depts;
            }
        //}
    }
    
    
    /**
     * 코스모스에 등록되어 있는 회원 타입
     * 
     * @return array
     */
    public function getUserTypes()
    {
        global $DB;
        
        $query = "SELECT
                            uu.user_type
				            ,uu.user_type_name
                FROM        {user} u
                JOIN        {user_ubion} uu ON u.id = uu.userid
                WHERE       u.deleted = 0
                GROUP BY    uu.user_type, uu.user_type_name
                ORDER BY    uu.user_type";
        
        return $DB->get_records_sql($query);
    }
    
    /**
     * 학교(기관)에서 사용되는 회원 타입
     * @return array
     */
    public function getUnivUserTypes()
    {
        global $DB;
        
        $query = "SELECT
                            uu.univ_user_type
				            ,uu.univ_user_type_name
                FROM        {user} u
                JOIN        {user_ubion} uu ON u.id = uu.userid
                WHERE       u.deleted = 0
                AND         u.auth = :auth
                GROUP BY    uu.univ_user_type, uu.univ_user_type_name
                ORDER BY    uu.univ_user_type";
        
        $param = array('auth' => get_config('local_ubion', 'haksa_auth'));
        
        return $DB->get_records_sql($query, $param);
    }
    
    
    /**
     * 코스모스에 등록된 모든 회원을 리턴해줍니다.
     * 
     * @param string $keyfield
     * @param string $keyword
     * @param string $auth
     * @param string $institutionCode
     * @param string $departmentCode
     * @param string $userType
     * @param string $univUserType
     * @param number $page
     * @param number $ls
     * @return array
     */
    public function getMembers($keyfield, $keyword, $auth=null, $institutionCode=null, $departmentCode=null, $userType=null, $univUserType=null, $page=1, $ls=15)
    {
        global $CFG, $DB;
        
        $query = "SELECT
                            ".self::REPLACE."
                FROM        {user} u
                LEFT JOIN   {user_ubion} uu ON u.id = uu.userid
                WHERE       u.deleted = 0
                AND         u.id > :siteguestid";
        
        $param = [
            'siteguestid' => $CFG->siteguest
        ];
        
        // 검색
        if (!empty($keyfield) && !empty($keyword)) {
            if ($keyfield == 'fullname') {
                $fullname = $DB->sql_fullname("u.firstname", "u.lastname");
                $query .= " AND ".$DB->sql_like($fullname, ':keyword');
                $param['keyword'] = $keyword.'%';
            } else {
                $query .= " AND mnethostid = :mnethostid AND ".$DB->sql_like('u.username', ':username');
                
                $param['mnethostid'] = $CFG->mnet_localhost_id;
                $param['username'] = $keyword.'%';
            }
        }
        
        // 인증타입
        if (!empty($auth)) {
            $query .= " AND u.auth = :auth";
            $param['auth'] = $auth;
        }
        
        // 소속
        if ($institutionCode != Parameter::getOptionAllValue()) {
            $query .= " AND uu.institution_code = :institution_code";
            $param['institution_code'] = $institutionCode;
        }
        
        // 부석
        if ($departmentCode != Parameter::getOptionAllValue()) {
            $query .= " AND uu.dept_code = :dept_code";
            $param['dept_code'] = $departmentCode;
        }
        
        // 코스모스 신분타입
        if ($userType != Parameter::getOptionAllValue()) {
            $query .= " AND uu.user_type = :user_type";
            $param['user_type'] = $userType;
        }
        
        // 학사 신분타입
        if ($univUserType != Parameter::getOptionAllValue()) {
            $query .= " AND uu.univ_user_type = :univ_user_type";
            $param['univ_user_type'] = $univUserType;
        }
        
        $totalQuery = str_replace(self::REPLACE, 'COUNT(1)', $query);
        $totalCount = $DB->get_field_sql($totalQuery, $param);
        

        $usernamefield = get_all_user_name_fields(true, 'u');
        $field = "uu.*
                  ,u.id
                  ,u.auth
                  ,u.username
                  ,u.idnumber
                  ,".$usernamefield."
                  ,u.institution
                  ,u.department
                  ,u.phone1
                  ,u.phone2
                  ,u.email";
        
        $query = str_replace(self::REPLACE, $field, $query);
        $query .= " ORDER BY u.id DESC";
        $limitfrom = ($page > 0 ) ?  $ls * ($page - 1) : 0;
        $lists = $DB->get_records_sql($query, $param, $limitfrom, $ls);
        
        //error_log($query);
        //error_log(print_r($param, true));
        
        return array($totalCount, $lists);
    }
    
    
    /**
     * 사용자 상세 정보 
     * 
     * @return string
     */
    public function doUserDetail()
    {
        global $DB;
        
        $id = Parameter::post('id', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'id' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => '사용자 ID'
                ,$this->validate()::PARAMVALUE => $id
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            if ($user = $DB->get_record_sql("SELECT * FROM {user} WHERE id = :id", array('id' => $id))) {
            
                if ($user->deleted) {
                    Javascript::printAlert('삭제된 회원입니다.');
                } else {
                    
                    $userUbion = $DB->get_record_sql("SELECT * FROM {user_ubion} WHERE userid = :userid", array('userid' => $id));
                    
                    $html = '<div class="row">';
                    $html .=    '<div class="col-xs-12 col-md-6">';
                    $html .=        '<h4>user</h4>';
                    $html .=        '<dl class="dl-horizontal">';
                    foreach ($user as $key => $value) {
                        // 비밀번호는 표시할 필요 없음
                        if ($key == 'password') continue;
                        
                        $html .=        '<dt>'.$key.'</dt>';
                        $html .=        '<dd class="mb-2">'.$value.'</dd>';
                    }
                    $html .=        '</dl>';
                    $html .=    '</div>';
                    $html .=    '<div class="col-xs-12 col-md-6">';
                    $html .=        '<h4>user ubion</h4>';
                    if (empty($userUbion)) {
                        $html .=    'NULL';
                    } else {
                        $html .=    '<dl class="dl-horizontal">';
                        foreach ($userUbion as $key => $value) {
                            $html .=    '<dt>'.$key.'</dt>';
                            $html .=    '<dd class="mb-2">'.$value.'</dd>';
                        }
                        $html .=    '</dl>';
                    }
                    $html .=    '</div>';
                    $html .= '</div>';
                    
                    if (Parameter::isAajx()) {
                        $fullname = $user->firstname;
                        Javascript::printJSON(array('code' => Javascript::getSuccessCode(), 'msg' => get_string('view_complete', 'local_ubion'), 'html' => $html, 'fullname' => $fullname));
                    } else {
                        return $html;
                    }
                }
            } else {
                Javascript::printAlert('존재하지 않는 회원입니다. ['.$id.']');
            }
        }
    }
    
    
    /**
     * 사용자가 코스모스 내에 수강중인 강좌 목록
     * @return string
     * 
     * @deprecated 새창으로 변경 해서 삭제될 예정인 함수입니다. => 사용자 피드백 전달 받은 후 최종 여부 결정
    public function doUserEnrol()
    {
        global $CFG, $DB;
        
        $id = Parameter::post('id', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'id' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => '사용자 ID'
                ,$this->validate()::PARAMVALUE => $id
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            if ($user = $DB->get_record_sql("SELECT * FROM {user} WHERE id = :id", array('id' => $id))) {
                
                if ($user->deleted) {
                    Javascript::printAlert('삭제된 회원입니다.');
                } else {
                    $html = '';
                    $userEnrol = array();
                    
                    if ($haksas = $DB->get_records_sql('SELECT id, year, semester_code, semester_name FROM {local_ubion_haksa} ORDER BY year, semester_code')) {
                        $CCourse = \local_ubion\course\Course::getInstance();
                        $CRegular = \local_ubion\course\Regular::getInstance();
                        
                        foreach ($haksas as $h) {
                            
                            if ($courses = $CRegular->getUserCourses($h->year, $h->semester_code, $user->id)) {
                            
                                $html .= '<fieldset class="">';
                                $html .=    '<legend>'.$h->year.'년 '.$h->semester_name.'</legend>';
                                $html .=    '<table class="table table-bordered table-coursemos">';
                                $html .=        '<colgroup>';
                                $html .=            '<col class="wp-80" />';
                                $html .=            '<col />';
                                $html .=        '</colgroup>';
                                $html .=        '<thead>';
                                $html .=            '<tr>';
                                $html .=                '<th>번호</th>';
                                $html .=                '<th>강좌명</th>';
                                $html .=            '</tr>';
                                $html .=        '</thead>';
                                $html .=        '<tbody>';
                                
                                $number = 1;
                                foreach ($courses as $c) {
                                    
                                    $courseLink = $CFG->wwwroot.'/course/view.php?id='.$c->id;
                                    $html .=         '<tr>';
                                    $html .=             '<td class="text-center">'.$number.'</td>';
                                    $html .=             '<td>';
                                    $html .=                '<a href="'.$courseLink.'" target="_blank">'.$CCourse->getName($c).'</a>';
                                    
                                    $addLabel = '';
                                    if (empty($c->visible)) {
                                        $addLabel .= '<label class="label label-danger mx-2">폐강</label>';
                                    }
                                    if (empty($c->cu_visible)) {
                                        $addLabel .= '<label class="label label-danger mx-2">미사용</label>';
                                    }
                                    
                                    $html .=                $addLabel;
                                    $html .=             '</td>';
                                    $html .=         '</tr>';
                                    
                                    $number++;
                                }
                                $html .=        '</tbody>';
                                $html .=    '</table>';
                                $html .= '</fieldset>';
                            }
                        }
                    }
                    
                    if (empty($html)) {
                        $html = '<span class="text-error">수강중인 강좌가 없습니다.</span>';
                    }
                    
                    if (Parameter::isAajx()) {
                        $fullname = $user->firstname;
                        
                        Javascript::printJSON(array('code' => Javascript::getSuccessCode(), 'msg' => get_string('view_complete', 'local_ubion'), 'html' => $html, 'fullname' => $fullname));
                    } else {
                        return $html;
                    }
                }
            } else {
                Javascript::printAlert('존재하지 않는 회원입니다. ['.$id.']');
            }
        }
    }
    */
    
    /**
     * 사용자 업로드시 사용가능한 컬럼명
     * @return array
     */
    public function getUploadAllowColumns()
    {
        // 해당 항목은 /admin/tool/uploaduser/index.php 파일 내용 발췌한 것입니다.
        
        // array of all valid fields for validation
        $STD_FIELDS = array('id', 'username', 'email',
            'city', 'country', 'lang', 'timezone', 'mailformat',
            'maildisplay', 'maildigest', 'htmleditor', 'autosubscribe',
            'institution', 'department', 'idnumber', 'skype',
            'msn', 'aim', 'yahoo', 'icq', 'phone1', 'phone2', 'address',
            'url', 'description', 'descriptionformat', 'password',
            'auth',        // watch out when changing auth type or using external auth plugins!
            'oldusername', // use when renaming users - this is the original username
            'suspended',   // 1 means suspend user account, 0 means activate user account, nothing means keep as is for existing users
            'deleted',     // 1 means delete user
            'mnethostid',  // Can not be used for adding, updating or deleting of users - only for enrolments, groups, cohorts and suspending.
            'interests',
        );
        // Include all name fields.
        $STD_FIELDS = array_merge($STD_FIELDS, get_all_user_name_fields());
        
        
        // 코스모스에서는 user_info_field를 사용하지 않기 때문에 따로 검사하지 않습니다.
        /*
        $PRF_FIELDS = array();
        
        if ($proffields = $DB->get_records('user_info_field')) {
            foreach ($proffields as $key => $proffield) {
                $profilefieldname = 'profile_field_'.$proffield->shortname;
                $PRF_FIELDS[] = $profilefieldname;
                // Re-index $proffields with key as shortname. This will be
                // used while checking if profile data is key and needs to be converted (eg. menu profile field)
                $proffields[$profilefieldname] = $proffield;
                unset($proffields[$key]);
            }
        }
        */
        
        return $STD_FIELDS;
    }
    
    public function getUploadUbionColumnPrefix()
    {
        return 'userubion-';
    }
    
    public function getUploadAllowColumnsUbion()
    {
        global $CFG, $DB;
        
        $query = "SELECT
                            column_name
                FROM        INFORMATION_SCHEMA.COLUMNS
                WHERE       TABLE_SCHEMA = :table_schema
                AND         TABLE_NAME = :table_name";

        $param = array('table_schema'=>$CFG->dbname, 'table_name' => $CFG->prefix.'user_ubion');
        
        // 변경되면 안되는 컬럼 목록
        $nowAllowColumnName = array('id', 'userid');
        
        $fields = array();
        if ($columns = $DB->get_records_sql($query, $param)) {
            foreach ($columns as $cs) {
                if (!in_array($cs->column_name, $nowAllowColumnName)) {
                    $fields[] = $this->getUploadUbionColumnPrefix().$cs->column_name;
                }
            }
        }
        
        return $fields;
    }
    
    
    /**
     * 전달된 컬럼 중에 허용되지 않는 컬럼명이 전달되었는지 확인
     * 
     * /admin/tool/uploaduser/locallib.php => uu_validate_user_upload_columns()
     *  
     * @param array $excelRow
     * @param array $fields
     * @param string $returnurl
     * @return array
     */
    public function getUploadColumnValidate($excelRow, $fields, $returnurl)
    {
        // test columns
        $processed = array();
        
        
        foreach ($excelRow as $key => $column) {
            if (!empty($column)) {
                $lccolumn = \core_text::strtolower($column);
                
                // 사용할수 있는 컬럼 목록에 존재하는 값인지 확인.
                if (in_array($column, $fields) or in_array($lccolumn, $fields)) {
                    // standard fields are only lowercase
                    $newcolumn = $lccolumn;
                } else if (preg_match('/^(sysrole|cohort|course|group|type|role|enrolperiod|enrolstatus)\d+$/', $lccolumn)) {
                    $newcolumn = $lccolumn;
                } else {
                    print_error('invalidfieldname', 'error', $returnurl, $column);
                }
                
                // 중복된 컬럼명이 존재하는지 확인.
                if (in_array($newcolumn, $processed)) {
                    print_error('duplicatefieldname', 'error', $returnurl, $newcolumn);
                }
                $processed[$key] = $newcolumn;
            }
        }
        
        return $processed;
    }
    
    
    /**
     * 사용자 일괄 업로드
     * 
     * /admin/tool/uploaduser/index.php 파일 내용 중 
     * 
     * } else if ($formdata = $mform2->get_data()) {
     *   ...
     * }
     * 
     * 항목과 거의 일치 합니다. (일부분만 변경처리함)
     * 
     * @param object $formdata
     * @param string $filename
     * @param array $filecolumns
     */
    public function setUploadUser($formdata, $filename, $filecolumns, $returnurl)
    {
        global $CFG, $DB, $OUTPUT;
        
        require_once($CFG->dirroot.'/user/lib.php');
        require_once($CFG->dirroot.'/group/lib.php');
        require_once($CFG->dirroot.'/cohort/lib.php');
        require_once $CFG->libdir.'/phpexcel/PHPExcel.php';
        require_once $CFG->dirroot.'/admin/tool/uploaduser/locallib.php';
        
        $struserrenamed             = get_string('userrenamed', 'tool_uploaduser');
        $strusernotrenamedexists    = get_string('usernotrenamedexists', 'error');
        $strusernotrenamedmissing   = get_string('usernotrenamedmissing', 'error');
        $strusernotrenamedoff       = get_string('usernotrenamedoff', 'error');
        $strusernotrenamedadmin     = get_string('usernotrenamedadmin', 'error');
        
        $struserupdated             = get_string('useraccountupdated', 'tool_uploaduser');
        $strusernotupdated          = get_string('usernotupdatederror', 'error');
        $strusernotupdatednotexists = get_string('usernotupdatednotexists', 'error');
        $strusernotupdatedadmin     = get_string('usernotupdatedadmin', 'error');
        
        $struseruptodate            = get_string('useraccountuptodate', 'tool_uploaduser');
        
        $struseradded               = get_string('newuser');
        $strusernotadded            = get_string('usernotaddedregistered', 'error');
        $strusernotaddederror       = get_string('usernotaddederror', 'error');
        
        $struserdeleted             = get_string('userdeleted', 'tool_uploaduser');
        $strusernotdeletederror     = get_string('usernotdeletederror', 'error');
        $strusernotdeletedmissing   = get_string('usernotdeletedmissing', 'error');
        $strusernotdeletedoff       = get_string('usernotdeletedoff', 'error');
        $strusernotdeletedadmin     = get_string('usernotdeletedadmin', 'error');
        
        $strcannotassignrole        = get_string('cannotassignrole', 'error');
        
        $struserauthunsupported     = get_string('userauthunsupported', 'error');
        $stremailduplicate          = get_string('useremailduplicate', 'error');
        
        $strinvalidpasswordpolicy   = get_string('invalidpasswordpolicy', 'error');
        $errorstr                   = get_string('error');
        
        $stryes                     = get_string('yes');
        $strno                      = get_string('no');
        $stryesnooptions = array(0=>$strno, 1=>$stryes);
        
        
        echo $OUTPUT->header();
        
        
        $optype = $formdata->uutype;
        $bulk = $formdata->uubulk;
        
        $updatetype        = isset($formdata->uuupdatetype) ? $formdata->uuupdatetype : 0;
        $createpasswords   = (!empty($formdata->uupasswordnew) and $optype != UU_USER_UPDATE);
        $updatepasswords   = (!empty($formdata->uupasswordold)  and $optype != UU_USER_ADDNEW and $optype != UU_USER_ADDINC and ($updatetype == UU_UPDATE_FILEOVERRIDE or $updatetype == UU_UPDATE_ALLOVERRIDE));
        $allowrenames      = (!empty($formdata->uuallowrenames) and $optype != UU_USER_ADDNEW and $optype != UU_USER_ADDINC);
        $allowdeletes      = (!empty($formdata->uuallowdeletes) and $optype != UU_USER_ADDNEW and $optype != UU_USER_ADDINC);
        $allowsuspends     = (!empty($formdata->uuallowsuspends));
        $bulk              = $formdata->uubulk;
        $noemailduplicates = empty($CFG->allowaccountssameemail) ? 1 : $formdata->uunoemailduplicates;
        $standardusernames = $formdata->uustandardusernames;
        $resetpasswords    = isset($formdata->uuforcepasswordchange) ? $formdata->uuforcepasswordchange : UU_PWRESET_NONE;
        
        // verification moved to two places: after upload and into form2
        $usersnew      = 0;
        $usersupdated  = 0;
        $usersuptodate = 0; //not printed yet anywhere
        $userserrors   = 0;
        $deletes       = 0;
        $deleteerrors  = 0;
        $renames       = 0;
        $renameerrors  = 0;
        $usersskipped  = 0;
        $weakpasswords = 0;
        
        // caches
        $ccache         = array(); // course cache - do not fetch all courses here, we  will not probably use them all anyway!
        $cohorts        = array();
        $rolecache      = uu_allowed_roles_cache(); // Course roles lookup cache.
        $sysrolecache   = uu_allowed_sysroles_cache(); // System roles lookup cache.
        $manualcache    = array(); // cache of used manual enrol plugins in each course
        $supportedauths = uu_supported_auths(); // officially supported plugins that are enabled
        
        // we use only manual enrol plugin here, if it is disabled no enrol is done
        if (enrol_is_enabled('manual')) {
            $manual = enrol_get_plugin('manual');
        } else {
            $manual = NULL;
        }
        
        // clear bulk selection
        if ($bulk) {
            $SESSION->bulk_users = array();
        }
        
        
        $excel = \PHPExcel_IOFactory::load($filename);
        $excelSheet = $excel->getActiveSheet()->toArray(null,false,false,false);
        
        $linenum = 1; //column header is first line
        
        // init upload progress tracker
        $upt = new \uu_progress_tracker();
        $upt->start(); // start table
        
        $userUbionColumnPrefix = $this->getUploadUbionColumnPrefix();
        foreach ($excelSheet as $index => $line) {
            if ($index > 0) {
                $upt->flush();
                $upt->track('line', $index);
                
                
                $user = new stdClass();
                $userUbion = new stdClass();
                
                
                // add fields to user object
                foreach ($line as $keynum => $value) {
                    // filecolumns에 존재하는 컬럼만 추가해줘야됨.
                    if (!isset($filecolumns[$keynum])) {
                        // this should not happen
                        continue;
                    }
                    
                    $key = $filecolumns[$keynum];
                    /* profile_field_는 사용하지 않기 때문에 주석 처리함. akddd
                     if (strpos($key, 'profile_field_') === 0) {
                     //NOTE: bloody mega hack alert!!
                     if (isset($USER->$key) and is_array($USER->$key)) {
                     // this must be some hacky field that is abusing arrays to store content and format
                     $user->$key = array();
                     $user->{$key['text']}   = $value;
                     $user->{$key['format']} = FORMAT_MOODLE;
                     } else {
                     $user->$key = trim($value);
                     }
                     }
                     */
                    if (strpos($key, $this->getUploadUbionColumnPrefix()) === 0) {
                        $userUbionKey = str_replace($this->getUploadUbionColumnPrefix(), '', $key);
                        $userUbion->$userUbionKey = trim($value);
                    } else {
                        $user->$key = trim($value);
                    }
                    
                    if (in_array($key, $upt->columns)) {
                        // default value in progress tracking table, can be changed later
                        $upt->track($key, s($value), 'normal');
                    }
                }
                
                // 사용자 이름이 없으면 빈값으로라도 처리해야됨..
                if (!isset($user->username)) {
                    // prevent warnings below
                    $user->username = '';
                }
                
                
                if ($optype == UU_USER_ADDNEW or $optype == UU_USER_ADDINC) {
                    // user creation is a special case - the username may be constructed from templates using firstname and lastname
                    // better never try this in mixed update types
                    $error = false;
                    if (!isset($user->firstname) or $user->firstname === '') {
                        $upt->track('status', get_string('missingfield', 'error', 'firstname'), 'error');
                        $upt->track('firstname', $errorstr, 'error');
                        $error = true;
                    }
                    if (!isset($user->lastname) or $user->lastname === '') {
                        $upt->track('status', get_string('missingfield', 'error', 'lastname'), 'error');
                        $upt->track('lastname', $errorstr, 'error');
                        $error = true;
                    }
                    if ($error) {
                        $userserrors++;
                        continue;
                    }
                    // we require username too - we might use template for it though
                    if (empty($user->username) and !empty($formdata->username)) {
                        $user->username = uu_process_template($formdata->username, $user);
                        $upt->track('username', s($user->username));
                    }
                }
                
                
                // normalize username
                $originalusername = $user->username;
                if ($standardusernames) {
                    //$user->username = core_user::clean_field($user->username, 'username');
                    
                    // 2013-01-23 USERNAME에서 strtolower하기 때문에 대소문자가 안맞아서 오류가 발생함.. akddd
                    $user->username = clean_param($user->username, PARAM_NOTAGS);
                }
                
                // make sure we really have username
                if (empty($user->username)) {
                    $upt->track('status', get_string('missingfield', 'error', 'username'), 'error');
                    $upt->track('username', $errorstr, 'error');
                    $userserrors++;
                    continue;
                } else if ($user->username === 'guest') {
                    $upt->track('status', get_string('guestnoeditprofileother', 'error'), 'error');
                    $userserrors++;
                    continue;
                }
                
                /* 코스모스에서는 대소문자 구별하기 위해서 해당 내용 주석처리 akddd
                if ($user->username !== core_user::clean_field($user->username, 'username')) {
                    $upt->track('status', get_string('invalidusername', 'error', 'username'), 'error');
                    $upt->track('username', $errorstr, 'error');
                    $userserrors++;
                }
                */
                
                if (empty($user->mnethostid)) {
                    $user->mnethostid = $CFG->mnet_localhost_id;
                }
                
                if ($existinguser = $DB->get_record('user', array('username'=>$user->username, 'mnethostid'=>$user->mnethostid))) {
                    $upt->track('id', $existinguser->id, 'normal', false);
                }
                
                
                if ($user->mnethostid == $CFG->mnet_localhost_id) {
                    $remoteuser = false;
                    
                    // Find out if username incrementing required.
                    if ($existinguser and $optype == UU_USER_ADDINC) {
                        $user->username = uu_increment_username($user->username);
                        $existinguser = false;
                    }
                    
                } else {
                    if (!$existinguser or $optype == UU_USER_ADDINC) {
                        $upt->track('status', get_string('errormnetadd', 'tool_uploaduser'), 'error');
                        $userserrors++;
                        continue;
                    }
                    
                    $remoteuser = true;
                    
                    // Make sure there are no changes of existing fields except the suspended status.
                    foreach ((array)$existinguser as $k => $v) {
                        if ($k === 'suspended') {
                            continue;
                        }
                        if (property_exists($user, $k)) {
                            $user->$k = $v;
                        }
                        if (in_array($k, $upt->columns)) {
                            if ($k === 'password' or $k === 'oldusername' or $k === 'deleted') {
                                $upt->track($k, '', 'normal', false);
                            } else {
                                $upt->track($k, s($v), 'normal', false);
                            }
                        }
                    }
                    unset($user->oldusername);
                    unset($user->password);
                    $user->auth = $existinguser->auth;
                }
                
                
                // notify about nay username changes
                if ($originalusername !== $user->username) {
                    $upt->track('username', '', 'normal', false); // clear previous
                    $upt->track('username', s($originalusername).'-->'.s($user->username), 'info');
                } else {
                    $upt->track('username', s($user->username), 'normal', false);
                }
                
                // add default values for remaining fields
                $formdefaults = array();
                if (!$existinguser || ($updatetype != UU_UPDATE_FILEOVERRIDE && $updatetype != UU_UPDATE_NOCHANGES)) {
                    foreach ($filecolumns as $field) {
                        if (isset($user->$field)) {
                            continue;
                        }
                        // all validation moved to form2
                        if (isset($formdata->$field)) {
                            // process templates
                            $user->$field = uu_process_template($formdata->$field, $user);
                            $formdefaults[$field] = true;
                            if (in_array($field, $upt->columns)) {
                                $upt->track($field, s($user->$field), 'normal');
                            }
                        }
                    }
                    
                    // profile_field_는 사용되지 않기 때문에 주석처리 akddd
                    /*
                    foreach ($PRF_FIELDS as $field) {
                        if (isset($user->$field)) {
                            continue;
                        }
                        if (isset($formdata->$field)) {
                            // process templates
                            $user->$field = uu_process_template($formdata->$field, $user);
                            
                            // Form contains key and later code expects value.
                            // Convert key to value for required profile fields.
                            require_once($CFG->dirroot.'/user/profile/field/'.$proffields[$field]->datatype.'/field.class.php');
                            $profilefieldclass = 'profile_field_'.$proffields[$field]->datatype;
                            $profilefield = new $profilefieldclass($proffields[$field]->id);
                            if (method_exists($profilefield, 'convert_external_data')) {
                                $user->$field = $profilefield->edit_save_data_preprocess($user->$field, null);
                            }
                            
                            $formdefaults[$field] = true;
                        }
                    }
                    */   
                }
                
                
                // delete user
                if (!empty($user->deleted)) {
                    if (!$allowdeletes or $remoteuser) {
                        $usersskipped++;
                        $upt->track('status', $strusernotdeletedoff, 'warning');
                        continue;
                    }
                    if ($existinguser) {
                        if (is_siteadmin($existinguser->id)) {
                            $upt->track('status', $strusernotdeletedadmin, 'error');
                            $deleteerrors++;
                            continue;
                        }
                        if (delete_user($existinguser)) {
                            $upt->track('status', $struserdeleted);
                            $deletes++;
                        } else {
                            $upt->track('status', $strusernotdeletederror, 'error');
                            $deleteerrors++;
                        }
                    } else {
                        $upt->track('status', $strusernotdeletedmissing, 'error');
                        $deleteerrors++;
                    }
                    continue;
                }
                // we do not need the deleted flag anymore
                unset($user->deleted);
                
                
                // renaming requested?
                if (!empty($user->oldusername) ) {
                    if (!$allowrenames) {
                        $usersskipped++;
                        $upt->track('status', $strusernotrenamedoff, 'warning');
                        continue;
                    }
                    
                    if ($existinguser) {
                        $upt->track('status', $strusernotrenamedexists, 'error');
                        $renameerrors++;
                        continue;
                    }
                    
                    if ($user->username === 'guest') {
                        $upt->track('status', get_string('guestnoeditprofileother', 'error'), 'error');
                        $renameerrors++;
                        continue;
                    }
                    
                    if ($standardusernames) {
                        // $oldusername = \core_user::clean_field($user->oldusername, 'username');
                        // 2013-01-23 USERNAME에서 strtolower하기 때문에 대소문자가 안맞아서 오류가 발생함.. akddd
                        $oldusername = clean_param($user->oldusername, PARAM_NOTAGS);
                    } else {
                        $oldusername = $user->oldusername;
                    }
                    
                    // no guessing when looking for old username, it must be exact match
                    if ($olduser = $DB->get_record('user', array('username'=>$oldusername, 'mnethostid'=>$CFG->mnet_localhost_id))) {
                        $upt->track('id', $olduser->id, 'normal', false);
                        if (is_siteadmin($olduser->id)) {
                            $upt->track('status', $strusernotrenamedadmin, 'error');
                            $renameerrors++;
                            continue;
                        }
                        $DB->set_field('user', 'username', $user->username, array('id'=>$olduser->id));
                        $upt->track('username', '', 'normal', false); // clear previous
                        $upt->track('username', s($oldusername).'-->'.s($user->username), 'info');
                        $upt->track('status', $struserrenamed);
                        $renames++;
                    } else {
                        $upt->track('status', $strusernotrenamedmissing, 'error');
                        $renameerrors++;
                        continue;
                    }
                    $existinguser = $olduser;
                    $existinguser->username = $user->username;
                }
                
                
                
                // can we process with update or insert?
                $skip = false;
                switch ($optype) {
                    case UU_USER_ADDNEW:
                        if ($existinguser) {
                            $usersskipped++;
                            $upt->track('status', $strusernotadded, 'warning');
                            $skip = true;
                        }
                        break;
                        
                    case UU_USER_ADDINC:
                        if ($existinguser) {
                            //this should not happen!
                            $upt->track('status', $strusernotaddederror, 'error');
                            $userserrors++;
                            $skip = true;
                        }
                        break;
                        
                    case UU_USER_ADD_UPDATE:
                        break;
                        
                    case UU_USER_UPDATE:
                        if (!$existinguser) {
                            $usersskipped++;
                            $upt->track('status', $strusernotupdatednotexists, 'warning');
                            $skip = true;
                        }
                        break;
                        
                    default:
                        // unknown type
                        $skip = true;
                }
                
                if ($skip) {
                    continue;
                }
                
                
                
                if ($existinguser) {
                    $user->id = $existinguser->id;
                    
                    $upt->track('username', html_writer::link(new moodle_url('/user/profile.php', array('id'=>$existinguser->id)), s($existinguser->username)), 'normal', false);
                    $upt->track('suspended', $stryesnooptions[$existinguser->suspended] , 'normal', false);
                    $upt->track('auth', $existinguser->auth, 'normal', false);
                    
                    if (is_siteadmin($user->id)) {
                        $upt->track('status', $strusernotupdatedadmin, 'error');
                        $userserrors++;
                        continue;
                    }
                    
                    $existinguser->timemodified = time();
                    // do NOT mess with timecreated or firstaccess here!
                    
                    /* profile 기능 사용하지 않음. akddd
                    //load existing profile data
                    profile_load_data($existinguser);
                    */
                    
                    $doupdate = false;
                    $dologout = false;
                    
                    // 기존 사용자 세부사항을 변경 허용한 경우..
                    if ($updatetype != UU_UPDATE_NOCHANGES and !$remoteuser) {
                        if (!empty($user->auth) and $user->auth !== $existinguser->auth) {
                            $upt->track('auth', s($existinguser->auth).'-->'.s($user->auth), 'info', false);
                            $existinguser->auth = $user->auth;
                            if (!isset($supportedauths[$user->auth])) {
                                $upt->track('auth', $struserauthunsupported, 'warning');
                            }
                            $doupdate = true;
                            if ($existinguser->auth === 'nologin') {
                                $dologout = true;
                            }
                        }
                        // $PRF_FIELDS는 사용되지 않기 때문에 allcolumns는 곧 $filecolumns로 인식하면 됨.
                        // $allcolumns = array_merge($filecolumns, $PRF_FIELDS);
                        $allcolumns = $filecolumns;
                        foreach ($allcolumns as $column) {
                            // userUbion 관련 컴럼인지 확인 akddd
                            $isUserUbionColumn = (strpos($column, $userUbionColumnPrefix) === false) ? true : false;
                            
                            if ($column === 'username' or $column === 'password' or $column === 'auth' or $column === 'suspended') {
                                // these can not be changed here
                                continue;
                            }
                            
                            // userubion 항목인 경우에는 하단에서 따로 체크할 것임 akddd ($isUserUbionColumn && ) 조건문만 추가함.
                            if ($isUserUbionColumn && !property_exists($user, $column) or !property_exists($existinguser, $column)) {
                                continue;
                            }
                            if ($updatetype == UU_UPDATE_MISSING) {
                                if (!is_null($existinguser->$column) and $existinguser->$column !== '') {
                                    continue;
                                }
                            } else if ($updatetype == UU_UPDATE_ALLOVERRIDE) {
                                // we override everything
                                
                            } else if ($updatetype == UU_UPDATE_FILEOVERRIDE) {
                                if (!empty($formdefaults[$column])) {
                                    // do not override with form defaults
                                    continue;
                                }
                            }
                            
                            if ($existinguser->$column !== $user->$column) {
                                if ($column === 'email') {
                                    // 사용자 이미일 값을 @만 설정한 경우에는 따로 validate 검사를 진행하지 않음. akddd
                                    if ($user->email != '@') {
                                        $select = $DB->sql_like('email', ':email', false, true, false, '|');
                                        $params = array('email' => $DB->sql_like_escape($user->email, '|'));
                                        if ($DB->record_exists_select('user', $select , $params)) {
                                            
                                            $changeincase = core_text::strtolower($existinguser->$column) === core_text::strtolower(
                                                $user->$column);
                                            
                                            if ($changeincase) {
                                                // If only case is different then switch to lower case and carry on.
                                                $user->$column = core_text::strtolower($user->$column);
                                                continue;
                                            } else if ($noemailduplicates) {
                                                $upt->track('email', $stremailduplicate, 'error');
                                                $upt->track('status', $strusernotupdated, 'error');
                                                $userserrors++;
                                                continue 2;
                                            } else {
                                                $upt->track('email', $stremailduplicate, 'warning');
                                            }
                                        }
                                        if (!validate_email($user->email)) {
                                            $upt->track('email', get_string('invalidemail'), 'warning');
                                        }
                                    }
                                }
                                
                                if ($column === 'lang') {
                                    if (empty($user->lang)) {
                                        // Do not change to not-set value.
                                        continue;
                                    } else if (core_user::clean_field($user->lang, 'lang') === '') {
                                        $upt->track('status', get_string('cannotfindlang', 'error', $user->lang), 'warning');
                                        continue;
                                    }
                                }
                                
                                if (in_array($column, $upt->columns)) {
                                    $upt->track($column, s($existinguser->$column).'-->'.s($user->$column), 'info', false);
                                }
                                $existinguser->$column = $user->$column;
                                $doupdate = true;
                            }
                            
                            // userubion 관련 설정 akddd
                            if ($isUserUbionColumn) {
                                // 실제로 디비 컬럼이 존재하는지 확인해야됨.
                                if (in_array($column, $allcolumns)) {
                                    $doupdate = true;
                                }
                            }
                        }
                    }
                    
                    // 선택한 auth가 존재하는지 확인
                    try {
                        $auth = get_auth_plugin($existinguser->auth);
                    } catch (Exception $e) {
                        $upt->track('auth', get_string('userautherror', 'error', s($existinguser->auth)), 'error');
                        $upt->track('status', $strusernotupdated, 'error');
                        $userserrors++;
                        continue;
                    }
                    $isinternalauth = $auth->is_internal();
                    
                    // deal with suspending and activating of accounts
                    if ($allowsuspends and isset($user->suspended) and $user->suspended !== '') {
                        $user->suspended = $user->suspended ? 1 : 0;
                        if ($existinguser->suspended != $user->suspended) {
                            $upt->track('suspended', '', 'normal', false);
                            $upt->track('suspended', $stryesnooptions[$existinguser->suspended].'-->'.$stryesnooptions[$user->suspended], 'info', false);
                            $existinguser->suspended = $user->suspended;
                            $doupdate = true;
                            if ($existinguser->suspended) {
                                $dologout = true;
                            }
                        }
                    }
                    
                    // 패스워드 변경 로직..
                    // changing of passwords is a special case
                    // do not force password changes for external auth plugins!
                    $oldpw = $existinguser->password;
                    
                    if ($remoteuser) {
                        // Do not mess with passwords of remote users.
                        
                    } else if (!$isinternalauth) {
                        $existinguser->password = AUTH_PASSWORD_NOT_CACHED;
                        $upt->track('password', '-', 'normal', false);
                        // clean up prefs
                        unset_user_preference('create_password', $existinguser);
                        unset_user_preference('auth_forcepasswordchange', $existinguser);
                        
                    } else if (!empty($user->password)) {
                        if ($updatepasswords) {
                            // Check for passwords that we want to force users to reset next
                            // time they log in.
                            $errmsg = null;
                            $weak = !check_password_policy($user->password, $errmsg);
                            if ($resetpasswords == UU_PWRESET_ALL or ($resetpasswords == UU_PWRESET_WEAK and $weak)) {
                                if ($weak) {
                                    $weakpasswords++;
                                    $upt->track('password', $strinvalidpasswordpolicy, 'warning');
                                }
                                set_user_preference('auth_forcepasswordchange', 1, $existinguser);
                            } else {
                                unset_user_preference('auth_forcepasswordchange', $existinguser);
                            }
                            unset_user_preference('create_password', $existinguser); // no need to create password any more
                            
                            // Use a low cost factor when generating bcrypt hash otherwise
                            // hashing would be slow when uploading lots of users. Hashes
                            // will be automatically updated to a higher cost factor the first
                            // time the user logs in.
                            $existinguser->password = hash_internal_user_password($user->password, true);
                            $upt->track('password', $user->password, 'normal', false);
                        } else {
                            // do not print password when not changed
                            $upt->track('password', '', 'normal', false);
                        }
                    }
                    
                    // 사용자 정보 변경 설정 값이 true 또는 비빌번호가 기존 비밀번호와 다른 경우
                    // 회원정보 업데이트
                    if ($doupdate or $existinguser->password !== $oldpw) {
                        // We want only users that were really updated.
                        user_update_user($existinguser, false, false);
                        
                        $upt->track('status', $struserupdated);
                        $usersupdated++;
                        
                        /* profile 사용하지 않음. akddd
                        if (!$remoteuser) {
                            // pre-process custom profile menu fields data from csv file
                            $existinguser = uu_pre_process_custom_profile_data($existinguser);
                            // save custom profile fields data from csv file
                            profile_save_data($existinguser);
                        }
                        */
                        
                        // userUbion 정보 업데이트
                        // user_ubion은 sso 대상자들이기 때문에 이곳에서 변경해도 학사 연동에 의해서 데이터가 다시 초기화 되므로 주의하시기 바랍니다.
                        $this->setUploadUserUbion($userUbion, $existinguser->id);
                        
                        if ($bulk == UU_BULK_UPDATED or $bulk == UU_BULK_ALL) {
                            if (!in_array($user->id, $SESSION->bulk_users)) {
                                $SESSION->bulk_users[] = $user->id;
                            }
                        }
                        
                        // Trigger event.
                        \core\event\user_updated::create_from_userid($existinguser->id)->trigger();
                        
                    } else {
                        // no user information changed
                        $upt->track('status', $struseruptodate);
                        $usersuptodate++;
                        
                        if ($bulk == UU_BULK_ALL) {
                            if (!in_array($user->id, $SESSION->bulk_users)) {
                                $SESSION->bulk_users[] = $user->id;
                            }
                        }
                    }
                    
                    if ($dologout) {
                        \core\session\manager::kill_user_sessions($existinguser->id);
                    }
                    
                } else {
                    // save the new user to the database
                    $user->confirmed    = 1;
                    $user->timemodified = time();
                    $user->timecreated  = time();
                    $user->mnethostid   = $CFG->mnet_localhost_id; // we support ONLY local accounts here, sorry
                    
                    if (!isset($user->suspended) or $user->suspended === '') {
                        $user->suspended = 0;
                    } else {
                        $user->suspended = $user->suspended ? 1 : 0;
                    }
                    $upt->track('suspended', $stryesnooptions[$user->suspended], 'normal', false);
                    
                    if (empty($user->auth)) {
                        $user->auth = 'manual';
                    }
                    $upt->track('auth', $user->auth, 'normal', false);
                    
                    // do not insert record if new auth plugin does not exist!
                    try {
                        $auth = get_auth_plugin($user->auth);
                    } catch (Exception $e) {
                        $upt->track('auth', get_string('userautherror', 'error', s($user->auth)), 'error');
                        $upt->track('status', $strusernotaddederror, 'error');
                        $userserrors++;
                        continue;
                    }
                    if (!isset($supportedauths[$user->auth])) {
                        $upt->track('auth', $struserauthunsupported, 'warning');
                    }
                    
                    $isinternalauth = $auth->is_internal();
                    
                    // 이메일이 @가 아닌 경우에만 email 검사
                    if ($user->email != '@') {
                        if (empty($user->email)) {
                            $upt->track('email', get_string('invalidemail'), 'error');
                            $upt->track('status', $strusernotaddederror, 'error');
                            $userserrors++;
                            continue;
                            
                        } else if ($DB->record_exists('user', array('email'=>$user->email))) {
                            if ($noemailduplicates) {
                                $upt->track('email', $stremailduplicate, 'error');
                                $upt->track('status', $strusernotaddederror, 'error');
                                $userserrors++;
                                continue;
                            } else {
                                $upt->track('email', $stremailduplicate, 'warning');
                            }
                        }
                        if (!validate_email($user->email)) {
                            $upt->track('email', get_string('invalidemail'), 'warning');
                        }
                    }
                    
                    if (empty($user->lang)) {
                        $user->lang = '';
                    } else if (core_user::clean_field($user->lang, 'lang') === '') {
                        $upt->track('status', get_string('cannotfindlang', 'error', $user->lang), 'warning');
                        $user->lang = '';
                    }
                    
                    $forcechangepassword = false;
                    
                    if ($isinternalauth) {
                        if (empty($user->password)) {
                            if ($createpasswords) {
                                $user->password = 'to be generated';
                                $upt->track('password', '', 'normal', false);
                                $upt->track('password', get_string('uupasswordcron', 'tool_uploaduser'), 'warning', false);
                            } else {
                                $upt->track('password', '', 'normal', false);
                                $upt->track('password', get_string('missingfield', 'error', 'password'), 'error');
                                $upt->track('status', $strusernotaddederror, 'error');
                                $userserrors++;
                                continue;
                            }
                        } else {
                            $errmsg = null;
                            $weak = !check_password_policy($user->password, $errmsg);
                            if ($resetpasswords == UU_PWRESET_ALL or ($resetpasswords == UU_PWRESET_WEAK and $weak)) {
                                if ($weak) {
                                    $weakpasswords++;
                                    $upt->track('password', $strinvalidpasswordpolicy, 'warning');
                                }
                                $forcechangepassword = true;
                            }
                            // Use a low cost factor when generating bcrypt hash otherwise
                            // hashing would be slow when uploading lots of users. Hashes
                            // will be automatically updated to a higher cost factor the first
                            // time the user logs in.
                            $user->password = hash_internal_user_password($user->password, true);
                        }
                    } else {
                        $user->password = AUTH_PASSWORD_NOT_CACHED;
                        $upt->track('password', '-', 'normal', false);
                    }
                    
                    $user->id = user_create_user($user, false, false);
                    $upt->track('username', html_writer::link(new moodle_url('/user/profile.php', array('id'=>$user->id)), s($user->username)), 'normal', false);
                    
                    /* profile은 사용하지 않음. akddd
                    // pre-process custom profile menu fields data from csv file
                    $user = uu_pre_process_custom_profile_data($user);
                    // save custom profile fields data
                    profile_save_data($user);
                    */
                    
                    // userUbion 정보 업데이트
                    // user_ubion은 sso 대상자들이기 때문에 이곳에서 변경해도 학사 연동에 의해서 데이터가 다시 초기화됨.
                    $this->setUploadUserUbion($userUbion, $user->id);
                    
                    if ($forcechangepassword) {
                        set_user_preference('auth_forcepasswordchange', 1, $user);
                    }
                    if ($user->password === 'to be generated') {
                        set_user_preference('create_password', 1, $user);
                    }
                    
                    // Trigger event.
                    \core\event\user_created::create_from_userid($user->id)->trigger();
                    
                    $upt->track('status', $struseradded);
                    $upt->track('id', $user->id, 'normal', false);
                    $usersnew++;
                    
                    // make sure user context exists
                    context_user::instance($user->id);
                    
                    if ($bulk == UU_BULK_NEW or $bulk == UU_BULK_ALL) {
                        if (!in_array($user->id, $SESSION->bulk_users)) {
                            $SESSION->bulk_users[] = $user->id;
                        }
                    }
                }
                
                
                // Update user interests.
                if (isset($user->interests) && strval($user->interests) !== '') {
                    useredit_update_interests($user, preg_split('/\s*,\s*/', $user->interests, -1, PREG_SPLIT_NO_EMPTY));
                }
                
                // add to cohort first, it might trigger enrolments indirectly - do NOT create cohorts here!
                foreach ($filecolumns as $column) {
                    if (!preg_match('/^cohort\d+$/', $column)) {
                        continue;
                    }
                    
                    if (!empty($user->$column)) {
                        $addcohort = $user->$column;
                        if (!isset($cohorts[$addcohort])) {
                            if (is_number($addcohort)) {
                                // only non-numeric idnumbers!
                                $cohort = $DB->get_record('cohort', array('id'=>$addcohort));
                            } else {
                                $cohort = $DB->get_record('cohort', array('idnumber'=>$addcohort));
                                if (empty($cohort) && has_capability('moodle/cohort:manage', context_system::instance())) {
                                    // Cohort was not found. Create a new one.
                                    $cohortid = cohort_add_cohort((object)array(
                                        'idnumber' => $addcohort,
                                        'name' => $addcohort,
                                        'contextid' => context_system::instance()->id
                                    ));
                                    $cohort = $DB->get_record('cohort', array('id'=>$cohortid));
                                }
                            }
                            
                            if (empty($cohort)) {
                                $cohorts[$addcohort] = get_string('unknowncohort', 'core_cohort', s($addcohort));
                            } else if (!empty($cohort->component)) {
                                // cohorts synchronised with external sources must not be modified!
                                $cohorts[$addcohort] = get_string('external', 'core_cohort');
                            } else {
                                $cohorts[$addcohort] = $cohort;
                            }
                        }
                        
                        if (is_object($cohorts[$addcohort])) {
                            $cohort = $cohorts[$addcohort];
                            if (!$DB->record_exists('cohort_members', array('cohortid'=>$cohort->id, 'userid'=>$user->id))) {
                                cohort_add_member($cohort->id, $user->id);
                                // we might add special column later, for now let's abuse enrolments
                                $upt->track('enrolments', get_string('useradded', 'core_cohort', s($cohort->name)));
                            }
                        } else {
                            // error message
                            $upt->track('enrolments', $cohorts[$addcohort], 'error');
                        }
                    }
                }
                
                
                // find course enrolments, groups, roles/types and enrol periods
                // this is again a special case, we always do this for any updated or created users
                foreach ($filecolumns as $column) {
                    if (preg_match('/^sysrole\d+$/', $column)) {
                        
                        if (!empty($user->$column)) {
                            $sysrolename = $user->$column;
                            if ($sysrolename[0] == '-') {
                                $removing = true;
                                $sysrolename = substr($sysrolename, 1);
                            } else {
                                $removing = false;
                            }
                            
                            if (array_key_exists($sysrolename, $sysrolecache)) {
                                $sysroleid = $sysrolecache[$sysrolename]->id;
                            } else {
                                $upt->track('enrolments', get_string('unknownrole', 'error', s($sysrolename)), 'error');
                                continue;
                            }
                            
                            if ($removing) {
                                if (user_has_role_assignment($user->id, $sysroleid, SYSCONTEXTID)) {
                                    role_unassign($sysroleid, $user->id, SYSCONTEXTID);
                                    $upt->track('enrolments', get_string('unassignedsysrole',
                                        'tool_uploaduser', $sysrolecache[$sysroleid]->name));
                                }
                            } else {
                                if (!user_has_role_assignment($user->id, $sysroleid, SYSCONTEXTID)) {
                                    role_assign($sysroleid, $user->id, SYSCONTEXTID);
                                    $upt->track('enrolments', get_string('assignedsysrole',
                                        'tool_uploaduser', $sysrolecache[$sysroleid]->name));
                                }
                            }
                        }
                        
                        continue;
                    }
                    if (!preg_match('/^course\d+$/', $column)) {
                        continue;
                    }
                    $i = substr($column, 6);
                    
                    if (empty($user->{'course'.$i})) {
                        continue;
                    }
                    $shortname = $user->{'course'.$i};
                    if (!array_key_exists($shortname, $ccache)) {
                        if (!$course = $DB->get_record('course', array('shortname'=>$shortname), 'id, shortname')) {
                            $upt->track('enrolments', get_string('unknowncourse', 'error', s($shortname)), 'error');
                            continue;
                        }
                        $ccache[$shortname] = $course;
                        $ccache[$shortname]->groups = null;
                    }
                    $courseid      = $ccache[$shortname]->id;
                    $coursecontext = context_course::instance($courseid);
                    if (!isset($manualcache[$courseid])) {
                        $manualcache[$courseid] = false;
                        if ($manual) {
                            if ($instances = enrol_get_instances($courseid, false)) {
                                foreach ($instances as $instance) {
                                    if ($instance->enrol === 'manual') {
                                        $manualcache[$courseid] = $instance;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    
                    if ($courseid == SITEID) {
                        // Technically frontpage does not have enrolments, but only role assignments,
                        // let's not invent new lang strings here for this rarely used feature.
                        
                        if (!empty($user->{'role'.$i})) {
                            $rolename = $user->{'role'.$i};
                            if (array_key_exists($rolename, $rolecache)) {
                                $roleid = $rolecache[$rolename]->id;
                            } else {
                                $upt->track('enrolments', get_string('unknownrole', 'error', s($rolename)), 'error');
                                continue;
                            }
                            
                            role_assign($roleid, $user->id, context_course::instance($courseid));
                            
                            $a = new stdClass();
                            $a->course = $shortname;
                            $a->role   = $rolecache[$roleid]->name;
                            $upt->track('enrolments', get_string('enrolledincourserole', 'enrol_manual', $a));
                        }
                        
                    } else if ($manual and $manualcache[$courseid]) {
                        
                        // find role
                        $roleid = false;
                        if (!empty($user->{'role'.$i})) {
                            $rolename = $user->{'role'.$i};
                            if (array_key_exists($rolename, $rolecache)) {
                                $roleid = $rolecache[$rolename]->id;
                            } else {
                                $upt->track('enrolments', get_string('unknownrole', 'error', s($rolename)), 'error');
                                continue;
                            }
                            
                        } else if (!empty($user->{'type'.$i})) {
                            // if no role, then find "old" enrolment type
                            $addtype = $user->{'type'.$i};
                            if ($addtype < 1 or $addtype > 3) {
                                $upt->track('enrolments', $strerror.': typeN = 1|2|3', 'error');
                                continue;
                            } else if (empty($formdata->{'uulegacy'.$addtype})) {
                                continue;
                            } else {
                                $roleid = $formdata->{'uulegacy'.$addtype};
                            }
                        } else {
                            // no role specified, use the default from manual enrol plugin
                            $roleid = $manualcache[$courseid]->roleid;
                        }
                        
                        if ($roleid) {
                            // Find duration and/or enrol status.
                            $timeend = 0;
                            $status = null;
                            
                            if (isset($user->{'enrolstatus'.$i})) {
                                $enrolstatus = $user->{'enrolstatus'.$i};
                                if ($enrolstatus == '') {
                                    $status = null;
                                } else if ($enrolstatus === (string)ENROL_USER_ACTIVE) {
                                    $status = ENROL_USER_ACTIVE;
                                } else if ($enrolstatus === (string)ENROL_USER_SUSPENDED) {
                                    $status = ENROL_USER_SUSPENDED;
                                } else {
                                    debugging('Unknown enrolment status.');
                                }
                            }
                            
                            if (!empty($user->{'enrolperiod'.$i})) {
                                $duration = (int)$user->{'enrolperiod'.$i} * 60*60*24; // convert days to seconds
                                if ($duration > 0) { // sanity check
                                    $timeend = $today + $duration;
                                }
                            } else if ($manualcache[$courseid]->enrolperiod > 0) {
                                $timeend = $today + $manualcache[$courseid]->enrolperiod;
                            }
                            
                            $manual->enrol_user($manualcache[$courseid], $user->id, $roleid, $today, $timeend, $status);
                            
                            $a = new stdClass();
                            $a->course = $shortname;
                            $a->role   = $rolecache[$roleid]->name;
                            $upt->track('enrolments', get_string('enrolledincourserole', 'enrol_manual', $a));
                        }
                    }
                    
                    // find group to add to
                    if (!empty($user->{'group'.$i})) {
                        // make sure user is enrolled into course before adding into groups
                        if (!is_enrolled($coursecontext, $user->id)) {
                            $upt->track('enrolments', get_string('addedtogroupnotenrolled', '', $user->{'group'.$i}), 'error');
                            continue;
                        }
                        //build group cache
                        if (is_null($ccache[$shortname]->groups)) {
                            $ccache[$shortname]->groups = array();
                            if ($groups = groups_get_all_groups($courseid)) {
                                foreach ($groups as $gid=>$group) {
                                    $ccache[$shortname]->groups[$gid] = new stdClass();
                                    $ccache[$shortname]->groups[$gid]->id   = $gid;
                                    $ccache[$shortname]->groups[$gid]->name = $group->name;
                                    if (!is_numeric($group->name)) { // only non-numeric names are supported!!!
                                        $ccache[$shortname]->groups[$group->name] = new stdClass();
                                        $ccache[$shortname]->groups[$group->name]->id   = $gid;
                                        $ccache[$shortname]->groups[$group->name]->name = $group->name;
                                    }
                                }
                            }
                        }
                        // group exists?
                        $addgroup = $user->{'group'.$i};
                        if (!array_key_exists($addgroup, $ccache[$shortname]->groups)) {
                            // if group doesn't exist,  create it
                            $newgroupdata = new stdClass();
                            $newgroupdata->name = $addgroup;
                            $newgroupdata->courseid = $ccache[$shortname]->id;
                            $newgroupdata->description = '';
                            $gid = groups_create_group($newgroupdata);
                            if ($gid){
                                $ccache[$shortname]->groups[$addgroup] = new stdClass();
                                $ccache[$shortname]->groups[$addgroup]->id   = $gid;
                                $ccache[$shortname]->groups[$addgroup]->name = $newgroupdata->name;
                            } else {
                                $upt->track('enrolments', get_string('unknowngroup', 'error', s($addgroup)), 'error');
                                continue;
                            }
                        }
                        $gid   = $ccache[$shortname]->groups[$addgroup]->id;
                        $gname = $ccache[$shortname]->groups[$addgroup]->name;
                        
                        try {
                            if (groups_add_member($gid, $user->id)) {
                                $upt->track('enrolments', get_string('addedtogroup', '', s($gname)));
                            }  else {
                                $upt->track('enrolments', get_string('addedtogroupnot', '', s($gname)), 'error');
                            }
                        } catch (moodle_exception $e) {
                            $upt->track('enrolments', get_string('addedtogroupnot', '', s($gname)), 'error');
                            continue;
                        }
                    }
                }
                
                // 사용자 검증을 하지 않음 (username때문에 오류 발생됨)
                // $validation[$user->username] = core_user::validate($user);
            } // if ($index > 0) {
        }   // end foreach
        
        $upt->close(); // close table
        
        echo '<div class="well">';
        echo '<p>';
        if ($optype != UU_USER_UPDATE) {
            echo get_string('userscreated', 'tool_uploaduser').': '.$usersnew.'<br />';
        }
        if ($optype == UU_USER_UPDATE or $optype == UU_USER_ADD_UPDATE) {
            echo get_string('usersupdated', 'tool_uploaduser').': '.$usersupdated.'<br />';
        }
        if ($allowdeletes) {
            echo get_string('usersdeleted', 'tool_uploaduser').': '.$deletes.'<br />';
            echo get_string('deleteerrors', 'tool_uploaduser').': '.$deleteerrors.'<br />';
        }
        if ($allowrenames) {
            echo get_string('usersrenamed', 'tool_uploaduser').': '.$renames.'<br />';
            echo get_string('renameerrors', 'tool_uploaduser').': '.$renameerrors.'<br />';
        }
        if ($usersskipped) {
            echo get_string('usersskipped', 'tool_uploaduser').': '.$usersskipped.'<br />';
        }
        echo get_string('usersweakpassword', 'tool_uploaduser').': '.$weakpasswords.'<br />';
        echo get_string('errors', 'tool_uploaduser').': '.$userserrors;
        echo '</p>';
        echo '</div>';
     
        // 이 페이지는 일괄 업로드이기 때문에 bulk하고는 관련이 없음.
        echo '<div class="text-center my-3"><a href="'.$returnurl.'" class="btn btn-primary">'.get_string('continue').'</a></div>';
        echo $OUTPUT->footer();
        die;
    }
    
    
    protected function setUploadUserUbion($userUbion, $userid)
    {
        global $CFG, $DB;
        
        
        
        // 빈값 검사를 위해 배열로 변경
        $arrayUserUbion = (array)$userUbion;
        
        if (!empty($arrayUserUbion)) {
            // profile은 사용하지 않아서 userUbion으로 대처합니다.
            // 기존에 userUbion에 등록된 레코드가 존재하는지 확인
            if ($oldUserUbion = $DB->get_record_sql('SELECT * FROM {user_ubion} WHERE userid = :uid', array('uid'=>$userid))) {
                $userUbion->id = $oldUserUbion->id;
                
                $DB->update_record('user_ubion', $userUbion);
            } else {
                $this->setUserUbionDefaultInsert($userUbion, $userid);
            }
        }
    }
    
    
    public function setUserUbionDefaultInsert($userUbion, $userid)
    {
        global $DB;
        
        $CUser = \local_ubion\user\User::getInstance();
        
        if (empty($userUbion)) {
            $userUbion = new \stdClass();
        }
        
        // 필수값이 셋팅이 안되서 전달되었을수도 있기 때문에 강제로 파라메터로 전달받아서 값 셋팅
        $userUbion->userid = $userid;
        
        // user_ubion에 사용자 추가시 일반/학사 데이터 사용자와 중복으로 입력시
        // 일반 사용자들도 userUbion에 데이터가 추가됨 (userUbion관련 항목들이 전부 빈값으로 넘어옴.)
        if (!isset($userUbion->user_type)) {
            $userUbion->user_type = $CUser::TYPE_ETC;
            $userUbion->user_type_name = $CUser->getTypeName($userUbion->user_type);
        }
        
        
        // 일부 항목들이 null로 입력되는걸 방지하기 위해 '' 처리
        $userUbion = $this->getUploadUserUbionNull($userUbion, 'institution_code');
        $userUbion = $this->getUploadUserUbionNull($userUbion, 'dept_code');
        
        
        $DB->insert_record('user_ubion', $userUbion);
    }
    
    
    /**
     * 조회조건항목에 null, '' 값이 2가지가 존재하면 조회시 일부 항목이 누락되기 때문에 '' 값으로 통일시켜줌
     * 
     * @param object $userUbion
     * @param string $columnName
     * @return object
     */
    private function getUploadUserUbionNull($userUbion, $columnName)
    {
        // $columnName값이 없으면 빈값으로 처리
        if (!isset($userUbion->$columnName)) {
            $userUbion->$columnName = '';
        }
        
        return $userUbion;
    }
    
    public function getDBHiddenColumns()
    {
        return array('id', 'userid');
    }
    
    public function doUbionUpdate()
    {
        global $DB;

        $this->setDBUpdate('user_ubion');
            
        $returnurl = Parameter::post('returnurl', null, PARAM_NOTAGS);
        if (empty($returnurl)) {
            $returnurl = \local_ubion\asite\Asite::getInstance()->sGetUrl('', 'mod', 'member', 'index');
        } else {
            $returnurl = base64_decode($returnurl);
        }
        
        redirect($returnurl);
    }
}