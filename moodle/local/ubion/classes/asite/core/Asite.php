<?php
namespace local_ubion\asite\core;

use local_ubion\core\base\Paging;

class Asite {
    
    /**
     * 링크를 생성하는 함수
     * 함수 첫 번째 매개 변수가 공백이면 새로운 주소를 생성하고
     * 함수 첫 번째 매개 변수가 공백이 아니면 현재 주소를 가공한다.
     */
    function sGetUrl()
    {
        $argsCnt  = func_num_args();
        $args = func_get_args();
        $sep = '&amp;';
        $o = $this->sMakeUrl($argsCnt, $args, $sep);
        return $o;
    }
    
    function sGetUrlDecode()
    {
        $argsCnt  = func_num_args();
        $args = func_get_args();
        $sep = '&';
        $o = $this->sMakeUrl($argsCnt, $args, $sep);
        return $o;
    }
    
    function sMakeUrl($argsCnt, $args, $sep='&amp;')
    {
        //$argsCnt  = func_num_args();
        //$args = func_get_args();
        
        $getData = null;
        
        if ( ($argsCnt%2) == 1 )
        {
            if ( empty($args[0]) )
            {
                $sPhpSelf = $_SERVER['PHP_SELF'];
            } else {
                $sPhpSelf = $args[0];
            }
            
            array_shift($args);
            $argsCnt--;
            
        } else {
            
            $getData = $_GET;
            $sPhpSelf = $_SERVER['PHP_SELF'];
            
            if (!empty($_SERVER['PATH_INFO'])) {
                $getData['mod'] = $this->sGetModuleByStr($_SERVER['PATH_INFO']);
                $getData['act'] = $this->sGetActionByStr($_SERVER['PATH_INFO']);
                
            }
        }
        
        $vars = null;
        
        if (!empty($getData)) {
            foreach ($getData as $k=>$v){
                
                if (is_array($v)) {
                    foreach ($v AS $k2=>$v2) {
                        $k2 = trim($k2);
                        $v2 = trim($v2);
                        
                        // empty로 검사하면 파라메터로 0값이 전달될때 문제가 생김
                        if ($v2 != ''){
                            $vars[$k][$k2] = $v2;
                        }
                    }
                    continue;
                }
                
                $k = trim($k);
                $v = trim($v);
                // empty로 검사하면 파라메터로 0값이 전달될때 문제가 생김
                if ($v != ''){
                    $vars[$k] = $v;
                }
            }
            unset($v);
        }
        
        if ($argsCnt) {
            for ($i=0; $i<$argsCnt; $i+=2) {
                $key = trim($args[$i]);
                $val = trim($args[($i+1)]);
                
                if (!isset($val) || empty($val)) {
                    unset($vars[$key]);
                    continue;
                }
                
                $vars[$key] = $val;
            }
        }
        
        $url = $sPhpSelf;
        
        if (isset($vars['mod'])) {
            $url = sprintf('%s/%s', $url, $vars['mod']);
            unset($vars['mod']);
        }
        
        if (isset($vars['act'])) {
            $url = sprintf('%s/@%s', $url, $vars['act']);
            unset($vars['act']);
        }
        
        
        if (!empty($vars)) {
            
            foreach ($vars AS $k=>$v) {
                if (is_array($v)) {
                    foreach ($v AS $k2=>$v2) {
                        $query[] = sprintf('%s=%s', "{$k}[{$k2}]", urlencode($v2));
                    }
                    continue;
                }
                $query[] = sprintf('%s=%s', $k, urlencode($v));
            }
            
            
            $qstr = implode($sep, $query);
            $o = sprintf('%s?%s', $url, $qstr);
        } else {
            $o = $url;
        }
        
        return $o;
    }
    
    
    /**
     * 메뉴 배열을 만든다.
     * @param string $path
     * @param number $dep
     * @return multitype:mixed
     */
    function sMakeMenu($path='', $dep=-1)
    {
        global $SITECFG;
        
        // require_once $SITECFG->modulePath.'/auth/lib.php';
        //$authLib = new Asite_modules_auth_lib();
        //$authUserList = $authLib->getUserAuth();
        $authUserList = 'siteadmin';
        
        $tmpMenu  = array();
        $func = __FUNCTION__;
        $dep++;
        
        if (empty($path)) {
            $path = $SITECFG->modulePath;
        }
        
        if (empty($_SERVER['PATH_INFO']) || $_SERVER['PATH_INFO'] === '/') {
            $pathInfoArr = array();
        } else {
            $pathInfo = trim($_SERVER['PATH_INFO'], '/');
            $pathInfoArr = explode('/', $pathInfo);
        }
        
        
        $module = $this->sGetModule();
        
        $act = $this->sGetAction();
        
        $i = 10000000;
        $directory = dir($path);
        while($entry = $directory->read()) {
            if ($entry != '.' && $entry != '..') {
                $tmpPath = "{$path}/{$entry}";
                if ( is_dir($tmpPath)) {
                    $infoFile = "{$tmpPath}/_info.txt";
                    if (is_file($infoFile)) {
                        $tmp = file_get_contents($infoFile);
                        if (!empty($tmp)) {
                            $val = json_decode($tmp, true);
                            $mod = str_replace($SITECFG->modulePath, '', $tmpPath);
                            $mod = trim($mod, '/');
                            
                            
                            if ($authUserList !== 'siteadmin' && array_search($mod, $authUserList) === false) {
                                continue;
                            }
                            
                            
                            // link 가 없으면 기본 index 로
                            if (empty($val['link'])) {
                                $val['link'] = 'index';
                            }
                            
                            
                            // 자식 메뉴 입력
                            $child = $this->$func($tmpPath, $dep);
                            if (!empty($child)) {
                                $val['child'] = $child;
                            }
                            
                            // val['act'] 이 있다면
                            $tmpAct = array();
                            if (!empty($val['act'])) {
                                
                                //$addAct = false;
                                foreach ($val['act'] as $k=>$v) {
                                    //$val['act'][$k]['href'] = sGetUrl('', 'mod', $mod, 'act', $v['link']);
                                    $val['act'][$k]['href'] = $this->sMakeMenuHref($mod, $v['link']); //sGetUrl('', 'mod', $mod.'/@'.$v['link']);
                                    
                                    if ($mod === $module) {
                                        //									if (isset($v['link']) && $v['link'] === '@'.$act || empty($v['link']) && $act === 'index') {
                                        if ((isset($v['link']) && $v['link'] === '@'.$act) || (empty($v['link']) && $act === 'index') || (isset($SITECFG->menuParentAct) && $v['link'] === '@'.$SITECFG->menuParentAct)) {
                                            $val['act'][$k]['active'] = 1;
                                        }
                                    }
                                    
                                    $val['act'][$k]['isAct'] = 1;
                                    
                                    /*
                                     if (!empty($val['link']) && $val['link'] === $v['link']) {
                                     $addAct = true;
                                     }
                                     */
                                }
                                
                                /*
                                 if ( !empty($val['link']) && !$addAct ) {
                                 $parentAct = array('name'=>$val['name'], 'link'=>$val['link'], 'class'=>$val['class']);
                                 $parentAct['href'] = sGetUrl('', 'mod', $mod, 'act', $parentAct['link']);
                                 
                                 if (isset($parentAct['link']) && $parentAct['link'] === $act) {
                                 $parentAct['active'] = 1;
                                 }
                                 
                                 array_unshift($val['act'], $parentAct);
                                 }
                                 */
                                
                                if (empty($val['child'])) {
                                    $val['child'] = $val['act'];
                                } else {
                                    $val['child'] = $val['act'] + $val['child'];// + $val['child'];
                                }
                            }
                            
                            
                            
                            // 네비게이션. 현재 경로 표시
                            if ($dep==0 && empty($pathInfoArr) && $entry === 'index') {
                                $val['active'] = 1;
                            } else if (isset($pathInfoArr[$dep]) && $pathInfoArr[$dep] == $entry){
                                $val['active'] = 1;
                            } else {
                                $val['active'] = 0;
                            }
                            
                            // href 링크 생성.
                            if (!empty($val['link']) && $val['link'] !== 'index') {
                                //$val['href'] = sGetUrl('', 'mod', $mod . '/@' .$val['link']);
                                $val['href'] = $this->sMakeMenuHref($mod, $val['link']);
                            } else {
                                $val['href'] = $this->sGetUrl('', 'mod', $mod);
                            }
                            
                            // class가 없으면 빈값 입력.
                            if (!isset($val['class'])) {
                                $val['class'] = '';
                            }
                            
                            if (!isset($val['target'])) {
                                $val['target'] = '';
                            }
                            
                            
                            // 정렬
                            if (empty($val['sort'])) {
                                $tmpMenu[$i][$entry] = $val;
                                $i++;
                            } else {
                                $sort = $val['sort'];
                                $sort = $sort * 1000;
                                
                                if (!isset($tmpMenu[$sort])) {
                                    $tmpMenu[$sort][$entry] = $val;
                                } else {
                                    while (!empty($tmpMenu[$sort])) {
                                        $sort --;
                                        if (empty($tmpMenu[$sort])) {
                                            $tmpMenu[$sort][$entry] = $val;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
        }
        unset($entry);
        $directory->close();
        
        // key 값 순으로 정렬하고
        ksort($tmpMenu);
        
        // key 값을 제외한 배열을 새로 생성
        $o = array();
        foreach ($tmpMenu AS $v) {
            $key = current(array_keys($v));
            $val = current(array_values($v));
            
            $o[$key] = $val;
        }
        unset($v);
        
        return $o;
    }
    
    /**
     * 메뉴 배열을 만드는 작업에서 (sMakeMenu) url을 만들 때 사용된다.
     * @param string $mod
     * @param string $link
     * @return string
     */
    function sMakeMenuHref($mod='', $link='')
    {
        if (!empty($link)
            && (preg_match("/\//", $link) || preg_match("/^[A-Za-z0-9\-\_]+$/i", $link))
        ) {
            $module = $this->sGetModuleByStr($link);
            $act = $this->sGetActionByStr($link);
            
            if (!empty($module)) {
                $mod = $mod.'/'.$module;
            }
            
            $o = $this->sGetUrl('', 'mod', $mod, 'act', $act);
        }else if (!empty($mod) && !empty($link) && preg_match("/^@.+/i", $link)) {
            $link = preg_replace("/^(\@)/i", "", $link);
            $o = $this->sGetUrl('', 'mod', $mod, 'act', $link);
        } else if (!empty($mod) && empty($link)) {
            $o = $this->sGetUrl('', 'mod', $mod);
        } else {
            $o = $link;
        }
        
        return $o;
    }
    
    /**
     * 생성된 메뉴를 사용한다.
     * @return string
     */
    function sGetMenu()
    {
        if (isset($GLOBALS['sGetMenu'])) {
            $o = $GLOBALS['sGetMenu'];
        } else {
            $o = $GLOBALS['sGetMenu'] = $this->sMakeMenu();
        }
        
        return $o;
    }
    
    /**
     * 액션을 구함
     * @return string
     */
    function sGetAction()
    {
        if (isset($GLOBALS['sGetAction'])) {
            $action = $GLOBALS['sGetAction'];
        } else {
            $action = 'index';
            
            if (isset($_SERVER['PATH_INFO'])) {
                $path_info = trim($_SERVER['PATH_INFO'], '/');
                preg_match("/^.+\@(.+)$/", $path_info, $m);
                
                if (!empty($m[1])) {
                    $action = $m[1];
                }
            }
            
            $GLOBALS['sGetAction'] = $action;
        }
        return $action;
    }
    
    /**
     * 문자열에서 액션명을 구한다.
     * @param string $pathInfo
     * @return string
     */
    function sGetActionByStr($pathInfo='')
    {
        $action = '';
        
        if (!empty($pathInfo)) {
            $path_info = trim($pathInfo, '/');
            preg_match("/^.+\@(.+)$/", $path_info, $m);
            
            if (!empty($m[1])) {
                $action = $m[1];
            }
        }
        
        return $action;
    }
    
    
    /**
     * 모듈을 구함
     * @return string
     */
    function sGetModule()
    {
        if (isset($GLOBALS['sGetModule'])) {
            $module = $GLOBALS['sGetModule'];
        } else {
            $module = 'index';
            
            $path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
            $path_info = trim($path_info, '/');
            
            preg_match("/^([^\@]+)(?:\/\@.+)?$/", $path_info, $m);
            
            if (!empty($m[1])) {
                $module = $m[1];
            }
            
            $GLOBALS['sGetModule'] = $module;
        }
        
        return $module;
    }
    
    /**
     * 문자열에서 모듈명을 추출한다.
     * @param string $pathInfo
     * @return string
     */
    function sGetModuleByStr($pathInfo='')
    {
        $module = '';
        
        $path_info = empty($pathInfo) ? '' : $pathInfo;
        $path_info = trim($path_info, '/');
        
        preg_match("/^([^\@]+)(?:\/\@.+)?$/", $path_info, $m);
        
        if (!empty($m[1])) {
            $module = $m[1];
        }
        
        return $module;
    }
    
    
    function sGetModuleName($module)
    {
        global $SITECFG;
        
        if (!empty($module)) {
            
            $tmpPath = $SITECFG->modulePath.'/'.$module;
            if ( is_dir($tmpPath)) {
                $infoFile = "{$tmpPath}/_info.txt";
                if (is_file($infoFile)) {
                    $tmp = file_get_contents($infoFile);
                    if (!empty($tmp)) {
                        $val = json_decode($tmp, true);
                        
                        return $val['name'];
                    }
                }
            }
        }
        
        return null;
    }
    
    
    /**
     * 현재 모듈의 하위 메뉴 추출
     * @param string $module
     * @param number $dep
     * @return string|multitype:
     */
    function sGetSubMenu($module='', $sDep=0)
    {
        if (empty($module)) {
            $module = $this->sGetModule();
        } else {
            $module = trim($module, '/');
        }
        
        if (empty($module)) {
            return '';
        }
        
        $sGetMenu = $this->sGetMenu();
        
        $moduleArr = explode('/', $module);
        
        if (count($moduleArr) < $sDep) {
            return '';
        }
        
        if ($sDep > 0) {
            array_splice($moduleArr, $sDep);
        }
        
        $tmp = array();
        $i = 0;
        foreach ($moduleArr as $v) {
            if ($i === 0) {
                if (empty($sGetMenu[$v]['child'])) {
                    return '';
                } else {
                    $tmp = $sGetMenu[$v]['child'] ;
                }
            } else {
                if (empty($tmp[$v]['child'])) {
                    return '';
                } else {
                    $tmp = $tmp[$v]['child'];
                }
            }
            
            $i++;
        }
        return $tmp;
    }
    
    
    /**
     * 네비 생성용 메뉴 배열 추출
     * @param string $module
     * @return string|number
     */
    function sGetNav($module='')
    {
        if (empty($module)) {
            $module = $this->sGetModule();
            $act = $this->sGetAction();
        } else {
            $module = trim($module, '/');
        }
        
        if (empty($module)) {
            return '';
        }
        
        $moduleArr = explode('/', $module);
        
        $sGetMenu = $this->sGetMenu();
        
        $tmp = $tmp2 = array();
        $i = 0;
        foreach ($moduleArr as $v) {
            if ($i === 0) {
                if (empty($sGetMenu[$v])) {
                    return '';
                } else {
                    $tmp2[] = $sGetMenu[$v];
                    
                    if (empty($sGetMenu[$v]['child'])) {
                        break;
                    } else {
                        $tmp = $sGetMenu[$v]['child'];
                    }
                }
            } else {
                if (empty($tmp[$v])) {
                    break;;
                } else {
                    $tmp2[] = $tmp[$v];
                    
                    if ( empty($tmp[$v]['child'] ) ) {
                        break;
                    } else {
                        $tmp = $tmp[$v]['child'];
                    }
                }
            }
            $i++;
        }
        
        if (!empty($act)) {
            foreach ($tmp as $k=>$v) {
                if (isset($v['isAct']) && $v['link'] === '@'.$act) {
                    $tmp2[] = $v;
                }
            }
            unset($v);
        }
        
        $tmp2[(count($tmp2)-1)]['isLast'] = 1;
        
        return $tmp2;
    }
    
    
    /**
     * 현재 모듈이 동기화 기능이 있는 모듈이면 정보를 반환한다.
     * @return \stdClass|boolean
     */
    function sIsCronModule()
    {
        global $SITECFG, $haksa;
        
        $cronFile = sprintf('%s/%s/demon.php', $SITECFG->modulePath, $SITECFG->module);
        
        if (is_file($cronFile)) {
            $logfile = 'sync_'.$SITECFG->moduleName;
            
            $sync = new \stdClass();
            $sync->is = $haksa->chkeckSyncprogress($logfile);
            $sync->logfile = $logfile;
            $sync->returnurl = $SITECFG->nowUrl;
            $sync->encoding_returnurl = base64_encode($sync->returnurl);
            
            return $sync;
        } else {
            return false;
        }
    }
    
    
    
    
    
    
    
    /**
     * head 부분 / body 마지막 부분에 html을 삽입할 때 사용한다.
     * @param string $str
     * @param string $key
     * @return boolean
     */
    function sAddHtml($str='', $key='')
    {
        if (empty($str) || empty($key)) {
            return false;
        }
        
        if (isset($GLOBALS[$key])) {
            $GLOBALS[$key] .= PHP_EOL . $str ;
        } else {
            $GLOBALS[$key] = $str;
        }
    }
    
    function sAddHtmlHeader($str='')
    {
        sAddHtml($str, __FUNCTION__);
    }
    
    function sAddHtmlFooter($str='')
    {
        sAddHtml($str, __FUNCTION__);
    }
    
    /**
     * 추가된 html 을 출력한다.
     * @param string $type
     * @return boolean
     */
    function sPrintHtml($type='')
    {
        if (empty($type)) {
            return false;
        }
        
        if ($type === 'head') {
            $key = 'sAddHtmlHeader';
        } else {
            $key = 'sAddHtmlFooter';
        }
        
        if (isset($GLOBALS[$key])) {
            return $GLOBALS[$key] ;
        }
    }
    function sPrintHtmlHeader()
    {
        return sPrintHtml('head');
    }
    function sPrintHtmlFooter()
    {
        return sPrintHtml('foot');
    }
    
    
    /**
     * js, css 를 head 에 추가하기 위해 사용하는 함수
     * 상대 경로 안됨. 리눅스 절대 경로 or 웹 절대 경로를 사용
     * @param string $file
     * @param string $type
     * @return boolean|string
     */
    function sAddJsCss($file, $type)
    {
        global $SITECFG;
        $o = '';
        
        if ($type === 'js') {
            $key = 'sAddJs';
            $oStr = '<script type="text/javascript" src="%s"></script>';
        } else if ($type === 'css') {
            $key = 'sAddCss';
            $oStr = '<link rel="stylesheet" href="%s">';
        } else {
            return '';
        }
        
        if (!is_array($file)) {
            $file = array($file);
        }
        
        $file = array_map( function( $a ){ return trim($a); }, $file );
        $file = array_unique($file);
        
        foreach ($file as $v) {
            if (empty($v)) {
                continue;
            }
            
            //$v = preg_replace("/^".addslashes($SITECFG->dirroot)."/", '', $v);
            $v = str_replace("\\", '/', $v);
            $o .= sprintf($oStr, $v) . PHP_EOL;
        }
        
        if (isset($GLOBALS[$key])) {
            $GLOBALS[$key] .= $o;
        } else {
            $GLOBALS[$key] = $o;
        }
    }
    
    /**
     * js 파일을 head 에 추가한다.
     * @param string $str
     */
    function sAddJs($file)
    {
        return $this->sAddJsCss($file, 'js');
    }
    
    function sPrintJs()
    {
        if (isset($GLOBALS['sAddJs'])) {
            return $GLOBALS['sAddJs'];
        }
    }
    
    
    /**
     * css 파일을 head에 추가한다.
     * @param string $str
     */
    function sAddCss($file)
    {
        return $this->sAddJsCss($file, 'css');
    }
    
    function sPrintCss()
    {
        if (isset($GLOBALS['sAddCss'])) {
            return $GLOBALS['sAddCss'];
        }
    }
    
    
    function sHeadHtml()
    {
        
        $o = $this->sPrintJs();
        $o .= $this->sPrintCss();
        $o .= $this->sPrintHtmlHeader();
        
        echo $o;
    }
    
    function sTailHtml()
    {
        $o = $this->sPrintHtmlFooter();
        
        echo $o;
    }
    
    
    
    function sHtmlChked($val1='', $val2='', $type='c')
    {
        if ($val1 == (int)$val1) {
            $val1 = (int)$val1;
        }
        
        if ($val2 == (int)$val2) {
            $val2 = (int)$val2;
        }
        
        $str = ($type === 'c') ? 'checked="checked"' : 'selected="selected"';
        
        echo ($val1 === $val2) ? $str : '';
    }
    
    function sPaging($paging, $print_totalpagenum = 0) {
        $paging->link = $this->sGetUrl();
        
        $paging->firstLink = str_replace('page=', '', $paging->firstLink);
        $paging->prevLink = str_replace('page=', '', $paging->prevLink);
        $paging->nextLink = str_replace('page=', '', $paging->nextLink);
        $paging->lastLink = str_replace('page=', '', $paging->lastLink);
        
        $pagingHtml = null;
        if ($paging->totalPage > $print_totalpagenum) {
            $pagingHtml .= '<div class="text-center">';
            $pagingHtml .= '<ul class="pagination">';
            
            if ($paging->firstLink) {
                $pagingHtml .= '<li>';
                $pagingHtml .= '	<a href="'.$this->sGetUrl('page', $paging->firstLink).'" class="nav_paging">';
                $pagingHtml .=          $paging->icons->first;
                $pagingHtml .= '	</a>';
                $pagingHtml .= '</li>';
            }
            
            if ($paging->prevLink) {
                $pagingHtml .= '<li>';
                $pagingHtml .= '	<a href="'.$this->sGetUrl('page', $paging->prevLink).'" class="nav_paging">';
                $pagingHtml .=          $paging->icons->prev;
                $pagingHtml .= '	</a>';
                $pagingHtml .= '</li>';
                
            }
            
            foreach ($paging->pageList as $pp) {
                $active_page_class = '';
                $page_link = $this->sGetUrl('page', $pp['pageNum']);
                
                if ($pp['pageNum'] == $paging->currentPage) {
                    $active_page_class = 'active';
                    $page_link = '#';
                }
                
                $pagingHtml .= '<li class="'.$active_page_class.'">';
                $pagingHtml .= '<a href="'.$page_link.'" class="nav_paging">';
                $pagingHtml .= 	$pp['pageNum'];
                $pagingHtml .=	'</a>';
                $pagingHtml .=	'</li>';
            }
            
            if ($paging->nextLink) {
                $pagingHtml .= '<li>';
                $pagingHtml .= '	<a href="'.$this->sGetUrl('page', $paging->nextLink).'" class="nav_paging">';
                $pagingHtml .=          $paging->icons->next;
                $pagingHtml .= '	</a>';
                $pagingHtml .= '</li>';
            }
            
            if ($paging->lastLink) {
                $pagingHtml .= '<li>';
                $pagingHtml .= '	<a href="'.$this->sGetUrl('page', $paging->lastLink).'" class="nav_paging">';
                $pagingHtml .=          $paging->icons->last;
                $pagingHtml .= '	</a>';
                $pagingHtml .= '</li>';
                
            }
            $pagingHtml .= '</ul>';
            $pagingHtml .= '</div>';
        }
        
        return $pagingHtml;
        // return Paging::getHTML($paging, $print_totalpagenum);
    }
    
    
    function geSitetMenus()
    {
        $menuHTML = '<ul class="menu">';
        
        $siteMenu = $this->sGetMenu();
        
        if (!empty($siteMenu)) {
            foreach ($siteMenu as $idx => $sm) {
                
                $class = '';
                if (isset($sm['active']) && $sm['active']) {
                    if (isset($sm['child'])) {
                        $class = 'open active';
                    } else {
                        $class = 'active';
                    }
                }
                
                $menuHTML .= '<li class="'.$class.'">';
                
                $ischildmenu = (isset($sm['child']) && !empty($sm['child'])) ? true : false;
                
                $dropmenuclass = ($ischildmenu) ? 'dropmenu' : '';
                
                $menuHTML .=      '<a class="'.$dropmenuclass.'" href="'.$sm['href'].'">';
                $menuHTML .=          '<i class="'.$sm['class'].' menuicon"></i><span class="menu-text"> '.$sm['name'].'</span>';
                $menuHTML .=      '</a>';
                
                
                if ($ischildmenu) {
                    $menuHTML .=  '<ul class="side-menu-sub">';
                    foreach ($sm['child'] as $smc) {
                        $subclass = (isset($smc['active']) && !empty($smc['active'])) ? 'active' : '';
                        
                        $menuHTML .= '<li class="'.$subclass.'">';
                        $menuHTML .=      '<a class="submenu '.$subclass.'" href="'.$smc['href'].'"><i class="fa '.$smc['class'].'"></i><span class="menu-text">'.$smc['name'].'</span></a>';
                        $menuHTML .= '</li>';
                        
                    }
                    $menuHTML .=  '</ul>';
                }
                $menuHTML .= '</li>';
            }
        } else {
            $menuHTML .= '<li> 등록된 메뉴가 없습니다. </li>';
        }
        
        $menuHTML .= '</ul>';
        
        return $menuHTML;
    }
    
    
    function getBreadcrumb()
    {
        global $CFG, $SITECFG;
    
        
        $breadcrumbHTML = '';
        
        $siteNav = $this->sGetNav();
        
        $navtitle = 'HOME';
        if (!empty($siteNav)) {
            if ($lastnav = end($siteNav)) {
                $navtitle = $lastnav['name'];
            }
        }
        
        $breadcrumbHTML .= '<h2>'.$navtitle.'</h2>';
        $breadcrumbHTML .= 	'<ul class="breadcrumb">';
        $breadcrumbHTML .= 		'<li class="breadcrumb-item"><a href="'.$SITECFG->wwwroot.'"><i class="fa fa-home"></i> Home</a></li>';
				
		if (!empty($siteNav)) {
            foreach ($siteNav as $idx => $sn) {
                if (!isset($sn->isLast)) {
                    $breadcrumbHTML .=  '<li class="breadcrumb-item">';
                    if (!empty($sn['child'])) {
                        $breadcrumbHTML .=  '<div class="dropdown">';
		        
                        $breadcrumbHTML .=      '<a class="dropdown-toggle" id="dLabel-'.$idx.'" role="button" data-toggle="dropdown" href="'.$sn['href'].'">'.$sn['name'].'<b class="caret"></b></a>';
		        
                        $breadcrumbHTML .=      '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel-'.$idx.'">';
		               
                        foreach ($sn['child'] as $snd) {
                            $breadcrumbHTML .=      '<li><a class="dropdown-item" tabindex="-1" href="'.$snd['href'].'">'.$snd['name'].'</a></li>';
                        }
		               
                        $breadcrumbHTML .=      '</ul>';
                        $breadcrumbHTML .=  '</div>';
                    } else {
                        $breadcrumbHTML .=  '<a href="'.$sn['href'].'">'.$sn['name'].'</a>';
                    }
                    $breadcrumbHTML .=  '</li>';
                } else {
                    $breadcrumbHTML .=  '<li class="active">'.$sn['name'].'</li>';
                }
            }
        }
        $breadcrumbHTML .= 	'</ul>';
        
        return $breadcrumbHTML;
    }
    
    
    function geSitetMenuSub()
    {
        $subMenuHTML = '';
        
        $siteSubMenu = $this->sGetSubMenu('', 2);
        
        if (!empty($siteSubMenu)) {
            $subMenuHTML .= '<div class="content-submenu">';
            $subMenuHTML .=    '<ul class="nav nav-tabs nav-submenu">';
            
            foreach ($siteSubMenu as $ssm) {
                $menuActiveClass= (isset($ssm['active']) && !empty($ssm['active'])) ? 'active' : '';
                if (isset($ssm['child']) && !empty($ssm['child'])) {
                    
                    
                    $subMenuHTML .= '<li class="nav-item dropdown '.$menuActiveClass.'">';
                    $subMenuHTML .=    '<a href="'.$ssm['href'].'" class="nav-link dropdown-toggle " data-toggle="dropdown">';
                    $subMenuHTML .=        $ssm['name'];
                    $subMenuHTML .=        '<b class="caret"></b>';
                    $subMenuHTML .=    '</a>';
                    $subMenuHTML .=    '<ul class="dropdown-menu">';
                    foreach ($ssm['child'] as $ssmd) {
                        $subMenuHTML .=    '<li><a href="'.$ssmd['href'].'" class="dropdown-item">'.$ssmd['name'].'</a></li>';
                    }
                    $subMenuHTML .=    '</ul>';
                    $subMenuHTML .= '</li>';
                } else {
                    $subMenuHTML .= '<li class="nav-item '.$menuActiveClass.'">';
                    $subMenuHTML .=    '<a href="'.$ssm['href'].'" class="nav-link ">'.$ssm['name'].'</a>';
                    $subMenuHTML .= '</li>';
                }
            }
            $subMenuHTML .=    '</ul>';
            $subMenuHTML .= '</div>';
        }
        
        return $subMenuHTML;
    }
    
    
}