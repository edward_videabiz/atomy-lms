<?php
namespace local_ubion\asite;

class Asite extends core\Asite {
    private static $instance;
    
    /**
     * Asite Class
     * @return Asite
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
}