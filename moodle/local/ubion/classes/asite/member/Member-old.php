<?php
namespace local_ubion\asite\member;

use local_ubion\base\Javascript;
use local_ubion\base\Parameter;
use local_ubion\base\Paging;


class Member extends \local_ubion\asite\core\member\Member
{
    private static $instance;
    
    /**
     * Member
     * @return Member
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
    
    
    /**
     * 코스모스에 등록된 모든 회원을 리턴해줍니다.
     *
     * @param string $keyfield
     * @param string $keyword
     * @param string $auth
     * @param string $institutionCode
     * @param string $departmentCode
     * @param string $userType
     * @param string $univUserType
     * @param number $page
     * @param number $ls
     * @return array
     */
    public function getMembers($keyfield, $keyword, $auth=null, $institutionCode=null, $departmentCode=null, $userType=null, $univUserType=null, $page=1, $ls=15)
    {
        global $CFG, $DB;
        
        $query = "SELECT
                            ".self::REPLACE."
                FROM        {user} u
                LEFT JOIN   {user_ubion} uu ON u.id = uu.userid
                WHERE       u.deleted = 0
                AND         u.id > :siteguestid";
        
        $param = [
            'siteguestid' => $CFG->siteguest
        ];
        
        
        // 관리자는 표시되면 안됨.
        $admins = $CFG->siteadmins;
        $query .= " AND u.id NOT IN (".$admins.")";
        
        // 검색
        if (!empty($keyfield) && !empty($keyword)) {
            if ($keyfield == 'fullname') {
                $fullname = $DB->sql_fullname("u.firstname", "u.lastname");
                $query .= " AND ".$DB->sql_like($fullname, ':keyword');
                $param['keyword'] = $keyword.'%';
            } else {
                $query .= " AND mnethostid = :mnethostid AND ".$DB->sql_like('u.username', ':username');
                
                $param['mnethostid'] = $CFG->mnet_localhost_id;
                $param['username'] = $keyword.'%';
            }
        }
        
        // 인증타입
        if (!empty($auth)) {
            $query .= " AND u.auth = :auth";
            $param['auth'] = $auth;
        }
        
        // 소속
        if ($institutionCode != Parameter::getOptionAllValue()) {
            $query .= " AND uu.institution_code = :institution_code";
            $param['institution_code'] = $institutionCode;
        }
        
        // 부석
        if ($departmentCode != Parameter::getOptionAllValue()) {
            $query .= " AND uu.dept_code = :dept_code";
            $param['dept_code'] = $departmentCode;
        }
        
        // 코스모스 신분타입
        if ($userType != Parameter::getOptionAllValue()) {
            $query .= " AND uu.user_type = :user_type";
            $param['user_type'] = $userType;
        }
        
        // 학사 신분타입
        if ($univUserType != Parameter::getOptionAllValue()) {
            $query .= " AND uu.univ_user_type = :univ_user_type";
            $param['univ_user_type'] = $univUserType;
        }
        
        $totalQuery = str_replace(self::REPLACE, 'COUNT(1)', $query);
        $totalCount = $DB->get_field_sql($totalQuery, $param);
        
        
        $usernamefield = get_all_user_name_fields(true, 'u');
        $field = "uu.*
                  ,u.id
                  ,u.auth
                  ,u.username
                  ,u.idnumber
                  ,".$usernamefield."
                  ,u.institution
                  ,u.department
                  ,u.phone1
                  ,u.phone2
                  ,u.email";
        
        $query = str_replace(self::REPLACE, $field, $query);
        $query .= " ORDER BY u.id DESC";
        $limitfrom = ($page > 0 ) ?  $ls * ($page - 1) : 0;
        $lists = $DB->get_records_sql($query, $param, $limitfrom, $ls);
        
        //error_log($query);
        //error_log(print_r($param, true));
        
        return array($totalCount, $lists);
    }
    
    /**
     * 코스모스에 등록된 모든 회원을 리턴해줍니다.
     *
     * @param string $keyfield
     * @param string $keyword
     * @param number $page
     * @param number $ls
     * @return array
     */
    public function getAdmins($keyfield, $keyword, $page=1, $ls=15)
    {
        global $CFG, $DB;
        
        $admins = $CFG->siteadmins;
        
        $query = "SELECT
                            ".self::REPLACE."
                FROM        {user} u
                LEFT JOIN   {user_ubion} uu ON u.id = uu.userid
                WHERE       u.deleted = 0
                AND         u.id IN (".$admins.")";
        
        $param = [];
        
        // 검색
        if (!empty($keyfield) && !empty($keyword)) {
            if ($keyfield == 'fullname') {
                $fullname = $DB->sql_fullname("u.firstname", "u.lastname");
                $query .= " AND ".$DB->sql_like($fullname, ':keyword');
                $param['keyword'] = $keyword.'%';
            } else {
                $query .= " AND mnethostid = :mnethostid AND ".$DB->sql_like('u.username', ':username');
                
                $param['mnethostid'] = $CFG->mnet_localhost_id;
                $param['username'] = $keyword.'%';
            }
        }
        
        $totalQuery = str_replace(self::REPLACE, 'COUNT(1)', $query);
        $totalCount = $DB->get_field_sql($totalQuery, $param);
        
        
        $usernamefield = get_all_user_name_fields(true, 'u');
        $field = "uu.*
                  ,u.id
                  ,u.auth
                  ,u.username
                  ,u.idnumber
                  ,".$usernamefield."
                  ,u.institution
                  ,u.department
                  ,u.phone1
                  ,u.phone2
                  ,u.email";
        
        $query = str_replace(self::REPLACE, $field, $query);
        $query .= " ORDER BY u.id DESC";
        $limitfrom = ($page > 0 ) ?  $ls * ($page - 1) : 0;
        $lists = $DB->get_records_sql($query, $param, $limitfrom, $ls);
        
        //error_log($query);
        //error_log(print_r($param, true));
        
        return array($totalCount, $lists);
    }
    
    
    public function doUserSearch()
    {
        $keyfield = Parameter::post('keyfield', 'memberkey', PARAM_ALPHANUMEXT);
        $keyword = Parameter::post('keyword', null, PARAM_NOTAGS);
        
        $validate = $this->validate()->make(array(
            'keyword' => array(
                'required',
                $this->validate()::PLACEHOLDER => get_string('keyword', 'local_ubion'),
                $this->validate()::PARAMVALUE => $keyword
            )
        ));
                
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            global $CFG, $DB;
            
            $usernamefield = get_all_user_name_fields(true, 'u');
            
            $query = "SELECT
                            u.id
                           ,u.auth
                           ,u.username
                           ,u.idnumber
                           ,".$usernamefield."
                           ,u.institution
                           ,u.department
                FROM        {user} u
                WHERE       u.deleted = 0";
            
            $param = [];
            
            // 검색
            if ($keyfield == 'fullname') {
                $fullname = $DB->sql_fullname("u.firstname", "u.lastname");
                $query .= " AND ".$DB->sql_like($fullname, ':keyword');
                $param['keyword'] = $keyword.'%';
            } else {
                $query .= " AND mnethostid = :mnethostid AND ".$DB->sql_like('u.username', ':username');
                
                $param['mnethostid'] = $CFG->mnet_localhost_id;
                $param['username'] = $keyword.'%';
            }
            
            
            $html = '';
            $query .= " ORDER BY u.id DESC";
            
            if ($lists = $DB->get_records_sql($query, $param)) {
                foreach ($lists as $l) {
                    $fullname = fullname($l);
                    
                    $html .= '<tr>';
                    $html .=    '<td class="text-center">'.$l->username.'</td>';
                    $html .=    '<td>'.$fullname.'</td>';
                    $html .=    '<td class="text-center">';
                    $html .=        '<button class="btn btn-default btn-sm btn-add" data-id="'.$l->id.'" data-fullname="'.strip_tags($fullname).'">'.get_string('add').'</button>';
                    $html .=    '</td>';
                    $html .= '</tr>';
                }
            }
            
            return Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'html' => $html));
        }
    }
    
    
    public function doAdminDelete()
    {
        global $CFG;
        
        $userid = Parameter::post('userid', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'userid' => array(
                'required', 'number',
                $this->validate()::PLACEHOLDER => get_string('userid', 'local_ubion'),
                $this->validate()::PARAMVALUE => $userid
            )
        ));
                
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            // 현재 등록되어 있는 admin 정보
            $admins = array();
            foreach (explode(',', $CFG->siteadmins) as $admin) {
                $admin = (int) $admin;
                if ($admin) {
                    $admins[$admin] = $admin;
                }
            }
            
            unset($admins[$userid]);
            set_config('siteadmins', implode(',', $admins));
            
            return Javascript::printJSON(array('code'=>Javascript::getSuccessCode(), 'msg' => get_string('delete_complete', 'local_ubion')));
        }
    }
}