<?php
namespace local_ubion\asite\course;


class Classify extends \local_ubion\asite\core\course\Classify
{
    private static $instance;
    
    /**
     * Classify
     * @return Classify
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
}