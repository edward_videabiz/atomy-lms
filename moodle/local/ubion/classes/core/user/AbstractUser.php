<?php
namespace local_ubion\core\user;

use local_ubion\base\Common;
use stdClass;

abstract class AbstractUser
{

    const TYPE_STUDENT = 10;

    // 학생
    const TYPE_PROFESSOR = 20;

    // 교수
    const TYPE_STAFF = 30;

    // 교직원
    const TYPE_ADMIN = 80;

    // 관리자
    const TYPE_ETC = 90;

    // 기타
    const TYPE_GUEST = 99;

    // 손님
    const GROUPCODE_U = 100;

    const GROUPCODE_G = 200;

    const LAYOUT_PROGRESS = 1;

    const LAYOUT_HAKGI = 2;

    public function getUserTypes()
    {
        $userType = array(
            self::TYPE_STUDENT => get_string('user_type_student', 'local_ubion'),
            self::TYPE_PROFESSOR => get_string('user_type_professor', 'local_ubion'),
            self::TYPE_STAFF => get_string('user_type_staff', 'local_ubion'),
            self::TYPE_ADMIN => get_string('user_type_admin', 'local_ubion'),
            self::TYPE_ETC => get_string('user_type_etc', 'local_ubion')
        );

        return $userType;
    }

    /**
     * user_ubion 정보를 리턴해줍니다.
     *
     * @param boolean $rebuild
     * @return mixed|NULL
     */
    function getUbion($rebuild = false)
    {
        global $USER, $DB;

        if (isloggedin() && ! isguestuser()) {

            // ubion 프로퍼티가 존재하지 않거나 user_bion 값이 존재하지 않을 경우
            if (! isset($USER->ubion) || ! isset($USER->ubion->user_ubion) || empty($USER->ubion->user_ubion) || $rebuild) {
                $this->setUbion();

                $query = "SELECT * FROM {user_ubion} WHERE userid = :userid";

                if ($user_ubion = $DB->get_record_sql($query, array(
                    'userid' => $USER->id
                ))) {
                    $USER->ubion->user_ubion = serialize($user_ubion);
                } else {
                    $USER->ubion->user_ubion = null;
                }
            } else {
                $user_ubion = unserialize($USER->ubion->user_ubion);
            }

            // 다중 신분 정보가 존재하는 경우
            if (! Common::isNullOrEmpty($user_ubion) && ! Common::isNullOrEmpty($user_ubion->rpst_member_key)) {
                // $USER->ubion->users 를 셋팅한 적이 없는 경우에만 진행 되어야 함.
                // 불필요하게 계속 select할 필요가 없음.
                if (! $USER->ubion->multi_users->select) {
                    // 1. 유보된 계정은 조회되지 않아야함.
                    // 2. use_yn이 Y인 계정만 조회되어야 함.
                    $query = "SELECT
									u.*
						FROM 		{user} u
						LEFT JOIN	{user_ubion} uu ON uu.userid = u.id
						WHERE		rpst_member_key = :rpst_member_key
						AND			deleted = 0
						AND			suspended = 0
						AND			uu.use_yn = 'Y'";

                    if ($multi_users = $DB->get_records_sql($query, array(
                        'rpst_member_key' => $user_ubion->rpst_member_key
                    ))) {
                        $multi_users_count = count($multi_users);
                        if ($multi_users_count > 1) {
                            $USER->ubion->multi_users->is = true;
                            $USER->ubion->multi_users->users = $multi_users;
                        }
                    }

                    // 최초 한번만 조회하도록 설정
                    $USER->ubion->multi_users->select = true;
                }
            }

            return $user_ubion;
        }

        return null;
    }

    /**
     * USER SESSION에 ubion property값을 셋팅해줍니다.
     */
    function setUbion()
    {
        global $USER;

        // 로그인 되어 있을 경우에만 해당.
        if (isloggedin() && ! isguestuser()) {
            // user session에 ubion 정보가 들어가있지 않으면 추가해줘야됨.
            if (! isset($USER->ubion)) {
                $USER->ubion = new stdClass();
                $USER->ubion->user_ubion = null;

                // course, courseall은 통합이 불가능합니다.
                // 상황에 따라 course, courseall을 각각 호출해야되는 경우가 있음.
                $USER->ubion->course = new stdClass();
                $USER->ubion->course->search = false;
                $USER->ubion->course->courses = array();
                $USER->ubion->course->time = time();

                $USER->ubion->courseall = new stdClass();
                $USER->ubion->courseall->search = false;
                $USER->ubion->courseall->courses = array();
                $USER->ubion->courseall->time = time();

                // 다중 신분 계정
                $USER->ubion->multi_users = new stdClass();
                $USER->ubion->multi_users->select = false;
                $USER->ubion->multi_users->is = false;
                $USER->ubion->multi_users->users = null;

                // 진도율 정보 기록
                // cmid => 출석상태
                $USER->ubion->progress = array();

                // 웹뷰에서 접속했는지 확인
                $USER->isWebview = false;
            }
        }
    }

    /**
     * 회원의 신분정보를 리턴해줍니다.
     *
     * @param string $type
     * @return string
     */
    function getTypeName($type)
    {
        $return = '&nbsp;';

        switch ($type) {
            case self::TYPE_STUDENT:
                $return = get_string('user_type_student', 'local_ubion');
                break;
            case self::TYPE_PROFESSOR:
                $return = get_string('user_type_professor', 'local_ubion');
                break;
            case self::TYPE_STAFF:
                $return = get_string('user_type_staff', 'local_ubion');
                break;
            case self::TYPE_ADMIN:
                $return = get_string('user_type_admin', 'local_ubion');
                break;
            case self::TYPE_GUEST:
                $return = get_string('user_type_guest', 'local_ubion');
                break;
            case self::TYPE_ETC:
            default:
                $return = get_string('user_type_etc', 'local_ubion');
                break;
        }

        return $return;
    }

    /**
     * 사용자 학번은 사용권한에 따라 노출할수 있는 길이를 지정해서 리턴받을수 있습니다.
     *
     * @param string $idnumber
     * @param number $cutLen
     * @return string
     */
    function getIdnumber($idnumber, $isProfessor = false, $cutLen = 4)
    {

        // 교수 및 담당 교수는 모든 학번을 표시.
        if ($isProfessor) {
            return $idnumber;
        } else {
            $strlen = strlen($idnumber);
            $substr = substr($idnumber, 0, $cutLen);
            return str_pad($substr, $strlen, '*');
        }
    }

    /**
     * 사용자 idnumber 또는 username을 리턴해줍니다.
     *
     * @param
     *            \stdClass object
     * @return string
     */
    function getIdnumberOrUsername($user)
    {
        if (empty($user->idnumber)) {
            return $user->username;
        } else {
            return $user->idnumber;
        }
    }

    /**
     * 사용자의 부서 정보를 리턴해줍니다.
     *
     * @param \stdClass $user
     * @return string
     */
    function getDepartment($user, $isNbsp = false)
    {
        global $DB;

        $departmentType = $this->getDepartmentType();

        if (isset($user->$departmentType)) {
            $department = $user->$departmentType;
        } else {
            $department = $DB->get_field_sql("SELECT " . $departmentType . " FROM {user} WHERE id = :id", array(
                'id' => $user->id
            ));
        }

        if ($isNbsp && empty($department)) {
            $department = '&nbsp;';
        }

        return $department;
    }

    /**
     * 학부/학과중 표시되어야 항목 값을 리턴시켜줍니다.
     *
     * @return string
     */
    protected function getDepartmentType()
    {
        return 'department';
    }

    /**
     * 사용자 사진 주소를 리턴해줍니다.
     *
     * @param \stdClass|number $userorid
     * @param number $size
     * @return \moodle_url
     */
    function getPicture($userorid, $size = 35)
    {
        global $PAGE, $DB;

        if (is_number($userorid)) {
            $field = "id, picture, firstname, lastname, firstnamephonetic, lastnamephonetic, middlename, alternatename, imagealt, email";
            $user = $DB->get_record_sql("SELECT " . $field . " FROM {user} WHERE id = :id", array(
                'id' => $userorid
            ));
        } else {
            $user = $userorid;
        }

        $userpicture = new \user_picture($user);
        $userpicture->size = $size;
        return $userpicture->get_url($PAGE);
    }

    function getGrade($grade, $isNbsp = false)
    {
        if ($isNbsp && empty($grade)) {
            $grade = '&nbsp;';
        }

        return $grade;
    }

    public function getMyCourseLayoutName()
    {
        return 'coursemos_layout';
    }

    /**
     * 대시보드에 표시될 레이아웃 형태
     *
     * @return int
     */
    public function getMyCourseLayout()
    {
        // 사용자가 선택한 내 강좌 레이아웃 형태
        $layout = get_user_preferences($this->getMyCourseLayoutName());

        // 사용자가 선택한 레이아웃이 없다면 기본값으로 설정
        if (empty($layout)) {
            $layout = get_config('local_ubion', 'user_course_layout');
        }

        // 기본값 조차도 설정이 안되어 있다면 진행강좌 목록으로 표시
        if (empty($layout)) {
            $layout = self::LAYOUT_PROGRESS;
        }

        return $layout;
    }

    /**
     * 강좌 즐겨찾기 기능 사용여부
     *
     * @return false
     */
    public function isFavorite()
    {
        return get_config('local_ubion', 'use_favorite');
    }
}