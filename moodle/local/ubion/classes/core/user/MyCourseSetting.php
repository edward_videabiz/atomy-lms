<?php
namespace local_ubion\core\user;

use local_ubion\base\Javascript;
use local_ubion\base\Parameter;

class MyCourseSetting extends \local_ubion\controller\Controller
{

    /**
     * 즐겨찾기한 강좌 목록
     *
     * @param int $userid
     * @return array
     */
    public function getCourses($userid = null)
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $fields = \local_ubion\course\Course::getInstance()->getCourseSelectField();

        $query = "SELECT
                            " . $fields . "
                  FROM      {course} c
                  JOIN      {course_ubion} cu ON c.id = cu.course
                  JOIN      {local_ubion_mycourse_setting} lums ON lums.courseid = cu.course
                  WHERE     c.visible = 1
                  AND       lums.userid = :userid
                  ORDER BY  lums.sortorder, lums.id";

        $param = array(
            'userid' => $userid
        );

        return $DB->get_records_sql($query, $param);
    }

    public function doCourseSortable()
    {
        global $DB, $USER;

        $courseidx = Parameter::post('courseidx', null, PARAM_NOTAGS);

        $validate = $this->validate()->make(array(
            'courseidx' => array(
                'required',
                $this->validate()::PLACEHOLDER => 'courseidx',
                $this->validate()::PARAMVALUE => $courseidx
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            // 전달된 courseidx를 분해해서 순서대로 저장 및 업데이트 해줘야됨.
            $courseOrders = explode('^', $courseidx);

            $i = 1;
            foreach ($courseOrders as $courseid) {
                if ($id = $DB->get_field_sql("SELECT id FROM {local_ubion_mycourse_setting} WHERE userid = :userid AND courseid = :courseid", array(
                    'userid' => $USER->id,
                    'courseid' => $courseid
                ))) {
                    $setting = new \stdClass();
                    $setting->id = $id;
                    $setting->sortorder = $i;
                    $DB->update_record('local_ubion_mycourse_setting', $setting);
                } else {
                    $setting = new \stdClass();
                    $setting->userid = $USER->id;
                    $setting->courseid = $courseid;
                    $setting->sortorder = $i;
                    $setting->id = $DB->insert_record('local_ubion_mycourse_setting', $setting);
                }

                $i ++;
            }

            // 강좌목록 순서 변경 시키는 로직 실행해야됨.
            // 세션 정보 초기화 => 정렬 완료 후 페이지 이동시 변경된 강좌 순서가 표시가 안되는 경우가 발생될수 있음.
            \local_ubion\course\Course::getInstance()->getMyCourses(array(), true);

            // 변경 이벤트 실행
            \local_ubion\event\user_mycourse_setting_changed::create(array(
                'context' => \context_user::instance($USER->id),
                'other' => array(
                    'type' => 'sortorder'
                )
            ))->trigger();

            Javascript::printAlert(get_string('save_complete', 'local_ubion'), true);
        }
    }
}