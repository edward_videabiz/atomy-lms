<?php
namespace local_ubion\core\user;

use local_ubion\base\Table;

/**
 * 과거강좌 조회 페이지
 *
 * @author akddd
 *        
 */
abstract class AbstractTransferTable extends Table
{

    public function __construct()
    {
        // 컬럼 숨기기 사용안함.
        $this->is_collapsible = false;

        parent::__construct('local-ubion-course-regular');
    }

    /**
     * Query the database for results to display in the table.
     *
     * @param int $pagesize
     *            size of page for paginated displayed table.
     * @param bool $useinitialsbar
     *            do you want to use the initials bar.
     */
    function query_db($pagesize, $useinitialsbar = false)
    {
        // table_sql class에서 일반 함수로 정의되어 있기 때문에
        // 이곳에서 query_db를 abstract 함수로 생성할수가 없음
        // 반드시 /local_ubion/course/TransferTable 에서 재정의 해주시기 바랍니다.
        throw new \moodle_exception('/local_ubion/course/TransferTable 에서 재정의 해주시기 바랍니다.');
    }
}