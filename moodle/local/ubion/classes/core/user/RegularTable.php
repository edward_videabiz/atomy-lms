<?php
namespace local_ubion\core\user;

use local_ubion\base\Table;

class RegularTable extends Table
{

    protected $CCourse = null;

    public function __construct(array $filters = null)
    {
        $this->CCourse = \local_ubion\course\Course::getInstance();

        $this->filters = $filters;

        // 컬럼 정의
        $this->addColumns('year', get_string('year', 'local_ubion'), 'text-center', 'wp-70');

        $this->addColumns('semester', get_string('semester', 'local_ubion'), 'text-center', 'wp-80');

        $this->addColumns('name', get_string('course_name', 'local_ubion'));

        // 입력한 컬럼 반영
        $this->setDefineColumns();

        // colgroup 출력
        $this->isPrintColgroup = true;

        // 컬럼 숨기기 사용안함.
        $this->is_collapsible = false;

        parent::__construct('local-ubion-course-regular');
    }

    /**
     * Query the database for results to display in the table.
     *
     * @param int $pagesize
     *            size of page for paginated displayed table.
     * @param bool $useinitialsbar
     *            do you want to use the initials bar.
     */
    public function query_db($pagesize, $useinitialsbar = false)
    {
        $CRegular = \local_ubion\course\Regular::getInstance();

        $courses = $CRegular->getUserCourses($this->getFilter('y'), $this->getFilter('s'));
        $total = count($courses);

        $this->pagesize($pagesize, $total);

        $this->rawdata = $courses;
    }

    public function col_semester($data)
    {
        return $this->CCourse->getSemesterName($data->semester_code);
    }

    public function col_name($data)
    {
        global $CFG;

        return '<a href="' . $CFG->wwwroot . '/course/view.php?id=' . $data->id . '">' . $this->CCourse->getName($data) . '</a>';
    }

    protected function getNotFoundMessage()
    {
        return get_string('no_mycourses', 'local_ubion');
    }
}