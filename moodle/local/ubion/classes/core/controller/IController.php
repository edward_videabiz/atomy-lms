<?php
namespace local_ubion\core\controller;

interface IController
{

    /**
     * POST시 $type값으로 전달된 값을 자동으로 controller와 매칭시켜줍니다.
     *
     * 반드시 실행된 전달된 type의 함수에서 redirect 또는 exit 구문을 생성해주시기 바랍니다.
     *
     * @param string $type
     */
    public function invoke($type);

    /**
     * Submit 되었는지를 판단함.
     *
     * @param string $type
     */
    public function isSubmit($type);
}