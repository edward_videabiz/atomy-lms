<?php
/**
 * restfull 형태로 개발할려고 했으나, 유지보수성을 위해서 최소한의 개념만 착용해서 코딩양을 줄이도록 개발했습니다.
 * 
 */
namespace local_ubion\core\controller;

use local_ubion\base\Javascript;
use local_ubion\base\Parameter;
use local_ubion\base\Validate;

class Controller implements IController
{

    const TYPENAME = 'coursemostype';

    const TYPENAME_NOPROGRESS = 'coursemostype_noprogress';

    const REPLACE = '##FIELD##';

    private $validate = null;

    /*
     * 2018-04-10 더이상 자동으로 invoke함수를 호출하지 않습니다.
     * => 개발 편의로 생성한건데, 유지보수할때 원치않게 invoke가 호출되거나 호출되지 않는 문제 찾기가 어려워할거 같아서 수동으로 호출되도록 변경함.
     * public function __construct($isAutoSubmit = true, $type = null)
     * {
     * // 전달된 type값이 없으면 파라메터에서 찾아와야됨.
     * if (empty($type)) {
     * $type = optional_param(self::TYPENAME, null, PARAM_ALPHANUM);
     * }
     *
     * if ($isAutoSubmit && $this->isSubmit($type)) {
     * $this->invoke($type);
     * }
     * }
     */

    /**
     * 2018-04-10 더이상 자동으로 invoke함수를 호출하지 않습니다.
     *
     * invoke 함수가 실행되어야 하는지 확인하는 함수
     *
     * A라는 클래스가 Controller를 상속하고 있고 coursemostype값이 전달이 되었는데 A 클래스 내에서 doXX 함수를 구현하지 않았아
     * B클래스의 doXXX를 호출되는 경우가 발생됨.
     *
     * 이를 방지하기 위해서 $_SERVER['REQUEST_URI']값고 호출된 클래스의 네임스페이스를 비교해서 호출 여부를 결정함.
     *
     * 필요에 따라 Controller를 상속하는 함수에서 override 하시면 됩니다.
     *
     * @deprecated 더이상 사용되지 않습니다.
     * @return boolean protected function isCallInvoke()
     *         {
     *         $parentClass = get_parent_class($this);
     *        
     *         // 첫번째 네임스페이스만 출력
     *         $namespace = current(explode('\\', $parentClass));
     *        
     *         $url = $namespace;
     *         if (strpos($namespace, '_') !== false) {
     *         $explode = explode('_', $namespace);
     *        
     *         $url = '';
     *         $slash = '';
     *         foreach ($explode as $e) {
     *         $url .= $slash.$e;
     *         $slash = '/';
     *         }
     *         }
     *        
     *         return (strpos($_SERVER['REQUEST_URI'], $url) !== false) ? true : false;
     *         }
     */

    /**
     * coursemostype값으로 전달된 파라메터를 class내의 함수와 연결 시켜줍니다.
     *
     * @see \local_ubion\core\controller\IController::invoke()
     */
    public function invoke($type)
    {
        $cancel = optional_param('cancel', 0, PARAM_RAW);

        // type값 자체가 존재하지 않는 경우에는 controller의 함수가 실행되면 안됨.
        if (! empty($type) && empty($cancel)) {
            // if ($this->isCallInvoke()) {
            // class내의 함수는 알파벳과 숫자로만 이루어져야됨.
            $functionName = 'do' . ucfirst($type);

            // 함수가 존재할 경우 실행
            // 추가 로직 후 redirect 등의 행위가 필요할수가 있기 때문에 $type값을 그대로 실행시키면 안됨.
            if (method_exists($this, $functionName)) {
                $this->$functionName();
            } else {
                Javascript::printJSON(array(
                    'code' => Javascript::getFailureCode(),
                    'msg' => get_string('not_found_coursemostype', 'local_ubion', $type)
                ));
            }
            // }
        }
    }

    /**
     * submit 여부
     *
     * @see \local_ubion\core\controller\IController::isSubmit()
     */
    public function isSubmit($type)
    {
        if (! empty($type)) {
            $postCount = count($_POST);

            if ($postCount > 0) {
                // submit된 대상이 한개라도 존재하는 경우
                $sesskey = Parameter::post('sesskey', 'a', PARAM_RAW);

                // csrf 공격을 막기 위해서 sesskey값도 검사해야됨.
                if (confirm_sesskey($sesskey)) {
                    return true;
                }
                /*
                 * 이부분은 좀 더 고려해봐야할듯함.
                 * else {
                 * if (Parameter::isAajx()) {
                 * Javascript::printJSON(array('code'=>'310', 'msg'=> get_string('not_found_coursemostype', 'local_ubion', $type), 'loginpage'=>get_login_url()));
                 * } else {
                 * redirect(get_login_url());
                 * }
                 * }
                 */
            }
        }

        return false;
    }

    public function validate()
    {
        if (is_null($this->validate)) {
            $this->validate = new Validate();
        }

        return $this->validate;
    }
}