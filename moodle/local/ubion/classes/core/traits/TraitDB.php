<?php
namespace local_ubion\core\traits;

use local_ubion\base\Javascript;
use local_ubion\base\Parameter;

/**
 * 특정 DB 테이블에 대해서 POST된 값으로 override 하려고 할때 사용하시면 됩니다.
 */
trait TraitDB
{

    // Trait에는 const를 사용할수 없음.
    private $NOSUBMITCOLUMN = 'NOSUBMITCOLUMN';

    public function getDBNoSubmitColumn()
    {
        return $this->NOSUBMITCOLUMN;
    }

    public function getDBHiddenColumns()
    {
        return array(
            'id',
            'course'
        );
    }

    public function getDBUnixtimeColumn()
    {
        return array(
            'study_start',
            'study_end'
        );
    }

    public function setDBUpdate($tableName)
    {
        global $DB;

        // 변경 여부 전달을 위해서 변수 선언
        $isUpdate = false;

        $id = Parameter::post('id', null, PARAM_INT);

        // 필수값 검사
        $validate = $this->validate()->make(array(
            'id' => array(
                'required',
                $this->validate()::PLACEHOLDER => 'id',
                $this->validate()::PARAMVALUE => $id
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            // id값을 가지고 변경하려는 테이블의 old값 가져오기
            $oldDB = $DB->get_record_sql("SELECT * FROM {" . $tableName . "} WHERE id = :id", array(
                'id' => $id
            ));

            // 이전 페이지에서 전달된 id값이 변경하려는 테이블에서 조회되지 않으면 오류라고 봐야됨.
            if (empty($oldDB)) {
                Javascript::printAlert('등록된 ' . $tableName . '값이 없습니다. id => ' . $id);
            } else {

                // 업데이트 되면 안되는 컬럼 목록
                $hiddenColumns = $this->getDBHiddenColumns();

                // unixtimecolumns
                $unixtimeColumns = $this->getDBUnixtimeColumn();

                $updateDB = new \stdClass();
                $updateDB->id = $id;

                // oldUserUbion의 컬럼명을 토대로 post된 값을 찾음
                // oldUserUbion에 존재하지 않는 컬럼명이 전달된 경우는 무시 (어짜피 디비에 존재하지 않는 컬럼이기 저장자체가 안됨)
                foreach ($oldDB as $oldColumn => $oldValue) {

                    if (in_array($oldColumn, $hiddenColumns)) {
                        continue;
                    }

                    // db에 존재하는 course_ubion 컬럼 값을 가지고 post된 값을 찾음.
                    $newValue = Parameter::post($oldColumn, $this->getDBNoSubmitColumn(), PARAM_NOTAGS);

                    // unixtime형태라면 전달된 값에 - 항목이 포함되어 있으면 strtotime을 진행해줘야됨.
                    if (in_array($oldColumn, $unixtimeColumns) && strpos($newValue, '-') !== false) {
                        $newValue = strtotime($newValue);
                    }

                    // 정상적으로 수정 페이지에서 전달된 항목의 값인 경우 $updateDB에 반영
                    // 기존 값이랑 변경사항이 없는 경우 저장
                    if ($newValue != $this->getDBNoSubmitColumn() && $oldValue != $newValue) {
                        $updateDB->$oldColumn = $newValue;

                        // 한개 항목이라도 수정이 된 경우에만 update 쿼리가 실행되어야 함.
                        $isUpdate = true;
                    }
                }

                if ($isUpdate) {
                    $DB->update_record($tableName, $updateDB);
                }
            }

            return $isUpdate;
        }
    }
}