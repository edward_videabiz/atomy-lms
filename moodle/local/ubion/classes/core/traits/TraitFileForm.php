<?php
namespace local_ubion\core\traits;

trait TraitFileForm
{

    /**
     * 기본 파일폼 설정
     * 기본값 그대로 사용해도 되지만 상황에 따라 Trait를 상속 받은 곳에서 재정의가 필요할수 있습니다.
     *
     * @param array $addOpeion
     * @return array
     */
    public function getMoodleFileFormOption(array $addOpeion = array())
    {
        global $CFG;
        require_once $CFG->dirroot . '/repository/lib.php';

        $maxbytes = get_max_upload_file_size();

        $options = array(
            'subdirs' => false,
            'maxfiles' => 1,
            'maxbytes' => $maxbytes
        );

        if (! empty($addOpeion)) {
            $options = array_merge($options, $addOpeion);
        }

        return $options;
    }

    /**
     * 파일폼 객체를 리턴해줍니다.
     *
     * @param string $name
     * @param array $options
     * @return \MoodleQuickForm_filemanager
     */
    public function getMoodleFileForm($name, array $options = array())
    {
        global $CFG;
        require_once ($CFG->dirroot . '/lib/form/filemanager.php');

        $id = 'fileform-' . $name;

        // 해당 함수를 사용하는 목적이 에디터 폼만 따로 출력하기 위함이기 때문에 따로 label에 대해서는 출력시키지 않습니다.
        $label = null;

        // option값이 따로 설정이 안되어 있다면 기본 옵션값으로 설정
        if (empty($options)) {
            $options = $this->getMoodleFileFormOption();
        }

        $fileform = new \MoodleQuickForm_filemanager($name, $label, array(
            'id' => $id
        ), $options);
        return $fileform;
    }

    /**
     * 파일폼에 본문 내용을 반영해줌 (수정시 사용)
     *
     * @param \MoodleQuickForm_filemanager $fileform
     * @param string $text
     * @param number $contextid
     * @param string $component
     * @param string $filearea
     * @param number $itemid
     * @param array $options
     * @return \\MoodleQuickForm_filemanager
     */
    public function setMoodleFileFormContent(\MoodleQuickForm_filemanager $fileform, $contextid, $component, $filearea, $itemid, array $options = array())
    {
        $draftid = file_get_submitted_draft_itemid($fileform->getName());

        file_prepare_draft_area($draftid, $contextid, $component, $filearea, $itemid, $options);
        $fileform->setValue($draftid);

        return $fileform;
    }
}