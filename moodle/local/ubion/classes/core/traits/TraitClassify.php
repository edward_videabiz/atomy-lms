<?php
namespace local_ubion\core\traits;

trait TraitClassify
{

    /**
     * 분류 가져오기
     *
     * @param string $coursetype
     * @return array
     */
    public function getClassifies($coursetype)
    {
        global $DB;

        $query = "SELECT
                            *
                  FROM      {local_ubion_classify}
                  WHERE     course_type = :coursetype
                  AND       visible = 1";

        $param = array(
            'coursetype' => $coursetype
        );

        $order = ' ORDER BY sortorder';

        return $DB->get_records_sql($query . $order, $param);
    }

    /**
     * 모든 분류 가져오기
     *
     * @param string $coursetype
     * @return array
     */
    public function getClassifiesAll($coursetype = null)
    {
        global $DB;

        $query = "SELECT
                            *
                  FROM      {local_ubion_classify}";

        $param = array();
        if (! empty($coursetype)) {
            $query .= " WHERE course_type = :course_type";
            $param['course_type'] = $coursetype;
        }

        $order = ' ORDER BY sortorder';

        return $DB->get_records_sql($query . $order, $param);
    }

    /**
     * 모든 분류 페이징 처리
     *
     * @param number $page
     * @param number $ls
     * @return array
     */
    public function getClassifiesPaging($page = 1, $ls = 15, $coursetype = null)
    {
        global $DB;

        $replace = \local_ubion\controller\Controller::REPLACE;
        $query = "SELECT
                            " . $replace . "
                  FROM      {local_ubion_classify}";

        $param = array();
        if (! empty($coursetype)) {
            $query .= " WHERE course_type = :course_type";
            $param['course_type'] = $coursetype;
        }

        $totalQuery = str_replace($replace, "COUNT(1)", $query);
        $totalCount = $DB->get_field_sql($totalQuery, $param);

        $query = str_replace($replace, "*", $query);
        $order = ' ORDER BY id';

        $limitfrom = ($page > 0) ? $ls * ($page - 1) : 0;

        $lists = $DB->get_records_sql($query . $order, $param, $limitfrom, $ls);

        return array(
            $totalCount,
            $lists
        );
    }
}