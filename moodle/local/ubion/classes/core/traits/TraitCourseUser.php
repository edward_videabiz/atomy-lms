<?php
namespace local_ubion\core\traits;

use context_course;

trait TraitCourseUser
{

    /**
     * 기본 정렬 컬럼
     *
     * @return string
     */
    public function getCourseUserDefaultSortColumn()
    {
        return 'idnumber';
    }

    /**
     * 기본 정렬 방법
     *
     * @return string
     */
    public function getCourseUserDefaultSortText()
    {
        return 'ASC';
    }

    /**
     * 강좌를 수강하고 있는 수강생 정보 쿼리 및 파라메터를 리턴해줍니다.
     *
     * @param int $courseorid
     * @param int $groupid
     * @param string $keyfield
     * @param string $keyword
     * @param int|string $roleid
     *
     * @return multitype:string multitype:string unknown number
     */
    function getCourseUserQuery($courseorid, array $addField = null, $groupid = null, $keyfield = null, $keyword = null, $roleid = null, $isRoleEqual = true)
    {
        global $DB;

        /*
         * if (is_number($course)) {
         * $query = "SELECT * FROM {course} WHERE id = :id";
         * $course = $DB->get_record_sql($query, array('id'=>$course));
         * }
         */
        if (is_object($courseorid)) {
            $courseid = $courseorid->id;
        } else {
            $courseid = $courseorid;
        }

        // 강좌 context_id값을 구해옴.
        $context = context_course::instance($courseid);

        $field = array(
            'DISTINCT u.*'
        );
        $join = array();
        $where = array(
            'ra.contextid = :contextid'
        );
        $param = array(
            'contextid' => $context->id
        );

        // 강좌가 그룹모드를 사용하는 경우
        // 리턴값에 그룹 값도 표시해주는게 좋을듯 싶음.
        if (! empty($groupid)) {
            $join[] = 'JOIN {groups_members} gm ON gm.userid = u.id AND gm.groupid = :groupid';
            $param['groupid'] = $groupid;
            $field[] = 'gm.groupid';
        }

        // roleid값이 빈값이면 기본 학생권한(학생, 청강생) 조회해야됨.
        if (empty($roleid)) {
            $roleid = $this->getStudentRoleID();
        }

        // 전달된 값이 존재하는 경우
        if (! empty($roleid)) {
            // -1은 모든 사용자(교수자 포함) 검색임
            if ($roleid != - 1) {
                if ($isRoleEqual) {
                    // 숫자 형태로 넘어온 경우
                    // get_config('core', 'gradebookroles') 값에 의해서 string형태로 넘어올수도 있음.
                    if (is_number($roleid)) {
                        $where[] = 'roleid = :roleid';
                        $param['roleid'] = $roleid;
                    } else {
                        $where[] = 'roleid IN (' . $roleid . ')';
                    }
                } else {
                    // 숫자 형태로 넘어온 경우
                    // get_config('core', 'gradebookroles') 값에 의해서 string형태로 넘어올수도 있음.
                    if (is_number($roleid)) {
                        $where[] = 'roleid != :roleid';
                        $param['roleid'] = $roleid;
                    } else {
                        $where[] = 'roleid NOT IN (' . $roleid . ')';
                    }
                }
            }
        }

        // 검색인 경우
        if (! empty($keyfield) && ! empty($keyword)) {
            switch ($keyfield) {
                case 'idnumber':
                    $where[] = $DB->sql_like('idnumber', ':idnumber');

                    $param['idnumber'] = $keyword . '%';
                    break;
                default:
                    $fullname = $DB->sql_fullname('u.firstname', 'u.lastname');
                    $where[] = $DB->sql_like('LOWER(' . $fullname . ')', ':keyword');

                    $param['keyword'] = '%' . strtolower($keyword) . '%';
                    break;
            }
        }

        // user_ubion 정보도 가져와야됨.
        $join[] = 'LEFT JOIN	{user_ubion} uu ON uu.userid = u.id';

        // 추가적으로 join 해야될 항목이 존재하는 경우
        $addJoin = $this->getAddJoin();
        if (! empty($addJoin)) {
            if (is_array($addJoin)) {
                $join = array_merge($join, $addJoin);
            } else {
                $join[] = $addJoin;
            }
        }

        // 추가적으로 조회해야되는 컬럼이 존재하는 경우
        if (! empty($addField)) {
            $field = array_merge($field, $addField);
        }

        // 쿼리 조합.
        $query = "SELECT
						" . join(', ', $field) . "
				FROM		{user} u
				LEFT JOIN	{role_assignments} ra ON ra.userid = u.id
				" . join("\n ", $join) . '
				WHERE ' . join(" \nAND ", $where);

        // error_log($query);
        // error_log(print_r($param, true));
        return array(
            $query,
            $param
        );
    }

    public function getAddJoin()
    {
        return null;
    }

    /**
     * 강좌를 수강하고 있는 수강생 모두를 리턴해줍니다.
     *
     * @param int|object $course
     * @param string $order
     * @param int $groupid
     * @param int|string $roleid
     * @return multitype:
     */
    function getCourseUserAll($courseorid, array $addField = null, $groupid = null, $order = 'idnumber', $sort = 'ASC', $roleid = null)
    {
        global $DB;

        list ($query, $param) = $this->getCourseUserQuery($courseorid, $addField, $groupid, null, null, $roleid);

        // 정렬값이 빈값이면 기본값으로 설정된 컬럼, 정렬 방법으로 설정
        if (empty($order)) {
            $order = $this->getCourseUserDefaultSortColumn();
            $sort = $this->getCourseUserDefaultSortText();
        } else {
            $sort = strtoupper($sort);
            if ($sort != 'ASC' && $sort != 'DESC') {
                $sort = 'ASC';
            }
        }

        $order = ' ORDER BY ' . $order . ' ' . $sort;
        ;

        // Common::print_r($query.$order);
        // Common::print_r($param);
        return $DB->get_records_sql($query . $order, $param);
    }

    /**
     * 강좌를 수강하고 있는 수강생 정보를 리턴해줍니다.
     *
     * @param int $course
     * @param array $addFiled
     * @param string $keyfield
     * @param string $keyword
     * @param int $groupid
     * @param int|string $roleid
     * @param int $page
     * @param int $ls
     * @param string $order
     * @param string $sort
     * @return multitype:unknown Ambigous <mixed, boolean>
     */
    function getCourseUsers($course, array $addField = null, $keyfield = null, $keyword = null, $groupid = null, $roleid = null, $page = 1, $ls = 20, $order = 'idnumber', $sort = 'ASC', $isRoleEqual = true)
    {
        global $DB;

        list ($query, $param) = $this->getCourseUserQuery($course, $addField, $groupid, $keyfield, $keyword, $roleid, $isRoleEqual);

        $totalcountsql = "SELECT COUNT(1) FROM (" . $query . ") a";

        // 전체 갯수
        $totalCount = $DB->get_field_sql($totalcountsql, $param);

        // 정렬값이 빈값이면 기본값으로 설정된 컬럼, 정렬 방법으로 설정
        if (empty($order)) {
            $order = $this->getCourseUserDefaultSortColumn();
            $sort = $this->getCourseUserDefaultSortText();
        } else {
            $sort = strtoupper($sort);
            if ($sort != 'ASC' && $sort != 'DESC') {
                $sort = 'ASC';
            }
        }

        // $order값이 fullname인 경우에는 다국어 적용해야됨.
        if ($order == 'fullname') {
            if (current_language() == 'ko') {
                $order = 'firstname';
            } else {
                $order = 'lastname';
            }
        }
        $order = ' ORDER BY ' . $order . ' ' . $sort;
        ;

        if (empty($ls)) {
            $list = $DB->get_records_sql($query . $order, $param);
        } else {
            // 페이징을 위한 변수 설정
            $page = ($page <= 0) ? 1 : $page;
            $limitfrom = $ls * ($page - 1);

            // error_log($query.$order);
            // error_log(print_r($param, true));
            $list = $DB->get_records_sql($query . $order, $param, $limitfrom, $ls);
        }

        return array(
            $totalCount,
            $list
        );
    }

    /**
     * 수강생(성적 부여대상자)로 인정되는 사용자 roleid를 리턴해줍니다.
     *
     * @return string
     */
    function getStudentRoleID()
    {
        return get_config('core', 'gradebookroles');
    }
}