<?php
namespace local_ubion\core\traits;

trait TraitPush
{

    public function getPushMessageCode()
    {
        // trait에서는 const 선언이 안되기 때문에 함수로 처리
        return 3;
    }

    public function getPushNotificationCode()
    {
        // trait에서는 const 선언이 안되기 때문에 함수로 처리
        return 4;
    }

    abstract public function pushParameter();

    /**
     * 푸시 서버로 전송
     *
     * @param string $parameter
     * @return boolean[]|NULL[]
     */
    public function push($parameter)
    {
        $config = get_config('local_ubion');
        
        $isSuccess = true;
        $result = null;

        // host, port, school값이 존재하는 경우에만 푸시 정보 전달.
        if (isset($config->host) && !empty($config->port) && !empty($config->school)) {
            $host = $config->push_host . ':' . $config->push_port;
            $path = "/message";
    
            $baseParameter = "school=" . $config->push_school; // 고정값임
            $parameter = $baseParameter . "&" . $parameter;
    
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $host.$path);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5); // timeout은 5초로 설정.
            $postResult = curl_exec($ch);
            // $info = curl_getinfo($ch);
             
            $isSuccess = false;
            if (curl_errno($ch)) {
                $result = curl_errno($ch).' : '.curl_error($ch);
                error_log(print_r($result, true));
            } else {
                $isSuccess = true;
                $result = json_decode($postResult);
            }
             
            curl_close($ch);
        }
        
        return array(
            $isSuccess,
            $result
        );
    }
}