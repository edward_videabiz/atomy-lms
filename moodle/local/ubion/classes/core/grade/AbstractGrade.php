<?php
namespace local_ubion\core\grade;

use grade_item;
use grade_category;

abstract class AbstractGrade
{

    /**
     * 성적 항목 추가
     *
     * @param int $courseid
     * @param string $itemname
     * @param int $grademax
     * @param string $idnumber
     * @param number $parent
     * @return number
     */
    function setItemInsert($courseid, $itemname, $grademax, $idnumber = null, $parent = 0)
    {
        return $this->setItem($courseid, $itemname, $grademax, $idnumber, null, $parent);
    }

    /**
     * 성적 항목 수정
     *
     * @param int $courseid
     * @param string $itemname
     * @param int $grademax
     * @param string $idnumber
     * @param int $gradeid
     * @return number
     */
    function setItemUpdate($courseid, $itemname, $grademax, $idnumber, $gradeid)
    {
        return $this->setItem($courseid, $itemname, $grademax, $idnumber, $gradeid);
    }

    /**
     * 성적 항목 추가/수정
     *
     * @param int $courseid
     * @param string $idnumber
     * @param string $itemname
     * @param float $grademax
     * @param int $gradeid
     * @param int $parent
     * @return number
     */
    function setItem($courseid, $itemname, $grademax, $idnumber = null, $gradeid = 0, $parent = 0)
    {
        global $CFG;

        require_once $CFG->dirroot . '/grade/lib.php';

        $gradeitem_param = new \stdClass();
        $gradeitem_param->gradetype = GRADE_TYPE_VALUE; // 성적유형 (기본값 : 값)
        $gradeitem_param->itemname = $itemname; // 성적 이름
        $gradeitem_param->idnumber = $idnumber; // 항목 정보
        $gradeitem_param->grademax = unformat_float($grademax); // 최고성적
        $gradeitem_param->grademin = unformat_float(0); // 최저성적
        $gradeitem_param->gradepass = unformat_float(0); // 통과성적
        $gradeitem_param->aggregationcoef = unformat_float(0); // 가산점수
                                                               // $gradeitem_param->aggregationcoef2 = unformat_float(0); // 가중치

        $gradeitem_param->display = 0; // 성적 표시 형식 (기본값 : 실점수)
        $gradeitem_param->gradepass = 0; // 통과 점수

        $grade_item = new grade_item(array(
            'id' => $gradeid,
            'courseid' => $courseid
        ));
        grade_item::set_properties($grade_item, $gradeitem_param);
        $grade_item->outcomeid = null;

        if (empty($gradeid)) {
            $grade_item->itemtype = 'manual'; // all new items to be manual only
            $grade_item->insert();

            // 상위 카테고리 값이 지정된 경우
            // 참고로 parent 항목 설정은 성적항목 추가할 때에만 설정 가능합니다. (변경화면에서는 상위 카테고리 변경 기능이 존재하지 않음)
            if (! empty($parent)) {
                // 실제 해당 강좌에 존재하는 카테고리인지 확인이 필요함.
                $grade_item->set_parent($parent, false);
            }
        } else {
            $grade_item->update();
        }

        $grade_item->set_hidden(0, false);
        $grade_item->set_locktime(0); // locktime first - it might be removed when unlocking
        $grade_item->set_locked(0, false, true);

        return $grade_item->id;
    }

    /**
     * 성적 카테고리 추가
     *
     * @param number $courseid
     * @param string $categoryname
     * @param float $grademax
     * @param string $idnumber
     * @param number $aggregation
     * @param boolean $isCompel
     * @return number
     */
    function setCategoryInsert($courseid, $categoryname, $grademax, $idnumber = null, $aggregation = null, $isCompel = false)
    {
        return $this->setCategory($courseid, $categoryname, $grademax, $idnumber, $aggregation, $isCompel);
    }

    /**
     * 성적 카테고리 수정
     *
     * @param int $courseid
     * @param string $categoryname
     * @param float $grademax
     * @param int $categoryid
     * @return number
     */
    function setCategoryUpdate($courseid, $categoryname, $grademax, $categoryid, $idnumber = null, $aggregation = null, $isCompel = false)
    {
        return setCategory($courseid, $categoryname, $grademax, $categoryid, $idnumber, $aggregation, $isCompel);
    }

    /**
     * 성적 카테고리 추가/수정
     *
     * @param int $courseid
     * @param string $categoryname
     * @param float $grademax
     * @param int $categoryid
     * @param number $idnumber
     * @param number $aggregation
     * @param boolean $isCompel
     * @return number
     */
    function setCategory($courseid, $categoryname, $grademax, $categoryid = null, $idnumber = null, $aggregation = null, $isCompel = false)
    {
        global $CFG;

        require_once $CFG->dirroot . '/grade/lib.php';

        // grade/edit/tree/category.php 내용 발췌함.
        if (empty($categoryid)) {
            $grade_category = new grade_category(array(
                'courseid' => $courseid
            ), false);
            $grade_category->apply_default_settings();
            $grade_category->apply_forced_settings();
        } else {
            if (! $grade_category = grade_category::fetch(array(
                'id' => $categoryid,
                'courseid' => $courseid
            ))) {
                print_error('invalidcategory');
            }
        }

        // 강제 적용이 아닌 경우 성적 설정에 존재하는 집계 방식이 선택되어야 함.
        if (! $isCompel) {
            // 집계 전략 설정
            // 집계전략 파라메터가 전달된 경우 실제 무들에서 제공해주는 집계 방식중 한개인지 확인해봐야됨.
            if ($aggregation) {
                $gradeAggregationsVisible = get_config('core', 'grade_aggregations_visible');
                $aggVisibleExplode = explode(',', $gradeAggregationsVisible);

                // 존재하지 않는 성적집계방식 값이면 null처리 (아래에서 기본값으로 설정함)
                if (! in_array($aggregation, $aggVisibleExplode)) {
                    $aggregation = null;
                }
            }
        }

        // 집계 전략 관련 전달된 파라메터값이 전달되지 않거나, 존재하지 않는 집계 전략이 전달된 경우에는 설정된 기본 집계방식으로 셋팅되어야 함.
        if (is_null($aggregation)) {
            $aggregation = get_config('core', 'grade_aggregation');
        }

        $gc = new \stdClass();
        $gc->fullname = $categoryname;
        $gc->aggregation = $aggregation; // 집계 전략
        $gc->aggregateonlygraded = 1; // 하위카테고리를 포함한 집계
        $gc->aggregateoutcomes = 0;

        // grade_item 관련
        $gc->grade_item_gradetype = 1;
        $gc->grade_item_idnumber = $idnumber;
        $gc->grade_item_grademax = $grademax; // 최고 성적
        $gc->grade_item_display = 0;
        $gc->grade_item_decimals = - 1;

        grade_category::set_properties($grade_category, $gc);

        if (empty($categoryid)) {
            $grade_category->insert();
        } else {
            $grade_category->update();
        }

        // / GRADE ITEM
        // grade item data saved with prefix "grade_item_"
        $itemdata = new \stdClass();
        foreach ($gc as $k => $v) {
            if (preg_match('/grade_item_(.*)/', $k, $matches)) {
                $itemdata->{$matches[1]} = $v;
            }
        }

        if (! isset($itemdata->aggregationcoef)) {
            $itemdata->aggregationcoef = 0;
        }

        if (! isset($itemdata->gradepass) || $itemdata->gradepass == '') {
            $itemdata->gradepass = 0;
        }

        if (! isset($itemdata->grademax) || $itemdata->grademax == '') {
            $itemdata->grademax = 0;
        }

        if (! isset($itemdata->grademin) || $itemdata->grademin == '') {
            $itemdata->grademin = 0;
        }

        $convert = array(
            'grademax',
            'grademin',
            'gradepass',
            'multfactor',
            'plusfactor',
            'aggregationcoef',
            'aggregationcoef2'
        );
        foreach ($convert as $param) {
            if (property_exists($itemdata, $param)) {
                $itemdata->$param = unformat_float($itemdata->$param);
            }
        }

        // When creating a new category, a number of grade item fields are filled out automatically, and are required.
        // If the user leaves these fields empty during creation of a category, we let the default values take effect
        // Otherwise, we let the user-entered grade item values take effect
        $grade_item = $grade_category->load_grade_item();
        $grade_item_copy = fullclone($grade_item);
        \grade_item::set_properties($grade_item, $itemdata);

        if (empty($grade_item->id)) {
            $grade_item->id = $grade_item_copy->id;
        }
        if (empty($grade_item->grademax) && $grade_item->grademax != '0') {
            $grade_item->grademax = $grade_item_copy->grademax;
        }
        if (empty($grade_item->grademin) && $grade_item->grademin != '0') {
            $grade_item->grademin = $grade_item_copy->grademin;
        }
        if (empty($grade_item->gradepass) && $grade_item->gradepass != '0') {
            $grade_item->gradepass = $grade_item_copy->gradepass;
        }
        if (empty($grade_item->aggregationcoef) && $grade_item->aggregationcoef != '0') {
            $grade_item->aggregationcoef = $grade_item_copy->aggregationcoef;
        }

        // Handle null decimals value - must be done before update!
        if (! property_exists($itemdata, 'decimals') or $itemdata->decimals < 0) {
            $grade_item->decimals = null;
        }

        // Change weightoverride flag. Check if the value is set, because it is not when the checkbox is not ticked.
        $itemdata->weightoverride = isset($itemdata->weightoverride) ? $itemdata->weightoverride : 0;
        if ($grade_item->weightoverride != $itemdata->weightoverride && $grade_category->aggregation == GRADE_AGGREGATE_SUM) {
            // If we are using natural weight and the weight has been un-overriden, force parent category to recalculate weights.
            $grade_category->force_regrading();
        }
        $grade_item->weightoverride = $itemdata->weightoverride;

        $grade_item->outcomeid = null;

        if (! empty($gc->grade_item_rescalegrades) && $gc->grade_item_rescalegrades == 'yes') {
            $grade_item->rescale_grades_keep_percentage($grade_item_copy->grademin, $grade_item_copy->grademax, $grade_item->grademin, $grade_item->grademax, 'gradebook');
        }

        // update hiding flag
        $grade_item->set_hidden(0, false);
        $grade_item->set_locktime(0); // locktime first - it might be removed when unlocking
        $grade_item->set_locked(0, false, true);

        $grade_item->update(); // We don't need to insert it, it's already created when the category is created

        return $grade_category->id;
    }
}