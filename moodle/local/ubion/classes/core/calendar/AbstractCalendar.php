<?php
namespace local_ubion\core\calendar;

use \local_ubion\base\Common;
use context_course;
use context_system;
use local_ubion\controller\Controller;

abstract class AbstractCalendar {
    /**
     * 전달된 강좌(그룹, 사용자등)에 특정 기간동안 존재하는 일정 목록을 가져옵니다.
     * 
     * @param array|boolean $courses
     * @param array|boolean $groups
     * @param array|boolean $users
     * @param number $timestart
     * @param number $timeend
     * @param number $page
     * @param number $maxlimit
     * @param boolean $withduration
     * @param boolean $ignorehidden
     * @return array
     */
    function getLists($courses, $groups=false, $users=null, $timestart, $timeend=null, $page=0, $maxlimit=5, $withduration=true, $ignorehidden=true)
    {
        global $DB, $USER;
        
        // Quick test
        if (is_bool($users) && is_bool($groups) && is_bool($courses)) {
            return array();
        }
        
        // user값이 전달되지 않았으면 사용자 정보까지 포함 되어야 함.
        if ($users === null) {
            $users = $USER->id;
        }
        
        
        
        $whereclause = '';
        // user값이 false값이 떨어지면
        
        if (is_array($users) && !empty($users)) {
            // Events from a number of users
            if (!empty($whereclause)) $whereclause .= ' OR';
            $whereclause .= ' (userid IN ('.implode(',', $users).') AND courseid = 0 AND groupid = 0)';
        } else if (is_numeric($users)) {
            // Events from one user
            if (!empty($whereclause)) {
                $whereclause .= ' OR';
            }
            $whereclause .= ' (userid = '.$users.' AND courseid = 0 AND groupid = 0)';
        } else if ($users === true) {
            // Events from ALL users
            if (!empty($whereclause)) {
                $whereclause .= ' OR';
            }
            $whereclause .= ' (userid != 0 AND courseid = 0 AND groupid = 0)';
        } else if ($users === false) {
            // No user at all, do nothing
        }
        
        // 그룹
        if (is_array($groups) && !empty($groups)) {
            // Events from a number of groups
            if (!empty($whereclause)) $whereclause .= ' OR';
            $whereclause .= ' groupid IN ('.implode(',', $groups).')';
        } else if (is_numeric($groups)) {
            // Events from one group
            if (!empty($whereclause)) $whereclause .= ' OR ';
            $whereclause .= ' groupid = '.$groups;
        } else if ($groups === true) {
            // Events from ALL groups
            if (!empty($whereclause)) $whereclause .= ' OR ';
            $whereclause .= ' groupid != 0';
        }
        // boolean false (no groups at all): we don't need to do anything
        
        if (is_array($courses) && !empty($courses)) {
            if (!empty($whereclause)) {
                $whereclause .= ' OR';
            }
            
            $whereclause .= ' (groupid = 0 AND courseid IN ('.implode(',', $courses).'))';
        } else if (is_numeric($courses)) {
            // One course
            if (!empty($whereclause)) $whereclause .= ' OR';
            $whereclause .= ' (groupid = 0 AND courseid = '.$courses.')';
        } else if ($courses === true) {
            // Events from ALL courses
            if (!empty($whereclause)) $whereclause .= ' OR';
            $whereclause .= ' (groupid = 0 AND courseid != 0)';
        }
        
        // Security check: if, by now, we have NOTHING in $whereclause, then it means
        // that NO event-selecting clauses were defined. Thus, we won't be returning ANY
        // events no matter what. Allowing the code to proceed might return a completely
        // valid query with only time constraints, thus selecting ALL events in that time frame!
        if (empty($whereclause)) {
            return array();
        }
        
        if ($withduration) {
            $timeclause = '(timestart >= '.$timestart.' OR timestart + timeduration > '.$timestart.')';
            if (!Common::isNullOrEmpty($timeend)) {
                $timeclause .= ' AND timestart <= '.$timeend;
            }
        }
        else {
            $timeclause = 'timestart >= '.$timestart;
            if (!Common::isNullOrEmpty($timeend)) {
                $timeclause .= ' AND timestart <= '.$timeend;
            }
        }
        if (!empty($whereclause)) {
            // We have additional constraints
            $whereclause = $timeclause.' AND ('.$whereclause.')';
        }
        else {
            // Just basic time filtering
            $whereclause = $timeclause;
        }
        
        if ($ignorehidden) {
            $whereclause .= ' AND visible = 1';
        }
        
        
        $query = "SELECT
						".Controller::REPLACE."
			FROM		{event}";
        
        if (!Common::isNullOrEmpty($whereclause)){
            $query .= ' WHERE '.$whereclause;
        }
        
        $totalCountQuery = str_replace(Controller::REPLACE, 'COUNT(1)', $query);
        $totalCount = $DB->get_field_sql($totalCountQuery);
        
        
        $query = str_replace(Controller::REPLACE, '*', $query);
        $query .= ' ORDER BY timestart';
        
        if (empty($page) || $page < 0) {
            $page = 1;
        }
        $limitfrom = $maxlimit * ($page - 1);
        $events = $DB->get_records_sql($query, null, $limitfrom, $maxlimit);
        
        return array($totalCount, $events);
    }
    
    
    /**
     * 조회된 일정에 대해서 url, icon 등 기본 메타 데이터를 설정해서 리턴해줍니다.
     * 
     * @param object $event
     * @param string $courseid
     * @param boolean $isAjax
     * @return object
     */
    function getMetadataSetting($event, $courseid=SITEID, $isAjax=true) 
    {
        global $CFG, $OUTPUT, $PAGE;
        
        // ajax로 호출시에 context값이 없어서 notice error가 떨어짐
        if ($isAjax) {
            if (empty($courseid) || $courseid == SITEID) {
                $PAGE->set_context(context_system::instance());
            } else {
                $PAGE->set_context(context_course::instance($courseid));
            }
        }
        
        $event->name = format_string($event->name, true);
        
        $ed = usergetdate($event->timestart);
        $event->link = $CFG->wwwroot.'/calendar/view.php?view=day&amp;course='.$courseid.'&amp;cal_d='.$ed['mday'].'&amp;cal_m='.$ed['mon'].'&amp;cal_y='.$ed['year'];
        
        // 일정중에 modulename이 등록된게 존재하는 경우
        if (!empty($event->modulename)) {
            // 강좌에 등록된 모듈인지 확인.
            $module = get_coursemodule_from_instance($event->modulename, $event->instance);
            
            if ($module === false) {
                return;	// 강좌안에 등록된 모듈이 아닌 경우에는 빈값으로 리턴
            }
            
            $modulename = get_string('modulename', $event->modulename);
            if (get_string_manager()->string_exists($event->eventtype, $event->modulename)) {
                // will be used as alt text if the event icon
                $eventtype = get_string($event->eventtype, $event->modulename);
            } else {
                $eventtype = '';
            }
            
            $icon = $OUTPUT->image_url('icon', $event->modulename) . '';
            $timeline_icon = $OUTPUT->image_url('timeline/'.$event->modulename, 'theme_'.$PAGE->theme->name);
            
            $event->icon = '<img src="'.$icon.'" alt="'.$eventtype.'" title="'.$modulename.'" class="icon" />';
            $event->timeline_icon = '<img src="'.$timeline_icon.'" alt="'.$eventtype.'" title="'.$modulename.'" class="icon" />';
            $event->link = $CFG->wwwroot.'/course/view.php?id='.$event->courseid;
            $event->refererlink = $CFG->wwwroot.'/mod/'.$event->modulename.'/view.php?id='.$module->id;
            $event->cmid = $module->id;
        } else if ($event->courseid == SITEID) {                              // Site event
            $timeline_icon = $OUTPUT->image_url('timeline/site', 'theme_'.$PAGE->theme->name);
            
            $alt = get_string('globalevent', 'calendar');
            $event->icon = '<img src="'.$OUTPUT->image_url('i/siteevent') . '" alt="'.$alt.'" class="icon" />';
            $event->timeline_icon = '<img src="'.$timeline_icon.'" alt="'.$alt.'" class="icon" />';
            $event->cssclass = 'calendar_event_global';
        } else if ($event->courseid != 0 && $event->courseid != SITEID && $event->groupid == 0) {          // Course event
            $timeline_icon = $OUTPUT->image_url('timeline/course', 'theme_'.$PAGE->theme->name);
            
            $alt = get_string('courseevent', 'calendar');
            $event->icon = '<img src="'.$OUTPUT->image_url('i/courseevent') . '" alt="'.$alt.'" class="icon" />';
            $event->timeline_icon = '<img src="'.$timeline_icon.'" alt="'.$alt.'" class="icon" />';
            $event->link = $CFG->wwwroot.'/course/view.php?id='.$event->courseid;
            $event->cssclass = 'calendar_event_course';
        } else if ($event->groupid) {                                    // Group event
            $timeline_icon = $OUTPUT->image_url('timeline/group', 'theme_'.$PAGE->theme->name);
            
            $alt = get_string('groupevent', 'calendar');
            $event->icon = '<img src="'.$OUTPUT->image_url('i/groupevent') . '" alt="'.$alt.'" class="icon" />';
            $event->timeline_icon = '<img src="'.$timeline_icon.'" alt="'.$alt.'" class="icon" />';
            $event->cssclass = 'calendar_event_group';
        } else if ($event->userid) {                                      // User event
            $timeline_icon = $OUTPUT->image_url('timeline/user', 'theme_'.$PAGE->theme->name);
            
            $alt = get_string('userevent', 'calendar');
            $event->icon = '<img src="'.$OUTPUT->image_url('i/userevent') . '" alt="'.$alt.'" class="icon" />';
            $event->timeline_icon = '<img src="'.$timeline_icon.'" alt="'.$alt.'" class="icon" />';
            $event->cssclass = 'calendar_event_user';
        }
        
        return $event;
    }
}