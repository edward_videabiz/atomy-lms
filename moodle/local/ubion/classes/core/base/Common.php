<?php
namespace local_ubion\core\base;

use core_useragent;
use stdClass;
use DateTime;

class Common
{

    private static $langList = null;

    /**
     * 초를 사용자가 알아보기 쉽게 H:i:s 형태로 리턴시켜줍니다.
     *
     * @param number $second
     * @return string
     */
    public static function getGMDate($second)
    {
        if (! self::isNullOrEmpty($second)) {
            if (is_float($second) || is_number($second)) {
                if ($second >= HOURSECS) {
                    $second = gmdate("H:i:s", $second);
                } else { // 시간 단위 이하는 분:초 형태로 표시 되어야 함.
                    $second = gmdate("i:s", $second);
                }
            }
        }

        return $second;
    }

    /**
     * 연도/월/일 시간/분 형태로 전달된 배열값을 unixtime 형태로 리턴해줍니다.
     *
     * @param array $array
     * @return number
     */
    public static function getArrayToUnixtime($array)
    {
        if (is_array($array)) {
            if (isset($array['enabled']) && $array['enabled']) {
                // 예외처리 없음.
                // Get the calendar type used - see MDL-18375.
                $calendartype = \core_calendar\type_factory::get_calendar_instance();
                $gregoriandate = $calendartype->convert_to_gregorian($array['year'], $array['month'], $array['day'], $array['hour'], $array['minute']);

                return make_timestamp($gregoriandate['year'], $gregoriandate['month'], $gregoriandate['day'], $gregoriandate['hour'], $gregoriandate['minute']);
            }
        }

        return 0;
    }

    /**
     * 연도/월/일 시간/분 형태로 전달된 배열값을 unixtime 형태로 리턴해줍니다.
     *
     * @param array $array
     * @return number
     */
    public static function getArrayYMDToUnixtime($array)
    {
        if (is_array($array)) {

            // 예외처리 없음.
            // Get the calendar type used - see MDL-18375.
            $calendartype = \core_calendar\type_factory::get_calendar_instance();
            $gregoriandate = $calendartype->convert_to_gregorian($array['year'], $array['month'], $array['day']);

            return make_timestamp($gregoriandate['year'], $gregoriandate['month'], $gregoriandate['day']);
        }

        return 0;
    }

    /**
     *
     * @param string $name
     * @param string $value
     * @param string $expire
     * @param string $is_override
     */
    public static function setCookie($name, $value, $expire = null, $is_override = false)
    {
        global $CFG;

        // 쿠키 내용 덮어씌우기가 아닌 경우에는 기존 내용에 추가적으로 입력된 내용 더해줘야됨.
        if (! $is_override) {
            $cookie = self::getCookie($name);
            // 기존에 등록된 쿠키가 존재한다면.
            if ($cookie !== false) {
                $value = $cookie . $value;
            }
        }

        // 0값을 등록하면 브라우저 종료와 동시에 만료
        if (self::isNullOrEmpty($expire, false)) {
            $expire = time() + DAYSECS; // 2012-09-28 쿠키 저장 기본 시간은 1일로 변경.
        }

        setcookie($name, rc4encrypt($value), $expire, $CFG->sessioncookiepath, $CFG->sessioncookiedomain, $CFG->cookiesecure, $CFG->cookiehttponly);
    }

    /**
     * 쿠키가 등록되어 있는지 확인
     * 만약 등록이 되어 있다면 암호화된 값을 해독해서 리턴 시켜줍니다.
     *
     * @param string $name
     * @return string|false
     */
    public static function getCookie($name)
    {
        // $cookie = $_COOKIE[$name];
        if (array_key_exists($name, $_COOKIE)) {
            $cookie = $_COOKIE[$name];
        } else {
            $cookie = '';
        }

        // 쿠키값이 존재하면 기존에 해당글을 본적이 있는지 확인 해야됨.
        if (! self::isNullOrEmpty($cookie)) {
            $cookie = rc4decrypt($cookie);

            return $cookie;
        } else {
            return false;
        }
    }

    /**
     * 저장되어 있는 쿠키값 중 특정 문자 열이 있는지 확인 합니다.
     *
     * @param string $name
     * @param string $str
     * @return boolean
     */
    public static function getCookieInStr($name, $str)
    {
        $cookie = self::getCookie($name);

        if ($cookie !== false) {
            // 문자열이 있는지 확인.
            if (strpos($cookie, $str) === FALSE) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 문자열이 빈값인지 확인 (빈값이면 true)
     *
     * @param $str :
     *            문자열
     * @return string
     */
    public static function isNullOrEmpty($str, $isempty = true)
    {
        // 먼저 해당 변수가 존재하는지 확인 (object->property로 전달되는 경우도 있음)
        if (isset($str)) {
            if (is_string($str)) {
                $str = trim($str);
            }

            if ($isempty) {
                return empty($str);
            } else {
                if ($str === null || $str === '') {
                    return true;
                }
            }
        } else {
            return true;
        }

        return false;
    }

    /**
     * 파일 사이즈를 사용자가 보기 편하도록 변환해서 반환 합니다.
     *
     * @param int $size
     *            : 사이즈
     * @return string $rtn
     */
    public static function getFileSize($size = 0)
    {
        $rtn = 0;

        if (is_numeric($size)) {
            if ($size <= 0) {
                $rtn = '0 Byte';
            } elseif ($size < 1024) {
                $rtn = $size . ' Byte';
            } elseif ($size >= 1024 && $size < 1048576) {
                $rtn = sprintf('%0.1f KB', $size / 1024);
            } elseif ($size >= 1048576 && $size < 1073741824) {
                $rtn = sprintf('%0.2f MB', $size / 1048576);
            } else {
                $rtn = sprintf('%0.2f GB', $size / 1073741824);
            }
        } else {
            $rtn = '파라메터가 숫자형이 아닙니다. 숫자형으로 전달해주시기 바랍니다.';
        }

        return $rtn;
    }

    /**
     * 문자열을 byte단위로 변환해서 리턴합니다.
     * 한글 : 3byte, 기타 : 1byte
     *
     * @param string $str
     *            : 문자열
     * @return int $rtn : byte
     */
    public static function getByte($str)
    {
        $rtn = 0;
        $maxi = strlen($str);

        for ($i = 0; $i < $maxi; $i ++) {
            $rtn ++;
        }

        return $rtn;
    }

    /**
     * print_r로 출력할 내용을 string형으로 리턴시킵니다.
     *
     * @param array $array
     * @return string;
     */
    public static function getPrintRToString($array)
    {
        ob_start();
        print_r($array);
        $str = ob_get_contents();
        ob_end_clean();

        return $str;
    }

    /**
     * print_r을 보기 좋게 출력해줍니다.
     *
     * @param array|object $array
     * @param string $return
     * @return string
     */
    public static function print_r($array, $return = false)
    {
        $str = "<pre>";
        $str .= self::GetPrintRToString($array);
        $str .= "</pre>";

        if ($return) {
            return $str;
        } else {
            echo $str;
        }
    }

    /**
     * 문자열에 "/" 가 2개 들어있는 경우 이를 1개로 변환해서 리턴합니다.
     * 예) http://site.com//index.php -> http://site.com/index.php
     * 참고 : CodeIgniter
     *
     * @param
     *            string
     * @return string
     */
    public static function getReduceDoubleSlashes($str)
    {
        return preg_replace("#([^:])//+#", "\\1/", $str);
    }

    /**
     * 고유값 가져오기
     */
    public static function getUUID()
    {
        return md5(uniqid());
        // return com_create_guid();
    }

    /**
     * 고유값 가져오기 v4
     */
    public static function getUUIDV4()
    {
        $bytes = random_bytes(16);

        assert(strlen($bytes) == 16);

        $bytes[6] = chr(ord($bytes[6]) & 0x0f | 0x40); // set version to 0100
        $bytes[8] = chr(ord($bytes[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($bytes), 4));
    }

    /**
     * 파일 확장자를 리턴해줍니다.
     *
     * @param string $filename
     * @param boolean $isExt
     */
    public static function getExt($filename, $isExt = true)
    {
        if (! self::isNullOrEmpty($filename)) {
            if (! $isExt) { // 파일명만 추출하고 싶을때
                return substr($filename, 0, strrpos($filename, '.'));
            } else { // 확장자만 추출하고 싶을때
                return strtolower(substr(strrchr($filename, '.'), 1));
            }
        } else {
            return null;
        }
    }

    /**
     * 실제 IP 정보를 리턴해줍니다.
     *
     * @return string
     */
    public static function getRealIPAddr()
    {
        if (! empty($_SERVER['HTTP_CLIENT_IP'])) {
            // check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    /**
     * 현재 접속중인 Device 값을 리턴해줍니다.
     *
     * @param string $theme
     * @return string
     */
    static function getDevice($theme = null)
    {
        $device = core_useragent::get_device_type();

        // 모바일, 타블렛은 현재는 같은 스킨을 사용하도록 함.
        switch ($device) {
            case "mobile":
                $device = "mobile";
                break;
            /*
             * case 'tablet':
             * $device = "tablet";
             * break;
             */
            default:
                $device = 'default';
                break;
        }

        return $device;
    }

    /**
     * 현재 접속 중인 device가 모바일인지 확인해줍니다.
     *
     * @return boolean
     */
    static function isMobile($device = null)
    {
        global $PAGE;

        $isMobile = false;
        if (self::isWebView()) {
            $isMobile = true;
        } else {
            // 전달된 device값이 빈값이면 device 재 확인
            if (empty($device)) {
                $device = self::getDevice();
            }

            // PC에서 테스트 목적으로 작업을 할 경우 theme 파라메터를 별도로 지정하지 않으면 default로 떨어짐.
            // 좀더 쉽게 테스트 하기 위해서 현재 테마명에 mobile이 들어가면 자동으로 mobile 스킨을 불러오도록 수정 함.
            if ($device == 'mobile') {
                // PC 화면 보인 경우가 있기 때문에 예외처리 해줘야됨.
                if ($device == $PAGE->devicetypeinuse) {
                    $isMobile = true;
                }
            } elseif (strpos($PAGE->theme->name, 'mobile') !== false) {
                $isMobile = true;
            }
        }

        return $isMobile;
    }

    /**
     * 현재 접속 중인 device가 android인지 확인
     *
     * @return boolean
     */
    public static function isAndroid()
    {
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);

        if (preg_match('/android/', $agent)) {
            return true;
        }

        return false;
    }

    /**
     * 현재 접속 중인 device가 Apple인지 확인
     *
     * @return boolean
     */
    public static function isApple()
    {
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);

        // PC용 화면도 필요하기 때문에...
        if (preg_match('/iphone|ipad|ipod/', $agent)) {
            return true;
        }

        return false;
    }

    /**
     * 트위터 처럼 날자 데이터 간략화 해서 리턴해줍니다.
     * 언어셋에 대한 유효성 검사는 없음 -_-;
     *
     * @param datetime $t1
     * @param datetime $t2
     * @param stdClass $i8n
     */
    static function getDateDiffTwitter($t1, $t2)
    {
        if (! $t2) {
            return;
        }

        $i8n = new stdClass();
        $i8n->y = get_string('time_year', 'local_ubion');
        $i8n->m = get_string('time_month', 'local_ubion');
        $i8n->d = get_string('time_day', 'local_ubion');
        $i8n->h = get_string('time_hour', 'local_ubion');
        $i8n->i = get_string('time_minute', 'local_ubion');
        $i8n->s = get_string('time_second', 'local_ubion');

        // $i8n->ago = array(' 후', ' 전');

        // 무들 형식이니깐.
        if (! empty($t1)) {
            $t1 = date("Y-m-d H:i:s", $t1);
        }

        if (! empty($t2)) {
            $t2 = date("Y-m-d H:i:s", $t2);
        }

        $t1 = new DateTime($t1);
        $t2 = new DateTime($t2);
        $t1 = $t1->diff($t2);

        foreach ($t1 as $key => $val) {
            if ($val) {
                return $val . get_string('time_ago_' . $t1->invert, 'local_ubion', $i8n->$key);
            }
        }

        return null;
    }
    
    /**
     * 전달된 날자간의 차이를 day 기준으로 리턴해줍니다.
     *
     * @param datetime $t1
     * @param datetime $t2
     * @param stdClass $i8n
     */
    static function getDateDiffTwitterDays($t1, $t2)
    {
        if (! $t2) {
            return;
        }
        
        // 무들 형식이니깐.
        if (! empty($t1)) {
            $t1 = date("Y-m-d H:i:s", $t1);
        }
        
        if (! empty($t2)) {
            $t2 = date("Y-m-d H:i:s", $t2);
        }
        
        $t1 = new DateTime($t1);
        $t2 = new DateTime($t2);
        $t1 = $t1->diff($t2);
        
        // invert값이 1이면 -값이므로 null로 전달해주는게 좋을듯함.
        if (empty($t1->invert)) {
            return $t1->days;
        }
    }

    /**
     * 데이터 암호화 함수
     *
     * @param string $plain_data
     * @param string $key
     * @return string
     */
    static function encryption($plain_data, $key = null)
    {
        if (self::isNullOrEmpty($key)) {
            $key = get_site_identifier();
        }

        $encrypted_data_on_binary = mcrypt_ecb(MCRYPT_SERPENT, $key, $plain_data, MCRYPT_ENCRYPT);
        $encrypted_data = base64_encode($encrypted_data_on_binary);
        return $encrypted_data;
    }

    /**
     * 데이터 복호화 함수
     *
     * @param string $encrypted_data
     * @param string $key
     * @return string
     */
    static function decryption($encrypted_data, $key = null)
    {
        if (self::isNullOrEmpty($key)) {
            $key = get_site_identifier();
        }

        $decrypted_data_on_binary = base64_decode($encrypted_data);
        $plain_data = mcrypt_ecb(MCRYPT_SERPENT, $key, $decrypted_data_on_binary, MCRYPT_DECRYPT);
        return $plain_data;
    }

    /**
     * 현재 사용자가 설정한 언어를 리턴해줍니다.
     *
     * @param boolean $is_short
     * @return string <string, mixed>
     */
    static function getCurrentLang($is_short = false)
    {
        $lang = current_language();

        // 짧은 이름이 필요한 경우
        if ($is_short) {
            // 짧은 이름이 필요하기 때문에 en_us 형식으로 되어 있는 경우 en만 리턴해줘야됨
            if (strpos($lang, '_') !== false) {
                return strtok($lang, '_');
            } else {
                return $lang;
            }
        } else {
            return $lang;
        }
    }

    /**
     * 전달된 문자열에 자동으로 hyperlink를 걸어줍니다.
     *
     * @param string $text
     * @param boolean $is_newindow
     * @return string
     */
    public static function autoLink($text, $is_newindow = true)
    {
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

        $url = null;
        if (preg_match($reg_exUrl, $text, $url)) {
            $target = '';
            if ($is_newindow) {
                $target = " target='_blank'";
            }

            return preg_replace($reg_exUrl, "<a href='{$url[0]}' $target>{$url[0]}</a>", $text);
        } else {
            return $text;
        }
    }

    /**
     * Webview에서 호출한 경우 true를 리턴해줍니다.
     *
     * @return boolean
     */
    public static function isWebView()
    {
        global $USER;

        return (isset($USER->is_webview) && $USER->is_webview) ? true : false;
    }

    public static function getPeriodTime($period, $add_hour = 8, $cipher = false)
    {
        $return = $period + $add_hour;

        if ($cipher) {
            $return = str_pad($return, 2, 0, STR_PAD_LEFT);
        }
        return $return;
    }

    /**
     * 배열 복사
     *
     * @param array $array
     * @return array
     */
    public static function getArrayCopy(array $array)
    {
        array_walk_recursive($array, function (&$value) {
            if (is_object($value)) {
                $value = clone $value;
            }
        });

        return $array;
    }

    /**
     * 메시지 내용을 출력하고 프로그램을 종료시킵니다.
     *
     * @param string $message
     * @param string $link
     * @param string $class
     */
    public static function printNotice($message, $link, $class = "alert-danger")
    {
        global $PAGE, $OUTPUT;

        $message = clean_text($message); // In case nasties are in here.

        if (CLI_SCRIPT) {
            echo ("!!$message!!\n");
            exit(1); // No success.
        }

        if (! $PAGE->headerprinted) {
            // Header not yet printed.
            $PAGE->set_title(get_string('notice'));
            echo $OUTPUT->header();
        } else {
            echo $OUTPUT->container_end_all(false);
        }

        echo $OUTPUT->box($message, 'alert ' . $class);
        echo $OUTPUT->continue_button($link);

        echo $OUTPUT->footer();
        exit(1); // General error code.
    }

    /**
     * 메시지 내용을 출력하고 프로그램을 종료시킵니다.
     *
     * @param string $message
     * @param string $class
     */
    public static function printError($message, $class = "alert-danger")
    {
        global $PAGE, $OUTPUT;

        $message = clean_text($message); // In case nasties are in here.

        if (CLI_SCRIPT) {
            echo ("!!$message!!\n");
            exit(1); // No success.
        }

        if (! $PAGE->headerprinted) {
            // Header not yet printed.
            $PAGE->set_context(\context_system::instance());
            $PAGE->set_title(get_string('notice'));
            echo $OUTPUT->header();
        } else {
            echo $OUTPUT->container_end_all(false);
        }

        echo $OUTPUT->box($message, 'alert ' . $class);

        echo $OUTPUT->footer();
        exit(1); // General error code.
    }

    /**
     * 사용중인 메모리 현황 리턴
     *
     * @return string
     */
    public static function getMemoryUsage()
    {
        return self::getFileSize(memory_get_peak_usage());
    }

    /**
     * 정렬 관련 텍스트 문구를 생성해줍니다.
     *
     * @param string $text
     * @param string $sby
     * @param string $paramSBY
     * @param string $paramSOrder
     * @param \moodle_url $baseurl
     * @return string
     */
    public static function getSortLink($text, $sby, $paramSBY = null, $paramSOrder = null, \moodle_url $baseurl = null)
    {
        if (empty($baseurl)) {
            // 현재 페이지 주소
            $scriptName = $_SERVER['SCRIPT_NAME'];
            $queryString = $_SERVER['QUERY_STRING'];

            $parameters = null;

            // 전달된 파라메터를 배열로 변환
            parse_str($queryString, $parameters);

            // 기본 주소
            $baseurl = new \moodle_url($scriptName, $parameters);
        }

        if (empty($paramSBY)) {
            $paramSBY = \local_ubion\base\Parameter::getSortBy();
        }

        if (empty($paramSOrder)) {
            $paramSOrder = \local_ubion\base\Parameter::getSortOrder();
        }

        $currentSBY = $baseurl->param($paramSBY);
        $currentSOrder = strtoupper($baseurl->param($paramSOrder));

        // 기본값은 asc
        $sortorder = 'asc';
        $isSortIcon = false;

        // 전달된 $sby값으로 정렬 되어 있는 경우 해당 링크를 클릭하면 asc, desc 로 변경을 해줘야됨.
        if ($currentSBY == $sby) {
            if ($currentSOrder == 'DESC') {
                $sortorder = 'asc';
            } else {
                $sortorder = 'desc';
            }

            $isSortIcon = true;
        } else {
            $sortorder = 'asc';
        }

        return \html_writer::link($baseurl->out(false, array(
            $paramSBY => $sby,
            $paramSOrder => strtoupper($sortorder)
        )), $text . get_accesshide(get_string('sortby') . ' ' . $text . ' ' . get_string($sortorder))) . ' ' . self::getSortIcon($sortorder, $isSortIcon);
    }

    public static function getSortIcon($order, $isSortIcon = true)
    {
        // global $OUTPUT;
        if (! $isSortIcon) {
            return '';
        }

        $order = strtoupper($order);

        if ($order == 'DESC') {
            return '<i class="fa fa-caret-up"></i>';
            // return $OUTPUT->pix_icon('t/sort_asc', get_string('asc'));
        } else {
            return '<i class="fa fa-caret-down"></i>';
            // return $OUTPUT->pix_icon('t/sort_desc', get_string('desc'));
        }
    }

    /**
     * 설치된 언어팩 목록
     *
     * @return array
     */
    public static function getInstallLangs()
    {
        return get_string_manager()->get_list_of_translations();
    }

    /**
     * 설치된 언어팩 및
     *
     * @return string[]
     */
    public static function getLangs(array $installLangs = null)
    {
        if (empty($installLangs)) {
            $installLangs = get_string_manager()->get_list_of_translations();
        }

        // 혹시라도 순서가 바뀔수도 있기 때문에 한글, 영어 순으로 표시하고
        // 그이외에는 $langs에서 전달된 순서대로 표시
        $languages = array(
            'ko',
            'en'
        );

        if (! empty($installLangs)) {
            foreach ($installLangs as $langKey => $langName) {
                if (! in_array($langKey, $languages)) {
                    array_push($languages, $langKey);
                }
            }
        }

        return $languages;
    }

    /**
     * 언어 목록 (언어약어, 언어명)
     *
     * @return array
     */
    public static function getLangList()
    {
        if (empty(self::$langList)) {

            /*
             * 무들 기본 방식은 무들 사이트에서 데이터를 가져오기 때문에 느림.
             * $controller = new \tool_langimport\controller();
             * $lang = array();
             * if ($availablelangs = $controller->availablelangs) {
             * foreach ($availablelangs as $as) {
             * $lang[$as[0]] = $as[2];
             * }
             * }
             *
             * self::$langList = $lang;
             */

            // 흔히 사용되는 항목들만 정리해놓음
            self::$langList = [
                'ko' => '한국어',
                'en' => 'English',
                'ja' => '日本語',
                'zh_cn' => '简体中文',
                'zh_tw' => '正體中文',
                'id' => 'Indonesian'
            ];
        }

        return self::$langList;
    }

    /**
     * 언어 약어로 전달된 값을 언어명으로 리턴
     *
     * @param string $lang
     * @return string
     */
    public static function getLangName($lang)
    {
        $langList = self::getLangList();

        return $langList[$lang] ?? null;
    }

    /**
     * 메뉴 등록시 필수값으로 지정이 되어야 할 언어
     *
     * @return string
     */
    public static function getRequiredLanguage()
    {
        $installLang = self::getInstallLangs();

        // 기본은 한글, 만약 한글 언어가 설치가 안되어 있다면 영문이 기본이여야 함.
        $defaultLang = 'ko';
        if (! isset($installLang[$defaultLang])) {
            $defaultLang = 'en';
        }

        return $defaultLang;
    }

    /**
     * 다국어로 등록된 메뉴(familysite등)를 사용자 언어 환경에 맞는 메뉴명으로 리턴
     *
     * @param array $unserialize
     * @param string $currentLang
     * @return string
     */
    public static function getUnserializeName(array $unserialize, $currentLang = null)
    {
        // 사용자가 선택한 언어
        if (empty($currentLang)) {
            $currentLang = current_language();
        }

        // $unserialize값에 사용자가 선택한 언어가 없다면
        // 기본적으로 영어가 표시
        // 만약 영어도 빈값이라면 한글로 표시해줘야됨.
        $name = $unserialize[$currentLang] ?? '';

        // 사용자가 선택한 언어가 없거나, 메뉴명을 따로지정하지 않은 경우 영어로 설정
        if (empty($name)) {
            $name = $unserialize['en'] ?? '';
        }

        // 영어도 빈값이라면 getRequiredLanguage값으로 표시해줌
        if (empty($name)) {
            $name = $unserialize[Common::getRequiredLanguage()] ?? '';
        }

        // 그래도 빈값이면 빈값 그대로 출력
        return $name;
    }

    /**
     * 사용자 기본 정렬 값
     *
     * @param string $prefix
     * @return string
     */
    public static function getUserDefaultSort($prefix = '')
    {
        $order = (current_language() == 'ko') ? 'firstname' : 'lastname';

        if (! empty($prefix)) {
            $order = $prefix . '.' . $order;
        }

        return $order;
    }

    public static function getArrayKeyValueObject(array $array, $selected = null)
    {
        $return = array();

        if (! empty($array)) {
            foreach ($array as $key => $value) {
                $tmp = new stdClass();
                $tmp->key = $key;
                $tmp->value = $value;
                $tmp->selected = null;

                if ($key == $selected) {
                    $tmp->selected = true;
                }

                $return[] = $tmp;
            }
        }

        return $return;
    }

    public static function getUserDate($unixtime)
    {
        return date('d-m-Y H:i:s', $unixtime);
    }

    public static function getUserDateShort($unixtime)
    {
        return date('d-m-Y', $unixtime);
    }

    public static function getFileName($fileName)
    {
        $filename = pathinfo(rawurlencode($fileName), PATHINFO_FILENAME);
        return rawurldecode($filename);
    }

    public static function setDirectoryFileDelete($dir)
    {
        /*
         * 숨김 파일에 대해서는 삭제가 불가능
         * array_map('unlink', glob($dir.'/*.*'));
         * rmdir($dir);
         */
        $files = array_diff(scandir($dir), array(
            '.',
            '..'
        ));

        foreach ($files as $file) {
            (is_dir($dir . '/' . $file)) ? self::setDirectoryFileDelete($dir . '/' . $file) : unlink($dir . '/' . $file);
        }

        return rmdir($dir);
    }
}