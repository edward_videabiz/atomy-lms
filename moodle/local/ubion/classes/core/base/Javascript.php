<?php
namespace local_ubion\core\base;

/**
 *
 * @category Javascript.php
 * @author Sung Hoon, Cho (akdddnet@gmail.com)
 * @since 2008. 11. 11.
 * @license BSD License
 * @version 0.2
 *         
 *          개념적으로는 SHE 작업하면서 사용했던 JsHelper와 비슷한 역활을 합니다.
 *          사이트 전체적으로 자주 사용되는 함수들만 생성해서 사용합니다~
 *         
 *          수정사항
 *          - 2011-03-03
 *          1. 기존에 사용하던 함수들 모두 삭제함.
 *          2. jquery-mobile같은 모바일 javascript framework 사용할때는 이 클래스를 사용하면 안됩니다.
 *         
 */
class Javascript
{

    /**
     * javascript tag 시작 또는 종료 구문을 리턴해줍니다.
     *
     * @param boolean $isEnd
     */
    private static function getScriptTag($isEnd = false)
    {
        if ($isEnd) {
            return "</script><body>&nbsp;</body></html>";
        } else {
            $string = "
<!DOCTYPE html>
<html>
	<head>
    	<meta charset=\"utf-8\" />
		<script>
";
            return $string;
        }
    }

    /**
     * redirect
     *
     * @param string $href
     */
    public static function redirect($href, $replace = false)
    {
        if ($replace) {
            $script = 'location.replace("' . $href . '")';
        } else {
            $script = 'location.href = "' . $href . '"';
        }

        self::getEndScript($script);
    }

    /**
     *
     * 경고 문구 출력 후 특정 페이지로 이동시킵니다.
     *
     * @param string $alert
     * @param string $href
     * @param boolean $replace
     */
    public static function getAlertMove($alert, $href, $replace = false)
    {
        $script = 'alert("' . $alert . '");';

        if ($replace) {
            $script .= 'location.replace("' . $href . '")';
        } else {
            $script .= 'location.href = "' . $href . '"';
        }

        self::getEndScript($script);
    }

    /**
     * Alert 문구 출력 후 창을 닫습니다.
     *
     * @param string $alert
     */
    public static function getAlertClose($alert)
    {
        $script = 'alert("' . $alert . '"); self.close();';

        self::getEndScript($script);
    }

    /**
     *
     * 경고 문구를 출력후 이전 페이지로 돌아갑니다.
     *
     * @param string $alert
     */
    public static function getAlert($alert)
    {
        $script = 'alert("' . $alert . '");';
        $script .= 'history.back(-1);';

        self::getEndScript($script);
    }

    public static function printAlert($alert, $isSuccess = false)
    {
        if (\local_ubion\base\Parameter::isAajx()) {
            $successCode = ($isSuccess) ? self::getSuccessCode() : self::getFailureCode();

            self::printJSON(array(
                'code' => $successCode,
                'msg' => $alert
            ));
        } else {
            self::getAlert($alert);
        }
    }

    public static function printAlertMove($alert, $link, $isSuccess = false)
    {
        if (\local_ubion\base\Parameter::isAajx()) {
            $successCode = ($isSuccess) ? self::getSuccessCode() : self::getFailureCode();

            self::printJSON(array(
                'code' => $successCode,
                'msg' => $alert,
                'href' => $link
            ));
        } else {
            self::getAlertMove($alert, $link);
        }
    }

    /**
     * 사용자가 직접 script 구문을 작성합니다.
     *
     * @param string $script
     */
    public static function userScript($script)
    {
        self::getEndScript($script);
    }

    public static function getAlertParentReload($alert)
    {
        if (\local_ubion\base\Parameter::isAajx()) {
            self::printJSON(array(
                'code' => self::getNotLoginCode(),
                'msg' => $alert
            ));
        } else {
            $script = 'alert("' . $alert . '");';

            $script .= "if (opener) {";
            $script .= "self.close();";
            $script .= "opener.document.location.href = opener.document.location.href;";
            $script .= "} else {";
            $script .= "location.href = location.href;";
            $script .= "}";

            self::getEndScript($script);
        }
    }

    /**
     * 이동
     *
     * @param string $url
     * @param string $isReplace
     */
    public static function move($url, $isReplace = false)
    {
        if ($isReplace) {
            $script = 'location.href="' . $url . '";';
        } else {
            $script = 'location.replace("' . $url . '");';
        }

        self::getEndScript($script);
    }

    /**
     *
     * 스크립트를 출력 후 모든 활동을 중지 시킵니다.
     *
     * @param string $script
     */
    private static function getEndScript($body)
    {
        $script = self::getScriptTag();
        $script .= preg_replace('/(\r\n|\r|\n)/', '\n', $body);
        $script .= self::getScriptTag(true);

        echo $script;
        exit();
    }

    /**
     * JSON 형태로 출력합니다.
     *
     * @param array $array
     */
    public static function printJSON(array $array)
    {
        header("Content-Type: application/json");

        echo json_encode($array);
        exit();
    }

    /**
     * Success Code
     *
     * @return string
     */
    public static function getSuccessCode()
    {
        return "100";
    }

    /**
     * Failed Code
     *
     * @return string
     */
    public static function getFailureCode()
    {
        return "300";
    }

    public static function getNotLoginCode()
    {
        return "399";
    }
}