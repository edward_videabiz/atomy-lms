<?php
namespace local_ubion\core\base;

use local_ubion\core\traits\TraitValidate;

/**
 *
 * @category Validate.php
 * @since 2008. 07. 31
 * @version 1.0
 *         
 *          수정사항
 *          - 2010. 12. 30
 *          1. 닷넷 영향을 받았던 코딩 체계를 자바 환경으로 수정함.
 *         
 *          - 2011. 03. 03
 *          1. 사용자 아이디 체크 함수 추가.
 *         
 *          - 2017. 05. 01
 *          laravel 형태로 변경함.
 */
class Validate
{
    use TraitValidate;

    // 전달된 값이 내부 로직에 의해서 변경되는걸 방지 하기 위해서 별도의 변수에 copy 시켜놓음.
    protected $data = null;

    // element => array(validate error 항목)
    protected $errors = array();

    protected $string_errors = array();

    protected $rules = null;

    protected $placeholder = array();

    protected $paramtype = array();

    protected $isRequestAll = false;

    const PLACEHOLDER = 'placeholder';

    const PARAMTYPE = 'paramtype';

    const PARAMVALUE = 'value';

    function __construct()
    {}

    /**
     * validation 항목 추가
     * 특별한 경우가 아니라면 make, makeNoRequest를 이용해주시기 바랍니다.
     *
     * @param string $elementid
     * @param array $rules
     * @param string $value
     */
    public function add($elementid, array $rules, $value)
    {
        $this->makeRule($elementid, $rules);

        $this->data[$elementid] = $value;
    }

    public function makeRequest(array $request, array $rules, $isAutoCheck = true)
    {
        $this->data = $this->parseData($request);
        $this->isRequestAll = true;

        $this->makeRules($rules);

        if ($isAutoCheck) {
            $this->validation();
        }

        return $this;
    }

    public function make(array $rules, $isAutoCheck = true)
    {
        $this->makeRules($rules);

        if ($isAutoCheck) {
            $this->validation();
        }

        return $this;
    }

    /**
     * validation rules 셋팅
     * elementid => rule 형태로 입력해주셔야 합니다.
     *
     * @param array $rules
     */
    protected function makeRules(array $rules)
    {
        foreach ($rules as $elementid => $rule) {
            $this->makeRule($elementid, $rule);
        }
    }

    /**
     * 특정 항목에 대한 validation rule설정
     *
     * @param string $elementid
     * @param string|array $rule
     */
    protected function makeRule($elementid, $rule)
    {
        if (! is_array($rule)) {
            $rule = array(
                $rule
            );
        }

        // rule을 key value 형태로 맞춰주기
        // required , array('required') 형태로 입력하는 경우 정의된 형태로 구성해주기
        $ruleInfo = array();

        foreach ($rule as $ruleKey => $ruleParam) {
            if ($ruleKey === self::PLACEHOLDER) {
                $this->placeholder[$elementid] = $ruleParam;
            } else if ($ruleKey === self::PARAMTYPE) {
                $this->paramtype[$elementid] = $ruleParam;
            } else if ($ruleKey === self::PARAMVALUE) {
                $this->data[$elementid] = $ruleParam;
            } else {
                // key값이 숫자이면 'required', 'alpha"형태로 전달된것임
                if (is_number($ruleKey)) {
                    $ruleInfo[$ruleParam] = null;
                } else {
                    $ruleInfo[$ruleKey] = $ruleParam;
                }
            }
        }

        $this->rules[$elementid] = $ruleInfo;
    }

    /**
     * GET, POST된 데이터에 대해서 클래스 내에서 변조 될수 있는 가능성 때문에 특정 변수에 복사해놓음.
     *
     * @param array $data
     * @return array[]
     */
    public function parseData(array $data)
    {
        $newData = [];

        foreach ($data as $key => $value) {
            $newData[$key] = $value;
        }

        return $newData;
    }

    /**
     * clean_param 함수를 거쳐서 값을 리턴시켜줌.
     *
     * @param string $key
     * @param string $default
     * @return NULL|mixed|number|string
     */
    public function getValue($key, $default = null)
    {
        $value = $this->data[$key] ?? null;

        // paramtype값이 지정되어 있지 않다면 PARAM_NOTAGS값이 기본값임
        $paramtype = $this->paramtype[$key] ?? PARAM_NOTAGS;

        // 빈값이 아니라면 clean_param 진행
        if (empty($value)) {
            // 기본값이 설정되어 있으면 기본값으로 전달해줘야됨.
            if ($default) {
                $value = $default;
            }
        } else {
            $value = clean_param(trim($value), $paramtype);
        }

        return $value;
    }

    /**
     * clean_param 함수를 거치지 않고 전달된 값 그대로 리턴
     *
     * @param string $key
     * @param string $default
     * @return string|NULL|mixed
     */
    public function getValueNoClean($key, $default = null)
    {
        $value = $this->data[$key] ?? null;

        // 빈값이라면 default값 전달
        if (empty($value)) {
            // 기본값이 설정되어 있으면 기본값으로 전달해줘야됨.
            if ($default) {
                $value = $default;
            }
        }

        return $value;
    }

    /**
     * validation 검사.
     */
    public function validation()
    {
        // 곧바로 $_POST를 사용하는 경우에는 clean_param 함수를 통해서 리턴된 값을 이용해서 validation 체크를 해야됨.
        $getValuePropertyName = ($this->isRequestAll) ? 'getValue' : 'getValueNoClean';

        foreach ($this->rules as $elementid => $rules) {

            foreach ($rules as $rule => $parameters) {

                // $value = $this->data[$elementid] ?? null;
                $value = $this->$getValuePropertyName($elementid);
                // error_log('key => '.$elementid.', value => '.$value);

                $method = "validate" . ucfirst($rule);
                if (method_exists($this, $method)) {
                    if (! $this->$method($elementid, $value, $parameters)) {
                        $this->errors[$elementid][$rule] = $parameters;
                    }
                } else {
                    $this->errors[$elementid][$rule] = 'NOT FOUND VALIDATE';
                }
            }
        }
    }

    /**
     * validate 검사시 실패한 항목이 있는지 검사.
     *
     * @return boolean
     */
    public function fails()
    {
        $count = count($this->errors);

        return ($count > 0) ? true : false;
    }

    /**
     * validate fails 항목 리턴
     * array key값이 elementid로 출력되기 때문에 해당 페이지에서 별도로 처리하셔야 합니다.
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * validate fails 항목에 대해서 사용자가 알아 볼수 있는 string형태로 리턴
     *
     * @return string
     */
    public function getErrorMessages()
    {
        $extendKeys = $this->getStringExtendKeys();

        $errors = array();
        if (! empty($this->errors)) {
            foreach ($this->errors as $elementid => $rule) {
                $title = $this->placeholder[$elementid] ?? $elementid;

                foreach ($rule as $ruleKey => $ruleParam) {
                    $msg = null;
                    if (in_array($ruleKey, $extendKeys)) {
                        $msg = get_string('validation_' . $ruleKey, 'local_ubion', array(
                            'element' => $title,
                            'value' => $ruleParam
                        ));
                    } else {
                        $msg = get_string('validation_' . $ruleKey, 'local_ubion', $title);
                    }

                    if (! empty($msg)) {
                        $errors[] = '- ' . $msg;
                    }
                }
            }
        }

        return join("\n", $errors);
    }

    /**
     * 오류 내용 출력시 get_string()에 파라메터를 전달해야되는 검사 항목
     *
     * @return string[]
     */
    protected function getStringExtendKeys()
    {
        return array(
            'digits',
            'max',
            'min',
            'requiredWith',
            'requiredWithAll',
            'requiredWithout',
            'requiredWithoutAll',
            'size'
        );
    }
}