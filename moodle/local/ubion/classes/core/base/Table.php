<?php
namespace local_ubion\core\base;

use html_writer;
global $CFG;
require_once $CFG->libdir . '/tablelib.php';

class Table extends \table_sql
{

    protected $csmsColumns = array();

    protected $csmsHeaders = array();

    protected $csmsCols = array();

    protected $csmsClass = array();

    protected $csmsAttr = array();

    protected $csmsNotCollapse = array();

    protected $csmsTableClass = array();

    protected $isPrintColgroup = false;

    protected $isMobileResponsive = false;

    protected $isPrint = null;

    protected $filters = null;

    /**
     * $headers, $columns 등을 따로 입력해야되다보니 코딩량이 증가되서 이를 한줄로 처리할수 있도록 함수 추가함.
     *
     * @param string $columnKey
     * @param string $columnTitle
     * @param string $columnClass
     * @param string $columnColClass
     * @param array $columnAttr
     */
    function addColumns($columnKey, $columnTitle, $columnClass = null, $columnColClass = null, array $columnAttr = null)
    {
        // 등록된 키가 없는 경우
        if (! isset($this->csmsColumns[$columnKey])) {
            // $this->columns에 반영할 값이기 때문에 key => value형태로 입력해야됨
            // 참고로 key값이 중요하고 value값은 의미가 없는 값임
            $this->csmsColumns[$columnKey] = $columnKey;

            $this->csmsHeaders[$columnKey] = $columnTitle;

            // 앞뒤로 공백 추가
            $this->csmsClass[$columnKey] = ' ' . $columnClass . ' ';

            $this->csmsCols[$columnKey] = $columnColClass;

            // 전달된 값이 존재하는 경우
            if (is_array($columnAttr) && ! empty($columnAttr)) {
                foreach ($columnAttr as $attKey => $attValue) {
                    $this->csmsAttr[$columnKey][$attKey] = $attValue;
                }
            }
        }
    }

    /**
     * define_columns와 비슷한 역할을 합니다.
     */
    protected function setDefineColumns()
    {
        $this->columns = array();
        $this->column_style = array();
        $this->column_class = array();
        $colnum = 0;

        foreach ($this->csmsColumns as $columnKey => $columnValue) {
            $this->columns[$columnKey] = $colnum ++;
            $this->column_style[$columnKey] = array();
            $this->column_class[$columnKey] = $this->csmsClass[$columnKey];
            $this->column_suppress[$columnKey] = false;

            $this->headers[] = $this->csmsHeaders[$columnKey];

            // 기본적으로 모든 컬럼에 대해서 sorting을 제공하지 않음.
            $this->column_nosort[] = $columnKey;
        }
    }

    /**
     * colgroup 항목 추가 여부
     *
     * @param boolean $is
     */
    public function setIsPrintColgroup($is)
    {
        $this->isPrintColgroup = $is;
    }

    /**
     * 정렬을 사용할 컬럼 설정
     *
     * @param string $columnKey
     */
    public function setSort($columnKey)
    {
        if (in_array($columnKey, $this->column_nosort)) {
            $this->column_nosort = array_diff($this->column_nosort, array(
                $columnKey
            ));
        }
    }

    public function setNotCollapse($columnKey)
    {
        if (! in_array($columnKey, $this->csmsNotCollapse)) {
            $this->csmsNotCollapse[] = $columnKey;
        }
    }

    /**
     * Must be called after table is defined.
     * Use methods above first. Cannot
     * use functions below till after calling this method.
     *
     * @return type?
     */
    public function setup()
    {
        parent::setup();

        // class 추가해주기
        $class = '';
        if (isset($this->attributes['class'])) {
            $class = $this->attributes['class'];
        }

        $addClass = array(
            'table-coursemos'
        );
        if ($this->isMobileResponsive) {
            $addClass[] = 'table-mobile-responsive';
        }

        if (! empty($this->csmsTableClass)) {
            $addClass = array_merge($addClass, $this->csmsTableClass);
        }

        $class .= join(' ', $addClass);

        $this->set_attribute('class', $class);
    }

    /**
     * This function is not part of the public api.
     */
    function start_html()
    {
        // global $OUTPUT;

        // Render button to allow user to reset table preferences.
        echo $this->render_reset_button();

        // Do we need to print initial bars?
        // $this->print_initials_bar();

        // Paging bar
        /*
         * pagingbar 부분만 주석처리함.
         * if ($this->use_pages) {
         * $pagingbar = new paging_bar($this->totalrows, $this->currpage, $this->pagesize, $this->baseurl);
         * $pagingbar->pagevar = $this->request[TABLE_VAR_PAGE];
         * echo $OUTPUT->render($pagingbar);
         * }
         */

        if (in_array(TABLE_P_TOP, $this->showdownloadbuttonsat)) {
            echo $this->download_buttons();
        }

        $this->wrap_html_start();
        // Start of main data table

        echo html_writer::start_tag('div', array(
            'class' => 'table-responsive'
        ));
        echo html_writer::start_tag('table', $this->attributes);
    }

    /**
     * This function is not part of the public api.
     */
    function print_headers()
    {
        global $CFG, $OUTPUT;

        if ($this->isPrintColgroup) {
            if (! empty($this->csmsCols)) {
                echo html_writer::start_tag('colgroup');
                foreach ($this->csmsCols as $colKey => $colValue) {
                    // 숨김항목으로 설정되어 있으면 50으로 변경되어야 함.
                    if (! in_array($colKey, $this->csmsNotCollapse) && isset($this->prefs['collapse'][$colKey]) && $this->prefs['collapse'][$colKey]) {
                        $colValue = 'wp-10';
                    }
                    echo html_writer::empty_tag('col', array(
                        'class' => $colValue
                    ));
                }
                echo html_writer::end_tag('colgroup');
            }
        }

        // parent::print_headers()와 거의 동일한데,
        // is_collapsible는 전체 컬럼에 대한 설정이고,
        // 각 컬럼별로 collapsible로 지정이 되지 않기 때문에 이에 대한 예외처리를 하는 구문만 추가시켜놓음.
        echo html_writer::start_tag('thead');
        echo html_writer::start_tag('tr');
        foreach ($this->columns as $column => $index) {

            $icon_hide = '';
            if ($this->is_collapsible) {
                $icon_hide = $this->show_hide_link($column, $index);
            }

            $primarysortcolumn = '';
            $primarysortorder = '';
            if (reset($this->prefs['sortby'])) {
                $primarysortcolumn = key($this->prefs['sortby']);
                $primarysortorder = current($this->prefs['sortby']);
            }

            switch ($column) {

                case 'fullname':
                    // Check the full name display for sortable fields.
                    $nameformat = $CFG->fullnamedisplay;
                    if ($nameformat == 'language') {
                        $nameformat = get_string('fullnamedisplay');
                    }
                    $requirednames = order_in_string(get_all_user_name_fields(), $nameformat);

                    if (! empty($requirednames)) {
                        if ($this->is_sortable($column)) {
                            // Done this way for the possibility of more than two sortable full name display fields.
                            $this->headers[$index] = '';
                            foreach ($requirednames as $name) {
                                $sortname = $this->sort_link(get_string($name), $name, $primarysortcolumn === $name, $primarysortorder);
                                $this->headers[$index] .= $sortname . ' / ';
                            }
                            $helpicon = '';
                            if (isset($this->helpforheaders[$index])) {
                                $helpicon = $OUTPUT->render($this->helpforheaders[$index]);
                            }
                            $this->headers[$index] = substr($this->headers[$index], 0, - 3) . $helpicon;
                        }
                    }
                    break;

                case 'userpic':
                    // do nothing, do not display sortable links
                    break;

                default:
                    if ($this->is_sortable($column)) {
                        $helpicon = '';
                        if (isset($this->helpforheaders[$index])) {
                            $helpicon = $OUTPUT->render($this->helpforheaders[$index]);
                        }
                        $this->headers[$index] = $this->sort_link($this->headers[$index], $column, $primarysortcolumn == $column, $primarysortorder) . $helpicon;
                    }
            }

            $attributes = array(
                'class' => 'header c' . $index . $this->column_class[$column],
                'scope' => 'col'
            );
            if ($this->headers[$index] === NULL) {
                $content = '&nbsp;';
            } else if (! empty($this->prefs['collapse'][$column])) {
                $content = $icon_hide;
            } else {
                if (is_array($this->column_style[$column])) {
                    $attributes['style'] = $this->make_styles_string($this->column_style[$column]);
                }
                $helpicon = '';
                if (isset($this->helpforheaders[$index]) && ! $this->is_sortable($column)) {
                    $helpicon = $OUTPUT->render($this->helpforheaders[$index]);
                }

                // collapse를 지원하지 않으면 해당 코드는 표시되면 안됨.
                if (! in_array($column, $this->csmsNotCollapse)) {
                    $content = $this->headers[$index] . $helpicon . html_writer::tag('div', $icon_hide, array(
                        'class' => 'commands'
                    ));
                } else {
                    $content = $this->headers[$index] . $helpicon;
                }
            }
            echo html_writer::tag('th', $content, $attributes);
        }

        echo html_writer::end_tag('tr');
        echo html_writer::end_tag('thead');
    }

    /**
     * Generate html code for the passed row.
     *
     * @param array $row
     *            Row data.
     * @param string $classname
     *            classes to add.
     *            
     * @return string $html html code for the row passed.
     */
    public function get_row_html($row, $classname = '')
    {
        static $suppress_lastrow = NULL;
        $rowclasses = array();

        if ($classname) {
            $rowclasses[] = $classname;
        }

        $rowid = $this->uniqueid . '_r' . $this->currentrow;
        $html = '';

        $html .= html_writer::start_tag('tr', array(
            'class' => implode(' ', $rowclasses),
            'id' => $rowid
        ));

        // If we have a separator, print it
        if ($row === NULL) {
            $colcount = count($this->columns);
            $html .= html_writer::tag('td', html_writer::tag('div', '', array(
                'class' => 'tabledivider'
            )), array(
                'colspan' => $colcount
            ));
        } else {
            $colbyindex = array_flip($this->columns);
            foreach ($row as $index => $data) {
                $column = $colbyindex[$index];

                if (empty($this->prefs['collapse'][$column])) {
                    if ($this->column_suppress[$column] && $suppress_lastrow !== NULL && $suppress_lastrow[$index] === $data) {
                        $content = '&nbsp;';
                    } else {
                        $content = $data;
                    }
                } else {
                    $content = '&nbsp;';
                }

                $attributes = array(
                    'class' => 'cell c' . $index . $this->column_class[$column],
                    'id' => $rowid . '_c' . $index,
                    'style' => $this->make_styles_string($this->column_style[$column])
                );

                // td 항목에 추가적으로 attribute값을 반입 하는 부분만 수정함.
                // 추가적으로 attribute를 추가해야되는 경우가있음.
                if (isset($this->csmsAttr[$column])) {
                    foreach ($this->csmsAttr[$column] as $attKey => $attValue) {
                        $attributes[$attKey] = $attValue;
                    }
                }

                $html .= html_writer::tag('td', $content, $attributes);
            }
        }

        $html .= html_writer::end_tag('tr');

        $suppress_enabled = array_sum($this->column_suppress);
        if ($suppress_enabled) {
            $suppress_lastrow = $row;
        }
        $this->currentrow ++;
        return $html;
    }

    /**
     * Fullname is treated as a special columname in tablelib and should always
     * be treated the same as the fullname of a user.
     *
     * @uses $this->useridfield if the userid field is not expected to be id
     *       then you need to override $this->useridfield to point at the correct
     *       field for the user id.
     *      
     * @param object $row
     *            the data from the db containing all fields from the
     *            users table necessary to construct the full name of the user in
     *            current language.
     * @return string contents of cell in column 'fullname', for this row.
     */
    function col_fullname($row)
    {
        $name = fullname($row);

        // 기본은 이름만 출력
        // 각 모듈 페이지별로 필요한 경우 링크 걸어줘야됩니다.
        return $name;
    }

    /**
     * Generate the HTML for the table preferences reset button.
     *
     * @return string HTML fragment, empty string if no need to reset
     */
    protected function render_reset_button()
    {
        if (! $this->can_be_reset()) {
            return '';
        }

        $url = $this->baseurl->out(false, array(
            $this->request[TABLE_VAR_RESET] => 1
        ));

        $html = html_writer::start_div('resettable text-right mb-3');
        $html .= html_writer::link($url, get_string('resettable'), array(
            'class' => 'btn btn-default'
        ));
        $html .= html_writer::end_div();

        return $html;
    }

    /**
     * 화면 출력 항목 여부를 리턴
     *
     * @param string $name
     * @return boolean
     */
    protected function isPrint($name)
    {
        return $this->isPrint[$name] ?? false;
    }

    /**
     * getparameter 리턴
     *
     * @param string $name
     * @return NULL|string
     */
    protected function getFilter($name)
    {
        return $this->filters[$name] ?? null;
    }

    /**
     * This function is not part of the public api.
     */
    function print_nothing_to_display()
    {

        // Render button to allow user to reset table preferences.
        echo $this->render_reset_button();

        echo '<div>' . $this->getNotFoundMessage() . '</div>';
    }

    protected function getNotFoundMessage()
    {
        return get_string('not_found_users', 'local_ubion');
    }
}