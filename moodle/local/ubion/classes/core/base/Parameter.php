<?php
namespace local_ubion\core\base;

class Parameter
{

    const METHOD_GET = 1;

    const METHOD_POST = 2;

    const METHOD_PUT = 3;

    const METHOD_DELETE = 4;

    /**
     * 기본 페이징 갯수
     *
     * @param bool $isAll
     * @return array
     */
    public static function getListSizes($isAll = false)
    {
        $lists = array(
            15 => 15,
            30 => 30,
            50 => 50,
            100 => 100
        );

        if ($isAll) {
            $lists[0] = get_string('all');
        }

        return $lists;
    }

    /**
     * 페이징 기본 사이즈
     *
     * @return number
     */
    public static function getDefaultListSize()
    {
        return 15;
    }

    /**
     * 정렬 키 파라메터 기본값
     *
     * @return string
     */
    public static function getSortBy()
    {
        return 'sby';
    }

    /**
     * 정렬 순서 파라메터 기본값
     */
    public static function getSortOrder()
    {
        return 'sorder';
    }

    /**
     * option 선택시 모든 항목 선택 값
     *
     * 해당 함수가 존재하는 이유는 일부 항목에서 빈값이 존재하는 경우가 있어서 빈값과, 전체 항목을 구분하기가 애매해지는 상황이 발생되기 때문에
     * 특정 명칭을 이용해서 전체 조회를 구분시켜줌
     *
     * @return string
     */
    public static function getOptionAllValue()
    {
        return '-9999';
    }

    public static function getKeyword($keyword = 'keyword', $nullValue = null)
    {
        return htmlspecialchars(optional_param($keyword, $nullValue, PARAM_NOTAGS));
    }

    /**
     * get parameter 가져오기
     *
     * @param string $key
     * @param string $default
     * @param string $type
     * @param
     *            boolean urldecode 여부
     * @return int|string
     */
    public static function get($key, $default, $type, $isDecode = true)
    {
        if (isset($_GET[$key])) {
            $value = $isDecode ? urldecode($_GET[$key]) : $_GET[$key];

            return clean_param($value, $type);
        } else {
            return $default;
        }
    }

    /**
     * 배열형태의 get 파라메터 가져오기
     *
     * @param string $key
     * @param string $default
     * @param string $type
     * @param
     *            boolean urldecode 여부
     * @return mixed|number|string
     */
    public static function getArray($key, $default, $type, $isDecode = true)
    {
        if (isset($_GET[$key])) {
            return self::paramArray($_GET[$key], $default, $type, $isDecode);
        } else {
            return $default;
        }
    }

    /**
     * post parameter 가져오기
     *
     * @param string $key
     * @param string $default
     * @param string $type
     * @param
     *            boolean urldecode 여부
     * @return mixed|number|string
     */
    public static function post($key, $default, $type, $isDecode = true)
    {
        if (isset($_POST[$key])) {
            $value = $isDecode ? urldecode($_POST[$key]) : $_POST[$key];

            return clean_param($value, $type);
        } else {
            return $default;
        }
    }

    /**
     * 배열형태의 post 파라메터 가져오기
     *
     * @param string $key
     * @param string $default
     * @param string $type
     * @param
     *            boolean urldecode 여부
     * @return mixed|number|string
     */
    public static function postArray($key, $default, $type, $isDecode = true)
    {
        if (isset($_POST[$key])) {
            return self::paramArray($_POST[$key], $default, $type, $isDecode);
        } else {
            return $default;
        }
    }

    /**
     * get, post 상관 없이 파라메터값 가져오기
     * get, post간에 중복된 파라메터 key값이 존재하면 $_SERVER['request_method'] 값이 더 우선시 됩니다.
     *
     * @param string $key
     * @param string $default
     * @param string $type
     * @param
     *            boolean urldecode 여부
     * @return mixed|number|string
     */
    public static function request($key, $default, $type, $isDecode = true)
    {
        $requestAll = self::getRequestAll();

        if (! empty($requestAll)) {
            if (isset($requestAll[$key])) {
                // jquery 에서 serialize할때 공백에 대해서 치환을 해서 전달해주기 때문에 urldecode 항목이 필요함.
                $value = $isDecode ? urldecode($requestAll[$key]) : $requestAll[$key];

                // 기본적으로 앞뒤로 공백이 존재하면 제거해줌.
                $default = clean_param(trim($value), $type);
            }
        }

        return $default;
    }

    /**
     * get, post 상관 없이 배열형태의 파라메터값 가져오기
     * get, post간에 중복된 파라메터 key값이 존재하면 request_method 값이 더 우선시 됩니다.
     *
     * @param string $key
     * @param string $default
     * @param string $type
     * @param
     *            boolean urldecode 여부
     * @return mixed|number|string
     */
    public static function requestArray($key, $default, $type, $isDecode = true)
    {
        $requestAll = self::getRequestAll();

        if (isset($requestAll[$key])) {
            return self::paramArray($requestAll[$key], $default, $type, $isDecode);
        } else {
            return $default;
        }
    }

    /**
     * method 리턴
     *
     * @return string
     */
    public static function getRequestMethod()
    {
        $method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';

        return strtoupper($method);
    }

    /**
     * array 형태 parameter 가져오기
     *
     * @param string $key
     * @param string $default
     * @param string $type
     * @param
     *            boolean urldecode 여부
     * @return mixed|number|string
     */
    public static function paramArray($params, $default, $type, $isDecode = true)
    {
        if (is_array($params)) {
            $result = array();
            foreach ($params as $paramKey => $paramValue) {
                /*
                 * if (!preg_match('/^[a-z0-9_-]+$/i', $paramKey)) {
                 * debugging('Invalid key name in optional_param_array() detected: '.$paramKey);
                 * continue;
                 * }
                 */
                $paramValue = $isDecode ? urldecode($paramValue) : $paramValue;
                $result[$paramKey] = clean_param($paramValue, $type);
            }

            return $result;
        } else {
            return $default;
        }
    }

    /**
     * 파라메터 존재여부 확인
     *
     * @param string $key
     * @return boolean
     */
    public static function has($key)
    {
        $data = self::getRequestAll();

        if (isset($data[$key])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 전달된 모든 파라메터를 리턴해줍니다.
     *
     * @return array()
     */
    public static function getRequestAll()
    {
        $request = (self::getRequestMethod() == 'POST') ? $_POST : $_GET;

        $requestAll = $request + $_GET;

        return $requestAll;
    }

    /**
     * ajax 형태 파라메터인지 확인
     *
     * @return boolean
     */
    public static function isAajx()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    /**
     * json 형태인지 확인
     *
     * @return boolean
     */
    public static function isJson()
    {
        $contentType = isset($_SERVER['CONTENT_TYPE']) ? strtolower($_SERVER['CONTENT_TYPE']) : null;

        if (! empty($contentType)) {
            if (strpos($contentType, 'json') !== false) {
                return true;
            }
        }

        return false;
    }

    private static function getMethod($method)
    {
        switch ($method) {
            case self::METHOD_POST:
            case self::METHOD_PUT:
            case self::METHOD_DELETE:
                $data = $_POST;
                break;
            default:
                $data = $_GET;
                break;
        }

        return $data;
    }
}