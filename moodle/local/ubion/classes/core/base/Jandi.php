<?php
namespace local_ubion\core\base;

use \stdClass;

class Jandi
{
    /**
     * 잔디로 메시지 보내기
     *
     * @param string $url
     * @param stdClass $json
     * @return void
     */
    public static function callWebhook($url, stdClass $json)
    {
        $headers = array();
        $headers[] = 'Accept: application/vnd.tosslab.jandi-v2+json';
        $headers[] = 'Content-Type: application/json';


        // 주소가 없으면 따로 전송하지 않음.
        if (!empty($url)) {

            // body가 없으면 웹훅이 전달되지 않음.
            // 자세한 내용은 아래 주소에서 확인하면 됨.
            // https://jandi.zendesk.com/hc/ko/articles/210952203-%EC%9E%94%EB%94%94-%EC%BB%A4%EB%84%A5%ED%8A%B8-%EC%9D%B8%EC%BB%A4%EB%B0%8D-%EC%9B%B9%ED%9B%85-Incoming-Webhook-%EC%9C%BC%EB%A1%9C-%EC%99%B8%EB%B6%80-%EB%8D%B0%EC%9D%B4%ED%84%B0%EB%A5%BC-%EC%9E%94%EB%94%94-%EB%A9%94%EC%8B%9C%EC%A7%80%EB%A1%9C-%EC%88%98%EC%8B%A0%ED%95%98%EA%B8%B0
            if (property_exists($json, 'body')) {

                $parameter = json_encode($json);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5);   // timeout은 5초로 설정.
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_exec($ch);

                if (curl_errno($ch)) {
                    error_log(print_r(curl_error($ch), true));
                }

                curl_close($ch);
            } else {
                error_log('webhook으로 전달된 parameter에 body 항목이 존재하지 않습니다.');
            }
        }
    }
}