<?php
namespace local_ubion\core\group;

use stdClass;
use context_course;
use local_ubion\base\Common;

abstract class AbstractGroup
{

    /**
     * 강좌내에 존재하는 모든 그룹 리턴
     *
     * @param number $courseid
     * @return array
     */
    public function getCourseGroups($courseid)
    {
        global $CFG;

        require_once $CFG->dirroot . '/lib/grouplib.php';

        $groupData = groups_get_course_data($courseid);

        // 리턴될 객체 셋팅
        $courseGroup = new \stdClass();
        $courseGroup->is = false;
        $courseGroup->isGroupings = false;
        $courseGroup->groups = array();
        $courseGroup->groupings = array();
        $courseGroup->mappings = array();
        $courseGroup->nomappings = array();

        // 해당 강좌에서 그룹 사용여부는 $groupData->groups가 한개라도 존재하는 경우 강좌내에서 그룹 사용여부로 판단.
        $groupCount = count($groupData->groups);
        if ($groupCount > 0) {
            $courseGroup->is = true;

            // group, grouping이 개별적으로 필요할수도 있기 때문에 별도의 변수에 저장
            $courseGroup->groups = $groupData->groups;
            $courseGroup->groupings = $groupData->groupings;

            // $courseGroup->mappings값을 좀 더 보기 좋은 형태로 바꿔서 전달해줌
            if ($groupData->groupings) {
                // 그룹 분류가 한개라도 존재하는 경우는 true
                $courseGroup->isGroupings = true;

                // 기본값 세팅
                foreach ($groupData->groupings as $cg) {
                    $cg->groups = array();
                    $courseGroup->mappings[$cg->id] = $cg;
                }

                foreach ($groupData->mappings as $ms) {
                    // 그룹 $groupData 존재하는 경우 (notice에러 방지 위해서 isset 검사 함)
                    if (isset($courseGroup->mappings[$ms->groupingid])) {

                        // array key notice 에러 방지 코드
                        if (isset($groupData->groups[$ms->groupid])) {
                            $courseGroup->mappings[$ms->groupingid]->groups[$ms->groupid] = $groupData->groups[$ms->groupid];
                            unset($groupData->groups[$ms->groupid]);
                        }
                    }
                }

                // 그룹분류에 소속된 그룹이 존재하는 경우 nomappings에 반영해줘야됨.
                if ($groupData->groups) {
                    foreach ($groupData->groups as $gg) {
                        $courseGroup->nomappings[$gg->id] = $gg;
                    }
                }
            }
        }
        unset($groupData);

        return $courseGroup;
        // return groups_get_all_groups($courseid);
    }

    /**
     * 모든 사용자 텍스트
     *
     * @return string
     */
    public function getAllUserString()
    {
        return get_string('allparticipants');
    }

    /**
     * 그룹 select option 항목 리턴
     *
     * @param \stdClass $courseGroup
     * @param int $groupid
     * @param boolean $isAll
     * @return string
     */
    public function getOptions(\stdClass $courseGroup, $groupid, $isAll = true)
    {
        $outputGroupOption = get_config('local_ubion', 'output_groupoption');

        if (empty($outputGroupOption)) {
            $option = $this->getOptionGroup($courseGroup, $groupid);
        } else {
            $option = $this->getOptionGrouping($courseGroup, $groupid);
        }

        $all = '';
        if ($isAll) {
            $all = '<option value="">' . $this->getAllUserString() . '</option>';
        }

        return $all . $option;
    }

    /**
     * select option 표시시 그룹분류 => 그룹명 까지 표시해줍니다.
     *
     * @param \stdClass $courseGroup
     * @param int $groupid
     * @return string
     */
    public function getOptionGrouping(\stdClass $courseGroup, $groupid)
    {
        $options = '';

        // <select> 항목에 대해서는 name값 및 다른 추가 attribute를 추가해야되는 경우에 대한 예외처리하기 보다는 options만 전달해주는게 더 효율적일거 같음.
        if ($courseGroup->isGroupings) {
            if ($courseGroup->mappings) {
                foreach ($courseGroup->mappings as $cm) {
                    $options .= '<optgroup label="' . htmlentities($cm->name, ENT_QUOTES) . '">';
                    foreach ($cm->groups as $cg) {
                        $selected = ($cg->id == $groupid) ? ' selected="selected"' : '';
                        $options .= '<option value="' . $cg->id . '" ' . $selected . '>' . $cg->name . '</option>';
                    }
                    $options .= '</optgroup>';
                }
            }

            if ($courseGroup->nomappings) {
                foreach ($courseGroup->nomappings as $cg) {
                    $selected = ($cg->id == $groupid) ? ' selected="selected"' : '';
                    $options .= '<option value="' . $cg->id . '" ' . $selected . '>' . $cg->name . '</option>';
                }
            }
        } else {
            // 단순 그룹만 출력
            $options .= $this->getOptionGroup($courseGroup, $groupid);
        }

        return $options;
    }

    /**
     * select option 표시시 그룹명을 표시해줍니다.
     *
     * @param \stdClass $courseGroup
     * @param int $groupid
     * @return string
     */
    public function getOptionGroup(\stdClass $courseGroup, $groupid)
    {
        $options = '';

        if ($courseGroup->is) {
            foreach ($courseGroup->groups as $cg) {
                $selected = ($cg->id == $groupid) ? ' selected="selected"' : '';
                $options .= '<option value="' . $cg->id . '" ' . $selected . '>' . $cg->name . '</option>';
            }
        }

        return $options;
    }

    /**
     * 그룹명을 label(badge) 형태로 리턴
     *
     * @param string $groupname
     * @param number $maxwidth
     * @param string $addClass
     * @param boolean $isTruncate
     * @return string
     */
    public function getBadge($groupname, $maxwidth = 60, $addClass = null, $isTruncate = true)
    {
        if (! empty($groupname)) {
            return '<span class="' . $this->getBadgeClass($addClass, $maxwidth, $isTruncate) . ' mr-1" title="' . htmlentities($groupname, ENT_QUOTES) . '">' . $groupname . '</span>';
        }
    }

    /**
     * 그룹명 badge class
     *
     * @param string $addClass
     * @param number $maxwidth
     * @param boolean $isTruncate
     * @return string
     */
    public function getBadgeClass($addClass = null, $maxwidth = 60, $isTruncate = true)
    {
        $name = 'label d-inline-block align-middle label-info';

        if (! empty($addClass)) {
            $name .= ' ' . $addClass;
        }

        if ($isTruncate) {
            if (! is_number($maxwidth)) {
                $maxwidth = 60;
            }

            $name .= ' text-truncate max-wp-' . $maxwidth;
        }

        return $name;
    }

    /**
     * 강좌 그룹모드를 사용하는 경우 강좌내 존재하는 그룹 및 본인이 속해있는 그룹 정보를 리턴해줍니다.
     *
     * @param number|object $courseorid
     *            : 강좌번호 또는 강좌 객체
     * @param boolean $isAll
     *            : 전체 사용자 출력 여부
     * @param boolean $isManager
     *            : 관리자(교수, 조교등) 여부
     * @return stdClass(is, groups, active)
     */
    function getGroupModeGroups($courseorid, $isAll = false, $isManager = false)
    {
        global $CFG, $USER;

        require_once $CFG->libdir . '/grouplib.php';

        if (is_number($courseorid)) {
            $course = \local_ubion\course\Course::getInstance()->getCourse($courseorid); // Needed to have numsections property available.
        } else {
            $course = $courseorid;
        }

        $courseGroup = new stdClass();
        $courseGroup->is = false;
        $courseGroup->groups = array();
        $courseGroup->active = null;

        $groupmode = groups_get_course_groupmode($course);

        // 0이면 미사용, 1 : 분리된 그룹, 2 : 열린 그룹
        if ($groupmode > 0) {
            $courseGroup->is = true;

            $context = context_course::instance($course->id);
            $aag = has_capability('moodle/site:accessallgroups', $context);

            // 열린 그룹인 경우
            if ($groupmode == VISIBLEGROUPS or $aag) {
                $allowedgroups = groups_get_all_groups($course->id, 0, $course->defaultgroupingid);
            } else {
                $allowedgroups = groups_get_all_groups($course->id, $USER->id, $course->defaultgroupingid);
            }

            $activegroup = groups_get_course_group($course, true, $allowedgroups);

            // 관리자(교수, 조교등)인 경우에는 페이지 처음 접근시 이전에 선택된 그룹이 자동으로 선택 되기 때문에 사용중에 혼란을 줄 여지가 있음.
            // 관리자만 예외사항으로 페이지 처음 접근 시에 group값을 null로 설정함.
            if ($isManager) {
                $group_parameter = optional_param('group', - 1, PARAM_INT);

                if ($group_parameter == - 1) {
                    $courseGroup->active = null;
                }
            }

            $groupsmenu = array();
            if (! $allowedgroups or $groupmode == VISIBLEGROUPS or $aag || $isAll) {
                $groupsmenu[0] = get_string('allparticipants');
            }

            if ($allowedgroups) {
                foreach ($allowedgroups as $group) {
                    $groupsmenu[$group->id] = format_string($group->name);
                }
            }

            $courseGroup->active = $activegroup;
            $courseGroup->groups = $groupsmenu;
        }

        return $courseGroup;
    }

    /**
     * 그룹내에 존재하는 모든 사용자를 리턴해줍니다.
     *
     * @param int $groupid
     * @param array $addField
     * @param string $sort
     * @return array|boolean
     */
    public function getUsers($groupid, array $addField = null, $order = null)
    {
        if (empty($order)) {
            $order = \local_ubion\base\Common::getUserDefaultSort('u');
        }

        $field = 'u.*';
        if (! empty($addField)) {
            $field .= ',' . join(',', $addField);
        }

        return groups_get_members($groupid, $field, $order);
    }

    /**
     * 그룹내에 존재하는 사용자를 페이징 처리 해서 리턴해줍니다.
     *
     * @param int $groupid
     * @param string $keyfield
     * @param string $keyword
     * @param int $page
     * @param int $ls
     * @param array $addField
     * @param string $order
     * @return array|boolean
     */
    public function getUsersPaging($groupid, $keyfield = null, $keyword = null, $page = 1, $ls = 15, array $addField = null, $order = null)
    {
        global $DB;

        $field = 'u.*';
        if (! empty($addField)) {
            $field .= ',' . join(',', $addField);
        }

        $usernameFields = get_all_user_name_fields(true, 'u');

        $field = "@RNUM := @RNUM + 1 AS rnum
    			,u.id
    			," . $usernameFields . "
    			,u.idnumber
    			,email
    			,u.picture
    			,imagealt
    			,g.name
    			,g.id AS groupid";

        $query = "SELECT
						##FIELD##
			FROM 		{user} u
			JOIN 		{groups_members} gm ON gm.userid = u.id
			JOIN		{groups} g ON g.id = gm.groupid
            JOIN		( SELECT @RNUM := 0 ) R
			WHERE		";

        $param = array();
        // 숫자로 넘어오면 특정 그룹만... 문자열이면 in으로 검색
        if (is_number($groupid)) {
            $query .= " groupid = :groupid";
            $param['groupid'] = $groupid;
        } else {
            $query .= " groupid IN (" . $groupid . ")";
        }

        if (! empty($keyfield) && ! empty($keyword)) {
            switch ($keyfield) {
                case 'idnumber':
                    $query .= " AND " . $DB->sql_like('u.idnumber', ':idnumber');
                    $param['idnumber'] = $keyword . '%';
                    break;
                default:
                    $fullname = $DB->sql_fullname('u.firstname', 'u.lastname');
                    $query .= ' AND ' . $DB->sql_like($fullname, ':keyword', false);
                    $param['keyword'] = '%' . strtolower($keyword) . '%';
                    break;
            }
        }

        $totalQuery = str_replace("##FIELD##", 'COUNT(1)', $query);
        $totalCount = $DB->get_field_sql($totalQuery, $param);

        $limitfrom = $ls * ($page - 1);
        $query = str_replace("##FIELD##", $field, $query);

        if (! empty($order)) {
            $order = ' ORDER BY ' . $order;
        } else {
            $order = ' ORDER BY g.name ASC';
            $order .= ',' . \local_ubion\base\Common::getUserDefaultSort('u');
        }

        $users = $DB->get_records_sql($query . $order, $param, $limitfrom, $ls);

        return array(
            $totalCount,
            $users
        );
    }

    /**
     * 강좌에 소속된 그룹 정보를 리턴해줍니다.
     * 그룹핑은 제외하고 그룹만 가지고 판단함.
     *
     * 그룹핑 정보까지 필요하면 getUserGroupings를 사용하시기 바랍니다.
     *
     * @param int $courseid
     * @param int $userid
     * @param string $field
     * @param int $groupingid
     * @return multitype:
     */
    function getUserGroups($courseid, $userid = null, $field = '', $groupingid = null)
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        // 추가 필드를 가져오는 경우
        if (! empty($field)) {
            $field = ',' . $field;
        }

        $replace = "##GROUPING##";
        $query = "SELECT
        					g.id
        					,g.name
        					$field
        		FROM 		{groups} g
        		JOIN 		{groups_members} gm ON g.id = gm.groupid
                " . $replace . "
        		WHERE 		g.courseid = :courseid
        		AND 		userid = :userid";

        $param = array(
            'courseid' => $courseid,
            'userid' => $userid
        );

        if (! empty($groupingid)) {
            $query = str_replace($replace, " JOIN {groupings_groups} gg ON gg.groupid = g.id", $query);
            $query .= " AND gg.groupingid = :groupingid";
            $param['groupingid'] = $groupingid;
        } else {
            $query = str_replace($replace, null, $query);
        }

        return $DB->get_records_sql($query, $param);
    }

    /**
     * 강좌에 소속된 그룹분류 및 그룹 정보를 리턴해줍니다.
     * (grouping까지 포함)
     *
     * @param int $courseid
     * @param int $userid
     * @param string $field
     * @param int $groupingid
     * @return multitype:
     */
    function getUserGroupingGroups($courseid, $userid = null, $field = '', $groupingid = null)
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        // 추가 필드를 가져오는 경우
        if (! empty($field)) {
            $field = ',' . $field;
        }

        $query = "SELECT
							gm.id AS gmid
							,g.id
							,g.name AS groupname
							,gs.name AS groupingname
							$field
    			FROM 		{groups} g
    			JOIN 		{groups_members} gm ON g.id = gm.groupid
    			LEFT JOIN	{groupings_groups} gg ON gg.groupid = g.id
    			LEFT JOIN	{groupings} gs ON gs.id = gg.groupingid
    			WHERE 		g.courseid = :courseid AND userid = :userid";

        $param = array(
            'courseid' => $courseid,
            'userid' => $userid
        );

        if (! empty($groupingid)) {
            $query .= " AND gg.groupingid = :groupingid";
            $param['groupingid'] = $groupingid;
        }

        return $DB->get_records_sql($query, $param);
    }

    /**
     * 특정 사용자가 소속된 그룹을 badge형태로 리턴해줍니다.
     *
     * @param int $courseid
     * @param int $userid
     * @param string $field
     * @param boolean $isNbsp
     * @return string
     */
    public function getUserGroupBadge($courseid, $userid = null, $field = '', $isNbsp = false)
    {
        global $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $groupName = '';
        if ($userGroup = $this->getUserGroups($courseid, $userid, $field)) {
            foreach ($userGroup as $ug) {
                $groupName .= $this->getBadge($ug->name);
            }
        }

        // 소속된 그룹이 없는 경우 공백이 아닌 nbsp가 리턴되어야 하는 경우
        if ($isNbsp && empty($groupName)) {
            $groupName = '&nbsp;';
        }

        return $groupName;
    }
}