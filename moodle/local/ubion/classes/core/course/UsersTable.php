<?php
namespace local_ubion\core\course;

use local_ubion\base\Table;

class UsersTable extends Table
{

    private $number = null;

    protected $course = null;

    protected $courseRoles = null;

    protected $isProfessor = false;

    protected $CCourse = null;

    protected $CUser = null;

    protected $CGroup = null;

    protected $courseContext = null;

    protected $datestring = null;

    protected $profRoleID = null;


    public function __construct($course, $courseRoles, $isProfessor = false, array $isPrint = null, array $filters = null)
    {
        $this->course = $course;
        $this->isPrint = $isPrint;
        $this->filters = $filters;
        $this->courseRoles = $courseRoles;

        $this->CCourse = \local_ubion\course\Course::getInstance();
        $this->CUser = \local_ubion\user\User::getInstance();
        $this->CGroup = \local_ubion\group\Group::getInstance();
        $this->courseContext = $this->isProfessor = $isProfessor;

        $this->courseContext = \context_course::instance($this->course->id);

        $i8n = new \stdClass();
        $i8n->number = get_string('number', 'local_ubion');
        $i8n->group = get_string('groups', 'group');
        $i8n->userpic = get_string('userpic');
        $i8n->idnumber = get_string('idnumber');
        $i8n->fullnameuser = get_string('fullnameuser');
        $i8n->department = get_string('department');
        $i8n->email = get_string('email');
        $i8n->phone2 = get_string('phone2');
        $i8n->role = get_string('role');
        $i8n->etc = get_string('etc', 'local_ubion');
        $i8n->lastcourseaccess = get_string('lastcourseaccess');

        $this->datestring = new \stdClass();
        $this->datestring->year = get_string('year');
        $this->datestring->years = get_string('years');
        $this->datestring->day = get_string('day');
        $this->datestring->days = get_string('days');
        $this->datestring->hour = get_string('hour');
        $this->datestring->hours = get_string('hours');
        $this->datestring->min = get_string('min');
        $this->datestring->mins = get_string('mins');
        $this->datestring->sec = get_string('sec');
        $this->datestring->secs = get_string('secs');

        // roleid
        $this->profRoleID = $this->CCourse->getRoleProfessor();

        // 컬럼 정의
        // 번호
        $this->addColumns('number', $i8n->number, 'text-center', 'wp-50', array(
            'data-title' => $i8n->number
        ));

        // 사진
        $this->addColumns('userpic', $i8n->userpic, 'text-center', 'wp-60', array(
            'data-title' => $i8n->userpic
        ));

        // 소속
        if ($isProfessor) {
            $this->addColumns('department', $i8n->department, '', 'wp-150', array(
                'data-title' => $i8n->department
            ));
        }

        // 교/학번
        $this->addColumns('idnumber', $i8n->idnumber, 'text-center', 'wp-100', array(
            'data-title' => $i8n->idnumber
        ));

        // 이름
        $this->addColumns('fullname', $i8n->fullnameuser, 'text-center', 'wp-120', array(
            'data-title' => $i8n->fullnameuser
        ));

        // 그룹
        if ($this->isPrint('group')) {
            $this->addColumns('group', $i8n->group, 'text-center', 'wp-80', array(
                'data-title' => $i8n->group
            ));
        }

        // 역할
        $this->addColumns('role', $i8n->role, 'text-center', 'wp-80', array(
            'data-title' => $i8n->role
        ));

        if ($isProfessor) {
            // 이메일
            if ($this->isPrint('email')) {
                $this->addColumns('email', $i8n->email, '', '', array(
                    'data-title' => $i8n->email
                ));
            }

            // 연락처
            if ($this->isPrint('phone')) {
                $this->addColumns('phone2', $i8n->phone2, 'text-center', 'wp-110', array(
                    'data-title' => $i8n->phone2
                ));
            }

            // 최종접속
            if ($this->isPrint('lastaccess')) {
                $this->addColumns('lastaccess', $i8n->lastcourseaccess, 'text-center', 'wp-120', array(
                    'data-title' => $i8n->lastcourseaccess
                ));
            }
        }

        // 입력한 컬럼 반영
        $this->setDefineColumns();

        // 정렬 설정
        $this->setSort('idnumber');
        $this->setSort('fullname');

        // colgroup 출력
        $this->isPrintColgroup = true;

        // 모바일용 출력화면
        $this->isMobileResponsive = true;

        parent::__construct('local-ubion-course-users-' . $this->course->id);
    }

    /**
     * Query the database for results to display in the table.
     *
     * @param int $pagesize
     *            size of page for paginated displayed table.
     * @param bool $useinitialsbar
     *            do you want to use the initials bar.
     */
    public function query_db($pagesize, $useinitialsbar = false)
    {
        global $DB;

        list ($query, $param) = $this->CCourse->getCourseUserQuery($this->course, null, $this->getFilter('groupid'), $this->getFilter('keyfield'), $this->getFilter('keyword'), $this->getFilter('roleid'), $this->getFilter('isRoleEqual'));

        $totalcountsql = "SELECT COUNT(1) FROM (" . $query . ") a";

        $total = $DB->get_field_sql($totalcountsql, $param);

        $ls = $this->getFilter('ls');
        if (empty($ls)) {
            \local_ubion\base\Parameter::getDefaultListSize();
        }

        $this->pagesize($ls, $total);

        $sort = $this->get_sql_sort();
        if (empty($sort)) {
            $sort = $this->CCourse->getCourseUserDefaultSortColumn(). ' ' . $this->CCourse->getCourseUserDefaultSortText();
        }

        if ($sort) {
            $sort = 'ORDER BY ' . $sort;
        }

        $this->rawdata = $DB->get_records_sql($query . $sort, $param, $this->get_page_start(), $this->get_page_size());

        $this->number = $this->get_page_start();
    }

    public function col_number($data)
    {
        $this->number ++;

        return $this->number;
    }

    public function col_userpic($data)
    {
        return '<img src="' . $this->CUser->getPicture($data) . '" alt="" />';
    }

    public function col_department($data)
    {
        return $this->CUser->getDepartment($data);
    }

    public function col_idnumber($data)
    {
        return $this->CUser->getIdnumber($data->idnumber, $this->isProfessor);
    }

    public function col_group($data)
    {
        $usergroups = $this->CGroup->getUserGroupBadge($this->course->id, $data->id, null, true);

        return $usergroups;
    }

    public function col_role($data)
    {
        $roleString = '';
        if ($userRoles = $this->CCourse->getUserRoles($this->course->id, $data->id, $this->courseContext->id)) {
            foreach ($userRoles as $ur) {
                $labelClass = 'label text-truncate max-wp-60 align-middle d-inline-block';
                if ($ur->roleid == $this->profRoleID) {
                    $labelClass .= ' label-primary';
                } else {
                    $labelClass .= ' label-default';
                }

                $roleString .= ' <span class="' . $labelClass . '" title="' . $this->courseRoles[$ur->roleid] . '">' . $this->courseRoles[$ur->roleid] . '</span> ';
            }
        } else {
            $roleString = '&nbsp;';
        }

        return $roleString;
    }

    public function col_lastaccess($data)
    {
        global $DB;

        $lastAccessQuery = "SELECT timeaccess FROM {user_lastaccess} WHERE userid = :userid AND courseid = :courseid";
        $lastAccessParam = array(
            'courseid' => $this->course->id,
            'userid' => $data->id
        );

        $userLastAccess = $DB->get_field_sql($lastAccessQuery, $lastAccessParam);

        // 최근 접속 시간
        if ($userLastAccess) {
            $text = format_time(time() - $userLastAccess, $this->datestring);
        } else {
            $text = get_string('never');
        }

        return $text;
    }
}