<?php
namespace local_ubion\core\course;

use moodleform;
defined('MOODLE_INTERNAL') || die();
require_once ($CFG->libdir . '/formslib.php');

class SettingForm extends moodleform
{

    protected function definition()
    {
        global $CFG, $DB;

        $mform = $this->_form;
        $pluginname = 'local_ubion';

        $course = $this->_customdata['course']; // this contains the data of this form
        $courseUbion = \local_ubion\course\Course::getInstance()->getUbion($course->id);

        $usedOptions = array(
            1 => get_string('use', $pluginname),
            0 => get_string('not_use', $pluginname)
        );

        // ##################
        // ### 강좌 설정 ####
        // ##################
        $mform->addElement('header', 'header-general', get_string('course_setting', $pluginname));

        // 강의실 사용여부
        $mform->addElement('selectyesno', 'cuvisible', get_string('course_setting_used', $pluginname));
        $mform->setDefault('cuvisible', $courseUbion->visible);
        $mform->addElement('static', 'static-visible', '', get_string('course_setting_used_help', $pluginname));

        // 언어 설정
        $languages = array();
        $languages[''] = get_string('forceno');
        $languages += get_string_manager()->get_list_of_translations();

        $mform->addElement('select', 'lang', get_string('forcelanguage'), $languages);

        // 손님 접속 여부
        if (isset($CFG->guestloginbutton) && $CFG->guestloginbutton == 1) {

            $isGuest = $DB->get_field_sql('SELECT status FROM {enrol} WHERE enrol = :enrol AND courseid = :courseid', array(
                'enrol' => 'guest',
                'courseid' => $course->id
            ));

            // 손님접속 여부는 0값이 허용, 1값이 미사용임.
            $options = array(
                '0' => get_string('yes'),
                1 => get_string('no')
            );
            $mform->addElement('select', 'guest', get_string('status', 'enrol_guest'), $options);
            $mform->setDefault('guest', $isGuest);
        }

        // ##################
        // ### 강좌 형식 ####
        // ##################
        $mform->addElement('header', 'header-type', get_string('type_format', 'plugin'));
        $mform->setExpanded('header-type');

        // 강좌 포맷
        $courseformats = get_sorted_course_formats(true);
        $formcourseformats = array();
        foreach ($courseformats as $cf) {
            $formcourseformats[$cf] = get_string('pluginname', "format_" . $cf);
        }

        if (isset($course->format)) {
            $course->format = course_get_format($course)->get_format(); // replace with default if not found
            if (! in_array($course->format, $courseformats)) {
                // this format is disabled. Still display it in the dropdown
                $formcourseformats[$course->format] = get_string('withdisablednote', 'moodle', get_string('pluginname', 'format_' . $course->format));
            }
        }

        $mform->addElement('select', 'format', get_string('format'), $formcourseformats);

        // 주차
        $maxsections = get_config('moodlecourse', 'maxsections');
        $sections = array();
        for ($i = 0; $i <= $maxsections; $i ++) {
            $sections[$i] = $i;
        }
        $mform->addElement('select', 'numsections', get_string('numberweeks'), $sections);
        $mform->setType('numsections', PARAM_INT);

        // 비공개 영역 표시 설정
        $options = array(
            0 => get_string('hiddensectionscollapsed'),
            1 => get_string('hiddensectionsinvisible')
        );
        $mform->addElement('select', 'hiddensections', get_string('hiddensections'), $options);
        $mform->setType('hiddensections', PARAM_INT);

        // 강좌 표시방법
        $options = array(
            COURSE_DISPLAY_SINGLEPAGE => get_string('coursedisplay_single'),
            COURSE_DISPLAY_MULTIPAGE => get_string('coursedisplay_multi')
        );
        $mform->addElement('select', 'coursedisplay', get_string('coursedisplay'), $options);
        $mform->setType('coursedisplay', PARAM_INT);

        // ####################
        // ### 강좌 썸네일 ####
        // ####################
        $mform->addElement('header', 'header-background', get_string('course_setting_background', $pluginname));
        $mform->setExpanded('header-background');

        // 강좌 썸네일
        if ($overviewfilesoptions = course_overviewfiles_options($course)) {
            $mform->addElement('filemanager', 'overviewfiles_filemanager', get_string('courseoverviewfiles'), null, $overviewfilesoptions);
            $mform->addHelpButton('overviewfiles_filemanager', 'courseoverviewfiles');
        }

        // #########################
        // ### 이수 / 진도 설정 ####
        // #########################
        $mform->addElement('header', 'header-cp', get_string('course_setting_cp', $pluginname));
        $mform->setExpanded('header-cp');

        // 진도관리 사용여부
        $mform->addElement('select', 'progress', get_string('course_setting_progress', $pluginname), $usedOptions);
        $mform->setDefault('progress', $courseUbion->progress);
        $mform->setType('progress', PARAM_INT);

        $mform->addElement('static', 'static-progress', '', get_string('course_setting_progress_help', $pluginname));

        // 온라인 출석부
        $COnAttendance = \local_ubonattend\Attendance::getInstance($course->id);

        // 사이트내 온라인 출석부를 사용하는 경우 표시
        if ($COnAttendance->isAttendance()) {
            // config 설정이 안되어 있으면 관리자 페이지에서 설정한 기본값이 설정되어 있기 때문에 config에 설정된 값을 default값으로 설정하면 됨.
            $onlineAttendanceConfig = $COnAttendance->getConfig(true);

            // 온라인 출석부 사용여부
            $mform->addElement('selectyesno', 'online', get_string('onlineattendance', 'local_ubonattend'));
            $mform->setDefault('online', $onlineAttendanceConfig->used);
            $mform->setType('online', PARAM_INT);

            // 지각 기능 사용여부
            if ($COnAttendance->isLate()) {
                $mform->addElement('selectyesno', 'islate', get_string('setting_use_late', 'local_ubonattend'));
                $mform->setDefault('islate', $onlineAttendanceConfig->islate);
                $mform->setType('islate', PARAM_INT);
            }

            // 진도 처리 방식 (진도율/열람시간)
            $progressTypes = \local_ubonattend\Progress::getProgressTypes();
            $mform->addElement('select', 'progresstype', get_string('progresstype', 'local_ubonattend'), $progressTypes);
            $mform->setDefault('progresstype', $onlineAttendanceConfig->progresstype);
            $mform->setType('progresstype', PARAM_INT);

            // 온라인 출석부를 사용함으로 설정해야지만 progresstype이 활성화 되어야 함.
            $mform->disabledIf('islate', 'online', 'eq', 0);

            // progresstype은 온라인 출석부와 상관 없이 진도처리 여부에 따라 활성화 여부가 결정되어야 함.
            $mform->disabledIf('progresstype', 'progress', 'eq', 0);

            // 진도처리방식이 "예"인 경우에만 온라인 출석부 관련 설정이 활성화 되어야 함.
            $mform->disabledIf('online', 'progress', 'eq', 0);
            $mform->disabledIf('islate', 'progress', 'eq', 0);
        }

        // ##################
        // ### 기타 설정 ####
        // ##################
        $mform->addElement('header', 'header-etc', get_string('course_setting_etc', $pluginname));
        $mform->setExpanded('header-etc');

        // 알림 사용여부
        $mform->addElement('select', 'notification', get_string('course_setting_notification', $pluginname), $usedOptions);
        $mform->setDefault('notification', $courseUbion->notification);
        $mform->setType('notification', PARAM_INT);

        $mform->addElement('static', 'static-notification', '', get_string('course_setting_notification_help', $pluginname));

        // ####################
        // ### 메타 데이터 ####
        // ####################
        // 최고 관리자인 경우
        if (is_siteadmin()) {
            $mform->addElement('header', 'header-meta', get_string('course_setting_meta', $pluginname));

            $returnurl = base64_encode($CFG->wwwroot . '/course/view.php?id=' . $course->id);
            $url = $CFG->wwwroot . '/local/ubion/index.php/vn/@ubion?id=' . $course->id . '&returnurl=' . $returnurl;
            $mform->addElement('static', 'static-courseubion', '', get_string('course_setting_ubion', $pluginname, $url));
        }

        // hidden 필드
        // 강좌 번호
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $course->id);

        $typename = Controller::TYPENAME;
        $mform->addElement('hidden', $typename);
        $mform->setType($typename, PARAM_ALPHANUMEXT);
        $mform->setDefault($typename, 'update');

        // 저장 버튼
        $this->add_action_buttons(true, get_string('save', $pluginname));
    }

    function validation($data, $files)
    {
        $errors = parent::validation($data, $files);

        $courseformat = course_get_format((object) array(
            'format' => $data['format']
        ));
        $formaterrors = $courseformat->edit_form_validation($data, $files, $errors);
        if (! empty($formaterrors) && is_array($formaterrors)) {
            $errors = array_merge($errors, $formaterrors);
        }

        return $errors;
    }
}