<?php
namespace local_ubion\core\course;

use local_ubion\base\Javascript;
use local_ubion\base\Parameter;

class Controller extends \local_ubion\controller\Controller
{

    /**
     * 강좌 수정
     *
     * 강좌 설정은 따로 웹서비스로 제공할일이 없기 때문에 setUpdate 함수는 따로 구현되지 않습니다.
     */
    public function doUpdate()
    {
        global $CFG, $DB;

        $pluginname = 'local_ubion';

        $CCourse = \local_ubion\course\Course::getInstance();

        $id = Parameter::post('id', null, PARAM_INT);

        // 강좌 정보 확인
        $courseInfo = $CCourse->getCourse($id);
        $courseContext = \context_course::instance($id);
        $courseUbion = $CCourse->getUbion($id);

        // 관리 권한이 존재해야됨.
        if ($CCourse->isProfessor($id, $courseContext)) {

            $format = Parameter::post('format', $courseInfo->format, PARAM_NOTAGS);

            // 커뮤니티형이면 주차 관련 설정이 존재하지 않음.
            $isNumsectionsFormat = ($format != 'ubcommunity') ? true : false;

            // course 테이블 변경
            $updateCourse = new \stdClass();
            $updateCourse->id = $courseInfo->id;
            $updateCourse->lang = Parameter::post('lang', null, PARAM_LANG);
            $updateCourse->enablecompletion = Parameter::post('enablecompletion', $courseInfo->enablecompletion, PARAM_LANG);
            $updateCourse->format = $format;

            // 강좌 요약 파일
            if ($overviewfilesoptions = course_overviewfiles_options($courseInfo->id)) {
                $overviewfiles = 'overviewfiles';
                $overviewfiles_filemanager = $overviewfiles . '_filemanager';
                $updateCourse->$overviewfiles_filemanager = Parameter::post($overviewfiles_filemanager, null, PARAM_INT);

                file_postupdate_standard_filemanager($updateCourse, $overviewfiles, $overviewfilesoptions, $courseContext, 'course', 'overviewfiles', 0);
            }

            $DB->update_record('course', $updateCourse);

            // course_format_options
            if ($isNumsectionsFormat) {
                $numsections = $courseInfo->numsections ?? get_config('moodlecourse', 'numsections');
                $courseFormatOption = new \stdClass();
                $courseFormatOption->numsections = Parameter::post('numsections', $numsections, PARAM_INT);
                $courseFormatOption->hiddensections = Parameter::post('hiddensections', $courseInfo->hiddensections, PARAM_INT);
                $courseFormatOption->coursedisplay = Parameter::post('coursedisplay', $courseInfo->coursedisplay, PARAM_INT);

                course_get_format((object) array(
                    'id' => $courseInfo->id,
                    'format' => $format
                ))->update_course_format_options($courseFormatOption, $courseInfo);
            }

            // 현재 포맷과, 과거 강좌 포맷이 서로 다르다면, 과거 강좌 포맷의 설정값을 삭제 해줘야됨.
            if ($courseInfo->format != $format) {
                $DB->delete_records('course_format_options', array(
                    'courseid' => $courseInfo->id,
                    'format' => $courseInfo->format
                ));
            }

            // 손님접근에 대한 처리
            $guest = Parameter::post('guest', null, PARAM_INT);
            if (! is_null($guest)) {
                $DB->execute('UPDATE {enrol} SET status = :status WHERE enrol = :enrol AND courseid = :courseid', array(
                    'status' => $guest,
                    'enrol' => 'guest',
                    'courseid' => $courseInfo->id
                ));
            }

            // course_ubion 변경 => $courseubion이 없을수가 없음?
            $updateUbion = new \stdClass();
            $updateUbion->id = $courseUbion->id;
            $updateUbion->visible = Parameter::post('cuvisible', 1, PARAM_INT);
            $updateUbion->progress = Parameter::post('progress', 1, PARAM_INT);
            $updateUbion->notification = Parameter::post('notification', 1, PARAM_INT);
            // $course_ubion->background = Parameter::post('course_background', 0, PARAM_INT);
            $DB->update_record('course_ubion', $updateUbion);

            // course_ubion 캐시 재성설
            $CCourse->getUbion($courseInfo->id, true);

            // 온라인 출석부 설정
            $COnAttendance = \local_ubonattend\Attendance::getInstance($courseInfo->id);

            // 온라인 출석부를 사용하는 경우 처리 로직
            if ($COnAttendance->isAttendance()) {
                // config 설정이 안되어 있으면 관리자 페이지에서 설정한 기본값이 설정되어 있기 때문에 config에 설정된 값을 default값으로 설정하면 됨.
                $oldOnAttendanceConfig = $COnAttendance->getConfig(true);

                // 온라인 출석부 사용여부
                $online = Parameter::post('online', $oldOnAttendanceConfig->used, PARAM_INT);

                // 지각 기능 사용여부
                $isLate = Parameter::post('islate', $oldOnAttendanceConfig->islate, PARAM_INT);

                // progresstype
                $progressType = Parameter::post('progresstype', $oldOnAttendanceConfig->progresstype, PARAM_INT);

                $COnAttendance->setAttendanceUsed($online, $isLate, $progressType);

                // 온라인 출석부를 사용 => 아니오 => 사용 형태로 설정을 여러번 변경한 경우
                // 또는 진도처리 방법에 대한 설정값이 변경이 되었으면 진도율 재계산을 진행해야됨.
                if ($oldOnAttendanceConfig->used != $online || $oldOnAttendanceConfig->islate != $isLate || $oldOnAttendanceConfig->progresstype != $progressType) {

                    $CProgress = new \local_ubonattend\Progress($courseInfo->id);
                    $CProgress->setReCalculation();
                }
            }

            // mdl_course 관련 캐시 재설정
            rebuild_course_cache($courseInfo->id, true);

            // 수정 이벤트 남겨야됨.
            $event = \local_ubion\event\course_settinged::create(array(
                'objectid' => $courseInfo->id,
                'context' => $courseContext
            ));
            $event->trigger();

            redirect($CFG->wwwroot . '/course/view.php?id=' . $id);
        } else {
            Javascript::printAlert(get_string('no_permissions', $pluginname));
        }
    }
}