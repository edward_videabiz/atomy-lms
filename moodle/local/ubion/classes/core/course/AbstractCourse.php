<?php
namespace local_ubion\core\course;

use PHPExcel_IOFactory;
use cache;
use stdClass;
use context_course;
use local_ubion\core\traits\TraitCourseUser;
use local_ubion\base\Javascript;
global $CFG;
require_once $CFG->dirroot . '/course/lib.php';

abstract class AbstractCourse
{
    /**
     * !!!!!!!!!!!!!!!!!! 주의사항 !!!!!!!!!!!!!!!!!!
     * 해당 class는 강좌 공통 적으로 사용되는 함수만 등록 해주시기 바랍니다.
     * 만약 정규 강좌용 함수를 추가하시려면 Regular 또는 AbstractRegular class를 이용해주시기 바랍니다.
     * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     */

    use TraitCourseUser;

    const TYPE_REGULAR = 'R';

    // 정규강좌
    const TYPE_IRREGULAR = 'IR';

    // 비정규강좌
    const TYPE_PRACTICE = 'P';

    // 실습강좌
    const TYPE_OPEN = 'CMS_O';

    // 오픈강좌
    const TYPE_ECLASS = 'CMS_E';

    // eclass
    const TYPE_ONLINE = 'CMS_ON';

    // 비교과(mooc)
    const TYPE_MIS = 'CMS_M';

    // 기타
    const CODE_1 = 10;

    // 1학기
    const CODE_S = 11;

    // 여름학기
    const CODE_2 = 20;

    // 2학기
    const CODE_W = 21;

    // 겨울학기
    const CODE_OPEN = 60;

    // eclass
    const CODE_ECLASS = 70;

    // eclass
    const CODE_ONLINE = 80;

    // 비교과(mooc)
    const CODE_99 = 99;

    // 기타 (비정규, 기타)
    const GROUPCODE_U = 'U';

    // 대학
    const GROUPCODE_G = 'G';

    // 대학원
    const VIEWTYPE_SECTION = 0;

    const VIEWTYPE_TAB = 1;

    const COMBINE_MAX = 999;

    const COMBINE_BUNBAN = 'CSMS-ZZ';

    /**
     * course_format 객체 리턴
     *
     * course_get_format을 직접 사용하는게 가장 좋은데, course_get_format 함수명이 잘 생각이 안나서 alias 형태로 만들어놓음.
     *
     * @param number $courseid
     * @return \format_base
     */
    public function getCourseFormat($courseid)
    {
        return course_get_format($courseid);
    }

    /**
     * 강좌 정보 리턴
     *
     * @param int $courseid
     * @return stdClass
     */
    public function getCourse($courseid)
    {
        return course_get_format($courseid)->get_course();
    }

    /**
     * course_ubion 정보를 리턴해줍니다.
     * (캐시 1시간, visible, setting값중 한개라도 0이면 무조건 DB select)
     * 만약 course_ubion 레코드가 없는 경우 신규로 추가후 리턴해줍니다.
     *
     * 특별한 경우가 아닌 이상 course_ubion에 반드시 레코드가 존재함...
     *
     * @param int $courseid
     * @return \stdClass
     */
    public function getUbion($courseid, $rebuild = false)
    {
        global $DB;

        $courseUbion = null;

        if ($courseid == SITEID) {
            return false;
        }

        // 캐시에서 course_ubion 정보 가져오기
        $cache = cache::make('local_ubion', 'course_ubion');

        if ($rebuild) {
            $courseUbion = false;
        } else {
            $courseUbion = $cache->get($courseid);
        }

        if ($courseUbion === false) {
            // 디비에서 직접 조회
            // course_ubion 테이블 조회 쿼리
            $query = "SELECT * FROM {course_ubion} WHERE course = :course";
            $param = array(
                'course' => $courseid
            );

            if (! $courseUbion = $DB->get_record_sql($query, $param)) {
                $courseUbion = $this->setUbion($courseid);
            }

            $cache->set($courseid, $courseUbion);
        }

        return $courseUbion;
    }

    /**
     * course_ubion 추가
     *
     * @param number $courseid
     * @return mixed
     */
    public function setUbion($courseid)
    {
        global $DB;

        $courseUbion = null;

        // 정규 및 비정규등 특정 로직에 의해서 생성이 된다면 course_ubion 레코드는 반드시 존재함.
        // 무들 기본 관리자 화면에서 생성한 경우에는 course_ubion이 없기 때문에 수동으로 생성해줘야됨.
        $courseFormat = course_get_format($courseid);
        $courseInfo = $courseFormat->get_course(); // Needed to have numsections property available.

        // 강좌 생성일을 가지고 연도 구하기
        $year = date('Y', $courseInfo->timecreated);
        $semester_code = self::CODE_99;

        $courseUbion = new stdClass();
        $courseUbion->course = $courseInfo->id;
        $courseUbion->year = $year;
        $courseUbion->semester_code = $semester_code;
        $courseUbion->ename = $courseInfo->fullname;
        $courseUbion->setting = 0;
        $courseUbion->visible = 1;
        $courseUbion->prof_id = null;
        $courseUbion->course_type = self::TYPE_MIS;
        $courseUbion->id = $DB->insert_record('course_ubion', $courseUbion);

        // 등록된 정보를 기반으로 다시 조회 (필수값만 입력했기 때문에 다른 column들은 조회되지가 않는 문제가 있음)
        return $DB->get_record_sql("SELECT * FROM {course_ubion} WHERE course = :course", array(
            'course' => $courseid
        ));
    }

    /**
     * 강좌가 종료되었는지 확인
     *
     * @param int $courseid
     * @param object $courseUbion
     * @return boolean
     */
    public function isStudyEnd($courseid, $courseUbion = null)
    {
        if (empty($courseUbion)) {
            $courseUbion = $this->getUbion($courseid);
        }

        $return = false;
        if (!empty($courseUbion->study_end) && time() > $courseUbion->study_end) {
            $return = true;
        }

        return $return;
    }

    /**
     * 강좌명 전달
     *
     * @param number|object $courseInfo
     * @param boolean $isCourseCode
     * @return string
     */
    public function getName($courseInfo, $isCourseCode = true)
    {
        global $DB;

        // courseid로 전달된 경우
        if (is_number($courseInfo)) {
            // 간혹 합반한 강좌를 삭제하는 경우가 있음.
            // course_get_format으로 접근하면 사이트 자체가 안떠버릴수 있기 때문에 DB에서 직접 조회하도록 변경함.
            // 2015-08-20 by akddd
            $courseInfo = $DB->get_record_sql("SELECT * FROM {course} WHERE id = :id", array(
                'id' => $courseInfo
            ));
        }

        // course_ubion 정보 가져오기
        $courseUbion = $this->getUbion($courseInfo->id);

        // 현재 언어가 한글인경우에만 한글 강좌명 출력
        if (current_language() == 'ko') {
            $course_name = $courseInfo->fullname;
        } else {
            if (isset($courseInfo->ename)) {
                $course_name = $courseInfo->ename;
            } else {
                $course_name = $courseUbion->ename;
            }

            // 영문강좌명이 빈값이면 한글 강좌명 그대로 출력
            if (empty($course_name)) {
                $course_name = $courseInfo->fullname;
            }
        }

        if ($isCourseCode) {
            // 2015-06-12 분반 정보가 존재할 경우에만 분반정보 표시
            // 단, 합반된 강좌인 경우에는 분반정보가 표시되지 않음.
            if (! empty($courseUbion->bunban_code) && strpos($courseUbion->bunban_code, self::COMBINE_BUNBAN) === false) {
                $course_name .= ' (';
                $course_name .= $courseUbion->bunban_code;
                $course_name .= ')';
            }
        }

        return $course_name;
    }

    // 현재 학기 리턴
    public function getCurrentSemester($rebuild = false)
    {
        global $DB;

        $currentSemester = null;
        $cache = cache::make('local_ubion', 'current_semester');

        if ($rebuild) {
            $cache->purge();
        }

        $currentSemester = $cache->get('current_semester');
        if ($currentSemester === false) {
            $currentSemester = $DB->get_record_sql("SELECT * FROM	{local_ubion_haksa} WHERE current_semester = :current_semester", array(
                'current_semester' => 1
            ));
            if ($currentSemester) {
                $cache->set('current_semester', $currentSemester);
            }
        }

        return $currentSemester;
    }

    /**
     * 강좌 포맷이 주차형인지를 확인해줍니다.
     *
     * @param string $format
     * @return boolean
     */
    public function isWeekFormat($format)
    {
        if (strpos($format, 'week') !== false || $format == 'ubmetro') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 교수권한을 가진 사용자인지를 리턴해줍니다.
     *
     * @param number $courseid
     * @param \context_course $context
     * @param number $userid
     * @return boolean
     */
    public function isProfessor($courseid, $context = null, $userid = null)
    {
        if (empty($context)) {
            $context = context_course::instance($courseid);
        }

        return has_capability('moodle/course:update', $context, $userid);
    }

    /**
     * 강좌내 교수 권한을 가진 사용자를 리턴해줍니다.
     *
     * @param number $courseid
     * @param boolean $reset
     * @return array
     */
    public function getProfessors($courseid, $reset = false)
    {
        global $CFG, $DB;

        if ($courseid == SITEID) {
            return false;
        }

        $cache = cache::make('local_ubion', 'course_professor');

        if ($reset) {
            $cache->delete($courseid);
        }

        $profs = $cache->get($courseid);
        if ($profs === false) {
            // 디비에서 조회
            $allnames = get_all_user_name_fields(true, 'u');

            // 강좌 관리자 (coursecontact)에서 선택한 사용자들 표시
            $profRoleid = $CFG->coursecontact;

            // coursecontact값으로 설정된 값이 없는 경우는 강제로 교수자 role을 사용
            if (empty($profRoleid)) {
                $profRoleid = $this->getRoleProfessor();
            }

            $managerRoles = explode(',', $profRoleid);
            list ($roleQuery, $roleParam) = $DB->get_in_or_equal($managerRoles, SQL_PARAMS_NAMED, 'rid');

            // plugin에 의해서 교수자에게 message send시 u.auth, u.suspended, u.deleted, u.emailstop 항목이 존재하지 않으면 debugging메시지가 출력되기 떄문에
            // 추가적으로 select field 추가시켜줌
            $query = "SELECT
							ra.userid AS id, $allnames,
							u.picture, u.imagealt, u.email, u.idnumber, u.auth, u.suspended, u.deleted, u.emailstop
				FROM		{course} c
				JOIN		{context} context ON context.contextlevel = :contextlevel AND context.instanceid = c.id  
				JOIN		{role_assignments} ra ON context.id = ra.contextid AND ra.roleid " . $roleQuery . "
				JOIN		{user} u ON ra.userid = u.id
				WHERE		c.id = :courseid
                ORDER BY    ra.id";

            $param = array(
                'contextlevel' => CONTEXT_COURSE,
                'courseid' => $courseid
            );
            $param = array_merge($param, $roleParam);

            $profs = $DB->get_records_sql($query, $param);

            // 조회된 정보캐시 등록
            $cache->set($courseid, $profs);
        }

        return $profs;
    }

    /**
     * 해당 강좌의 교수님을 평문 형태로 리턴해줍니다.
     *
     * @param number $courseid
     * @param boolean $reset
     * @param string $symbol
     * @return string
     */
    public function getProfessorString($courseid, $reset = false, $symbol = ', ')
    {
        $professorString = '';
        if ($professors = $this->getProfessors($courseid, $reset)) {
            $delimiter = '';
            foreach ($professors as $prof) {
                $professorString .= $delimiter . fullname($prof);
                $delimiter = $symbol;
            }
        }

        return $professorString;
    }

    /**
     * 강좌의 대표 교수님를 리턴해줍니다.
     *
     * @param number $courseid
     * @return NULL|mixed
     */
    public function getMainProfessor($courseid)
    {
        $mainProfessor = null;
        if ($professors = $this->getProfessors($courseid)) {

            if ($course_ubion = $this->getUbion($courseid)) {
                $professorCount = count($professors);

                $mainProfessor = null;
                if ($professorCount > 1) {
                    // 주 담당 교수 정보가 없으면 첫번째 교수가 주담당교수임.
                    if (empty($course_ubion->prof_idnumber)) {
                        $mainProfessor = current($professors);
                    } else {
                        foreach ($professors as $prof) {
                            if ($prof->idnumber == $course_ubion->prof_idnumber) {
                                $mainProfessor = $prof;
                                break;
                            }
                        }

                        // 주 담당교수 정보와 일치하는 사용자가 없다면 첫번째 사용자로 표시
                        if (empty($mainProfessor)) {
                            $mainProfessor = current($professors);
                        }
                    }
                } else {
                    $mainProfessor = current($professors);
                }
            } else {
                $mainProfessor = current($professors);
            }
        }

        return $mainProfessor;
    }

    /**
     * 교수 및 조교 캐시 삭제
     *
     * @param int $courseid
     */
    public function setCourseProfAssiCacheDelete($courseid)
    {
        $this->getProfessors($courseid, true);
    }

    /**
     * 학기 목록을 리턴해줍니다.
     *
     * @param boolean $isIrregular
     * @param boolean $isOnline
     * @param boolean $isEclass
     * @return array()
     */
    public function getSemesters($isIrregular = false, $isOnline = false, $isEclass = false)
    {
        $semesters = array(
            self::CODE_1 => $this->getSemesterName(self::CODE_1),
            self::CODE_S => $this->getSemesterName(self::CODE_S),
            self::CODE_2 => $this->getSemesterName(self::CODE_2),
            self::CODE_W => $this->getSemesterName(self::CODE_W),
            self::CODE_99 => $this->getSemesterName(self::CODE_99),
            self::CODE_ONLINE => $this->getSemesterName(self::CODE_ONLINE),
            self::CODE_ECLASS => $this->getSemesterName(self::CODE_ECLASS)
        );

        if (! $isIrregular) {
            unset($semesters[self::CODE_99]);
        }

        if (! $isOnline) {
            unset($semesters[self::CODE_ONLINE]);
        }

        if (! $isEclass) {
            unset($semesters[self::CODE_ECLASS]);
        }

        return $semesters;
    }

    function getSemesterName($code)
    {
        return get_string('semester_' . $code, 'local_ubion');
    }

    /**
     * 강좌 section 명을 리턴해줍니다.
     *
     * @param \stdClass $course
     * @param int $sectionnum
     * @return string
     */
    public function getSectionName($course, $sectionnum)
    {
        $format = course_get_format($course);
        return $format->get_section_name($sectionnum);
    }

    /**
     * 간략한 section명을 리턴해줍니다.
     * 예 : 1주차, 1주제
     *
     * @param \stdClass $course
     * @param int $sectionnum
     * @return string
     */
    public function getSectionNameShort($course, $sectionnum)
    {
        if ($sectionnum > 0) {
            // week가 포함되어 있으면 주차로 인정
            if (strpos($course->format, 'week') !== false) {
                $name = get_string('sectionname_short_week', 'local_ubion', $sectionnum);
            } else {
                $name = get_string('sectionname_short_topic', 'local_ubion', $sectionnum);
            }
        } else {
            $name = get_string('section0name', 'format_' . $course->format);
        }

        return $name;
    }

    /**
     * 전달된 주차가 현재 주차인지를 확인해줍니다.
     *
     * @param number $courseid
     * @param number $sectionnum
     * @return boolean
     */
    public function isCurrentSection($courseid, $sectionnum)
    {
        $course_format = course_get_format($courseid);
        return $course_format->is_section_current($sectionnum);
    }

    /**
     * 강좌 개설 부서를 리턴해줍니다.
     *
     * @param object $course
     * @param number $courseid
     * @return string
     */
    public function getDepartment($course, $courseid)
    {
        // 기본적으로 강좌를 가져올때 dept_name_01~04까지는 모두 가져옴.
        // 일부 함수에 의해서 dept_name을 가져오지 않을수도 있는데, 문제는 courseubion의 데이터가 course만 존재할수 있고, course_ubion이 섞여있을수 있기 때문에 강좌 번호를 정확하게 알수가 없음.
        // 어쩔수 없이 2번째 파라메터에 강좌 번호를 필수로 가져오도록 처리함.
        if (! isset($course->dept_name_01) || ! isset($course->dept_name_02) || ! isset($course->dept_name_03) || ! isset($course->dept_name_04)) {
            $courseUbion = $this->getUbion($courseid);
        } else {
            $courseUbion = $course;
        }

        $department = $courseUbion->dept_name_01;

        // 가장 마지막에 설정된 학과명을 출력
        if (! empty($courseUbion->dept_name_04)) {
            $department = $courseUbion->dept_name_04;
        } else if (! empty($courseUbion->dept_name_03)) {
            $department = $courseUbion->dept_name_03;
        } else if (! empty($courseUbion->dept_name_02)) {
            $department = $courseUbion->dept_name_02;
        }

        return $department;
    }

    /**
     * 강좌 시작일을 가져옵니다.
     *
     * course->startdate보다 course_ubion->study_start가 더 우선순위가 높습니다.
     *
     * @return string
     */
    public function getStartdate($courseorid)
    {
        if (is_number($courseorid)) {
            $startdate = $this->getCourse($courseorid)->startdate;
            $courseid = $courseorid;
        } else {
            $startdate = $courseorid->startdate;
            $courseid = $courseorid->id;
        }

        if ($courseUbion = $this->getUbion($courseid)) {
            if (! empty($courseUbion->study_start)) {
                $startdate = $courseUbion->study_start;
            }
        }

        return $startdate;
    }

    /**
     * 강좌 종료일을 가져옵니다.
     *
     * course_ubion->study_end 정보를 리턴해줍니다.
     *
     * @return string
     */
    public function getEnddate($courseorid)
    {
        $enddate = null;
        if (is_number($courseorid)) {
            $courseid = $courseorid;
        } else {
            $courseid = $courseorid->id;
        }

        if ($courseUbion = $this->getUbion($courseid)) {
            if (! empty($courseUbion->study_end)) {
                $enddate = $courseUbion->study_end;
            }
        }

        return $enddate;
    }

    /**
     * 강좌 조회 필드
     *
     * @param array $addField
     * @return string
     */
    public function getCourseSelectField(array $addField = array())
    {
        $field = "c.id
    			,c.fullname
    			,c.shortname
    			,c.idnumber
    			,c.format
    			,c.marker
    			,c.startdate
    			,c.groupmode
    			,c.groupmodeforce
    			,c.category
    			,(SELECT value FROM {course_format_options} WHERE courseid = c.id AND format = c.format AND sectionid = 0 AND name = 'numsections' LIMIT 1) AS numsections
    			,c.visible
    			,c.lang
    			,cu.year
    			,cu.semester_code
                ,cu.course_code
                ,cu.bunban_code
                ,cu.bunban_code2
                ,cu.group_code
                ,cu.campus
    			,cu.ename
    			,cu.curriculum
    			,cu.setting
    			,cu.course_type
                ,cu.course_gubun_code
    			,cu.visible AS cu_visible
    			,cu.study_start
    			,cu.study_end
    			,cu.day_code
    			,cu.hour
    			,cu.room_name";

        if (! empty($addField)) {
            $field .= join(',', $addField);
        }

        return $field;
    }

    /**
     * 수강중인 강좌 목록
     *
     * @param array $addField
     * @param boolean $rebuild
     * @param int $userid
     * @param boolean $isAll
     * @return array
     */
    public function getMyCourses(array $addField = array(), $rebuild = false, $userid = null, $isAll = false)
    {
        global $USER, $DB;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        // 손님 계정이거나, 로그인 되어 있지 않으면 false 리턴
        if (isguestuser() || ! isloggedin()) {
            return false;
        }

        // $USER에 ubion property 셋팅
        \local_ubion\user\User::getInstance()->setUbion();

        $time = time();

        $isSelect = false;

        // 본인이 수강중인 강좌를 조회한 적이 없는 경우
        if (! $USER->ubion->course->search) {
            $isSelect = true;
        } else {
            // 조회한적이 있는 경우 1분 정도가 지났으면 디비에서 다시 조회
            $sessionTime = $USER->ubion->course->time;

            // 1분 후에 다시 강좌 목록 재 조회
            if (($time - $sessionTime) > $this->getMyCourseSessionTime()) {
                // error_log('time end => '.date('Y-m-d H:i:s'));
                $isSelect = true;
            }
        }

        // 디비에서 새로 조회해야되는 경우
        if ($isSelect || $rebuild || $isAll) {

            $selectField = $this->getCourseSelectField($addField);

            $query = "SELECT
                                " . $selectField . "
        			FROM		{course} c
        			JOIN		{course_ubion} cu ON cu.course = c.id
        			LEFT JOIN	{course_ubion_combined} cuc ON cuc.t_course = c.id
        			JOIN		{enrol} e ON c.id = e.courseid
        			JOIN		{user_enrolments} ue ON e.id = ue.enrolid AND ue.userid = :userid
        			WHERE		c.visible = 1
        			AND			if (cuc.id IS NOT NULL, cu.visible, 1) = 1";

            // 강좌명으로 정렬을 새로 하기 때문에 디비에서는 따로 정렬을 하지 않습니다.
            // order by idnumber

            $param = array(
                'userid' => $userid
            );

            // error_log($query);
            // error_log(print_r($param, true));

            // 강좌 코드보다는 강좌명으로 정렬하는게 좋음
            // 다국어 때문에 php단에서 정렬을 다시 합니다.
            if ($courses = $DB->get_records_sql($query, $param)) {
                foreach ($courses as $c) {
                    $c->coursename = $this->getName($c);
                }

                // 강좌명으로 정렬 시켜주기
                $courses = $this->getCourseNameSort($courses);
            }

            // 전체보기(숨김 강좌까지 모두 표시)인 경우에는 세션 기록 되면 안됨.
            if ($isAll) {
                $USER->ubion->course->search = false;
            } else {
                // session에 기록
                // 추가 필드를 요청 하는 경우에는 세션에 따로 기록 하지 않음.
                if (empty($addField)) {
                    $USER->ubion->course->courses = serialize($courses);
                    $USER->ubion->course->search = true;
                    $USER->ubion->course->time = time();
                }
            }
        } else {
            $courses = unserialize($USER->ubion->course->courses);
        }

        return $courses;
    }

    /**
     * 세션에 저장되어 있는 내 강좌 목록 재 조회 시간
     *
     * @return int
     */
    public function getMyCourseSessionTime()
    {
        $sessionTime = get_config('local_ubion', 'progress_course_sessiontime');

        // 기본 1분
        if (empty($sessionTime)) {
            $sessionTime = 60;
        }

        return $sessionTime;
    }

    /**
     * 강좌명으로 정렬
     *
     * @param array $courses
     * @return array
     */
    public function getCourseNameSort(array $courses)
    {
        usort($courses, function ($a, $b) {
            return strcmp($a->coursename, $b->coursename);
        });

        return $courses;
    }

    /**
     * 내 강좌 목록을 mustache에 전달하기 위한 셋팅
     *
     * @param array $courses
     * @return array
     */
    public function getMyCourseSetting(array $courses)
    {
        global $CFG;

        if (! empty($courses)) {
            $time = time();

            foreach ($courses as $cc) {
                $cc->url = $CFG->wwwroot . '/course/view.php?id=' . $cc->id;

                $profs = $this->getProfessorString($cc->id);
                if (empty($profs)) {
                    $profs = '&nbsp;';
                }

                $cc->profs = $profs;
                $cc->coursename = $this->getName($cc);

                // 강좌 대표 이미지
                $cc->imageurl = $this->getImageURL($cc->id);

                $cc->courseLabel = $this->getTypeLabel($cc->course_type, $cc->course_gubun_code);
                $cc->courseLabelSub = $this->getTypeSubLabel($cc);
                $cc->periodLabel = $this->getPeriodLabel($cc->study_start, $cc->study_end, $time);
            }

            // mustache로 전달하려면 key값을 제거해야됨.
            $courses = array_values($courses);
        }

        return $courses;
    }

    public function getPeriodLabel($start, $end, $time = null)
    {
        if (empty($time)) {
            $time = time();
        }

        // js에서 컨트롤 되기 key값에 주의해주시기 바랍니다.
        // '1' => get_string('period_ing', 'local_ubion')
        // '2' => get_string('period_before', 'local_ubion')
        // '3' => get_string('period_end', 'local_ubion')

        $class = $text = '';
        if ($start > $time) {
            $class = 'label-info label-period-2';
            $text = get_string('period_before', 'local_ubion');
        } else if ($time >= $start && $time <= $end) {
            $class = 'label-success label-period-1';
            $text = get_string('period_ing', 'local_ubion');
        } else if ($time > $end) {
            $class = 'label-default label-period-3';
            $text = get_string('period_end', 'local_ubion');
        }

        $title = date('Y-m-d', $start) . ' ~ ' . date('Y-m-d', $end);

        $label = '<label class="label label-period ' . $class . '" title="' . $title . '">' . $text . '</label>';
        return $label;
    }

    /**
     * 내 강좌 목록을 강좌 타입별로 리턴
     *
     * @param string $coursetype
     * @param int $year
     * @param string $semester
     * @param int $userid
     * @param array $addField
     * @return array
     */
    public function getMyCoursesType($coursetype, $year, $semester = null, $userid = null, array $addField = array())
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $CCourse = \local_ubion\course\Course::getInstance();

        $selectField = $this->getCourseSelectField($addField);

        // role_assignments는 역할별로 레코드가 여러개 나올수 있기 때문에 enrol, user_enrolments 조합을 사용하는게 좋습니다.
        $query = "SELECT
                        " . $selectField . "
              FROM      {course} c
              JOIN      {course_ubion} cu ON c.id = cu.course
              JOIN      {context} ctx ON ctx.contextlevel = :contextlevel AND instanceid = cu.course
              JOIN      {role_assignments} ra ON ra.contextid = ctx.id
              WHERE     cu.year = :year
              ##SEMESTER##
              AND       cu.course_type = :coursetype
              AND       ra.userid = :userid";

        $param = array(

            'contextlevel' => CONTEXT_COURSE,
            'year' => $year,
            'coursetype' => $coursetype,
            'userid' => $userid
        );

        $usingSemester = array(
            $CCourse::TYPE_REGULAR
        );

        if (in_array($coursetype, $usingSemester)) {
            $query = str_replace("##SEMESTER##", "AND cu.semester_code = :semester_code", $query);
            $param['semester_code'] = $semester;
        } else {
            $query = str_replace("##SEMESTER##", "", $query);
        }

        $query .= ' GROUP BY c.id';
        // error_log($query);
        // error_log(print_r($param, true));
        return $DB->get_records_sql($query, $param);
    }

    /**
     * 강좌내 사용자 역할 정보를 리턴해줍니다.
     *
     * @param number $courseid
     * @param number $userid
     * @param number $contextid
     * @return array
     */
    public function getUserRoles($courseid, $userid = null, $contextid = null)
    {
        global $DB, $USER;

        if (empty($contextid)) {
            $contextid = context_course::instance($courseid)->id;
        }

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $query = "SELECT roleid FROM {role_assignments} WHERE userid = :userid AND contextid = :contextid ORDER BY roleid";

        return $DB->get_records_sql($query, array(
            'userid' => $userid,
            'contextid' => $contextid
        ));
    }

    /**
     * 교수 roleid 리턴
     *
     * @return int
     */
    public function getRoleProfessor()
    {
        $roleid = get_config('local_ubion', 'role_professor');

        // roleid값이 설정이 안되어 있으면 3으로 설정
        if (empty($roleid)) {
            $roleid = 3;
        }

        return $roleid;
    }

    /**
     * 학생 roleid 리턴
     *
     * @return int
     */
    public function getRoleStudent()
    {
        $roleid = get_config('local_ubion', 'role_student');

        // roleid값이 설정이 안되어 있으면 5으로 설정
        if (empty($roleid)) {
            $roleid = 5;
        }

        return $roleid;
    }

    /**
     * 수강중인 모든 강좌를 리턴시켜줍니다.
     *
     * @param int $userid
     * @param int $roleid
     * @return array
     */
    public function getUserCourseAll($userid = null, $roleid = null)
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $query = "SELECT
                            " . $this->getCourseSelectField() . "
                FROM        {course} c
                JOIN        {course_ubion} cu ON c.id = cu.course
                JOIN        {context} ctx ON ctx.contextlevel = :contextlevel AND ctx.instanceid = cu.course
                JOIN        {role_assignments} ra ON ra.contextid = ctx.id
                WHERE       ra.userid = :userid";

        $param = array(
            'userid' => $userid,
            'contextlevel' => CONTEXT_COURSE
        );

        if (! empty($roleid)) {
            $query .= " AND ra.roleid = :roleid";
            $param['roleid'] = $roleid;
        }

        $query .= " ORDER BY    cu.year, cu.semester_code";

        return $DB->get_records_sql($query, $param);
    }

    /**
     * 강좌 알림 기능 사용여부 리턴
     *
     * @param int|object $courseorid
     * @return boolean
     */
    public function isNotification($courseorid)
    {
        $isSite = false;
        if (is_number($courseorid)) {
            if ($courseorid == SITEID) {
                $isSite = true;
            }
        } else {
            if ($courseorid->id == SITEID) {
                $isSite = true;
            }
        }

        // SITE이면 false가 리턴 되어야 함.
        if ($isSite) {
            return false;
        } else {
            if (isset($courseorid->notification)) {
                return $courseorid->notification;
            } else {
                $courseUbion = $this->getUbion($courseorid);
                return $courseUbion->notification;
            }
        }
    }

    /**
     * 검색 가능한 연도
     *
     * @param string $pYear
     * @param object $currentSemester
     * @return array
     */
    public function getAllowYearParameter($pYear, $currentSemester = null)
    {
        $defaultYear = date('Y');

        $lmsStartYear = $this->getLMSStartYear($defaultYear);
        $csmsStartYear = $this->getCSMSStartYear($defaultYear);

        // 현재학기 설정값이 전달이 안되었으면 현재학기 값 가져오기
        if (empty($currentSemester)) {
            $currentSemester = $this->getCurrentSemester();
        }

        // 연도 파라메터에 대한 예외처리
        if (empty($pYear)) {
            if ($currentSemester) {
                $pYear = $currentSemester->year;
            } else {
                $pYear = $defaultYear;
            }
        } else {
            // 전달된 연도가 lms시작연도보다 작으면 lms시작연도로 고정
            // 그리고 전달된 연도가 현재 연도보다 크면 현재 연도로 설정
            if ($pYear < $lmsStartYear) {
                $pYear = $lmsStartYear;
            } else if ($pYear > $defaultYear) {
                $pYear = $defaultYear;
            }
        }

        return array(
            $pYear,
            $lmsStartYear,
            $csmsStartYear
        );
    }

    /**
     * LMS 시작 연도
     *
     * @param int $defaultYear
     * @return int
     */
    public function getLMSStartYear($defaultYear = null)
    {
        if (empty($defaultYear)) {
            $defaultYear = date('Y');
        }

        $year = get_config('local_ubion', 'lms_start_year');

        // lms(코스모스가 아닌 학교 lms) 시작값이 설정되어 있지 않으면 현재 연도 설정
        if (empty($year)) {
            $year = $defaultYear;
        }

        return $year;
    }

    /**
     * LMS 시작 연도
     *
     * @param int $defaultYear
     * @return int
     */
    public function getCSMSStartYear($defaultYear = null)
    {
        if (empty($defaultYear)) {
            $defaultYear = date('Y');
        }

        $year = get_config('local_ubion', 'csms_start_year');

        // lms(코스모스가 아닌 학교 lms) 시작값이 설정되어 있지 않으면 현재 연도 설정
        if (empty($year)) {
            $year = $defaultYear;
        }

        return $year;
    }

    /**
     * 검색 가능한 학기
     *
     * @param string $pSemester
     * @param object $currentSemester
     * @return array
     */
    public function getAllowSemesterParameter($pSemester, $currentSemester = null)
    {
        $defaultSemester = self::CODE_1;
        $lmsStartSemester = $this->getLMSStartSemester($defaultSemester);
        $csmsStartSemester = $this->getCSMSStartSemester($defaultSemester);

        // 현재학기 설정값이 전달이 안되었으면 현재학기 값 가져오기
        if (empty($currentSemester)) {
            $currentSemester = $this->getCurrentSemester();
        }

        // 전달된 학기 유효성 검사
        $pSemester = $this->getSemesterValidation($pSemester);

        return array(
            $pSemester,
            $lmsStartSemester,
            $csmsStartSemester
        );
    }

    /**
     * LMS 시작 연도
     *
     * @param int $defaultYear
     * @return int
     */
    public function getLMSStartSemester($defaultSemester = null)
    {
        if (empty($defaultSemester)) {
            $defaultSemester = date('Y');
        }

        $semester = get_config('local_ubion', 'lms_start_semester');

        // lms(코스모스가 아닌 학교 lms) 시작값이 설정되어 있지 않으면 현재 연도 설정
        if (empty($semester)) {
            $semester = $defaultSemester;
        }

        return $semester;
    }

    /**
     * LMS 시작 연도
     *
     * @param int $defaultYear
     * @return int
     */
    public function getCSMSStartSemester($defaultSemester = null)
    {
        if (empty($defaultSemester)) {
            $defaultSemester = date('Y');
        }

        $semester = get_config('local_ubion', 'csms_start_semester');

        // lms(코스모스가 아닌 학교 lms) 시작값이 설정되어 있지 않으면 현재 연도 설정
        if (empty($semester)) {
            $semester = $defaultSemester;
        }

        return $semester;
    }

    /**
     * 전달된 학기의 유효성 검사
     * 빈값이나, 등록된 학기가 아니라면 1학기로 리턴됩니다.
     *
     * @param string $semester
     * @param object $currentSemester
     * @return string
     */
    public function getSemesterValidation($semester, $currentSemester = null)
    {
        // 학기 파라메터에 대한 예외처리
        if (empty($semester)) {
            // 현재학기가 설정되어 있으면 현재학기가 먼저 선택되어야 함.
            if ($currentSemester) {
                $semester = $currentSemester->semester_code;
            } else {
                // 현재학기가 없으면 무조건 1학기가 선택
                $semester = self::CODE_1;
            }
        } else {
            // 전달된 학기가 정규학기(1학기, 여름학기, 2학기, 겨울학기)인지 확인
            // 만약 정규가가 아니면 1학기로 고정
            if (array_key_exists($semester, $this->getSemesters())) {
                // 무조건 대문자로..
                $semester = strtoupper($semester);
            } else {
                // 등록된 학기가 아니면 무조건 1학기로 표시
                $semester = self::CODE_1;
            }
        }

        return $semester;
    }

    /**
     * 사용자 일괄 추가
     *
     * @param int $courseid
     * @param string $elementName
     * @param int $roleid
     * @return boolean[]|NULL[]|string[]
     */
    public function setUserBatch($courseid, $elementName = 'uploaduser', $roleid = null)
    {
        global $CFG, $DB;

        require_once $CFG->libdir . '/phpexcel/PHPExcel.php';

        $isSuccess = false;
        $message = null;
        $html = null;

        if (! isset($_FILES[$elementName])) {
            $message = get_string('uploadfile_notfound', 'local_ubion') . '<br/>';
        } else {

            // 저장경로
            $savePath = $CFG->tempdir . '/uploaduser';

            // 저장 경로가 없으면 생성해줘야됨.
            if (! is_dir($savePath)) {
                mkdir($savePath);
            }

            // 첨부파일 확장자 검사.
            $extentions = array(
                'xls',
                'xlsx'
            );

            $path = pathinfo($_FILES[$elementName]['name']);
            $ext = strtolower($path['extension']);

            if (! in_array($ext, $extentions)) {
                Javascript::getAlert(get_string('uploadfile_excel', 'local_ubion'));
            }

            $filename = $savePath . '/' . time();
            // 일회성 파일이기 때문에 파일 이름 중복 검사는 딱히 안해줘도 상관 없음.
            move_uploaded_file($_FILES[$elementName]['tmp_name'], $filename);

            $param = array(
                'courseid' => $courseid,
                'enrol' => 'manual'
            );
            if ($instance = $DB->get_record('enrol', $param)) {
                $enrolManual = enrol_get_plugin('manual');

                $excel = PHPExcel_IOFactory::load($filename);
                $excelSheet = $excel->getActiveSheet()->toArray(null, false, false, false);

                $stdfields = array(
                    'username'
                );

                // 사용할수 있는 컬럼 목록 가져오기
                $fileColumns = array();
                // 첫번째 row의 내용을 가지고 판단함..
                foreach ($excelSheet[0] as $key => $column) {
                    if (! empty($column)) {

                        $lccolumn = strtolower($column);

                        // 사용할수 있는 컬럼 목록에 존재하는 값인지 확인.
                        if (in_array($column, $stdfields) or in_array($lccolumn, $stdfields)) {
                            // standard fields are only lowercase
                            $newcolumn = $lccolumn;
                        } else {
                            $message .= get_string('invalidfieldname', 'error', $column) . '<br/>';
                        }

                        // 중복된 컬럼명이 존재하는지 확인.
                        if (in_array($newcolumn, $fileColumns)) {
                            $message .= get_string('duplicatefieldname', 'error', $newcolumn) . '<br/>';
                        }
                        $fileColumns[$key] = $newcolumn;
                    }
                }

                // roleid값이 따로 지정되어 있지 않으면 학생 권한으로 등록
                if (empty($roleid)) {
                    $roleid = \local_ubion\course\Course::getInstance()->getRoleStudent();
                }

                $html = '<h3 class="mt-0 mb-4">Result</h3>';
                $html .= '<div class="table-responsive">';
                $html .= '<table class="table table-bordered table-coursemos">';
                $html .= '<colgroup>';
                $html .= '<col class="wp-50" />';
                $html .= '<col />';
                $html .= '<col class="wp-250" />';
                $html .= '</colgroup>';
                $html .= '<thead>';
                $html .= '<tr>';
                $html .= '<th>' . get_string('number', 'local_ubion') . '</th>';
                foreach ($fileColumns as $fs) {
                    $html .= '<th>' . $fs . '</th>';
                }
                $html .= '<th>status</th>';
                $html .= '</tr>';
                $html .= '</thead>';
                $html .= '<tbody>';

                $number = 1;
                $transaction = $DB->start_delegated_transaction();
                // 데이터 재구성
                foreach ($excelSheet as $index => $excelRow) {
                    // 0번째 row는 title이므로 패스해야됨..
                    if ($index > 0) {
                        $excelDataRow = array();
                        foreach ($excelRow as $cellIndex => $cell_value) {
                            // filecolumns에 존재하는 컬럼만 추가해줘야됨.
                            if (array_key_exists($cellIndex, $fileColumns)) {
                                $columnName = $fileColumns[$cellIndex];
                                $excelDataRow[$columnName] = $cell_value;
                            }
                        }

                        $excelDataRowStatus = array();
                        if (isset($excelDataRow['username'])) {
                            // $stdusername = clean_param($excelDataRow['username'], PARAM_USERNAME);
                            // 2013-01-23 USERNAME에서 strtolower하기 때문에 대소문자가 안맞아서 오류가 발생함..
                            $stdusername = clean_param($excelDataRow['username'], PARAM_NOTAGS);

                            if ($excelDataRow['username'] != $stdusername) {
                                $excelDataRowStatus[] = get_string('invalidusernameupload');
                            }

                            // 실제 사용자가 존재하는지 확인
                            $usernamefield = get_all_user_name_fields(true);
                            if ($userInfo = $DB->get_record_sql('SELECT id, username, ' . $usernamefield . ' FROM {user} WHERE mnethostid = :mnethostid AND username = :username', array(
                                'username' => $stdusername,
                                'mnethostid' => $CFG->mnet_localhost_id
                            ))) {

                                // 강좌에 등록시켜주기
                                $enrolManual->enrol_user($instance, $userInfo->id, $roleid);

                                $excelDataRowStatus[] = '<span class="text-success bold">Success : </span>' . fullname($userInfo) . ' (' . $userInfo->username . ')';
                            } else {
                                $excelDataRowStatus[] = get_string('not_found_user', 'local_ubion', $stdusername);
                            }
                        } else {
                            $excelDataRowStatus[] = get_string('missingusername');
                        }

                        $html .= '<tr>';
                        $html .= '<td class="text-center">' . $number . '</td>';
                        foreach ($excelDataRow as $columnName => $columnValue) {

                            $html .= '<td>' . $columnValue . '</td>';
                        }

                        $html .= '<td>' . implode('<br/>', $excelDataRowStatus) . '</td>';
                        $html .= '</tr>';

                        $number ++;
                    }
                }
                $transaction->allow_commit();
                $html .= '</tbody>';
                $html .= '</table>';
                $html .= '</div>';

                $isSuccess = true;
            } else {
                $message = get_string('not_found_enrol_manual', 'local_ubion');
            }
        }

        return array(
            $isSuccess,
            $message,
            $html
        );
    }

    /**
     * 각 강좌 타이별 명칭 (label 형태로 리턴해줍니다.)
     *
     * @param string $coursetype
     * @param string $courseGubunCode
     *
     * @return string
     */
    public function getTypeLabel($coursetype, $courseGubunCode = null)
    {
        $class = '';

        switch ($coursetype) {
            case self::TYPE_REGULAR:
                $class = 'label-coursetype-re';
                break;
            case self::TYPE_IRREGULAR:
                $class = 'label-coursetype-ir';
                break;
            case self::TYPE_PRACTICE:
                $class = 'label-coursetype-pr';
                break;
            case self::TYPE_OPEN:
                $class = 'label-coursetype-op';
                break;
            case self::TYPE_ECLASS:
                $class = 'label-coursetype-ec';
                break;
            case self::TYPE_ONLINE:
                $class = 'label-coursetype-on';
                break;
            default:
                $class = 'label-coursetype-mi';
                break;
        }

        // 세부 분류값이 전달된 경우
        if (! empty($courseGubunCode)) {

            // 세부 분류가 존재한다면 각 coursetype 함수를 생성해서 필요한 값을 리턴해주도록 구현해주시면 됩니다.
            // 예 : coursetype값이 R이고 courseGubunCode값이 pratic인경우
            // getTypeLabelR() 함수 생성해주고 필요한 label, text값 리턴해주도록 구현해주면 됩니다.
            $functionName = 'getTypeLabel' . strtoupper($coursetype);
            // error_log($functionName);

            // 함수가 등록되지 않았다면 상위 switch값 그대로 전달해줌.
            if (method_exists($this, $functionName)) {
                list ($class, $text) = $this->$functionName($courseGubunCode);
            }
        }

        $text = get_string("course_type_" . strtolower($coursetype), 'local_ubion');

        return '<label class="label label-coursetype ' . $class . '">' . $text . '</label>';
    }

    /**
     * 강좌 타입별 세부 분류
     *
     * @param \stdClass $course
     * @return NULL|string
     */
    public function getTypeSubLabel(\stdClass $course)
    {
        // course_type값이 없으면 빈값 리턴
        $courseType = $course->course_type ?? null;
        if (empty($courseType)) {
            return null;
        }

        $addClass = '';
        $text = null;
        $functionName = 'getCourseTypeSubLabel' . strtoupper($course->course_type);

        if (method_exists($this, $functionName)) {
            list ($addClass, $text) = $this->$functionName($course);
        }

        // text가 존재할 경우 진행
        if (! empty($text)) {
            return '<div class="label label-coursetype label-coursetype-under ' . $addClass . '">' . $text . '</div>';
        } else {
            return null;
        }
    }

    /**
     * 정규 강좌 세부 타이틀 명칭 (학부/대학원 구분)
     *
     * @param \stdClass $course
     * @return string
     */
    public function getCourseTypeSubLabelR(\stdClass $course)
    {
        // 필요값이 전달되었는지 확인
        if (isset($course->group_code)) {
            $groupCode = $course->group_code;
        } else {
            if ($courseUbion = $this->getUbion($course->id)) {
                $groupCode = $courseUbion->groupCode;
            }
        }

        $class = $text = null;

        if (! empty($groupCode)) {
            if ($groupCode == self::GROUPCODE_G) {
                $text = get_string('group_code_g', 'local_ubion');
            } else {
                $text = get_string('group_code_u', 'local_ubion');
            }
        }

        return array(
            $class,
            $text
        );
    }

    /**
     * 코스모스에서 기본적으로 제공되는 강좌 타입
     *
     * 특별한 경우가 아니라면 특정 타입을 삭제 하기 위해서 해당 함수는 override 하지 말고 getCourseTypes()에서 처리해주시기 바랍니다.
     *
     * @param boolean $isAll
     * @return stdClass[]
     */
    protected function getBaseCourseTypes($isAll = true)
    {
        $types = [
            '' => $this->getCourseTypeObject('', get_string('course_type_all', 'local_ubion')),
            self::TYPE_REGULAR => $this->getCourseTypeObject(self::TYPE_REGULAR, get_string('course_type_' . strtolower(self::TYPE_REGULAR), 'local_ubion')),
            self::TYPE_IRREGULAR => $this->getCourseTypeObject(self::TYPE_IRREGULAR, get_string('course_type_' . strtolower(self::TYPE_IRREGULAR), 'local_ubion')),
            self::TYPE_ONLINE => $this->getCourseTypeObject(self::TYPE_ONLINE, get_string('course_type_' . strtolower(self::TYPE_ONLINE), 'local_ubion')),
            self::TYPE_ECLASS => $this->getCourseTypeObject(self::TYPE_ECLASS, get_string('course_type_' . strtolower(self::TYPE_ECLASS), 'local_ubion')),
            self::TYPE_MIS => $this->getCourseTypeObject(self::TYPE_MIS, get_string('course_type_' . strtolower(self::TYPE_MIS), 'local_ubion'))
        ];

        if (empty($isAll)) {
            unset($types['']);
        }

        return $types;
    }

    /**
     * 강좌 타입 리턴
     *
     * @param boolean $isAll
     * @param boolean $isMustache
     * @return string[]
     */
    public function getCourseTypes($isAll = true, $isMustache = true)
    {
        $types = $this->getBaseCourseTypes($isAll);


        if ($isMustache) {
            $types = array_values($types);
        }

        return $types;
    }

    /**
     * coursetype object
     *
     * @param string $key
     * @param string $text
     * @return \stdClass
     */
    protected function getCourseTypeObject($key, $text)
    {
        $type = new \stdClass();
        $type->key = $key;
        $type->text = $text;
        $type->selected = false;

        return $type;
    }

    /**
     * 강좌 썸네일 파일
     *
     * /lib/coursecatlib.php => get_course_overviewfiles() 함수와 99% 동일함..
     * (new course_in_list()->get_course_overviewfiles() 호출하기 보다 함수를 가져다가 수정해서 사용하는게 좀 더 효율적일거 같음)
     *
     * @param int $courseid
     *
     * @return array|array
     */
    public function getOverviewFiles($courseid)
    {
        global $CFG;

        if (empty($CFG->courseoverviewfileslimit)) {
            return array();
        }
        require_once ($CFG->libdir . '/filestorage/file_storage.php');
        require_once ($CFG->dirroot . '/course/lib.php');
        $fs = get_file_storage();
        $context = context_course::instance($courseid);
        $files = $fs->get_area_files($context->id, 'course', 'overviewfiles', false, 'filename', false);
        if (count($files)) {
            $overviewfilesoptions = course_overviewfiles_options($courseid);
            $acceptedtypes = $overviewfilesoptions['accepted_types'];
            if ($acceptedtypes !== '*') {
                // Filter only files with allowed extensions.
                require_once ($CFG->libdir . '/filelib.php');
                foreach ($files as $key => $file) {
                    if (! file_extension_in_typegroup($file->get_filename(), $acceptedtypes)) {
                        unset($files[$key]);
                    }
                }
            }
            if (count($files) > $CFG->courseoverviewfileslimit) {
                // Return no more than $CFG->courseoverviewfileslimit files.
                $files = array_slice($files, 0, $CFG->courseoverviewfileslimit, true);
            }
        }
        return $files;
    }

    /**
     * 강좌 대표 이미지 리턴
     *
     * 강좌 대표 이미지 설정되지 않았으면 빈값이 리턴됩니다.
     *
     * @param int $courseid
     * @param boolean $escaped
     * @return NULL|string
     */
    public function getOverviewFileImage($courseid, $escaped = true)
    {
        $imageurl = null;
        if ($files = $this->getOverviewFiles($courseid)) {
            foreach ($files as $f) {
                if ($f->is_valid_image()) {
                    $path = '/' . $f->get_contextid() . '/' . $f->get_component() . '/' . $f->get_filearea() . $f->get_filepath() . $f->get_filename();
                    $moodleurl = new \moodle_url('/pluginfile.php' . $path);
                    $imageurl = $moodleurl->out($escaped);
                    break;
                }
            }
        }

        return $imageurl;
    }

    /**
     * 강좌 대표 이미지
     * 강좌 대표 이미지가 지정되어 있지 않으면 주 담당 교수 사진이 리턴됩니다.
     *
     * @param int $courseid
     * @return string
     */
    public function getImageURL($courseid)
    {
        global $OUTPUT, $PAGE;

        if (empty($PAGE->context)) {
            $PAGE->set_context(\context_system::instance());
        }

        // 강좌에 썸네일 이미지가 지정되었는지 확인
        $overviewfile = $this->getOverviewFileImage($courseid);

        // 등록된 이미지가 없는 경우 담당 교수 사진 출력
        if (empty($overviewfile)) {
            if ($prof = $this->getMainProfessor($courseid)) {
                $overviewfile = \local_ubion\user\User::getInstance()->getPicture($prof, 100)->out();
            } else {
                // 교수자가 없으면 기본 사용자 사진 출력
                $overviewfile = $OUTPUT->image_url('u/f1');
            }
        }

        return $overviewfile;
    }

    /**
     * 강좌 진도처리 사용여부
     *
     * @param int $courseid
     * @return boolean
     */
    public function isProgress($courseid)
    {
        $isProgress = false;
        if ($courseUbion = $this->getUbion($courseid)) {
            $isProgress = $courseUbion->progress;
        }

        return $isProgress;
    }

    /**
     * 강좌 주차 리턴
     *
     * @param object $course
     * @return number
     */
    public function getNumSections($course)
    {
        return $course->numsections ?? 0;
    }
}