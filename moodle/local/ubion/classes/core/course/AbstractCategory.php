<?php
namespace local_ubion\core\course;

use coursecat;
global $CFG;
require_once $CFG->libdir . '/coursecatlib.php';

abstract class AbstractCategory
{

    public function setInsert($idnumber, $name, $parent = 0)
    {
        $category = new \stdClass();
        $category->name = $name;
        $category->idnumber = $idnumber;
        $category->descriptionformat = 1;
        $category->sortorder = 999;
        $category->parent = $parent;

        $category = coursecat::create($category);

        return $category;
    }

    public function setUpdate($id, $idnumber, $name, $parent = 0)
    {
        $coursecat = coursecat::get($id, MUST_EXIST, true);

        $category = new \stdClass();
        $category->id = $id;
        $category->name = $name;
        $category->idnumber = $idnumber;
        $category->parent = $parent;

        $coursecat->update($category);
    }
}