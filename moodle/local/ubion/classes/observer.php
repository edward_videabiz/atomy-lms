<?php

/**
 * Event observer for local_ubion.
 */
class local_ubion_observer
{

    /**
     * Triggered via course_module_created event.
     *
     * @param \core\event\course_deleted $event
     */
    public static function course_deleted(\core\event\course_deleted $event)
    {
        global $DB;

        // 강좌 번호
        $courseid = $event->courseid;

        $param = [
            'course' => $courseid
        ];

        // course_ubion에 레코드가 존재하는지 확인
        if ($course_ubion_id = $DB->get_field_sql('SELECT id FROM {course_ubion} WHERE course = :course', $param)) {
            $DB->delete_records('course_ubion', array(
                'id' => $course_ubion_id
            ));
        }
    }
}