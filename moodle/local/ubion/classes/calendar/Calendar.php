<?php
namespace local_ubion\calendar;

use local_ubion\core\calendar\AbstractCalendar;

class Calendar extends AbstractCalendar {
    
    private static $instance;
    
    /**
     * 강좌 Class
     * @return Calendar
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
    
    
}