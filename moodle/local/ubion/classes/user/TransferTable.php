<?php
namespace local_ubion\user;

class TransferTable extends \local_ubion\core\user\AbstractTransferTable
{

    protected $CCourse = null;

    public function __construct(array $filters = null)
    {
        $this->CCourse = \local_ubion\course\Course::getInstance();

        // 컬럼 정의
        $this->addColumns('year', get_string('year', 'local_ubion'), 'text-center', 'wp-70');

        // 입력한 컬럼 반영
        $this->setDefineColumns();

        // colgroup 출력
        $this->isPrintColgroup = true;

        parent::__construct();
    }

    function query_db($pagesize, $useinitialsbar = false)
    {
        error_log('transfer');
    }

    public function col_semester($data)
    {
        return $this->CCourse->getSemesterName($data->semester_code);
    }

    public function col_name($data)
    {
        // return '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$data->id.'">'.$this->CCourse->getName($data).'</a>';
        return 'col_name 구현필요';
    }

    protected function getNotFoundMessage()
    {
        return get_string('no_mycourses', 'local_ubion');
    }
}