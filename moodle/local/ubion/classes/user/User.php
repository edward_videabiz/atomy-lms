<?php
namespace local_ubion\user;

use local_ubion\core\user\AbstractUser;

class User extends AbstractUser
{

    private static $instance;

    /**
     * User
     *
     * @return User
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c();
        }
        return self::$instance;
    }
}