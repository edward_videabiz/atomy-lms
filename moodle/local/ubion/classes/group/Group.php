<?php
namespace local_ubion\group;

use local_ubion\core\group\AbstractGroup;

class Group extends AbstractGroup
{

    private static $instance;

    /**
     * 강좌 Class
     *
     * @return Group
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c();
        }
        return self::$instance;
    }
}