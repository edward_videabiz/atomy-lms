<?php
namespace local_ubion\vn;

use stdClass;
use local_ubion\base\Parameter;
use local_ubion\base\Javascript;

class Course extends \local_ubion\controller\Controller
{
    private static $instance;
    
    use \local_ubion\core\traits\TraitDB;
    
    
    /**
     * Course
     * @return Course
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
    
    /**
     * 강좌 상세 정보
     *
     * @return string
     */
    public function doCourseDetail()
    {
        global $DB;
        
        $id = Parameter::post('id', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'id' => array(
                'required',
                $this->validate()::PLACEHOLDER => '강좌 ID',
                $this->validate()::PARAMVALUE => $id
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            if ($course = $DB->get_record_sql("SELECT * FROM {course} WHERE id = :id", array(
                'id' => $id
            ))) {
                
                $courseUbion = $DB->get_record_sql("SELECT * FROM {course_ubion} WHERE course = :course", array(
                    'course' => $id
                ));
                
                $html = '<div class="row">';
                $html .= '<div class="col-xs-12 col-md-6">';
                $html .= '<h4>course</h4>';
                $html .= '<dl class="dl-horizontal">';
                foreach ($course as $key => $value) {
                    // 비밀번호는 표시할 필요 없음
                    if ($key == 'password')
                        continue;
                        
                        $html .= '<dt>' . $key . '</dt>';
                        $html .= '<dd class="mb-2">' . $value . '</dd>';
                }
                $html .= '</dl>';
                $html .= '</div>';
                $html .= '<div class="col-xs-12 col-md-6">';
                $html .= '<h4>course ubion</h4>';
                if (empty($courseUbion)) {
                    $html .= 'NULL';
                } else {
                    $html .= '<dl class="dl-horizontal">';
                    foreach ($courseUbion as $key => $value) {
                        $html .= '<dt>' . $key . '</dt>';
                        $html .= '<dd class="mb-2">' . $value . '</dd>';
                    }
                    $html .= '</dl>';
                }
                $html .= '</div>';
                $html .= '</div>';
                
                if (Parameter::isAajx()) {
                    $fullname = \local_ubion\course\Course::getInstance()->getName($course);
                    Javascript::printJSON(array(
                        'code' => Javascript::getSuccessCode(),
                        'msg' => get_string('view_complete', 'local_ubion'),
                        'html' => $html,
                        'fullname' => $fullname
                    ));
                } else {
                    return $html;
                }
            } else {
                Javascript::printAlert('존재하지 않는 강좌입니다. [' . $id . ']');
            }
        }
    }
    
    /**
     * 강좌 공개 여부 변경
     *
     * @return boolean
     */
    public function doOpen()
    {
        global $CFG, $DB;
        
        $id = Parameter::post('id', null, PARAM_INT);
        $openType = Parameter::post('opentype', null, PARAM_ALPHANUMEXT);
        
        $validate = $this->validate()->make(array(
            'id' => array(
                'required',
                $this->validate()::PLACEHOLDER => '강좌 ID',
                $this->validate()::PARAMVALUE => $id
            ),
            'opentype' => array(
                'required',
                $this->validate()::PLACEHOLDER => '강좌 공개 여부',
                $this->validate()::PARAMVALUE => $openType
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            if ($course = $DB->get_record_sql("SELECT * FROM {course} WHERE id = :id", array(
                'id' => $id
            ))) {
                require_once $CFG->libdir . '/enrollib.php';
                
                $guestEnrol = $DB->get_record_sql("SELECT id, status FROM {enrol} WHERE enrol = :enrol AND courseid = :courseid", array(
                    'enrol' => 'guest',
                    'courseid' => $id
                ));
                
                $status = ENROL_INSTANCE_ENABLED;
                if ($openType == 'openCancel') {
                    $status = ENROL_INSTANCE_DISABLED;
                }
                
                // 강좌에 손님 접속 등록방법이 존재하지 않으면 추가해줘야됨.
                if (empty($guestEnrol)) {
                    $course = course_get_format($id)->get_course();
                    
                    // guest enrol이 등록되어 있지 않기 때문에 enrol을 등록시켜줘야됨.
                    $plugin = enrol_get_plugin('guest');
                    $plugin->add_instance($course, array(
                        'status' => $status,
                        'password' => ''
                    ));
                } else {
                    if ($guestEnrol->status != $status) {
                        $updateEnrol = new stdClass();
                        $updateEnrol->id = $guestEnrol->id;
                        $updateEnrol->status = $status;
                        $DB->update_record('enrol', $updateEnrol);
                    }
                }
                
                if (Parameter::isAajx()) {
                    Javascript::printJSON(array(
                        'code' => Javascript::getSuccessCode(),
                        'msg' => get_string('save_complete', 'local_ubion')
                    ));
                } else {
                    return true;
                }
            } else {
                Javascript::printAlert('존재하지 않는 강좌입니다. [' . $id . ']');
            }
        }
    }
    
    /**
     * 강좌 폐강 여부 변경
     *
     * @return boolean
     */
    public function doVisible()
    {
        global $DB;
        
        $id = Parameter::post('id', null, PARAM_INT);
        $visibleType = Parameter::post('visibletype', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'id' => array(
                'required',
                $this->validate()::PLACEHOLDER => '강좌 ID',
                $this->validate()::PARAMVALUE => $id
            ),
            'visibletype' => array(
                'required',
                $this->validate()::PLACEHOLDER => '강좌 폐강 여부',
                $this->validate()::PARAMVALUE => $visibleType
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            
            if ($course = $DB->get_record_sql("SELECT id, visible FROM {course} WHERE id = :id", array(
                'id' => $id
            ))) {
                
                if ($course->visible != $visibleType) {
                    $updateCourse = new \stdClass();
                    $updateCourse->id = $course->id;
                    $updateCourse->visible = $visibleType;
                    
                    $DB->update_record('course', $updateCourse);
                    
                    rebuild_course_cache($course->id);
                }
                
                if (Parameter::isAajx()) {
                    Javascript::printJSON(array(
                        'code' => Javascript::getSuccessCode(),
                        'msg' => get_string('save_complete', 'local_ubion')
                    ));
                } else {
                    return true;
                }
            } else {
                Javascript::printAlert('존재하지 않는 강좌입니다. [' . $id . ']');
            }
        }
    }
    
    /**
     * course_ubion 업데이트
     */
    public function doUbionUpdate()
    {
        $result = $this->setDBUpdate('course_ubion');
        
        if ($result) {
            $courseid = Parameter::post('course', null, PARAM_INT);
            
            if (! empty($courseid) && $courseid > SITEID) {
                // 변경사항이 존재하기 때문에 캐시 새로고침
                \local_ubion\course\Course::getInstance()->getUbion($courseid, true);
            }
        }
        
        $returnurl = Parameter::post('returnurl', null, PARAM_NOTAGS);
        if (empty($returnurl)) {
            $returnurl = \local_ubion\asite\Asite::getInstance()->sGetUrl('', 'mod', 'regular', 'index');
        } else {
            $returnurl = base64_decode($returnurl);
        }
        
        redirect($returnurl);
    }
}