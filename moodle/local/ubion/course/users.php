<?php
require '../../../config.php';
require_once $CFG->dirroot.'/course/lib.php';

use \local_ubion\base\Parameter;
use \local_ubion\base\Common;
use \local_ubion\group\Group;
use \local_ubion\user\User;
use \local_ubion\course\Course;

$id = required_param('id', PARAM_INT);

$pluginname = 'local_ubion';


// 이메일 표시여부
$isPrintEmail = true;

// 핸드폰 번호 표시여부
$isPrintPhone = true;

// 마지막 접속시간
$isPrintLastaccess = true;


$CCourse = Course::getInstance();
$CGroup = Group::getInstance();
$CUser = User::getInstance();

$i8n = new stdClass();
$i8n->participants = get_string('participants');
$i8n->search = get_string('search');
$i8n->group = get_string('groups', 'group');
$i8n->idnumber = get_string('idnumber');
$i8n->fullnameuser = get_string('fullnameuser');


$page         = optional_param('page', 1, PARAM_INT);
$roleid       = optional_param('roleid', 0, PARAM_INT);
$groupid      = optional_param('groupid', 0, PARAM_INT);
$ls           = optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);
$keyfield     = optional_param('keyfield', null, PARAM_ALPHANUMEXT);
$keyword      = Parameter::getKeyword();

// 정렬
$sby        = optional_param(Parameter::getSortBy(), 'idnumber', PARAM_ALPHANUMEXT);
$sorder         = optional_param(Parameter::getSortOrder(), 'ASC', PARAM_ALPHANUM);

$defaultURL = new moodle_url('/local/ubion/course/users.php', array(
    'id' => $id
));

$PAGE->set_url($defaultURL);

$course = course_get_format($id)->get_course();
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);

$PAGE->set_pagelayout('incourse');
$PAGE->set_title($course->shortname." : ".$i8n->participants);
$PAGE->set_heading($course->fullname);
$PAGE->set_context($context);
$PAGE->set_pagetype('course-users');
$PAGE->add_body_class('path-user');                     // So we can style it independently
$PAGE->set_other_editing_capability('moodle/course:manageactivities');

$PAGE->navbar->add($i8n->participants, $defaultURL);


// 강좌에서 그룹을 분류해서 사용하는지 확인
$courseGroup = $CGroup->getCourseGroups($course->id);

// 강좌내 역할 모두 가져오기
$courseRoles = role_fix_names(get_profile_roles($context), $context, ROLENAME_ALIAS, true);

// 검색 항목에 표시될 role (교수는 따로 표시되기 때문에 역할 조회에서 교수자는 제외해줘야됨)
$searchCourseRoles = Common::getArrayCopy($courseRoles);

$isProfessor = $CCourse->isProfessor($course->id);


// 검색 옵션
$keyFieldOptions = array(
    'idnumber' => $i8n->idnumber
    ,'fullname' => $i8n->fullnameuser
);

// 학생이 검색을 통해서 특정 사용자의 학번을 추측할수 있기 때문에
// 교수자가 아니면 학번 검색이 되면 안됨
if (!$isProfessor) {
    unset($keyFieldOptions['idnumber']);
    
    if ($keyfield == 'idnumber') {
        $keyfield = 'fullname';
    }
}

$profRoleID = $CCourse->getRoleProfessor();
$assiRoleID = $CCourse->getRoleAssistant();

// 교수자 영역에 표시될 role
// 만약 조교까지 포함 하고 싶다면
// $teacherRoleIDS = array($profRoleID, $assiRoleID)
// 을 사용하시면 됩니다.
$teacherRoleIDS = array($profRoleID);
$teacherIDS = join(",", $teacherRoleIDS);

if ($teacherRoleIDS) {
    foreach ($teacherRoleIDS as $tid) {
        
        unset($searchCourseRoles[$tid]);
    }
}

// 교수자 목록만 뽑아오기
list($profUserCount, $profUsers) = $CCourse->getCourseUsers($course, null, null, null, null, $teacherIDS, $page, 0);


// roleid값이 전달이 되었으면 해당 role만 검색되도록 변수값 조정해줘야됨.
$isRoleEqual = false;
if (!empty($roleid)) {
    $teacherIDS = $roleid;
    $isRoleEqual = true;
}

$isPrint = array(
    'group' => $courseGroup->is
    ,'email' => $isPrintEmail
    ,'phone' => $isPrintPhone
    ,'lastaccess' => $isPrintLastaccess
);

$filters = array(
    'id' => $id
    ,'keyfield' => $keyfield
    ,'keyword' => $keyword
    ,'groupid' => $groupid
    ,'roleid' => $teacherIDS
    ,'isRoleEqual' => $isRoleEqual
    ,'page' => $page
    ,'ls' => $ls
);

$userTable = new \local_ubion\course\UsersTable($course, $courseRoles, $isProfessor, $isPrint, $filters);

$baseurl = new moodle_url('/local/ubion/course/users.php', array(
    'id' => $id
    ,'keyfield' => $keyfield
    ,'keyword' => $keyword
    ,'groupid' => $groupid
    ,'roleid' => $roleid
    ,'page' => $page
    ,'ls' => $ls
));
$userTable->define_baseurl($baseurl);

$PAGE->requires->css('/local/ubion/course/styles.css');

echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->participants);
?>
	<div class="course-users">
		<form class="form-horizontal well form-search">
			
			<div class="form-group">
            	<label class="control-label col-sm-3"><?= get_string('listsize', 'local_ubion'); ?></label>
            	<div class="col-sm-9">
    				<select name="ls" class="form-control form-control-auto">
    					<?php
    					if ($listSizes = Parameter::getListSizes()) {
    					    foreach ($listSizes as $lsKey => $lsValue) {
    					        $selected = ($lsKey == $ls) ? ' selected="selected"' : '';
    					        echo '<option value="'.$lsKey.'" '.$selected.'>'.$lsValue.'</option>';
    					    }
    					}
    					?>
    				</select>
            	</div>
            </div>
			
			<?php 
			$searchCourseRoleCount = count($searchCourseRoles);
			
			// 역할이 1개 이상인 경우 표시
			if ($searchCourseRoleCount > 1) {
			?>
			<div class="form-group row">
				<label class="control-label col-sm-3"><?= get_string('role'); ?></label>
				<div class="col-sm-9">
					<select name="roleid" class="form-control-auto">
						<option value=""><?= get_string('allparticipants'); ?></option>
        			<?php
        			if ($courseRoles) {
        			    foreach ($searchCourseRoles as $roleKey => $roleName) {
        			        $selected = ($roleKey == $roleid) ? ' selected="selected"' : '';
        			        echo '<option value="'.$roleKey.'" '.$selected.'>'.$roleName.'</option>';
        			    }
        			}
        			?>
        			</select>
				</div>
			</div>
			
			<?php 
			}
			
            if ($courseGroup->is) {
            ?>
			<div class="form-group row">
				<label class="control-label col-sm-3"><?=$i8n->group;?></label>
				<div class="col-sm-9">
					<select name="groupid" class="form-control-auto">
						<?php
						echo $CGroup->getOptions($courseGroup, $groupid);
						?>
					</select>
				</div>
			</div>
			<?php
            }
            ?>
			<div class="form-group">
            	<label class="control-label col-sm-3"><?= $i8n->search ?></label>
            	<div class="col-sm-9 form-inline">
            		<input type="hidden" name="id" value="<?= $id; ?>" />
        			<select name="keyfield" class="form-control">
        				<?php 
        				foreach ($keyFieldOptions as $kKey => $kText) {
        				    $selected = ($kKey == $keyfield) ? ' selected="selected"' : '';
        				    echo '<option value="'.$kKey.'" '.$selected.'>'.$kText.'</option>';
        				}
        				?>
        			</select>
        			
                	<input type="text" name="keyword" placeholder="<?= get_string('keyword', $pluginname); ?>" class="form-control" value="<?= $keyword ?>" />
                	
                	<button type="submit" class="btn btn-default"><?= $i8n->search; ?></button>
                	
                	<?php 
                	// 검색 취소
                	if ((!empty($keyfield) && !empty($keyword)) || !empty($groupid)) {
                	    echo '<a href="'.$defaultURL.'" class="btn btn-default">'.get_string('search_cancel', 'local_ubion').'</a>';
                	}
                	?>
            	</div>
            </div>
		</form>
		
		<div class="users">
			
			<div class="panel panel-default panel-collapse panel-professor">
				<div class="panel-heading">
					<a href="#" class="panel-heading-link">
						<?php
						$roles = role_fix_names(get_all_roles($context), $context);
						echo $roles[$CCourse->getRoleProfessor()]->localname ?? get_string('professor', 'local_ubion'); 
						?>
					</a>
				</div>

                <div class="panel-body">
                	<div class="row">
                    	<?php 
            			if ($profUserCount > 0) {
            			    foreach ($profUsers as $pf) {
            			        $profFullname = fullname($pf);
            			?>
						<div class="col-xs-4 col-lg-2">
							<div class="card-box">
								<div class="user-thumb">
                                    <img src="<?= $CUser->getPicture($pf, 100); ?>" alt="<?= $profFullname; ?>" class="img-thumb" />
                                </div>
                                <div class="">
                                    <h5 class="mt-2 mb-0 text-center"><?= $profFullname; ?></h5>
                                </div>
							</div>
						</div>
						<?php
            			    }
            			} else {
            			    echo '<div class="col-xs-12">'.get_string('not_found_users', 'local_ubion').'</div>';
            			}
            			?>
					</div>
				</div>
			</div>
			
			<div class="panel panel-default panel-collapse panel-student">
				<div class="panel-heading">
					<a href="#" class="panel-heading-link">
						<?= $i8n->participants; ?>
					</a>
				</div>

				<div class="panel-body">
					<?php 
					echo $userTable->out($ls, false);
					?>
				</div>
			</div>
		</div>
	</div>


<?php 
echo $OUTPUT->footer();