<?php
require_once '../../../config.php';

$id = required_param('id', PARAM_INT);
$pluginname = 'local_ubion';

$CCourse = \local_ubion\course\Course::getInstance();

// 강좌 정보
$course = $CCourse->getCourse($id);
$courseContext = context_course::instance($course->id);

$PAGE->set_context($courseContext);
$PAGE->set_course($course);

// 반드시 로그인이 필요함.
require_login($course);

$i8n = new stdClass();
$i8n->title = get_string('course_setting', $pluginname);
$i8n->yes = get_string('yes');
$i8n->no = get_string('no');

$PAGE->set_url('/local/ubion/course/setting.php', array('id' => $id));
$PAGE->set_pagelayout('incourse');
$PAGE->navbar->add($i8n->title, $PAGE->url->out());
$PAGE->set_title($i8n->title);

// 관리 권한이 존재하는지 확인
$isProfessor = $CCourse->isProfessor($course->id, $courseContext);
if (!$isProfessor) {
    echo $OUTPUT->notification(get_string('no_permissions', $pluginname));
}


$args = array(
    'course' => $course
);
$form = new \local_ubion\course\SettingForm(null, $args);

if ($form->is_cancelled()) {
    // 취소시 강좌 메인으로 이동
    redirect($CFG->wwwroot.'/course/view.php?id='.$id);
} else if ($data = $form->get_data()) {
    
    $controllder = \local_ubion\course\Controller::getInstance();
    
    $type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
    if ($controllder->isSubmit($type)) {
        $controllder->invoke($type);
    }
    
} else {

    // 등록된 썸네일 이미지 가져오기
    $overviewfilesoptions = course_overviewfiles_options($course);
    if ($overviewfilesoptions) {
        file_prepare_standard_filemanager($course, 'overviewfiles', $overviewfilesoptions, $courseContext, 'course', 'overviewfiles', 0);
    }
    
    // 필요한 값 $form에 전달해줘야됨.
    $form->set_data($course);
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading($i8n->title, 2);
    
    echo $form->display();
    
    echo $OUTPUT->footer();
}