$(function() {
	$('.coursemos-admin .dropmenu').click(function(e){
		e.preventDefault();
		$(this).closest('li').toggleClass('open');

	});
	
	
	$('.coursemos-admin .logo-toggler .toggler').click(function(e){
		coursemos_menu_backdrop = 'coursemos_menu_backdrop';
		
		if($('body').hasClass('coursemos-page-sm') || $('body').hasClass('coursemos-page-xs')) {
			if($('.coursemos-admin').hasClass('coursemos-menu-show')) {
				$('.coursemos-admin').removeClass('coursemos-menu-show');
				$("." + coursemos_menu_backdrop).remove();
				
			} else {
				
				
				// 기존에 등록된 항목이 있으면 삭제
				if ($("." + coursemos_menu_backdrop).length > 0) {
					$("." + coursemos_menu_backdrop).remove();
				}
				
				$("body").append($("<div />").addClass(coursemos_menu_backdrop));
				
				$("." + coursemos_menu_backdrop).click(function() {
					$(this).remove();
					$("body").removeClass('coursemos-menu-show');
				});
				
				
				$('.coursemos-admin').addClass('coursemos-menu-show');
			}
			
		} else {
			$('.coursemos-admin').toggleClass('coursemos-menu-collapsed');
		}
	});
	

	
	$('.panel-widget .panel-heading').click(function() {
		heading = $(this);
		 panel = $(this).parents('.panel');
		 
        if (panel.hasClass('panel-collapsed')) { 
       	 	panel.removeClass('panel-collapsed');
       	 	heading.find('.panel-icon').removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
        }
        else {
       	 	panel.addClass('panel-collapsed');
       	 	heading.find('.panel-icon').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
        }
        
        return false;
    });
		
	$(".form-control-ls").change(function() {
		var ls = $(this).val();

		if($.isNumeric(ls)) {

			var queryParameters = {}, queryString = location.search.substring(1),
		    re = /([^&=]+)=([^&]*)/g, m;

			while (m = re.exec(queryString)) {
			    queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
			}

			queryParameters['ls'] = ls;
			// ls 변경으로 인해서 page값이 변경이 되는데 기존 page갯수보다 작아지면..
			// 문제가 생기기 때문에 ls값을 변경 하면 무조건 page 1로 이동으로 시켜야됨.
			queryParameters['page'] = 1;

			location.search =  $.param(queryParameters);
		}
	});
	
	
	/* ---------- Acivate Functions ---------- */
	// template_functions();
			
});

/* ---------- Numbers Sepparator ---------- */

function numberWithCommas(x) {
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1.$2");
    return x;
}

/* ---------- Template Functions ---------- */		
		
function template_functions(){
	/* ---------- form validate ---------- */
	$("form.form-validate").validate();
	
	/* ---------- Disable moving to top ---------- */
	$('a[href="#"][data-top!=true]').click(function(e){
		e.preventDefault();
	});
		
	/* ---------- Datapicker ---------- */
	$(".input-datepicker").datepicker({
		autoclose: true
		,language : 'kr'
		,format : 'yyyy-mm-dd'
	});
	
	/* ---------- Tooltip ---------- */
	$('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});

	/* ---------- Popover ---------- */
	$('[rel="popover"],[data-rel="popover"]').popover();

	/* ---------- File Manager ---------- 
	var elf = $('.file-manager').elfinder({
		url : 'misc/elfinder-connector/connector.php'  // connector URL (REQUIRED)
	}).elfinder('instance');
	*/
	
	
	$('.dropdown-toggle').dropdown();
}


function coursemos_backdrop() {
	
	
}