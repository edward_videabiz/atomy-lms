<?php

if (!defined('SITECFG')) {
    define('SITECFG', true);
 
    // moodle config include
    require_once __DIR__.'/../../../config.php';
    
    global $CFG;
    
    if (empty($CFG)) {
        exit;
    }
    
    // 로그인 체크
    require_login();
    
    if (!is_siteadmin()) {
        \local_ubion\base\Javascript::getAlertMove(get_string('no_permissions', 'local_ubion'), $CFG->wwwroot, true);
    }
    
    
    global $SITECFG;
    $SITECFG = new \stdClass();
    $SITECFG->path = __DIR__;
    $SITECFG->modulePath = "{$SITECFG->path}/modules";
    
    
    // http://domain.com/local/ubion/Asite 주소
    $SITECFG->wwwroot = $CFG->wwwroot.'/local/ubion/';
    $SITECFG->realwww = str_replace($CFG->dirroot, '', $SITECFG->path);
    $SITECFG->realwww  = trim($_SERVER['HTTP_HOST'],'/') . str_replace('\\', '/', $SITECFG->realwww);
    if (empty($_SERVER['HTTPS'])) {
        $SITECFG->realwww = 'http://'.$SITECFG->realwww;
    } else {
        $SITECFG->realwww = 'https://'.$SITECFG->realwww;
    }
    
    // CAsite 위치 변경하시면 오류 납니다. 
    $CAsite = \local_ubion\asite\Asite::getInstance();
    
    
    // 현재 전체 모듈 ex) haksa/haksa/member
    $SITECFG->module = $CAsite->sGetModule();
    
    // 현재 모듈을 배열로 쪼갠 것
    $SITECFG->moduleArr = explode('/', $SITECFG->module);
    
    // 모듈 depth
    $SITECFG->moduleDepth = count($SITECFG->moduleArr);
    
    // 현재 모듈명 ex) member
    $SITECFG->moduleName = $SITECFG->moduleArr[($SITECFG->moduleDepth-1)];
    
    // 현재 액션명
    $SITECFG->act = $CAsite->sGetAction();
    
    
    
    // 현재 URL
    $SITECFG->nowUrl = $CAsite->sGetUrl();
    
    // 현재 모둘 url
    $SITECFG->moduleUrl = $CAsite->sGetUrl('') .'/'. $SITECFG->module;
    
    // 현재 모듈 실제 url _SERVER['PAHT_INFO'] 없는 실제 URL. ex) /local/ubion/Asite/modules/haksa/course
    $SITECFG->moduleRealUrl = $SITECFG->realwww .'/modules/'. $SITECFG->module;
    $SITECFG->moduleRelativePath = str_replace($CFG->wwwroot, '', $SITECFG->moduleRealUrl);
    
    //현재 액션 URL
    $SITECFG->actionUrl = $SITECFG->moduleUrl;
    if (!empty($SITECFG->act)) {
        $SITECFG->actionUrl .= '/@' . $SITECFG->act;
    }
    
    $SITECFG->homeUrl = $CAsite->sGetUrl('');//dirname($_SERVER['PHP_SELF']);
    
    
    
    /*
     * 공통 파일 인클루드
     * modules/locallib.php, modules/모듈/locallib.php 파일들이 있다면 인클루드 한다.
     * 모든 모듈에 적용이 되야할 코드가 있다면 modules/locallib.php 파일에 삽입한다.
     * 특정 모듈에만 공통으로 적용되야 할 코드가 있다면 module/모듈/locallib.php 파일에 삽입한다.
     * 최대 2단계까지 가져옴
     * /local/ubion/Asite/modules/모듈명/locallib.php
     * /local/ubion/Asite/modules/모듈명/depth1/locallib.php
     * /local/ubion/Asite/modules/모듈명/depth1/depth2/locallib.php
     */
    $file = $SITECFG->modulePath.'/locallib.php';
    if (is_file($file)) {
        include_once $file;
    }
    
    if (isset($SITECFG->moduleArr[0])) {
        $file = $SITECFG->modulePath.'/'.$SITECFG->moduleArr[0].'/locallib.php';
        if (is_file($file)) {
            include_once $file;
        }
    }
    
    if (isset($SITECFG->moduleArr[0]) && isset($SITECFG->moduleArr[1])) {
        $file = $SITECFG->modulePath.'/'.$SITECFG->moduleArr[0].'/'.$SITECFG->moduleArr[1].'/locallib.php';
        if (is_file($file)) {
            include_once $file;
        }
    }
    
    $SITECFG->cronConfigFile = $CFG->dataroot.'/haksaCron.cache.php';
    
    
    // 무들 에러 방지용
    $PAGE->set_url($CAsite->sGetUrl());
    $PAGE->set_course($SITE);
    $PAGE->set_context(context_system::instance());
    $PAGE->set_pagelayout('coursemosadmin');
    $PAGE->set_title('Coursemos - '.$CAsite->sGetModuleName($SITECFG->module));
    
    // 필요한 css 및 script 로드
    $PAGE->requires->css('/local/ubion/Asite/style/layout.css');
    $PAGE->requires->css('/local/ubion/Asite/style/layout-responsive.css');
    
    //$PAGE->requires->js('/local/ubion/Asite/javascript/core.js');
    $PAGE->requires->js('/local/ubion/Asite/javascript/custom.js');
    
    
    // 사전에 정의된 위치에 style.css, module.js 파일이 존재한다면 자동으로 로드 시켜줌
    $currentDir = $SITECFG->modulePath.'/'.$SITECFG->module;
    if (is_file($currentDir.'/styles.css')) {
        $PAGE->requires->css($SITECFG->moduleRelativePath.'/styles.css');
    }
    
    if (is_file($currentDir.'/module.js')) {
        $PAGE->requires->js($SITECFG->moduleRelativePath.'/module.js');
    }
    
    
    
    //require_once $SITECFG->modulePath.'/auth/lib.php';
    //$auth = new Asite_modules_auth_lib();
    //$auth->authRedirectUrl($SITECFG->module);
    
    
    
    
    // 메뉴 선언
    
    
    // error_log(print_r($siteMenu, true));
}