<?php

use local_ubion\base\Common;

$CCourse = \local_ubion\course\Course::getInstance();

// 필수값 체크해야됨.
$CVNCourse = \local_vn\Course::getInstance();
$config = $CVNCourse->getConfig();

// 수강해야될 강좌 번호
$onCourseID = $config->courseid ?? null;
$offCourseID = $config->courseid_offline ?? null;



$courseClosure = function($courseid) use ($CCourse) {
    $course = $CCourse->getCourse($courseid);
    $modinfo = get_fast_modinfo($course);
    
    $courseSections = array();
    if ($sections = $modinfo->get_section_info_all()) {
        foreach ($sections as $ss) {
            $cmids = $modinfo->sections[$ss->section] ?? null;
            
            $section = new stdClass();
            $section->section = $ss->section;
            $section->cmids = $cmids;
            $section->cmCount = count($cmids);
            $section->name = $CCourse->getSectionName($course, $ss->section);
            
            
            $courseSections[$ss->section] = $section;
        }
    }
    
    $info = new stdClass();
    $info->id = $courseid;
    $info->name = $CCourse->getName($course);
    $info->modinfo = $modinfo;
    $info->sections = $courseSections;
    
    return $info;
};

$courses = [];
$courses['on'] = $courseClosure($onCourseID);
$courses['off'] = $courseClosure($offCourseID);


echo $OUTPUT->header();

?>
<div class="asite-content">
	<ul class="nav nav-tabs" role="tablist">
		<?php 
		$isFirst = true;
		foreach ($courses as $courseType => $course) {
		    $class = ($isFirst) ? 'active' : ''; 
		    echo '<li role="presentation" class="'.$class.'">';
		    echo     '<a href="#'.$courseType.'" aria-controls="'.$courseType.'" role="tab" data-toggle="tab">';
		    echo          $course->name;
		    echo     '</a>';
		    echo '</li>';
		    
		    $isFirst = false;
		}
		?>
	</ul>
	<div class="tab-content">
		<?php 
		$isFirst = true;
		foreach ($courses as $courseType => $course) {
		    $class = ($isFirst) ? 'active' : '';
            ?>
		    <div role="tabpanel" class="tab-pane <?= $class; ?>" id="<?= $courseType; ?>">
		    	<div class="table-responsive">
            		<table class="table table-bordered table-coursemos">
            			<colgroup>
            				<col class="wp-50" />
            				<col class="wp-200" />
            				<col />
            			</colgroup>
            			<thead>
            				<tr>
            					<th class="text-center"><?=get_string('number', 'local_ubion');?></th>
            					<th class="text-center"><?=get_string('sectionname', 'local_vn');?></th>
            					<th class="text-center"><?=get_string('sectionname_sub', 'local_vn');?></th>
            				</tr>
            			</thead>
            			<tbody>
                			<?php
                			if (!empty($course->sections)) {
                			    foreach ($course->sections as $cs) {
                			        if ($cs->cmCount > 0) {
                			            $first = true;
                    			        
                    			        foreach ($cs->cmids as $cmid) {
                    			            echo '<tr>';
                    			            $cminfo = $course->modinfo->get_cm($cmid);
                    			            
                    			            if ($first) {
                    			                
                    			                echo '<td rowspan="'.$cs->cmCount.'" class="text-center">'.$cs->section.'</td>';
                    			                echo '<td rowspan="'.$cs->cmCount.'">'.$cs->name.'</td>';
                    			            }
                    			            
                    			            echo '<td>';
                    			            echo     '<img src="'.$cminfo->get_icon_url().'" alt="'.$cminfo->modname.'" class="mr-2" />';
                    			            echo     $cminfo->name;
                    			            echo '</td>';
                    			            
                    			            $first= false;
                    			            echo '</tr>';
                    			        }
                    			        
                			        } else {
                			            echo '<tr>';
                			            echo     '<td class="text-center">'.$cs->section.'</td>';
                			            echo     '<td colspan="3">'.get_string('no_modules', 'local_vn').'</td>';
                			            echo '</tr>';
                			        }
                			    }
                			} else {
                			    echo '<tr><td colspan="5" class="p-4">'.get_string('no_sections', 'local_vn').'</td></tr>';
                			}
                            ?>
            			</tbody>
            		</table>
                </div>
                <div class="text-right">
                	<a href="<?=$CFG->wwwroot.'/course/view.php?id='.$course->id.'&edit=on&sesskey='.sesskey()?>" class="btn btn-primary"><?=get_string('course_editing', 'local_vn')?></a>
                </div>
		    </div>
    		
                
		    <?php 
		    $isFirst = false;
		}
		?>
    </div>
</div>

<?php
echo $OUTPUT->footer();