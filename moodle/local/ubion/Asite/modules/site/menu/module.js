M.local_ubion_Asite_site_menu = {
		pluginname : 'local_ubion'
};

M.local_ubion_Asite_site_menu.index = function(Y, courseid) {
	$(".content-submenu .nav-submenu li a").each(function() {
		paramCourseID = getURLParameter($(this).attr('href'), 'courseid');

		activeClass = false;
		if (courseid == 1) {
			activeClass = (paramCourseID === null) ? true : false;	
		} else if (courseid == 0) {
			activeClass = (paramCourseID == courseid) ? true : false;
		}

		if (activeClass) {
			$(this).parent().addClass('active');
		} else {
			$(this).parent().removeClass('active');
		}
		
		// 여기만 예외사항임.
		navtext = (courseid == 1) ? '사이트 메뉴' : '강좌 메뉴';
		
		$(".content-breadcrumb h2").text(navtext);
	});
}