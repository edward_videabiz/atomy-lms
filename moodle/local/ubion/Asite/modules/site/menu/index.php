<?php
use \local_ubion\base\Common;

$pluginname = 'local_ubmenu';

// class
$CMenu = \local_ubmenu\Menu::getInstance();

// parameter
$courseid = optional_param('courseid', 1, PARAM_INT);   

// 0 : 강좌, 1 : 사이트
/* atomy에서는 사이트 메뉴가 존재하지 않음
if ($courseid > SITEID) {
    $courseid = SITEID;
} else if ($courseid < 0) {
    $courseid = 0;
}
*/
$courseid = 0;

$isSiteMenu = false;
if (empty($courseid)) {
    $menus = $CMenu->getAdminCourseMenus();
} else {
    $isSiteMenu = true;
    $menus = $CMenu->getAdminSiteMenus();
}

$PAGE->requires->strings_for_js(array(
    'add'
    ,'notsubmenu'
    ,'modify_title'
    ,'delete_confirm'
), $pluginname);


$PAGE->requires->js_call_amd('local_ubmenu/menu', 'management', array(
    'courseid' => $courseid
));

$PAGE->requires->js_init_call('M.local_ubion_Asite_site_menu.index', array('courseid'=>$courseid));

echo $OUTPUT->header();

// 현재 설치되어 있는 언어팩 목록 가져오기
$installLangs = Common::getInstallLangs();
$languages = Common::getLangs($installLangs);
$defaultLang = Common::getRequiredLanguage();
?>
<div class="local-ubmenu-management">
	
	<p class="text-danger">
		<i class="fa fa-info-circle"></i>
		<?= get_string('cache_message', $pluginname); ?>
	</p>
	<div class="box-content">
		
	<?php 
		$moveIcon = $OUTPUT->image_url('i/move_2d');
		if (empty($menus)) {
		    echo '<div class="well">'.get_string('nomenus', $pluginname).'</div>';
		} else {
		    echo '<ul class="menu">';
		    foreach ($menus as $ms) {
		        // 관리페이지에서는 강좌별 환경에 따른 설정은 실행되면 안되고, 디비값 그대로 출력되어야 함.
		        $ms = $CMenu->settingMenu($ms, $courseid, false);
		        $hideClass = (empty($ms->visible)) ? 'menuHide' : '';
		        
		        echo '<li data-id="'.$ms->id.'" class="'.$hideClass.'">';
		        echo      '<img src="'.$moveIcon.'" alt="move" class="moveicon" />';
		        echo      '<span class="menu-name">'.$ms->name.'</span>';
		        if (isset($ms->nameUnSerialize)) {
		            echo      '<div class="menu-lang">';
		            // 언어팩은 무조건 한개는 존재하게 되어 있으므로 예외처리 없음.
		            foreach ($languages as $ls) {
		                // 한글은 위에서 표시했기 때문에 무시
		                if ($ls == $defaultLang) {
		                    continue;
		                }
		                
		                echo  '<div class="menu-lang-item">';
		                echo      '<label class="menu-lang-title">'.$installLangs[$ls].' : </label>';
		                echo      $ms->nameUnSerialize[$ls] ?? '';
		                echo  '</div>';
		            }
		            echo      '</div>';
		        }
		        
		        echo      '<div class="menu-url">['.$ms->url.']</div>';
		        echo      '<div class="menu-label">';
		        if (empty($ms->visible)) {
		            echo      '<span class="label label-danger">'.get_string('use_n', $pluginname).'</span>';
		        }
		        if (!empty($ms->notedit)) {
		            echo      '<span class="label label-danger">'.get_string('notedit_n', $pluginname).'</span>';
		        }
		        
		        if (!empty($ms->blank)) {
		            echo      '<span class="label label-info">'.get_string('new_window', $pluginname).'</span>';
		        }
		        echo      '</div>';
		        
		        echo      '<div class="menu-actions">';
		        //echo         '<div class="btn-group">';
		        echo             '<button type="button" class="btn btn-info btn-menu-insert"> '.get_string('add').' </button>';
		        echo             '<button type="button" class="btn btn-default btn-menu-update"> '.get_string('modify', 'local_ubion').' </button>';
		        echo             '<button type="button" class="btn btn-danger btn-menu-delete"> '.get_string('delete').' </button>';
		        //echo         '</div>';
		        echo      '</div>';
		        
		        // 2단계 시작
		        echo      '<ul>';
		        if (!empty($ms->submenu)) {
		            foreach ($ms->submenu as $mss) {
		                // 관리페이지에서는 강좌별 환경에 따른 설정은 실행되면 안되고, 디비값 그대로 출력되어야 함.
		                $mss = $CMenu->settingMenu($mss, $courseid, false);
		                $subHideClass = (empty($mss->visible)) ? 'menuHide' : '';
		                
		                echo  '<li data-id="'.$mss->id.'" class="'.$hideClass.' '.$subHideClass.'">';
		                echo      '<img src="'.$moveIcon.'" alt="move" class="moveicon" />';
		                echo      '<span class="menu-name">'.$mss->name.'</span>';
		                echo      '<div class="menu-label">';
		                if (empty($mss->visible)) {
		                    echo     '<span class="label label-danger">'.get_string('use_n', $pluginname).'</span>';
		                }
		                
		                if (!empty($mss->notedit)) {
		                    echo     '<span class="label label-danger">'.get_string('notedit_n', $pluginname).'</span>';
		                }
		                
		                if (!empty($mss->blank)) {
		                    echo     '<span class="label label-info">'.get_string('new_window', $pluginname).'</span>';
		                }
		                echo      '</div>';
		                if (isset($mss->nameUnSerialize)) {
		                    echo      '<div class="menu-lang">';
		                    // 언어팩은 무조건 한개는 존재하게 되어 있으므로 예외처리 없음.
		                    foreach ($languages as $ls) {
		                        // 한글은 위에서 표시했기 때문에 무시
		                        if ($ls == $defaultLang) {
		                            continue;
		                        }
		                        
		                        echo  '<div class="menu-lang-item">';
		                        echo      '<label class="menu-lang-title">'.$installLangs[$ls].' : </label>';
		                        echo      $mss->nameUnSerialize[$ls] ?? '';
		                        echo  '</div>';
		                    }
		                    echo      '</div>';
		                }
		                echo      '<div class="menu-url">['.$mss->url.']</div>';
		                echo      '<div class="menu-actions">';
		                //echo         '<div class="btn-group">';
		                echo             '<button type="button" class="btn btn-info btn-menu-insert"> '.get_string('add').' </button>';
		                echo             '<button type="button" class="btn btn-default btn-menu-update"> '.get_string('modify', 'local_ubion').' </button>';
		                echo             '<button type="button" class="btn btn-danger btn-menu-delete"> '.get_string('delete').' </button>';
		                //echo         '</div>';
		                echo      '</div>';
		                echo  '</li>';
		            }
		        }
		        echo      '</ul>';
		        echo '</li>';
		    }
		    echo '</ul>';
		}
		?>
		
        
        <div class="actions text-right">
        	<div class="pull-left text-left div-save">
        		<button type="button" class="btn btn-success btn-menu-save"><?= get_string('move_save', $pluginname)?></button>
        		<span class="text-danger">
        			<?= get_string('move_save_help', $pluginname)?>
        		</span>
        	</div>
        	<button type="button" class="btn btn-primary btn-menu-add"><?= get_string('add', $pluginname); ?></button>
        </div>
        
        
        <div id="modal-menu" class="modal fade" tabindex="-1" role="dialog">
        	<div class="modal-dialog modal-lg" role="document">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title"><?= get_string('add', $pluginname); ?></h4>
        			</div>
        			<div class="modal-body">
        				<form method="post" action="<?= $CFG->wwwroot.'/local/ubmenu/action.php'; ?>" class="form-horizontal form-validate" id="modal-form">
                        	<div class="form-group form-group-name">
                        		<label class="col-md-3 control-label"><?= get_string('stringid', $pluginname); ?></label>
                        		<div class="col-md-9">
                        			<input type="hidden" name="<?php echo $CMenu::TYPENAME; ?>" value="insert" />
                        			<input type="hidden" name="id" />
                        			<input type="hidden" name="courseid" value="<?php echo $courseid; ?>" />
                        			<input type="hidden" name="parentid" />
                        			<input type="text" name="stringid" class="required alpha" placeholder="<?= get_string('stringid', $pluginname); ?>" />
                        			<div class="help-block text-danger">
                        				<?= get_string('stringid_help', $pluginname); ?>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="form-group form-group-name">
                        		<label class="col-md-3 control-label"><?= get_string('name', $pluginname); ?></label>
                        		<div class="col-md-9">
                        		<?php 
                        		if ($languages) {
                        		    $requiredLang = Common::getRequiredLanguage();
                        		    // 언어팩에 대한 예외처리 없음.
                        		    foreach ($languages as $ls) {
                        		        $requireClass = ($ls == $requiredLang) ? 'required' : '';
                        		        
                        		        echo '<div class="form-group">';
                        		        echo     '<label class="col-sm-3 control-label">'.$installLangs[$ls].'</label>';
                        		        echo     '<div class="col-sm-9">';
                        		        echo         '<input type="text" name="lang-'.$ls.'" class="'.$requireClass.' form-control" />';
                        		        echo     '</div>';
                        		        echo '</div>';
                        		    }
                        		}
                    			?>
                    				
                        		</div>
                        	</div>
                        	<div class="form-group form-group-url">
                        		<label class="col-md-3 control-label"><?= get_string('url', $pluginname); ?></label>
                        		<div class="col-md-9">
                        			<div class="form-inline">
                            			<input type="text" name="url" class="required w-75" placeholder="<?= get_string('url', $pluginname); ?>" />
                            			&nbsp;
                            			<div class="checkbox">
                                			<label>
                                				<input type="checkbox" name="blank" value="1" /> <?= get_string('new_window', $pluginname); ?>
                                			</label>
                            			</div>
                        			</div>
                        			<div class="help-block text-info">
                        				<?= get_string('url_help', $pluginname); ?>
                        			</div>
                        		</div>
                        	</div>
                        	
                        	<div class="form-group form-group-class">
                        		<label class="col-md-3 control-label"><?= get_string('class', $pluginname); ?></label>
                        		<div class="col-md-9">
                        			<input type="text" name="class" placeholder="<?= get_string('class', $pluginname); ?>" />
                        			<div class="help-block text-info">
                        				<?= get_string('class_help', $pluginname); ?>
                        			</div>
                        		</div>
                        	</div>
                        	
                        	<div class="form-group form-group-viewlevel">
                        		<label class="col-md-3 control-label"><?= get_string('accesslevel', $pluginname); ?></label>
                        		<div class="col-md-9">
                        			<label class="radio-inline">
                        				<input type="radio" name="viewlevel" value="0" checked="checked" class="form-check-input" /> <?= get_string('accesslevel_all', $pluginname); ?>
                        			</label>
                    				<?php 
                    				// 사이트메뉴가 아닐 경우에만 표시해줘야됨.
                    				if (!$isSiteMenu) {
                                        echo '<label class="radio-inline">';
                    				    echo    '<input type="radio" name="viewlevel" value="'.\local_ubion\user\User::getInstance()::TYPE_PROFESSOR.'" class="form-check-input" /> '.get_string('accesslevel_prof', $pluginname);
                        				echo '</label>';
                    				} else {
                    				    echo '<div class="form-text text-info">';
                    				    echo    '사이트 메뉴에서는 학교별로 신분값이 각각 다르기 때문에 전체 허용만 가능합니다.<br/>';
                    				    echo    '학교 신분별로 메뉴 표시 여부를 달리 하고 싶으신 경우에는 Menu.php 파일의 isMenuView() 함수를 이용해서 처리 해주시기 바랍니다.';
                    				    echo '</div>';
                    				}
                        			?>
                        		</div>
                        	</div>
                        	
                        	<div class="form-group form-group-useyn">
                        		<label class="col-md-3 control-label"><?= get_string('use_yn', $pluginname); ?></label>
                        		<div class="col-md-9">
                        			<label class="radio-inline">
                        				<input type="radio" name="visible" value="1" checked="checked" class="form-check-input" /> <?= get_string('use_y', $pluginname); ?>
                        			</label>
                    			
                        			<label class="radio-inline">
                        				<input type="radio" name="visible" value="0" class="form-check-input" /> <?= get_string('use_n', $pluginname); ?>
                        			</label>
                        		</div>
                        	</div>
                        	<div class="form-group form-group-collapseyn">
                        		<label class="col-md-3 control-label"><?= get_string('collapse_yn', $pluginname); ?></label>
                        		<div class="col-md-9 form-inline">
                        			<label class="radio-inline">
                        				<input type="radio" name="collapse" value="0" checked="checked" class="form-check-input" /> <?= get_string('collapse_n', $pluginname); ?>
                        			</label>
                        			<label class="radio-inline">
                        				<input type="radio" name="collapse" value="1" class="form-check-input" /> <?= get_string('collapse_y', $pluginname); ?>
                        			</label>
                        			<div class="help-block text-info">
                        				<?= get_string('collapse_help', $pluginname); ?>
                        			</div>
                        		</div>
                        	</div>
                        	<div class="form-group form-group-notedityn">
                        		<label class="col-md-3 control-label"><?= get_string('notedit_yn', $pluginname); ?></label>
                        		<div class="col-md-9 form-inline">
                        			<label class="radio-inline">
                        				<input type="radio" name="notedit" value="0" checked="checked" class="form-check-input" /> <?= get_string('notedit_y', $pluginname); ?>
                        			</label>
                        			<label class="radio-inline">
                        				<input type="radio" name="notedit" value="1" class="form-check-input" /> <?= get_string('notedit_n', $pluginname); ?>
                        			</label>
                        			<div class="help-block text-info">
                        				<?= get_string('notedit_help', $pluginname); ?>
                        			</div>
                        		</div>
                        	</div>
                        
                        	<div class="form-group">
                        		<div class="col-md-offset-3 col-md-9">
                        			<button type="submit" class="btn btn-primary"><?= get_string('save', 'local_ubion')?></button>
                        			<button type="button" class="btn btn-default" data-dismiss="modal"><?= get_string('close', 'local_ubion'); ?></button>
                        		</div>
                        	</div>
                        </form>
        			</div>
        		</div><!-- /.modal-content -->
        	</div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
	</div>
</div>
<?php 
echo $OUTPUT->footer();
?>