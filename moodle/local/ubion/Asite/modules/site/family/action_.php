<?php

$controllder = \theme_coursemos\FamilySites::getInstance();

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}