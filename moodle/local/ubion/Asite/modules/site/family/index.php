<?php
use \local_ubion\base\Common;

$pluginname = 'theme_coursemos';

$i8n = new stdClass();
$i8n->add = get_string('add');
$i8n->edit = get_string('edit');
$i8n->delete = get_string('delete');

// class
$CThemeFamily = \theme_coursemos\FamilySites::getInstance();


// 현재 설치되어 있는 언어팩 목록 가져오기
$installLangs = Common::getInstallLangs();
$languages = Common::getLangs($installLangs);

$PAGE->requires->js_call_amd('theme_coursemos/strings', 'management', array(
    'url' => $CAsite->sGetUrl('', 'mod', 'site/family', 'act', 'action_')
));

echo $OUTPUT->header();


$moveIcon = $OUTPUT->image_url('i/move_2d');


// strings에서 지원하는 type 목록
// key => title 형태로 입력해주시면 됩니다.
$siteTypes = array(
    $CThemeFamily::TYPE_HEADER_TOP => get_string('family_type_header_top', $pluginname),
    $CThemeFamily::TYPE_HEADER_MIDDLE => get_string('family_type_header_middle', $pluginname),
    $CThemeFamily::TYPE_HEADER_BOTTOM => get_string('family_type_header_bottom', $pluginname),
    $CThemeFamily::TYPE_LOGIN_FAMILY => get_string('family_type_login_family', $pluginname),
    $CThemeFamily::TYPE_LOGIN_BUTTON => get_string('family_type_login_button', $pluginname)
);

// 반복되서 사용되는 항목 closure로 구현
$closureFamilyItem = function($sites) use ($CThemeFamily, $moveIcon, $installLangs, $languages){
    $html = '';
    if (!empty($sites)) {
        $defaultLang = Common::getRequiredLanguage();
        
        foreach ($sites as $ss) {
            
            $langSerialize = unserialize($ss->value);
            
            // 한글이 지정안되어 있으면 영어로 표시
            $siteName = $langSerialize[$defaultLang] ?? $langSerialize['en'];
            
            $html .= '<li class="sites" data-id="'.$ss->id.'">';
            $html .=     '<h5 class="site-name">';
            $html .=         '<img src="'.$moveIcon.'" alt="move" class="moveicon" />';
            
            // 이미지가 등록된 경우 표시
            if (!empty($ss->icon)) {
                $html .=     ' <i class="fa '.$ss->icon.'"></i> ';
            }
            $html .=         '<span class="site-title">'.$siteName.'</span>';
            $html .=         ' <small><a href="'.$ss->url.'" target="_blank">'.$ss->url.'</a></small>';
            
            $html .=     '</h5>';
            $html .=     '<ul class="lang">';
            
            
            // 언어팩은 무조건 한개는 존재하게 되어 있으므로 예외처리 없음.
            foreach ($languages as $ls) {
                // 한글은 위에서 표시했기 때문에 무시
                if ($ls == $defaultLang) {
                    continue;
                }
                
                $html .=      '<li class="lang-item">';
                $html .=          '<label class="lang-title">'.$installLangs[$ls].'</label>';
                $html .=          $langSerialize[$ls] ?? '';
                $html .=      '</li>';
            }
            
            $html .=         '<li class="lang-item">';
            $html .=             '<button type="button" class="btn btn-xs btn-info btn-string-update" data-id="'.$ss->id.'">'.get_string('edit').'</button> ';
            $html .=             '<button type="button" class="btn btn-xs btn-danger btn-string-delete" data-id="'.$ss->id.'">'.get_string('delete').'</button> ';
            $html .=         '</li>';
            $html .=     '</ul>';
            $html .= '</li>';
        }
    } else {
        $html .= '<li class="sites">'.get_string('family_nosites', 'theme_coursemos').'</li>';
    }
    
    return $html;
};

?>

<div class="theme-coursemos-family">
	<p class="text-danger mb-4">
		<i class="fa fa-info-circle"></i>
		<?= get_string('cache_message', $pluginname); ?>
	</p>
	<div class="box-content">
		<?php 
		
		foreach ($siteTypes as $sType => $sText) {
		    $typeSites = $CThemeFamily->getLists($sType);
		    
		    echo '<fieldset class="family-box">';
		    echo      '<legend>';
		    echo          $sText;
		    echo          ' <button type="button" class="btn btn-sm btn-primary btn-string-add" data-type="'.$sType.'">'.$i8n->add.'</button>';
		    echo      '</legend>';
		    echo      '<ul class="family-sites" data-type="'.$sType.'">';
		    echo          $closureFamilyItem($typeSites);
		    echo      '</ul>';
		    echo '</fieldset>';
		}
		
		?>
		
		
	</div>
	
	<div class="actions text-right">
    	<button type="button" class="btn btn-primary btn-string-add"><?=$i8n->add?></button>
    </div>
	
	<div id="modal-family" class="modal fade" tabindex="-1" role="dialog">
    	<div class="modal-dialog modal-lg" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    				<h4 class="modal-title">Stringid <span class="modal-title-changetype"><?= $i8n->add; ?></span></h4>
    			</div>
    			<div class="modal-body">
    				<form method="post" class="form-horizontal form-validate" id="modal-form" action="<?= $CAsite->sGetUrl('act', 'action_'); ?>">
                    	<div class="form-group form-group-name">
                    		<label class="col-md-3 control-label">stringid</label>
                    		<div class="col-md-9">
                    			<input type="hidden" name="<?= $CThemeFamily::TYPENAME; ?>" value="insert" />
                    			<input type="hidden" name="id" />
                    			<input type="text" name="stringid" class="required form-control" placeholder="stringid" />
                    			<input type="hidden" name="type" class="required" value="header-top" />
                    		</div>
                    	</div>
                    	<div class="form-group form-group-url">
                    		<label class="col-md-3 control-label"><?= get_string('family_name', $pluginname)?></label>
                    		<div class="col-md-9">
                    			
                    			<?php 
                    			if ($languages) {
                    			    $requiredLang = Common::getRequiredLanguage();
                    			    // 언어팩에 대한 예외처리 없음.
                    			    foreach ($languages as $ls) {
                    			        $requireClass = ($ls == $requiredLang) ? 'required' : '';
                    			        
                    			        echo '<div class="form-group">';
                    			        echo     '<label class="col-sm-3 control-label">'.$installLangs[$ls].'</label>';
                    			        echo     '<div class="col-sm-9">';
                    			        echo         '<input type="text" name="lang-'.$ls.'" class="'.$requireClass.' form-control" />';
                    			        echo     '</div>';
                    			        echo '</div>';
                    			    }
                    			}
                    			?>
                    		</div>
                    	</div>
                    	
                    	<div class="form-group form-group-url">
                    		<label class="col-md-3 control-label">icon</label>
                    		<div class="col-md-9">
                    			<div class="form-inline">
                        			<input type="text" name="icon" class="" placeholder="icon" />
                    			</div>
                    		</div>
                    	</div>
                    	
                    	<div class="form-group form-group-url">
                    		<label class="col-md-3 control-label">URL</label>
                    		<div class="col-md-9">
                    			<div class="form-inline">
                        			<input type="text" name="url" class="required w-75" placeholder="URL" />
                    			</div>
                    		</div>
                    	</div>
                    
                    	<div class="form-group">
                    		<div class="col-md-offset-3 col-md-9">
                    			<button type="submit" class="btn btn-primary"><?= get_string('save', 'local_ubion'); ?></button>
                    			<button type="button" class="btn btn-default" data-dismiss="modal"><?= get_string('close', 'local_ubion'); ?></button>
                    		</div>
                    	</div>
                    </form>
    			</div>
    		</div><!-- /.modal-content -->
    	</div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<?php 
echo $OUTPUT->footer();