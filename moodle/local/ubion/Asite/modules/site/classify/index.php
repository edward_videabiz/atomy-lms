<?php
use \local_ubion\base\Common;
use \local_ubion\base\Parameter;
use \local_ubion\base\Paging;

$pluginname = 'local_ubion';

$coursetype = optional_param('course_type', null, PARAM_NOTAGS);
$page = optional_param('page', 1, PARAM_INT);
$ls = optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);

$actionURL = $CAsite->sGetUrl('', 'mod', 'site/classify', 'act', 'action_');

// 현재 설치되어 있는 언어팩 목록 가져오기
$installLangs = Common::getInstallLangs();
$languages = Common::getLangs($installLangs);

$CAClassify = \local_ubion\asite\course\Classify::getInstance();

$courseTypes = $CAClassify->getCourseTypes();

$CAClassify->checkDefaultClassify($courseTypes);

$i8n = new \stdClass();
$i8n->use = get_string('use', 'local_ubion');
$i8n->not_use = get_string('not_use', 'local_ubion');
$i8n->modify = get_string('modify', 'local_ubion');
$i8n->delete = get_string('delete');

$PAGE->requires->js_call_amd('local_ubion/asiteClassify', 'index', array(
    'url' => $actionURL
));

$PAGE->requires->strings_for_js(array(
    'classify_delete_confirm'
    ,'classify_delete_confirm_multiple'
), $pluginname);


$colspan = 5;
$isClassifySelect = false;
$paging = null;
if (empty($coursetype)) {
    list($totalCount, $classifies) = $CAClassify->getClassifiesPaging($page, $ls);
    $paging = Paging::getPaging($totalCount, $page, $ls);
    $paging = $CAsite->sPaging($paging);
} else {
    // 정렬 기능이 활성화되어야 하기 때문에 colspan 증가
    $colspan = 6;
    $isClassifySelect = true;
    // 강좌 타입이 선택되어 있으면 선택된 강좌 타입에 등록된 모든 분류가 표시되어야 함.
    $classifies = $CAClassify->getClassifiesAll($coursetype);
    $totalCount = count($classifies);
}

echo $OUTPUT->header();

?>
<div class="asite-course asite-course-irregular">
	
	<form class="form-horizontal form-search mb-4 form-validate well">
		<div class="form-group">
        	<label class="control-label col-sm-3">강좌 타입</label>
        	<div class="col-sm-9">
				<select name="course_type" class="form-control form-control-coursetype">
					<option value="">전체</option>
					<?php
					foreach ($courseTypes as $ctKey => $ctText) {
					    $selected = ($coursetype == $ctKey) ? ' selected="selected"' : '';
					    
					    echo '<option value="'.$ctKey.'" '.$selected.'>'.$ctText.'</option>';
					}
					?>
				</select>
				<div class="form-text text-info">강좌 타입 선택시 분류 정렬 기능을 사용하실수 있습니다.</div>
        	</div>
        </div>
		<?php 
        if (!$isClassifySelect) {
        ?>
		<div class="form-group">
        	<label class="control-label col-sm-3">화면 표시 갯수</label>
        	<div class="col-sm-9">
        		
				<select name="ls" class="form-control form-control-ls">
					<?php
					if ($listSizes = Parameter::getListSizes()) {
					    foreach ($listSizes as $lsKey => $lsValue) {
					        $selected = ($lsKey == $ls) ? ' selected="selected"' : '';
					        echo '<option value="'.$lsKey.'" '.$selected.'>'.$lsValue.'</option>';
					    }
					}
					?>
				</select>
				
        	</div>
        </div>
    	<?php 
    	}
		?>
	</form>
	
	
	
	<form method="post" class="form-classify" action="<?= $actionURL; ?>">
    	<input type="hidden" name="<?php echo $CAClassify::TYPENAME; ?>" value="checkDelete" />
    	<input type="hidden" name="sesskey" value="<?php echo sesskey(); ?>" />
    	
        <table class="table table-bordered table-coursemos table-classify coursemos-sortable">
        	<colgroup>
        		<?php 
        		if ($isClassifySelect) {
        		    echo '<col class="wp-40" />';
        		}
        		?>
        		<col class="wp-50" />
        		<col class="wp-120" />
        		<col />
        		<col class="wp-80" />
        		<col class="wp-150" />
        	</colgroup>
        	<thead>
        		<tr>
        			<?php 
            		if ($isClassifySelect) {
            		    echo '<th>&nbsp;</th>';
            		}
            		?>
        			<th><input type="checkbox" class="all_checked" /></th>
        			<th>강좌 타입</th>
        			<th>분류명</th>
        			<th>사용여부</th>
        			<th>관리</th>
        		</tr>
        	</thead>
        	<tbody>
        		<?php 
        		
        		
        		if ($totalCount > 0) {
        		    $defaultLang = Common::getRequiredLanguage();
        		    
        		    foreach ($classifies as $cs) {
        		        $langSerialize = unserialize($cs->value);
        		        
        		        // 한글이 지정안되어 있으면 영어로 표시
        		        $name = Common::getUnserializeName($langSerialize);
        		        
        		        $visibleString = '<label class="label label-info">사용</label>';
        		        if (empty($cs->visible)) {
        		            $visibleString = '<label class="label label-danger">미사용</label>';
        		        }
        		        
        		        echo '<tr data-id="'.$cs->id.'">';
        		        if ($isClassifySelect) {
        		          echo    '<td class="text-center"><i class="fa fa-arrows moveicon"></i></td>';
        		        }
        		        echo      '<td class="text-center">';
        		        echo          '<input type="checkbox" class="form-control-clsf" name="clsf[]" value="'.$cs->id.'" />';
        		        echo      '</td>';
        		        echo      '<td class="text-center">';
        		        echo          $courseTypes[$cs->course_type] ?? '&nbsp;';
                        echo      '</td>';
        		        echo      '<td>';
        		        echo          '<span class="classify-name">'.$name.'</span>';
        		        echo      '</td>';
        		        
        		        echo      '<td class="text-center">';
        		        echo          $visibleString;
        		        echo      '</td>';
        		        echo      '<td class="text-center">';
        		        echo          '<button type="button" class="btn btn-sm btn-default btn-update mr-2">수정</button>';
        		        
        		        // 기본 분류에 대해서는 삭제 버튼이 표시되면 안됨.
        		        if (empty($cs->basic)) {
        		          echo          '<button type="button" class="btn btn-sm btn-danger btn-delete">삭제</button>';
        		        }
        		        echo      '</td>';
        		        echo '</tr>';
        		    }
        		} else {
        		    echo '<tr><td colspan="'.$colspan.'">등록된 분류 항목이 없습니다.</td></tr>';
        		}
        		?>
        	</tbody>	
        	<tfoot>
        		<tr>
        			<td colspan="<?= $colspan ?>"><?php print_string('selected_item', $pluginname); ?> <button type="button" class="btn btn-danger btn-checked-delete btn-sm"><?= $i8n->delete; ?></button></td>
        		</tr>
        	</tfoot>
        </table>
        <?php 
        echo $paging;
        ?>
    </form>
    
    <div class="text-right">
    	<button type="button" class="btn btn-primary btn-add-classify">분류 추가</button>
    </div>
    
    <div class="modal" tabindex="-1" role="dialog" id="modal-classify">
    	<div class="modal-dialog modal-lg" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    				<h5 class="modal-title">분류 추가</h5>    				
    			</div>
    			<div class="modal-body">
    				<form method="post" class="form-modal-classify form-horizontal form-validate" action="<?= $actionURL; ?>">
    					<div class="form-group row">
    						<label class="control-label col-md-3">강좌 타입</label>
    						<div class="col-md-9">
    							<select name="course_type" class="form-control required">
    								<?php 
    								foreach ($courseTypes as $ctKey => $ctText) {
    								    $selected = ($coursetype == $ctKey) ? ' selected="selected"' : '';
    								    
    								    echo '<option value="'.$ctKey.'" '.$selected.'>'.$ctText.'</option>';
    								}
    								?>
    							</select>
    							<div class="form-text text-danger">
    								분류 수정시 강좌 분류는 수정이 불가능 하오니 신중히 선택해주시기 바랍니다.
    							</div>			
    						</div>
    					</div>
    					<div class="form-group row">
    						<label class="control-label col-md-3">분류명</label>
    						<div class="col-md-9">
    							<input type="hidden" name="<?= $CAClassify::TYPENAME; ?>" value="insert" />
    							<input type="hidden" name="sesskey" value="<?= sesskey(); ?>" />
    							<input type="hidden" name="id" value="" />
    							
    							<?php 
                    			if ($languages) {
                    			    $requiredLang = Common::getRequiredLanguage();
                    			    
                    			    // 언어팩에 대한 예외처리 없음.
                    			    foreach ($languages as $ls) {
                    			        $requireClass = ($ls == $requiredLang) ? 'required' : '';
                    			        
                    			        echo '<div class="form-group">';
                    			        echo     '<label class="col-sm-3 control-label">'.$installLangs[$ls].'</label>';
                    			        echo     '<div class="col-sm-9">';
                    			        echo         '<input type="text" name="lang-'.$ls.'" class="'.$requireClass.' form-control" />';
                    			        echo     '</div>';
                    			        echo '</div>';
                    			    }
                    			}
                    			?>
    						</div>
    					</div>
    					<div class="form-group row">
    						<label class="control-label col-md-3">사용여부</label>
    						<div class="col-md-9">	
    							<label class="radio-inline">
    								<input type="radio" name="visible" value="1" checked="checked" class="form-check-input" /> <?php echo $i8n->use; ?>
    							</label>
							
    							<label class="radio-inline">
    								<input type="radio" name="visible" value="0" class="form-check-input" /> <?php echo $i8n->not_use; ?>
    							</label>
    						</div>
    					</div>
    					<div class="form-group row">
    						<div class="col-md-9 col-md-offset-3">
    							<button type="submit" class="btn btn-primary"><?php print_string('save', 'local_ubion'); ?></button>
                				<button type="button" class="btn btn-default" data-dismiss="modal"><?php print_string('close', 'local_ubion'); ?></button>			
    						</div>
    					</div>
    				</form>
    			</div>
    		</div><!-- /.modal-content -->
    	</div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<?php 
echo $OUTPUT->footer();