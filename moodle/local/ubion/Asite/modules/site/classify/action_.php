<?php
$controllder = \local_ubion\asite\course\Classify::getInstance();

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}