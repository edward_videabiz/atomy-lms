<?php

$PAGE->requires->js_call_amd('local_ubion/asiteMember', 'user_batch');

echo $OUTPUT->header();

?>
<div class="asite-member">
	<form id="add_form" action="<?php echo $CAsite->sGetUrl('act', 'delete_view'); ?>" method="post" class="form-horizontal form-validate" enctype="multipart/form-data">
		<div class="form-group">
			<label class="control-label col-sm-3">양식 다운로드</label>
			<div class="col-sm-9">
				<a href="delete.xlsx" class="btn btn-success">다운로드</a>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
    			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                	<div class="panel panel-default">
                		<div class="panel-heading" role="tab" id="heading1">
                			<h4 class="panel-title">
                				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseRequired" aria-expanded="true" aria-controls="collapseRequired">
                					<span class="text-danger">필수항목</span>
                				</a>
                			</h4>
                		</div>
                		<div id="collapseRequired" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                			<div class="panel-body">
                				<table class="table table-bordered table-coursemos">
                					<colgroup>
                						<col class="wp-150" />
                						<col />
                					</colgroup>
                					<thead>
                						<tr>
                							<th>컬럼명</th>
                							<th>설명</th>
                						</tr>
                					</thead>
                					<tbody>
                						<tr>
                							<td class="text-right">username</td>
                							<td>사용자 아이디</td>
                						</tr>
                						<tr>
                							<td class="text-right">idnumber</td>
                							<td>사용자 학번</td>
                						</tr>
                						<tr>
                							<td class="text-right">firstname</td>
                							<td>한글명</td>
                						</tr>
                					</tbody>
                					<tfoot>
                						<tr>
                							<td colspan="2">
                								<span class="text-danger">username, idnumber, firstname 3개 항목이 일치하는 사용자가 존재할 경우에만 삭제됩니다.</span>
                							</td>
                						</tr>
                					</tfoot>
                				</table>
                			</div>
                		</div>
                	</div> 	<!-- panel end -->
                </div>
    		</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3">첨부파일</label>
			<div class="col-sm-9">
				<input type="file" name="uploaduser" class="form-control required" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-9 col-sm-offset-3">
				<button type="submit" class="btn btn-primary btn-submit">저장</button>
			</div>
		</div>
	</form>
	
	
</div>
<?php 
echo $OUTPUT->footer();