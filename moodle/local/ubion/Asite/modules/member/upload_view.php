<?php
use \local_ubion\base\Javascript;
use \local_ubion\base\Parameter;

require_once $CFG->libdir.'/phpexcel/PHPExcel.php';
require_once $CFG->libdir.'/csvlib.class.php';
require_once $CFG->dirroot.'/admin/tool/uploaduser/locallib.php';
require_once $CFG->dirroot.'/admin/tool/uploaduser/user_form.php';

$CMember = \local_ubion\asite\member\Member::getInstance();

$iid = Parameter::post('iid', null, PARAM_INT);
$returnUrl = $CAsite->sGetUrl('act', 'upload');

// 첨부파일이 전달되었는지 확인
if (!isset($_FILES['uploaduser']) && empty($iid)) {
    Javascript::getAlertMove('첨부파일이 존재하지 않습니다.', $CAsite->sGetUrl('act', 'upload'), true);
} else {
    
    
    // 저장경로
    $savePath = $CFG->tempdir.'/uploaduser';
    
    // 저장 경로가 없으면 생성해줘야됨.
    if (!is_dir($savePath)) {
        mkdir($savePath);
    }
    
    // iid는 저장된 파일명임
    // 해당 값이 없으면 해당 페이지를 처음 접근 한것이기 때문에 파일 저장 로직을 실행시켜야됨.
    if (empty($iid)) {
        // 첨부파일 확장자 검사.
        $extentions = array('xls', 'xlsx');
        
        $path = pathinfo($_FILES['uploaduser']['name']);
        $ext = strtolower($path['extension']);
        
        if (!in_array($ext, $extentions)) {
            Javascript::getAlert('허용되지 않는 확장자입니다. xls, xlsx만 업로드 가능합니다.');
        }
        
        $iid = csv_import_reader::get_new_iid('ubion_uploaduser');
        $filename = $savePath.'/'.$iid;
        // 일회성 파일이기 때문에 파일 이름 중복 검사는 딱히 안해줘도 상관 없음.
        move_uploaded_file($_FILES['uploaduser']['tmp_name'], $filename);
    } else {
        $filename = $savePath.'/'.$iid;
    }
    
    
    $stdfield = $CMember->getUploadAllowColumns();
    $stdfield = array_merge($stdfield, get_all_user_name_fields(), $CMember->getUploadAllowColumnsUbion());
    
    
    $excel = PHPExcel_IOFactory::load($filename);
    $excelSheet = $excel->getActiveSheet()->toArray(null,false,false,false);
    
    // 사용할수 있는 컬럼 목록 가져오기
    $fileColumns = $CMember->getUploadColumnValidate($excelSheet[0], $stdfield, $returnUrl);
    
    // 사용자 일괄 업로드 미리 보기 화면에서 사용자 올리기 기본 옵션값을 바꾸고 싶은 경우 formData 배열에 key => value 형태로 입력하시면 됩니다.
    // key값은 /admin/tool/uploaduser/user_form.php 파일 참고
    $formData = array(
        'iid' => $iid
        ,'previewrows' => 100                           // 미리 보기 갯수는 이 페이지에서는 무의미한 값임
        ,'uutype' =>  UU_USER_ADD_UPDATE                // 올리기 형식                   => 새 사용자 추가 및 기존 사용자 업데이트를 기본값으로 설정
        ,'uuupdatetype' => UU_UPDATE_FILEOVERRIDE       // 기존 사용자 세부사항          => 파일로 덮어씌우기
        ,'uuallowsuspends' => 0                         // 계정 활성화 및 사용정지 허용  => 아니오
        ,'uustandardusernames' => 0                     // 사용자이름 표준화             => 아니오
    );
    $mform2 = new admin_uploaduser_form2(null, array('columns'=>$fileColumns, 'data' => $formData));
    
    if ($formdata = $mform2->get_data()) {
        $CMember->setUploadUser($formdata, $filename, $fileColumns, $returnUrl);
        die;
    }
    
    
    echo $OUTPUT->header();
    echo '<div class="asite-member">';
?>  
	<div class="table-responsive">
    	<table class="table table-bordered table-coursemos">
    		<thead>
    			<tr>
    				<th>번호</th>
    			<?php 
    			foreach ($fileColumns as $fs) {
                    echo '<th>'.$fs.'</th>';   
    			}
    			echo '<th>status</th>';
    			?>
    			</tr>
    		</thead>
    		<tbody>
<?php     
    $stremailduplicate = get_string('useremailduplicate', 'error');
    $number = 1;
    
    // 데이터 재구성
    // /admin/tool/uploaduser/index.php 파일 내용 발췌
    foreach ($excelSheet as $index=>$excelRow) {
        if ($index > 0) {
            $excelDataRow = array();
            foreach ($excelRow as $cellIndex => $cellValue) {
                // filecolumns에 존재하는 컬럼만 추가해줘야됨.
                if (array_key_exists($cellIndex, $fileColumns)) {
                    $columnName = $fileColumns[$cellIndex];
                    $excelDataRow[$columnName] = $cellValue;
                }
            }
            
            $excelDataRowStatus = array();
            if (isset($excelDataRow['username'])) {
                //$stdUserName = clean_param($excelDataRow['username'], PARAM_USERNAME);
                // 2013-01-23 USERNAME에서 strtolower하기 때문에 대소문자가 안맞아서 오류가 발생함..
                $stdUserName = clean_param($excelDataRow['username'], PARAM_NOTAGS);
                if ($excelDataRow['username'] !== $stdUserName) {
                    $excelDataRowStatus[] = get_string('invalidusernameupload');
                }
                if ($userid = $DB->get_field_sql('SELECT id FROM {user} WHERE username = :username AND mnethostid = :mnethostid', array('username'=>$stdUserName, 'mnethostid'=>$CFG->mnet_localhost_id))) {
                    $excelDataRowStatus[] = html_writer::link(new moodle_url('/user/profile.php', array('id'=>$userid)), $excelDataRow['username']);
                }
            } else {
                $excelDataRowStatus[] = get_string('missingusername');
            }
            
            
            if (isset($excelDataRow['email'])) {
                // 이메일을 알지 못하는 사용자를 대비해서 @로 입력된건 더이상 검사하지 않음.
                if ($excelDataRow['email'] != '@') {
                    if (!validate_email($excelDataRow['email'])) {
                        $excelDataRowStatus[] = get_string('invalidemail');
                    }
                    
                    $select = $DB->sql_like('email', ':email', false, true, false, '|');
                    $params = array('email' => $DB->sql_like_escape($excelDataRow['email'], '|'));
                    if ($DB->record_exists_select('user', $select , $params)) {
                        $excelDataRowStatus[] = $stremailduplicate;
                    }
                }
            }
            
            echo '<tr>';
            echo    '<td class="text-center">'.$number.'</td>';
            foreach ($excelDataRow as $columnName => $columnValue) {
                
                echo '<td>'.$columnValue.'</td>';
            }
            
            echo    '<td>'.implode('<br/>', $excelDataRowStatus).'</td>';
            echo '</tr>';
            
            $number++;
        }
    }
    
?>
			</tbody>
		</table>
	</div>
<?php

    $mform2->display();
    echo '</div>';
    echo $OUTPUT->footer();
}