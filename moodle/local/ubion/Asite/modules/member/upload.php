<?php
$PAGE->requires->js_call_amd('local_ubion/asiteMember', 'user_batch');

echo $OUTPUT->header();

$CMember = \local_ubion\asite\member\Member::getInstance();
$uploadUbionColumnPrevix = $CMember->getUploadUbionColumnPrefix(); 
?>
<div class="asite-member">
	<form id="add_form" action="<?php echo $CAsite->sGetUrl('act', 'upload_view'); ?>" method="post" class="form-horizontal form-validate" enctype="multipart/form-data">
		<div class="form-group">
			<label class="control-label col-sm-3">양식 다운로드</label>
			<div class="col-sm-9">
				<!--  <a href="<?= $CFG->wwwroot.'/local/ubion/Asite/modules/member/user_register.xlsx'; ?>" class="btn btn-success">다운로드</a> -->
				<a href="user_register.xlsx" class="btn btn-success">다운로드</a>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
    			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                	<div class="panel panel-default">
                		<div class="panel-heading" role="tab" id="heading1">
                			<h4 class="panel-title">
                				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseRequired" aria-expanded="true" aria-controls="collapseRequired">
                					<span class="text-danger">필수항목</span>
                				</a>
                			</h4>
                		</div>
                		<div id="collapseRequired" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                			<div class="panel-body">
                				<table class="table table-bordered table-coursemos">
                					<colgroup>
                						<col class="wp-150" />
                						<col />
                					</colgroup>
                					<thead>
                						<tr>
                							<th>컬럼명</th>
                							<th>설명</th>
                						</tr>
                					</thead>
                					<tbody>
                						<tr>
                							<td class="text-right">username</td>
                							<td>사용자 아이디</td>
                						</tr>
                						<tr>
                							<td class="text-right">password</td>
                							<td>사용자 비밀번호</td>
                						</tr>
                						<tr>
                							<td class="text-right">idnumber</td>
                							<td>학번 - 특별한 경우가 아니라면 username과 동일하게 입력해주시기 바랍니다. </td>
                						</tr>
                						<tr>
                							<td class="text-right">firstname</td>
                							<td>한글명</td>
                						</tr>
                						<tr>
                							<td class="text-right">lastname</td>
                							<td>영문명</td>
                						</tr>
                						<tr>
                							<td class="text-right">email</td>
                							<td>
                								이메일은 중복되지 않는 값을 입력하셔야 합니다.<br/>
                								만약 이메일을 알수 없는 사용자인 경우에는 @만 입력해주시면 됩니다.
                							</td>
                						</tr>
                					</tbody>
                				</table>
                			</div>
                		</div>
                	</div> 	<!-- panel end -->
                	
                	<div class="panel panel-default">
                		<div class="panel-heading" role="tab" id="heading2">
                			<h4 class="panel-title">
                				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOption" aria-expanded="true" aria-controls="collapseOption">
                					선택 사항
                				</a>
                			</h4>
                		</div>
                		<div id="collapseOption" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                			<div class="panel-body">
                				<table class="table table-bordered table-coursemos">
                					<colgroup>
                						<col class="wp-150" />
                						<col />
                					</colgroup>
                					<thead>
                						<tr>
                							<th>컬럼명</th>
                							<th>설명</th>
                						</tr>
                					</thead>
                					<tbody>
                						<tr>
                							<td class="text-right">auth</td>
                							<td>
                								<p>사용자 인증 타입</p>
                								<dl class="dl-horizontal dl-horizontal-sm">
                									<dt>manual</dt>
                									<dd>기본 인증</dd>
                									<dt>univ</dt>
                									<dd>학내 구성원 (SSO 사용자)</dd>
                								</dl>
                							</td>
                						</tr>
                						<tr>
                							<td class="text-right">phone1</td>
                							<td>사무실 연락처</td>
                						</tr>
                						<tr>
                							<td class="text-right">phon2</td>
                							<td>핸드폰 번호</td>
                						</tr>
                						<tr>
                							<td class="text-right">institution</td>
                							<td>상위 부서명</td>
                						</tr>
                						<tr>
                							<td class="text-right">department</td>
                							<td>하위 부서명</td>
                						</tr>
                						<tr>
                							<td class="text-right">suspended</td>
                							<td>
                								<p>유보된 계정 여부</p>
                								<dl class="dl-horizontal dl-horizontal-sm">
                									<dt>0</dt>
                									<dd>기본</dd>
                									<dt>1</dt>
                									<dd>유보된 계정 (로그인 불가)</dd>
                								</dl>
                							</td>
                						</tr>
                					</tbody>
                					<tfoot>
                						<tr>
                							<td colspan="2">
                								더 많은 값들은 user 테이블을 참고하시면 됩니다.
                							</td>
                						</tr>
                					</tfoot>
                				</table>
                			</div>
                		</div>
                	</div>	<!-- panel end -->
                	
                	<div class="panel panel-default">
                		<div class="panel-heading" role="tab" id="heading2">
                			<h4 class="panel-title">
                				<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseUserUbion" aria-expanded="true" aria-controls="collapseUserUbion">
                					추가 입력 항목 (userubion)
                				</a>
                			</h4>
                		</div>
                		<div id="collapseUserUbion" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                			<div class="panel-body">
                				<table class="table table-bordered table-coursemos">
                					<colgroup>
                						<col class="wp-150" />
                						<col />
                					</colgroup>
                					<thead>
                						<tr>
                							<th>컬럼명</th>
                							<th>설명</th>
                						</tr>
                					</thead>
                					<tbody>
                						<tr>
                							<td class="text-right"><strong class="text-primary"><?= $uploadUbionColumnPrevix; ?></strong>user_type</td>
                							<td>
                								<p>코스모스 사용자 구분</p>
                								
                								예 : 
                								<dl class="dl-horizontal dl-horizontal-sm">
                									<?php 
                									$userTypes = \local_ubion\user\User::getInstance()->getUserTypes();
                									
                									foreach ($userTypes as $uKey => $uValue) {
                									    echo '<dt>'.$uKey.'</dt>';
                									    echo '<dd>'.$uValue.'</dd>';
                									}
                									?>
                								</dl>
                								<span class="text-info">해당 값은 코스모스에서 사용되는 사용자 구분값이며, 학교에서 사용되는 사용자 구분과는 다름을 알려드립니다.</span>
                							</td>
                						</tr>
                						<tr>
                							<td class="text-right"><strong class="text-primary"><?= $uploadUbionColumnPrevix; ?></strong>user_type_name</td>
                							<td>
                								<p>코스모스 사용자 구분명</p>
                								<span class="text-info">위 user_ubion-user_type 참고</span>
                							</td>
                						</tr>
                						<tr>
                							<td class="text-right"><strong class="text-primary"><?= $uploadUbionColumnPrevix; ?></strong>institution_code</td>
                							<td>상위 부서 코드</td>
                						</tr>
                						<tr>
                							<td class="text-right"><strong class="text-primary"><?= $uploadUbionColumnPrevix; ?></strong>dept_code</td>
                							<td>하위 부서 코드</td>
                						</tr>
                						
                					</tbody>
                					<tfoot>
                						<tr>
                							<td colspan="2">
                								<p><span class="text-danger">컬럼명앞에 반드시 <strong class="text-primary"><?= $uploadUbionColumnPrevix; ?></strong>를 붙여주셔야 합니다.</span></p>
                								더 많은 값들은 user_ubion 테이블을 참고하시면 됩니다.
                							</td>
                						</tr>
                					</tfoot>
                				</table>
                			</div>
                		</div>
                	</div> 	<!-- panel end -->
                </div>
    		</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3">첨부파일</label>
			<div class="col-sm-9">
				<input type="file" name="uploaduser" class="form-control required" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-9 col-sm-offset-3">
				<button type="submit" class="btn btn-primary">저장</button>
			</div>
		</div>
	</form>
	
	
</div>
<?php 
echo $OUTPUT->footer();