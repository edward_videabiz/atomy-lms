<?php


$controllder = \local_ubion\asite\member\Member::getInstance();

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}