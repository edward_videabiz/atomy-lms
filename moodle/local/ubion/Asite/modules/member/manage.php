<?php
use \local_ubion\base\Parameter;
use \local_ubion\base\Paging;

$i8n = new stdClass();
$i8n->edit = get_string('edit');
$i8n->delete = get_string('delete');
$i8n->logs = get_string('logs');
$i8n->userdetails = get_string('userdetails');
$i8n->authmethod = get_string('authmethod', 'local_ubion');
$i8n->admin_add = get_string('admin_add', 'local_ubion');
$i8n->admin_delete = get_string('admin_delete', 'local_ubion');

$CUser = \local_ubion\user\User::getInstance();
$CMember = \local_ubion\asite\member\Member::getInstance();

// post될 주소
$actionURL = $CAsite->sGetUrl('act', 'action_');
// echo $actionURL;die;
$returnURL = base64_encode($SITECFG->nowUrl);

$keyfield = optional_param('keyfield', null, PARAM_NOTAGS);
$keyword = Parameter::getKeyword();
$page = optional_param('page', 1, PARAM_INT);
$ls = optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);

 
list ($totalCount, $members) = $CMember->getAdmins($keyfield, $keyword, $page, $ls);
$paging = Paging::getPaging($totalCount, $page, $ls);

$PAGE->requires->js_call_amd('local_ubion/asiteMember', 'admin', array(
    'url' => $actionURL
));

$PAGE->requires->strings_for_js(array(
    'add',
    'delete'
), 'core');

$PAGE->requires->strings_for_js(array(
    'admin_confirm',
    'admin_delete_confirm'
), 'local_ubion');

echo $OUTPUT->header();

?>
<div class="asite-member">
	<form class="form-horizontal form-search mb-4 form-validate well">
		<div class="form-group">
        	<label class="control-label col-sm-3"><?=get_string('listsize', 'local_ubion');?></label>
        	<div class="col-sm-9">
				<select name="ls" class="form-control">
					<?php
					if ($listSizes = Parameter::getListSizes()) {
					    foreach ($listSizes as $lsKey => $lsValue) {
					        $selected = ($lsKey == $ls) ? ' selected="selected"' : '';
					        echo '<option value="'.$lsKey.'" '.$selected.'>'.$lsValue.'</option>';
					    }
					}
					?>
				</select>
        	</div>
        </div>
        
        <div class="form-group">
        	<label class="control-label col-sm-3"><?= get_string('search'); ?></label>
        	<div class="col-sm-9">
        		<div class="form-inline">
        			<?php 
        			$keyfields = array(    
                         'memberkey' => get_string('username')
                        ,'fullname' => get_string('fullnameuser')
        			);
        			
        			echo '<select name="keyfield" class="form-control">';
        			foreach ($keyfields as $key => $value) {
        			    $selected = ($keyfield == $key) ? ' selected="selected"' : '';
        			    echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';    
        			}
        			echo '</select>';
        			echo '<input type="text" class="form-control" name="keyword" value="'.$keyword.'" placeholder="'.get_string('keyword', 'local_ubion').'" />';	
        			?>
				</div>
        	</div>
        </div>
        <div class="form-group">
        	<div class="col-sm-9 col-sm-offset-3">
        		<input type="submit" class="btn btn-success" value="<?= get_string('search'); ?>" />
        		
        		<?php 
        		if (!empty($keyword)) {
        		    echo '<a href="'.$CAsite->sGetUrl('', 'mod', 'member', 'act', 'manage').'" class="btn btn-default">'.get_string('search_cancel', 'local_ubion').'</a>';
        		}
        		?>
        	</div>
        </div>
	</form>
	
	<div class="form-result">
		<?php
		if ($totalCount > 0) {
            echo '<div class="paging-info small mb-3">';
            echo    get_string('total') . ' : <span class="text-info">' . number_format($totalCount) . '</span>,  <span class="ml-2">' . number_format($paging->currentPage) . ' / ' . number_format($paging->totalPage) . '</span>';
            echo '</div>';
		}
		?>
		<div class="table-responsive">
			<table class="table table-bordered table-coursemos">
    			<colgroup>
    				<col class="wp-50" />
    				<col class="wp-100" />
    				<col />
    				<col class="wp-130" />
    				<col class="wp-120" />
    				<col class="wp-180" />
    			</colgroup>
    			<thead>
    				<tr>
    					<th class="text-center"><?=get_string('number', 'local_ubion');?></th>
    					<th class="text-center"><?=get_string('username');?></th>
    					<th class="text-center"><?=get_string('fullnameuser');?></th>
    					<th class="text-center"><?=get_string('phone2');?></th>
    					<th class="text-center"><?=$i8n->userdetails;?></th>
    					<th class="text-center"><?=get_string('etc', 'local_ubion');?></th>
    				</tr>
    			</thead>
    			<tbody>
    				<?php 
    				if ($totalCount > 0) {
    				    $iconMarginClass = 'etc-icon-btn';
    				    
    				    $printNum = $totalCount - (($page - 1) * $ls);
    				    foreach ($members as $m) {
    				        $fullname = fullname($m);
    				        
    				        echo '<tr>';
    				        echo    '<td class="text-center">'.number_format($printNum).'</td>';
    				        echo	'<td class="text-center">'.$m->username.'</td>';
    				        echo	'<td>'.$fullname.'</td>';
    				        echo	'<td class="text-center">'.$m->phone2.'</td>';
    				        echo	'<td class="text-center">';
    				        echo        '<button class="btn btn-info btn-xs btn-user-default" data-id="'.$m->id.'">'.$i8n->userdetails.'</button>';
    				        echo    '</td>';
    				        echo	'<td class="text-center">';
    				        echo		'<a href="'.$CFG->wwwroot.'/user/editadvanced.php?id='.$m->id.'&amp;course=1'.'" target="_balnk" class="'.$iconMarginClass.'" title="'.$i8n->edit.'">';
    				        echo            '<i class="fa fa-pencil"></i>';
    				        echo		'</a>';
    				        
//    				        echo		'<a href="'.$CAsite->sGetUrl('', 'mod', 'member', 'act', 'ubion', 'id', $m->id, 'returnurl', $returnURL).'" target="_balnk" class="'.$iconMarginClass.'" title="user-ubion '.$i8n->edit.'">';
//    				        echo             '<i class="fa fa-edit"></i>';
//    				        echo		'</a>';
    				        
    				        echo		'<a href="'.$CFG->wwwroot.'/report/log/index.php?chooselog=1&user='.$m->id.'&date=&modid=&modaction=&logformat=showashtml" target="_blank" class="'.$iconMarginClass.'" title="'.$i8n->logs.'">';
    				        echo            '<i class="fa fa-history"></i>';
    				        echo		'</a>';
    				        
    				        if ($m->userid != 2) {
        				        echo	'<a href="#" class="'.$iconMarginClass.' btn-admin-delete" title="'.$i8n->admin_delete.'" data-fullname="'.$fullname.'" data-userid="'.$m->id.'">';
        				        echo        '<i class="fa fa-remove"></i>';
        				        echo	'</a>';
    				        }
    				        echo    '</td>';
    				        echo '</tr>';
    				        $printNum--;
    				    }
    				} else {
    				    echo '<tr><td colspan="6" class="p-4">'.get_string('not_found_users', 'local_ubion').'</td></tr>';
    				}
    				?>
    			</tbody>
    		</table>
		</div>
		
		<?php 
		echo $CAsite->sPaging($paging);
		?>
		
		<div class="text-right">
			<button class="btn btn-primary btn-admin-add"><?=$i8n->admin_add;?></button>
		</div>
		<div id="modal-user-detail" class="modal" tabindex="-1" role="dialog">
        	<div class="modal-dialog modal-lg" role="document">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h5 class="modal-title">[<span class="fullname"></span>]</h5>
        			</div>
        			<div class="modal-body">
        				
        			</div>
        			<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_string('close', 'local_ubion'); ?></button>
        			</div>
        		</div><!-- /.modal-content -->
        	</div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        <div id="modal-usersearch" class="modal" tabindex="-1" role="dialog">
        	<div class="modal-dialog modal-lg" role="document">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h5 class="modal-title"><?=$i8n->admin_add;?></h5>
        			</div>
        			<div class="modal-body" style="max-height: 800px">
						<p style="text-align: center; font-weight: bold">Search user and set as admin</p>
        				<form class="form-horizontal form-validate form-user-search well" method="post">
        					<div class="form-group">
                            	<label class="control-label col-sm-3"><?= get_string('search'); ?></label>
                            	<div class="col-sm-9">
                            		<div class="form-inline">
                            			<?php 
                            			$keyfields = array(    
                                             'memberkey' => get_string('username')
                                            ,'fullname' => get_string('fullnameuser')
                            			);
                            			
                            			echo '<select name="keyfield" class="form-control mb-2 mb-md-0">';
                            			foreach ($keyfields as $key => $value) {
                            			    echo '<option value="'.$key.'">'.$value.'</option>';    
                            			}
                            			echo '</select>';
                            			?>
                        				<input type="text" class="form-control mb-2 mb-md-0" name="keyword" value="" placeholder="<?= get_string('keyword', 'local_ubion')?>" minlength="2" />
                        				<input type="hidden" name="<?=$CMember::TYPENAME?>" value="userSearch" />
                        				<input type="hidden" name="sesskey" value="<?=sesskey()?>" />
                        				<input type="submit" class="btn btn-success" value="<?= get_string('search'); ?>" />
                    				</div>
                            	</div>
                            </div>
        				</form>
        				<div class="user-lists table-responsive d-none">
            				<table class="table table-bordered table-coursemos table-userlists">
            					<colgroup>
            						<col class="wp-150" />
            						<col />
            						<col class="wp-100" />
            					</colgroup>
            					<thead>
            						<tr>
            							<th><?=get_string('username')?></th>
            							<th><?=get_string('fullnameuser')?></th>
            							<th><?=get_string('etc', 'local_ubion')?></th>
            						</tr>
            					</thead>
            					<tbody>
            						
            					</tbody>
            				</table>
        				</div>
        				<div class="paging"></div>
						<p style="text-align: center; font-weight: bold">Or create new user and set as admin</p>
						<form class="form-validate form-add-user well" method="post">
							<div class="form-row col-md-12">
								<i>Fields marked with <span style="color: red">*</span> are required!!</i>
								<br/><br/>
							</div>
							
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="username">Username<span style="color: red">*</span></label>
                                    <input type="text" class="form-control" id="username" name="username">
                                </div>
								<div class="form-group col-md-6">
                                    <label for="phone">Phone number</label>
                                    <input type="text" class="form-control" id="phone" name="phone">
                                </div>
                            </div>
							<div class="form-row">
								<div class="form-group col-md-6">
                                    <label for="cf-pwd">Password<span style="color: red">*</span></label>
                                    <input type="password" class="form-control" id="pwd" name="password">
                                </div>
								<div class="form-group col-md-6">
                                    <label for="cf-pwd">Confirm password<span style="color: red">*</span></label>
                                    <input type="password" class="form-control" id="cf-pwd" name="cf-password">
                                </div>
							</div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="firstname">First name<span style="color: red">*</span></label>
                                    <input type="text" class="form-control" name="firstname" id="firstname">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="lastname">Last name<span style="color: red">*</span></label>
                                    <input type="text" class="form-control" name="lastname" id="lastname">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">Email<span style="color: red">*</span></label>
                                    <input type="email" class="form-control" name="email" id="email">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="working-part">Working part</label>
                                    <input type="text" name="working-part" class="form-control" id="working-part">
                                </div>
                            </div>
                            <input type="hidden" name="<?=$CMember::TYPENAME?>" value="addUser" />
                            <input type="hidden" name="sesskey" value="<?=sesskey()?>" />
                            <button type="submit" class="btn btn-success ml-3">Add user</button>
                        </form>
        			</div>
        			<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_string('close', 'local_ubion'); ?></button>
        			</div>
        		</div><!-- /.modal-content -->
        	</div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
	</div>
</div>

<?php 
echo $OUTPUT->footer();	