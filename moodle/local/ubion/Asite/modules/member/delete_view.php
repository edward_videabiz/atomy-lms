<?php
use \local_ubion\base\Javascript;
use \local_ubion\base\Parameter;

require_once $CFG->libdir.'/phpexcel/PHPExcel.php';
require_once $CFG->libdir.'/csvlib.class.php';

$iid = Parameter::post('iid', null, PARAM_INT);
$returnUrl = $CAsite->sGetUrl('act', 'delete');

// 첨부파일이 전달되었는지 확인
if (!isset($_FILES['uploaduser']) && empty($iid)) {
    Javascript::getAlertMove('첨부파일이 존재하지 않습니다.', $CAsite->sGetUrl('act', 'upload'), true);
} else {
    
    
    // 저장경로
    $savePath = $CFG->tempdir.'/deleteuser';
    
    // 저장 경로가 없으면 생성해줘야됨.
    if (!is_dir($savePath)) {
        mkdir($savePath);
    }
    
    // iid는 저장된 파일명임
    // 해당 값이 없으면 해당 페이지를 처음 접근 한것이기 때문에 파일 저장 로직을 실행시켜야됨.
    if (empty($iid)) {
        // 첨부파일 확장자 검사.
        $extentions = array('xls', 'xlsx');
        
        $path = pathinfo($_FILES['uploaduser']['name']);
        $ext = strtolower($path['extension']);
        
        if (!in_array($ext, $extentions)) {
            Javascript::getAlert('허용되지 않는 확장자입니다. xls, xlsx만 업로드 가능합니다.');
        }
        
        $iid = csv_import_reader::get_new_iid('ubion_deleteuser');
        $filename = $savePath.'/'.$iid;
        // 일회성 파일이기 때문에 파일 이름 중복 검사는 딱히 안해줘도 상관 없음.
        move_uploaded_file($_FILES['uploaduser']['tmp_name'], $filename);
    } else {
        $filename = $savePath.'/'.$iid;
    }
    
    
    $fileColumns = array('username', 'idnumber', 'firstname');
    
    $excel = PHPExcel_IOFactory::load($filename);
    $excelSheet = $excel->getActiveSheet()->toArray(null,false,false,false);
    
    echo $OUTPUT->header();
    echo '<div class="asite-member">';
?>
	<div class="table-responsive">
    	<table class="table table-bordered table-coursemos">
    		<thead>
    			<tr>
    				<th>번호</th>
    			<?php 
    			foreach ($fileColumns as $fs) {
                    echo '<th>'.$fs.'</th>';   
    			}
    			echo '<th>status</th>';
    			?>
    			</tr>
    		</thead>
    		<tbody>
    		<?php
    		$number = 1;
    		$query = "SELECT * FROM {user} WHERE mnethostid = :mnethostid AND username = :username AND idnumber = :idnumber AND firstname = :firstname";
    		$param = array('mnethostid' => $CFG->mnet_localhost_id);
    		
    		foreach ($excelSheet as $index=>$excelRow) {
    		    if ($index > 0) {
    		        
    		        $excelDataRow = array();
    		        foreach ($excelRow as $cellIndex => $cellValue) {
    		            // filecolumns에 존재하는 컬럼만 추가해줘야됨.
    		            if (array_key_exists($cellIndex, $fileColumns)) {
    		                $columnName = $fileColumns[$cellIndex];
    		                $excelDataRow[$columnName] = trim($cellValue);
    		            }
    		        }
    		        
    		        
    		        $excelDataRowStatus = array();
    		        if (!isset($excelDataRow['username'])) {
    		            $excelDataRowStatus[] = 'username은 필수값입니다.';
    		        }
    		        
    		        if (!isset($excelDataRow['idnumber'])) {
    		            $excelDataRowStatus[] = 'idnumber은 필수값입니다.';
    		        }
    		        
    		        if (!isset($excelDataRow['firstname'])) {
    		            $excelDataRowStatus[] = 'firstname은 필수값입니다.';
    		        }
    		        
    		        // 3가지 입력항목이 모두 일치하는 경우 삭제 해야됨.
    		        // 단 idnumber는 빈값이 존재할수 있음 
    		        if (!empty($excelDataRow['username']) && !empty($excelDataRow['firstname'])) {
    		            $param['username'] = $excelDataRow['username'];
    		            $param['idnumber'] = $excelDataRow['idnumber'];
    		            $param['firstname'] = $excelDataRow['firstname'];
    		            
    		            if ($user = $DB->get_record_sql($query, $param)) {
    		                delete_user($user);
    		                
    		                // 혹시 몰라서 레코드는 삭제하지 않음
    		                // $DB->delete_records('user_ubion', array('userid' => $user->id));
    		                
    		                
    		                $excelDataRowStatus[] = $param['firstname'].' 삭제 완료';
    		            } else {
    		                $excelDataRowStatus[] = '<span class="text-danger">일치하는 사용자가 없습니다.</span>';
    		            }
    		        } else {
    		            $excelDataRowStatus[] = '<span class="text-danger">username 또는 idnumber는 반드시 입력하셔야 합니다. (빈값, 공백 불가)</span>';
    		        }
    		            
    		        
    		        
    		        echo '<tr>';
    		        echo    '<td class="text-center">'.$number.'</td>';
    		        foreach ($fileColumns as $columnName) {
    		            
    		            echo '<td class="text-center">';
    		            echo     $excelDataRow[$columnName] ?? '&nbsp;';
    		            echo '</td>';
    		        }
    		        
    		        echo    '<td>'.implode('<br/>', $excelDataRowStatus).'</td>';
    		        echo '</tr>';
    		        
    		        $number++;
    		        
    		    }
    		}
    		?>
    		</tbody>
    	</table>
    </div>
    
    <div class="text-center">
    	<a href="<?= $CAsite->sGetUrl('', 'mod', 'member', 'act', 'delete'); ?>" type="button" class="btn btn-default">확인</a>
    </div>
<?php
    echo '</div>';
    echo $OUTPUT->footer();
}