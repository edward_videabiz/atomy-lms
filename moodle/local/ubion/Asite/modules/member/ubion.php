<?php
$id = required_param('id', PARAM_INT);
$returnurl = optional_param('returnurl', null, PARAM_NOTAGS);
use \local_ubion\base\Javascript;
use \local_ubion\user\User;

$CMember = \local_ubion\asite\member\Member::getInstance();
$CUser = User::getInstance();

// 실제 존재하는 회원인지 확인
if ($user = $DB->get_record_sql("SELECT * FROM {user} WHERE id = :id", array('id' => $id))) {
    
    $userUbion = $DB->get_record_sql("SELECT * FROM {user_ubion} WHERE userid = :userid", array('userid' => $user->id));
    
    // 만약 userubion이 존재하지 않는다면 강제로 user_ubion구성
    // 단 user_type은 기타로 생성함.
    // 필요시 수정 페이지에서 수정하면됨.
    if (empty($userUbion)) {
        // 강제로 user_ubion 추가
        $CMember->setUserUbionDefaultInsert(null, $user->id);
        
        
        // 추가후 새로 데이터 불러옴
        $userUbion = $DB->get_record_sql("SELECT * FROM {user_ubion} WHERE userid = :userid", array('userid' => $user->id));
    }
    
    // 수정 불가 컬럼
    $hiddenColumns = $CMember->getDBHiddenColumns();
    
    echo $OUTPUT->header();
    ?>
    <div class="asite-member">
    
    	<form method="post" action="<?= $CAsite->sGetUrl('act', 'action_'); ?>" class="form-horizontal">
    		<?php 
    		foreach ($userUbion as $column => $value) {
    		    if (in_array($column, $hiddenColumns)) {
    		        continue;
    		    }
    		    echo '<div class="form-group">';
    		    echo      '<label class="control-label col-sm-3">'.$column.'</label>';
    		    echo      '<div class="col-sm-9">';
    		    echo          '<input type="text" name="'.$column.'" class="form-control" value="'.$value.'"s />';
    		    echo      '</div>';
    		    echo '</div>';
    		}
    		?>
    		<div class="form-group">
            	<div class="col-sm-9 col-sm-offset-3">
            		<input type="hidden" name="<?= $CMember::TYPENAME; ?>" value="ubionUpdate" />
            		<input type="hidden" name="sesskey" value="<?= sesskey(); ?>" />
            		<input type="hidden" name="returnurl" value="<?= $returnurl; ?>" />
            		<?php 
            		foreach ($hiddenColumns as $hs) {
            		    if (isset($userUbion->$hs)) {
                            echo '<input type="hidden" name="'.$hs.'" value="'.$userUbion->$hs.'" />';
            		    }
            		}
            		?>
            		<input type="submit" class="btn btn-primary" value="저장" />
            	</div>
            </div>
    	</form>
    
    </div>
    <?php 
    echo $OUTPUT->footer();
} else {
    Javascript::getAlert('존재하지 않는 회원입니다.');
}
