<?php
use \local_ubion\base\Parameter;
use \local_ubion\base\Paging;

$i8n = new stdClass();
$i8n->edit = get_string('edit');
$i8n->delete = get_string('delete');
$i8n->logs = get_string('logs');
$i8n->userdetails = get_string('userdetails');
$i8n->authmethod = get_string('authmethod', 'local_ubion');

$CUser = \local_ubion\user\User::getInstance();
$CMember = \local_ubion\asite\member\Member::getInstance();

// post될 주소
$actionURL = $CAsite->sGetUrl('act', 'action_');
$returnURL = base64_encode($SITECFG->nowUrl);

$auth = optional_param('auth', '', PARAM_NOTAGS);		// 인증타입 (OCW때문에 PARAM_AUTH하면 안됨)
$userType = optional_param('user_type', Parameter::getOptionAllValue(), PARAM_NOTAGS);
$univUserType = optional_param('univ_user_type', Parameter::getOptionAllValue(), PARAM_NOTAGS);
$institutionCode = optional_param('institution', Parameter::getOptionAllValue(), PARAM_ALPHANUMEXT);
$departmentCode = optional_param('department', Parameter::getOptionAllValue(), PARAM_ALPHANUMEXT);
$keyfield = optional_param('keyfield', null, PARAM_NOTAGS);
$keyword = Parameter::getKeyword();
$page = optional_param('page', 1, PARAM_INT);
$ls = optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);


list ($totalCount, $members) = $CMember->getMembers($keyfield, $keyword, $auth, $institutionCode, $departmentCode, $userType, $univUserType, $page, $ls);
$paging = Paging::getPaging($totalCount, $page, $ls);

$PAGE->requires->js_call_amd('local_ubion/asiteMember', 'index', array(
    'url' => $actionURL
    ,'optionAllString' => Parameter::getOptionAllValue()
    ,'enrolUrl' => $CAsite->sGetUrl('act', 'courses')
));


$PAGE->requires->strings_for_js(array(
    'as_login'
), 'local_ubion');

echo $OUTPUT->header();

?>
<div class="asite-member">
	<form class="form-horizontal form-search mb-4 form-validate well">
		<div class="form-group">
        	<label class="control-label col-sm-3"><?=get_string('listsize', 'local_ubion');?></label>
        	<div class="col-sm-9">
				<select name="ls" class="form-control">
					<?php
					if ($listSizes = Parameter::getListSizes()) {
					    foreach ($listSizes as $lsKey => $lsValue) {
					        $selected = ($lsKey == $ls) ? ' selected="selected"' : '';
					        echo '<option value="'.$lsKey.'" '.$selected.'>'.$lsValue.'</option>';
					    }
					}
					?>
				</select>
        	</div>
        </div>
        <div class="form-group">
        	<label class="control-label col-sm-3"><?= $i8n->authmethod; ?></label>
        	<div class="col-sm-9">
				<select name="auth" class="form-control">
					<option value="<?= Parameter::getOptionAllValue(); ?>"><?= $i8n->authmethod; ?></option>
					<?php
					if ($auths = get_enabled_auth_plugins()) {
					    foreach ($auths as $as) {
					        $selected = ($as == $auth) ? 'selected="selected"' : '';
					        echo '<option value="'.$as.'" '.$selected.'>'.get_string('pluginname', 'auth_'.$as).'</option>';
					    }
					}
					?>
				</select>
        	</div>
        </div>
		
        <div class="form-group">
        	<label class="control-label col-sm-3"><?= get_string('search'); ?></label>
        	<div class="col-sm-9">
        		<div class="form-inline">
        			<?php 
        			$keyfields = array(    
                         'memberkey' => get_string('username')
                        ,'fullname' => get_string('fullnameuser')
        			);
        			
        			echo '<select name="keyfield" class="form-control">';
        			foreach ($keyfields as $key => $value) {
        			    $selected = ($keyfield == $key) ? ' selected="selected"' : '';
        			    echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';    
        			}
        			echo '</select>';
        			?>
    				<input type="text" class="form-control" name="keyword" value="<?= $keyword; ?>" placeholder="<?= get_string('keyword', 'local_ubion')?>" />
				</div>
        	</div>
        </div>
        <div class="form-group">
        	<div class="col-sm-9 col-sm-offset-3">
        		<input type="submit" class="btn btn-success" value="<?= get_string('search'); ?>" />
        	</div>
        </div>
	</form>
	
	<div class="form-result">
		<?php
		if ($totalCount > 0) {
            echo '<div class="paging-info small mb-3">';
            echo    get_string('total') . ' : <span class="text-info">' . number_format($totalCount) . '</span>,  <span class="ml-2">' . number_format($paging->currentPage) . ' / ' . number_format($paging->totalPage) . '</span>';
            echo '</div>';
		}
		?>
		<div class="table-responsive">
			<table class="table table-bordered table-coursemos">
    			<colgroup>
    				<col class="wp-50" />
    				<col class="wp-100" />
    				<col />
    				<col class="wp-130" />
    				<col class="wp-120" />
    				<col class="wp-180" />
    			</colgroup>
    			<thead>
    				<tr>
    					<th class="text-center"><?=get_string('number', 'local_ubion');?></th>
    					<th class="text-center"><?=get_string('username');?></th>
    					<th class="text-center"><?=get_string('fullnameuser');?></th>
    					<th class="text-center"><?=get_string('phone2');?></th>
    					<th class="text-center"><?=$i8n->userdetails;?></th>
    					<th class="text-center"><?=get_string('etc', 'local_ubion');?></th>
    				</tr>
    			</thead>
    			<tbody>
    				<?php 
    				if ($totalCount > 0) {
    				    $iconMarginClass = 'etc-icon-btn';
    				    
    				    $printNum = $totalCount - (($page - 1) * $ls);
    				    foreach ($members as $m) {
    				        echo '<tr>';
    				        echo    '<td class="text-center">'.number_format($printNum).'</td>';
    				        echo	'<td class="text-center">'.$m->username.'</td>';
    				        echo	'<td>'.fullname($m).'</td>';
    				        echo	'<td class="text-center">'.$m->phone2.'</td>';
    				        echo	'<td class="text-center">';
    				        echo        '<button class="btn btn-info btn-xs btn-user-default" data-id="'.$m->id.'">'.$i8n->userdetails.'</button>';
    				        echo    '</td>';
    				        echo	'<td class="text-center">';
    				        echo		'<a href="'.$CFG->wwwroot.'/user/editadvanced.php?id='.$m->id.'&amp;course=1'.'" target="_balnk" class="'.$iconMarginClass.'" title="'.$i8n->edit.'">';
    				        echo            '<i class="fa fa-pencil"></i>';
    				        echo		'</a>';
    				        
    				        echo		'<a href="'.$CAsite->sGetUrl('', 'mod', 'member', 'act', 'ubion', 'id', $m->id, 'returnurl', $returnURL).'" target="_balnk" class="'.$iconMarginClass.'" title="user-ubion '.$i8n->edit.'">';
    				        echo             '<i class="fa fa-edit"></i>';
    				        echo		'</a>';
    				        
    				        echo		'<a href="'.$CFG->wwwroot.'/report/log/index.php?chooselog=1&user='.$m->id.'&date=&modid=&modaction=&logformat=showashtml" target="_balnk" class="'.$iconMarginClass.'" title="'.$i8n->logs.'">';
    				        echo            '<i class="fa fa-history"></i>';
    				        echo		'</a>';
    				        
    				        echo		'<a href="'.$CFG->wwwroot.'/admin/user.php?delete='.$m->id.'&sesskey='.sesskey().'" target="_balnk" class="'.$iconMarginClass.'" title="'.$i8n->delete.'">';
    				        echo            '<i class="fa fa-remove"></i>';
    				        echo		'</a>';
    				        
    				        echo		'<a href="'.$CFG->wwwroot.'/course/loginas.php?id=1&user='.$m->id.'&sesskey='.sesskey().'" target="_balnk" class="btn-aslogin '.$iconMarginClass.'" title="as login" data-username="'.fullname($m).'">';
    				        echo            '<i class="fa fa-user-secret"></i>';
    				        echo		'</a>';
    				        echo    '</td>';
    				        echo '</tr>';
    				        $printNum--;
    				    }
    				} else {
    				    echo '<tr><td colspan="6" class="p-4">'.get_string('not_found_users', 'local_ubion').'</td></tr>';
    				}
    				?>
    			</tbody>
    		</table>
		</div>
		<?php 
		echo $CAsite->sPaging($paging);
		?>
		<div id="modal-user-detail" class="modal" tabindex="-1" role="dialog">
        	<div class="modal-dialog modal-lg" role="document">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h5 class="modal-title">[<span class="fullname"></span>]</h5>
        			</div>
        			<div class="modal-body">
        				
        			</div>
        			<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo get_string('close', 'local_ubion'); ?></button>
        			</div>
        		</div><!-- /.modal-content -->
        	</div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
	</div>
</div>

<?php 
echo $OUTPUT->footer();