<?php

use local_ubion\base\Javascript;

require_once $CFG->libdir.'/phpexcel/PHPExcel.php';

$id = required_param('id', PARAM_INT);

list ($isSuccess, $message, $html) = \local_ubion\course\Course::getInstance()->setUserBatch($id);

if ($isSuccess) {
    echo $OUTPUT->header();
    echo '<div class="asite-course asite-course-irregular">';
    echo    $html;
    echo    '<div class="text-center"><a href="'.$CAsite->sGetUrl('', 'mod', $SITECFG->module).'" class="btn btn-default">'.get_string('move').'</a></div>';
    echo '</div>';
    echo $OUTPUT->footer();
    
} else {
    Javascript::printAlert($message);
}