<?php

$id = required_param('id', PARAM_INT);

$CCourse = \local_ubion\course\Course::getInstance();
$CVnCourse = \local_vn\Course::getInstance();

$courseInfo = $CCourse->getCourse($id);

$PAGE->requires->js_call_amd('local_ubion/asiteIrregular', 'batch');

$enrolurl = $CFG->wwwroot.'/user/index.php?id='.$id;
echo $OUTPUT->header();
?>
<div class="asite-course asite-course-irregular">
	<div class="well">
		<div class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-sm-3"><?= get_string('course_name', 'local_ubion');  ?></label>
				<div class="col-sm-9">
					<div class="form-control-plaintext"><?= $CCourse->getName($courseInfo); ?></div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="well">
		<form id="add_form" action="<?php echo $CAsite->sGetUrl('act', 'batch_view'); ?>" method="post" class="form-horizontal form-validate" enctype="multipart/form-data">
			<div class="form-group">
				<label class="control-label col-sm-3"><?= get_string('samplefile', 'local_ubion') ?></label>
				<div class="col-sm-9">
					<a href="batch.xlsx" class="btn btn-success"><?= get_string('down'); ?></a>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3"><?= get_string('upload'); ?></label>
				<div class="col-sm-9">
					<input type="hidden" name="sesskey" value="<?= sesskey(); ?>" />
					<input type="hidden" name="<?= $CVnCourse::TYPENAME; ?>" value="batch" />
					<input type="hidden" name="id" value="<?= $id; ?>" />
					<input type="file" name="uploaduser" class="form-control required" placeholder="<?= get_string('file') ?>" />
				</div>
			</div>
			<div class="form-group">
    			<div class="col-sm-9 col-sm-offset-3">
    				<button type="submit" class="btn btn-primary"><?= get_string('save'); ?></button>
    			</div>
    		</div>
		</form>
	</div>
</div>
<?php
echo $OUTPUT->footer();