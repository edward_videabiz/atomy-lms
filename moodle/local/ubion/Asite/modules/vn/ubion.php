<?php
use local_ubion\base\Javascript;
use local_ubion\course\Course;

$id = required_param('id', PARAM_INT);
$returnurl = optional_param('returnurl', null, PARAM_NOTAGS);

$CCourse = Course::getInstance();
$CVnCourse = \local_ubion\vn\Course::getInstance();

// 실제 존재하는 강좌인지 확인
if ($course = $DB->get_record_sql("SELECT * FROM {course} WHERE id = :id", array(
    'id' => $id
))) {

    // getUbion에서 추가까지 진행되기 때문에 따로 예외처리 안해도 됨.
    // 항상 디비 데이터가 표시되도록 캐시 사용안하도록 파라메터 추가
    $courseUbion = $CCourse->getUbion($id, true);

    // 수정 불가 컬럼
    $hiddenColumns = $CVnCourse->getDBHiddenColumns();

    // unixtimecolumn
    $unixtimeColumns = $CVnCourse->getDBUnixtimeColumn();

    echo $OUTPUT->header();
    ?>
    <div class="asite-course">
    
    	<form method="post" action="<?=$CAsite->sGetUrl('act', 'action_');?>" class="form-horizontal">
    		<?php
    foreach ($courseUbion as $column => $value) {
        if (in_array($column, $hiddenColumns)) {
            continue;
        }
        echo '<div class="form-group">';
        echo '<label class="control-label col-sm-3">' . $column . '</label>';
        echo '<div class="col-sm-9">';

        $addClass = '';
        // 시작, 종료일에 대한 예외처리
        if (in_array($column, $unixtimeColumns) && ! empty($value)) {
            $addClass = 'form-datepicker';
            $value = date('Y-m-d', $value);
        }
        echo '<input type="text" name="' . $column . '" class="form-control ' . $addClass . '" value="' . $value . '" />';
        echo '</div>';
        echo '</div>';
    }
    ?>
    		<div class="form-group">
            	<div class="col-sm-9 col-sm-offset-3">
            		<input type="hidden" name="<?=$CVnCourse::TYPENAME;?>" value="ubionUpdate" />
            		<input type="hidden" name="sesskey" value="<?=sesskey();?>" />
            		<input type="hidden" name="returnurl" value="<?=$returnurl;?>" />
            		<?php
    foreach ($hiddenColumns as $hs) {
        if (isset($courseUbion->$hs)) {
            echo '<input type="hidden" name="' . $hs . '" value="' . $courseUbion->$hs . '" />';
        }
    }
    ?>
            		<input type="submit" class="btn btn-primary" value="<?= get_string('save'); ?>" />
            	</div>
            </div>
    	</form>
    
    </div>
    <?php
    echo $OUTPUT->footer();
} else {
    Javascript::getAlert(get_string('not_found_course', 'local_ubion'));
}
