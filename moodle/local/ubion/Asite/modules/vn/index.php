<?php
use local_ubion\base\Parameter;
use local_ubion\base\Paging;
use local_ubion\base\Common;

$i8n = new stdClass();
$i8n->fullnameuser = get_string('fullnameuser');
$i8n->username = get_string('username', 'local_vn');
$i8n->search = get_string('search');
$i8n->completion_n = get_string('completion_n', 'local_vn');
$i8n->completion_y = get_string('completion_y', 'local_vn');
$i8n->completion_yn = get_string('completion_yn', 'local_vn');
$i8n->log = get_string('log', 'local_vn');
$i8n->onoff = get_string('onoff', 'local_vn');
$i8n->off_user = get_string('off_user', 'local_vn');
$i8n->on_user = get_string('on_user', 'local_vn');

$CCourse = \local_ubion\course\Course::getInstance();
$CUser = \local_ubion\user\User::getInstance();

// post될 주소
$actionURL = $CAsite->sGetUrl('act', 'action_');

$optionValueNull = Parameter::getOptionAllValue();

$complete = optional_param('complete', $optionValueNull, PARAM_INT);
$keyfield = optional_param('keyfield', null, PARAM_NOTAGS);
$keyword = Parameter::getKeyword();
$page = optional_param('page', 1, PARAM_INT);
$ls = optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);
$onoff = optional_param('onoff', null, PARAM_INT);

$CVnCourse = \local_vn\Course::getInstance();
$config = $CVnCourse->getConfig();

$courseid = $config->courseid ?? null;
if (empty($courseid)) {
    $link = $CFG->wwwroot . '/admin/settings.php?section=local_vn_default';
    Common::printNotice(get_string('no_courseid', 'local_vn'), $link);
}

list ($totalCount, $users) = $CVnCourse->getEnrolusers($courseid, $onoff, $complete, $keyfield, $keyword, $page, $ls);

$paging = Paging::getPaging($totalCount, $page, $ls);


$PAGE->requires->js_call_amd('local_ubion/asiteVN', 'index', array(
    'courseid' => $courseid,
    'url' => $actionURL,
));

$PAGE->requires->string_for_js('not_selected_completion', 'local_vn');
$PAGE->requires->string_for_js('min_user', 'local_ubion');

echo $OUTPUT->header();

?>
<div class="asite-vn">
	<form class="form-horizontal form-search mb-4 form-validate well">
		<div class="form-group">
        	<label class="control-label col-sm-3"><?=get_string('listsize', 'local_ubion');?></label>
        	<div class="col-sm-9">
				<select name="ls" class="form-control">
					<?php
                    if ($listSizes = Parameter::getListSizes()) {
                        foreach ($listSizes as $lsKey => $lsValue) {
                            $selected = ($lsKey == $ls) ? ' selected="selected"' : '';
                            echo '<option value="' . $lsKey . '" ' . $selected . '>' . $lsValue . '</option>';
                        }
                    }
                    ?>
				</select>
        	</div>
        </div>
        <div class="form-group">
        	<label class="control-label col-sm-3"><?=$i8n->onoff;?></label>
        	<div class="col-sm-9">
				<select name="onoff" class="form-control">
					<?php
					$options = array(
					    $optionValueNull => $i8n->onoff,
					    '0' => $i8n->on_user,
					    '1' => $i8n->off_user
					);
					
					foreach ($options as $osKey => $osValue) {
					    $osKey = (string) $osKey;
					    $selected = ($osKey == $onoff) ? ' selected="selected"' : '';
					    
					    echo '<option value="' . $osKey . '" ' . $selected . '>' . $osValue . '</option>';
					}
                    ?>
				</select>
        	</div>
        </div>
        <div class="form-group">
        	<label class="control-label col-sm-3"><?=$i8n->completion_yn;?></label>
        	<div class="col-sm-9 form-inline">
        		<select name="complete" class="form-control">
            		<?php
                    $options = array(
                        $optionValueNull => $i8n->completion_yn,
                        $CVnCourse::COMPLETION_ON => get_string('completion_on', 'local_vn'),
                        $CVnCourse::COMPLETION_OFF => get_string('completion_off', 'local_vn')
                    );
        
                    foreach ($options as $osKey => $osValue) {
                        $osKey = (string) $osKey;
                        $selected = ($osKey == $complete) ? ' selected="selected"' : '';
        
                        echo '<option value="' . $osKey . '" ' . $selected . '>' . $osValue . '</option>';
                    }
                    ?>
    			</select>
        	</div>
        </div>
        <div class="form-group">
        	<label class="control-label col-sm-3"><?=$i8n->search;?></label>
        	<div class="col-sm-9">
        		<div class="form-inline">
        			<?php
                    $keyfields = array(
                        'username' => $i8n->username,
                        'fullname' => $i8n->fullnameuser
                    );
            
                    echo '<select name="keyfield" class="form-control">';
                    foreach ($keyfields as $key => $value) {
                        $selected = ($keyfield == $key) ? ' selected="selected"' : '';
                        echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
                    }
                    echo '</select>';
                    ?>
    				<input type="text" class="form-control" name="keyword" value="<?=$keyword;?>" placeholder="<?=get_string('keyword', 'local_ubion')?>" />
				</div>
        	</div>
        </div>
        <div class="form-group">
        	<div class="col-sm-9 col-sm-offset-3">
        		<input type="submit" class="btn btn-default" name="search" value="<?=$i8n->search;?>" />
        		
        		<?php
                if (! empty($keyword) && ! empty($keyfield)) {
        
                    echo '<a href="' . $CAsite->sGetUrl('', 'mod', 'vn') . '" class="btn btn-default">' . get_string('search_cancel', 'local_ubion') . '</a>';
                }
                ?>
        	</div>
        </div>
	</form>
	
	<div class="form-result">
		    	
    	<?php
        if ($totalCount > 0) {
            echo '<div class="paging-info small mb-3">';
            echo get_string('total') . ' : <span class="text-info">' . number_format($totalCount) . '</span>,  <span class="ml-2">' . number_format($paging->currentPage) . ' / ' . number_format($paging->totalPage) . '</span>';
            echo '</div>';
        }
        ?>
		
		<div class="table-responsive">
			<form class="form-validate form-checkusers" method="post" action="<?= $actionURL ?>">
    			<table class="table table-bordered table-coursemos">
        			<colgroup>
        				<?php
        				/*
        				<col class="wp-50" />
        				*/ ?>
        				<col class="wp-80" />
        				<col class="wp-120" />
        				<col class="wp-120" />
        				<col />
        				<col class="wp-200" />
        				<col class="wp-100" />
        			</colgroup>
        			<thead>
        				<tr>
            				<?php
            				/*
        					<th class="text-center">
        						<input type="checkbox" name="all_checked" class="checkbox-all" />
        					</th>
        					*/ ?>
        					<th class="text-center"><?=get_string('number', 'local_ubion');?></th>
        					<th class="text-center"><?=$i8n->onoff?></th>
        					<th class="text-center"><?=$i8n->username;?></th>
        					<th class="text-center"><?=$i8n->fullnameuser;?></th>
        					<th class="text-center"><?=get_string('edufinishdate', 'local_vn').' / '.$i8n->log;?></th>
        					<th class="text-center"><?=get_string('educertdate', 'local_vn');?></th>
        				</tr>
        			</thead>
        			<tbody>
            			<?php
                        if ($totalCount > 0) {
                            $number = $totalCount - (($page - 1) * $ls);
                            foreach ($users as $u) {
                                echo '<tr>';
                                /*
                                echo '<td class="text-center">';
                                echo '<input type="checkbox" name="userids[]" value="' . $u->id . '" />';
                                echo '</td>';
                                */
                                echo '<td class="text-center">' . number_format($number) . '</td>';
                                echo '<td class="text-center">';
                                echo    ($u->onoff) ? $i8n->off_user : '<span class="text-primary">'.$i8n->on_user.'</span>';
                                echo '</td>';
                                echo '<td class="text-center">' . $u->username . '</td>';
                                echo '<td>';
                                echo    fullname($u);
                                echo '</td>';
                
                                // timecompleted값은 course_completions 값임
                                // local_vn_complete_attempt 테이블은 회차별 기록이기 때문에 course_completions값이 가장 정확함
                                echo '<td class="text-center">';
                                $timecomplete = $u->lmsedufinishdate;
                                if (empty($timecomplete)) {
                                    $timecomplete = $u->online_timecompleted;
                                } else {
                                    // 2018-09-12T00:00:00 형태이기 때문에 strtotime 해줘야됨.
                                    $timecomplete = strtotime($timecomplete);
                                }
                                
                                /*
                                if (empty($timecomplete)) {
                                    echo '<label class="label label-danger">' . $i8n->completion_n . '</label>';
                                } else {
                                    $timecomplete = Common::getUserDate($timecomplete);
                                    echo '<label class="label label-primary" data-toggle="tooltip" data-placement="top" title="' . $timecomplete . '">' . $i8n->completion_y . '</label>';
                                }
                                */
                                
                                if (!empty($timecomplete)) {
                                    echo '<button type="button" class="btn btn-primary btn-xs" title="'.Common::getUserDate($timecomplete).'" data-toggle="tooltip" data-placement="top">'.Common::getUserDateShort($timecomplete).'</button>';
                                }
                                
                                // 온라인 강좌라면 상세 기록이 표시되어야 함.
                                echo '<button type="button" class="btn btn-xs btn-default btn-log ml-1" data-userid="' . $u->id . '">' . $i8n->log . '</button>';
                                
                                echo '</td>';
                                echo '<td class="text-center">';
                                $timecomplete = $u->lmseducertdate;
                                if (empty($timecomplete)) {
                                    $timecomplete = $u->offline_timecompleted;
                                } else {
                                    // 2018-09-12T00:00:00 형태이기 때문에 strtotime 해줘야됨.
                                    $timecomplete = strtotime($timecomplete);
                                }
                                /*
                                if (empty($timecomplete)) {
                                    echo '<label class="label label-danger">' . $i8n->completion_n . '</label>';
                                } else {
                                    $timecomplete = Common::getUserDate($timecomplete);
                                    echo '<label class="label label-primary" data-toggle="tooltip" data-placement="top" title="' . $timecomplete . '">' . $i8n->completion_y . '</label>';
                                }
                                */
                                if (!empty($timecomplete)) {
                                    echo '<label class="label label-primary" title="'.Common::getUserDate($timecomplete).'" data-toggle="tooltip" data-placement="top">'.Common::getUserDateShort($timecomplete).'</label>';
                                }
                                echo '</td>';
                                
                                echo '</tr>';
                
                                $number --;
                            }
                        } else {
                            echo '<tr><td colspan="6" class="p-4 text-center">' . get_string('no_users', 'local_vn') . '</td></tr>';
                        }
                        ?>
        			</tbody>
        			<?php
                    if ($totalCount > 0) {
                    /*
                    ?>
        			<tfoot>
        				<tr>
        					<td colspan="6">
        						<div class="form-inline">
        							<span class="help-inline text-info"><?=get_string('selected_user', 'local_ubion');?></span>
        							
        							<input type="hidden" name="<?= $CVnCourse::TYPENAME; ?>" value="checkCompletion" />
        							<input type="hidden" name="courseid" value="<?= $courseid; ?>" />
        							<input type="hidden" name="sesskey" value="<?= sesskey(); ?>" />
        							<select name="complete" class="form-control">
                                		<?php
                                        $options = array(
                                            $optionValueNull => $i8n->completion_yn,
                                            '1' => $i8n->completion_y,
                                            '0' => $i8n->completion_n
                                        );
                                
                                        foreach ($options as $osKey => $osValue) {
                                            $osKey = (string) $osKey;
                                            echo '<option value="' . $osKey . '">' . $osValue . '</option>';
                                        }
                                        ?>
                        			</select>
        							<button type="submit" class="btn btn-primary btn-selected-user">변경</button>
        						</div>
        					</td>
        				</tr>
        			</tfoot>
        			<?php
        			*/
                    }
                    ?>
        		</table>
    		</form>
    		<?=$CAsite->sPaging($paging);?>
    	</div>
    	
		
		<div id="modal-attempt" class="modal" tabindex="-1" role="dialog">
        	<div class="modal-dialog modal-lg" role="document">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title"></h4>
        			</div>
        			<div class="modal-body">
        				<table class="table table-bordered table-coursemos">
        					<colgroup>
        						<col class="wp-80" />
        						<col />
        						<col class="wp-200" />
        						<col class="wp-120" />
        					</colgroup>
        					<thead>
        						<tr>
        							<th><?= print_string('attempt', 'local_vn'); ?></th>
        							<th><?= print_string('study_period', 'local_vn'); ?></th>
        							<th><?= print_string('completion_time', 'local_vn'); ?></th>
        							<th><?= print_string('log', 'local_vn'); ?></th>
        						</tr>
        					</thead>
        					<tbody class="table-body">
        					
        					</tbody>
        				</table>
        			</div>
        			<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal"><?=get_string('close', 'local_ubion');?></button>
        			</div>
        		</div><!-- /.modal-content -->
        	</div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        
        <div id="modal-attempt-activity" class="modal" tabindex="-1" role="dialog">
        	<div class="modal-dialog modal-lg" role="document">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title"></h4>
        			</div>
        			<div class="modal-body">
        				<table class="table table-bordered table-coursemos">
        					<colgroup>
        						<col class="wp-80" />
        						<col />
        						<col class="wp-200" />
        					</colgroup>
        					<thead>
        						<tr>
        							<th><?= print_string('number', 'local_ubion'); ?></th>
        							<th><?= print_string('activity_name', 'local_vn'); ?></th>
        							<th><?= print_string('completion_time', 'local_vn'); ?></th>
        						</tr>
        					</thead>
        					<tbody class="table-body">
        					
        					</tbody>
        				</table>
        				
        				<div class="paging"></div>
        			</div>
        			<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal"><?=get_string('close', 'local_ubion');?></button>
        			</div>
        		</div><!-- /.modal-content -->
        	</div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
</div>

<?php
echo $OUTPUT->footer();