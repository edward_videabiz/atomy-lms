<?php

$controllder = \local_vn\Course::getInstance();

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}
