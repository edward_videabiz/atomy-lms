<?php

/* 변경사항
 *
 * 이 파일에 무언가를 추가 해야 한다면, modules/common.php 에 추가하면 됨.
 *
 * template_를 사용하면서 각 모듈 php파일에서 assign 을 매번 해줘야 했으나, 자동으로 assign 되도록 했기 때문에
 * $tpl_->assign() 를 하지 않아도 됨.
 *
 * template_ 를 사용하지 않고 순수 php 문법으로만 페이지를 만드록 싶다면 _tpl/액션.html 을 만들지 않으면됨.
 */

require_once __DIR__."/config.php";


// 기본 세팅 시작
$modulePath = $SITECFG->modulePath .'/'. $SITECFG->module;
if (!is_dir($modulePath)) {
    
    if (is_array($SITECFG->moduleArr) && strpos($SITECFG->moduleName, '.xls') !== false) {
        
        $filePath = $SITECFG->modulePath;
        foreach ($SITECFG->moduleArr as $mr) {
            if (!empty($mr)) {
                $filePath .= '/'.$mr;
            }
        }
     
        // 실제 파일이 존재하는 경우
        if (file_exists($filePath)) {
            header("Location: ".$SITECFG->moduleRealUrl);
            exit;
        }
    }
    
    
    die('not found module.');
}

/*
 * 하나의 act (php 파일)에서 tpl 파일 분기하기.
 * 액션(act)이 @index 면 index.php 파일과 _tpl/index.html 을 읽는다.
 * 이때 @index[test] 라고 선언하면 index.php 파일과 _tpl/test.html 을 읽는다.
 */
if (preg_match("/^(.*)\[(.*)\]$/i", $SITECFG->act, $tmpMatch)) {
    $tmpAct = $tmpMatch[1];
} else {
    $tmpAct = $SITECFG->act;
}

$actPage = "{$modulePath}/{$tmpAct}.php";
if (!is_file($actPage)) {
    die('not found act.');
}

// action 이름이 _(언더바)로 끝나면 (action_) 독립 실행 한다.
if (preg_match("/.+\_$/", $tmpAct)) {
    include $actPage;
    exit;
}

//require_once $SITECFG->modulePath.'/auth/lib.php';
//$auth = new Asite_modules_auth_lib();
//$auth->authRedirectUrl($SITECFG->module);



// layout 실행
ob_start();
include $actPage;
$html = ob_get_contents();
ob_end_clean();
echo $html;
