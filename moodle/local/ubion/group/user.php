<?php
require_once '../../../config.php';


$pluginname = 'local_ubion';

$id = optional_param('id', 0, PARAM_INTEGER);

$CCourse = \local_ubion\course\Course::getInstance();
$CGroup = \local_ubion\group\Group::getInstance();
$CUser = \local_ubion\user\User::getInstance();

$course = $CCourse->getCourse($id);
$courseUbion = $CCourse->getUbion($id);

// 강좌 context
$courseContext = context_course::instance($course->id);
$PAGE->set_context($courseContext);

// 반드시 로그인이 필요함.
require_login($course);

$i8n = new stdClass();
$i8n->group_member = get_string('group_member', $pluginname);

$PAGE->set_url('/local/ubion/group/user.php', array('id'=>$id));
$PAGE->set_pagelayout('incourse');
$PAGE->set_title($i8n->group_member);
$PAGE->navbar->add($i8n->group_member, $PAGE->url->out());

$isProfessor = $CCourse->isProfessor($id, $courseContext);

// eclass나 비교과인 경우에는 본인이 소속된 그룹만 보여야됨.
$isGroupAll = true;
if ($courseUbion->course_type == $CCourse::TYPE_ECLASS || $courseUbion->course_type == $CCourse::TYPE_ONLINE) {
    if (!$isProfessor) {
        $isGroupAll = false;
    }
}

// 기본적으로는 사용자 이름으로 정렬
$orderSQL = 'lastname';
if (current_language() == 'ko') {
    $orderSQL = 'firstname';
}

$courseGroup = $CGroup->getCourseGroups($id);


// 반복되서 사용되는 항목 closure로 구현
$closureGroup = function($users, &$notinGroup, $isGroup = false) use ($CFG, $CUser, $isProfessor, $pluginname) {
    
    $html = '<ul class="users row">';
    if ($users) {
        foreach ($users as $u) {
            $u->fullname = fullname($u);
            
            // 학번
            $idnumber = $CUser->getIdnumber($u->idnumber, $isProfessor);
            
            // 사진
            $picture = $CUser->getPicture($u->id);
            
            $html .= '<li class="col-xs-12 col-sm-6 col-md-4">';
            $html .=    '<a href="'.$CFG->wwwroot.'/local/ubmessage/message.php?id='.$u->id.'" title="'.$u->fullname.'" class="ellipsis">';
            $html .=	   '<img src="'.$picture.'" alt="'.$u->fullname.'" />&nbsp;&nbsp;';
            $html .= 	  $u->fullname.' ('.$idnumber.')';
            $html .=    '</a>';
            $html .= '</li>';
            
            if ($notinGroup && array_key_exists($u->id, $notinGroup)) {
                unset($notinGroup[$u->id]);
            }
        }
    } else {
        if ($isGroup) {
            $noUserMessage = get_string('no_user_group', $pluginname);
        } else {
            $noUserMessage = get_string('no_user_course', $pluginname);
        }
        
        $html .= '<li class="text-info">'.$noUserMessage.'</li>';
    }
    $html .= '</ul>';
    
    return $html;
};

$PAGE->requires->css('/local/ubion/group/styles.css');

echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->group_member, 2);
?>

<div class="local-ubion local-ubion-group">
<?php
// 강좌내에 그룹이 존재하는지 확인
if ($courseGroup->is) {
    
	// 모든 그룹구성원을 볼수 있는 경우
	if ($isGroupAll) {
		// 강좌에 속한 회원 모두 가져오기
		$users = $CCourse->getCourseUserAll($course);
	
		// 그룹에 속하지 않는 사용자
		$notingUsers = $users;
		
		// 강좌내에 존재하는 모든 그룹을 가져옴
		// 그룹 분류내에 존재하지 않는 그룹이 있는 경우를 걸러내기도 함. 
		$notingGroups = groups_get_all_groups($course->id);
	
		
		// 강좌내 강좌 분류를 사용하는 경우
		if ($gropping = groups_get_all_groupings($course->id)) {
			foreach ($gropping as $gp) {
				
				echo '<fieldset class="fieldset-group">';
				echo 	'<legend>'.$gp->name.'</legend>';
				
				if (!empty($gp->groups)) {
				    foreach ($gp->groups as $gpg) {
				        echo '<fieldset class="fieldset-group">';
				        echo '<legend>'.$gpg->name.'</legend>';
				        
				        $users = groups_get_members($gpg->id, 'u.*', $orderSQL);
				        echo $closureGroup($users, $notingUsers, true);
				        echo '</fieldset>';
				        
				        // 그룹 분류에 속하지 않는 그룹이 존재하는 경우 아래 로직에서 기타 그룹으로 표시해줘야됨.
				        unset($notingGroups[$gpg->id]);
				    }
				} else {
				    echo '<div class="text-info">'.get_string('groupsnone', 'group').'</div>';
				}
				
				echo '</fieldset>';
				
			}
		}
		
		// 그룹분류에 속하지 않는 그룹이 존재할수도 있음.
		if ($notingGroups) {
			foreach ($notingGroups as $gpg) {
				echo '<fieldset class="fieldset-group">';
				echo '<legend>'.$gpg->name.'</legend>';

				$users = groups_get_members($gpg->id, 'u.*', $orderSQL);
				echo $closureGroup($users, $notingUsers, true);
				echo '</fieldset>';
			}
		}
		
	
		// 그룹에 소속되지 않은 사용자가 존재하는 경우 표시
		if (!empty($notingUsers)) {
			
			echo '<fieldset class="fieldset-group">';
			echo '<legend>'.get_string('group_others', $pluginname).'</legend>';
	
			$emptyNothingUsers = array();
			echo $closureGroup($notingUsers, $emptyNothingUsers);
			echo '</fieldset>';
		}
	} else {
		$notingUsers = array();
		
		// 무조건 리턴값이 있음.
		// key값이 0이면 소속되어 있는 모든 그룹 정보가 담겨있음.
		// key값이 0보다 크면 그룹 분류 코드임.
		$myGroups = groups_get_user_groups($course->id);
			
		// 0번째 항목만 존재하는지 확인
		$myAllGroups = $myGroups[0];
		unset($myGroups[0]);
		
		// my_groups => 그룹분류 목록
		// my_all_groups => 강좌내 본인이 소속된 모든 그룹
		if ($myGroups || $myAllGroups) {
			
			// 그룹분류가 존재하는지 확인
			if ($myGroups) {
				foreach ($myGroups as $groupingid => $groupids) {
					if ($groupingInfo = groups_get_grouping($groupingid)) {
						echo '<fieldset class="fieldset-group">';
						echo 	'<legend>'.$groupingInfo->name.'</legend>';
					
						foreach ($groupids as $groupid) {
							$groupInfo = groups_get_group($groupid);
							echo '<fieldset class="fieldset-group">';
							echo '<legend>'.$groupInfo->name.'</legend>';
								
							$users = groups_get_members($groupid, 'u.*', $orderSQL);
							echo $closureGroup($users, $notingUsers, true);
							echo '</fieldset>';
							
							// my_all_groups는 강좌내 본인이 소속된 그룹 모두 가지고 있기 때문에 
							// 그룹핑에 존재하는 그룹이 my_all_groups에 존재하는 그룹을 삭제 시켜줘야됨.
							// my_all_groups에 남아 있는 그룹이 있으면 그룹분류 출력 후 그룹만 별도로 출력시켜줘야됨.
							if (in_array($groupid, $myAllGroups)) {
								$myAllGroups = array_values(array_diff($myAllGroups, array($groupid)));
							}
						}
						echo '</fieldset>';	
					}
				}
				
				// 그룹 분류가 없이 그룹에 속한 경우
				if ($myAllGroups) {
					foreach ($myAllGroups as $groupid) {
						$groupInfo = groups_get_group($groupid);
						echo '<fieldset class="fieldset-group">';
						echo '<legend>'.$groupInfo->name.'</legend>';
					
						$users = groups_get_members($groupid, 'u.*', $orderSQL);
						echo $closureGroup($users, $notingUsers, true);
						echo '</fieldset>';
					}	
				}
			} else {
				// 그룹분류가 없는 상태이기 때문에 그룹만 출력
				foreach ($myAllGroups as $groupid) {
					$groupInfo = groups_get_group($groupid);
					echo '<fieldset class="fieldset-group">';
					echo '<legend>'.$groupInfo->name.'</legend>';
						
					$users = groups_get_members($groupid, 'u.*', $orderSQL);
					echo $closureGroup($users, $notingUsers, true);
					echo '</fieldset>';
				}
			}
		} else {	
			echo $OUTPUT->notification(get_string('group_no_belong', $pluginname));
		}
	}
} else {
	echo $OUTPUT->notification(get_string('nogroups', 'group'));
}
?>
</div>
<?php
echo $OUTPUT->footer();