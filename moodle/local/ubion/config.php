<?php
/**
 * @category 	config.php
 * @author 		Sung Hoon, Cho (akdddnet@gmail.com)
 * @since		2011. 4. 11.
 * @license		BSD License
 * @version		0.1
 *
 * ## 작업하면서 해당 파일은 수시로 변경될수 있습니다 ##
 *
 * 반드시 지켜주셨으면 하는 사항!!!
 * 	1. CFG값은 되도록이면 대문자로 입력해주시기 바랍니다.
 * 	2. 간혹 작업을 하다보면 CFG에 값을 설정 해줘야되는 상황이 발생이 되는데 곧바로 CFG->xxx 형식으로 입력하지 말아주셨으면 합니다.
 *     무들 자체에서 사용되는 이름과 충돌 위험성도 있지만 차후에 유지 보수 진행시에 블루에서 작업한 내역인지 확인하기도 용이 하기 때문에
 *     반드시 CFG->BLUESOFT->xxx 형태로 사용해주시기 바랍니다. (단 set_config 함수는 절대 사용하면 안됩니다.)
 *     DB랑 연동해서 사용해야되는 경우에는 무들에서 제공해주는 방식으로 사용하시기바랍니다.. (블록, 모듈은 무들에서 settings.php 파일로 해결 가능함)
 *
 * 	3. 무들 자체 코드는 변경 하지 맙시다!!
 *
 *  2011-08-01
 *  	1. course_wizard_column 추가.
 */

//require_once __DIR__."/../config.php";
global $CFG, $PAGE;

// 강좌 블록 설정
// 강좌 개요, 학습활동, 강좌 관리
//$CFG->defaultblocks_override = 'ubion_shortcut';
$CFG->UB = new stdClass();
$CFG->UB->SITE_INFO = new stdClass();
$CFG->UB->SITE_INFO->CLASS = 'pnu';


$CFG->UB = new stdClass();
$CFG->UB->LOCAL = '/local/ubion';
$CFG->UB->WWW = $CFG->wwwroot.$CFG->UB->LOCAL;

// SSO URL
$CFG->UB->SSO = 'http://portal.inu.ac.kr:7780';
$CFG->UB->SSO_URL = $CFG->UB->SSO.'/enview/user/login.face?redirectUrl='.$CFG->wwwroot.'/local/ubion/sso/sso.php';
//$CFG->UB->SSO = 'http://portal.inu.ac.kr';
//$CFG->UB->SSO_URL = $CFG->UB->SSO.'/enview/';

//https://ssodev.ewha.ac.kr/SSO_IDP/swift/sso/loginCheck.jsp
// sso_rsp는 반드시 http://를 제거하고 전달해줘야됨.
$CFG->UB->SSO_RSP = str_replace('http://', '', $CFG->wwwroot);
// cybertest.ewha.ac.kr로 테스트 하는데 cyber.ewha.ac.kr로 보내야되는 상황인데 소켓 통신은 cybertest로 진행해야됨.
// 실제 운영에서는 쓰일일이 없을텐데.. 쩝;;
$CFG->UB->SSO_RSP_FSOCKET = $CFG->UB->SSO_RSP;
$CFG->UB->SSO_RSP = str_replace('test', '', $CFG->UB->SSO_RSP);
$CFG->UB->SSO_FORGOT = 'http://portal.pcu.ac.kr/';
/*
 if(!strcmp($CFG->wwwroot, 'http://global.moodler.co.kr')) {
 $CFG->UB->SSO_RSP = 'global.moodler.co.kr';
 } else {
 $CFG->UB->SSO_RSP = 'cyber.moodler.co.kr';
 }
 */

$CFG->UB->SSO_RelayState = 'makeSsoToken_php.jsp';
$CFG->UB->SSO_INIpluginData = '';
$CFG->UB->SSO_LOGOUT = '/swift/sso/swiftLogOutReq.jsp?RSP='.$CFG->UB->SSO_RSP;
$CFG->UB->SSO_LOGIN = $CFG->wwwroot.'/index.jsp?retUrl=/';


// path 설정.
$CFG->UB->PATH = new stdClass();
$CFG->UB->PATH->ROOT 		= $CFG->dirroot.$CFG->UB->LOCAL;
$CFG->UB->PATH->UTIL		= $CFG->UB->PATH->ROOT."/utility";
$CFG->UB->PATH->WS			= $CFG->UB->PATH->ROOT."/webservice";
$CFG->UB->PATH->LIB			= $CFG->UB->PATH->ROOT."/library";
//$CFG->UB->PATH->HAKSA		= $CFG->UB->PATH->ROOT."/haksa";
$CFG->UB->PATH->HAKSA		= $CFG->UB->PATH->ROOT."/Asite/modules/haksa/haksa";


$CFG->UB->PATH->TEMPLATE_			= new stdClass();
$CFG->UB->PATH->TEMPLATE_->PATH		= $CFG->UB->PATH->UTIL."/template_";
$CFG->UB->PATH->TEMPLATE_->SKIN		= $CFG->dirroot; //$CFG->dirroot."/theme";
$CFG->UB->PATH->TEMPLATE_->IS_COMPILE	= true;
$CFG->UB->PATH->TEMPLATE_->IS_CACHING	= false;
$CFG->UB->PATH->TEMPLATE_->CACHE_EXPIRE	= 3600;

$CFG->UB->PATH->TEMPLATE_->TEMP		= $CFG->dataroot."/template_";
$CFG->UB->PATH->TEMPLATE_->COMPILE	= $CFG->UB->PATH->TEMPLATE_->TEMP."/_compile";
$CFG->UB->PATH->TEMPLATE_->CACHE	= $CFG->UB->PATH->TEMPLATE_->TEMP."/_cache";


// 특정 폴더가 없으면 생성해줘야됨.
if(!is_dir($CFG->UB->PATH->TEMPLATE_->TEMP)) {
	mkdir($CFG->UB->PATH->TEMPLATE_->TEMP, 0777);
}

if(!is_dir($CFG->UB->PATH->TEMPLATE_->COMPILE)) {
	mkdir($CFG->UB->PATH->TEMPLATE_->COMPILE, 0777);
}

if(!is_dir($CFG->UB->PATH->TEMPLATE_->CACHE)) {
	mkdir($CFG->UB->PATH->TEMPLATE_->CACHE, 0777);
}

require_once $CFG->UB->PATH->UTIL."/Common.php";
require_once $CFG->UB->PATH->UTIL."/Javascript.php";
require_once $CFG->UB->PATH->UTIL."/Validate.php";
require_once $CFG->UB->PATH->UTIL."/Parameter.php";
require_once $CFG->UB->PATH->ROOT."/template_config.php";

// ubboard 모듈 ID값
$CFG->UB->UBBOARD = 24;

// 사이트 인덱스에서 사용되는 게시판
$CFG->UB->BOARD = new stdClass();
$CFG->UB->BOARD->NOTICE = 1;
$CFG->UB->BOARD->QNA = 2;
$CFG->UB->BOARD->FAQ = 3;
$CFG->UB->BOARD->FILES = 4;
$CFG->UB->BOARD->SAMPLE = 2395;

$CFG->UB->BOARD->NOTICE_ENG = 5;
$CFG->UB->BOARD->QNA_ENG = 6;
$CFG->UB->BOARD->FAQ_ENG = 7;
$CFG->UB->BOARD->FILES_ENG = 8;


// 조교, 청강 role id정의 해야됨.
// 서울대학교에 특화 되어 있습니다.
$CFG->UB->ROLE = new stdClass();
$CFG->UB->ROLE->PROFESSOR 	= 3;
$CFG->UB->ROLE->STUDENT		= 5;
$CFG->UB->ROLE->ASSISTANT 	= 9;		// 조교
$CFG->UB->ROLE->AUDITOR  	= 10;		// 청강생
$CFG->UB->ROLE->STUDENT_LEAVE = 11;	// 휴학생
$CFG->UB->ROLE->STUDENT_GROUP = array(5, 10, 11);	// 학생으로 판단할 role id 모음
$CFG->UB->ROLE->PROFESSOR_GROUP = array(3, 9);

// 통계에서 사용되는 user_type별 명칭
$CFG->UB->STATS = new stdClass();

// 이값을 변경하면 local/ubion/library/user/lib.php에 LOCAL_UBION_USER_STUDENT 등과 같이 상수도 변경해줘야됨.
$CFG->UB->STATS->USER_TYPE = array(
		10 => '학생'
		,20 => '대학원'
		,21 => '군휴학'
		,30 => '교수'
		,40 => '교직원'
		,50 => '관리자'
		,60 => '조교'
		,99 => '손님'
		,90 => '기타'
);


// 출석부에 표시할 상태 값만 등록해야 함.
$CFG->UB->STATS->WORD = array(
		'20'=>'일반휴학'
		,'21'=>'군휴학'
);

$CFG->UB->STATS->IN_MILITARY = 21; //군휴가  (출석부표시)


// 사용자 상태 : 인덕데 특화
$CFG->UB->USERSTATE = new stdClass();
$CFG->UB->USERSTATE->CODE = new stdClass();
$CFG->UB->USERSTATE->CODE->ABSENCE = 10; //인덕대 재학/재직
$CFG->UB->USERSTATE->CODE->IN_HOME = 20; //일반휴학
$CFG->UB->USERSTATE->CODE->IN_MILITARY = 21; //군휴학
$CFG->UB->USERSTATE->CODE->LEAVEGROUP = array(20,21); //휴학그룹 : 휴학구분이 많아 질 경우 대비
$CFG->UB->USERSTATE->TYPE = array();
$CFG->UB->USERSTATE->TYPE[$CFG->UB->USERSTATE->CODE->ABSENCE]='재학/재직';
$CFG->UB->USERSTATE->TYPE[$CFG->UB->USERSTATE->CODE->IN_MILITARY]='군휴가';
$CFG->UB->USERSTATE->TYPE[$CFG->UB->USERSTATE->CODE->IN_HOME]='가사휴가';



// 기초능력 평가
$CFG->UB->UBBASIC = new stdClass();
$CFG->UB->UBBASIC->PREFIX="UBBASIC_";
$CFG->UB->UBBASIC->PATH = '/11';
$CFG->UB->UBBASIC->DEFAULT_TYPE = "BL_01_01";


// 출석부 관련 설정
// settings에 추가하면 학교 담당자가 기능 사용여부를 결정할수 있기 때문에 config에 명시함.
$CFG->UB->ATTENDANCE = new stdClass;
$CFG->UB->ATTENDANCE->ISONLINE = 1;
$CFG->UB->ATTENDANCE->ISLATE = 0;

/* 학사연동 관련 설정 */
$CFG->UB->HAKSA = new stdClass();
$CFG->UB->HAKSA->AUTH = 'univ';
$CFG->UB->HAKSA->LOG = 1;										// 로그 사용
$CFG->UB->HAKSA->LOGPATH = $CFG->dataroot."/haksalogs"; 		// 로그파일 저장 위치
$CFG->UB->HAKSA->CSVPATH = $CFG->dataroot.'/haksacsv';

// logs 관련 db가 존재하지 않으면 생성함.
if(!is_dir($CFG->UB->HAKSA->LOGPATH)) {
	mkdir($CFG->UB->HAKSA->LOGPATH, 0777);
}

// csv 파일 기록
if(!is_dir($CFG->UB->HAKSA->CSVPATH)) {
	mkdir($CFG->UB->HAKSA->CSVPATH, 0777);
}

//$CFG->UB->HAKSA->PHP = '/usr/local/zend/bin/php';
$CFG->UB->HAKSA->PHP = '/usr/local/php/bin/php';
$CFG->UB->HAKSA->PHPINIPATH = '/usr/local/apache2/conf/php.ini';
$CFG->UB->HAKSA->DEMONLOGFILE = $CFG->UB->HAKSA->LOGPATH.'/demon.log';

$CFG->UB->HAKSA->LOGUSE = array(
							"sync_category"=>"Category synchronization progress log",
							"sync_course"=>"Course synchronization progress log",
							"sync_member"=>"User synchronization progress log",
							"sync_professor"=>"Professor synchronization progress log",
							"sync_student"=>"Student synchronization progress log"
);



// 학사관리에서 사용
/*
$CFG->UB->HAKSA->TERMS = array(
		 array('code'=>'1', 'name'=>'1학기')
		,array('code'=>'S', 'name'=>'여름학기')
		,array('code'=>'2', 'name'=>'2학기')
		,array('code'=>'W', 'name'=>'겨울학기')
);
*/


// 최상위 카테고리명
$CFG->UB->HAKSA->CURRICULUM = '정규강좌';
$CFG->UB->HAKSA->CURRICULUM_IRREGULAR = '비정규강좌';
$CFG->UB->HAKSA->CURRICULUM_IRREGULAR_NUM = 6;
$CFG->UB->HAKSA->CURRICULUM_PRACTICE = '실습강좌';
$CFG->UB->HAKSA->CURRICULUM_CYBEREDU = '열린이러닝';
$CFG->UB->HAKSA->CURRICULUM_CYBEREDU_IDNUMBER = 'elearning';

// 학기 코드
$CFG->UB->HAKSA->SEMESTER_CODE = new stdClass;
$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_1 = '10';
$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_S = '11';
$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_2 = '20';
$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_W = '21';
$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_ECLASS = '70';
$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_EDU = '80';
$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_99 = '99';

// 실제로 사용되는 학기 (selectbox 구현하기 위함)
$CFG->UB->HAKSA->SEMESTER = array();
$CFG->UB->HAKSA->SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_1] = '1학기';
$CFG->UB->HAKSA->SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_S] = '여름학기';
$CFG->UB->HAKSA->SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_2] = '2학기';
$CFG->UB->HAKSA->SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_W] = '겨울학기';
$CFG->UB->HAKSA->SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_99] = $CFG->UB->HAKSA->CURRICULUM_IRREGULAR;

// 학교/기관마다 사용하는 학기 값
$CFG->UB->HAKSA->UNIV_SEMESTER = array();
$CFG->UB->HAKSA->UNIV_SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_1] = '10';
$CFG->UB->HAKSA->UNIV_SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_S] = '11';
$CFG->UB->HAKSA->UNIV_SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_2] = '20';
$CFG->UB->HAKSA->UNIV_SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_W] = '21';
$CFG->UB->HAKSA->UNIV_SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_ECLASS] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_ECLASS;
$CFG->UB->HAKSA->UNIV_SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_EDU] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_EDU;
$CFG->UB->HAKSA->UNIV_SEMESTER[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_99] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_99;

$CFG->UB->HAKSA->UNIV_SEMESTER_SWITCH = array();
$CFG->UB->HAKSA->UNIV_SEMESTER_SWITCH['10'] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_1;
$CFG->UB->HAKSA->UNIV_SEMESTER_SWITCH['11'] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_S;
$CFG->UB->HAKSA->UNIV_SEMESTER_SWITCH['20'] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_2;
$CFG->UB->HAKSA->UNIV_SEMESTER_SWITCH['21'] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_W;
$CFG->UB->HAKSA->UNIV_SEMESTER_SWITCH[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_ECLASS] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_ECLASS;
$CFG->UB->HAKSA->UNIV_SEMESTER_SWITCH[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_EDU] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_EDU;
$CFG->UB->HAKSA->UNIV_SEMESTER_SWITCH[$CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_99] = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_99;


// 학교 시작 연도
$CFG->UB->START_MOODLE = new stdClass();
$CFG->UB->START_MOODLE->YEAR = 2016;
$CFG->UB->START_MOODLE->SEMESTER = $CFG->UB->HAKSA->SEMESTER_CODE->CURRICULUM_1;
