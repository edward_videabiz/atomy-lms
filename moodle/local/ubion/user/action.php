<?php
require '../../../config.php';

$controllder = new \local_ubion\user\MyCourseSetting();

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);
if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}