<?php
require_once '../../../config.php';

$CCourse = \local_ubion\course\Course::getInstance();
$CUser = \local_ubion\user\User::getInstance();
$CMyCourseSetting = new \local_ubion\user\MyCourseSetting();

$pluginname = 'local_ubion';

require_login();

$PAGE->set_url('/local/ubion/user/mycourse_setting.php');
$PAGE->set_context(context_user::instance($USER->id));

$i8n = new stdClass();
$i8n->title = get_string('user_sortorder', $pluginname);
$i8n->show = get_string('screen_view_show', 'local_ubion');
$i8n->hide = get_string('screen_view_hide', 'local_ubion');

$PAGE->set_title($i8n->title);

// 강좌 즐겨찾기 기능을 사용하는지 체크
if (! $CUser->isFavorite()) {
    \local_ubion\base\Common::printNotice(get_string('not_used_favorite', $pluginname), $CFG->wwwroot);
}


// 강좌 정렬은 진행강좌만 변경 가능하기 때문에 진행강좌 목록을 가져옴.
if ($myCourses = $CMyCourseSetting->getCourses()) {
    
    foreach ($myCourses as $mc) {
        $mc->fullname = $CCourse->getName($mc);
        $mc->profs = $CCourse->getProfessorString($mc->id);
        $mc->label = $CCourse->getTypeLabel($mc->course_type, $mc->course_gubun_code);
        
        
        $mc->trclass = '';
        if (empty($mc->cu_visible)) {
            $mc->trclass = 'disabled';
            
        }
    }
    
    
    
    $myCourses = array_values($myCourses);
}

$PAGE->requires->js_call_amd('local_ubion/user', 'mycourse_setting', array('url' => $CFG->wwwroot.'/local/ubion/user/action.php'));

echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->title);


$ctx = new stdClass();
$ctx->courses = $myCourses;

echo $OUTPUT->render_from_template('local_ubion/user_mycourse_setting', $ctx);
echo $OUTPUT->footer();