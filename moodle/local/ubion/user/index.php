<?php
require '../../../config.php';

$y = optional_param("year", null, PARAM_INT);
$s = optional_param("semester", null, PARAM_ALPHANUM);


$i8n = new stdClass();
$i8n->title = get_string('my_courses', 'local_ubion');
$i8n->year = get_string('year', 'local_ubion');
$i8n->semester = get_string('semester', 'local_ubion');
$i8n->course_name = get_string('course_name', 'local_ubion');

$PAGE->set_url('/local/ubion/user/index.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($i8n->title);
$PAGE->set_heading($i8n->title);

require_login();


$PAGE->navbar->add($i8n->title, $PAGE->url->out());

$CCourse = \local_ubion\course\Course::getInstance();
$CRegular = \local_ubion\course\Regular::getInstance();

$currentSemester = $CCourse->getCurrentSemester();

list($y, $lmsStartYear, $csmsStartYear) = $CCourse->getAllowYearParameter($y, $currentSemester);
list($s, $lmsStartSemester, $csmsStartSemester) = $CCourse->getAllowSemesterParameter($s, $currentSemester);

$years = range($lmsStartYear, date('Y'));
$semesters = $CCourse->getSemesters();

$filters = array(
    'y' => $y
    ,'s' => $s
);

if ($y >= $csmsStartYear && $s >= $csmsStartSemester) {
    $table = new \local_ubion\user\RegularTable($filters);
} else {
    $table = new \local_ubion\user\TransferTable($filters);
}

$baseurl = new moodle_url('/local/ubion/user/index.php', $filters);
$table->define_baseurl($baseurl);

echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->title, 2);


echo '<div class="course-regular">';
echo    '<form class="form-horizontal form-search well">';
echo        '<div class="form-group mb-0">';
echo            '<label class="control-label col-sm-3">'.$i8n->year.' / '.$i8n->semester.'</label>';
echo            '<div class="col-sm-9 form-inline">';
echo                '<select name="year" class="form-control mr-1 mb-1 mb-sm-0">';
foreach ($years as $yr) {
    $selected = ($yr == $y) ? 'selected="selected"' : '';
    echo '<option value="'.$yr.'" '.$selected.'>'.$yr.'</option>';
}
echo                '</select>';
echo                '<select name="semester" class="form-control mb-1 mb-sm-0">';
foreach ($semesters as $sCode => $sValue) {
    $selected = ($sCode == $s) ? 'selected="selected"' : '';
    
    echo '<option value="'.$sCode.'" '.$selected.'>'.$sValue.'</option>';
}
echo                '</select>';
echo                ' <button type="submit" class="btn btn-default">'.get_string('search').'</button>';
echo            '</div>';
echo        '</div>';
echo    '</form>';

echo    '<div class="course-lists well">';
// 한학기에 100개씩 될일은 없을듯.
echo        $table->out(100, false);
echo    '</div>';
echo '</div>';

echo $OUTPUT->footer();