define(['jquery', 'core/modal_factory', 'core/modal_events', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate'], function($, ModalFactory, ModalEvents) {
	var asiteMember = {};
	
	asiteMember.index = function(url, optionAllString, enrolUrl) {
		
		institution = $(".form-control-institution");
		department = $(".form-control-department");
		
		if (institution.val() == optionAllString) {
			department.prop('disabled', true);
		} else {
			department.prop('disabled', false);
		}
		
		institution.change(function() {
			value = $(this).val();
			
			department.find('option').remove();
			department.append('<option value="'+optionAllString+'">All</option>');
			
			if (value != optionAllString) {
				department.prop('disabled', false);
				
				$.ajax({
					 data: param = coursemostype+'=departments&institution='+value
					,url : url
					,success:function(data){
						if(data.code == mesg.success) {
							$.each(data.depts, function(idx, l) {
								department.append('<option value="'+l.dept_code+'">'+l.department+'</option>');
							});
							// 
						} else {
							alert(data.msg);
						}
					}
					,error: function(xhr, option, error){
				      alert(xhr.status + ' : ' + error);
				  	}
				});
			} else {
				department.prop('disabled', true);
			}
		});
		
		
		$(".btn-user-default").click(function() {
			userid = $(this).data('id');
			
			if (userid > 0) {
				$.ajax({
					 data: param = coursemostype+'=userDetail&id='+userid
					,url : url
					,success:function(data){
						if(data.code == mesg.success) {
							$("#modal-user-detail .modal-title .fullname").text(data.fullname);
							$("#modal-user-detail .modal-body").html(data.html);
							$("#modal-user-detail").modal('show');
						} else {
							alert(data.msg);
						}
					}
					,error: function(xhr, option, error){
				      alert(xhr.status + ' : ' + error);
				  	}
				});	
			} else {
				alert('userid값이 존재하지 않습니다.');
			}
		});
		
		
		// 수강강좌
		$(".btn-enrol").click(function() {
			userid = $(this).data('id');
			
			window.open(enrolUrl+'?id=' + userid, 'asite_userenrol_' + userid, 'width=600, height=800, scrollbars=1');
			
			/*
			if (userid > 0) {
				
				$.ajax({
					 data: param = coursemostype+'=userEnrol&id='+userid
					,url : url
					,success:function(data){
						
						if(data.code == mesg.success) {
							$("#modal-user-detail .modal-title .fullname").text(data.fullname);
							$("#modal-user-detail .modal-body").html(data.html);
							$("#modal-user-detail").modal('show');
						} else {
							alert(data.msg);
						}
					}
					,error: function(xhr, option, error){
				      alert(xhr.status + ' : ' + error);
				  	}
				});	
			} else {
				alert('userid값이 존재하지 않습니다.');
			}
			*/
		});
		
		// as login
		$(".btn-aslogin").click(function(e) {
			
			if (!confirm(M.util.get_string('as_login', 'local_ubion', $(this).data('username')))) {
				e.preventDefault();
				return false;
			}
		});
	};
	
	asiteMember.user_batch = function() {
	    $("#add_form").validate({
	       submitHandler : function(form) {
	           $("#add_form .btn-submit").prop("disabled", true); 
               showSubmitProgress();
               form.submit();
	       } 
	    });
	};
	
	asiteMember.admin = function(url) {
		$( ".btn-admin-delete" ).each(function(index) {
			$(this).on("click", function(){
				trigger = $(this);
				userid = trigger.data('userid');
				fullname = trigger.data('fullname');
				var answer = window.confirm(M.util.get_string('admin_delete_confirm', 'local_ubion', fullname));
				if (answer) {
					$.ajax({
						data : param = coursemostype + '=adminDelete&userid=' + userid,
						url : url,
						success : function(data) {
							if (data.code == mesg.success) {
								location.reload();
							} else {
								alert("CODE: " + data.code + " : " + data.msg);
							}
						},
						error : function(xhr, option, error) {
							alert(xhr.status + ' : ' + error);
						}
					});
				}
			});
		});
		// $(".btn-admin-delete").each(function() {
		// 	trigger = $(this);
		// 	userid = trigger.data('userid');
		// 	fullname = trigger.data('fullname');
		// 	// ModalFactory.create({
		// 	//     type: ModalFactory.types.SAVE_CANCEL,
		// 	// 	title: fullname,
		// 	// 	body: M.util.get_string('admin_delete_confirm', 'local_ubion', fullname)
		// 	// }, trigger)
		// 	// .done(function(modal) {
		// 	// 	// text문구 변경
		// 	// 	var footer = $(modal.footer);
				
		// 	// 	var saveButton = footer.find('[data-action="save"]');
		// 	// 	saveButton.addClass('btn-danger').text(M.util.get_string('delete', 'core'));
				
		// 	//     modal.getRoot().on(ModalEvents.save, function(e) {
		// 	//         e.preventDefault();
		// 	//         $.ajax({
		// 	// 			data : param = coursemostype + '=adminDelete&userid=' + userid,
		// 	// 			url : url,
		// 	// 			success : function(data) {
		// 	// 				if (data.code == mesg.success) {
		// 	// 					location.reload();
		// 	// 				} else {
		// 	// 					alert("CODE: " + data.code + " : " + data.msg);
		// 	// 				}
		// 	// 			},
		// 	// 			error : function(xhr, option, error) {
		// 	// 				alert(xhr.status + ' : ' + error);
		// 	// 			}
		// 	// 		});
		// 	//     });
		// 	// });
		// });
		
		$(".btn-admin-add").click(function() {
			$("#modal-usersearch").modal('show');
			
			$("#modal-usersearch input[name='keyword']").focus();
		});

		$("#modal-usersearch .form-add-user").validate({
			submitHandler: function(form) {
				$.ajax({
					data : $(form).serialize(),
					url : url,
					success : function(data) {
						if (data.code == mesg.success) {
							$.ajax({
								data : param = coursemostype + '=adminInsert&userid=' + data.userid,
								url : url,
								success : function(data) {
									if (data.code == mesg.success) {
										location.reload();
									} else {
										alert(data.msg);
									}
								},
								error : function(xhr, option, error) {
									alert(xhr.status + ' : ' + error);
								}
							});
						} else {
							alert('Failed code: ' + data.code + '\nMessage: ' + data.msg);
						}
					},
					error : function(xhr, option, error) {
						alert(xhr.status + ' : ' + error + '\r\nMay be your username or email existed.');
					}
				});

				return false;
			}
		})
		
		$("#modal-usersearch .form-user-search").validate({
			submitHandler: function(form) {
				$.ajax({
					data : $(form).serialize(),
					url : url,
					success : function(data) {
						if (data.code == mesg.success) {
							$("#modal-usersearch .user-lists").removeClass('d-none');
							
							$("#modal-usersearch .user-lists .table-userlists tbody").empty().html(data.html);
							
							$("#modal-usersearch .paging").html(data.paging);
							

							$( "#modal-usersearch .user-lists .table-userlists .btn-add" ).each(function(index) {
								$(this).on("click", function(){
									trigger = $(this);
									userid = trigger.data('id');
									fullname = trigger.data('fullname');
									var yes = window.confirm(M.util.get_string('admin_confirm', 'local_ubion', fullname));
									if (yes) {
										$.ajax({
											data : param = coursemostype + '=adminInsert&userid=' + userid,
											url : url,
											success : function(data) {
												if (data.code == mesg.success) {
													location.reload();
												} else {
													alert(data.msg);
												}
											},
											error : function(xhr, option, error) {
												alert(xhr.status + ' : ' + error);
											}
										});
									}
								});
							});
							
							// $("#modal-usersearch .user-lists .table-userlists .btn-add").each(function() {
							// 	var trigger = $(this);
								
							// 	userid = trigger.data('id');
							// 	fullname = trigger.data('fullname');
								
							// 	ModalFactory.create({
							// 	    type: ModalFactory.types.SAVE_CANCEL,
							// 		title: fullname,
							// 		body: M.util.get_string('admin_confirm', 'local_ubion', fullname)
							// 	}, trigger)
							// 	.done(function(modal) {
									
							// 		// text문구 변경
							// 		var footer = $(modal.footer);
									
							// 		var saveButton = footer.find('[data-action="save"]');
							// 		saveButton.text(M.util.get_string('add', 'core'));
									
							// 	    modal.getRoot().on(ModalEvents.save, function(e) {
							// 	        e.preventDefault();
								        
							// 	        $.ajax({
							// 				data : param = coursemostype + '=adminInsert&userid=' + userid,
							// 				url : url,
							// 				success : function(data) {
							// 					if (data.code == mesg.success) {
							// 						location.reload();
							// 					} else {
							// 						alert(data.msg);
							// 					}
							// 				},
							// 				error : function(xhr, option, error) {
							// 					alert(xhr.status + ' : ' + error);
							// 				}
							// 			});
							// 	    });
							// 	});
							// });
						} else {
							alert(data.msg);
						}
					},
					error : function(xhr, option, error) {
						alert(xhr.status + ' : ' + error);
					}
				});
				
				return false;
			}
		})
	};
	
	return asiteMember;
});