define(['jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate'], function($) {
    var asiteVN = {};
    
    asiteVN.index = function(courseid, url) {
    	
    	$(".asite-vn .checkbox-all").click(function() {
    		var checked = $(this).is(":checked");
    		
    		$(".asite-vn .form-validate input[name='userids[]']").not(':disabled').each(function(i) {
				this.checked = checked;
    		});
    	});
    	
    	$(".asite-vn .form-checkusers").validate({
    		submitHandler : function(form) {
    			
    			if (parseInt($(form.complete).val()) >= 0) {
    				if($(".asite-vn .form-checkusers input[name='userids[]']:checked").length > 0){
    					$(".asite-vn .form-checkusers button[type='submit']").attr('disabled', 'disabled');
    					
    					showSubmitProgress();
    					form.submit();
    				} else {
    					alert(M.util.get_string('min_user', 'local_ubion'));
    					// location.hash = 'form-send';
    					return false;
    				}
    			} else {
    				alert(M.util.get_string('not_selected_completion', 'local_vn'));
    				return false;	
    			}
    			
    		}
    	});
    	
        $(".asite-vn .btn-log").click(function() {
        	userid = $(this).data('userid');
        	
        	$.ajax({
				 data: param = coursemostype+'=userAttempt&courseid='+courseid+'&userid='+userid
				,url : url
				,success:function(data){
					if(data.code == mesg.success) {
						$("#modal-attempt .modal-title").html(data.fullname);
						$("#modal-attempt .modal-body .table-body").empty();
						$("#modal-attempt .modal-body .table-body").html(data.html);
						
						$("#modal-attempt").modal('show');
						
						
						// .btn-activity-log modal 이벤트 부여
						$(".asite-vn .btn-activity-log").click(function() {
							var attemptid = $(this).data('attemptid');
							
							$.ajax({
								 data: param = coursemostype+'=userAttemptActivity&attemptid=' + attemptid 
								,url : url
								,success:function(data){
									if(data.code == mesg.success) {
										
										$("#modal-attempt-activity .modal-title").html(data.title);
										$("#modal-attempt-activity .modal-body .table-body").empty();
										$("#modal-attempt-activity .modal-body .table-body").html(data.html);
										
										$("#modal-attempt-activity").modal('show');			
									}
								}
							});
							
								
						});
					} else {
						alert(data.msg);
					}
				}
				,error: function(xhr, option, error){
			      alert(xhr.status + ' : ' + error);
			  	}
			});
        });
    }
    
    return asiteVN;
});