define(['jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate', 'theme_coursemos/jquery-sortable'], function($) {
    var asiteClassify = {};
    
    asiteClassify.index = function(url) {
        
        $(".form-search .form-control-coursetype").change(function() {
            $(".form-search").submit();    
        });
        
        $(".btn-add-classify").click(function() {
            // 신규 추가시에는 활성화
            $("#modal-classify form select[name='course_type']").removeAttr('disabled');
            
            $("#modal-classify").modal('show'); 
        });
         
         
        $(".btn-update").click(function() {
            
            id = $(this).closest('tr').data('id');
             
            $.ajax({
                 data: param = coursemostype+'=view&id='+id
                ,url : url
                ,success:function(data){
                    if(data.code == mesg.success) {
                        $("#modal-classify .modal-title .modal-title-changetype").text('변경');
     
     
                        $("#modal-classify form input[name='"+coursemostype+"']").val('update');
                         
                        coursemosBindElement($("#modal-classify form"), data.result);
                        
                        // course_type은 비활성화 되어있어야 함.
                        $("#modal-classify form select[name='course_type']").attr('disabled', 'disabled');
                         
                        // value값은 serialize되어 있기 때문에 unserialize해서 수동으로 바인딩 해줘야됨.
                        $.each(data.result.valueUnserialize, function(langKey, langValue) {
                            $("#modal-classify form [name='lang-"+langKey+"']").val(langValue);
                        });
                         
                         
                    } else {
                        alert(data.msg);
                    }
                }
                ,error: function(xhr, option, error){
                    alert(xhr.status + ' : ' + error);
                }
            });
             
            $("#modal-classify").modal('show');
        });
         
         
        $(".btn-delete").click(function() {
            tr = $(this).closest('tr');
             
            //classify_delete_confirm 
            id = tr.data('id');
             
            name = tr.find('.classify-name').text();
             
            if (confirm(M.util.get_string('classify_delete_confirm', 'local_ubion', name))) {
                $.ajax({
                    data: param = coursemostype+'=delete&id='+id
                   ,url : url
                   ,success:function(data){
                       if(data.code == mesg.success) {
                           location.reload();
                       } else {
                           alert(data.msg);
                       }
                   }
                   ,error: function(xhr, option, error){
                       alert(xhr.status + ' : ' + error);
                   }
               });
            }
             
            return false;
        });
         
         
        $(".all_checked").click(function () {
            checked = $(this).is(':checked');
    
            $(".asite-course .form-control-clsf").prop('checked', checked);
        });
         
         
        $(".btn-checked-delete").click(function () {
            selectCount = $(".asite-course .form-control-clsf:checked").length;
    
            if (selectCount > 0) {
                if (confirm(M.util.get_string('classify_delete_confirm_multiple', 'local_ubion', selectCount))) {
                    $(".asite-course .form-classify").submit();
                }
            } else {
                alert(M.util.get_string('classify_no_checked', 'local_ubion'));
            }
    
            return false;
        });
         
         
        // sortorder
        classifies = $(".form-classify .table-classify").sortable({
             containerSelector: 'table'
            ,itemPath: '> tbody'
            ,itemSelector: 'tr'
            ,handle : '.moveicon'
            ,placeholder: '<tr class="placeholder"/>'
            ,onDrop: function  ($item, container, _super) {
                _super($item, container);
                 
                data = classifies.sortable("serialize").get(0);
                 
                jsonString = JSON.stringify(data);
                     
                $.ajax({
                     data: param = coursemostype+'=sortorder&json='+jsonString
                    ,url : url
                    ,success:function(data){
                        if(data.code == mesg.success) {
                             
                        } else {
                            alert(data.msg);
                        }
                    }
                    ,error: function(xhr, option, error){
                      alert(xhr.status + ' : ' + error);
                    }
                });
            }
             
        });
         
        $(".form-validate").validate();
    };
    
    
    return asiteClassify;
});