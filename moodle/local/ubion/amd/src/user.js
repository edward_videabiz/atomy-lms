define(['jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery-sortable'], function($) {
    var user = {};
    
    user.mycourse_setting = function(url) {
        
        $(".local-ubion-user-sortorder .table-user-sortorder").sortable({
            containerSelector: 'table',
             itemPath: '> tbody',
             itemSelector: 'tr',
             handle: '.i-move',
             placeholder: '<tr class="placeholder"/>',
             onDrop: function  ($item, container, _super) {
                 _super($item, container);
                 
                 var item = "";
                 var division = "";
                
                 $(".my-course-lists tr").each(function() {
                    // id값 뽑아내기
                    courseid = $(this).data("courseid");
                    if(courseid) {  
                        item += division + courseid;
                        division = "^";
                    }
                 });
                
                 param = coursemostype + "=courseSortable&courseidx="+item
                
                 $.ajax({
                     url: url
                    ,data: param
                    ,type:'post'
                    ,success:function(data){
                        if(data.code == mesg.success) {
                            
                        } else {
                            alert(data.msg);
                        }
                    }
                 });     
                 
                 
             }
        });
    }
    
    return user;
});