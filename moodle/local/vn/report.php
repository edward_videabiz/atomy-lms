<?php
use local_ubion\base\Common;

require_once '../../config.php';

$id = required_param('id', PARAM_INT);
$userid = optional_param('userid', $USER->id, PARAM_INT);


$CCourse = \local_ubion\course\Course::getInstance();
$course = $CCourse->getCourse($id);


$pluginname = 'local_vn';
$i8n = get_strings(array(
    'report_completion'
), $pluginname);


## 페이지 셋팅
$context= context_course::instance($id);
$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);

// 주소 셋팅
$basePath = $CFG->wwwroot.'/local/vn';
$urlParam = array('id' => $id);

$PAGE->set_url($basePath.'/report.php', $urlParam);
$PAGE->set_pagelayout('incourse');
$PAGE->set_heading($course->fullname);
$PAGE->set_title($course->fullname.' : '.$i8n->report_completion);
$PAGE->navbar->add($i8n->report_completion, $PAGE->url->out());

$PAGE->requires->js_call_amd('local_vn/report', 'index', array( 'id' => $id ));


if (!$CCourse->isProfessor($id, $context)) {
    $userid = $USER->id;
}

$ctx = new stdClass();
$ctx->attempts = null;

$CVnCourse = \local_vn\Course::getInstance();
// 시도한 회차 정보 가져오기
if ($attempts = $CVnCourse->getAttempts($id, $userid)) {
    
    $period = $CVnCourse->getConfig()->complete_period;
    
    foreach ($attempts as $as) {
        $as->start = Common::getUserDate($as->timecreated);
        $as->end = Common::getUserDate($as->timecreated + $period);
        $timecompleteText = '-';
        if (!empty($as->timecomplete)) {
            $timecompleteText = '<span class="text-primary font-weight-bold">'.Common::getUserDate($as->timecomplete).'</span>';
        }
        $as->timecompleteText = $timecompleteText;
    }   
    
    $ctx->attempts = array_values($attempts);
    
}




echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->report_completion, 2, 'pagetitle');
echo $OUTPUT->render_from_template('local_vn/report', $ctx);
echo $OUTPUT->footer();