<?php
$observers = array(
    // 강좌 삭제 (course_ubion에 레코드 추가는 강좌 포맷에서 이뤄지기 때문에 따로 이벤트 처리 하지 않습니다.)
    array(
        'eventname' => '\core\event\user_enrolment_created',
        'callback' => '\local_vn\Observer::user_enrolment_created'
    ),
    array(
        'eventname' => '\core\event\user_enrolment_deleted',
        'callback' => '\local_vn\Observer::user_enrolment_deleted'
    ),
    array(
        'eventname' => '\core\event\course_completed',
        'callback' => '\local_vn\Observer::course_completed'
    ),
    array(
        'eventname' => '\local_vn\event\course_complete_deleted',
        'callback' => '\local_vn\Observer::course_complete_deleted'
    ),
    array(
        'eventname' => '\core\event\course_module_completion_updated',
        'callback' => '\local_vn\Observer::course_module_completion_updated'
    )
);
