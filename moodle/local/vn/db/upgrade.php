<?php

function xmldb_local_vn_upgrade($oldversion)
{
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2019051001) {
        
        // Define table local_vn_complete_failed to be created.
        $table = new xmldb_table('local_vn_complete_failed');
        
        // Adding fields to table local_vn_complete_failed.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('attempt', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecomplete', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('attemptid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        
        // Adding keys to table local_vn_complete_failed.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        
        // Adding indexes to table local_vn_complete_failed.
        $table->add_index('courseid', XMLDB_INDEX_NOTUNIQUE, array('courseid'));
        $table->add_index('timecreated', XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
        $table->add_index('attemptid', XMLDB_INDEX_NOTUNIQUE, array('attemptid'));
        
        // Conditionally launch create table for local_vn_complete_failed.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
        // Vn savepoint reached.
        upgrade_plugin_savepoint(true, 2019051001, 'local', 'vn');
    }
    
    
    
    
    return true;
}