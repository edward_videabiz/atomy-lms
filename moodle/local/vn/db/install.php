<?php
defined('MOODLE_INTERNAL') || die();

function xmldb_local_vn_install() {
    
    global $CFG;
    require_once $CFG->libdir.'/enrollib.php';
    
    // 등록 만료 조치 => 강좌에서 제명
    set_config('expiredaction', ENROL_EXT_REMOVED_UNENROL, 'enrol_manual');
    
    // 등록 만료 통지를 보내는 시간
    set_config('expirynotifyhour', 0, 'enrol_manual');
    
    // 등록이 만료되기 전에 통지
    set_config('expirynotify', 0, 'enrol_manual');
    
    // 통지 임계값
    set_config('expirythreshold', 0, 'enrol_manual');
    
    
    return true;
}


