<?php
defined('MOODLE_INTERNAL') || die();

$tasks = array(
    array(
        'classname' => '\local_vn\task\APIReCall',
        'blocking' => 0,
        'minute' => '*',
        'hour' => '*/1',
        'day' => '*',
        'dayofweek' => '*',
        'month' => '*'
    ),
);