define(['jquery', 'theme_coursemos/bootstrap', 'local_vn/jquery.circliful', 'theme_coursemos/timer.jquery'], function($) {
    var user_progress = {};
    
    user_progress.index = function(id, progress, timesecond, timeformat) {
    	window.resizeTo(800, 600);
    	
    	$('.progress-circle').circliful();
    	
    	
    	$('#accordion .collapse').on('show.bs.collapse', function(){
    		// $(this).parent().addClass('active');
    		$(this).parent().find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
    	}).on('hide.bs.collapse', function(){
    		// $(this).parent().removeClass('active');
    		$(this).parent().find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
    	});
    	
    	
    	var progressTimer = $(".user-progress .page-title .timer").timer({
    		countdown: true
    		,duration: timesecond
    		,format : timeformat
    		,callback: function() {
    			user_progress.nextActivity(id);
    		}
    	});
    	
    	
    	$(".user-progress .form-control-timer").click(function() {
    		var checked = $(this).is(':checked');
    		
    		// 체크가 되어 있으면 멈춰야됨.
    		
    		if (checked) {
    			progressTimer.timer('pause');
    		} else {
    			progressTimer.timer('resume');	
    		}
    	});
    	
    	$(".user-progress .btn-automove").click(function() {
    		user_progress.nextActivity(id);
    	});
    };
    
    user_progress.nextActivity = function(id) {
    	
    	$.ajax({
			 data: param = coursemostype+'=nextActivityURL&courseid=' + id 
			,url : 'action.php'
			,success : function(data){
				// console.log(data);
				if (data.code == mesg.success) {
					location.href = data.href;
				} else if (data.code == '101') {
					alert(data.msg);
					location.href = data.href;
					
				} else {
					alert(data.msg);
				}
			}
		});
    }
    
    return user_progress;
});