define(['jquery', 'theme_coursemos/bootstrap'], function($) {
    var report = {};
    
    report.index = function(courseid) {
    	
		// .btn-activity-log modal 이벤트 부여
		$(".local-vn .btn-activity-log").click(function() {
			var attemptid = $(this).data('attemptid');
			
			$.ajax({
				 data: param = coursemostype+'=userAttemptActivity&attemptid=' + attemptid 
				,url : 'action.php'
				,success:function(data){
					if(data.code == mesg.success) {
						
						$("#modal-attempt-activity .modal-title").html(data.title);
						$("#modal-attempt-activity .modal-body .table-body").empty();
						$("#modal-attempt-activity .modal-body .table-body").html(data.html);
						
						$("#modal-attempt-activity").modal('show');			
					}
				}
			});
			
		});
    }
    
    return report;
});