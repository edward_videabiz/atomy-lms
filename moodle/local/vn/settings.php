<?php
defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $pluginname = 'local_vn';

    $ADMIN->add('localplugins', new admin_category('local_vn', get_string('pluginname', $pluginname)));

    // ##############
    // ## 기본설정 ##
    // ##############
    $temp = new admin_settingpage('local_vn_default', get_string('setting_default', $pluginname));

    // 강좌번호
    $name = $pluginname . '/courseid';
    $title = new lang_string('setting_courseid', $pluginname);
    $description = '';
    $default = null;
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    // 오프라인 강좌번호
    $name = $pluginname . '/courseid_offline';
    $title = new lang_string('setting_courseid_offline', $pluginname);
    $description = '';
    $default = null;
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    // 수강신청 이후 완료해야되는 기간
    $name = $pluginname . '/complete_period';
    $title = new lang_string('setting_complete_period', $pluginname);
    $description = new lang_string('setting_complete_period_desc', $pluginname);
    // 초단위로 입력해야됨 (기본 31일로 설정)
    $default = 2678400;
    $setting = new admin_setting_configduration($name, $title, $description, $default);
    $temp->add($setting);
    
    // 자동 이동 시간
    $name = $pluginname . '/automove_second';
    $title = new lang_string('setting_automove_second', $pluginname);
    $description = new lang_string('setting_automove_second_desc', $pluginname);
    // 초단위로 입력해야됨 (기본 30초 설정)
    $default = 30;
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $ADMIN->add($pluginname, $temp);
    
    // ##############
    // ## API 설정 ##
    // ##############
    $temp = new admin_settingpage('local_vn_api', get_string('setting_api', $pluginname));
    
    // API 주소
    $name = $pluginname . '/apiurl';
    $title = new lang_string('setting_apiurl', $pluginname);
    $description = '';
    $default = 'https://www.atomy.com:449';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    // API 토큰
    $name = $pluginname . '/apitoken';
    $title = new lang_string('setting_apitoken', $pluginname);
    $description = '';
    $default = 'N4WPKJOKLP3LLOW6MABV3YVTOULG2196';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $ADMIN->add($pluginname, $temp);
}