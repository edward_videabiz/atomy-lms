<?php
require_once '../../config.php';

$id = required_param('id', PARAM_INT);

$CCourse = \local_ubion\course\Course::getInstance();
$course = $CCourse->getCourse($id);

$pluginname = 'local_vn';
$i8n = get_strings(array(
    'learning_status'
), $pluginname);

## 페이지 셋팅
$context= context_course::instance($id);
$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);

// 주소 셋팅
$basePath = $CFG->wwwroot.'/local/vn';
$urlParam = array('id' => $id);

$PAGE->set_url($basePath.'/report.php', $urlParam);
$PAGE->set_pagelayout('popup');
$PAGE->set_heading($course->fullname);
$PAGE->set_title($course->fullname.' : '.$i8n->learning_status);



$CVNCourse = \local_vn\Course::getInstance();

$modinfo = get_fast_modinfo($course);
list ($sections, $completionModuleCount, $completionProgress, $currentCM) = $CVNCourse->getSections($course);

// 자동 이동 시간을 1시간 이상 설정할일이 없을듯하므로 강제로 %S로 고정함.
$timeformat = '%S';
$timesecond = $CVNCourse->getConfig()->automove_second ?? 30;


//error_log(print_r($sections, true));
$ctx = new stdClass();
$ctx->progress = ($completionModuleCount > 0) ? round($completionProgress / $completionModuleCount, 1) : 0;
$ctx->sections = $sections;
$ctx->pageTitle = null;
$ctx->isComplete = false;

// 수동으로 이수가 완료되었거나, 더이상 학습할 항목이 없는 경우
$timecompleted = null;
$isOverriden = false;
if ($attempts = $CVNCourse->getAttempts($id, $USER->id)) {
    // 가장 마지막 항목으로 비교해야됨. 
    $attempt = end($attempts);
    $timecompleted = $attempt->timecomplete;
    $isOverriden = $attempt->overriden;
}

// 진행해야될 항목이 존재하는 경우
if (!empty($currentCM)) {
    $ctx->pageTitle = get_string('automove', $pluginname, array('timer' => $timesecond, 'name' => $modinfo->get_cm($currentCM)->name));
}

// 크론 주기에 의해서 timecompleted값이 아직 갱신이 안되어 있을수도 있기 때문에
// currentCM(이수해야될 학습활동)이 빈값인지도 추가적으로 체크해야됨.
if (!empty($timecompleted) || empty($currentCM)) {
    $ctx->isComplete = true;
    $ctx->progress = 100;
    $ctx->pageTitle = get_string('automove_complete', $pluginname);
    
    if ($isOverriden) {
        $ctx->pageTitle .= ' <span class="text-danger">('.get_string('automove_complete_overriden', $pluginname).')</span>';
    }
}
$PAGE->requires->js_call_amd('local_vn/user_progress', 'index', array(
    'id' => $id, 
    'progress' => $ctx->progress, 
    'timesecond' => $timesecond,
    'timeformat' => $timeformat
));

$PAGE->requires->strings_for_js(array(
    'numseconds'
), 'core');

echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_vn/user_progress', $ctx);
echo $OUTPUT->footer();