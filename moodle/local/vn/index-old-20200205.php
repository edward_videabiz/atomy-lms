<?php
require_once '../../config.php';
require_once $CFG->libdir.'/completionlib.php';


// header 및 왼쪽 메뉴 없애주기
$PAGE->add_body_classes([
    'coursemos-menu-none'
]);

$id = optional_param('id', null, PARAM_INT);

if (!isloggedin()) {
    redirect(get_login_url());
}

$userid = $USER->id;

$CVNCourse = \local_vn\Course::getInstance();
$vnConfig = $CVNCourse->getConfig();

// 온라인 강좌
$onCourseID = $vnConfig->courseid ?? null;

// 퀴즈
$offCourseID = $vnConfig->courseid_offline ?? null;

// id값이 SITEID로 넘어오는 경우 강제로 온라인 강좌로 이동
if ($id == SITEID) {
    $id = $onCourseID;
}


// 자동 이동 시간을 1시간 이상 설정할일이 없을듯하므로 강제로 %S로 고정함.
$timeformat = '%S';
$timesecond = $vnConfig->automove_second ?? 30;


$ctx = new stdClass();
$ctx->isTab = false;

// 온라인 강좌를 이수 완료했거나, 오프라인 사용자라면 탭이 활성화 되어야 함.
$userUbion = \local_ubion\user\User::getInstance()->getUbion();

// 화면에 표시되어야 할 강좌
$courseid = $onCourseID;

$isOffUser = false;
$onoff = $userUbion->onoff ?? null;
if (!empty($onoff)) {
    $isOffUser = true;
}


if ($isOffUser) {
    // 오프라인 사용자라면 무조건 표시    
    $ctx->isTab = true;
    $courseid = $offCourseID;
} else {
    // 온라인이면 lmsedufinishdate값으로 판단
    $checkCompleteDate = $userUbion->lmsedufinishdate ?? null;

    $isOnlineComplete = false;
    if (!empty($checkCompleteDate)) {
        $isOnlineComplete = true;
    } else {
        $params = array(
            'userid'    => $userid,
            'course'    => $onCourseID
        );

        $ccompletion = new completion_completion($params);
        $isOnlineComplete = $ccompletion->is_complete() ;
    }

    // 온라인 강좌 이수 완료 했으면 퀴즈 강좌가 자동 선택되어야 함.
    if ($isOnlineComplete) {
        $ctx->isTab = true;
        $courseid = $offCourseID;
    }
}


// 파라메터로 강좌가 전달이 된 경우 파라메터 값이 더 우선시 되어야 함.
if (!empty($id)) {
    $courseid = $id;
}

// manual 계정은 테스트 계정이기 때문에
// 강좌에 참여자로 등록이 안되어있으면 참여자로 등록시켜줘야됨.
if (isloggedin() && $USER->auth == 'manual') {
    $context= context_course::instance($courseid);
    if (!is_enrolled($context)) {
        // manual 사용자는 기간 제한 없음
        $CVNCourse->setEnrol($courseid, $userid);
    }
}


// 오프라인 강좌가 선택된 경우라면 온라인 강좌를 이수했는지 확인 해야됨.
if ($courseid == $offCourseID) {
    // 온라인 강좌 이수를 완료하지 않은 경우
    if (! $CVNCourse->isOnlineCourseComplete($userid, $userUbion)) {
        // redirect($CFG->wwwroot.'/local/vn/index.php', get_string('online_course_not_complete', 'local_vn'));
        \local_ubion\base\Javascript::getAlertMove(get_string('online_course_not_complete', 'local_vn'), $CFG->wwwroot.'/local/vn/index.php');
    }
}


// 선택한 강좌를 이수 완료했는지 확인
$isComplete = false;

// 온라인 사용자면 교육 완료일, 오프라인 사용자라면 이수 완료일로 체크
$checkColumnName = ($courseid == $onCourseID) ? 'lmsedufinishdate' : 'lmseducertdate';

// 완료일이 지정되어 있지 않으면 강좌 이수 완료가 되었는지 확인
if (empty($userUbion->$checkColumnName)) {
    // 강좌 이수 여부로 수료 여부 판단
    $isComplete = $CVNCourse->isCourseComplete($courseid, $userid);

    // 선택된 강좌가 온라인 강좌인데
    // 이수를 완료하지 않았어도 오프라인 사용자라면 이수 완료한것처럼 처리되어야 함.
    if ($courseid == $onCourseID && !$isComplete && $isOffUser) {
        $isComplete = true;
    }
} else {
    // 이수 완료일이 설정되어 있기 때문에 수료 완료로 판단
    $isComplete = true;
}


$CCourse = \local_ubion\course\Course::getInstance();
$course = $CCourse->getCourse($courseid);

$pluginname = 'local_vn';
$i8n = get_strings(array(
    'learning_status'
), $pluginname);

## 페이지 셋팅
$context= context_course::instance($courseid);
$PAGE->set_context($context);
$PAGE->set_course($course);
require_login($course);


// 주소 셋팅
$basePath = $CFG->wwwroot.'/local/vn';

$PAGE->set_url($basePath.'/index.php');
$PAGE->set_pagelayout('base');
$PAGE->set_heading($course->fullname);
$PAGE->set_title($course->fullname.' : '.$i8n->learning_status);


// 사용자 정보
$ctx->userinfo = new stdClass();
$ctx->userinfo->username = $USER->username;
$ctx->userinfo->fullname = fullname($USER);
$ctx->userinfo->enrolPeriod = null;


if ($courseid == $onCourseID) {
    if ($enrolInfo = $CVNCourse->getEnrolInfo($courseid, $userid)) {
        $enrolPeriod = '';

        // ago 표시하기 위한 변수
        $timestart = time();
        if (empty($enrolInfo->timestart)) {
            $enrolPeriod .= get_string('study_nolimit', 'local_vn');
        } else {
            $enrolPeriod .= \local_ubion\base\Common::getUserDate($enrolInfo->timestart);
            $timestart = $enrolInfo->timestart;
        }

        $enrolPeriod .= ' ~ ';

        if (empty($enrolInfo->timeend)) {
            $enrolPeriod .= get_string('study_nolimit', 'local_vn');
        } else {
            $enrolPeriod .= \local_ubion\base\Common::getUserDate($enrolInfo->timeend);

            /* x일 종료는 향후에 기능 요구가 더 많아 질수 있으므로 일단 주석처리
            $days = \local_ubion\base\Common::getDateDiffTwitterDays($timestart, $enrolInfo->timeend);
            $ago = null;
            $class = 'text-info';
            if (empty($days)) {
                $ago = get_string('study_ago_today', 'local_vn');
                $class = 'text-danger';
            } else {
                $ago = get_string('study_ago', 'local_vn', $days);
            }
            $enrolPeriod .= '<span class="ml-2 '.$class.'">('.$ago.')</span>';
            */
        }

        // 완료기간에 대해서 일단은 표시하지 않음
        // $ctx->userinfo->enrolPeriod = $enrolPeriod;
    }
}


// 강좌내 학습자원/활동 정보 가져오기
$modinfo = get_fast_modinfo($course);
list ($sections, $completionModuleCount, $completionProgress, $currentCM) = $CVNCourse->getSections($course);

// 숨김 주차가 존재하는경우 array key값이 중간에 비어있어서
// mustache에서 반복문이 제대로 안도는 경우가 발생되기 때문에 key값 자체를 제거해줘야됨.
$sections = array_values($sections);


//error_log(print_r($sections, true));


// 진도율 정보
$ctx->progress = ($completionModuleCount > 0) ? round($completionProgress / $completionModuleCount, 1) : 0;
$ctx->sections = $sections;
$ctx->pageTitle = null;
$ctx->isComplete = false;

// 수동으로 이수가 완료되었거나, 더이상 학습할 항목이 없는 경우
$timecompleted = null;
$isOverriden = false;
if ($attempts = $CVNCourse->getAttempts($courseid, $userid)) {
    // 가장 마지막 항목으로 비교해야됨.
    $attempt = end($attempts);
    $timecompleted = $attempt->timecomplete;
    $isOverriden = $attempt->overriden;
}

// 진행해야될 항목이 존재하는 경우
if (!empty($currentCM)) {
    $ctx->pageTitle = get_string('automove', $pluginname, array('timer' => $timesecond, 'name' => $modinfo->get_cm($currentCM)->name));
}

// 크론 주기에 의해서 timecompleted값이 아직 갱신이 안되어 있을수도 있기 때문에
// currentCM(이수해야될 학습활동)이 빈값인지도 추가적으로 체크해야됨.
if (!empty($timecompleted) || empty($currentCM) || $isComplete) {
    $ctx->isComplete = true;
    $ctx->progress = 100;
    $ctx->pageTitle = get_string('automove_complete', $pluginname);

    if ($isOverriden) {
        $ctx->pageTitle .= ' <span class="text-danger">('.get_string('automove_complete_overriden', $pluginname).')</span>';
    }
}


$ctx->online = new stdClass();
$ctx->online->current = ($courseid == $onCourseID);
$ctx->online->text = \local_ubion\course\Course::getInstance()->getName($onCourseID);
$ctx->online->url = $CFG->wwwroot.'/local/vn/index.php?id='.$onCourseID;

$ctx->offline = new stdClass();
$ctx->offline->current = ($courseid == $offCourseID);
$ctx->offline->text = \local_ubion\course\Course::getInstance()->getName($offCourseID);
$ctx->offline->url = $CFG->wwwroot.'/local/vn/index.php?id='.$offCourseID;

// progress 관련
$progressAttr = [
    'animationStep' => 15,
    'foregroundBorderWidth' => 13,
    'backgroundBorderWidth' => 13,
    'backgroundColor' => '#e5e5e5',
    'foregroundColor' => 'url(#svg-atomy)',
    'fontColor' => '#444',
    'iconSize' => 10
];

if ($courseid == $offCourseID) {
    $progressAttr['percent'] = 0;
    if ($isComplete) {
        $progressAttr['percent'] = 100;
        $text = get_string('exam_complete', 'local_vn');
    } else {
        $text = get_string('exam_incomplete', 'local_vn');
    }
    $progressAttr['replacePercentageByText'] = $text;
    $progressAttr['percentageTextSize'] = 14;
} else {
    $progressAttr['percent'] = $ctx->progress;
}

$progressAttribute = '';
foreach ($progressAttr as $attrKey => $attrValue) {
    $progressAttribute .= ' data-'.$attrKey.'="'.$attrValue.'"';
}

$ctx->progress_tags = '<div class="progress-circle" '.$progressAttribute.'></div>';



$PAGE->requires->js_call_amd('local_vn/index', 'index', array(
    'id' => $courseid,
    'progress' => $ctx->progress,
    'timesecond' => $timesecond,
    'timeformat' => $timeformat
));

$PAGE->requires->strings_for_js(array(
    'numseconds'
), 'core');

echo $OUTPUT->header();
if ($isComplete && $courseid == $offCourseID) {
    $ctx->isTab = false;
    echo $OUTPUT->render_from_template('local_vn/course_completed', $ctx);
} else {
    echo $OUTPUT->render_from_template('local_vn/index', $ctx);
}
echo $OUTPUT->footer();