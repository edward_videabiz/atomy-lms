<?php
namespace local_vn\task;

use local_ubion\base\Common;

class APIReCall extends \core\task\scheduled_task
{
    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('task_apirecal', 'local_vn');
    }
    
    /**
     * Do the job.
     * Throw exceptions on errors (the job will be retried).
     */
    public function execute()
    {
        global $DB;
        
        // API 연동 실패건에 대한 재전송할수 있는 기간
        $days = 7;
        $prevTimecreated = strtotime("-".$days." days");
        
        $query = "SELECT 
                            lvcf.*
                  FROM      {local_vn_complete_failed} lvcf
                  JOIN      {local_vn_complete_attempt} lvca ON lvcf.attemptid = lvca.id
                  JOIN      {user} u ON u.id = lvcf.userid
                  WHERE     lvcf.timecreated >= :timecreated";
        
        $param = [
            'timecreated' => $prevTimecreated
        ];
        
        if ($failed = $DB->get_records_sql($query, $param)) {
            $CVnAPI = \local_vn\API::getInstance();
            
            foreach ($failed as $f) {
                $userInfo = $DB->get_record_sql("SELECT * FROM {user} WHERE id = :id", array('id' => $f->userid));
                $CVnAPI->setCourseComplete($f->courseid, $userInfo, $f->timecomplete, $f->attemptid);
            }
        }
        
        
        // 7일 이전의 데이터는 삭제 처리함.
        $query = "DELETE FROM {local_vn_complete_failed} WHERE timecreated < :timecreated";
        $param = [
            'timecreated' => $prevTimecreated - 1
        ];
        $DB->execute($query, $param);
    }
}