<?php
namespace local_vn\event;

defined('MOODLE_INTERNAL') || die();

/**
 * delete_course_completed 
 *
 * 이수설정 잠금 해제
 *
 */
class course_complete_deleted extends \core\event\base
{

    /**
     * Set basic properties for the event.
     */
    protected function init()
    {
        $this->data['crud'] = 'd';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
    }

    /**
     * Returns localised general event name.
     *
     * @return string
     */
    public static function get_name()
    {
        return get_string('event_course_complete_deleted', 'local_vn');
    }

    /**
     * Returns non-localised event description with id's for admin use only.
     *
     * @return string
     */
    public function get_description()
    {
        return "The course complete deleted the course with id '$this->courseid'.";
    }
}

