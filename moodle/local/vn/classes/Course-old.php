<?php
namespace local_vn;

use local_ubion\base\Javascript;
use local_ubion\base\Parameter;
use local_ubion\base\Common;

class Course extends \local_ubion\controller\Controller
{

    const COMPLETION_ON = 1;
    const COMPLETION_OFF = 2;
    
    private static $instance;

    private $config = null;

    /**
     * Course
     *
     * @return Course
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c();
        }
        return self::$instance;
    }
    
    /**
     * 왼쪽 메뉴 영역 표시여부 
     * 
     * @param int $courseid
     * @return boolean
     */
    public function isLnb($courseid)
    {
        return \local_ubion\course\Course::getInstance()->isProfessor($courseid);
    }

    /**
     * config값 가져오기
     *
     * @return \stdClass
     */
    public function getConfig()
    {
        if (empty($this->config)) {
            $this->config = get_config('local_vn');
        }

        return $this->config;
    }

    /**
     * 참여자 목록
     *
     * @param int $courseid
     * @param int $completion
     * @param string $keyfield
     * @param string $keyword
     * @param int $page
     * @param int $ls
     * @return array[]|mixed[]|boolean[]
     
    public function getEnrolusers($courseid, $completion = null, $keyfield = null, $keyword = null, $page = 1, $ls = 0)
    {
        global $DB, $CFG;

        $userfield = get_all_user_name_fields(true, 'u');

        $query = "SELECT
                				u.id,
                                u.username,
                                " . $userfield . ",
                                MAX(vca.attempt) AS attempt,
                                cc.timecompleted,
                                MAX(vca.overriden) AS overriden
                  FROM  		{enrol} e
                  JOIN			{context} ctx ON ctx.contextlevel = :contextlevel AND ctx.instanceid = e.courseid
                  JOIN			{role_assignments} ra ON ra.contextid = ctx.id
                  JOIN  		{user_enrolments} ue ON ue.enrolid = e.id AND ue.userid = ra.userid
                  JOIN  		{user} u ON u.id = ue.userid
                  LEFT JOIN	    {course_completions} cc ON cc.course = e.courseid AND cc.userid = u.id
                  LEFT JOIN 	{local_vn_complete_attempt} vca ON vca.courseid = e.courseid AND vca.userid = u.id
                  WHERE         e.courseid = :courseid
                  AND           ra.roleid = :roleid";
        

        $roleid = \local_ubion\course\Course::getInstance()->getStudentRoleID();

        $param = [
            'contextlevel' => CONTEXT_COURSE,
            'courseid' => $courseid,
            'roleid' => $roleid
        ];
        
        
        // 관리자는 표시되지 않아야됨.
        $admins = $CFG->siteadmins;
        $query .= " AND u.id NOT IN (".$admins.")";

        // 수료여부
        if ($this->isSearchColumn($completion)) {
            // 0 : 미사용
            // 1 : 사용
            if (empty($completion)) {
                $query .= " AND cc.id IS NULL";
            } else {
                $query .= " AND cc.id > 0";
            }
        }

        // keyfield, keyword
        // 검색
        if (! empty($keyfield) && ! empty($keyword)) {

            if ($keyfield == 'fullname') {
                $fullname = $DB->sql_fullname("u.firstname", "u.lastname");
                $query .= " AND " . $DB->sql_like($fullname, ':keyword');
                $param['keyword'] = $keyword . '%';
            } else {
                $query .= " AND u.mnethostid = :mnethostid AND " . $DB->sql_like('u.username', ':username');

                $param['mnethostid'] = $CFG->mnet_localhost_id;
                $param['username'] = $keyword . '%';
            }
        }

        $query .= " GROUP BY      u.id";

        $totalQuery = "SELECT COUNT(1) FROM (" . $query . ") a";
        $totalCount = $DB->get_field_sql($totalQuery, $param);

        $users = array();
        if ($totalCount > 0) {
            $limitfrom = ($page > 0) ? $ls * ($page - 1) : 0;
            
            //error_log($query);
            //error_log(print_r($param, true));
            $users = $DB->get_records_sql($query, $param, $limitfrom, $ls);
        }

        return array(
            $totalCount,
            $users
        );
    }
    */
    
    /**
     * 참여자 목록
     *
     * @param int $courseid
     * @param int $completion
     * @param string $keyfield
     * @param string $keyword
     * @param int $page
     * @param int $ls
     * @return array[]|mixed[]|boolean[]
     */
    public function getEnrolusers($courseid, $onoff= null, $completion = null, $keyfield = null, $keyword = null, $page = 1, $ls = 0)
    {
        global $DB, $CFG;
        
        $userfield = get_all_user_name_fields(true, 'u');
        
        $config = $this->getConfig();
        $onCourseID = $config->courseid ?? null;
        $offCourseID = $config->courseid_offline ?? null;
        
        $courseReplace = "##COURSEID##";
        $courseQuery = "SELECT 	
                    					vca.userid,
                    					cc.timecompleted,
                    					MAX(vca.attempt) AS attempt,
                                        MAX(vca.overriden) AS overriden
                    	FROM 		    {local_vn_complete_attempt} vca
                    	LEFT JOIN	    {course_completions} cc ON cc.course = vca.courseid AND cc.userid = vca.userid
                    	WHERE 		    vca.courseid = ".$courseReplace."
                    	GROUP BY 	    vca.userid";
        
        $onCourseQuery = str_replace($courseReplace, $onCourseID, $courseQuery);
        $offCourseQuery = str_replace($courseReplace, $offCourseID, $courseQuery);
        
        // 관리자는 표시되지 않아야됨.
        $admins = $CFG->siteadmins;
        $userQuery = "	SELECT 
                    					ra.userid
                    	FROM 			{course} c
                    	JOIN 			{context} ctx ON ctx.contextlevel = :contextlevel AND ctx.instanceid = c.id
                    	JOIN 			{role_assignments} ra ON ra.contextid = ctx.id
                    	WHERE 		    c.id IN (:on_courseid, :off_courseid)
                    	AND			    ra.roleid = :roleid
                        AND             ra.userid NOT IN (".$admins.")
                    	GROUP BY 	    ra.userid";
        
        
        $roleid = \local_ubion\course\Course::getInstance()->getStudentRoleID();
        
        $param = [
            'contextlevel' => CONTEXT_COURSE,
            'on_courseid' => $onCourseID,
            'off_courseid' => $offCourseID,
            'roleid' => $roleid
        ];
        
        $query = "SELECT 
                			    u.id,
                                u.username,
                                " . $userfield . ",
                				uu.onoff,
                				uu.lmsedufinishdate,
                				uu.lmseducertdate,
                				online.timecompleted AS online_timecompleted,
                				online.attempt AS online_attempt,
                				online.overriden AS online_overriden,
                				offline.timecompleted AS offline_timecompleted,
                				offline.attempt AS offline_attempt,
                				offline.overriden AS offline_overriden
                  FROM (
                        ".$userQuery."
                  ) ulist
                  JOIN {user} u ON ulist.userid = u.id
                  JOIN {user_ubion} uu ON u.id = uu.userid
                  LEFT JOIN (
                        ".$onCourseQuery."                	
                  ) online ON online.userid = u.id
                  LEFT JOIN (
                        ".$offCourseQuery."
                  ) offline ON offline.userid = u.id
                  WHERE u.deleted = 0";
        
        // 수료여부
        if ($this->isSearchColumn($completion)) {
            // 1 : 교육 완료
            // 2 : 수료 완료
            if ($completion == self::COMPLETION_ON) {
                $query .= " AND (uu.lmsedufinishdate IS NOT NULL || online.timecompleted IS NOT NULL)";
            } else if ($completion == self::COMPLETION_OFF) {
                $query .= " AND (uu.lmseducertdate IS NOT NULL || offline.timecompleted IS NOT NULL)";
            }
        }
        
        if ($this->isSearchColumn($onoff)) {
            if (empty($onoff)) {
                $param['onoff'] = 0;
            } else {
                $param['onoff'] = 1;
            }
            
            $query .= " AND uu.onoff = :onoff";
        }
        
        // keyfield, keyword
        // 검색
        if (! empty($keyfield) && ! empty($keyword)) {
            
            if ($keyfield == 'fullname') {
                $fullname = $DB->sql_fullname("u.firstname", "u.lastname");
                $query .= " AND " . $DB->sql_like($fullname, ':keyword');
                $param['keyword'] = $keyword . '%';
            } else {
                $query .= " AND u.mnethostid = :mnethostid AND " . $DB->sql_like('u.username', ':username');
                
                $param['mnethostid'] = $CFG->mnet_localhost_id;
                $param['username'] = $keyword . '%';
            }
        }
        
        
        $totalQuery = "SELECT COUNT(1) FROM (" . $query . ") a";
        $totalCount = $DB->get_field_sql($totalQuery, $param);
        
        $users = array();
        if ($totalCount > 0) {
            $limitfrom = ($page > 0) ? $ls * ($page - 1) : 0;
            
            $query .= " ORDER BY u.username";
            
            //error_log($query);
            //error_log(print_r($param, true));
            $users = $DB->get_records_sql($query, $param, $limitfrom, $ls);
        }
        
        return array(
            $totalCount,
            $users
        );
    }

    /**
     * 강좌에 수강생으로 추가
     *
     * @param int $courseid
     * @param int $userid
     * @param int $timecreated
     * @param int $timeend
     * @param int $roleid
     */
    public function setEnrol($courseid, $userid, $timecreated = 0, $timeend = 0, $roleid = 0)
    {
        global $DB;

        $enrolManual = enrol_get_plugin('manual');

        $query = "SELECT * FROM {enrol} WHERE courseid=:courseid AND enrol=:enrol";
        $param = [
            'courseid' => $courseid,
            'enrol' => 'manual'
        ];
        $instance = $DB->get_record_sql($query, $param);

        if (empty($roleid)) {
            $roleid = \local_ubion\course\Course::getInstance()->getRoleStudent();
        }

        $enrolManual = enrol_get_plugin('manual');
        $enrolManual->enrol_user($instance, $userid, $roleid, $timecreated, $timeend);
    }

    /**
     * 강좌 시도 횟수 추가
     *
     * @param int $courseid
     * @param int $userid
     * @param int $timecreated
     * @return int
     */
    public function setAttempt($courseid, $userid, $timecreated = 0)
    {
        global $DB;

        // 기존에 등록된 항목이 존재하는지 확인
        $attempt = $this->getMaxAttempt($courseid, $userid);
        // 시도 횟수 증가
        $attempt ++;

        $completion = new \stdClass();
        $completion->courseid = $courseid;
        $completion->userid = $userid;
        $completion->attempt = $attempt;
        $completion->timecreated = $timecreated;
        $completion->id = $DB->insert_record('local_vn_complete_attempt', $completion);

        return $completion->id;
    }

    /**
     * 최대 시도 횟수
     *
     * @param int $courseid
     * @param int $userid
     * @return int
     */
    public function getMaxAttempt($courseid, $userid)
    {
        global $DB;

        $query = "SELECT 
                            MAX(attempt) 
                  FROM      {local_vn_complete_attempt} 
                  WHERE     courseid = :courseid
                  AND       userid = :userid";

        $param = [
            'courseid' => $courseid,
            'userid' => $userid
        ];

        $attempt = $DB->get_field_sql($query, $param);

        // null값이 전달될수 있기 때문에 0으로 변경해줘야됨.
        if (empty($attempt)) {
            $attempt = 0;
        }

        return $attempt;
    }
    
    
    /**
     * 사용자가 시도한 회차 정보 리턴
     * 
     * @param int $courseid
     * @param int $userid
     * @return array
     */
    public function getAttempts($courseid, $userid=null)
    {
        global $DB, $USER;
        
        if (empty($userid)) {
            $userid = $USER->id;
        }
        
        $query = "SELECT
                            *
                  FROM      {local_vn_complete_attempt}
                  WHERE     courseid = :courseid
                  AND       userid = :userid
                  ORDER BY  attempt";
        
        $param = [
            'courseid' => $courseid,
            'userid' => $userid
        ];
        
        return $DB->get_records_sql($query, $param);
    }
    
    public function getAttempt($attemptid)
    {
        global $DB;
        
        $query = "SELECT
                            *
                  FROM      {local_vn_complete_attempt}
                  WHERE     id = :id";
        
        $param = [
            'id' => $attemptid
        ];
        
        return $DB->get_record_sql($query, $param);
    }

    /**
     * 사용자 회차 테이블의 id값 리턴
     *
     * @param int $courseid
     * @param int $userid
     * @return mixed|boolean
     */
    public function getAttemptID($courseid, $userid = null)
    {
        global $DB, $USER;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        // 가장 마지막으로 실행한 값을 리턴해줘야됨.
        $query = "SELECT
                            id
                  FROM      {local_vn_complete_attempt}
                  WHERE     courseid = :courseid
                  AND       userid = :userid
                  ORDER BY  timecreated DESC LIMIT 1";

        $param = [
            'courseid' => $courseid,
            'userid' => $userid
        ];

        return $DB->get_field_sql($query, $param);
    }

    /**
     * 사용자 - 모듈별 이수 완료시간을 저장하는 테이블의 id값 리턴
     *
     * @param int $attemptid
     * @param int $cmid
     * @return mixed|boolean
     */
    public function getAttemptCMID($attemptid, $cmid)
    {
        global $DB;

        $query = "SELECT id FROM {local_vn_complete_cm} WHERE attemptid = :attemptid AND cmid = :cmid";
        $param = [
            'attemptid' => $attemptid,
            'cmid' => $cmid
        ];

        return $DB->get_field_sql($query, $param);
    }

    /**
     * 모듈 이수 완료 시간 저장
     *
     * @param int $courseid
     * @param int $cmid
     * @param int $userid
     * @param int $state
     * @param int $timecreated
     */
    public function setCMComplete($courseid, $cmid, $userid, $state, $timecreated = null)
    {
        global $CFG, $DB;

        require_once $CFG->libdir . '/completionlib.php';

        $id = $this->getAttemptID($courseid, $userid);

        // user_enrolment_created event에 의해서 무조건 attempt가 생성될수 밖에 없음
        if (! empty($id)) {
            // state 값이 completion_complete이면 insert
            // 그이외의 값이면 delete를 진행할려고 했으나,
            // 생각지 못한 부분에 의해서 attemptid, cmid값이 2개 이상 등록될수 있는 상황이 발생될거 같아서
            // select 후 insert, update 문이 진행되도록 처리함.

            // 완료시간
            $time = ($state == COMPLETION_COMPLETE) ? $timecreated : 0;

            $cm = new \stdClass();

            $cm->timecomplete = $time;

            $attemptCMID = $this->getAttemptCMID($id, $cmid);

            if (empty($attemptCMID)) {
                $cm->attemptid = $id;
                $cm->cmid = $cmid;
                $cm->id = $DB->insert_record('local_vn_complete_cm', $cm);
            } else {
                $cm->id = $attemptCMID;
                $DB->update_record('local_vn_complete_cm', $cm);
            }

            // 애터미 API와 통신할때 usertoken값에 따라서
            // 이곳에서 강좌 이수 완료 여부를 체크 해야되는 경우가 있습니다.
        }
    }

    /**
     * 강좌 완료 
     * 
     * @param int $courseid
     * @param int $userid
     * @param int $timecompleted
     * @param int $overriden
     */
    public function setCourseCompleted($courseid, $userid, $timecompleted, $overriden = 0)
    {
        global $DB;

        $query = "SELECT * FROM {user} WHERE id = :id";
        $param = [
            'id' => $userid
        ];
        
        if ($user = $DB->get_record_sql($query, $param)) {
        
            $attemptid = $this->getAttemptID($courseid, $userid);
    
            // attempt가 없으면 신규로 추가
            if (empty($attemptid)) {
                $attemptid = $this->setAttempt($courseid, $userid, $timecompleted);
            }
    
            $complete = new \stdClass();
            $complete->id = $attemptid;
            $complete->timecomplete = $timecompleted;
    
            if (! empty($overriden)) {
                $complete->overriden = $overriden;
            }
    
            $DB->update_record('local_vn_complete_attempt', $complete);
            
            
            // 강좌 이수 완료되었기 때문에 enrol의 timeend값을 0으로 바꿔줘야됨
            // 해당 값을 변경하지 않으면 강좌 제명당할수 있음
            $query = "SELECT * FROM {enrol} WHERE courseid=:courseid AND enrol=:enrol";
            $param = [
                'courseid' => $courseid,
                'enrol' => 'manual'
            ];
            
            if ($instance = $DB->get_record_sql($query, $param)) {
                $enrolManual = enrol_get_plugin('manual');
                $enrolManual->update_user_enrol($instance, $userid, null, null, 0);
            }
            
            // Atomy API 호출
            $CAPI = \local_vn\API::getInstance();
            $CAPI->setCourseComplete($courseid, $user, $timecompleted, $attemptid);
        }
    }
    
    
    public function setCourseUnCompleted($courseid, $userid)
    {
        global $DB;
        
        $query = "SELECT * FROM {user} WHERE id = :id";
        $param = [
            'id' => $userid
        ];
        
        // 사용자 정보
        if ($user = $DB->get_record_sql($query, $param)) {
            
            $attemptid = $this->getAttemptID($courseid, $userid);
            
            // attempt가 없으면 pass
            // 취소 기능이므로 따로 attempt추가 할 필요는 없을듯.
            if (!empty($attemptid)) {
                $complete = new \stdClass();
                $complete->id = $attemptid;
                $complete->timecomplete = null;
                
                // 수료 취소 이후 정상적인 수료 로직에 의해서 수료 처리 될수 있으므로
                // overriden 값은 0으로 설정해줘야됨.
                $complete->overriden = 0;
                
                $DB->update_record('local_vn_complete_attempt', $complete);
                
                
                // enrol의 timeend값 조정
                $query = "SELECT * FROM {enrol} WHERE courseid=:courseid AND enrol=:enrol";
                $param = [
                    'courseid' => $courseid,
                    'enrol' => 'manual'
                ];
                
                if ($instance = $DB->get_record_sql($query, $param)) {
                    
                    $query = "SELECT timestart FROM {user_enrolments} WHERE enrolid = :enrolid AND userid = :userid";
                    $param = [
                        'enrolid' => $instance->id,
                        'userid' => $userid
                    ];
                    
                    $timestart = $DB->get_record_sql($query, $param);
                    
                    // 기간 제한이 없는 경우가 아니라면 
                    // 강좌 참여 시작일 기준으로 종료일 설정을 해줘야됨.
                    if (!empty($timestart) && $timestart > 0) {
                        $timeend = $timestart + $this->getConfig()->complete_period;
                        
                        $enrolManual = enrol_get_plugin('manual');
                        $enrolManual->update_user_enrol($instance, $userid, null, null, $timeend);
                    }
                    
                    
                    // Atomy API 호출
                    // 취소에 대해서는 API 통신을 진행하지 않기로 협의되어 있습니다.
                }
                
             
            }
            
            
        }
        
        
    }

    /**
     * 강좌 이수 정보 삭제
     * @deprecated 사용되지 않음.
     * @param int $courseid
     */
    public function setCourseCompleteAllDeleted($courseid)
    {
        global $DB;

        // local_vn_complete_attempt 에 등록된 모든 사용자 정보 가져와서
        // 가장 마지막에 시도한 회차의 timecomplete 값을 null로 update해줘야됨.

        // 사용자 목록 가져오기
        $query = "SELECT 
                            userid
                            ,MAX(attempt) AS attempt 
                  FROM      {local_vn_complete_attempt} 
                  WHERE     courseid = :courseid
                  GROUP BY  userid";

        $param = [
            'courseid' => $courseid
        ];

        if ($users = $DB->get_records_sql($query, $param)) {

            // 수동인정한 사용자는 제외해야됨.
            $query = "UPDATE 
                                {local_vn_complete_attempt} 
                      SET 
                                timecomplete = :timecomplete 
                      WHERE     courseid = :courseid
                      AND       userid = :userid
                      AND       attempt = :attempt
                      AND       overriden = 0";

            // 가장 마지막 회차에 대해서만 timecompleted값을 바꿔줘야됨.
            $transaction = $DB->start_delegated_transaction();
            foreach ($users as $u) {
                $param = [
                    'courseid' => $courseid,
                    'userid' => $u->id,
                    'attempt' => $u->attempt
                ];

                $DB->execute($query, $param);
            }
            $transaction->allow_commit();
        }
    }

    /**
     * 쿼리 조건문으로 사용여부
     *
     * @param string $value
     * @return boolean
     */
    public function isSearchColumn($value)
    {
        if (is_null($value) || $value <= Parameter::getOptionAllValue()) {
            return false;
        }

        return true;
    }
    
    
    public function doUserAttempt()
    {
        global $DB, $USER;
        
        $courseid = Parameter::post('courseid', null, PARAM_INT);
        $userid = Parameter::post('userid', $USER->id, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            ),
            'userid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('userid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $userid
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            
            // userid값은 교수자가 아니면 무조건 본인으로 설정해줘야됨.
            if (!\local_ubion\course\Course::getInstance()->isProfessor($courseid)) {
                $userid = $USER->id;
            }
            
            $html = '';
            
            $userfield = get_all_user_name_fields(true);
            
            $userInfo = $DB->get_record_sql("SELECT id, ".$userfield." FROM {user} WHERE id = :id", array('id' => $userid));
            
            $fullname = fullname($userInfo);
            
            if ($attempts = $this->getAttempts($courseid, $userid)) {
                $logText = get_string('log', 'local_vn');
                $period = $this->getConfig()->complete_period;
                foreach ($attempts as $as) {
                    $studyStart = Common::getUserDate($as->timecreated);
                    $studyEnd = Common::getUserDate($as->timecreated + $period);
                    
                    if (!empty($as->timecomplete)) {
                        $timecompleted = '<span class="text-primary font-weight-bold">'.Common::getUserDate($as->timecomplete).'</span>';
                    } else {
                        $timecompleted = '-';
                    }
                    
                    $html .= '<tr>';
                    $html .=    '<td class="text-center">'.$as->attempt.'</td>';
                    $html .=    '<td class="text-center">'.$studyStart.' ~ '.$studyEnd.'</td>';
                    $html .=    '<td class="text-center">'.$timecompleted.'</td>';
                    $html .=    '<td class="text-center">';
                    $html .=        '<button type="button" class="btn btn-xs btn-default btn-activity-log" data-attemptid="'.$as->id.'">'.$logText.'</button>';
                    $html .=    '</td>';
                    $html .= '</tr>';
                }
            } else {
                $html .= '<tr>';
                $html .=    '<td colspan="4">'.get_string('no_study', 'local_vn').'</td>';
                $html .= '</tr>';
            }
            
            
            return Javascript::printJSON(array('code' => Javascript::getSuccessCode(), 'html' => $html, 'fullname' => $fullname));
        }
    }
    
    
    public function doUserAttemptActivity()
    {
        global $DB, $USER, $PAGE;
        
        $attemptid = Parameter::post('attemptid', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'attemptid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('attemptid', 'local_vn')
                ,$this->validate()::PARAMVALUE => $attemptid
            )
        ));
        
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            // 강좌에 등록된 
            if ($attempt = $DB->get_record_sql("SELECT * FROM {local_vn_complete_attempt} WHERE id = :id", array('id' => $attemptid))) {
                
                // 교수자가 아니면 본인 이외의 현황을 볼수 없음
                if (!\local_ubion\course\Course::getInstance()->isProfessor($attempt->courseid)) {
                    if ($attempt->userid != $USER->id) {
                        Javascript::printAlert(get_string('no_permission_activity', 'local_vn'));
                    }
                }
                
                if (empty($PAGE->context)) {
                    $PAGE->set_context(\context_course::instance($attempt->courseid));
                }
                
                $title = get_string('attempt_log', 'local_vn', $attempt->attempt);
                $html = '';
                
                $CCourse = \local_ubion\course\Course::getInstance();
                $course = $CCourse->getCourse($attempt->courseid);
                
                // 이수기능을 사용하는 학습자원/활동
                $cms = $this->getCompletionActivity($course);
                
                // 회차별 이수정보를 기록하고 있는 테이블
                $cmsActivity = $this->getUserAttemptActivity($attemptid);
                
                
                // 강좌에서 이수기능을 사용하는 학습자원/활동이 존재하는 경우
                if (!empty($cms)) {
                    $number = 1;
                    foreach ($cms as $cm) {
                        $timecompletion = $cmsActivity[$cm->id]->timecomplete ?? 0;
                        
                        if (!empty($timecompletion)) {
                            $timecompletion = '<span class="text-primary font-weight-bold">'.Common::getUserDate($timecompletion).'</span>';
                        } else {
                            $timecompletion = '-';
                        }
                        
                        $html .= '<tr>';
                        $html .=    '<td class="text-center">'.$number.'</td>';
                        $html .=    '<td><img src="'.$cm->get_icon_url().'" alt="'.$cm->modname.'" class="mr-2" />'.$cm->name.'</td>';
                        $html .=    '<td class="text-center">'.$timecompletion.'</td>';
                        $html .= '</tr>';
                        
                        $number++;
                    }
                } else {
                    $html .= '<tr><td colspan="3">'.get_string('nocriteriaset', 'completion').'</td></tr>';
                }
                
                return Javascript::printJSON(array('code' => Javascript::getSuccessCode(), 'html' => $html, 'title' => $title));
                
            } else {
                Javascript::printAlert(get_string('notfound_attempt', 'local_vn', $attemptid));
            }
        }
    }
    
    
    /**
     * 이수기능을 사용하는 학습자원/활동 목록
     * 
     * @param int $courseorid
     * @param \completion_info $CCompletionInfo
     * @return \cm_info[]
     */
    public function getCompletionActivity($courseorid, $CCompletionInfo = null)
    {
        // 이수기능을 사용하는 학습자원/활동
        $cms = array();
        
        if (is_number($courseorid)) {
            $course = \local_ubion\course\Course::getInstance()->getCourse($courseorid);
        } else {
            $course = $courseorid;
        }
        
        if (empty($CCompletionInfo)) {
            $CCompletionInfo = new \completion_info($course);
        }
        
        $modinfo = get_fast_modinfo($course);
        foreach ($modinfo->cms as $cm) {
            if ($cm->visible) {
                if ($CCompletionInfo->is_enabled($cm) != COMPLETION_TRACKING_NONE) {
                    $cms[$cm->id] = $cm;
                }
            }
        }
        
        return $cms;
    }
    
    /**
     * 특정 회차에 진행되었던 학습자원/활동 완료 기록
     * 
     * @param int $attemptid
     * @return array
     */
    public function getUserAttemptActivity($attemptid)
    {
        global $DB;
        
        return $DB->get_records_sql("SELECT cmid, timecomplete FROM {local_vn_complete_cm} WHERE attemptid = :attemptid", array('attemptid' => $attemptid));
    }
    
    
    /**
     * 강좌 제명시 무들에서 삭제시켜주지 않는 데이터들 삭제
     * 강좌 구성에 따라 삭제해야될 항목들이 증가될수 있습니다.
     * 
     * @param int $courseid
     * @param int $userid
     */
    public function setEnrolDelete($courseid, $userid)
    {
        global $DB;
        
        // 사용자의 이수정보 삭제 해야됨.
        $DB->delete_records('course_completions', array('userid'=> $userid, 'course' => $courseid));
        $completion = \cache::make('core', 'completion');
        $key = $userid . '_' . $courseid;
        if ($completion->get($key)) {
            $completion->delete($key);
        }
        
        
        $DB->delete_records('course_completion_crit_compl', array('userid'=> $userid, 'course' => $courseid));
        $coursecompletion = \cache::make('core', 'coursecompletion');
        $key = $userid . '_' . $courseid;
        if ($coursecompletion->get($key)) {
            $coursecompletion->delete($key);
        }
        
        
        // 학습자원/활동별 이수 완료여부 삭제
        $course = \local_ubion\course\Course::getInstance()->getCourse($courseid);
        if ($cms = $this->getCompletionActivity($course)) {
            foreach ($cms as $cm) {
                $param = [
                    'coursemoduleid' => $cm->id,
                    'userid' => $userid
                ];
                
                $DB->delete_records('course_modules_completion', $param);
            }
        }
        
        
        // 삭제되지 않는 기록들 같이 삭제 해줘야됨.
        // scorm_scoes_track, scorm_aicc_session
        $query = "SELECT id FROM {scorm} WHERE course = :course";
        $param = ['course' => $courseid];
        
        if ($scorms = $DB->get_records_sql($query, $param)) {
            foreach ($scorms as $ss) {
                $deleteParam = [
                    'scormid' => $ss->id,
                    'userid' => $userid
                ];
                
                $DB->delete_records('scorm_aicc_session', $deleteParam);
                $DB->delete_records('scorm_scoes_track', $deleteParam);
            }
        }
    }
    
    
    public function doCheckCompletion()
    {
        $courseid = Parameter::post('courseid', null, PARAM_INT);
        $complete = Parameter::post('complete', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number',
                $this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion'),
                $this->validate()::PARAMVALUE => $courseid
            ),
            'complete' => array(
                'required', 'number',
                $this->validate()::PLACEHOLDER => get_string('completion_yn', 'local_vn'),
                $this->validate()::PARAMVALUE => $complete
            ),
        ));
                
        if ($validate->fails()) {
                Javascript::printAlert($validate->getErrorMessages());
        } else {
            
            if ($complete == Parameter::getOptionAllValue()) {
                Javascript::printAlert(get_string('not_selected_completion', 'local_vn'));
            }
            
            // 선택된 사용자 목록
            $users = Parameter::postArray('userids', null, PARAM_INT);
            
            $userCount = count($users);
            
            if ($userCount > 0) {
                
                $uniqueUsers = array_unique($users);
                
                // 수료인 경우
                if ($complete == '1') {
                    $timecompleted = time();
                    
                    foreach ($uniqueUsers as $uu) {
                        $this->setCourseCompleted($courseid, $uu, $timecompleted);
                    }
                } else {
                    foreach ($uniqueUsers as $uu) {
                        $this->setCourseUnCompleted($courseid, $uu);
                    }
                }
                
                
            } else {
                Javascript::printAlert(get_string('notfound_check_user', 'local_vn'));
            }
            
        }
    }
    
    
    /**
     * 주차별 학습 활동 진도율
     * 
     * @param int $courseorid
     * @param int $userid
     * @return array
     */
    public function getSections($courseorid, $userid = null)
    {
        global $CFG, $USER, $DB;
        require_once $CFG->dirroot.'/mod/scorm/locallib.php';
        
        $CCourse = \local_ubion\course\Course::getInstance();
        
        if (is_number($courseorid)) {
            $course = $CCourse->getCourse($courseorid);
        } else {
            $course = $courseorid;
        }
        
        if (empty($userid)) {
            $userid = $USER->id;
        }
        
        $CCompletion = new \report_ubcompletion\Completion($course->id);
        
        $modinfo = get_fast_modinfo($course);
        
        
        // 이수해야될 학습활동 cmid
        $currentCM = null;
        
        // 이수처리해야될 학습활동 갯수
        $completionModuleCount = 0;
        
        // 이수처리해야될 학습활동 전체 합산 진도율
        $completionProgress = 0;
        
        if ($sections = $CCompletion->getSections($modinfo)) {
            
            // 사용자 이수현황정보
            $userCompletions = $CCompletion->getUserCompletions($userid);
            
            $currentCM = null;
            
            // $CCompletion->getSections 에는 cmid값만 담겨져있기 때문에 cm 관련된 추가 정보를 더 반영해줘야됨.
            foreach ($sections as $section) {
                // 주차 진도율
                $sectionPercent = 0;
                
                // 주차내에 존재하는 학습자원/활동
                $section->cmidsinfo = array();
                
                // 학습 진행 예정인 주차 여부
                $section->current = false;
                
                
                if (!empty($section->cmids)) {
                    foreach ($section->cmids as $cmid) {
                        
                        $cminfo = $modinfo->get_cm($cmid);
                        
                        $info = new \stdClass();
                        $info->id = $cmid;
                        $info->name = $cminfo->name;
                        $info->modname = $cminfo->modname;
                        $info->icon = $cminfo->get_icon_url();
                        $info->state = COMPLETION_INCOMPLETE;
                        $info->overrideby = 0;
                        $info->date = null;
                        $info->percent = 0;
                        $info->url = $cminfo->url;
                        $info->onclick = $cminfo->onclick;
                        
                        // 동영상, econtents 같이 진도율을 체크하는 모듈인 경우 viewer.php로 링크 변경해줘야됨.
                        if (in_array($info->modname, \local_ubonattend\Progress::getAllowModules())) {
                            $info->url = str_replace('view.php', 'viewer.php', $info->url);
                        }
                        
                        // 사용자 이수 완료 여부 체크 
                        if (isset($userCompletions[$cmid])) {
                            $info->state = $userCompletions[$cmid]->completionstate;
                            $info->overrideby = $userCompletions[$cmid]->overrideby;
                            $info->date = $userCompletions[$cmid]->timemodified;
                        }
                        
                        // 이수 완료되지 않는 모듈인 경우 해당 주차부터 학습할수 있도록 current값을 변경해줘야됨.
                        if (empty($currentCM) && $info->state != COMPLETION_COMPLETE) {
                            $section->current = true;
                            $currentCM = $cmid;
                        }
                        
                        // 진도율 정보
                        $modulePercent = 0;
                        if ($info->state == COMPLETION_COMPLETE) {
                            // 이수 완료되었으면 무조건 진도율은 100%
                            $modulePercent = 100;
                        } else {
                            // 스콤인 경우에는 보고된 성적(grade reported)값 으로 진도율 판단 
                            if ($cminfo->modname == 'scorm') {
                                // error_log('cmid : '.$cmid);
                                if ($scorm = $DB->get_record_sql("SELECT * FROM {scorm} WHERE id = :id", array('id' => $cminfo->instance))) {
                                    $modulePercent = scorm_grade_user($scorm, $userid);
                                    
                                    // 100보다 크면 100으로 고정
                                    if ($modulePercent > 100) {
                                        $modulePercent = 100;
                                    }
                                }
                            }
                        }
                        $info->percent = $modulePercent;
                        
                        $section->cmidsinfo[] = $info;
                        $sectionPercent += $modulePercent;
                        
                        // 이수처리를 진행하는 모듈 갯수
                        $completionModuleCount++;
                        
                        // 이수처리를 진행하는 모듈의 진도율 총합
                        $completionProgress += $modulePercent;
                    }
                }
                
                $section->isCompletionModule = false;
                $cmCount = count($section->cmidsinfo);
                if ($cmCount > 0) {
                    $section->isCompletionModule = true;
                    $sectionPercent = round($sectionPercent / $cmCount, 1);
                }
                
                $section->percent = $sectionPercent;
            }
        }
        
        return array($sections, $completionModuleCount, $completionProgress, $currentCM);
    }
    
    public function doNextActivityURL()
    {
        global $CFG, $PAGE;
        
        $courseid = Parameter::post('courseid', null, PARAM_INT);
        
        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number',
                $this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion'),
                $this->validate()::PARAMVALUE => $courseid
            )
        ));
                
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            
            if (empty($PAGE->context)) {
                $PAGE->set_context(\context_course::instance($courseid));
            }
            
            list ($sections, $completionModuleCount, $completionProgress, $currentCM) = $this->getSections($courseid);
            
            $text = get_string('automove_complete', 'local_vn');
            $text = strip_tags($text);
            
            $code = '101';
            $message = strip_tags(get_string('automove_complete', 'local_vn'));
            $href = $CFG->wwwroot.'/local/vn/index.php?id='.$courseid;
            
            if (!empty($currentCM)) {
                $modinfo = get_fast_modinfo($courseid);
                $cm = $modinfo->get_cm($currentCM);
            
                $code = Javascript::getSuccessCode();
                $message = get_string('view_complete', 'local_ubion');
                $href = $cm->url->out();
                
                // 뷰어로 열람을 해야되는 경우
                // view => viewer로 치환
                if (in_array($cm->modname, \local_ubonattend\Progress::getAllowModules())) {
                    $href = str_replace('view.php', 'viewer.php', $href);
                }
            }
            
            Javascript::printJSON(array('code' => $code, 'msg' => $message, 'href' => $href));
        }
    }
    
    /**
     * 온라인 강좌 수료 여부 확인
     * 
     * @param int $userid
     * @return boolean
     */
    public function isOnlineCourseComplete($userid, $userUbion = null)
    {
        global $DB;
        
        if (empty($userUbion)) {
            $userUbion = $DB->get_record_sql("SELECT * FROM {user_ubion} WHERE userid = :userid", array('userid' => $userid));
        }
        
        $isComplete = false;
        $onoff = $userUbion->onoff;
        
        // 오프라인 사용자라면 온라인 강좌는 이수 완료로 판단
        if ($onoff) {
            // 상황에 따라 lmsedufinishdate 값을 추가적으로 검사를 더 해야될수도 있음.
            // $lmsedufinishdate = $userUbion->lmsedufinishdate ?? null;
            $isComplete = true;
        } else {
            // 온라인 사용자라면 lmsedufinishdate값이 존재하거나, 강좌 이수 완료했는지 확인해봐야됨.
            
            $lmsedufinishdate = $userUbion->lmsedufinishdate ?? null;
            
            if (!empty($lmsedufinishdate)) {
                $isComplete = true;
            } else {
                $onCourseID = $this->getConfig()->courseid ?? null;
                $isComplete = $this->isCourseComplete($onCourseID, $userid);
            }
        }
        
        
        return $isComplete;
    }
    
    /**
     * 강좌 이수 완료 여부
     * 
     * @param int $courseid
     * @param int $userid
     * @return boolean
     */
    public function isCourseComplete($courseid, $userid)
    {
        global $CFG;

        require_once $CFG->libdir.'/completionlib.php';
        
        $params = array(
            'userid'    => $userid,
            'course'    => $courseid
        );
        
        $ccompletion = new \completion_completion($params);
        return $ccompletion->is_complete();
    }
    
    /**
     * enrol 정보 리턴
     * 
     * @param int $courseid
     * @param int $userid
     * @return boolean | \stdClass
     */
    public function getEnrolInfo($courseid, $userid = null)
    {
        global $DB, $USER;
        
        if (empty($userid)) {
            $userid = $USER->id;
        }
        
        $query = "SELECT 
                            ue.*
                  FROM      {enrol} e
                  JOIN      {user_enrolments} ue ON ue.enrolid = e.id AND ue.userid = :userid
                  WHERE     e.courseid = :courseid
                  AND       e.enrol = :enrol 
                  AND       ue.status = :active 
                  AND       e.status = :enabled";
        
        $param = array(
            'enabled'   => ENROL_INSTANCE_ENABLED, 
            'active'    => ENROL_USER_ACTIVE, 
            'userid'    => $userid, 
            'courseid'  => $courseid,
            'enrol'     => 'manual'
        );
        
        return $DB->get_record_sql($query, $param);
    }
}