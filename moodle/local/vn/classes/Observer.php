<?php
namespace local_vn;

class Observer
{

    public static function user_enrolment_created(\core\event\user_enrolment_created $event)
    {
        /**
         * **
         * [data:protected] => Array
         * (
         * [eventname] => \core\event\user_enrolment_created
         * [component] => core
         * [action] => created
         * [target] => user_enrolment
         * [objecttable] => user_enrolments
         * [objectid] => 2
         * [crud] => c
         * [edulevel] => 0
         * [contextid] => 22
         * [contextlevel] => 50
         * [contextinstanceid] => 2
         * [userid] => 2
         * [courseid] => 2
         * [relateduserid] => 3
         * [anonymous] => 0
         * [other] => Array
         * (
         * [enrol] => manual
         * )
         *
         * [timecreated] => 1536295687
         * )
         *
         * [logextra:protected] =>
         * [context:protected] => context_course Object
         * (
         * [_id:protected] => 22
         * [_contextlevel:protected] => 50
         * [_instanceid:protected] => 2
         * [_path:protected] => /1/3/22
         * [_depth:protected] => 3
         * )
         *
         * [triggered:core\event\base:private] => 1
         * [dispatched:core\event\base:private] =>
         * [restored:core\event\base:private] =>
         * [recordsnapshots:core\event\base:private] => Array
         * (
         * )
         */
        $CVnCourse = \local_vn\Course::getInstance();

        // 추가된 사용자
        $courseid = $event->courseid;
        $userid = $event->relateduserid;
        $timecreated = $event->timecreated;

        // local_vn_complete_attempt 에 추가된 기록 남겨줘야됨.
        $CVnCourse->setAttempt($courseid, $userid, $timecreated);
    }

    public static function user_enrolment_deleted(\core\event\user_enrolment_deleted $event)
    {
        // error_log('user_enrolment_deleted');
        // error_log(print_r($event, true));
        
        $courseid = $event->courseid;
        $userid = $event->relateduserid;
        
        $CVnCourse = \local_vn\Course::getInstance();
        $CVnCourse->setEnrolDelete($courseid, $userid);
    }

    public static function course_completed(\core\event\course_completed $event)
    {

        /**
         * core\event\course_completed Object
         * (
         * [data:protected] => Array
         * (
         * [eventname] => \core\event\course_completed
         * [component] => core
         * [action] => completed
         * [target] => course
         * [objecttable] => course_completions
         * [objectid] => 5
         * [crud] => u
         * [edulevel] => 2
         * [contextid] => 22
         * [contextlevel] => 50
         * [contextinstanceid] => 2
         * [userid] => 2
         * [courseid] => 2
         * [relateduserid] => 3
         * [anonymous] => 0
         * [other] => Array
         * (
         * [relateduserid] => 3
         * )
         *
         * [timecreated] => 1536562637
         * )
         *
         * [logextra:protected] =>
         * [context:protected] => context_course Object
         * (
         * [_id:protected] => 22
         * [_contextlevel:protected] => 50
         * [_instanceid:protected] => 2
         * [_path:protected] => /1/3/22
         * [_depth:protected] => 3
         * )
         *
         * [triggered:core\event\base:private] => 1
         * [dispatched:core\event\base:private] =>
         * [restored:core\event\base:private] =>
         * [recordsnapshots:core\event\base:private] => Array
         * (
         * [course_completions] => Array
         * (
         * [5] => stdClass Object
         * (
         * [userid] => 3
         * [course] => 2
         * [timeenrolled] => 1536295687
         * [timestarted] => 1536562622
         * [timecompleted] => 1536562622
         * [reaggregate] => 1536562622
         * [id] => 5
         * )
         *
         * )
         *
         * )
         *
         * )
         */

        error_log('course_completed');
        error_log(print_r($event, true));
        $courseid = $event->courseid;
        $userid = $event->relateduserid;
        $timecomplted = $event->timecreated;
        
        $vnConfig = get_config('local_vn');
        $vnCourseid = $vnConfig->courseid ?? null;
        $vnOffCourseid = $vnConfig->courseid_offline ?? null;
        
        // 강좌 이수 완료시 API 연동이 진행되어야 할 강좌인 경우
        if ($courseid == $vnCourseid || $courseid == $vnOffCourseid) {
            $CVnCourse = \local_vn\Course::getInstance();
            $CVnCourse->setCourseCompleted($courseid, $userid, $timecomplted);
        }
    }

    /*
     * 사용되지 않음
     * 강좌 구성이 다 된 상태에서 사용자들이 학습 하기 때문에 이수 옵션 변경으로 인해 강좌 이수 완료값이 바뀔일이 없음.
     * 만약 이수 옵션 변경이 이루어진 경우를 대비 해야된다면
     * 별도의 파일을 이용해서 $CVnCourse->setCourseCompleteDeleted($courseid);를 실행하는게 좋을듯함.
     *
     * public static function course_complete_deleted(\local_vn\event\course_complete_deleted $event)
     * {
     * // error_log('course_complete_deleted');
     * // error_log(print_r($event, true));
     * // 이수 잠금 해제(삭제)
     * $CVnCourse = \local_vn\Course::getInstance();
     * $CVnCourse->setCourseCompleteAllDeleted($courseid);
     * }
     */

    /**
     * 모듈 이수 정보 변경 이벤트
     *
     * @param \core\event\course_module_completion_updated $event
     */
    public static function course_module_completion_updated(\core\event\course_module_completion_updated $event)
    {
        $courseid = $event->courseid;
        $userid = $event->relateduserid;
        $cmid = $event->contextinstanceid;
        $state = $event->other['completionstate'];

        $CVnCourse = \local_vn\Course::getInstance();
        $CVnCourse->setCMComplete($courseid, $cmid, $userid, $state, $event->timecreated);
    }
}