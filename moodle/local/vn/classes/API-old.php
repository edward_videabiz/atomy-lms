<?php
namespace local_vn;

use local_ubion\base\Common;

class API
{

    // token login : [POST] https://www.atomy.com:449/apiglobal/authentication/vn/v1/sso/auth
    // id pw login : [POST] https://www.atomy.com:449/apiglobal/lms/vn/v1/account/auth
    // completion : [PUT] https://www.atomy.com:449/apiglobal/lms/vn/v1/account/{userId}/status/{finishDate}
    // completion 2차 수정 : [POST] https://www.atomy.com:449/apiglobal/lms/vn/v1/account/status
    private static $instance;

    protected $config = null;

    /**
     * SSO
     *
     * @return \local_vn\API
     */
    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c();
        }
        return self::$instance;
    }

    public function getConfig()
    {
        if (empty($this->config)) {
            $this->config = get_config('local_vn');
        }

        return $this->config;
    }

    private function getApiUrl()
    {
        return $this->getConfig()->apiurl;
    }

    private function getApiToken()
    {
        return $this->getConfig()->apitoken;
    }


    /**
     * 강좌 이수 완료 API
     *
     * @param int $courseid
     * @param \stdClass $user
     * @param string $date
     * @param int $attemptid
     * 
     * @return boolean[]|NULL[]|mixed[]
     */
    public function setCourseComplete($courseid, $user, $timecompleted, $attemptid)
    {
        global $DB;
        
        $api = '/apiglobal/lms/vn/v1/account/status';

        // courseid에 따라 항목명이 달라져야됨
        // 온라인 : EduFinishDate
        // 오프라인 : EduCertifyDate
        
        $config = $this->getConfig();
        
        // 수강해야될 강좌 번호
        $onCourseID = $config->courseid ?? null;
        $offCourseID = $config->courseid_offline ?? null;
        
        // 정해진 강좌에 대해서만 API 호출이 이루어져야됨.
        if ($courseid == $onCourseID || $courseid == $offCourseID) {
            
            // API 호출 여부
            // 온라인 : EduFinishDate값이 설정되어 있으면 API 호출 할 필요 없음.
            // 오프라인 
            //   - EduFinishDate값이 설정되어 있으면 API 호출할 필요 없음.
            //   - EduCertifyDate값이 설정되어 있으면 API 호출할 필요 없음.
            $isAPICall = true;
            
            $ymd = date('Y-m-d', $timecompleted);
            
            // 배열 형태로 전달해줘야됨.
            $array = [];
            
            $userComplete = new \stdClass();
            $userComplete->CustNo = $user->username;
            
            $userUbion = $DB->get_record_sql("SELECT * FROM {user_ubion} WHERE userid = :userid", ['userid' => $user->id]);
            
            // 온라인, 오프라인 강좌에 따라서 property명이 달라져야됨.
            $updateUserUbionProperty = '';
            $updateAPIProperty = '';
            if ($courseid == $onCourseID) {
                $updateUserUbionProperty = 'lmseducertdate';
                $updateAPIProperty = 'EduFinishDate';
                $userComplete->EduFinishDate = $ymd;
                
                // 교육 완료일이 설정되어 있다면 API 호출 할 필요가 없음.
                $lmseducertdate = $userUbion->lmseducertdate ?? null;
                if (!empty($lmseducertdate)) {
                    $isAPICall = false;
                }
            } else if ($courseid == $offCourseID) {
                $updateUserUbionProperty = 'lmsedufinishdate';
                $updateAPIProperty = 'EduCertifyDate';
                $userComplete->EduCertifyDate = $ymd;
                
                // 이수 완료일이 설정되어 있다면 API 호출 할 필요가 없음.
                $lmsedufinishdate = $userUbion->lmsedufinishdate ?? null;
                if (!empty($lmsedufinishdate)) {
                    $isAPICall = false;
                }
            }
            
            // 베트남 계정이 아닌 경우에는 API 호출할 필요가 없음.
            if ($user->auth != 'vn') {
                $isAPICall = false;
            }
            
            // API 호출이 필요한 경우
            if ($isAPICall) {
                $array[] = $userComplete;
                
                $parameter = json_encode($array);
                
                // API 호출
                list ($isSuccess, $message, $data) = $this->curl($api, null, $parameter);
                
                // API 연동 성공 여부
                $isAPIFail = true;
                
                if ($isSuccess) {
                    if (!empty($data)) {
                        
                        // 1명만 전달이 되기 때문에 사용자별로 예외처리 할 필요 없음
                        foreach ($data as $returnData) {
                            if ($returnData->IsSuccess) {
                                $isAPIFail = false;
                                
                                // 정상적으로 API 호출이 되었다면, user_ubion에도 수료 완료일 저장
                                // 사용자가 로그인 하면 자동으로 변경이 되긴 하는데 
                                // 문제는 강좌 및 학습활동 이수 조건이 초기화 되고, 
                                // 이후 크론에 의해서 강좌 및 학습활동 이수가 완료 되어서
                                // 애터미 API 호출이 이루어질 경우 
                                // 과거에 강좌가 이수를 완료했음에도 불구하고 다시 API 호출이 이루어지는 문제가 있음.
                                if (!empty($userUbion) && !empty($updateUserUbionProperty) && !empty($updateAPIProperty)) {
                                    $updateUserUbion = new \stdClass();
                                    $updateUserUbion->id = $userUbion->id;
                                    $updateUserUbion->$updateUserUbionProperty = $returnData->$updateAPIProperty;
                                    $DB->update_record('user_ubion', $updateUserUbion);
                                }
                            }
                        }
                    }
                }
                
                // API 호출시 정상적으로 값 저장이 안된 경우 local_vn_complete_failed 에 저장해놓고서
                // 특정시간마다 다시 재 호출을 해야됨.
                if ($isAPIFail) {
                    // $attemptInfo = $this->get
                    $CVNCourse = \local_vn\Course::getInstance();
                    if ($attemptInfo = $CVNCourse->getAttempt($attemptid)) {
                        
                        $query = "SELECT
                                        id
                              FROM      {local_vn_complete_failed}
                              WHERE     attemptid = :attemptid";
                        
                        // 기존에 등록된 정보가 존재하는지 확인
                        // API에 문제가 있어서 몇시간동안 계속 실패가 떨어질수 있음.
                        $id = $DB->get_field_sql($query, ['attemptid' => $attemptid]);
                        
                        if (empty($id)) {
                            $failed = new \stdClass();
                            $failed->courseid = $attemptInfo->courseid;
                            $failed->userid = $attemptInfo->userid;
                            $failed->attempt = $attemptInfo->attempt;
                            $failed->timecomplete = $timecompleted;
                            $failed->timecreated = time();
                            $failed->attemptid = $attemptid;
                            $DB->insert_record('local_vn_complete_failed', $failed);
                        }
                    }
                } else {
                    // 전송 실패 이력 삭제
                    $DB->delete_records('local_vn_complete_failed', ['attemptid' => $attemptid]);
                }
            }
        }
    }

    protected function curl($apiname, $userToken, $parameter = null, $isPut = false)
    {
        $url = $this->getApiUrl() . $apiname;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        // put으로 전송해야되는 경우
        if ($isPut) {
            curl_setopt($ch, CURLOPT_PUT, 1);
        } else {
            curl_setopt($ch, CURLOPT_POST, 1);
        }

        $requireHeader = [
            'User-Agent: AtomyAPI',
            'Content-Type: application/json',
            'Accept-Encoding: gzip',
            'Atomy-Api-Token: ' . $this->getApiToken()
        ];

        // userToken이 빈값이 아닌 경우
        // id, pw로 로그인 할때는 필요가 없음
        if (! empty($userToken)) {
            $requireHeader[] = 'Atomy-User-Token: ' . $userToken;
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $requireHeader);

        // 파라메터가 빈값이 아닌 경우
        if (! empty($parameter)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
        }

        $result = curl_exec($ch);
        curl_close($ch);
        
        error_log('url : ' . $url);
        error_log(print_r($requireHeader, true));
        error_log(print_r($parameter, true));
        error_log(print_r($result, true));
        
        $isSuccess = false;
        $message = $result;
        $data = null;

        if (! empty($result)) {
            $json = json_decode($result);

            if (json_last_error() === JSON_ERROR_NONE) {

                // Status가 존재하는 경우
                if ($json->Status == '1') {
                    $isSuccess = true;
                    $data = $json->Data;
                }
            }
        }
        
        return array(
            $isSuccess,
            $message,
            $data
        );
    }

    public function curlLogin($api, $parameter)
    {
        // 로그인 전용 curl
        $url = $this->getApiUrl() . $api;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        // id, pw로 로그인시에는 usertoken이 필수값이 아님
        $requireHeader = [
            'User-Agent: AtomyAPI',
            'Content-Type: application/json',
            'Accept-Encoding: gzip',
            'Atomy-Api-Token: ' . $this->getApiToken()
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $requireHeader);

        // 파라메터 설정
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);

        $result = curl_exec($ch);

        error_log('url : ' . $url);
        error_log(print_r($requireHeader, true));
        error_log(print_r($parameter, true));
        error_log(print_r($result, true));

        $isSuccess = false;
        $message = $result;
        $data = $userToken = null;

        if (! empty($result)) {
            
            $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            
            $header = substr($result, 0, $headerSize);
            $body = substr($result, $headerSize);
            
            // 로그인 API는 header 정보도 같이 가져와야되기 때문에 추가 작업이 필요함.
            if ($headers = $this->getCurlHeader($header)) {
                $userToken = $headers['Atomy-User-Token'] ?? null;
            }

            $json = json_decode($body);

            if (json_last_error() === JSON_ERROR_NONE) {

                // Status가 존재하는 경우
                if ($json->Status == '1') {
                    $isSuccess = true;
                    $data = $json->Data;
                }
            }
        }

        
        curl_close($ch);
        
        return array(
            $isSuccess,
            $message,
            $data,
            $userToken
        );
    }

    /**
     * Curl header 정보를 배열 형태로 리턴
     *
     * @param string $text
     * @return array[]
     */
    protected function getCurlHeader($text)
    {
        $headers = array();
        
        $explode = explode("\r\n", $text);

        foreach ($explode as $i => $line) {

            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                // 빈값이 아닌 경우
                if (!empty($line)) {
                    list ($key, $value) = explode(': ', $line);
    
                    $headers[$key] = $value;
                }
            }
        }

        return $headers;
    }

    /**
     * 로그인
     *
     * @param \stdClass $data
     * @param string $userToken
     */
    protected function login($data, $userToken)
    {
        global $CFG, $DB;

        $username = null;

        if (! empty($data)) {
            // 1. getAuthentication으로 사용자 정보를 받아옴
            $username = $data->CustNo;

            // 2. 전달된 정보를 토대로 moodle에 등록된 회원인지 확인
            $query = "SELECT id FROM {user} WHERE mnethostid = :mnethostid AND username = :username";
            $param = [
                'mnethostid' => $CFG->mnet_localhost_id,
                'username' => $username
            ];

            // 로그인시마다 sso에서 전달해준 값으로 사용자 추가 및 update를 진행해야됨.
            if ($userid = $DB->get_field_sql($query, $param)) {
                // 회원 업데이트
                $member = $this->getMemberInfo($data, true);
                $this->setUpdateMember($userid, $member);
            } else {
                // 회원 추가
                $member = $this->getMemberInfo($data);
                $userid = $this->setInsertMember($member);
            }
            
            // userubion에 token값을 저장하기 위해서 data에 userToken값 할당
            $data->userToken = $userToken;

            // userubion setting
            $this->setUserUbion($userid, $data);

            // 사용자 로그인
            $query = "SELECT * FROM {user} WHERE id = :id";
            $param = [
                'id' => $userid
            ];

            // 정말로 사용자가 존재하는지 확인
            if ($user = $DB->get_record_sql($query, $param)) {
                // 로그인 처리
                $USER = complete_user_login($user);

                // 세션에 usertoken 값 기록
                $USER->ATOMY_USERTOKEN = $userToken;
                
                // 관리자이면 관리자 화면으로 곧바로 이동 시켜줘야됨.
                if (is_siteadmin()) {
                    redirect($CFG->wwwroot.'/local/ubion/');
                } else {
                    $CVnCourse = \local_vn\Course::getInstance();
                    $config = $CVnCourse->getConfig();
                    
                    $reqGubun = $data->LMSReqGubun ?? null;
                    $isOfflineUser = $this->isOfflineUser($reqGubun);
                    
                    $onCourseID = $config->courseid ?? null;
                    $offCourseID = $config->courseid_offline ?? null;
                    
                    // 오프라인 사용자인 경우 곧바로 퀴즈 강좌로 이동해야됨.
                    if ($isOfflineUser) {
                        // 수강해야될 강좌 번호
                        $courseid = $offCourseID;
                        
                        // 오프라인 사용자는 온라인 강좌도 복습할수 있어야 되므로, 온라인 강좌에 참여자 등록 시켜줘야됨
                        // 단 기간 제한은 없음.
                        $context = \context_course::instance($onCourseID);
                        
                        if (! is_enrolled($context)) {
                            $timestart = 0;
                            $timeend = 0;
                            
                            $CVnCourse->setEnrol($courseid, $userid, $timestart, $timeend);
                        }
                        
						$courseid = $onCourseID;
                        // 오프라인 강좌도 수강 신청되어야 함.
                        $context = \context_course::instance($offCourseID);
                        
                        if (! is_enrolled($context)) {
                            $timestart = 0;
                            $timeend = 0;
                            
                            $CVnCourse->setEnrol($courseid, $userid, $timestart, $timeend);
                        }
                    } else {
                        // 수강해야될 강좌 번호
                        $courseid = $onCourseID;
                        
                        // 온라인 사용자라면, 참여자 등록이 안되어 있으면 
                        // 특정 기간 동안 수강할수 있어야됨.
                        $context = \context_course::instance($courseid);
                        // 강좌에 참여자로 구성되어 있는지 확인
                        if (! is_enrolled($context)) {
                            $timestart = time();
                            $timeend = $timestart + $config->complete_period;
                            
                            $CVnCourse->setEnrol($courseid, $userid, $timestart, $timeend);
                        }
                    }
    
                    // 강좌에 참여자로 등록시켜주기
                    if (! empty($courseid)) {
                        $this->redirectActivities($courseid);
                    } else {
                        // 강좌번호가 따로 존재하지 않는다면 메인 페이지로 이동
                        redirect($CFG->wwwroot);
                    }
                }
            } else {
                Common::printNotice(get_string('notfound_user', 'local_vn', $username), get_login_url());
            }
        }

        redirect(get_login_url());
    }
    
    
    public function redirectActivities($courseid)
    {
        global $CFG;
        
        // TODO 전달된 courseid로 이수해야될 학습자원/활동으로 자동으로 redirect 시켜줘야됨.
        
        // 일단은 강좌로 이동 시켜놓음
        redirect($CFG->wwwroot . '/local/vn/index.php?id=' . $courseid);
    }

    /**
     *
     * 입력받은 id, pw로 로그인
     *
     * auth/vn/auth.php에서 호출됨
     *
     * @param string $id
     * @param string $pw
     * @return boolean
     */
    public function loginIDPW($id, $pw)
    {
        $parameter = new \stdClass();
        $parameter->UserId = $id;
        $parameter->Password = $pw;

        $parameter = json_encode($parameter);

        $api = '/apiglobal/lms/vn/v1/account/auth';
        list ($isSuccess, $message, $data, $userToken) = $this->curlLogin($api, $parameter);

        if ($isSuccess) {
            // 강제 로그인 처리
            $this->login($data, $userToken);
        } else {
            return false;
        }
    }

    /**
     * 토큰 로그인
     *
     * @param string $userToken
     * @return boolean[]|mixed[]|NULL[]
     */
    public function loginUserToken($userToken)
    {
        $api = '/apiglobal/authentication/vn/v1/sso/auth';
        list ($isSuccess, $message, $data, $userToken) = $this->curlLogin($api, $userToken);
        
        if ($isSuccess) {
            // 강제 로그인 처리
            $this->login($data, $userToken);
        } else {
            Common::printNotice($message, get_login_url());
        }
    }
    
    private function getMemberInfo($data, $isUpdate = false)
    {
        global $CFG;

        $member = new \stdClass();

        // 이름
        $korName = $data->CustName;
        $engName = $data->CustName;
        if (empty($engName)) {
            $engName = $korName;
        }
        $member->firstname = $korName;
        $member->lastname = $engName;
        $member->mnethostid = $CFG->mnet_localhost_id; // always local user

        // 이메일이 빈값이면 @로 처리해줘야됨.
        $email = $data->Email;
        if (empty($email)) {
            $email = "@";
        }
        $member->email = $email;

        // user테이블의 phone1, phone2의 길이가 20자이기 때문에
        // 연락처 20자리보다 크면 빈값처리
        // PhoneNo값이 CertInfo에 위치함.
        // $phone1 = $data->PhoneNo;
        $phone1 = '';
        if (isset($data->CenterInfo)) {
            if (isset($data->CenterInfo->PhoneNo)) {
                $phone1 = $data->CenterInfo->PhoneNo;
            }
        }
        
        if (strlen($phone1) >= 20) {
            $phone1 = '';
        }
        $member->phone1 = $phone1;

        $phone2 = $data->HandPhone;
        if (strlen($phone2) >= 20) {
            $phone2 = '';
        }
        $member->phone2 = $phone2;

        // 추가시에만 반영되어야 할 항목
        if (! $isUpdate) {
            $memberkey = $data->CustNo;

            $member->mnethostid = $CFG->mnet_localhost_id; // always local user

            $member->confirmed = 1; // 인증여부
            $member->deleted = 0; // 삭제여부
            $member->policyagreed = 0; // 사이트정책
                                       // $member->auth = get_config('local_ubion', 'haksa_auth'); // 인증타입
            $member->auth = 'vn';
            $member->username = $memberkey; // 사용자명
            $member->idnumber = $memberkey; // 사용자명
            $member->password = 'nosavepassword';

            $member->country = 'VN'; // 국가
            $member->city = ''; // 도시

            $member->lang = ''; // 선호언어
            $member->timezone = "99"; // 시간대 (서버시간대)

            $member->description = ""; // 개인 소개
            $member->descriptionformat = "1"; // 개인소개 포맷
            $member->mailformat = "1"; // 이메일 양식 (0:텍스트, 1:HTML)
            $member->maildigest = "0"; // 이메일 요약 형식

            // 2016-03-02 메일 전송시 보내는 사람 메일이 제대로 표시되지 않는 문제로 인해 다시 2값으로 변경함
            // 그리고, 교수 이외에는 강좌를 같이 수강한 사용자라도 이메일은 표시되면 안되기 때문에
            // /user/view.php에서 or ($user->maildisplay == 2 && is_enrolled($coursecontext, $USER)) 부분 주석 처리를 진행해야됨.
            $member->maildisplay = "2"; // 이메일 공개 여부 (0:비공개, 1:공개, 2:강좌참여자에게만 공개)
            $member->htmleditor = "1"; // 에디터 사용여부 (0: 무들 기본, 1 : tiny 또는 무들 설정값)
            $member->ajax = '1'; // Ajax 및 자바
            $member->autosubscribe = "1"; // 포럼 자동 구독
            $member->trackforums = "0"; // 포럼 추적
            $member->imagealt = "";
            $member->screenreader = "0"; // 화면 읽기 도구

            // 기타 입력사항
            $member->url = ""; // 이메일
            $member->icq = ""; // icq
            $member->skype = ""; // skype
            $member->aim = ""; // aim
            $member->yahoo = ""; // yahoo
            $member->msn = ""; // msn
            $member->address = ''; // 주소
        }

        return $member;
    }

    /**
     * 회원 추가
     *
     * @param \stdClass $member
     * @return int
     */
    private function setInsertMember($member)
    {
        global $DB;

        if ($member->id = $DB->insert_record('user', $member)) {
            // Create USER context for this user.
            \context_user::instance($member->id);
        }

        return $member->id;
    }

    /**
     * 회원 업데이트
     *
     * @param int $userid
     * @param \stdClass $member
     */
    private function setUpdateMember($userid, $member)
    {
        global $DB;

        $member->id = $userid;
        $DB->update_record('user', $member);
    }

    /**
     * userUbion 테이블에 데이터 자장
     * 
     * @param int $userid
     * @param \stdClass $data
     */
    private function setUserUbion($userid, $data)
    {
        global $DB;
        
        $userUbionID = $DB->get_field_sql("SELECT id FROM {user_ubion} WHERE userid = :userid", array('userid' => $userid));
        
        $userUbion = new \stdClass();
        $userUbion->jisacode = $data->JisaCode;
        $userUbion->lmsedufinishdate = $data->LMSEduFinishDate ?? null;
        $userUbion->lmseducertdate = $data->LMSEduCertDate ?? null;
        $userUbion->atomyusertoken = $data->userToken;
        
        // 온/오프 사용자 
        // 오프라인 사용자는 온라인 강의를 들을 필요 없이 곧바로 퀴즈를 풀면됨.
        $reqGubun = $data->LMSReqGubun ?? null;
        $userUbion->onoff = $this->isOfflineUser($reqGubun);
        
        if (empty($userUbionID)) {
            $userUbion->userid = $userid;
            $userUbion->user_type = '';
            $DB->insert_record('user_ubion', $userUbion);
        } else {
            $userUbion->id = $userUbionID;
            $DB->update_record('user_ubion', $userUbion);
        }
    }
    
    
    /**
     * 오프라인 사용자인지 확인
     * 
     * @param string $onoff
     * @return boolean
     */
    public function isOfflineUser($onoff)
    {
        $isOfflineUser = 0;
        
        // 대소문자가 어떻게 전달되지 모르므로 무조건 소문자로 치환해서 검사
        $onoff = strtolower($onoff);
        
        // 온오프여부값이 전달이 되지 않는 경우도 있음.
        if (!empty($onoff) && $onoff != 'on') {
            $isOfflineUser = 1;    
        }
        
        return $isOfflineUser;
    }
}