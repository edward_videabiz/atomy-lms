<?php
use \local_ubion\base\Common;

foreach ($lists as $l) {
    
    if (!empty($l->cms)) {
        
        
        $isFirst = true;
        foreach ($l->cms as $cm) {
            $accesslogURL = $CFG->wwwroot.'/report/log/index.php?id='.$courseInfo->id.'&user='.$user->id.'&date=0&modid='.$cm->id.'&chooselog=1';
            $customdata = $cm->customdata;
            
            // 출석인정 요구시간
            $timeAttend = '-';
            
            if ($customdata->playtime > 0) {
                $timeAttend = $customdata->playtime / 100 * $customdata->completionprogress;
                // 출석인정 요구 시간에 대한 반올림 처리
                $timeAttend = round($timeAttend);
            }
            
            $onclick = (!empty($cm->onclick)) ? ' onclick="'.$cm->onclick.'"' : '';
            
            $tbody .= '<tr>';
            if ($isFirst) {
                $rowspan = ($l->rowspan > 1) ? ' rowspan="'.$l->rowspan.'"' : '';
                $tbody .= '<td class="text-center" '.$rowspan.'>'.$l->section.'</td>';
            }
            
            // 학습활동
            $tbody .=     '<td>';
            $tbody .=         '<a href="'.$cm->url.'" '.$onclick.'>';
            $tbody .=             '<img src="'.$cm->icon.'" alt="'.$cm->modname.'" class="mr-2" />'.$cm->name;
            $tbody .=         '</a>';
            $tbody .=     '</td>';
            
            // 출석 인정 요구시간
            $tbody .=     '<td class="text-center" title="'.$timeAttend.'">'.Common::getGMDate($timeAttend).'</td>';
            
            // 진도율
            $progress = $cm->totalprogress;
            if ($config->progresstype != \local_ubonattend\Progress::PROGRESSTYPE_PERCENT) {
                $progress = $cm->totalprogress_time;
            }
            $tbody .=     '<td class="text-center" title="'.$progress.'">';
            $tbody .=           Common::getGMDate($progress);
            if ($isProfessor) {
                if (!empty($cm->attempts)) {
                    $tbody .=       '<br/>';
                    $tbody .=       '<button class="btn btn-xs btn-default btn-attempt mt-1" data-cmid="'.$cm->id.'">'.get_string('view_attempts', $pluginname, $cm->attempts).'</button>';
                }
                $tbody .=           '<br/>';
                $tbody .=           '<a href="'.$accesslogURL.'" class="btn btn-xs btn-default mr-1 mt-1" target="_blank">accesslog</a>';
                
                // 상세보기는 관리자만 가능
                if (is_siteadmin()) {
                    $tbody .=       '<button class="btn btn-xs btn-default btn-detail mt-1" data-cmid="'.$cm->id.'">detaillog</button>';
                }
            }
            $tbody .=     '</td>';
            
            // 출석
            $tbody .=     '<td class="text-center">';
            $tbody .=         $COnAttendance->getStatusSymbol($cm->status);
            
            // 재생시간이 0으로 설정되어 있는 경우
            if (empty($customdata->playtime)) {
                $tbody .=    '<br/>';
                $tbody .=     '<span class="small text-info">'.get_string('playtime0', $pluginname).'</span>';
            }
            
            if ($isProfessor) {
                // 인정 / 인정 취소 버튼 표시
                $tbody .=     '<br/>';
                if (empty($cm->recognition)) {
                    // 인정버튼이 현 출석상태값과 동일하면 표시할 필요가 없음
                    if ($cm->status != \local_ubonattend\Attendance::CODE_ATTENDANCE) {
                        // 출석/학습인정 버튼
                        $tbody .= '<button class="btn btn-xs btn-default btn-recognition mt-1" data-cmid="'.$cm->id.'" data-recognition="'.\local_ubonattend\Attendance::CODE_ATTENDANCE.'">';
                        $tbody .=     $i8n->recognition_attendance;
                        $tbody .= '</button>';
                    }
                    
                    // 지각 인정
                    if ($isLate) {
                        // 인정버튼이 현 출석상태값과 동일하면 표시할 필요가 없음
                        if ($cm->status != \local_ubonattend\Attendance::CODE_LATE) {
                            $tbody .= '<button class="btn btn-xs btn-default btn-recognition ml-1 mt-1" data-cmid="'.$cm->id.'" data-recognition="'.\local_ubonattend\Attendance::CODE_LATE.'">';
                            $tbody .=     $i8n->recognition_lateness;
                            $tbody .= '</button>';
                        }
                    }
                    
                    // 결석인정
                    // 인정버튼이 현 출석상태값과 동일하면 표시할 필요가 없음
                    if ($cm->status != \local_ubonattend\Attendance::CODE_ABSENCE) {
                        $tbody .= '<button class="btn btn-xs btn-default btn-recognition ml-1 mt-1" data-cmid="'.$cm->id.'" data-recognition="'.\local_ubonattend\Attendance::CODE_ABSENCE.'">';
                        $tbody .=     $i8n->recognition_absence;
                        $tbody .= '</button>';
                    }
                    
                } else {
                    // 온라인 출석부를 사용하면 출석/지각/결석 관련 항목이 표시되어야 하고,
                    // 일반 진도관리 페이지라면 학습인정 문구가 표시되어야 함.
                    $recognitionStatusName = $COnAttendance->getRecognitionStatusName($cm->recognition);
                    
                    $tbody .= 	'<button class="btn btn-xs btn-danger btn-recognition mt-1" data-cmid="'.$cm->id.'" data-recognition="0" data-statusname="'.$recognitionStatusName.'">';
                    $tbody .= 		$i8n->recognition_cancel;
                    $tbody .= 	'</button>';
                }
            }
            $tbody .=     '</td>';
            
            if ($isFirst) {
                $rowspan = ($l->rowspan > 1) ? ' rowspan="'.$l->rowspan.'"' : '';
                $tbody .= '<td class="text-center" '.$rowspan.'>'.$COnAttendance->getStatusSymbol($l->status).'</td>';
            }
            $tbody .= '</tr>';
            
            $isFirst = false;
        }
    } else {
        $tbody .= '<tr>';
        $tbody .=    '<td class="text-center">'.$l->section.'</td>';
        $tbody .=    '<td class="text-center">&nbsp;</td>';
        $tbody .=    '<td class="text-center">&nbsp;</td>';
        $tbody .=    '<td class="text-center">&nbsp;</td>';
        $tbody .=    '<td class="text-center">&nbsp;</td>';
        $tbody .=    '<td class="text-center">&nbsp;</td>';
        $tbody .= '</tr>';
    }
}