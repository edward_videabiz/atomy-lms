<?php
require '_head.php';


$i8n->title = get_string('tab_setting', $pluginname);

$PAGE->set_url($baseurl.'/setting.php?id='.$id);
$PAGE->set_title($courseInfo->fullname.' : ' . $i8n->title);
$PAGE->set_heading($i8n->title);
$PAGE->navbar->add($i8n->title, $baseurl.'/setting.php?id='.$id);

// 현재 설정된 주차별 기간 설정값 가져오기
$config = $COnAttendance->getConfig($courseInfo->id);

// 지각 기능 사용여부
$isLate = $config->islate;
$PAGE->requires->js_call_amd('local_ubonattend/attendance', 'setting', array(
    'isLate' => $isLate
));

$PAGE->requires->strings_for_js(array(
    'error_oldate'
    ,'error_oldate2'
    ,'error_oldate3'
    ,'error_percent'
    ,'error_percent2'
    ,'confirm_section_change'
), $pluginname);

// mustache에 전달할때에는 id값을 제거한 상태로 보내야됨
if ($sections = $COnAttendance->getSections($courseInfo->numsections)) {
    $sections = array_values($sections);
}

$colspan = 5;
if ($config->islate) {
    $colspan = 7;
}
//error_log(print_r($sections, true));

// 강좌 종료일이 지난 강좌에 대해서는 더이상 수정이 불가능해야됨.
$isDisabled = $CCourse->isStudyEnd($courseInfo->id);


$ctx = new stdClass();
$ctx->courseid = $courseInfo->id;
$ctx->isLate = $isLate;
$ctx->sections = $sections;
$ctx->colspan = $colspan;
$ctx->typename = $COnAttendance::TYPENAME;
$ctx->isDisabled = $isDisabled;

echo $OUTPUT->header();
echo $COnAttendance->getTabHTML('setting');
echo $OUTPUT->render_from_template('local_ubonattend/setting', $ctx);
echo $OUTPUT->footer();