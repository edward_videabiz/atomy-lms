<?php
namespace local_ubonattend;

class Attendance extends \local_ubonattend\core\Attendance
{
   private static $instance;
       
   /**
    * 강좌 Class
    * @return Attendance
    */
   public static function getInstance($courseid) {
       if (!isset(self::$instance)) {
           $c = __CLASS__;
           self::$instance = new $c($courseid);
       }
       return self::$instance;
   }
}