<?php
namespace local_ubonattend\event;
defined('MOODLE_INTERNAL') || die();

class section_changed extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_section_changed', 'local_ubonattend');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
    	return new \moodle_url('/local/ubonattend/setting.php', array('id' => $this->courseid ));
    }
}
