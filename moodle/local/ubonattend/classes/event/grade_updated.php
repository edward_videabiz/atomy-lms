<?php
namespace local_ubonattend\event;
defined('MOODLE_INTERNAL') || die();

class grade_updated extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
        $this->data['objecttable'] = 'local_ubonattend_config';
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_grade_updated', 'local_ubonattend');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
        return new \moodle_url('/local/ubonattend/grade.php', array('id' => $this->courseid ));
    }
    
    public static function get_objectid_mapping() {
        return array('db' => 'local_ubonattend_config', 'restore' => 'local_ubonattend_config');
    }
}
