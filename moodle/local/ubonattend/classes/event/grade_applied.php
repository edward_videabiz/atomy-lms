<?php
namespace local_ubonattend\event;
defined('MOODLE_INTERNAL') || die();

class grade_applied extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'c';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_grade_applied', 'local_ubonattend');
    }

    /**
     * Get URL related to the action
     *
     * @return \moodle_url
     */
    public function get_url() {
    	return new \moodle_url('/local/ubonattend/index.php', array('id' => $this->courseid ));
    }
}
