<?php
namespace local_ubonattend;

use \local_ubion\base\Common;

trait TraitProgress
{
    /**
     * track 정보 리턴
     *
     * @param object $module
     * @param int $userid
     * @return \stdClass
     */
    public function getProgressTrack($modname, $module, $userid=null)
    {
        global $DB, $USER;
        
        if (empty($userid)) {
            $userid = $USER->id;
        }
        
        $time = time();
        
        $moduleTrack = $modname.'_track';
        $propertyname = $modname.'id';
        
        $query = "SELECT * FROM {".$moduleTrack."} WHERE ".$propertyname." = :moduleid AND userid = :userid";
        $param = array('moduleid' => $module->id, 'userid' => $userid);
        
        $isComplete = 0;
        if ($track = $DB->get_record_sql($query, $param)) {
            
            // 시도횟수 증가
            $attempts = $track->attempts + 1;
            $track->attempts = $attempts;
            $DB->update_record($moduleTrack, $track);
            
            // 동영상을 끝까지 시청했는지 판단
            // 판단 기준
            // 1. 수동인정
            // 2. 진도율이 동영상 재생시간보다 큰 경우
            if (\local_ubonattend\Progress::getRecognitionAttendance($track->recognition) || $track->totalprogress >= $module->playtime) {
                $isComplete = 1;
            }
        } else {
            $track = new \stdClass();
            $track->userid = $userid;
            $track->$propertyname = $module->id;
            $track->timefirst = $time;
            $track->timelast = $time;
            $track->attempts = 1;
            $track->id = $DB->insert_record($moduleTrack, $track);
        }
        
        // 접속 디바이스 정보 저장
        $device = new \stdClass();
        $device->track = $track->id;
        $device->attempt = $track->attempts;
        $device->device = \core_useragent::get_device_type();
        $device->ip = getremoteaddr();
        $device->timecreated = $time;
        
        // 앱에서 접속할수도 있음
        $isWebview = $USER->isWebview ?? false;
        if ($isWebview) {
            $device->device = 'app';
        }
        $DB->insert_record($moduleTrack.'_device', $device);
        
        
        // 추가정보 반영해서 track값 리턴시켜줌
        $track->isComplete = $isComplete;
        
        return $track;
    }
    
    /**
     * 동영상 진도처리기 기간
     *
     * @param object $mod
     * @param int|object $sectionid
     * @return \stdClass
     */
    public static function getProgressPeriod($mod, $sectionorid)
    {
        $courseid = $mod->course;
        
        $progressInfo = new \stdClass();
        $progressInfo->timeopen = $mod->timeopen;
        $progressInfo->timeclose = $mod->timeclose;
        $progressInfo->timelateness = $mod->timeclose;
        $progressInfo->progress = $mod->progress;
        $progressInfo->useattend = $mod->useattend;
        
        // 출석 관련 설정
        $progressInfo->completionprogress = $mod->completionprogress;
        $progressInfo->tardinessprogress = $mod->tardinessprogress;
        
        $COnAttendance = \local_ubonattend\Attendance::getInstance($courseid);
        
        // 온라인 출석부를 사용하는 경우
        if ($COnAttendance->isAttendanceCourse()) {
            
            if (!empty($sectionorid)) {
                if (is_number($sectionorid)) {
                    $section = $COnAttendance->getSection($sectionorid);
                } else {
                    $section = $sectionorid;
                }
                
                // 0주차이거나, 등록되지 않는 sectionid값이 전달될수도 있기 때문에 예외처리
                if (!empty($section)) {
                    // 온라인 출석부를 사용하기 때문에 무조건 진도처리는 예로 설정
                    // 온라인 출석여부는 사용자가 따로 지정할수 있기 때문에 따로 처리 하지 않음.
                    $progressInfo->progress = 1;
                    
                    $progressInfo->timeopen = $section->time_start;
                    $progressInfo->timeclose = $section->time_end;
                    $progressInfo->completionprogress = $section->attendance_percent;
                    
                    $progressInfo->timelateness = $section->time_end;
                    $progressInfo->tardinessprogress = $section->attendance_percent;
                    
                    // 지각 기능을 사용하는 경우
                    if ($COnAttendance->isLateCourse()) {
                        $progressInfo->timelateness = $section->time_lateness;
                        $progressInfo->tardinessprogress = $section->lateness_percent;
                    }
                }
            }
        }
        
        return $progressInfo;
    }
    
    /**
     * 진도처리기간에 대해서 사용자가 알아볼수 있도록 텍스트 형태로 리턴
     *
     * @param object $mod
     * @param int $sectionid
     * @return string
     */
    public static function getProgressPeriodText($mod, $sectionid)
    {
        $progressInfo = self::getProgressPeriod($mod, $sectionid);
        
        return self::_getProgressPeriodText($progressInfo);
    }
    
    /**
     * 진도처리기간에 대해서 사용자가 알아볼수 있도록 텍스트 형태로 리턴
     * 
     * @param \stdClass $progressInfo
     * @return string
     */
    public static function getProgressPeriodTextFromProgressInfo($progressInfo)
    {
        return self::_getProgressPeriodText($progressInfo);
    }
    
    /**
     * 진도처리기간에 대해서 사용자가 알아볼수 있도록 텍스트 형태로 리턴
     * 
     * @param \stdClass $progressInfo
     * @return string
     */
    protected static function _getProgressPeriodText($progressInfo)
    {
        $noTimeLimit = get_string('no_time_limit', 'local_ubonattend');
        
        $details = '';
        
        // 시작, 종료일 둘중 한개라도 값이 할당되어 있을때 진도처리기간이 표시되어야 함.
        if (!empty($progressInfo->timeopen) || !empty($progressInfo->timeclose)) {
        
            // 시작일
            if (empty($progressInfo->timeopen)) {
                $details .= $noTimeLimit;
            } else {
                $details .= Common::getUserDate($progressInfo->timeopen);
            }
            
            $details .= ' ~ ';
            
            // 종료일
            if (empty($progressInfo->timeclose)) {
                $details .= $noTimeLimit;
            } else {
                $details .= Common::getUserDate($progressInfo->timeclose);
            }
            
            // 지각 기능
            // 온라인 출석부에서 출석인정시간과 지각 인정시간을 동일하게 설정하는 경우가 있음
            if (!empty($progressInfo->timelateness) && $progressInfo->timeclose != $progressInfo->timelateness) {
                $details .= '<span class="text-late ml-1">('.get_string('late', 'local_ubonattend').' : '.Common::getUserDate($progressInfo->timelateness).')</span>';
            }
        }
        return $details;
    }
    
    
    /**
     * xx초 마다 진도처리 기록을 남김
     * 
     * @param int $playtime
     * @param boolean $isMillisecond
     * @return number
     */
    public static function getProgressInterval($playtime, $isMillisecond=true)
    {
        // 5분 이내 30초 단위
        // 5분 이상 1분 단위 체크함 
        $intervalSecond = ($playtime <= 300) ? 30000 : 60000;
        
        // millisecond 형태가 아니라면 /1000을 진행해줘야됨.
        if (empty($isMillisecond)) {
            $intervalSecond = $intervalSecond / 1000;
        }
        
        return $intervalSecond;
    }
    
    
    /**
     * course_modules 테이블에서 직접 cm정보 조회
     * 
     * @param int $courseid
     * @param int $moduleid
     * @param int $instanceid
     * @return mixed|boolean
     */
    public static function getProgressCMID($courseid, $moduleid, $instanceid)
    {
        global $DB;
        
        $query = "SELECT id FROM {course_modules} WHERE course = :courseid AND module = :module AND instance = :instance";
        $param = array('courseid' => $courseid, 'module' => $moduleid, 'instance' => $instanceid);
        
        return $DB->get_field_sql($query, $param);
    }
    
    
    /**
     * 동영상 상세 기록 저장
     * 
     * @param \cm_info $cm
     * @param int $state
     * @param int $positionfrom
     * @param int $positionto
     */
    public static function setProgressTrackDetail(\cm_info $cm, $state, $positionfrom, $positionto, $userid = null)
    {
        global $DB, $USER;
        
        if (empty($userid)) {
            $userid = $USER->id;
        }
        
        $customdata = $cm->customdata;
        
        $query = "SELECT * FROM {".$cm->modname."_track} WHERE ".$cm->modname."id = :moduleid AND userid = :userid";
        $param = array('moduleid' => $cm->instance, 'userid' => $userid);
        
        // trackInfo 체크
        if ($trackInfo = $DB->get_record_sql($query, $param)) {
        
            $isInsert = false;
            
            $moduleidstring = $cm->modname.'id';
            $moduleid = $trackInfo->$moduleidstring;
            
            // 요청들어오는데로 무조건 저장 (update문 없음)
            // update구문으로 인해 진도처리기간이 변경되는 8번 코드는 계속 업데이트 되기 때문에 특정 시점에 어느위치까지 영상을 시청했는지 알수가 없음.
            // 무조건 추가하고 뷰어단에서 script call이 이루어지지 않도록 변경조치함. 
            // TODO 특정 시점이 지난면 뷰어가 자동 종료되도록 변경조치해야됨.
            $isInsert = true;
            
            /*
            // 이전 상태값의 데이터와 전달된 데이터가 같은 경우에는 디비 기록하지 않음
            if($lastTrackDetail = $DB->get_record_sql('SELECT id, state, positionnew, positionold FROM {'.$cm->modname.'_track_detail} WHERE track = :track ORDER BY id DESC LIMIT 1', array('track'=>$trackInfo->id))) {
                // 8번인 경우에는 positionfrom, positionto와 상관 없이 update
                
                
                // 전달된 상태값이 8번이고, track_detail에 기록된 가장 마지막 state값이 8번인 경우에는 해당 레코드에 update 시켜줘야됨.
                if($lastTrackDetail->state == \local_ubonattend\Progress::STATE_INTERVAL && $lastTrackDetail->state == $state) {
                    // 현재 재생 위치가 과거 재생 위치보다 큰 경우에만 위치값 update
                    // 버퍼링에 의해서 현재 위치가 비정상위치가 찍힐수 있기 때문에 진도율이 제대로 구해지지 않는 버그가 있을수 있음.
                    // 2014-11-12 by akddd
                    if($positionto >= $lastTrackDetail->positionnew) {
                        $trackDetail = new \stdClass();
                        $trackDetail->id = $lastTrackDetail->id;
                        $trackDetail->positionold = $positionfrom;
                        $trackDetail->positionnew = $positionto;
                        $trackDetail->timetrack = time();
                        
                        $DB->update_record($cm->modname.'_track_detail', $trackDetail);
                    } else {
                        $isInsert = true;
                    }
                } else if ($lastTrackDetail->state != $state || $lastTrackDetail->positionold != $positionfrom || $lastTrackDetail->positionnew != $positionto) {
                    $isInsert = true;
                }
                
            } else {
                $isInsert = true;
            }
            */
            
            // 재생 완료 이벤트는 이전 상태값과 무관하게 실행되어야 함.
            if ($state == \local_ubonattend\Progress::STATE_COMPLETE) {
                $isInsert = true;
                
                // 재생 완료인 경우 무조건 100% 표시하기 위해서
                // positionfrom은 0, positionto는 동영상의 재생시간으로 표시
                $positionfrom = 0;
                $positionto = $customdata->playtime;
            }
            
            // track_detail에 insert
            if ($isInsert) {
                $trackDetail = new \stdClass();
                $trackDetail->track = $trackInfo->id;
                $trackDetail->attempt = $trackInfo->attempts;
                $trackDetail->state = $state;
                $trackDetail->positionold = $positionfrom;		// 반올림 보다는 내림?을 해서 처리하는게 더 효과적일듯...
                $trackDetail->positionnew = $positionto;
                $trackDetail->timetrack = time();
                $trackDetail->progress = 0;
                
                $trackDetail->id = $DB->insert_record($cm->modname.'_track_detail', $trackDetail);
            }
            
            // 강좌 종료일이 지났는지 확인 해봐야됨.
            // 만약 강좌 종료일과 상관없이 
            $isStudyEnd = self::isStudyEnd($cm->course);
            
            // 강좌 종료일이 지났는지 확인 해봐야됨
            // 만약 강좌 종료일이 지났으면 진도처리 재계산이 되면 안됨.
            if (! $isStudyEnd) {
            
                
                // 사용자 진도율 정보 반영 해주는 로직 실행
                $CProgress = new \local_ubonattend\Progress($cm->course);
                list ($isProgressPeriod, $status) = $CProgress->setUserViewerProgress($cm, $trackInfo);
                
                // 진도 체크 기간인 경우
                if ($isProgressPeriod) {
                    
                    // 세션에서 cmid에 대한 출석 상태값을 비교해서 출석 상태값이 변경되었으면 주차 출석 상태값을 변경해줘야됨.
                    $isUpdateSection = false;
                    
                    $userUbion = \local_ubion\user\User::getInstance()->getUbion();
                    if (isset($USER->ubion->progress[$cm->id])) {
                        if ($USER->ubion->progress[$cm->id] != $status) {
                            $isUpdateSection = true;
                            
                            $USER->ubion->progress[$cm->id] = $status;
                        }
                    } else {
                        // cmid에 대해 한번도 체크를 한적이 없기 때문에 무조건 update 진행
                        $isUpdateSection = true;
                        
                        // 세션에 정보 기록
                        $USER->ubion->progress[$cm->id] = $status;
                    }
                    
                    // 해당 동영상의 출석 상태가 변경되었다면 주차 출석에 대한 정보도 변경해줘야됨.
                    if ($isUpdateSection) {
                        $CProgress->setUserSectionReCalculate($cm->section);
                    }
                }
            }
        }
    }
    
    
    /**
     * 강좌 종료일이 지났는지 확인
     * 학교마다 요구 조건이 다를거 같아서 함수로 별도로 빼놓음.
     * 향후 해당 옵션의 변경 요청이 자주 들어온다면 설정으로 빼놓아야 할듯함.
     * 
     * @param int $courseid
     * @return boolean
     */
    public static function isStudyEnd($courseid)
    {
        return \local_ubion\course\Course::getInstance()->isStudyEnd($courseid);
    }
    
    
    /**
     * 팝업창 옵션
     *
     * @param int $width
     * @param int $height
     * @return string
     */
    public static function getPopupOption($width, $height)
    {
        $opt = "width=".$width;
        $opt .= ",height=".$height;
        $opt .= ",toolbar=no";
        $opt .= ",location=no";
        $opt .= ",menubar=no";
        $opt .= ",copyhistory=no";
        $opt .= ",status=no";
        $opt .= ",directories=no";
        $opt .= ",scrollbars=yes";
        $opt .= ",resizable=yes";
        
        return $opt;
    }
    
    
    /**
     * 모듈의 customdata 설정  
     *
     * @param object $mod
     * @param object $cm
     * @return string
     */
    public static function getCustomData($mod, $cm)
    {   
        // 진도처리 관련 항목
        $progressInfo = self::getProgressPeriod($mod, $cm->section);
        
        
        // 상황에 따라 필요한 항목들을 추가해주시면 됩니다.
        // 참고로 해당 함수 코드 수정시 /mod/모듈/lib.php의 모듈_cm_info_view() 함수도 같이 수정이 이루어져야될수 있습니다.
        $options = new \stdClass();
        $options->playtime = $mod->playtime;
        $options->timeopen = $progressInfo->timeopen;
        $options->timeclose = $progressInfo->timeclose;
        $options->timelateness = $progressInfo->timelateness;
        $options->completionprogress = $progressInfo->completionprogress;
        $options->tardinessprogress = $progressInfo->tardinessprogress;
        $options->progress = $mod->progress;
        $options->useattend = $mod->useattend;
        $options->autograde = $mod->autograde;
        $options->add_grade_item = $mod->add_grade_item;
        $options->details = '';
        
        // 온라인 출석부 설정과 상관 없이 무조건 뷰어 열람 시간으로 체크되어야 하는 경우 true로 설정해주시면 됩니다.
        // 참고로 isOpenTimeCheck는 
        // 각 모듈마다 체크하는 조건문이 달라지기 때문에 
        // /mod/모듈/lib.php => vod_get_coursemodule_info() 함수에서 처리해서 전달해주셔야 합니다. 
        $options->isOpenTimeCheck = $mod->isOpenTimeCheck ?? false;
        
        $details = '';
        
        // 진도처리를 진행하는 경우
        if ($mod->progress) {
            $periodText = self::getProgressPeriodTextFromProgressInfo($progressInfo);
            
            if (!empty($periodText)) {
                // 진도처리 기간 표시
                $details .= '<span class="text-time ml-1">';
                $details .=     $periodText;
                $details .= '</span>';    
            }
        }
        
        $options->details = self::getCustomDataDetails($mod, $cm, $details);
        
        return $options;
    }
    
    
    /**
     * 모듈별로 details 항목에 추가적으로 표시되어야 할 항목들이 있으면 해당 함수 override 사용해서 $details에 적용해주시면 됩니다. 
     * 
     * @param object $mod
     * @param object $cm
     * @param string $details
     * @return string
     */
    public static function getCustomDataDetails($mod, $cm, $details)
    {
        return $details;
    }
    
}