<?php
namespace local_ubonattend\core;

use context_course;
use stdClass;
use local_ubion\base\Javascript;
use local_ubion\base\Parameter;
use local_ubion\base\Common;
use local_ubion\course\Course;

class Attendance extends \local_ubion\controller\Controller
{
    use \local_ubion\core\traits\TraitCourseUser;

    const GRADE_IDNUMBER = 'online_attendance';
    const GRADE_SCORE = 20;

    const PERCENT_ATTENDANCE = 90;
    const PERCENT_LATENESS = 50;

    // !!! 주의 !!!
    // 추가 출석 상태값 부여시 0값은 사용하면 안됩니다.
    const CODE_ATTENDANCE = 1;
    const CODE_ABSENCE = 2;
    const CODE_LATE = 3;

    const VIEWTYPE_PROGRESS = 1;
    const VIEWTYPE_ATTENDANCE = 2;

    protected $config = null;
    protected $courseid = null;
    protected $pluginname = 'local_ubonattend';


    public function __construct($courseid)
    {
        $this->courseid = $courseid;
        $this->config = $this->getConfig();
    }

    /**
     * 사이트내 온라인 출석부 사용여부
     * @return number;
     */
    public function isAttendance()
    {
        return get_config('local_ubonattend', 'use');
    }

    /**
     * 사이트내 지각 기능 사용여부
     *
     * @return number;
     */
    public function isLate()
    {
        return get_config('local_ubonattend', 'use_late');
    }

    /**
     * 강좌내 온라인 출석부 사용여부
     */
    public function isAttendanceCourse()
    {
        global $DB;

        if ($this->isAttendance()) {
            return $this->config->used;
        } else {
            return false;
        }
    }


    /**
     * 강좌 지각 기능 사용여부
     *
     * @return boolean
     */
    public function isLateCourse()
    {
        if ($this->isAttendance() && $this->isLate()) {
            return $this->config->islate;
        }

        return false;
    }


    /**
     * 온라인 출석부 사용여부 및 progresstype 설정 변경
     *
     * @param int $used
     * @param int $isLate
     * @param int $progressType
     */
    public function setAttendanceUsed($used, $isLate, $progressType)
    {
        global $DB;


        $updateConfig = new \stdClass();
        $updateConfig->id = $this->config->id;
        $updateConfig->used = $used;

        // 온라인 출석부를 사용하지 않으면 무조건 지각 기능은 사용안함으로 설정
        if (empty($used)) {
            $isLate = 0;
        }
        $updateConfig->islate = $isLate;
        $updateConfig->progresstype = $progressType;
        $DB->update_record('local_ubonattend_config', $updateConfig);

        // 캐시 재정의
        $config = $this->getConfig(true);
    }


    /**
     * 출석부 셋팅값을 리턴해줍니다.
     *
     * @param boolean $rebuild
     * @return \stdClass
     */
    public function getConfig($rebuild=false)
    {
        global $DB;

        $cache = \cache::make('local_ubonattend', 'config');
        if ($rebuild) {
            $cache->delete($this->courseid);
        }

        $config = $cache->get($this->courseid);

        if ($config === false) {
            $query = "SELECT * FROM {local_ubonattend_config} WHERE courseid = :courseid";
            $param = array('courseid' => $this->courseid);

            $config = $DB->get_record_sql($query, $param);

            // 디비에서 조회된 내용이 없는 경우
            if (empty($config)) {
                $config = $this->setDefaultConfig();
            }

            // 캐시가 생성된 후 사이트 지각 사용여부가 변경되었을수도 있기 때문에
            // 지각 기능을 사용하지 않는다면 islate값은 0으로 전달되어야 함.
            // 또는 디비에서 강제로 지각 기능을 활성화 시켜놓는 경우도 있음.
            if (! $this->isLate()) {
                $config->islate = 0;
            }


            $cache->set($this->courseid, $config);

            // 싱글톤 형태로 운영되기 때문에 꼭 config값을 재 반영해줘야됩니다.
            $this->config = $config;
        }

        return $config;
    }


    /**
     * 강좌별 출석부 기본 설정값
     *
     *
     * @return \stdClass
     */
    public function setDefaultConfig()
    {
        global $DB;

        // 기본 설정값 가져오기
        $attendanceConfig = get_config('local_ubonattend');

        // 강좌내 온라인 출석부 사용여부
        $used = $attendanceConfig->use_in_course ?? 0;

        // 강좌내 지각 기본 활성화 여부
        $isLate = $attendanceConfig->use_late_in_course ?? 0;

        $gradename = get_string('gradename', $this->pluginname);
        $gradescore= $attendanceConfig->gradescore ?? self::GRADE_SCORE;
        $minscore= $attendanceConfig->minscore ?? 0;
        $lateness= $attendanceConfig->lateness ?? -1;
        $absence= $attendanceConfig->absence ?? -2;
        $gradeapply = 0;    // 기본 값 설정이기 때문에 성적 반영여부는 0으로 설정되어야 함.

        $progresstype = $attendanceConfig->progress_type ?? \local_ubonattend\Progress::PROGRESSTYPE_PERCENT;


        $config = new \stdClass();
        $config->courseid = $this->courseid;
        $config->used = $used;
        $config->islate = $isLate;


        // 성적 관련 항목은 사이트 설정의 기본값으로 설정
        // 성적 반영을 위해서는 성적 항목페이지 접근해야되기 때문에 기본값으로 저장해도 상관 없음.
        $config->gradename = $gradename;
        $config->gradescore = $gradescore;
        $config->minscore= $minscore;
        $config->lateness= $lateness;
        $config->absence= $absence;
        $config->gradeapply = $gradeapply;
        $config->progresstype = $progresstype;

        $config->id = $DB->insert_record('local_ubonattend_config', $config);

        // 변경된 정보로 캐시 새로 고침해줘야됨.
        return $this->getConfig(true);
    }

    /**
     * 성적 관련 점수 저장
     *
     * @param int $configid
     * @param string $gradename
     * @param int $gradescore
     * @param int $minscore
     * @param int $lateness
     * @param int $absence
     * @return stdClass
     */
    public function setConfigScore($configid, $gradename, $gradescore, $minscore, $lateness, $absence)
    {
        global $DB;

        $updateConfig = new \stdClass();
        $updateConfig->id = $configid;
        $updateConfig->gradename = $gradename;
        $updateConfig->gradescore = $gradescore;
        $updateConfig->minscore = $minscore;
        $updateConfig->lateness = $lateness;
        $updateConfig->absence = $absence;

        $DB->update_record('local_ubonattend_config', $updateConfig);

        return $this->getConfig(true);
    }


    /**
     * 탭 HTML
     *
     * @param string $current
     * @return string
     */
    public function getTabHTML($current)
    {
        global $CFG, $OUTPUT;

        $baseurl = $CFG->wwwroot.'/local/ubonattend';

        $html = '';

        if (\local_ubion\course\Course::getInstance()->isProfessor($this->courseid)) {
            $tabs = array();

            $isOnlineAttendance = $this->config->used;

            if ($isOnlineAttendance) {
                $tabs[] = new \tabobject('index', $baseurl.'/index.php?id='.$this->courseid, get_string('tab_status', $this->pluginname));
                $tabs[] = new \tabobject('setting', $baseurl.'/setting.php?id='.$this->courseid, get_string('tab_setting', $this->pluginname));
                $tabs[] = new \tabobject('grade', $baseurl.'/grade.php?id='.$this->courseid, get_string('tab_grade', $this->pluginname));

                $html = '<div class="tabmenu">';
                $html .= $OUTPUT->tabtree($tabs, $current);
                $html .= '</div>';
            } else {
                $html .= $OUTPUT->heading(get_string('tab_progress', $this->pluginname));
            }
        }

        return $html;
    }

    /**
     * 성적 항목 가져오기
     *
     * @return mixed|boolean
     */
    public function getGradeItem()
    {
        global $DB;

        $query = "SELECT
							*
				FROM		{grade_items}
				WHERE		idnumber = :idnumber
				AND			courseid = :courseid";

        return $DB->get_record_sql($query, array('courseid' => $this->courseid, 'idnumber'=>self::GRADE_IDNUMBER));
    }

    /**
     * 성적 항목 추가 (action)
     */
    function doGradeInsert()
    {
        global $CFG;

        $courseid = Parameter::request('id', null, PARAM_INT);
        $itemname = Parameter::request('itemname', null, PARAM_NOTAGS);

        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            )
            ,'itemname' => array(
                'required'
                ,$this->validate()::PLACEHOLDER => get_string('itemname', 'grades')
                ,$this->validate()::PARAMVALUE => $itemname
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            // 권한 검사
            if (! Course::getInstance()->isProfessor($courseid)) {
                Javascript::printAlert(get_string('no_permissions', 'local_ubion'));
            }

            $gradescore = get_config('local_ubonattend', 'gradescore');

            // 오프라인출석부에 설정된 출석부 값이 없다면 기본 성적값으로 입력되어야 함.
            if (empty($gradescore)) {
                $gradescore = self::GRADE_SCORE;
            }

            if ($this->setGradeInsert($itemname, $gradescore, self::GRADE_IDNUMBER)) {
                Javascript::getAlertMove(get_string('save_complete', 'local_ubion'), $CFG->wwwroot.'/local/ubonattend/grade.php?id='.$courseid);
            } else {
                Javascript::printAlert(get_string('error_attendace_grade_add', $this->pluginname));
            }
        }
    }

    /**
     * 성적 항목 추가
     *
     * @param string $name
     * @param int $score
     * @param string $idnumber
     * @return number|boolean
     */
    public function setGradeInsert($name, $score, $idnumber)
    {
        if ($gradeitemid = \local_ubion\grade\Grade::getInstance()->setItemInsert($this->courseid, $name, $score, $idnumber)) {

            $params = array(
                'context' => context_course::instance($this->courseid)
                ,'objectid' => $this->config->id

            );
            $event = \local_ubonattend\event\grade_created::create($params);
            $event->trigger();

            return $gradeitemid;

        } else {
            return false;
        }
    }

    /**
     * 성적 항목 수정 (action)
     */
    function doGradeUpdate()
    {
        global $DB, $CFG;

        $courseid = Parameter::request('id', null, PARAM_INT);
        $itemname = Parameter::request('itemname', null, PARAM_NOTAGS);

        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            )
            ,'itemname' => array(
                'required'
                , $this->validate()::PLACEHOLDER => get_string('itemname', 'grades')
                , $this->validate()::PARAMVALUE => $itemname
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            // 권한 검사
            if (! Course::getInstance()->isProfessor($courseid)) {
                Javascript::printAlert(get_string('no_permissions', 'local_ubion'));
            }

            $attendanceConfig = get_config('local_ubonattend');

            $gradescore = Parameter::request('gradescore', $attendanceConfig->gradescore, PARAM_INT);
            $minscore = Parameter::request('minscore', $attendanceConfig->minscore, PARAM_INT);
            $lateness = Parameter::request('lateness', $attendanceConfig->lateness, PARAM_FLOAT);
            $absence = Parameter::request('absence', $attendanceConfig->absence, PARAM_FLOAT);

            // 출석 최소점수는 출석점수보다 클수는 없음.
            if ($minscore > $gradescore) {
                $minscore = $gradescore;
            }

            // 기존에 등록된 항목이 존재하는지 확인해야됨.
            if ($configid = $DB->get_field_sql("SELECT id FROM {local_ubonattend_config} WHERE courseid = :courseid", array('courseid'=>$courseid))) {
                $this->setConfigScore($configid, $itemname, $gradescore, $minscore, $lateness, $absence);
            } else {
                $config = $this->setDefaultConfig();
                $configid = $config->id;
            }


            $this->setGradeUpdate($configid, $itemname, $gradescore, self::GRADE_IDNUMBER, $minscore, $lateness, $absence);



            Javascript::printAlertMove(get_string('save_complete', 'local_ubion'), $CFG->wwwroot.'/local/ubonattend/grade.php?id='.$courseid);
        }
    }

    /**
     * 성적 항목 수정
     *
     * @param int $configid
     * @param string $name
     * @param int $score
     * @param string $idnumber
     * @param int $minscore
     * @param number $lateness
     * @param number $absence
     */
    public function setGradeUpdate($configid, $name, $score, $idnumber, $minscore, $lateness, $absence)
    {
        $gradeid = null;
        if ($gradeitem = $this->getGradeItem()) {
            $gradeid = \local_ubion\grade\Grade::getInstance()->setItemUpdate($this->courseid, $name, $score, $idnumber, $gradeitem->id);

            $params = array(
                'context' => context_course::instance($this->courseid)
                ,'objectid' => $configid
            );

            $event = \local_ubonattend\event\grade_updated::create($params);
            $event->trigger();
        } else {
            $this->setGradeInsert($name, $score, $idnumber);
        }
    }


    /**
     * 온라인 출석부 가져오기
     *
     * @param int $numsections
     * @param boolean $isDelete
     * @return array
     */
    public function getSections($numsections=null, $isDelete = false)
    {
        global $DB;

        if (empty($numsections)) {
            $course = \local_ubion\course\Course::getInstance()->getCourse($this->courseid);
            $numsections = $course->numsections ?? 0;
        }

        $sections = array();
        if ($numsections > 0) {
            $query = "SELECT
    							online.*
    							,cs.id
    							,cs.section
    							,online.id AS onlineid
    				  FROM		{course_sections} cs
    				  JOIN	    {local_ubonattend} online ON cs.id = online.sectionid
    				  WHERE		cs.course = :courseid
    				  AND 		cs.section > :section
    				  AND		cs.section <= :numsections
    				  ORDER BY 	cs.section";

            $param = array('courseid' => $this->courseid, 'section' => 0, 'numsections'=> $numsections);

            $sections = $DB->get_records_sql($query, $param);

            // 불필요한 데이터를 삭제하려는 경우이며 온라인 출석부 셋팅 페이지에서만 사용됩니다.
            // 예)
            //   - 강좌 백업/복구로 인해 sectionid가 달라지는 경우
            //   - 주차가 16주차였다가 10주차로 변경하는 경우
            //   - 기타 등등.
            if ($isDelete) {

                // 등록된 데이터가 존재하는 경우
                if (!empty($sections)) {
                    // local_ubonattend에 존재하는 모든 데이터 가져오기
                    if ($ubonattendAllSections = $DB->get_records_sql("SELECT * FROM {local_ubonattend} WHERE courseid = :courseid", array('courseid' => $this->courseid))) {

                        // $sections를 반복 돌면서 $ubonattendAllSections 데이터와 확인 후 삭제된 sectionid값이 존재하면 삭제 처리
                        foreach ($sections as $ss) {
                            if (isset($ubonattendAllSections[$ss->onlineid])) {
                                unset($ubonattendAllSections[$ss->onlineid]);
                            }
                        }

                        // $ubonattendAllSections에 존재하는 데이터는 불필요한 데이터임.
                        if (!empty($ubonattendAllSections)) {

                            $transaction = $DB->start_delegated_transaction();
                            foreach ($ubonattendAllSections as $uas) {
                                $DB->delete_records('local_ubonattend', array('id' => $uas->id));
                            }
                            $transaction->allow_commit();
                        }
                    }
                } else {
                    // 등록된 데이터가 없다면 현재 등록된 모든 데이터 삭제
                    $DB->delete_records('local_ubonattend', array('courseid' => $this->courseid));
                }
            }

            // 등록된 주차가 없으면 기본 값 셋팅 후 리턴
            if (empty($sections)) {
                // 디폴트값 설정 후 다시 재조회해야됨.
                $this->setDefaultSections();
                $sections = $this->getSections($numsections);
            } else {
                foreach ($sections as $ss) {
                    $ss->time_start_ymd = date('Y-m-d', $ss->time_start);
                    $ss->time_end_ymd = date('Y-m-d', $ss->time_end);
                    $ss->time_lateness_ymd = date('Y-m-d', $ss->time_lateness);

                }
            }
        }

        return $sections;
    }


    /**
     * 특정 주차에 대한 진도처리 기간 리턴
     *
     * @param int $sectionid => 1, 2주차값이 아닌 mdl_course_sections 테이블의 id값입니다.
     * @return mixed|boolean
     */
    public function getSection($sectionid)
    {
        global $DB;

        // 특정주차의 진도처리 기간정보 가져오기
        // 상황에 따라 캐시화 시켜야될수 있습니다.
        $query = "SELECT * FROM {local_ubonattend} WHERE courseid = :courseid AND sectionid = :sectionid";
        $param = array('courseid' => $this->courseid, 'sectionid' => $sectionid);

        return $DB->get_record_sql($query, $param);
    }


    /**
     * 온라인 출석부 기본 셋팅
     *
     */
    public function setDefaultSections()
    {
        global $DB, $CFG;
        require_once $CFG->dirroot.'/course/lib.php';

        $CCourse = Course::getInstance();
        $config = $this->config;
        $settingConfig = get_config($this->pluginname);

        $time = time();

        // 온라인 출석부를 사용하는 강좌인 경우에만 진행되어야 함.
        if ($config->used) {
            // 지각 기능 사용여부
            $isLate = $config->islate;

            // 강좌가 존재하는 경우
            if ($courseFormat = $CCourse->getCourseFormat($this->courseid)) {
                $course = $courseFormat->get_course();

                // 주차 수
                $numsections = $course->numsections ?? 0;

                // 해당 강좌에 존재하지 않는 주차가 있는지 확인
                if ($numsections > 0) {
                    // 학사 연동에 의해서 온라인 출석부 셋팅이 될수 있기 때문에
                    // course_sections 테이블에 주차 정보가 없으면 생성해줘야됨.
                    course_create_sections_if_missing($course, range(0, $numsections));

                    // 등록된 주차에 대해서 반복을 돌면서 local_ubonattend 셋팅해주기
                    $modinfo = get_fast_modinfo($course);

                    if ($courseSections = $modinfo->get_section_info_all()) {
                        foreach ($courseSections as $cs) {
                            if ($cs->section > 0) {
                                $sectionid = $cs->id;
                                $section = $cs->section;

                                // 시작, 종료, 지각 기본값 선언
                                $timestart = $timeend = $timelateness = null;

                                // printtype값이 설정이 된 경우 (주차포맷은 주별/일별을 따로 설정할수있음)
                                if (isset($course->printtype)) {
                                    // get_section_dates로 가져오면 주차/일자 계산이 완료되었기 때문에 따로 예외처리 할 필요가 없습니다.
                                    // 참고로 get_section_dates 함수는 주차 형태 강좌 포맷(weeks, ubsweeks)에만 존재합니다.
                                    if (method_exists($courseFormat, 'get_section_dates')) {
                                        $dates = $courseFormat->get_section_dates($section);


                                        // 주차, 일별에 따라서 계산이 달라져야되는데..
                                        $timestart = strtotime(date('Y-m-d', $dates->start).' 00:00:00');
                                        $timeend = strtotime(date('Y-m-d', $dates->end).' 23:59:59');

                                        // 일별 형태이면 지각 기간을 + 1일
                                        // 주차 형태이면 지각 기간을 + 1주일
                                        if ($course->printtype == \format_ubsweeks::PRINTTYPE_DAY_FIR || $course->printtype == \format_ubsweeks::PRINTTYPE_DAY_THU) {
                                            // 주차인 경우에는 화면 표시 목적으로 24시간을 빼줘야됨.
                                            $timelateness = $timeend + DAYSECS;
                                        } else {
                                            $timelateness = $timeend + WEEKSECS;
                                        }
                                    }

                                }

                                // timestart가 따로 지정이 되어 있지 않다면 주차로 계산해버림
                                if (empty($timestart)) {
                                    $timestart = $course->startdate + ($section - 1) * WEEKSECS;
                                    $timeend = $timestart + WEEKSECS - 1;
                                    $timelateness = $timeend + WEEKSECS;
                                }

                                // 지각 기능을 사용하지 않으면
                                // 종료일과 지각일을 동일하게 설정하면됨.
                                if (empty($isLate)) {
                                    $timelateness = $timeend;
                                }

                                // 기본 출석인정률, 지각 인정률
                                $attendancePercent = $settingConfig->percent_attendance ?? self::PERCENT_ATTENDANCE;
                                $latenessPercent = $settingConfig->percent_lateness ?? self::PERCENT_LATENESS;


                                $on = new \stdClass();
                                $on->courseid = $this->courseid;
                                $on->sectionid = $sectionid;
                                $on->time_start = $timestart;
                                $on->time_end = $timeend;
                                $on->time_lateness = $timelateness;
                                $on->attendance_percent = $attendancePercent;
                                $on->lateness_percent = $latenessPercent;
                                $on->timecreated = $time;
                                $on->id = $DB->insert_record('local_ubonattend', $on);
                            }
                        }
                    }

                }
            }
        }
    }

    public function doSectionChange()
    {
        global $DB, $CFG;

        $courseid = Parameter::post('id', null, PARAM_INT);

        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number'
                    ,$this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion')
                    ,$this->validate()::PARAMVALUE => $courseid
                )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $CCourse = Course::getInstance();

            if ($course = $CCourse->getCourse($courseid)) {

                // 권한 검사
                if (! Course::getInstance()->isProfessor($courseid)) {
                    Javascript::printAlert(get_string('no_permissions', 'local_ubion'));
                }

                $time = time();

                // 강좌 종료일이 지났는지 확인
                if ($courseUbion = $CCourse->getUbion($courseid)) {
                    if ($time > $courseUbion->study_end) {
                        Javascript::printAlert(get_string('course_end_message', $this->pluginname));
                    }
                }

                $settingConfig = get_config($this->pluginname);

                // 주차 수
                $numsections = $course->numsections ?? 0;

                // 온라인 출석부를 웹서비스 형태로 전달해줄일이 없기 때문에 이곳에서 모두 처리합니다.
                // 현재 등록되어 있는 주차 정보 가져오기
                if ($sections = $this->getSections($numsections)) {
                    foreach ($sections as $ss) {
                        $section = $ss->section;

                        // post로 전달된 데이터 셋팅

                        // 일자는 y-m-d 형태로 전달되기 때문에 unixtime으로 변경해주는 과정이 필요함.
                        $startTime = Parameter::post('start_time_'.$section, null, PARAM_ALPHANUMEXT);
                        $startTime = strtotime($startTime.' 00:00:00');

                        $endTime = Parameter::post('end_time_'.$section, null, PARAM_ALPHANUMEXT);
                        $endTime = strtotime($endTime.' 23:59:59');

                        $attendancePercent = Parameter::post('attendance_percent_'.$section, null, PARAM_INT);


                        // 지각 기능을 사용하는 경우
                        if ($this->config->islate) {
                            $latenessTime = Parameter::post('lateness_time_'.$section, null, PARAM_ALPHANUMEXT);
                            $latenessTime = strtotime($latenessTime.' 23:59:59');

                            $latenessPercent = Parameter::post('lateness_percent_'.$section, null, PARAM_INT);
                        } else {
                            // 지각기능을 사용하지 않으면 지각 관련 항목이 전달되지 않기 때문에
                            // 지각인정 기간은 출석 인정기간
                            // 지각인정률은 디비에 존재하는 값 그대로 설정
                            $latenessTime = $endTime;
                            $latenessPercent = $ss->lateness_percent;
                        }

                        $force = Parameter::post('force_'.$section, 0, PARAM_INT);


                        $on = new \stdClass();
                        $on->time_start = $startTime;
                        $on->time_end = $endTime;
                        $on->attendance_percent = $attendancePercent;
                        $on->time_lateness = $latenessTime;
                        $on->lateness_percent = $latenessPercent;
                        $on->force_attendance = $force;
                        $on->timecreated = $time;

                        // local_ubonattend에 레코드가 없을수도 있음.
                        if (empty($ss->onlineid)) {
                            $on->courseid = $courseid;
                            $on->sectionid = $ss->id;

                            $on->id = $DB->insert_record('local_ubonattend', $on);
                        } else {
                            // 과거 데이터를 old 테이블에 기록해야됨.
                            $oldON = clone $ss;
                            unset($oldON->id);

                            $DB->insert_record('local_ubonattend_old', $oldON);

                            // 현재 테이블에 전달된 정보 반영
                            $on->id = $ss->onlineid;
                            $DB->update_record('local_ubonattend', $on);
                        }
                    }

                    // 각 모듈의 customdata에 진도처리 기간이 저장되기 때문에
                    // 온라인 출석부 설정이 변경되었으면 강좌 캐시부터 삭제해야됨.
                    // 강좌 캐시 삭제
                    rebuild_course_cache($courseid, true);

                    // 진도율 재계산
                    $CProgress = new \local_ubonattend\Progress($courseid);
                    $CProgress->setReCalculation();


                    // 출석부 변경 이벤트 기록
                    $params = array(
                        'context' => context_course::instance($courseid)

                    );
                    $event = \local_ubonattend\event\section_changed::create($params);
                    $event->trigger();


                }


            }

            redirect($CFG->wwwroot.'/local/ubonattend/setting.php?id='.$courseid);
        }
    }


    /**
     * 주차별 사용자 출석 정보 리턴
     *
     * @param string $userInQuery
     * @return array
     */
    public function getSectionUserStatus($userInQuery = null)
    {
        global $DB;

        // 진행 예정중인 주차에 대해서는 출결여부를 가져올 필요가 없음
        // 만약 데이터를 가져오게 된다면 주차가 시작되지 않았음에도 불구하고 결석 처리가 됩니다.
        $query = "SELECT
                            lus.*
                  FROM      {course_sections} cs
                  JOIN      {local_ubonattend} lu ON lu.courseid = cs.course AND lu.sectionid = cs.id
                  JOIN      {local_ubonattend_section} lus ON lus.courseid = lu.courseid AND lus.sectionid = lu.sectionid
                  WHERE     cs.course = :courseid
                  AND       lu.time_start <= :time";

        $param = array('courseid' => $this->courseid, 'time' => time());

        if (!empty($userInQuery)) {
            if (is_number($userInQuery)) {
                $query .= " AND userid = :userid";
                $param['userid'] = $userInQuery;
            } else {
                $query .= " AND userid IN (".$userInQuery.")";
            }
        }


        // error_log($query);
        // error_log(print_r($param, true));
        return $DB->get_records_sql($query, $param);
    }


    /**
     * 모듈별 사용자 출석 정보 리턴
     *
     * @param string $userInQuery
     * @return array
     */
    public function getModuleUserStatus($userInQuery = null)
    {
        global $DB;

        $query = "SELECT
                            *
                  FROM      {local_ubonattend_module}
                  WHERE     courseid = :courseid";

        $param = array('courseid' => $this->courseid);

        if (!empty($userInQuery)) {
            if (is_number($userInQuery)) {
                $query .= " AND userid = :userid";
                $param['userid'] = $userInQuery;
            } else {
                $query .= " AND userid IN (".$userInQuery.")";
            }
        }


        return $DB->get_records_sql($query, $param);
    }

    /**
     * 사용자의 모듈별 출석 상태값 리턴
     *
     * @param int $userid
     * @return array
     */
    public function getUserModuleStatus($userid)
    {
        global $DB;

        $query = "SELECT cmid, status, progress, progress_time FROM {local_ubonattend_module} WHERE courseid = :courseid AND userid = :userid";
        $param = array('courseid' => $this->courseid, 'userid' => $userid);

        return $DB->get_records_sql($query, $param);
    }


    /**
     * 출석 상태에 맞는 심볼 기호 리턴
     *
     * @param string $status
     * @param boolean $isNbsp
     * @return string
     */
    public function getStatusSymbol($status, $isNbsp = false)
    {
        $symbol = "";

        if ($status == self::CODE_ATTENDANCE) {
            $symbol = 'O';
        } else if ($status == self::CODE_LATE) {
            $symbol = '▲';
        } else if ($status == self::CODE_ABSENCE) {
            $symbol = 'X';
        }

        if (empty($symbol) && $isNbsp) {
            $symbol = '&nbsp;';
        }

        return $symbol;
    }


    /**
     * 강좌 주차 정보 리턴
     * $modinfo->get_section_info_all() 값에서 isAllowModule property를 신규로 추가함
     *
     * @param \course_modinfo $modinfo
     * @param int $numsections
     * @return \section_info[]
     */
    public function getCourseSections(\course_modinfo $modinfo = null, $numsections = null)
    {
        if (empty($numsections)) {
            $course = \local_ubion\course\Course::getInstance()->getCourse($this->courseid);
            $numsections = $course->numsections ?? 0;
        }

        if (empty($modinfo)) {
            $modinfo = get_fast_modinfo($this->courseid);
        }

        $allowModules = \local_ubonattend\Progress::getAllowModules();

        $courseSections = $modinfo->get_section_info_all();

        // 0주차는 사용하지 않음
        unset($courseSections[0]);

        foreach ($courseSections as $section) {
            if ($section->visible & $section->section > 0 && $section->section <= $numsections) {
                $section->allowModules = array();

                $cmids = $modinfo->sections[$section->section] ?? null;

                // 해당 주차에 등록된 항목이 존재하는 경우
                if (!empty($cmids)) {
                    foreach ($cmids as $cmid) {
                        $cm = $modinfo->get_cm($cmid);

                        if ($cm->visible && in_array($cm->modname, $allowModules)) {
                            // $cm자체를 반입할려고 했으나, 객체 크기가 많이 증가되는 관계로 cmid값만 저장되도록 변경했으며
                            // get_cm()으로 정보 가져오면 됩니다.
                            $section->allowModules[] = $cm->id;
                        }
                    }
                }
            }
        }


        return $courseSections;
    }


    public function doAttempts()
    {
        global $USER, $DB;

        $courseid = Parameter::post('id', null, PARAM_INT);
        $cmid = Parameter::post('cmid', null, PARAM_INT);
        $userid = Parameter::post('userid', null, PARAM_INT);

        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            )
            ,'cmid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('cmid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $CCourse = \local_ubion\course\Course::getInstance();

            // 교수자 권한이 없으면 본인 데이터만 확인 가능함.
            if (!$CCourse->isProfessor($courseid)) {
                $userid = $USER->id;
            }

            $CProgress = new \local_ubonattend\Progress($courseid);

            $modinfo = get_fast_modinfo($courseid);
            $cm = $modinfo->get_cm($cmid);
            $customdata = $cm->customdata;

            $playtime = $customdata->playtime;
            $intervalTime = $CProgress->getIntervalTime($playtime, false);

            $timeopen = $customdata->timeopen;
            // 종료, 지각 기간에 interval time을 더해줘야됨.
            $timeclose = $customdata->timeclose + $intervalTime;
            $timelateness = $customdata->timelateness + $intervalTime;

            $html = '';

            if ($progressDetail = $CProgress->getUserModuleProgressDetail($cm->modname, $cm->instance, $userid)) {

                // $strOutOfProgress = get_string('outofprogress', $this->pluginname);

                // getUserModuleProgressDetail에서는 groupby로 가져오기 때문에 단일 레코드 다시 조회
                $trackInfo = $DB->get_record_sql("SELECT * FROM {".$cm->modname."_track} WHERE ".$cm->modname."id = :moduleid AND userid = :userid", array('moduleid'=>$cm->instance, 'userid' => $userid));


                $allOpenTime = 0;           // 전체 뷰어 오픈시간
                $cumulativeOpenTime = 0;    // 누적 오픈 시간
                $maxPosition = 0;           // 최대 재생위치

                $progress = $totalprogress = '0%';

                // 진도체크 방식(진도율, 열람시간, 열람여부)에 따라
                // 사용되는 property가 달라짐
                if ($this->config->progresstype == $CProgress::PROGRESSTYPE_OPEN) {
                    $progress = '<span title="'.$trackInfo->progress_time.'">'.Common::getGMDate($trackInfo->progress_time).'</span>';
                    $totalprogress = '<span title="'.$trackInfo->totalprogress_time.'">'.Common::getGMDate($trackInfo->totalprogress_time).'</span>';
                } else {
                    // 진도율 : position값을 가지고 퍼센트값을 구함
                    // 열람시간 : 뷰어 열람시간으로 퍼센트값을 구함
                    $check = $trackInfo->progress;
                    $checkLateness = $trackInfo->totalprogress;

                    // 열람시간으로 체크하는 경우
                    // econtents 같은 경우에는 무조건 열람 시간으로 체크 해야됨.
                    if ($this->config->progresstype == $CProgress::PROGRESSTYPE_OPENTIME || $customdata->isOpenTimeCheck) {
                        $check = $trackInfo->progress_time;
                        $checkLateness = $trackInfo->totalprogress_time;

                    }

                    if ($check > 0) {
                        $progress = $this->getPercent($check, $playtime).'%';
                        $progress .= '<br/>';
                        $progress .= '(<small title="'.$check.'">'.Common::getGMDate($check).'</small>)';
                    }

                    if ($checkLateness > 0) {
                        $totalprogress = $this->getPercent($checkLateness, $playtime).'%';
                        $totalprogress .= '<br/>';
                        $totalprogress .= '(<small title="'.$checkLateness.'">'.Common::getGMDate($checkLateness).'</small>)';
                    }
                }

                // 온라인 출석부 사용여부에 따라 타이틀값이 달라짐
                // 사용 : 출석 인정시간
                // 미사용 : 학습 인정시간
                if ($this->isAttendanceCourse()) {
                    $totalAttendanceTitle = get_string('total_attendancetime', $this->pluginname);
                } else {
                    $totalAttendanceTitle = get_string('time_recognized', $this->pluginname);
                }



                // 1. 진도처리 기간외 학습시간 항목 추가해줘야됨
                // 2. 출석인정시간, 지각인정시간, 진도처리 기간외 학습시간 항목을 상단(시작일, 출석 인정기간, 지각 인정기간) 항목으로 옴겨놓기
                // 전체 열람 시간 구하기
                foreach ($progressDetail as $pd) {
                    $openTime = $pd->timeend - $pd->timestart;
                    $allOpenTime += $openTime;
                }

                $maxProgressText = $progress;
                $absenceTime = $trackInfo->progress_time;

                // 학습인정시간 (출석인정시간)
                $html .= '<tr>';
                $html .=    '<th colspan="4" class="text-right align-middle"><span class="text-primary">'.$totalAttendanceTitle.'</span></th>';
                $html .=    '<td class="text-center" title="'.$trackInfo->progress_time.'">'.Common::getGMDate($trackInfo->progress_time).'</td>';
                $html .=    '<td class="text-center">';
                $html .=        $progress;
                $html .=    '</td>';
                $html .= '</tr>';

                // 지각 기능을 사용하는 경우
                if ($this->isLateCourse()) {

                    // totalprogress는 출서기간까지 포함이 되는데
                    // 총 열람 시간을 구할때
                    // 출석 인정시간 + 지각 인정시간 + 기간외 학습시간을 더한 값이 총 열람시간으로 판단하는 경우가 있음
                    // 이 부분이 헷갈리는 경우가 많기 때문에
                    // 지각 인정기간은 출석 인정기간을 제외한 값만 표시되도록 조치함.
                    // 기존 : totalprogress_time을 그대로 출력함
                    // 변경 : totalprogress_time - progress_time
                    $totalprogressTime = $trackInfo->totalprogress_time - $trackInfo->progress_time;
                    $html .= '<tr>';
                    $html .=    '<th colspan="4" class="text-right align-middle"><span class="text-warning">'.get_string('total_latenesstime', $this->pluginname).'</span></th>';
                    $html .=    '<td class="text-center" title="'.$totalprogressTime.'">'.Common::getGMDate($totalprogressTime).'</td>';
                    $html .=    '<td class="text-center">'.$totalprogress.'</td>';
                    $html .= '</tr>';

                    $absenceTime = $trackInfo->totalprogress_time;
                    $maxProgressText = $totalprogress;
                }


                // 총 열람시간
                $allEtcTime = $allOpenTime - $absenceTime;
                $html .= '<tr>';
                $html .=    '<th colspan="4" class="text-right align-middle"><span class="text-danger">'.get_string('total_etc', $this->pluginname).'</span></th>';
                $html .=    '<td class="text-center" title="'.$allEtcTime.'">'.Common::getGMDate($allEtcTime).'</td>';
                $html .=    '<td class="text-center">-</td>';
                $html .= '</tr>';

                $html .= '<tr class="borderbottombold">';
                $html .=    '<th colspan="4" class="text-right align-middle">'.get_string('total_all', $this->pluginname).'</th>';
                $html .=    '<td class="text-center" title="'.$allOpenTime.'">'.Common::getGMDate($allOpenTime).'</td>';
                $html .=    '<td class="text-center">';
                //$html .=        $maxProgressText;
                $html .=        '-';
                $html .=    '</td>';
                $html .= '</tr>';


                // 클로져 함수
                $closure = function($position, $openTime) use(& $cumulativeOpenTime, & $maxPosition, $CProgress, $playtime) {
                    // 누적 뷰어 열람시간
                    $cumulativeOpenTime += $openTime;

                    // 최대 재생 위치
                    if ($position > $maxPosition) {
                        $maxPosition = $position;
                    }

                    // 열람 여부로 판단할때에는 열람시간만 표시
                    // 열람 시간으로 판단할때에는 퍼센트까지 표시
                    if ($this->config->progresstype == $CProgress::PROGRESSTYPE_OPEN) {
                        $cumulativeOpenTimeText = '<span title="'.$cumulativeOpenTime.'">'.Common::getGMDate($cumulativeOpenTime).'</span>';
                    } else {
                        $cumulativeOpenTimeText = $this->getPercent($cumulativeOpenTime ,$playtime).'%<br/><small title="'.$cumulativeOpenTime.'">('.Common::getGMDate($cumulativeOpenTime).')</small>';
                    }

                    // 최대 재생시간 관련 텍스트
                    $maxPositionText = $this->getPercent($maxPosition, $playtime).'%<br/><small title="'.$maxPosition.'">('.Common::getGMDate($maxPosition).')</small>';


                    // 출석인정시간, 학습인정시간 관련 변수 설정
                    // 열람여부, 열람 시간인 경우에는 뷰어 열람 시간을 가지고 계산하고,
                    // 진도율이면 position값을 설정
                    if ($this->config->progresstype == $CProgress::PROGRESSTYPE_OPEN || $this->config->progresstype == $CProgress::PROGRESSTYPE_OPENTIME) {
                        $cumulativeText = $cumulativeOpenTimeText;
                    } else {
                        $cumulativeText = $maxPositionText;
                    }

                    return $cumulativeText;
                };


                // 상세 정보 출력
                foreach ($progressDetail as $pd) {


                    // 화면 표시용 텍스트
                    $cumulativeOpenTimeText = $maxPositionText = $cumulativeText = null;

                    $trclass = '';
                    $cumulativeText = null;             // 누적 열람시간, 누적 최대학습구간


                    $isProgressPeriod = false;      // 진도처리 기간인지 확인
                    $isSomeTrackDetailSelect = false;
                    $someTrackDetailInfo = null;

                    if ($pd->timestart >= $timeopen) {

                        if ($pd->timeend <= $timeclose) {
                            // 진도 기간내 학습
                            $isProgressPeriod = true;
                        } else if ($pd->timeend <= $timelateness) {
                            // 출석에서 지각으로 변경되는 시점인 경우
                            if ($pd->timestart > $timeclose && $pd->timeend > $timeclose) {
                                $trclass = "warning";
                                $isProgressPeriod = true;
                            } else {
                                $trclass = "info";
                                $isProgressPeriod = true;
                            }
                        } else if ($pd->timestart <= $timeclose || $pd->timestart <= $timelateness) {
                            // 학생이 동영상을 보기 시작한 시점이 진도처리기간 종료일과 겹치는 경우가 있음
                            $trclass = "info";
                            $isSomeTrackDetailSelect = true;
                        } else {
                            // 출석, 지각 기간외 학습
                            $trclass = "danger";
                        }
                    } else {
                        // 학생이 동영상을 보기 시작한 시점은 진도처리기간이 아니였지만
                        // 동영상 종료전 시점에는 진도처리기간내에 존재하는 경우가 있음
                        if ($pd->timeend > $timeopen && ($pd->timestart <= $timeclose || $pd->timestart <= $timelateness)) {
                            $trclass = "info";
                            $isSomeTrackDetailSelect = true;
                        } else {
                            $trclass = "danger";
                        }
                    }

                    // 회차에 여러 출석 상태값이 담겨지는 경우
                    if ($isSomeTrackDetailSelect) {
                        $someTrackDetailInfo = $this->getSomeTrackDetailInfo($CProgress, $cm->modname, $trackInfo->id, $pd->attempt, $timeopen, $timeclose, $timelateness);
                    }

                    $isSomeTrackDetailInfo = false;
                    $rowspan = null;
                    // 특정 회차에 대해서 세분화 표시해야되는 경우
                    if (!empty($someTrackDetailInfo)) {
                        $isSomeTrackDetailInfo = true;
                        if ($someTrackDetailInfo->rowspan > 1) {
                            $rowspan = ' rowspan = "'.$someTrackDetailInfo->rowspan.'"';
                        }
                    } else {

                        // 열람시간
                        $openTime = $pd->timeend - $pd->timestart;

                        // 진도처리 기간인 경우 진도율/열람시간 설정
                        if ($isProgressPeriod) {
                            $cumulativeText = $closure($pd->progress, $openTime);
                        }
                    }


                    $html .= '<tr class="'.$trclass.'">';
                    $html .=    '<td class="text-center" '.$rowspan.'>'.$pd->attempt.'</td>';

                    // IP
                    $ip = $pd->ip;
                    if (!empty($pd->device)) {
                        $ip .= '<br/>('.$pd->device.')';
                    }
                    $html .=    '<td class="text-center" '.$rowspan.'>';
                    $html .=        $ip;
                    $html .=    '</td>';

                    // 시작/종료시간
                    $html .=    '<td class="text-center" title="'.$pd->timestart.'" '.$rowspan.'>'.Common::getUserDate($pd->timestart).'</td>';
                    $html .=    '<td class="text-center" title="'.$pd->timestart.'" '.$rowspan.'>'.Common::getUserDate($pd->timeend).'</td>';

                    // 회차에 대해서 출석/지각/기간외 학습으로 세분화 해서 표시해야되는 경우가 있음
                    // 세분화 할 필요가 없다면 해당 회차에 대한 값을 그대로 출력
                    if ($isSomeTrackDetailInfo) {
                        $isFirst = true;
                        foreach ($someTrackDetailInfo as $propertyName => $std) {
                            if ($propertyName != 'rowspan') {
                                if (! $std->isnodata) {

                                    if (!$isFirst) {
                                        $html .= '<tr class="'.$trclass.'">';
                                    }

                                    $title = null;
                                    $tdClass = "";
                                    if ($propertyName == 'attendance') {
                                        $title = get_string('sometrack_attendance', $this->pluginname);
                                        $cumulativeText = $closure($std->progress, $std->time);
                                    } else if ($propertyName == 'lateness') {
                                        $title = get_string('sometrack_lateness', $this->pluginname);
                                        $cumulativeText = $closure($std->progress, $std->time);
                                    } else {
                                        $title = get_string('sometrack_absence', $this->pluginname);
                                        $cumulativeText = '-';
                                        $tdClass = "text-danger";
                                    }

                                    $html .=    '<td class="text-center '.$tdClass.'">';
                                    $html .=        $title.'<span class="" title="'.$std->time.'">'.Common::getGMDate($std->time).'</span>';
                                    $html .=    '</td>';

                                    $html .=    '<td class="text-center p-1 '.$tdClass.'">'.$cumulativeText.'</td>';

                                    if (!$isFirst) {
                                        $html .= '</tr>';
                                    }

                                    $isFirst = false;
                                }
                            }
                        }

                    } else {
                        $html .=    '<td class="text-center" title="'.$openTime.'">';
                        $html .=        Common::getGMDate($openTime);
                        $html .=    '</td>';
                        $html .=    '<td class="text-center">';
                        $html .=        $cumulativeText;
                        $html .=    '</td>';
                    }

                    $html .= '</tr>';
                }

            } else {
                $html .= '<tr>';
                $html .=    '<td colspan="6" class="p-4">'.get_string('no_data', $this->pluginname).'</td>';
                $html .= '</tr>';
            }




            if (Parameter::isAajx()) {
                // 모듈 진도처리 기간 정보 가져오기
                $moduleInfoTable = $this->getModuleInfoTable($cm, $intervalTime);

                // 진도율이면 누적 최대 학습구간이 표시되어야 함
                // 단 강제로 열람시간으로 진행되어야 하는 모듈(econtents)은 제외해야됨
                if ($this->config->progresstype == \local_ubonattend\Progress::PROGRESSTYPE_PERCENT && !$customdata->isOpenTimeCheck) {
                    $cumulativeTitle = get_string('cumulative_maxposition', $this->pluginname);
                    $cumulativeHelp = get_string('cumulative_maxposition_help', $this->pluginname);
                } else {
                    $cumulativeTitle = get_string('cumulative_opentime', $this->pluginname);
                    $cumulativeHelp = get_string('cumulative_opentime_help', $this->pluginname);
                }

                Javascript::printJSON(
                    array(
                        'code'=>Javascript::getSuccessCode()
                        ,'msg' => get_string('view_complete', 'local_ubion')
                        ,'html'=>$html
                        ,'title'=>$cm->name
                        ,'cumulativeTitle' => $cumulativeTitle
                        ,'cumulativeHelp' => $cumulativeHelp
                        ,'moduleInfoTable' => $moduleInfoTable
                    )
                );
            } else {
                return $html;
            }
        }
    }


    /**
     * 모듈의 진도처리 기간 정보를 html table 형태로 출력
     *
     * @param \cm_info $cm
     * @param int $intervalTime
     * @return string
     */
    public function getModuleInfoTable(\cm_info $cm, $intervalTime)
    {
        $customdata = $cm->customdata;

        $strGracePeriodDesc = get_string('grace_period_desc', $this->pluginname);
        $strGracePeriod = get_string('grace_period', $this->pluginname);
        $timeopenText = $timecloseText = $timelatenessText = get_string('no_time_limit', $this->pluginname);



        if ($customdata->timeopen > 0) {
            $timeopenText = Common::getUserDate($customdata->timeopen);
        }

        if ($customdata->timeclose > 0) {
            $timecloseText = Common::getUserDate($customdata->timeclose).' <span class="text-info ml-2">'.$strGracePeriod.' : '.Common::getUserDate($customdata->timeclose + $intervalTime).'</span>';
        }

        if ($customdata->timelateness > 0) {
            $timelatenessText = Common::getUserDate($customdata->timelateness).' <span class="text-info ml-2">'.$strGracePeriod.' : '.Common::getUserDate($customdata->timelateness + $intervalTime).'</span>';
        }



        $html = '<table class="table table-bordered table-coursemos mb-1">';
        $html .=     '<colgroup>';
        $html .=         '<col class="wp-150" />';
        $html .=         '<col />';
        $html .=     '</colgroup>';
        $html .=     '<tbody>';
        $html .=         '<tr>';
        $html .=             '<th class="text-right">'.get_string('attendance_start', $this->pluginname).'</th>';
        $html .=             '<td>'.$timeopenText.'</td>';
        $html .=         '</tr>';
        $html .=         '<tr>';
        $html .=             '<th class="text-right">'.get_string('attendance_end', $this->pluginname).'</th>';
        $html .=             '<td>'.$timecloseText.'</td>';
        $html .=         '</tr>';

        if ($this->isLateCourse()) {
            $html .=     '<tr>';
            $html .=         '<th class="text-right">'.get_string('attendance_lateness', $this->pluginname).'</th>';
            $html .=         '<td>'.$timelatenessText.'</td>';
            $html .=     '</tr>';
        }

        // 진도처리 방식
        if ($this->config->progresstype == \local_ubonattend\Progress::PROGRESSTYPE_OPENTIME) {
            $progressTypeText = get_string('progresstype_opentime', $this->pluginname);
        } else if ($this->config->progresstype == \local_ubonattend\Progress::PROGRESSTYPE_OPEN) {
            $progressTypeText = get_string('progresstype_open', $this->pluginname);
        } else {
            $progressTypeText = get_string('progresstype_progress', $this->pluginname);
        }

        if ($customdata->isOpenTimeCheck) {
            $progressTypeText = '<span class="text-danger">'.get_string('progresstype_override_opentime', $this->pluginname).'</span>';
        } else {
            $progressTypeText = '<strong>'.$progressTypeText.'</strong>';
        }

        $html .=        '<tr>';
        $html .=            '<th class="text-right">'.get_string('progresstype', $this->pluginname).'</th>';
        $html .=            '<td>'.$progressTypeText.'</td>';
        $html .=        '</tr>';
        $html .=    '</tbody>';
        $html .= '</table>';
        $html .= '<p class="text-info mb-4">※ '.get_string('grace_period_desc', $this->pluginname).'</p>';

        return $html;
    }

    public function getTrackBackgroundDesc($isSome=true)
    {
        $html = get_string('backgroundcolor', $this->pluginname).' : ';
        $html .= '<span class="label label-default">'.get_string('backgroundcolor_attendance', $this->pluginname).'</span>';
        $html .= '<span class="label label-warning ml-1">'.get_string('backgroundcolor_late', $this->pluginname).'</span>';
        $html .= '<span class="label label-danger ml-1">'.get_string('backgroundcolor_outofprogress', $this->pluginname).'</span>';

        if ($isSome) {
            $html .= '<span class="label label-info ml-1">'.get_string('backgroundcolor_some_track', $this->pluginname).'</span>';
        }

        return $html;
    }


    /**
     * 회차를 세분화 해서 출력해야되는 경우 출석/지각/기간외 시간 및 진도율 정보를 리턴해줍니다.
     *
     * @param \local_ubonattend\Progress $CProgress
     * @param string $modname
     * @param int $trackid
     * @param int $attempt
     * @param int $timeopen
     * @param int $timeclose
     * @param int $timelateness
     * @return \stdClass
     */
    public function getSomeTrackDetailInfo(\local_ubonattend\Progress $CProgress, $modname, $trackid, $attempt, $timeopen, $timeclose, $timelateness)
    {

        $isLate = $this->isLateCourse();

        $default = new \stdClass();
        $default->time = 0;
        $default->progress = 0;
        $default->isnodata = true;

        $someTracks = new \stdClass();
        $someTracks->rowspan = 0;
        $someTracks->attendance = clone $default;
        $someTracks->lateness = clone $default;
        $someTracks->absence = clone $default;


        // 해당 회차의 전체 기록
        if ($someTotal = $CProgress->getUserSomeTrackDetail($modname, $trackid, $attempt)) {

            $totalOpenTime = $someTotal->timeend - $someTotal->timestart;

            // 출석 기간내
            $some = $CProgress->getUserSomeTrackDetail($modname, $trackid, $attempt, $timeopen, $timeclose);

            if ($some->cnt > 0) {
                $someTracks->rowspan++;

                $someTracks->attendance->time = ($some->timeend - $some->timestart);
                $someTracks->attendance->progress = $some->progress;
                $someTracks->attendance->isnodata = false;
            }


            if ($isLate) {
                // 지각 기간내
                $some = $CProgress->getUserSomeTrackDetail($modname, $trackid, $attempt, $timeopen, $timelateness);

                if ($some->cnt > 0) {
                    $someTracks->rowspan++;

                    $someTracks->lateness->time = ($some->timeend - $some->timestart);
                    $someTracks->lateness->progress = $some->progress;
                    $someTracks->lateness->isnodata = false;
                }
            }


            if ($isLate) {
                $someTracks->absence->time = $totalOpenTime - $someTracks->lateness->time;


                // 출석기간내 학습한 기록이 있다면 출석 - 지각 해줘야됨.
                if (! $someTracks->attendance->isnodata) {
                    $someTracks->lateness->time -= $someTracks->attendance->time;
                }
            } else {
                $someTracks->absence->time = $totalOpenTime - $someTracks->attendance->time;
            }


            if ($someTracks->absence->time > 0) {
                $someTracks->absence->isnodata = false;
                $someTracks->rowspan++;
            }

        }

        return $someTracks;
    }

    /**
     * 허용되는 수동인정 값
     *
     * @return number[]
     */
    public function getAllowRecognition()
    {
        // 0 값은 인정 취소입니다.
        return array(
            0
            ,\local_ubonattend\Attendance::CODE_ABSENCE
            ,\local_ubonattend\Attendance::CODE_ATTENDANCE
            ,\local_ubonattend\Attendance::CODE_LATE
        );
    }

    public function doRecognition()
    {
        global $USER;

        $courseid = Parameter::post('id', null, PARAM_INT);
        $cmid = Parameter::post('cmid', null, PARAM_INT);
        $userid = Parameter::post('userid', null, PARAM_INT);
        $recognition = Parameter::post('recognition', -1, PARAM_INT);

        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            )
            ,'cmid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('cmid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $CCourse = \local_ubion\course\Course::getInstance();

            // 교수자 권한이 없으면 처리 불가
            if (!$CCourse->isProfessor($courseid)) {
                Javascript::printAlert(get_string('no_permissions', 'local_ubion'));
            }

            // 허용 인정 상태값
            $allowRecognition = $this->getAllowRecognition();

            // 인정값이 전달되지 않았거나, 허용되지 않는 인정 상태값이 전달된 경우
            if (!in_array($recognition, $allowRecognition)) {
                Javascript::printAlert(get_string('no_data_recognition', $this->pluginname));
            }

            // 지각 기능을 사용하지 않는데 지각 인정값이 전달된 경우
            if (!$this->isLateCourse() && $recognition == \local_ubonattend\Attendance::CODE_LATE) {
                Javascript::printAlert(get_string('no_save_recognition_lateness', $this->pluginname));
            }


            $modinfo = get_fast_modinfo($courseid);
            $cm = $modinfo->get_cm($cmid);

            $CProgress = new \local_ubonattend\Progress($courseid);

            $CProgress->setUserRecognition($userid, $cmid, $recognition);

            Javascript::printAlert(get_string('save_complete', 'local_ubion'), true);
        }
    }


    public function doDetails()
    {
        global $DB, $USER;

        $courseid = Parameter::post('id', null, PARAM_INT);
        $cmid = Parameter::post('cmid', null, PARAM_INT);
        $userid = Parameter::post('userid', null, PARAM_INT);

        $validate = $this->validate()->make(array(
            'courseid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('courseid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            )
            ,'cmid' => array(
                'required', 'number'
                ,$this->validate()::PLACEHOLDER => get_string('cmid', 'local_ubion')
                ,$this->validate()::PARAMVALUE => $courseid
            )
        ));

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            $CCourse = \local_ubion\course\Course::getInstance();

            // 교수자 권한이 없으면 본인 데이터만 확인 가능함.
            if (!$CCourse->isProfessor($courseid)) {
                $userid = $USER->id;
            }

            $CProgress = new \local_ubonattend\Progress($courseid);

            $modinfo = get_fast_modinfo($courseid);
            $cm = $modinfo->get_cm($cmid);

            $customdata = $cm->customdata;

            $playtime = $customdata->playtime;
            $intervalTime = $CProgress->getIntervalTime($playtime, false);

            $timeopen = $customdata->timeopen;
            // 종료, 지각 기간에 interval time을 더해줘야됨.
            $timeclose = $customdata->timeclose + $intervalTime;
            $timelateness = $customdata->timelateness + $intervalTime;


            // 뷰어 열람 횟수 가져오기
            $query = "SELECT * FROM {".$cm->modname."_track} WHERE ".$cm->modname."id = :moduleid AND userid = :userid";
            $param = array('userid' => $userid, 'moduleid' => $cm->instance);

            $html = '';
            $isNoData = false;
            $trackid = null;
            if ($trackInfo = $DB->get_record_sql($query, $param)) {
                $trackid = $trackInfo->id;

                // 상세 기록 가져오기
                if ($trackDetails = $DB->get_records_sql("SELECT * FROM {".$cm->modname."_track_detail} WHERE track = :trackid ORDER BY timetrack", array('trackid' => $trackInfo->id))) {
                    $oldAttempt = 0;
                    foreach($trackDetails as $td) {
                        $border = (!empty($oldAttempt) && $oldAttempt != $td->attempt) ? 'bordertopbold' : '';

                        $backgroundcolor = 'danger';
                        if ($td->timetrack >= $timeopen) {
                            if ($td->timetrack <= $timeclose) {
                                $backgroundcolor = '';
                            } else if ($td->timetrack <= $timelateness) {
                                $backgroundcolor = ' warning';
                            }
                        }

                        $class = $border.' '.$backgroundcolor;

                        $html .= '<tr class="'.$class.'">';
                        $html .= 	'<td class="text-center">'.$td->attempt.'</td>';
                        $html .= 	'<td class="text-center">'.$td->state.'</td>';
                        $html .= 	'<td class="text-center" title="'.$td->positionold.'">'.Common::getGMDate($td->positionold).'</td>';
                        $html .= 	'<td class="text-center" title="'.$td->positionnew.'">'.Common::getGMDate($td->positionnew).'</td>';
                        $html .= 	'<td class="text-center" title="'.$td->timetrack.'">'.Common::getUserDate($td->timetrack).'</td>';
                        $html .= '</tr>';

                        $oldAttempt = $td->attempt;
                    }
                } else {
                    $isNoData = true;
                }
            } else {
                $isNoData = true;
            }

            if ($isNoData) {
                $html .= '<tr>';
                $html .=    '<td class="text-center p-3" colspan="8">'.get_string('no_data', $this->pluginname).'</td>';
                $html .= '</tr>';
            }


            if (Parameter::isAajx()) {

                $moduleInfoTable = $this->getModuleInfoTable($cm, $intervalTime);

                Javascript::printJSON(
                    array(
                        'code'=>Javascript::getSuccessCode()
                        ,'msg' => get_string('view_complete', 'local_ubion')
                        ,'html'=>$html
                        ,'title'=>$cm->name
                        ,'trackid' => $trackid
                        ,'moduleInfoTable' => $moduleInfoTable
                    )
                );

            } else {
                return $html;
            }
        }
    }


    /**
     * 인정 취소 관련 문구 리턴
     *
     * @param string $status
     * @return string
     */
    public function getRecognitionStatusName($status)
    {
        $name = '';
        switch ($status) {
            case self::CODE_ABSENCE :
                $name = get_string('recognition_absence', $this->pluginname);
                break;
            case self::CODE_LATE :
                $name = get_string('recognition_lateness', $this->pluginname);
                break;
            default :
                $name = get_string('recognition_attendance', $this->pluginname);
                break;
        }

        return $name;
    }


    public function getPercent($userViewTime, $modulePlaytime)
    {
        $percent = floor($userViewTime / $modulePlaytime * 100);

        if ($percent > 100) {
            $percent = 100;
        }

        return $percent;
    }
}