<?php
namespace local_ubonattend\core;

use local_ubion\core\traits\TraitCourseUser;

class Progress 
{
    use TraitCourseUser;
    
    // 진도율 방식
    const PROGRESSTYPE_PERCENT = 1;
    
    // 뷰어 열람시간
    const PROGRESSTYPE_OPENTIME = 2;
    
    // 뷰어 열람 여부
    const PROGRESSTYPE_OPEN = 3;
    
    const STATE_PLAY = 3;
    const STATE_MOVE = 2;
    const STATE_INTERVAL = 8;
    const STATE_COMPLETE = 10;
    
    
    protected $pluginname = 'local_ubonattend';
    
    protected $course = null;
    protected $courseid = null;
    protected $onlineConfig = null;
    protected $allowModules = null;
    
    public function __construct($courseid)
    {
        
        $this->courseid = $courseid;
        $this->course = \local_ubion\course\Course::getInstance()->getCourse($this->courseid);
        
        $this->onlineConfig = \local_ubonattend\Attendance::getInstance($this->courseid)->getConfig();
        $this->allowModules = self::getAllowModules();
    }
        
    
    public static function getProgressTypes()
    {
        return array(
            self::PROGRESSTYPE_PERCENT => get_string('progresstype_progress', 'local_ubonattend')
            ,self::PROGRESSTYPE_OPENTIME => get_string('progresstype_opentime', 'local_ubonattend')
            ,self::PROGRESSTYPE_OPEN => get_string('progresstype_open', 'local_ubonattend')
        );
    }
    
    /**
     * 수동인정시 진도율을 100%로 표시해줘야되는 출석 상태값
     * (상황에 따라 결석인정으로 처리하는 경우가 있기 때문에 수동인정을 모두 진도율을 100%로 처리하면 안됨)
     *
     * @param int $recognition
     * @return boolean
     */
    public static function getRecognitionAttendance($recognition)
    {
        // 수동인정시 진도율을 100%로 표시해줘야되는 출석 상태값
        $attendanceCode = array(
            \local_ubonattend\Attendance::CODE_ATTENDANCE
            ,\local_ubonattend\Attendance::CODE_LATE
        );
        
        if (in_array($recognition, $attendanceCode)) {
            return true;
        }
        
        return false;
    }
    
        
    
    public static function getAllowModules()
    {
        $allow = get_config('local_ubonattend', 'allow_modules');
        
        $allowModules = array();
        
        if (!empty($allow)) {
            $allowModules = explode(',', $allow);
        }
        
        // 공백 제거
        if ($allowModules) {
            foreach ($allowModules as &$as) {
                $as = trim($as);
            }
        }
        
        return $allowModules;
    }
    
    
    public static function getIntervalTime($playtime, $isMillisecond=true)
    {
        // 5분 이내 30초 단위
        // 5분 이상 1분 단위 체크함
        $intervalSecond = ($playtime <= 300) ? 30000 : 60000;
        
        // millisecond 형태가 아니라면 /1000을 진행해줘야됨.
        if (empty($isMillisecond)) {
            $intervalSecond = $intervalSecond / 1000;
        }
        
        return $intervalSecond;
    }
    
    
    /**
     * 진도율 재계산
     * 
     */
    public function setReCalculation()
    {
        // error_log('recal');
        global $CFG, $DB;
        
        require_once $CFG->libdir.'/completionlib.php';
        
        $CCourse = \local_ubion\course\Course::getInstance();
        
        $numsections = $this->course->numsections ?? 0;
        
        // 강좌가 종료되었는지 확인 해봐야됨.
        $isStudyEnd = $CCourse->isStudyEnd($this->courseid);
        
        // 교수자 권한이 존재하고, 강좌가 종료되지 않은 경우에만 진도 재계산이 진행되어야 함.
        if ($CCourse->isProfessor($this->courseid) && !$isStudyEnd) {
            
            // 진도처리를 진행하는 모듈이 존재하는 경우
            if ($this->allowModules) {
                
                // 온라인 출석부를 사용하는 경우 강좌내 온라인 출석부 기간 정보 가져오기
                $sections = null;
                if ($this->onlineConfig->used) {
                    $sections = \local_ubonattend\Attendance::getInstance($this->courseid)->getSections();
                }
                
                // 해당 강좌의 진도 정보 모두 삭제
                // 데이터를 삭제 시킨 뒤 insert하는 방식으로 처리함. (select 후 insert, update문 검사하는것보다 delete 후 무조건 insert하는게 더 빠름)
                // 2018-07-17 대형 강의실인 경우 id값 증가 속도가 상당히 크기 때문에 select 후 insert, update 할건지 판단.. 
                // (조건문이 모두 인덱스 걸려있는 컬럼이라서 속도에는 문제가 없음)
                // $this->setCourseResetProgress();
                
                // 강좌에 등록된 모듈 정보
                $modinfo = get_fast_modinfo($this->courseid);
                
                // 구성원 정보 가져오기
                // 사용자가 없으면 진도율 재계산할 의미가 없음
                $roleid = $this->getStudentRoleID();
                if ($users = $this->getCourseUserAll($this->courseid, array(), null, null, null, $roleid)) {
                    
                    // 등록된 주차를 반복돌면서 진도율 체크를 진행.
                    foreach ($modinfo->get_section_info_all() as $section) {
                        // 0주차는 패스
                        // 주차가 숨겨져있거나 numsections보다 크면 pass
                        if ($section->visible & $section->section > 0 && $section->section <= $numsections) {

                            // 온라인 출석부 특정 주차 셋팅 정보 가져옴
                            $sectionSetting = $sections[$section->id] ?? null;
                            
                            // 해당 주차에 존재하는 모듈(진도처리를 진행하는 모듈)에 대해 진도율 재계산
                            $this->setSectionProgress($modinfo, $section, $sectionSetting, $users);
                            
                        }   // if ($section->visible & $section->section > 0 && $section->section <= $numsections) 
                    } // end foreach ($modinfo->get_section_info_all())
                } // end if ($users)
            }
        }
    }
    
    
    public function setSectionReCalculation($sectionid)
    {
        $CCourse = \local_ubion\course\Course::getInstance();
        
        // 강좌가 종료되었는지 확인 해봐야됨.
        $isStudyEnd = $CCourse->isStudyEnd($this->courseid);
        
        // 교수자 권한이 존재하고, 강좌가 종료되지 않은 경우에만 진도 재계산이 진행되어야 함.
        if ($CCourse->isProfessor($this->courseid) && !$isStudyEnd) {
            // 구성원 정보 가져오기
            // 사용자가 없으면 진도율 재계산할 의미가 없음
            $roleid = $this->getStudentRoleID();
            if ($users = $this->getCourseUserAll($this->courseid, array(), null, null, null, $roleid)) {
                foreach ($users as $u) {
                    $this->setUserSectionReCalculate($sectionid, $u->id);
                }
                
            } // end if ($users)
        }
    }
    
    
    /**
     * 특정 주차에 존재하는 모듈들 진도율 재계산
     * 
     * @param \course_modinfo $modinfo
     * @param int|\section_info $sectionornum
     * @param \stdClass $sectionSetting
     * @param array $users
     */
    public function setSectionProgress(\course_modinfo $modinfo, $sectionornum, $sectionSetting, $users)
    {
        global $DB;
        
        if (is_number($sectionornum)) {
            $section = $modinfo->get_section_info($sectionornum);
        } else {
            $section = $sectionornum;
        }
        
        $cmids = $modinfo->sections[$section->section] ?? null;
        
        // 해당 주차에 등록된 항목이 존재하는 경우
        if (!empty($cmids)) {
            // error_log('section : '.$section->section);
            // error_log(print_r($sectionSetting, true));
            
            // $userSectionStatus[userid][출석상태-출석]
            // $userSectionStatus[userid][출석상태-지각]
            // $userSectionStatus[userid][출석상태-결석]
            $userSectionStatus = array();
            
            $transaction = $DB->start_delegated_transaction();
            foreach ($cmids as $cmid) {
                $cm = $modinfo->get_cm($cmid);
                
                if (!empty($cm)) {
                    $userSectionStatus = $this->setModuleProgress($cm, $users, $sectionSetting, $userSectionStatus);
                }
            }
            
            
            // 주차 출석에 대한 로직 실행
            foreach ($users as $u) {
                $sectionStatus = $userSectionStatus[$u->id] ?? array();
                
                $status = $this->getSectionStatus($sectionStatus);
                $this->setUserSectionStatus($u->id, $section->id, $status);
            }
            $transaction->allow_commit();
            // error_log(print_r($userSectionStatus, true));
            
        }   // end if (!empty($cmids))
    }
    
    /**
     * 모듈의 출석 상태
     * 
     * @param \stdClass $progressObject
     * @return number[]|NULL[]
     */
    public function getModuleProgress(\stdClass $progressObject)
    {
        // $progressObject 에 포함되어야 할 항목들
        // progress : 출석 기간내 maxposition
        // progress_time : 출석 기간내 뷰어 열람시간
        // totalprogress : 출석+지각 기간내 maxposition
        // progress_time : 출석+지각 기간내 뷰어 열람시간
        // playtime : 동영상 재생시간
        // progress_percent : 출석 인정률
        // lateness_percent : 지각 인정률
        // open_status : 열람여부에 따른 출석/지각 상태값
        // recognition : 수동인정 여부
        
        // 진도 체크 방법에 따라서 로직이 달라짐
        // 기본 결석
        $status = \local_ubonattend\Attendance::CODE_ABSENCE;
        $progress = 0;
        
        // 수동인정이 아닌 경우 (기본상태값)
        if (empty($progressObject->recognition)) {
            
            // 진도체크 방식이 열람방식인 경우
            if ($this->onlineConfig->progresstype == self::PROGRESSTYPE_OPEN) {
                
                // 단순 열람 방식이라면..
                // $progressObject->open_status값을 리턴해주면 됨.
                $status = $progressObject->open_status;
                
                // 열람 여부로만 결정하기 때문에 출석 및 지각인 경우에는 진도율을 100%로 맞춰줌
                if ($status == \local_ubonattend\Attendance::CODE_ATTENDANCE || $status == \local_ubonattend\Attendance::CODE_LATE) {
                    $progress = 100;
                }
                
            } else {
                
                // 진도처리방식
                // 진도율, 열람시간
                $check = $progressObject->progress;
                $checkLateness = $progressObject->totalprogress;
                
                // 열람시간으로 체크하는 경우
                // econtents 같은 경우에는 무조건 열람 시간으로 체크 해야됨. 
                if ($this->onlineConfig->progresstype == self::PROGRESSTYPE_OPENTIME || $progressObject->isOpenTimeCheck) {
                    $check = $progressObject->progress_time;
                    $checkLateness = $progressObject->totalprogress_time;
                }
                
                
                // 최종 진도율 정보 구하기
                // 전달된 객체에 대한 예외처리 없음
                // 단 playtime이 0인 경우는 진도율을 못구하기 때문에 해당 항목에 대해서는 모두 결석으로 처리해야됨.
                if ($progressObject->playtime > 0) {
                    // 반올림에 의해서 출석이 인정될수 있기 때문에 조건문 검사 전에는 반올림 처리하면 안됨.
                    $percent = $latenessPercent = $check / $progressObject->playtime * 100;
                    
                    // 출석기간내에 시청한 기록이 출석인정률보다 높으면 출석
                    if ($percent >= $progressObject->progress_percent) {
                        $status = \local_ubonattend\Attendance::CODE_ATTENDANCE;
                    }
                    
                    // 지각 기능을 사용하는 경우에 추가적인 검사를 진행함
                    if ($this->onlineConfig->islate) {
                        $latenessPercent = $checkLateness / $progressObject->playtime * 100;
                        
                        // 현재 출석 상태가 결석이면 지각 인정률을 비교해서 지각으로 판단할건지 체크 해봐야됨.
                        if ($status == \local_ubonattend\Attendance::CODE_ABSENCE) {
                            if ($latenessPercent >= $progressObject->lateness_percent) {
                                $status = \local_ubonattend\Attendance::CODE_LATE;
                            }
                        }
                    }
                    
                    // 지각 기간 진도율이 출석 기간 진도율 보다 크면 지각 기간 진도율이 표시되어야 함.
                    $percent = ($latenessPercent > $percent) ? $latenessPercent : $percent;
                    
                    // 반드시 소수점 첫번째에서 반올림 처리해야됨.
                    // 안그럼 89.7% 인 경우 화면상에서 90%로 표시되기 때문에 학생들이 지각 또는 결석으로 처리된 상황에 대해서 문의 요청이 올수 있음.
                    $progress = round($percent, 1);
                    
                } else {
                    // error_log('playtime 0');
                }
                
            }
        } else {
            
            // 수동인정값이 진도율 100%로 처리되어야 하는 경우
            if (self::getRecognitionAttendance($progressObject->recognition)) {
                $status = $progressObject->recognition;
                $progress = 100;
                
                /* 해당 부분은 학교 요청이 있을 경우에만 진행
                 * 참고로 배속 기능에 의해서 동영상 재생시간보다 뷰어 열람시간이 작을수 있기 때문에 가급적이면 해당 코드는 주석처리되어 있는게 좋을거 같습니다.
                 // 지각 포함 뷰어 열람 시간이 동영상 재생시간 보다 작으면 강제로 지각 포함 뷰어 열람 시간을 재생시간으로 맞춰줌
                 if ($progressObject->totalprogress_time < $moduleInfo->playtime) {
                 $progressTime = $moduleInfo->playtime;
                 }
                 */
            }
        }
        
        // 진도율은 100%를 넘을수 없음
        if ($progress > 100) {
            $progress = 100;
        }
        
        // 출석상태, 진도율, 뷰어 열람시간
        // 지각 기능을 사용하지 않는 경우 progress_time, totalprogress_time값을 동일하게 적용하기 때문에 
        // 뷰어 열람시간은 totalprogress_time으로 전달해주면 됨
        return array($status, $progress, $progressObject->totalprogress_time);
    }
    
    
    /**
     * 모듈 한개에 대한 진도율 정보 반영
     * 
     * @param \cm_info $cm
     * @param array $users
     * @param int|object $sectionorid
     * @param array $userSectionStatus
     * @return number[]
     */
    public function setModuleProgress(\cm_info $cm, $users, $sectionorid, $userSectionStatus=array())
    {
        global $DB;
        
        // 모듈이 활성화 되어 있고, 진도처리를 진행하는 모듈인 경우
        if ($cm->visible && in_array($cm->modname, $this->allowModules)) {
            
            $customdata = $cm->customdata;
            
            $query = "SELECT * FROM {".$cm->modname."} WHERE id = :id";
            
            $addCheck = true;
            // 온라인 출석부를 사용하는 경우라면 useattend까지 검사해야됨.
            if ($this->onlineConfig->used) {
                $addCheck = $customdata->useattend;
            }
            
            // 진도처리를 진행하는 모듈인 경우
            if ($customdata->progress && $addCheck) {
                
                // interval 시간 가져오기
                $intervalSecond = self::getIntervalTime($customdata->playtime, false);
                
                
                // 사용자별 상세 기록 가져오기
                // 처음에는 trackdetail의 모든 정보를 가져와서 출석인정기간, 지각인정기간에 대해서 프로그램적으로 처리 했다가
                // track_detail을 주기적으로 삭제해주는 기능 추가해서 디비에서 모두 처리해서 가져오는 방법으로 수정 조치함.
                $attendanceTracks = $this->getUserModuleProgress($cm->modname, $cm->instance, $customdata->timeopen, $customdata->timeclose, $intervalSecond);
                $latenessTracks = array();
                if ($this->onlineConfig->islate) {
                    $latenessTracks = $this->getUserModuleProgress($cm->modname, $cm->instance, $customdata->timeopen, $customdata->timelateness, $intervalSecond);
                }
                
                
                // track의 id값 구하기
                // track이 존재하는 사용자에 대해서
                // 진도율 정보를 0으로 처리한 뒤 진도처리 기간내에 시청한 기록
                // 참고로 tracid를 구하는 이유는 진도(지각포함)처리 기간이 아닌 시점에 동영상을 시청한 경우
                // 또는 기간 변경으로 인해 출석상태가 변경되어야 하는 경우
                // $attendanceTracks에 조회되지 않기 때문에 출석 상태값을 변경하지 못하는 문제가 있을수 있음
                // 이를 위해서 track 레코드가 존재하는 사용자들에 대해서는 출석 상태값을 0으로 셋팅한뒤 진도율에 따라 출석 상태값을 변경시키도록 구현함
                $trackInfoQuery = "SELECT id, recognition, userid FROM {".$cm->modname."_track} WHERE ".$cm->modname."id = :moduleid AND userid = :userid";
                $trackInfoParam = array('moduleid' => $cm->instance, 'userid' => 0);
                
                // 사용자별로 반복 돌면서 track 정보가 존재하는 사용자들 진도율 정보 변경
                foreach ($users as $u) {
                    
                    // 각 모듈에 대한 출석정보 주차 출석정보 배열에 담아두기
                    if (!isset($userSectionStatus[$u->id])) {
                        $userSectionStatus[$u->id] = $this->getSectionAttendanceDefault();
                    }
                
                    // 출석기간 진도율
                    $attendanceTrack = $attendanceTracks[$u->id] ?? null;
                    
                    // 지각기간 포함 진도율
                    $latenessTrack = $latenessTracks[$u->id] ?? null;
                    
                    // 사용자 출석 상태 저장
                    $status = $this->setUserModuleProgress($cm, $u->id, $attendanceTrack, $latenessTrack);
                    
                    
                    $userSectionStatus[$u->id][$status]++;
                    
                }
            }
        }
        
        
        return $userSectionStatus;
    }
    
    
    /**
     * 모듈 - 사용자 출석 상태 저장
     * 
     * @param \cm_info $cm
     * @param int|\stdClass $userid
     * @param \stdClass $attendanceTrack
     * @param \stdClass $latenessTrack
     * @return number[]|NULL[]
     */
    public function setUserModuleProgress(\cm_info $cm, $useridOrTrackInfo, $attendanceTrack, $latenessTrack)
    {
        global $DB, $CFG;
        require_once $CFG->libdir . '/completionlib.php';
        
        if (is_number($useridOrTrackInfo)) {
            $query = "SELECT * FROM {".$cm->modname."_track} WHERE ".$cm->modname."id = :moduleid AND userid = :userid";
            $param = array('moduleid' => $cm->instance, 'userid' => $useridOrTrackInfo);
            
            $trackInfo = $DB->get_record_sql($query, $param);
        } else {
            $trackInfo = $useridOrTrackInfo;
        }
        
        
        
        // 기본값은 결석
        $status = \local_ubonattend\Attendance::CODE_ABSENCE;
        
        // track 정보가 존재하면 진도율 정보 계산해봐야됨.
        if (!empty($trackInfo)) {
            
            $customdata = $cm->customdata;
            
            // 기본값 선언
            $progress = $progressTime = 0;
            $totalProgress = $totalProgressTime = 0;
            $openStatus = \local_ubonattend\Attendance::CODE_ABSENCE;
            
            // 출석 정보가 존재하는 경우
            // 기본적으로 출석과 지각은 동일한 데이터가 반영
            if (!empty($attendanceTrack)) {
                $progress = $totalProgress = $attendanceTrack->progress;
                $progressTime = $totalProgressTime = $attendanceTrack->viewertime;
                $openStatus = \local_ubonattend\Attendance::CODE_ATTENDANCE;
                // error_log('userid : '.$u->id.', status => '.$openStatus);
            }
            
            // 지각 기능을 사용하는 경우 지각인정 기간을 포함한 진도기간을 산출해줘야됨.
            if ($this->onlineConfig->islate && !empty($latenessTrack)) {
                $totalProgress = $latenessTrack->progress;
                $totalProgressTime = $latenessTrack->viewertime;
                
                // 열람 여부값이 결석 상태라면
                // 출석 인정기간에 뷰어 오픈은 안했기 때문에 지각으로 설정되어야 함.
                if ($openStatus == \local_ubonattend\Attendance::CODE_ABSENCE) {
                    $openStatus = \local_ubonattend\Attendance::CODE_LATE;
                }
            }
            
            
            $progressObject = new \stdClass();
            $progressObject->id = $trackInfo->id;
            $progressObject->progress = $progress;
            $progressObject->progress_time = $progressTime;
            $progressObject->totalprogress = $totalProgress;
            $progressObject->totalprogress_time = $totalProgressTime;
            $DB->update_record($cm->modname.'_track', $progressObject);
            
            
            // 사용자의 진도율 구하기
            $progressObject->playtime = $customdata->playtime;
            $progressObject->progress_percent = $customdata->completionprogress;
            $progressObject->lateness_percent = $customdata->tardinessprogress;
            $progressObject->open_status = $openStatus;
            $progressObject->recognition = $trackInfo->recognition;
            $progressObject->isOpenTimeCheck = $customdata->isOpenTimeCheck ?? false;
            
            list ($status, $progress, $progressTime) = $this->getModuleProgress($progressObject);
            $this->setUserModuleStatus($trackInfo->userid, $cm->id, $status, $progress, $progressTime);
            
            
            // completion 관련 로직 실행
            $completion = new \completion_info($this->course);
            
            // 자동으로 학습활동을 반영하는 경우에만 실행이되어야 함
            if($completion->is_enabled($cm) == COMPLETION_TRACKING_AUTOMATIC) {
                $completion->update_state($cm, COMPLETION_COMPLETE, $trackInfo->userid);
            } else {
                // 성적 항목을 추가하고, 자동성적 반영으로 선택한 경우 성적 반영 로직을 실행시켜줘야됨.
                if ($customdata->autograde && $customdata->add_grade_item) {
                    require_once $CFG->dirroot.'/mod/'.$cm->modname.'/lib.php';
                    
                    // 이수 체크를 진행하지 않아도 성적은 반영될수 있어야됨.
                    // 2016-11-24 by akddd
                    $functionname = $cm->modname.'_get_completion_state';
                    if(function_exists($functionname)) {
                        $functionname($this->course, $cm, $trackInfo->userid, COMPLETION_COMPLETE, false);
                    }
                }
            }
        }
        return $status;
    }
    
    
    /**
     * 강좌-모듈-사용자별 진도 정보 기록
     *
     * @param int $userid
     * @param int $cmid
     * @param int $status
     * @param int $progress
     * @param int $progressTime
     */
    public function setUserModuleStatus($userid, $cmid, $status, $progress, $progressTime)
    {
        global $DB;
        
        
        $module = new \stdClass();
        $module->status = $status;
        
        // !! 주의 !! 
        // 반올림으로 인해 출석 처리가 될수 있기 때문에 소수점 버림으로 저장해야됩니다.
        $module->progress = floor($progress);   
        $module->progress_time = $progressTime;
        
        
        $query = "SELECT id FROM {local_ubonattend_module} WHERE courseid = :courseid AND cmid = :cmid AND userid = :userid";
        $param = array('courseid' => $this->courseid, 'cmid' => $cmid, 'userid' => $userid);            
        if ($id = $DB->get_field_sql($query, $param)) {
            // 등록된 경우
            $module->id = $id;
            $DB->update_record('local_ubonattend_module', $module);
            
        } else {
            $module->courseid = $this->courseid;
            $module->cmid = $cmid;
            $module->userid = $userid;
            
            $DB->insert_record('local_ubonattend_module', $module);
        }    
    }
    
    
    /**
     * 주차 출석 상태값 리턴
     * 
     * @param array $sectionStatus
     * @return number
     */
    public function getSectionStatus(array $sectionStatus = array())
    {
        $status = null;
        
        // 상황에 따라서 지각이 x번 이상이면 결석 처리
        // 또는 출석이 x개 이상이면 무조건 출석 등의 요구조건이 있을때 해당 함수를 override해서 처리 하시면 됩니다.
        // 기본 로직은 결석 항목이 한개라도 존재하면 결석
        // 결석 항목이 없고, 지각 항목이 한개라도 존재하면 지각
        // 결석, 지각이 없는 경우에만 출석으로 인정
        if (!empty($sectionStatus)) {
            $lateCount = $sectionStatus[\local_ubonattend\Attendance::CODE_LATE] ?? 0;
            $absenceCount = $sectionStatus[\local_ubonattend\Attendance::CODE_ABSENCE] ?? 0;
            $attendanceCount = $sectionStatus[\local_ubonattend\Attendance::CODE_ATTENDANCE] ?? 0;
            
            // 결석 일수가 없는 경우
            if (empty($absenceCount)) {
                // 지각이 한개라도 존재하면 지각
                if ($lateCount > 0) {
                    $status = \local_ubonattend\Attendance::CODE_LATE;
                } else if ($attendanceCount > 0) {
                    $status = \local_ubonattend\Attendance::CODE_ATTENDANCE;
                }
            } else {
                $status = \local_ubonattend\Attendance::CODE_ABSENCE;
            }
        }
        
        return $status;
    }
    
    
    /**
     * 주차 출석 계산을 위한 기본 배열
     * 
     * @return array[]
     */
    public function getSectionAttendanceDefault()
    {
        return array(
            \local_ubonattend\Attendance::CODE_ATTENDANCE => 0
            ,\local_ubonattend\Attendance::CODE_LATE => 0
            ,\local_ubonattend\Attendance::CODE_ABSENCE => 0
        );
    }
    
    
    
    /**
     * 사용자의 주차 출석 상태값 저장
     *
     * @param int $userid
     * @param int $sectionid
     * @param int $status
     */
    public function setUserSectionStatus($userid, $sectionid, $status)
    {
        global $DB;
        
        // 주차내에 모듈이 없어서 status가 빈값으로 전달될수 있기 때문에 빈값(0포함)이 전달되면 주차 출석 정보를 기록하지 않음
        if (!empty($status)) {
        
            $module = new \stdClass();
            $module->status = $status;
            
            
            $query = "SELECT id FROM {local_ubonattend_section} WHERE courseid = :courseid AND sectionid = :sectionid AND userid = :userid";
            $param = array('courseid' => $this->courseid, 'sectionid' => $sectionid, 'userid' => $userid);
            
            if ($id = $DB->get_field_sql($query, $param)) {
                // 등록된 경우
                $module->id = $id;
                $DB->update_record('local_ubonattend_section', $module);
            } else {
                $module->courseid = $this->courseid;
                $module->sectionid = $sectionid;
                $module->userid = $userid;
                
                $DB->insert_record('local_ubonattend_section', $module);
            }
        }
    }
    
    
    /**
     * 주차 출석 정보 반영
     * 
     * @param int $sectionid
     * @param int $userid
     * @return boolean
     */
    public function setUserSectionReCalculate($sectionid, $userid=null)
    {
        global $DB, $USER;
        
        if (empty($userid)) {
            $userid = $USER->id;
        }
        
        if (!empty($this->allowModules)) {
        
            // 수동인정에 의해서 주차 출석이 재계산되기 때문에
            // $this->setSectionProgress() 함수 호출까지 가지 않고 직접 local_ubonattend_module에서 출석정보를 가져와서 반영
            
            $modinfo = get_fast_modinfo($this->courseid);
            
            // 주차내에 진도처리를 진행하는 모듈을 알아와야됨.
            // 강좌내에 존재하지 않는 sectionid값이 전달되었을 경우에는 재계산이 되면 안되기 때문에 get_section_info_all에서 반복 돌면서
            // 실제 존재하는 sectionid값인지 검사해봐야됨.
            $sections = $modinfo->get_section_info_all();
            foreach ($sections as $section) {
                if ($section->id == $sectionid) {
                    
                    // 주차내에 존재하는 cmid
                    $cmids = $modinfo->sections[$section->section] ?? null;
                    
                    if (!empty($cmids)) {
                        
                        $query = "SELECT status FROM {local_ubonattend_module} WHERE courseid = :courseid AND cmid = :cmid AND userid = :userid";
                        $param = array('courseid' => $this->courseid, 'cmid' => null, 'userid' => $userid);
                        
                        $sectionStatus = $this->getSectionAttendanceDefault();
                        
                        foreach ($cmids as $cmid) {
                            $cm = $modinfo->get_cm($cmid);
                            
                            // 모듈이 활성화 되어 있고, 진도처리를 진행하는 모듈인 경우
                            if ($cm->visible && in_array($cm->modname, $this->allowModules)) {
                                
                                if ($moduleInfo = $DB->get_record_sql("SELECT * FROM {".$cm->modname."} WHERE id = :id", array('id' => $cm->instance))) {
                                    
                                    // 온라인 출석부를 사용하는 경우라면 useattend까지 검사해야됨.
                                    $addCheck = true;
                                    if ($this->onlineConfig->used) {
                                        $addCheck = $moduleInfo->useattend;
                                    }
                                    
                                    // 진도처리를 진행하는 모듈인 경우
                                    if ($moduleInfo->progress && $addCheck) {
                                        $param['cmid'] = $cm->id;
                                        
                                        // 해당 모듈의 출석 상태값 가져오기
                                        $status = $DB->get_field_sql($query, $param);
                                        
                                        // 데이터가 없다면 동영상 시청을 한번도 안한 경우임
                                        if (empty($status)) {
                                            $sectionStatus[\local_ubonattend\Attendance::CODE_ABSENCE]++;
                                        } else {
                                            $sectionStatus[$status]++;
                                        }
                                    }
                                    
                                }
                            }
                        }
                        
                        // error_log(print_r($sectionStatus, true));
                        // 진도처리를 진행하는 모듈의 출석상태값을 기준으로 주차 출석 정보를 반영
                        $status = $this->getSectionStatus($sectionStatus);
                        // error_log('status : '.$status);
                        $this->setUserSectionStatus($userid, $sectionid, $status);
                    }
                    
                    break;
                }
            }
        }
    }
    
    /**
     * 특정 기간내에 시청한 사용자별 진도율 및 뷰어 열람 시간 
     * 
     * @param string $module
     * @param int $moduleid
     * @param int $timeopen
     * @param int $timeclose
     * @param int $intervalSecond
     * @return array
     */
    public function getUserModuleProgress($module, $moduleid, $timeopen, $timeclose, $intervalSecond, $userid = null)
    {
        global $DB;
        
        $replace = '##USERQUERY##';
        $periodReplace = "##PERIODQUERY##";
        
        // 주차별로 동영상의 진도처리기간이 다르기 때문에 강좌 단위로 조회 불가능하므로,
        // 모듈 단위로 조회해야됩니다.
        $query = "SELECT
                             userid
                            ,SUM(viewertime) AS viewertime
                            ,MAX(progress) AS progress
                  FROM      (
                              SELECT
                                        userid
                                        ,attempt
                                        ,(MAX(timetrack) - MIN(timetrack)) AS viewertime
                                        ,MAX(positionnew) AS progress
                              FROM      {".$module."_track} mt
                              JOIN      {".$module."_track_detail} mtd ON mt.id = mtd.track
                              WHERE     mt.".$module."id = :moduleid
                              ".$periodReplace."
                              ".$replace."
                              GROUP BY  userid, attempt
                  ) a
                  GROUP BY userid";
        
        $param = [
            'moduleid' => $moduleid
        ];
        
        // 기간이 설정된 경우
        $addPeriodQuery = '';
        if (!empty($timeopen) || !empty($timeclose)) {
            if (!empty($timeopen)) {
                $addPeriodQuery = ' AND timetrack >= :timeopen';
                $param['timeopen'] = $timeopen;
            }
            
            if (!empty($timeclose)) {
                $addPeriodQuery = ' AND timetrack <= :timeclose';
                
                
                // 진도기록을 intervalSecond(1분이라고 가정)마다 기록하기 때문에
                // 10분짜리 동영상에 대해 진도처리기간이 xx년 xx월 xx일 23시 59분 59초까지인데,
                // 사용자가 최초 뷰어 오픈을 23시 50분 10초에 뷰어 오픈을 한 경우
                // 23시 59분 10초 ~ 24시 00분 10초 기록에 대해서 DB 쿼리문에 조회되지 않기 때문에 최대 59초정도 학생에게 불리함
                // 이를 방지하기 위해서 진도처리 종료기간을 timeclose + intervalsecond 해서 학생들한테 유리하도록 변경
                // 참고로 학생에게 유리해야지만 문의가 덜 들어옴..
                
                $timeclose = $timeclose + $intervalSecond;
                $param['timeclose'] = $timeclose;
            }
        }
        
        
        $addQuery = '';
        if (!empty($userid)) {
            $addQuery = ' AND   userid = :userid';
            $param['userid'] = $userid;
        }
        
        $query = str_replace($replace, $addQuery, $query);
        $query = str_replace($periodReplace, $addPeriodQuery, $query);
        
        
        return $DB->get_records_sql($query, $param);
    }
    
    
    /**
     * 사용자가 특정모듈을 시청한 상세기록
     * 
     * @param string $module
     * @param int $moduleid
     * @param int $userid
     * @return array
     */
    public function getUserModuleProgressDetail($module, $moduleid, $userid)
    {
        global $DB;
        
        
        // getUserModuleProgress 함수와 쿼리가 비슷하오니, 혹시라도 둘중 한개라도 쿼리가 수정이 되면 
        // getUserModuleProgress, getUserModuleProgressDetail 2개 함수 모두 데이터 검증을 해보셔야 합니다.
        $query = "SELECT
                            mtd.attempt
                            ,MIN(timetrack) AS timestart
                            ,MAX(timetrack) AS timeend
                            ,MAX(positionnew) AS progress
                            ,MAX(mtde.ip) AS ip
                            ,MAX(mtde.device) AS device
                  FROM      {".$module."_track} mt
                  JOIN      {".$module."_track_detail} mtd ON mt.id = mtd.track
                  LEFT JOIN {".$module."_track_device} mtde ON mtde.track = mtd.track AND mtde.attempt = mtd.attempt
                  WHERE     mt.".$module."id = :moduleid
                  AND       userid = :userid 
                  GROUP BY  mtd.attempt";
        
        
        $param = array('moduleid' => $moduleid, 'userid' => $userid);
        
        return $DB->get_records_sql($query, $param);
    }
    
    
    /**
     * 사용자가 특정모듈을 시청한 상세기록
     * @deprecated 미사용되고 있으며 삭제 예정임
     * 
     * @param string $module
     * @param int $trackid
     * @param int $attempt
     * @return array
     */
    public function getUserTrackAttemptDetail($module, $trackid, $attempt, $timestart, $timeend)
    {
        global $DB;
        
        $query = "SELECT
                            MIN(timetrack) AS timestart
                            ,MAX(timetrack) AS timeend
                            ,MAX(positionnew) AS progress
                  FROM      {".$module."_track_detail}
                  WHERE     track = :track
                  AND       attempt = :attempt
                  AND       timetrack >= :timestart
                  AND       timetrack <= :timeend
                  ORDER BY  attempt";
        
        
        $param = array('track' => $trackid, 'attempt' => $attempt, 'timestart' => $timestart, 'timeend' => $timeend);
        
        return $DB->get_record_sql($query, $param);
    }
    
    
    /**
     * 상세 기록 리턴
     * @deprecated 미사용되고 있으며 삭제 예정임
     * 
     * @param string $module
     * @param int $trackid
     * @param int $attempt
     * @return array
     */
    public function getUserTrackDetail($module, $trackid, $attempt = null)
    {
        global $DB;
        
        $query = "SELECT
                            *
                  FROM      {".$module."_track_detail}
                  WHERE     track = :track";
        
        $param = array('track' => $trackid);
        
        $order = " timetrack";
        
        if (!empty($attempt)) {
            $query .= " AND attempt = :attempt";
            $param['attempt'] = $attempt;
            
            $order = " attempt, timetrack";
        }
        
        $query .= " ORDER BY ".$order;
        
        
        return $DB->get_records_sql($query, $param);
    }
    
    /**
     * 특정 회차에 대한 열람시간/position위치값 리턴
     * 
     * @param string $module
     * @param int $trackid
     * @param int $attempt
     * @param int $timeopen
     * @param int $timeclose
     * 
     * @return \stdClass
     */
    public function getUserSomeTrackDetail($module, $trackid, $attempt, $timeopen=null, $timeclose=null)
    {
        global $DB;
        
        $query = "SELECT
                            MIN(timetrack) AS timestart
                            ,MAX(timetrack) AS timeend
                            ,MAX(positionnew) AS progress
                            ,COUNT(1) AS cnt
                  FROM      {".$module."_track_detail}
                  WHERE     track = :track
                  AND       attempt = :attempt";
        
        $param = array('track' => $trackid, 'attempt' => $attempt);
        
        if (!empty($timeopen) && !empty($timeclose)) {
            $query .= " AND timetrack >= :start AND timetrack <= :end";
            $param['start'] = $timeopen;
            $param['end'] = $timeclose;
        } else if (!empty($timeopen)) {
            $query .= " AND timetrack < :start";
            $param['start'] = $timeopen;
        } else if (!empty($timeclose)) {
            $query .= " AND timetrack > :end";
            $param['end'] = $timeclose;
        }
        
        return $DB->get_record_sql($query, $param);
    }
    
    
    /**
     * 동영상을 시청한 최대 위치
     * 
     * @param string $module
     * @param int $moduleid
     * @param int $timeopen
     * @param int $timeclose
     * @param int $userid
     * 
     * @return int
     */
    public function getMaxPosition($module, $moduleid, $timeopen, $timeclose, $userid = null)
    {
        global $DB, $USER;
        
        if (empty($userid)) {
            $userid = $USER->id;
        }
        
        $query = "SELECT
                            MAX(positionnew) AS progress
                  FROM      {".$module."_track} mt
                  JOIN      {".$module."_track_detail} mtd ON mt.id = mtd.track
                  WHERE     mt.".$module."id = :moduleid
                  AND       userid = :userid
                  AND       timetrack >= :timeopen
                  AND       timetrack <= :timeclose";
        
        $param = array('moduleid' => $moduleid, 'userid' => $userid, 'timeopen' => $timeopen, 'timeclose' => $timeclose);
        
        return $DB->get_field_sql($query, $param);
        
    }
    
    
    /**
     * 수동인정
     * 
     * @param int $userid
     * @param int $cmid
     * @param int $recognition
     */
    public function setUserRecognition($userid, $cmid, $recognition)
    {
        global $DB;
        
        $cm = get_fast_modinfo($this->courseid)->get_cm($cmid);
        
        $trackTable = $cm->modname.'_track';
        $columnid = $cm->modname.'id';
        
        // track에 수동인정 값 반영 후 진도율 재계산
        // 출석인정 => 인정 취소로 인해 주차 출석이 변경이 되어야 되기 때문에
        // 해당 모듈 및 주차에 대한 진도율 재계산이 무조건 이루어지도록 처리함.
        // $recognition 값은 출석 상태값과 동일한 값을 사용합니다.
        $query = 'SELECT * FROM {'.$trackTable.'} WHERE userid = :userid AND '.$columnid.' = :moduleid';
        $param = array('userid' => $userid, 'moduleid' => $cm->instance);
        
        $isUpdate = true;
        
        if ($trackInfo = $DB->get_record_sql($query, $param)) {
            // 빈값이면 인정 취소임
            if (empty($recognition)) {
                // attempts가 0이면 수동인정으로 생성된 track이기 때문에 삭제 
                if (empty($trackInfo->attempts)) {
                    $DB->delete_records($trackTable, array('id' => $trackInfo->id));
                    
                    // 모듈별 출석상태를 담고 있는 레코드도 같이 삭제되어야 함.
                    $DB->delete_records('local_ubonattend_module', array('courseid' => $this->courseid, 'cmid' => $cmid, 'userid' => $userid));
                    $isUpdate = false;
                }
            }
            
            // 삭제된 경우가 아니라면 update 진행해줘야됨.
            if ($isUpdate) {
                $trackInfo->recognition = $recognition;
                $DB->update_record($trackTable, $trackInfo);
            }
        } else {
            // 등록된 track이 없다면 추가
            // 단, 인정 취소가 아닐 경우에만 추가해줘야됨.
            if (!empty($recognition)) {
                $time = time();
                
                $trackInfo = new \stdClass();
                $trackInfo->userid = $userid;
                $trackInfo->$columnid = $cm->instance;
                $trackInfo->recognition = $recognition;
                $trackInfo->timefirst = $time;
                $trackInfo->timelast = $time;
                
                $trackInfo->id = $DB->insert_record($trackTable, $trackInfo);
            }
        }
        
        // track 항목이 삭제되지 않은 경우에 대해서는 모듈 진도율 계산이 진행되어야 함.
        // track이 삭제되어 있으면 setUserViewerProgress 함수 호출시 에러가 발생됩니다.
        if ($isUpdate) {
            // trackInfo가 빈값이면 개별 동영상의 진도체크는 진행할 필요 없음
            // 수동인정은 진도처리기간과 상관없이 진도율이 변경되어야 하므로 $isPeriodCheck는 false로 전달되어야 함.
            list ($isProgressPeriod, $status) = $this->setUserViewerProgress($cm, $trackInfo, false);
        }
        
        // 모듈이 수동인정 또는 수동인정 취소됨에 따른 주차 출석 정보도 변경되어야 함.
        $this->setUserSectionReCalculate($cm->section, $userid);
    }
    
    
    /**
     * 사용자 진도율 정보 반영 (뷰어 및 수동인정 페이지에서 사용됩니다.)
     * 
     * @param \cm_info $cm
     * @param \stdClass $trackInfo
     * @param int $userid
     * @param boolean $isPeriodCheck
     * @return array($isProgressPeriod, $status)
     */
    public function setUserViewerProgress(\cm_info $cm, $trackInfo, $isPeriodCheck = true)
    {
        global $USER;
        
        $customdata = $cm->customdata;
        $userid = $trackInfo->userid;
        
        // 진도처리를 위해 필요한 정보 가져오기
        $intervalSecond = self::getIntervalTime($customdata->playtime, false);
        
        $status = \local_ubonattend\Attendance::CODE_ABSENCE;
        $isProgressPeriod = true;
        
        // 진도처리 기간에만 진도율 변경을 하고 싶은 경우
        if ($isPeriodCheck) {
            // 진도처리 기간인 경우 추가 로직 실행
            // 단 timeclose값은 1분마다 한번씩 저장되기 때문에 실제 동영상 종료일 + 1분을 진행해줘야됨.
            $timeopen = $customdata->timeopen;
            $timeclose = $customdata->timelateness;
            
            // 종료시간을 따로 지정하지 않는 경우도 있기 때문에 종료시간을 지정한 경우에만 추가시간을 더해줘야됨.
            if ($timeclose > 0) {
                $timeclose += $intervalSecond;
            }
            
            // 진도율값 변경 여부
            $isProgressPeriod = $this->isProgressPeriodCheck($timeopen, $timeclose);
        }
        
        
        // 진도율 정보를 반영해야되는 경우
        if ($isProgressPeriod) {
            
            // 1. 진도처리기간내에 maxposition을 가져와서 진도율을 구한다.
            // 2. 진도처리기간내에 뷰어 열람한 시간을 가져온다
            // 3. 1, 2에서 가져온 정보를 토대로 출석 상태를 구한 뒤 디비에 반영
            // 4. 출석상태 변경에 따른 주차 출석 여부 반영
            $attendanceTracks = $this->getUserModuleProgress($cm->modname, $cm->instance, $customdata->timeopen, $customdata->timeclose, $intervalSecond, $userid);
            
            $latenessTracks = null;
            if (\local_ubonattend\Attendance::getInstance($this->courseid)->isLateCourse()) {
                $latenessTracks = $this->getUserModuleProgress($cm->modname, $cm->instance, $customdata->timeopen, $customdata->timelateness, $intervalSecond, $userid);
            }
            
            $attendanceTrack = $attendanceTracks[$userid] ?? null;
            $latenessTrack = $latenessTracks[$userid] ?? null;
            
            // 동영상 출석상태값 반영
            $status = $this->setUserModuleProgress($cm, $trackInfo, $attendanceTrack, $latenessTrack);
        }
        
        // $isProgressPeriod : 진도처리기간
        // $status : 출석상태
        return array($isProgressPeriod, $status);
    }
    
    
    /**
     * 진도처리 기간인지 확인
     * 
     * @param int $timeopen
     * @param int $timeclose
     * @return boolean
     */
    public function isProgressPeriodCheck($timeopen, $timeclose)
    {
        $time = time();
        $is = true;
        
        // 일자가 지정되었으면 현재시간 하고 비교해봐야됨.
        if ((!empty($timeopen) && $timeopen > $time) || (!empty($timeclose) && $timeclose < $time)) {
            $is = false;
        }
        
        return $is;
    }
    
    /**
     * 강좌내의 출석 상태값 리턴
     * 
     */
    public function setCourseResetProgress()
    {
        global $DB;
        
        $DB->delete_records('local_ubonattend_section', array('courseid' => $this->courseid));
        $DB->delete_records('local_ubonattend_module', array('courseid' => $this->courseid));
    }
    
}