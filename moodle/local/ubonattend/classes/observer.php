<?php
namespace local_ubonattend;

/**
 * Event observer
 */
class observer
{

    /**
     * Triggered via course_module_created event.
     *
     * @param \core\event\course_module_created $event
     */
    public static function course_module_created(\core\event\course_module_created $event)
    {

        /**
         * $event sample data
         * core\\event\\course_module_created Object
         * (
         * [data:protected] => Array
         * (
         * [eventname] => \\core\\event\\course_module_created
         * [component] => core
         * [action] => created
         * [target] => course_module
         * [objecttable] => course_modules
         * [objectid] => 73
         * [crud] => c
         * [level] => 1
         * [contextid] => 6980
         * [contextlevel] => 70
         * [contextinstanceid] => 73
         * [userid] => 2
         * [courseid] => 2
         * [relateduserid] =>
         * [other] => Array
         * (
         * [modulename] => forum
         * [name] => bbbbbbbbbbbbb
         * [instanceid] => 18
         * )
         *
         * [timecreated] => 1386222977
         * )
         *
         * [logextra:protected] =>
         * [context:protected] => context_module Object
         * (
         * [_id:protected] => 6980
         * [_contextlevel:protected] => 70
         * [_instanceid:protected] => 73
         * [_path:protected] => /1/190/191/194/15/6980
         * [_depth:protected] => 6
         * )
         *
         * [triggered:core\\event\\base:private] => 1
         * [dispatched:core\\event\\base:private] =>
         * [restored:core\\event\\base:private] =>
         * [recordsnapshots:core\\event\\base:private] => Array
         * (
         * )
         *
         * )
         */

        // 모듈 추가로 인해 주차 진도 재계산
        $allowModules = \local_ubonattend\Progress::getAllowModules();
        if (in_array($event->other['modulename'], $allowModules)) {
            $modinfo = get_fast_modinfo($event->courseid);
            $cm = $modinfo->get_cm($event->objectid);

            $CProgress = new \local_ubonattend\Progress($event->courseid);
            $CProgress->setSectionReCalculation($cm->section);
        }
    }

    /**
     * Triggered via user_enrolment_deleted event.
     *
     * @param \core\event\course_module_deleted $event
     */
    public static function course_module_deleted(\core\event\course_module_deleted $event)
    {

        /**
         * $event sample data
         * core\\event\\course_module_deleted Object
         * (
         * [data:protected] => Array
         * (
         * [eventname] => \\core\\event\\course_module_deleted
         * [component] => core
         * [action] => deleted
         * [target] => course_module
         * [objecttable] => course_modules
         * [objectid] => 69
         * [crud] => d
         * [level] => 1
         * [contextid] => 6976
         * [contextlevel] => 70
         * [contextinstanceid] => 69
         * [userid] => 2
         * [courseid] => 2
         * [relateduserid] =>
         * [other] => Array
         * (
         * [modulename] => forum
         * [instanceid] => 14
         * )
         *
         * [timecreated] => 1386208049
         * )
         *
         * [logextra:protected] =>
         * [context:protected] => context_module Object
         * (
         * [_id:protected] => 6976
         * [_contextlevel:protected] => 70
         * [_instanceid:protected] => 69
         * [_path:protected] => /1/190/191/194/15/6976
         * [_depth:protected] => 6
         * )
         *
         * [triggered:core\\event\\base:private] => 1
         * [dispatched:core\\event\\base:private] =>
         * [restored:core\\event\\base:private] =>
         * [recordsnapshots:core\\event\\base:private] => Array
         * (
         * [course_modules] => Array
         * (
         * [69] => stdClass Object
         * (
         * [id] => 69
         * [course] => 2
         * [module] => 9
         * [instance] => 14
         * [section] => 1 // course_sections 테이블 고유 아이디 임(강좌 주차가 아닙니다.)
         * [idnumber] =>
         * [added] => 1386208038
         * [score] => 0
         * [indent] => 0
         * [visible] => 1
         * [visibleold] => 1
         * [groupmode] => 0
         * [groupingid] => 0
         * [groupmembersonly] => 0
         * [completion] => 1
         * [completiongradeitemnumber] =>
         * [completionview] => 0
         * [completionexpected] => 0
         * [availablefrom] => 0
         * [availableuntil] => 0
         * [showavailability] => 1
         * [showdescription] => 0
         * )
         *
         * )
         *
         * )
         *
         * )
         */
        $allowModules = \local_ubonattend\Progress::getAllowModules();
        if (in_array($event->other['modulename'], $allowModules)) {
            // 모듈 삭제로 인한 주차 출석 정보는 재계산되어야 함.
            $courseModules = $event->get_record_snapshot('course_modules', $event->objectid);

            $CProgress = new \local_ubonattend\Progress($event->courseid);
            $CProgress->setSectionReCalculation($courseModules->section);
        }
    }

    /**
     * Triggered via local_ubion_course_module_visible event.
     *
     * @param \local_ubion\event\course_module_visible $event
     */
    public static function course_module_visible(\local_ubion\event\course_module_visible $event)
    {

        /**
         * core\\event\\local_ubion_course_module_visible Object
         * (
         * [data:protected] => Array
         * (
         * [eventname] => \\core\\event\\local_ubion_course_module_visible
         * [component] => core
         * [action] => visible
         * [target] => local_ubion_course_module
         * [objecttable] => course_modules
         * [objectid] => 79
         * [crud] => d
         * [level] => 1
         * [contextid] => 6986
         * [contextlevel] => 70
         * [contextinstanceid] => 79
         * [userid] => 2
         * [courseid] => 2
         * [relateduserid] =>
         * [other] => Array
         * (
         * [modulename] => forum
         * [instanceid] => 24
         * )
         *
         * [timecreated] => 1386229458
         * )
         *
         * [logextra:protected] =>
         * [context:protected] => context_module Object
         * (
         * [_id:protected] => 6986
         * [_contextlevel:protected] => 70
         * [_instanceid:protected] => 79
         * [_path:protected] => /1/190/191/194/15/6986
         * [_depth:protected] => 6
         * )
         *
         * [triggered:core\\event\\base:private] => 1
         * [dispatched:core\\event\\base:private] =>
         * [restored:core\\event\\base:private] =>
         * [recordsnapshots:core\\event\\base:private] => Array
         * (
         * )
         *
         * )
         */
        $allowModules = \local_ubonattend\Progress::getAllowModules();
        if (in_array($event->other['modulename'], $allowModules)) {
            // 모듈 숨김으로 인해 주차 출석 정보는 재계산되어야 함.
            $modinfo = get_fast_modinfo($event->courseid);
            if ($cm = $modinfo->get_cm($event->objectid)) {
                $CProgress = new \local_ubonattend\Progress($event->courseid);
                $CProgress->setSectionReCalculation($cm->section);
            }
        }
    }

    /**
     * Triggered via local_ubion_course_module_move event.
     *
     * @param \local_ubion\event\course_module_move $event
     */
    public static function course_module_move(\local_ubion\event\course_module_move $event)
    {

        /**
         * core\\event\\local_ubion_course_module_move Object
         * (
         * [data:protected] => Array
         * (
         * [eventname] => \\core\\event\\local_ubion_course_module_move
         * [component] => core
         * [action] => move
         * [target] => local_ubion_course_module
         * [objecttable] => course_modules
         * [objectid] => 58
         * [crud] => u
         * [level] => 1
         * [contextid] => 189
         * [contextlevel] => 70
         * [contextinstanceid] => 58
         * [userid] => 2
         * [courseid] => 2
         * [relateduserid] =>
         * [other] => Array
         * (
         * [modulename] => resource
         * [instanceid] => 4
         * [oldsectionid] => 841
         * [newsectionid] => 838
         * )
         *
         * [timecreated] => 1386230691
         * )
         *
         * [logextra:protected] =>
         * [context:protected] => context_module Object
         * (
         * [_id:protected] => 189
         * [_contextlevel:protected] => 70
         * [_instanceid:protected] => 58
         * [_path:protected] => /1/190/191/194/15/189
         * [_depth:protected] => 6
         * )
         *
         * [triggered:core\\event\\base:private] => 1
         * [dispatched:core\\event\\base:private] =>
         * [restored:core\\event\\base:private] =>
         * [recordsnapshots:core\\event\\base:private] => Array
         * (
         * )
         *
         * )
         */
        $allowModules = \local_ubonattend\Progress::getAllowModules();
        if (in_array($event->other['modulename'], $allowModules)) {
            $newsectionid = $event->other['newsectionid'];
            $oldsectionid = $event->other['oldsectionid'];

            // 모듈이 다른 주차로 이동된 경우
            if ($newsectionid != $oldsectionid) {
                $CProgress = new \local_ubonattend\Progress($event->courseid);
                $CProgress->setSectionReCalculation($oldsectionid);
                $CProgress->setSectionReCalculation($newsectionid);
            }
        }
    }
}