<?php
use \local_ubion\base\Common;
use \local_ubion\base\Parameter;

require '_head.php';

$i8n->recognition_cancel = get_string('recognition_cancel', $pluginname);
$i8n->recognition_absence = get_string('recognition_absence', $pluginname);
$i8n->recognition_attendance = get_string('recognition_attendance', $pluginname);
$i8n->recognition_lateness = get_string('recognition_lateness', $pluginname);
$i8n->recognition_study = get_string('recognition_study', $pluginname);
$i8n->recognition_not_study = get_string('recognition_not_study', $pluginname);

$COnAttendance = \local_ubonattend\Attendance::getInstance($courseInfo->id);
$CUser = \local_ubion\user\User::getInstance();
$CGroup = \local_ubion\group\Group::getInstance();

$userid     = optional_param('userid', $USER->id, PARAM_INT);
$viewtype   = optional_param('viewtype', null, PARAM_INT);

$urlParam = array('id' => $courseInfo->id, 'userid' => $userid);

// 교수자 권한이 아니라면 파라메터로 전달받은 userid값은 본인 아이디로 강제로 변환
if (!$isProfessor) {
    $userid = $USER->id;
    unset($urlParam['userid']);
    
    $user = $USER;
} else {
    $user = $DB->get_record_sql('SELECT * FROM {user} WHERE id = :id', array('id' => $userid));
}
$userfullname = fullname($user);


// 온라인 출석부를 사용하면 기본 화면은 온라인 출석부가 표시되어야 함.
if ($isOnlineAttendance) {
    if (empty($viewtype)) {
        $viewtype = $COnAttendance::VIEWTYPE_ATTENDANCE;
    }
}


if ($viewtype == $COnAttendance::VIEWTYPE_ATTENDANCE) {
    $i8n->title = get_string('user_attendance_title', $pluginname, $userfullname);
} else {
    $i8n->title = get_string('user_progress_title', $pluginname, $userfullname);
}

$PAGE->set_url($baseurl.'/my_status.php', $urlParam);
$PAGE->set_title($courseInfo->fullname.' : ' . $i8n->title);
$PAGE->set_heading($i8n->title);
$PAGE->navbar->add($i8n->title, $PAGE->url);

// [sectionid] => status
$userSectionStatus = array();
if ($tmpUserSectionStatus = $COnAttendance->getSectionUserStatus($user->id)) {
    foreach ($tmpUserSectionStatus as $uss) {
        $userSectionStatus[$uss->sectionid] = $uss->status;
    }
    
    unset($tmpUserSectionStatus);
}

// [cmid] => status
$userModuleStatus = $COnAttendance->getUserModuleStatus($user->id);

// 강좌에 존재하는 주차
$modinfo = get_fast_modinfo($courseInfo->id);
$courseSections = $COnAttendance->getCourseSections($modinfo);

// 화면 출력을 위해 전달되어야 할 객체
$lists = array();

foreach ($courseSections as $cs) {
    $section = new stdClass();
    $section->id = $cs->id;
    $section->section = $cs->section;
    $section->cms = array();
    $section->status = $userSectionStatus[$section->id] ?? \local_ubonattend\Attendance::CODE_ABSENCE;
    
    // $section->name = $CCourse->getSectionName($courseInfo, $section->section);
    
    // 진도처리하는 모듈이 존재하는 경우
    if (!empty($cs->allowModules)) {
        foreach ($cs->allowModules as $cmid) {
            $cminfo = $modinfo->get_cm($cmid);
            
            // 뷰어 열람 횟수 가져오기
            $query = "SELECT * FROM {".$cminfo->modname."_track} WHERE ".$cminfo->modname."id = :moduleid AND userid = :userid";
            $param = array('userid' => $user->id, 'moduleid' => $cminfo->instance);
            $trackInfo = $DB->get_record_sql($query, $param);
            
            $userStatus = $userModuleStatus[$cmid] ?? null;
            
            $cm = new stdClass();
            $cm->id = $cminfo->id;
            $cm->name = $cminfo->name;
            $cm->url = $cminfo->url;
            $cm->onclick = $cminfo->onclick;
            $cm->modname = $cminfo->modname;
            $cm->icon = $cminfo->get_icon_url();
            $cm->customdata = $cminfo->customdata;
            $cm->playtime = $cminfo->customdata->playtime;
            
            // 출석 상태
            $cm->status = \local_ubonattend\Attendance::CODE_ABSENCE;
            $cm->percent = '-';
            
            // trackinfo 관련 설정
            $cm->progress = '-';
            $cm->progress_time = '-';
            $cm->totalprogress = '-';
            $cm->totalprogress_time = '-';
            $cm->attempts = 0;
            $cm->recognition = 0;
            
            // trackInfo가 조회된 경우만 전달
            if (!empty($trackInfo)) {
                $cm->progress = $trackInfo->progress;
                $cm->progress_time = $trackInfo->progress_time;
                
                $cm->totalprogress = $trackInfo->totalprogress;
                $cm->totalprogress_time = $trackInfo->totalprogress_time;
                
                $cm->attempts = $trackInfo->attempts;
                $cm->recognition = $trackInfo->recognition;
            }
            
            // 출석 상태값
            if (!empty($userStatus)) {
                $cm->status = $userStatus->status;
                $cm->progress = $userStatus->progress.'%';
            }
            
            $section->cms[] = $cm;
        }
    }
    $section->rowspan = count($section->cms);
    $lists[] = $section;
}


// mustache에서는 조건문이 사용안되기 때문에 이곳에서 html을 구성해서 전달합니다.
$tbody = '';

$isOnlinePage = ($isOnlineAttendance && $viewtype == $COnAttendance::VIEWTYPE_ATTENDANCE) ? true : false;

$config = $COnAttendance->getConfig();
if ($config->progresstype == \local_ubonattend\Progress::PROGRESSTYPE_PERCENT) {
    $cumulativeTitle = get_string('cumulative_maxposition', $pluginname);
    $cumulativeTitleHelp = get_string('cumulative_maxposition_help', $pluginname);
} else {
    $cumulativeTitle = get_string('cumulative_opentime', $pluginname);
    $cumulativeTitleHelp = get_string('cumulative_opentime_help', $pluginname);
}



if ($isOnlinePage) {
    include '_my_status_attendance.php';
} else {
    include '_my_status_progress.php';
} 

// 필요한 값 셋팅
$ctx = new stdClass();
$ctx->user = $user;
$ctx->fullname = $userfullname;
$ctx->tbody = $tbody;
$ctx->listurl = $baseurl.'/report.php?id='.$courseInfo->id.'&viwtype='.$viewtype;
$ctx->isOnlineAttendance = $isOnlineAttendance;
$ctx->isOnlinePage = $isOnlinePage;
$ctx->cumulativeTitle = $cumulativeTitle;
$ctx->cumulativeTitleHelp = $cumulativeTitleHelp;
$ctx->trackBackgroundDesc = $COnAttendance->getTrackBackgroundDesc();
$ctx->trackDetailBackgroundDesc = $COnAttendance->getTrackBackgroundDesc(false);

$PAGE->requires->js_call_amd('local_ubonattend/attendance', 'user_status', array(
    'courseid' => $courseInfo->id
    ,'userid' => $user->id
    ,'action' => $baseurl.'/action.php'
    ,'isOnlineAttendance' => $isOnlineAttendance
));

$PAGE->requires->strings_for_js(array(
    'confirm_recognition_y'
    ,'confirm_recognition_n'
), $pluginname);

echo $OUTPUT->header();
echo $COnAttendance->getTabHTML('index');
echo $OUTPUT->render_from_template('local_ubonattend/my_status', $ctx);
echo $OUTPUT->footer();