<?php
use \local_ubion\base\Common;
use \local_ubion\base\Parameter;

require '_head.php';

$COnAttendance = \local_ubonattend\Attendance::getInstance($courseInfo->id);
$CUser = \local_ubion\user\User::getInstance();
$CGroup = \local_ubion\group\Group::getInstance();

$page       = optional_param('page', 1, PARAM_INT);     		// page
$ls			= optional_param('ls', Parameter::getDefaultListSize(), PARAM_ALPHANUMEXT);				// 리스트 갯수
$viewtype   = optional_param('viewtype', null, PARAM_INT);
$keyfield   = optional_param('keyfield', null, PARAM_ALPHANUMEXT);
$keyword 	= Parameter::getKeyword();
$groupid    = optional_param('groupid', null, PARAM_INT);
$order		= optional_param('sby', null, PARAM_ALPHA);
$orderType  = optional_param('sorder', 'ASC', PARAM_ALPHA);

$i8n->title = get_string('tab_progress', $pluginname);
if ($isOnlineAttendance) {
    $i8n->title = get_string('tab_status', $pluginname);
}
$i8n->number = get_string('number', 'local_ubion');
$i8n->search = get_string('search');
$i8n->fullnameuser = get_string('fullnameuser');

$i8n->group = get_string('groups', 'group');
$i8n->department = get_string('department');
$i8n->idnumber = get_string('idnumber');
$i8n->userpic = get_string('userpic');

$i8n->attendance = get_string('attendance', 'local_uboffattend');
$i8n->absence = get_string('absence', 'local_uboffattend');
$i8n->late = get_string('late', 'local_uboffattend');

$PAGE->set_url($baseurl.'/report.php?id='.$courseInfo->id);
$PAGE->set_title($courseInfo->fullname.' : ' . $i8n->title);
$PAGE->set_heading($i8n->title);
$PAGE->navbar->add($i8n->title, $baseurl.'/report.php?id='.$courseInfo->id);

// 그룹 설정
// 강좌에서 그룹을 분류해서 사용하는지 확인
// 출석부에 그룹 정보가 표시되는게 이상하기 때문에 group정보는 표시되지 않도록 변경함.
$courseGroup = $CGroup->getCourseGroups($courseInfo->id);

// 온라인 출석부를 사용하면 기본 화면은 온라인 출석부가 표시되어야 함.
if ($COnAttendance->isAttendanceCourse()) {
    if (empty($viewtype)) {
        $viewtype = $COnAttendance::VIEWTYPE_ATTENDANCE;
    }
}


$PAGE->requires->js_call_amd('local_ubonattend/attendance', 'report', array(
    'fixedColumnCount' => 4
));


echo $OUTPUT->header();
echo $COnAttendance->getTabHTML('index');



// !!!!!!!!!
// 주차별 표시하는데 mustache로 구현하는데(if 조건문을 사용못함 ㅠ) 시간이 많이 걸리는 관계로 php 파일에서 처리합니다.
// !!!!!!!!!

// 리스트별 사용자 목록 가져오기
// 전체인 경우
$addField = array();

list($totalCount, $users) = $COnAttendance->getCourseUsers($courseInfo, $addField, $keyfield, $keyword, $groupid, null, $page, $ls, $order, $orderType);

// 페이징 처리하는데 모든 사용자의 정보를 다 가져올 필요가 없기 때문에 in 조건문을 사용하기 위해 조회된 사용자의 inquery문 작성
$userInQuery = null;
$comma = null;
foreach ($users as $u) {
    $userInQuery .= $comma.$u->id;
    $comma = ',';
}


?>

<div class="local-ubonattend">
	<form class="form-horizontal well form-search" method="get">
		<div class="form-group">
			<label class="control-label col-sm-3"><?= get_string('paging_number_list', 'local_ubion')?></label>
			<div class="col-sm-9">
				<select name="listsize" class="form-control form-control-auto form-control-listsize" id="listsize">
					<?php
					if ($listSizes = Parameter::getListSizes(true)) {
					    foreach ($listSizes as $lsKey => $lsValue) {
					        $selected = ($lsKey == $ls) ? ' selected="selected"' : '';
					        echo '<option value="'.$lsKey.'" '.$selected.'>'.$lsValue.'</option>';
					    }
					}
					?>
				</select>
			</div>
		</div>
		
		<?php
        if ($courseGroup->is) {
        ?>
		<div class="form-group row">
			<label class="control-label col-sm-3"><?= $i8n->group; ?></label>
			<div class="col-sm-9">
				<select name="groupid" class="form-control form-control-auto form-control-group">
					<?php
					echo $CGroup->getOptions($courseGroup, $groupid);
					?>
				</select>
			</div>
		</div>
		<?php
        }
        ?>
		
		<div class="form-group row">
			<label class="control-label col-sm-3" for="id_keyword"><?= $i8n->search; ?></label>
			<div class="col-sm-9">	
				<div class="form-inline">
					<input type="hidden" name="id" value="<?= $courseInfo->id; ?>" />
					<select name="keyfield" class="keyfield form-control mb-2 mb-md-0">
						<option value="idnumber" <?php echo ($keyfield == 'idnumber') ? 'selected="selected"' : ''; ?>><?= $i8n->idnumber; ?></option>
						<option value="username" <?php echo ($keyfield == 'username') ? 'selected="selected"' : ''; ?>><?= $i8n->fullnameuser; ?></option>
					</select>
					<input type="text" name="keyword" value="<?= $keyword; ?>" placeholder="<?= $i8n->search; ?>" class="required form-control  mb-2 mb-md-0" id="id_keyword" />
					<button type="submit" class="search btn btn-default mb-2 mb-md-0"><?= $i8n->search; ?></button>
					<?php 
					if (!empty($keyword)) {
					    echo '<a href="'.$baseurl.'/report.php?id='.$courseInfo->id.'" class="btn-search-cancel btn btn-default">'.get_string('search_cancel', 'local_ubion').'</a>';
					}
					?>
				</div>
			</div>
		</div>
	</form>
	
	<div class="user-status well">
		<?php 
		// 사용자 환경에 맞는 파일 로드
		$filename = '_report';
		$filename .= ($viewtype == $COnAttendance::VIEWTYPE_ATTENDANCE) ? '_attendance' : '_progress';
		if (Common::isMobile()) {
		    $filename .= '_mobile';
		}
		
		$filename .= '.php';
		include ($filename);
		?>
	</div>
</div>

<?php 
echo $OUTPUT->footer();