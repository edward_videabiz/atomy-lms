<?php
$string['all_list'] = '전체 목록';
$string['attempt'] = '회차';
$string['attendance_week_batch'] = '일괄출석인정';
$string['attendance_week_batch_help'] = '체크한 주차에 대해서 모든 사용자는 출석으로 인정 됩니다.';
$string['attendance_end'] = '출석 인정기간';
$string['attendance_lateness'] = '지각 인정기간';
$string['attendance_lateness_percent'] = '지각인정범위(%)';
$string['attendance_percent'] = '출석인정범위(%)';
$string['attendance_section_status'] = '주차 출석';
$string['attendance_start'] = '시작일';
$string['attendance_status'] = '출석';
$string['backgroundcolor_attendance'] = '출석 (기간내 학습)';
$string['backgroundcolor_late'] = '지각';
$string['backgroundcolor_outofprogress'] = '진도처리 기간외 학습';
$string['backgroundcolor_some_track'] = '출결상태 변경 구간';
$string['backgroundcolor'] = '배경색';
$string['confirm_section_change'] = '학습기간 중에 기간을 변경할 경우 출석부 기능이 정상동작하지 않을수 있습니다.';
$string['confirm_recognition_y'] = '{$a} 처리 하시겠습니까?';
$string['confirm_recognition_n'] = '{$a}을 취소하시겠습니까?';
$string['course_end_message'] = '강좌가 종료된 경우 온라인 출석부 설정 변경은 불가능합니다.';
$string['course_modules'] = '강의 자료';
$string['cumulative_maxposition'] = '누적<br/>최대 학습구간';
$string['cumulative_maxposition_help'] = '사용자가 재생한 최대 재생위치 (열람 시간과는 무관합니다.)';
$string['cumulative_opentime'] = '누적<br/>열람 시간';
$string['cumulative_opentime_help'] = '사용자가 뷰어를 열람한 누적시간';
$string['error_notuse'] = '온라인 출석부가 비활성화 되어 있습니다.';
$string['error_notuse_course'] = '온라인 출석부를 사용하지 않는 강좌입니다.';
$string['error_oldate'] = '시작일이 이전 시작일인({$a})과 동일하거나 이후로 설정 해 주세요.';
$string['error_oldate2'] = '출석인정기간은 시작일인({$a})보다 이후로 설정 해 주세요.';
$string['error_oldate3'] = '지각인정기간은 출석인정기간({$a})과 동일하거나 이후로 설정 해 주세요.';
$string['error_percent'] = '출석인정범위(%)는 지각인정범위(%) 보다 큰값으로 입력하셔야 합니다.';
$string['error_percent2'] = '지각인정범위(%)는 0% ~ 출석인정범위(%)로 지정 해 주시기 바랍니다.';
$string['event_grade_applied'] = '출석 성적 반영';
$string['event_grade_created'] = '출석 성적 항목 등록';
$string['event_grade_updated'] = '출석 성적 항목 수정';
$string['event_section_changed'] = '온라인 출석부 설정';
$string['grace_period'] = '유예기간';
$string['grace_period_desc'] = '유예기간 : 사용자-서버간에 네트워크 지연으로 기록이 늦게 되는 경우가 발생되기 때문에 진도처리 종료일에 유예기간을 추가함';
$string['grade_noexist'] = '<p>성적 항목이 존재하지 않습니다.</p><p>성적 항목을 추가하시겠습니까?</p>';
$string['gradename'] = '온라인 출석부';
$string['ip'] = 'IP 주소<br/>(기기유형)';
$string['late'] = '지각';
$string['no_data'] = '열람 이력이 없습니다.';
$string['no_data_recognition'] = '인정 값이 전달되지 않았거나, 존재하지 않는 인정값이 전달되었습니다.';
$string['no_save_recognition_lateness'] = '지각 기능을 사용하지 않는 강좌이므로 지각 인정 처리할수 없습니다.';
$string['no_time_limit'] = '기간제한없음';
$string['notchange_setting'] = '출석(진도) 설정은 {$a}에서 변경 가능합니다.';
$string['notfound_week'] = '등록된 주차가 없습니다.';
$string['onlineattendance'] = '온라인 출석부';
$string['playtime'] = '콘텐츠 길이';
$string['playtime0'] = '재생시간이 0으로 설정되어 있습니다. 관리자에게 문의하여 주시기 바랍니다.';
$string['pluginname'] = '온라인 출석부';
$string['privacy:metadata'] = 'The ubonattend plugin does not store any personal data about any user.';
$string['progress_rate'] = '진도율';
$string['progresstype'] = '진도 처리 방식';
$string['progresstype_open'] = '열람 여부';
$string['progresstype_opentime'] = '열람 시간';
$string['progresstype_override_opentime'] = '해당 모듈은 강좌 설정과 무관하게 <strong>열람 시간</strong>으로 체크 됩니다.';
$string['progresstype_progress'] = '진도율';
$string['recognition_absence'] = '결석인정';
$string['recognition_attendance'] = '출석인정';
$string['recognition_cancel'] = '인정취소';
$string['recognition_lateness'] = '지각인정';
$string['recognition_not_study'] = '학습미인정';
$string['recognition_study'] = '학습인정';
$string['setting_allow_module'] = '진도처리 모듈';
$string['setting_allow_module_desc'] = '진도처리 기능을 사용하는 모듈명을 ,로 구분해서 입력해주십시요.';
$string['setting_grade_absence'] = '결석 차감';
$string['setting_grade_absence_desc'] = '결석 차감';
$string['setting_grade_lateness'] = '지각 차감';
$string['setting_grade_lateness_desc'] = '지각 차감';
$string['setting_grade_minscore'] = '출석 최저 점수';
$string['setting_grade_minscore_desc'] = '출석 최저 점수';
$string['setting_grade_score'] = '출석 점수';
$string['setting_grade_score_desc'] = '출석 점수';
$string['setting_heading_default'] = '기본 설정';
$string['setting_heading_grade'] = '성적 설정';
$string['setting_heading_progress'] = '진도 설정';
$string['setting_percent_attend'] = '출석 인정률';
$string['setting_percent_attend_desc'] = '사용자 진도율이 입력한 출석 인정률 보다 크거나 같으면 출석으로 인정됩니다.';
$string['setting_percent_lateness'] = '지각 인정률';
$string['setting_percent_lateness_desc'] = '사용자 진도율이 입력한 지각 인정률 보다 크거나 같으면 지각으로 인정됩니다.';
$string['setting_progresstype_default'] = '기본 진도처리방식';
$string['setting_progresstype_default_desc'] = '기본값으로 사용될 진도처리 방식을 선택해주십시요.';
$string['setting_use'] = '온라인 출석부 사용여부';
$string['setting_use_desc'] = '체크시 온라인 출석부를 사용하실수 있습니다.';
$string['setting_use_in_course'] = '강좌내 온라인 출석부 사용여부';
$string['setting_use_in_course_desc'] = '체크시 강좌내 온라인 출석부는 기본 활성화 됩니다.';
$string['setting_use_late'] = '지각 기능 사용여부';
$string['setting_use_late_desc'] = '체크시 지각 기능을 사용하실수 있습니다.';
$string['setting_use_late_in_course'] = '강좌내 지각 기능 사용여부';
$string['setting_use_late_in_course_desc'] = '체크시 강좌내 지각 기능이 기본 활성화 됩니다.';
$string['sometrack_absence'] = '기간외 : ';
$string['sometrack_attendance'] = '출석 : ';
$string['sometrack_lateness'] = '지각 : ';
$string['tab_grade'] = '성적 항목 관리';
$string['tab_progress'] = '진도 현황';
$string['tab_setting'] = '출석부 설정';
$string['tab_status'] = '출석 상태';
$string['time_attend'] = '출석인정요구시간';
$string['time_end'] = '열람 종료 시간';
$string['time_recognized'] = '학습 인정시간';
$string['time_start'] = '열람 시작 시간';
$string['time_study'] = '열람 시간';
$string['time_study_help'] = '뷰어를 열람한 시간';
$string['total_all'] = '총 열람시간 (진도처리 기간외 학습 포함)';
$string['total_attendancetime'] = '출석 인정시간';
$string['total_etc'] = '기간외 학습시간';
$string['total_latenesstime'] = '지각 인정시간';
$string['user_attendance_title'] = '[{$a}] 출석 현황';
$string['user_progress_title'] = '[{$a}] 진도 현황';
$string['view_attempts'] = '{$a}회 열람';
$string['week'] = '주차';