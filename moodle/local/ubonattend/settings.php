<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/local/ubonattend/lib.php');

if ($hassiteconfig) {
    $pluginname = 'local_ubonattend';

    $settings = new admin_settingpage('local_ubonattend', get_string('pluginname', $pluginname));
	
    ###############
	## 기본 설정 ##
	###############
    $settings->add(new admin_setting_heading('local_ubonattend_default', get_string('setting_heading_default', $pluginname), ''));
    
    // 온라인 출석부 사용여부
    $name = $pluginname.'/use';
    $title = get_string('setting_use', $pluginname);
    $description = get_string('setting_use_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('local_ubonattend_config_reset_caches');
    $settings->add($setting);
    
    
    // 강좌내 온라인 출석부 사용여부
    $name = $pluginname.'/use_in_course';
    $title = get_string('setting_use_in_course', $pluginname);
    $description = get_string('setting_use_in_course_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
    
    
    // 지각 기능 사용여부
    $name = $pluginname.'/use_late';
    $title = get_string('setting_use_late', $pluginname);
    $description = get_string('setting_use_late_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('local_ubonattend_config_reset_caches');
    $settings->add($setting);
    
    
    // 강좌내 지각기능 기본 활성화여부
    $name = $pluginname.'/use_late_in_course';
    $title = get_string('setting_use_late_in_course', $pluginname);
    $description = get_string('setting_use_late_in_course_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
    
    
    ###############
    ## 진도 설정 ##
    ###############
    $settings->add(new admin_setting_heading('local_ubonattend_progress', get_string('setting_heading_progress', $pluginname), ''));
    
    // 진도처리를 진행하는 모듈
    $name = $pluginname.'/allow_modules';
    $title = get_string('setting_allow_module', $pluginname);
    $description = get_string('setting_allow_module_desc', $pluginname);
    $default = 'vod, econtents';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_NOTAGS, 50);
    $settings->add($setting);
    
    
    // 기본 진도처리 방법
    $progressTypes = \local_ubonattend\Progress::getProgressTypes();
    $name = $pluginname.'/progress_type';
    $title = get_string('setting_progresstype_default', $pluginname);
    $description = get_string('setting_progresstype_default_desc', $pluginname);
    $default = \local_ubonattend\Progress::PROGRESSTYPE_PERCENT;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $progressTypes);
    $settings->add($setting);
    
    
    // 출석 인정률
    $name = $pluginname.'/percent_attendance';
    $title = get_string('setting_percent_attend', $pluginname);
    $description = get_string('setting_percent_attend_desc', $pluginname);
    $default = 90;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $settings->add($setting);
    
    
    // 지각 인정률
    $name = $pluginname.'/percent_lateness';
    $title = get_string('setting_percent_lateness', $pluginname);
    $description = get_string('setting_percent_lateness_desc', $pluginname);
    $default = 50;
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_INT);
    $settings->add($setting);
    
    
    
    ###############
    ## 성적 설정 ##
    ###############
    $settings->add(new admin_setting_heading('local_ubonattend_grade', get_string('setting_heading_grade', $pluginname), ''));
    
    $options = array();
    for ($i=1; $i<=100; $i++) {
        $options[$i] = $i;
    }
    
    $optionsMinus = array();
    for($i=0; $i >= -20; $i = $i-0.5) {
        $key = ''.$i.'';
        $optionsMinus[$key] = $i;
    }
    
    // 출석점수
    $name = $pluginname.'/gradescore';
    $title = get_string('setting_grade_score', $pluginname);
    $description = get_string('setting_grade_score_desc', $pluginname);
    $default = 20;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $settings->add($setting);
    
    
    // 출석 최저 점수
    $name = $pluginname.'/minscore';
    $title = get_string('setting_grade_minscore', $pluginname);
    $description = get_string('setting_grade_minscore_desc', $pluginname);
    $default = 0;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $settings->add($setting);
    
        
    // 지각 차감 점수
    $name = $pluginname.'/lateness';
    $title = get_string('setting_grade_lateness', $pluginname);
    $description = get_string('setting_grade_lateness_desc', $pluginname);
    $default = -1;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $optionsMinus);
    $settings->add($setting);
    
        
    // 결석 차감 점수
    $name = $pluginname.'/absence';
    $title = get_string('setting_grade_absence', $pluginname);
    $description = get_string('setting_grade_absence_desc', $pluginname);
    $default = -2;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $optionsMinus);
    $settings->add($setting);
    
    
    
	$ADMIN->add('localplugins', $settings);
}