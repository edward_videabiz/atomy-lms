<?php

$definitions = array(
    'config' => array(
        'mode' => cache_store::MODE_APPLICATION,
        'persistent' => true,
        'simplekeys' => true,
        'ttl' => 3600,
    ),
);
