<?php
function xmldb_local_ubonattend_upgrade($oldversion) {
	global $DB;

	$dbman = $DB->get_manager();

	if ($oldversion < 2018070505) {
        
        // uboffattend savepoint reached.
	    upgrade_plugin_savepoint(true, 2018070505, 'local', 'ubonattend');
    }
    
    
    if ($oldversion < 2018070506) {
        
        // Define field attendtype to be dropped from local_ubonattend_config.
        $table = new xmldb_table('local_ubonattend_config');
        $field = new xmldb_field('attendtype');
        
        // Conditionally launch drop field attendtype.
        if ($dbman->field_exists($table, $field)) {
            $dbman->drop_field($table, $field);
        }
        
        // Ubonattend savepoint reached.
        upgrade_plugin_savepoint(true, 2018070506, 'local', 'ubonattend');
    }
    
    
	return true;
}