define(['jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate', 'theme_coursemos/jquery.filedownload', 'theme_coursemos/jquery.tableHeadFixer'], function($) {
	var attendance = {};
	
	attendance.pluginname = 'local_ubonattend';
	
	// 온라인 출석부(보고서)
	attendance.report = function(fixedColumnCount) {
	   // 한페이지에 보여줄 사용자 수...
        $(".local-ubonattend .form-search .form-control-listsize").change(function() {
            location_search('ls', $(this).val());
        });
        
        // 그룹
        $(".local-ubonattend .form-search .form-control-group").change(function() {
            location_search('groupid', $(this).val());
        });
        
        
        $(".local-ubonattend .table-online-attendance").tableHeadFixer({"head" : false, "left" : fixedColumnCount});
	}
	
	
	// 온라인 출석부(보고서)
    attendance.user_status = function(courseid, userid, action, isOnlineAttendance) {
        
        $(".local-ubonattend .btn-attempt").click(function() {
            cmid = $(this).data('cmid');  
            
            if (cmid) {
                param = coursemostype + '=attempts&id='+courseid+'&userid=' + userid + '&cmid=' + cmid
               
                $.ajax({
                     data: param
                    ,url : action
                    ,success:function(data){
                        if(data.code == mesg.success) {
                            $(".local-ubonattend .modal-attempt .modal-title").text(data.title);
                            
                            $(".local-ubonattend .modal-attempt .moduleInfoTable").html(data.moduleInfoTable);
                            $(".local-ubonattend .modal-attempt .table-progress-lists tbody").html(data.html);
                            
                            $(".local-ubonattend .modal-attempt .cumulative-title").html(data.cumulativeTitle);
                            $(".local-ubonattend .modal-attempt .cumulative-help").attr('title', data.cumulativeHelp).attr('data-original-title', data.cumulativeHelp);
                            
                            
                            $(".local-ubonattend .modal-attempt").modal('show');
                            
                            // 다른 모듈의 상세 정보를 확인하다보면 스크롤이 하단에 위치하는 경우가 있기 때문에
                            // show시 스크롤 위치를 0으로 맞춰줌
                            $(".local-ubonattend .modal-attempt .modal-body").scrollTop(0);
                        } else {
                            alert(data.msg);
                        }
                    }
                    ,error: function(xhr, option, error){
                        alert(xhr.status + ' : ' + error);
                    }
                });
            } 
        });
        
        $(".local-ubonattend .btn-detail").click(function() {
            cmid = $(this).data('cmid');  
            
            if (cmid) {
                param = coursemostype + '=details&id='+courseid+'&userid=' + userid + '&cmid=' + cmid
               
                $.ajax({
                     data: param
                    ,url : action
                    ,success:function(data){
                        if(data.code == mesg.success) {
                            $(".local-ubonattend .modal-track-detail .modal-title").text(data.title);
                            $(".local-ubonattend .modal-track-detail .moduleInfoTable").html(data.moduleInfoTable);
                            $(".local-ubonattend .modal-track-detail .trackid").html(data.trackid);
                            $(".local-ubonattend .modal-track-detail .table-trackdetail tbody").html(data.html);
                            
                            $(".local-ubonattend .modal-track-detail").modal('show');
                            
                         // 다른 모듈의 상세 정보를 확인하다보면 스크롤이 하단에 위치하는 경우가 있기 때문에
                            // show시 스크롤 위치를 0으로 맞춰줌
                            $(".local-ubonattend .modal-track-detail .modal-body").scrollTop(0);
                        } else {
                            alert(data.msg);
                        }
                    }
                    ,error: function(xhr, option, error){
                        alert(xhr.status + ' : ' + error);
                    }
                });
            }
            
            
        });
        
        
        $(".local-ubonattend .btn-recognition").click(function() {
            cmid = $(this).data('cmid');  
            
            if (cmid) {
                recognition = $(this).data('recognition');
                
                // 숫자 형태인 경우
                if ($.isNumeric(recognition)) {
                    statusName = $(this).text();
                    confirmMessage = 'confirm_recognition_y';
                    
                    if (recognition == "0") {
                        confirmMessage = 'confirm_recognition_n';
                        statusName = $(this).data('statusname');
                    }
                    
                    
                    
                    if (confirm(M.util.get_string(confirmMessage, 'local_ubonattend', statusName))) {
                        
                        param = coursemostype + '=recognition&id='+courseid+'&userid=' + userid + '&cmid=' + cmid + '&recognition=' + recognition;
                        
                        $.ajax({
                            data: param
                           ,url : action
                           ,success:function(data){
                               if(data.code == mesg.success) {
                                   location.reload();
                               } else {
                                   alert(data.msg);
                               }
                           }
                           ,error: function(xhr, option, error){
                               alert(xhr.status + ' : ' + error);
                           }
                       });        
                    }   
                }
            }
            
            
        });
        
    }
	
	// 성적 관리
	attendance.grade = function() {
		// 일괄 처리 validate 설정
		$(".local-ubonattend .form-grade").validate();
	}
	
	attendance.setting = function(isLate) {
	    
	    $(".local-ubonattend .form-setting").validate({
	        submitHandler : function(form) {
	            var isSuccess = true;
	            var oldDate = '';
	            
	            $(".local-ubonattend .form-setting .table-sections tr.section").each(function() {
	                section = $(this).data('section');
	                
	                // element
	                elStartDate = $(this).find('input[name="start_time_'+section+'"]');
	                elEndDate = $(this).find('input[name="end_time_'+section+'"]');
	                elAttendancePercent = $(this).find('input[name="attendance_percent_'+section+'"]');
	                
	                startDate = elStartDate.val();
	                endDate = elEndDate.val();
	                attendancePercent = parseInt(elAttendancePercent.val())
	                
	                if (isLate) {
	                    elLatenessDate = $(this).find('input[name="lateness_time_'+section+'"]');
	                    elLatenessPercent = $(this).find('input[name="lateness_percent_'+section+'"]');
	                    
	                    latenessDate = elLatenessDate.val();
	                    latenessPercent = parseInt(elLatenessPercent.val())
	                }
	                
	                if (oldDate!='' && startDate < oldDate) {
	                    alert(M.util.get_string('error_oldate', 'local_ubonattend', oldDate));
	                    elStartDate.focus();
	                    isSuccess = false;
	                    return false;
	                }
	                
	                
	                if (endDate < startDate) {
	                    alert(M.util.get_string('error_oldate2', 'local_ubonattend', startDate));
	                    elEndDate.focus();
	                    isSuccess = false;
	                    return false;
	                }
	                
	                if (isLate) {
	                    if (attendancePercent < latenessPercent) {
	                        alert(M.util.get_string('error_percent', 'local_ubonattend'));
	                        elAttendancePercent.focus();
	                        isSuccess = false;
	                        return false;
	                    }
	                    
	                    if (latenessDate < endDate) {
	                        alert(M.util.get_string('error_oldate3', 'local_ubonattend', endDate));
	                        elLatenessDate.focus();
	                        isSuccess = false;
	                        return false;
	                    }
	                }
	                

	                if (latenessPercent < 0) {
	                    alert(M.util.get_string('error_percent2', 'local_ubonattend'));
	                    elLatenessPercent.focus();
	                    isSuccess = false;
	                    return false;
	                }
	                
	                oldDate = startDate;
	            });  
	            
	            
	            
	            if (isSuccess) {
	                if (!confirm(M.util.get_string('confirm_section_change', 'local_ubonattend'))) {
	                    return false;
	                }

	                // 버튼 비활성화
	                $(".local-ubonattend .form-setting .btn-save").attr('disabled', 'disalbed');
	                
	                // 진도 재계산이 돌아가기 때문에 늦게 처리될수가 있음.
	                showSubmitProgress();
	                
	                form.submit();
	            }
	            
	            return false;
	        }
	    });
	}
	
	
	return attendance;
});