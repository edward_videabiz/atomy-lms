<?php
require '_head.php';

$i8n->title = get_string('tab_grade', $pluginname);

$PAGE->set_url($baseurl.'/grade.php?id='.$courseInfo->id);
$PAGE->set_title($courseInfo->fullname.' : ' . $i8n->title);
$PAGE->set_heading($i8n->title);
$PAGE->navbar->add($i8n->title, $baseurl.'/grade.php?id='.$courseInfo->id);


// 기존에 설정된 오프라인 성적 차감점수 가져오기
$attendanceConfig = $COnAttendance->getConfig();

$isGrade = false;
$type = 'gradeInsert';
$gradeName = get_string('gradename', $pluginname);
if ($gradeItem = $COnAttendance->getGradeItem()) {
	$isGrade = true;
	$type = 'gradeUpdate';
	$gradeName = $gradeItem->itemname;
}

$PAGE->requires->js_call_amd('local_ubonattend/attendance', 'grade');

// mustache에 전달된 객체
$ctx = new stdClass();
$ctx->typename = $COnAttendance::TYPENAME;
$ctx->type = $type;
$ctx->tabHTML = $COnAttendance->getTabHTML('grade');
$ctx->courseid = $courseInfo->id;

// 이전 페이지로 돌아갈 주소
$ctx->settingURL = $baseurl.'/setting.php?id='.$courseInfo->id;

#### 성적 항목 ####
// 성적 항목 존재여부
$ctx->isGrade = $isGrade;

// 성적명
$ctx->gradeName = $gradeName;


$gradeScoreOption = array();
for ($i = 0; $i <= 100; $i++) {
    $gradeScoreOption[$i] = $i;
}

$minusScoreOption = array();
for ($i = 0; $i >= -20; $i = $i-0.5) {
    // string으로 변경하지 않으면 0 항목이 표시되지 않음.
    $stringi = (string)$i;
    $minusScoreOption[$stringi] = $stringi;   
}

$ctx->gradeScoreOption = \local_ubion\base\Common::getArrayKeyValueObject($gradeScoreOption, $attendanceConfig->gradescore);
$ctx->minScoreOption = \local_ubion\base\Common::getArrayKeyValueObject($gradeScoreOption, $attendanceConfig->minscore);

$ctx->latenessOption = \local_ubion\base\Common::getArrayKeyValueObject($minusScoreOption, $attendanceConfig->lateness);
$ctx->absenceOption = \local_ubion\base\Common::getArrayKeyValueObject($minusScoreOption, $attendanceConfig->absence);




echo $OUTPUT->header();
echo $OUTPUT->render_from_template('local_ubonattend/grade', $ctx);
echo $OUTPUT->footer();