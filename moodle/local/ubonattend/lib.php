<?php
function local_ubonattend_config_reset_caches() {
    $cache = \cache::make('local_ubonattend', 'config');
    $cache->purge();
}