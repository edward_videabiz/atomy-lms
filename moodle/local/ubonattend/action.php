<?php
require('../../config.php');

$id 		= required_param('id', PARAM_INT);

$controllder = \local_ubonattend\Attendance::getInstance($id);

$type = optional_param($controllder::TYPENAME, null, PARAM_ALPHANUM);

if ($controllder->isSubmit($type)) {
    $controllder->invoke($type);
}