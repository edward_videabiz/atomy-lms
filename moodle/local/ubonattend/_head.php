<?php
use local_ubion\course\Course;
use local_ubion\base\Common;

require '../../config.php';

$pluginname = 'local_ubonattend';
$id = required_param('id', PARAM_INT);

$CCourse = Course::getInstance();
$COnAttendance = \local_ubonattend\Attendance::getInstance($id);

$i8n = new stdClass();
$i8n->pluginname = get_string('onlineattendance', $pluginname);

// 강좌 정보 가져오기
$courseInfo = $CCourse->getCourse($id);
$courseContext = context_course::instance($courseInfo->id);

$PAGE->set_context($courseContext);
$PAGE->set_course($courseInfo);
require_login($courseInfo);

$PAGE->set_pagelayout('incourse');
$PAGE->set_heading($courseInfo->fullname);

$isProfessor = $CCourse->isProfessor($courseInfo->id, $courseContext);
$isOnlineAttendance = $COnAttendance->isAttendanceCourse();
$isLate = $COnAttendance->isLateCourse();

$baseurl = $CFG->wwwroot.'/local/ubonattend';
$PAGE->set_url($baseurl.'/index.php?id='.$id);
// 온라인 출석부를 사용하는 경우에만 네비게이션 영역에 온라인 출석부 항목 추가
if ($isOnlineAttendance) {
    $PAGE->navbar->add($i8n->pluginname, $baseurl.'/index.php?id='.$id);
}


// 특정 페이지 이외에는 교수자만 접근 가능해야됨.
$scriptName = explode('/', $_SERVER['SCRIPT_NAME']);
$scriptName = array_pop($scriptName);
$scriptName = str_replace('.php', '', $scriptName);

// 보고서 페이지인 경우에는 따로 온라인 출석부 사용여부를 체크하지 않아도 됨
$reportPages = array('report', 'my_status');

// 온라인 출석부를 사용하지 않으면 진도현황이 표시되기 때문에
// 보고서 페이지인 경우에는 온라인출석부 사용여부를 검사하면 안됨
if (!in_array($scriptName, $reportPages)) {
    // 사이트내 오프라인 출석부 사용여부
    if (! $COnAttendance->isAttendance()) {
        Common::printNotice(get_string('error_notuse', $pluginname), $CFG->wwwroot.'/course/view.php?id='.$id);
    }

    // 강좌내 오프라인 출석부 사용여부
    if (! $isOnlineAttendance) {
        Common::printNotice(get_string('error_notuse_course', $pluginname), $CFG->wwwroot.'/course/view.php?id='.$id);
    }
}

if ($scriptName != 'my_status') {
    if (!$isProfessor) {
        redirect($CFG->wwwroot.'/local/ubonattend/my_status.php?id='.$courseInfo->id);
    }
}
