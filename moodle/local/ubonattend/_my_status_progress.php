<?php
use \local_ubion\base\Common;

foreach ($lists as $l) {
    
    if (!empty($l->cms)) {
        
        $isFirst = true;
        foreach ($l->cms as $cm) {
            $accesslogURL = $CFG->wwwroot.'/report/log/index.php?id='.$courseInfo->id.'&user='.$user->id.'&date=0&modid='.$cm->id.'&chooselog=1';
            $customdata = $cm->customdata;
            
            // 출석인정 요구시간
            $timeAttend = '-';
            
            if ($customdata->playtime > 0) {
                $timeAttend = $customdata->playtime / 100 * $customdata->completionprogress;
                // 출석인정 요구 시간에 대한 반올림 처리
                $timeAttend = round($timeAttend);
            }
            
            $onclick = (!empty($cm->onclick)) ? ' onclick="'.$cm->onclick.'"' : '';
            
            $tbody .= '<tr>';
            if ($isFirst) {
                $rowspan = ($l->rowspan > 1) ? ' rowspan="'.$l->rowspan.'"' : '';
                $tbody .= '<td class="text-center" '.$rowspan.'>'.$l->section.'</td>';
            }
            
            // 학습활동
            $tbody .=     '<td>';
            $tbody .=         '<a href="'.$cm->url.'" '.$onclick.'>';
            $tbody .=             '<img src="'.$cm->icon.'" alt="'.$cm->modname.'" class="mr-2" />'.$cm->name;
            $tbody .=         '</a>';
            $tbody .=     '</td>';
            
            // 콘텐츠 길이
            $tbody .=     '<td class="text-center" title="'.$cm->playtime.'">'.Common::getGMDate($cm->playtime).'</td>';
            
            // 학습시간
            // 진도율
            $progress = $cm->totalprogress;
            if ($config->progresstype != \local_ubonattend\Progress::PROGRESSTYPE_PERCENT) {
                $progress = $cm->totalprogress_time;
            }
            
            $tbody .=     '<td class="text-center" title="'.$progress.'">';
            $tbody .=           Common::getGMDate($progress);
            if ($isProfessor) {
                if (!empty($cm->attempts)) {
                    $tbody .=       '<br/>';
                    $tbody .=       '<button class="btn btn-xs btn-default btn-attempt mt-1" data-cmid="'.$cm->id.'">'.get_string('view_attempts', $pluginname, $cm->attempts).'</button>';
                }
                $tbody .=           '<br/>';
                $tbody .=           '<a href="'.$accesslogURL.'" class="btn btn-xs btn-default mr-1 mt-1" target="_blank">accesslog</a>';
                
                // 상세보기는 관리자만 가능
                if (is_siteadmin()) {
                    $tbody .=       '<button class="btn btn-xs btn-default btn-detail mt-1" data-cmid="'.$cm->id.'">detaillog</button>';
                }
            }
            $tbody .=     '</td>';
            
            // 진도율
            $tbody .=     '<td class="text-center">';
            $tbody .=         $cm->progress;
            
            
            // 재생시간이 0으로 설정되어 있는 경우
            if (empty($customdata->playtime)) {
                $tbody .=    '<br/>';
                $tbody .=     '<span class="small text-info">'.get_string('playtime0', $pluginname).'</span>';
            }
            
            if ($isProfessor) {
                // 인정 / 인정 취소 버튼 표시
                $tbody .=     '<br/>';
                if (empty($cm->recognition)) {
                    // 인정버튼이 현 출석상태값과 동일하면 표시할 필요가 없음
                    if ($cm->status != \local_ubonattend\Attendance::CODE_ATTENDANCE) {
                        // 출석/학습인정 버튼
                        $tbody .= '<button class="btn btn-xs btn-default btn-recognition mt-1" data-cmid="'.$cm->id.'" data-recognition="'.\local_ubonattend\Attendance::CODE_ATTENDANCE.'">';
                        $tbody .=     $i8n->recognition_study;
                        $tbody .= '</button>';
                    }
                    
                    // 인정버튼이 현 출석상태값과 동일하면 표시할 필요가 없음
                    if ($cm->status != \local_ubonattend\Attendance::CODE_ABSENCE) {
                        // 결석인정
                        $tbody .= '<button class="btn btn-xs btn-default btn-recognition ml-1 mt-1" data-cmid="'.$cm->id.'" data-recognition="'.\local_ubonattend\Attendance::CODE_ABSENCE.'">';
                        $tbody .=     $i8n->recognition_not_study;
                        $tbody .= '</button>';
                    }
                    
                } else {
                    $tbody .= 	'<button class="btn btn-xs btn-danger btn-recognition mt-1" data-cmid="'.$cm->id.'" data-recognition="0" data-statusname="'.$i8n->recognition_study.'">';
                    $tbody .= 		$i8n->recognition_cancel;
                    $tbody .= 	'</button>';
                }
            }
            $tbody .=     '</td>';
            
            $tbody .= '</tr>';
            
            $isFirst = false;
        }
    } else {
        $tbody .= '<tr>';
        $tbody .=    '<td class="text-center">'.$l->section.'</td>';
        $tbody .=    '<td class="text-center">&nbsp;</td>';
        $tbody .=    '<td class="text-center">&nbsp;</td>';
        $tbody .=    '<td class="text-center">&nbsp;</td>';
        $tbody .=    '<td class="text-center">&nbsp;</td>';
        $tbody .= '</tr>';
    }
}