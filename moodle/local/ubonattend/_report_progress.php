<?php
use \local_ubion\base\Common;
use local_ubion\base\Paging;

// 진도처리를 진행하는 주차 정보
$courseSections = $COnAttendance->getCourseSections();


// [userid][cmid] = 출석 상태값
// userProgress는 사용자가 뷰어를 열람한 사용자들에 대해서만 기록이 남기 때문에
// 뷰어를 열람하지 않는 사용자에 대한 예외처리는 따로 진행해야됨.
$userModuleProgress = array();
if ($userProgress = $COnAttendance->getModuleUserStatus($userInQuery)) {
    foreach ($userProgress as $up) {
        $userModuleProgress[$up->userid][$up->cmid] = $up->progress.'%';
    }
}

if ($totalCount > 0) {
    $modinfo = get_fast_modinfo($courseInfo->id);
?>

<div class="table-responsive">
	<table class="table table-bordered table-coursemos table-progress table-online-attendance mb-0">
		<thead>
			<tr>
				<th class="fixedcolumn min-wp-50" rowspan="2"><?= $i8n->number; ?></th>
				<th class="fixedcolumn min-wp-60" rowspan="2"><?= $i8n->userpic; ?></th>
				<th class="fixedcolumn min-wp-100" rowspan="2"><?= Common::getSortLink($i8n->idnumber, 'idnumber'); ?></th>
				<th class="fixedcolumn min-wp-120 leftlastfixedcolumn" rowspan="2"><?= Common::getSortLink($i8n->fullnameuser, 'fullname'); ?></th>
				<th class="min-wp-120" rowspan="2"><?= $i8n->department; ?></th>
				<?php
				if ($courseGroup->is) {
					echo '<th class="min-wp-80" rowspan="2">'.$i8n->group.'</th>';
				}
				
				// 강좌에 존재하는 주차 출력
				foreach ($courseSections as $cs) {
			        $sectionName = $CCourse->getSectionName($courseInfo, $cs->section);
			        
			        $moduleCount = count($cs->allowModules);
			        $colspan = ($moduleCount > 1) ? ' colspan="'.$moduleCount.'"' : '';
			        
			        echo '<th class="text-truncate max-wp-50 min-wp-50 borderbottom" title="'.$sectionName.'" data-toggle="tooltip" data-container="body" '.$colspan.'>';
				    echo    $sectionName;
				    echo '</th>';
				}
				?>
			</tr>
			<tr>
			<?php 
			foreach ($courseSections as $cs) {
			    if (!empty($cs->allowModules)) {
			        foreach ($cs->allowModules as $cmid) {
			            echo '<th class="text-center nofixed text-truncate max-wp-50 min-wp-50">';
			            if ($cm = $modinfo->get_cm($cmid)) {
			                echo '<img src="'.$cm->get_icon_url().'" alt="'.$cm->modname.'" title="'.$cm->name.'" data-toggle="tooltip" data-container="body" />';
			            } else {
                            echo '&nbsp;';   
			            }
			            echo '</th>';
			        }
			    } else {
			        echo '<th class="nofixed text-truncate max-wp-50 min-wp-50">&nbsp;</th>';
			    }
			}
			?>
			</tr>
		</thead>
		<tbody>
			<?php 
			$page = empty($page) ? 1 : $page;
			$number = (($page-1) * $ls) + 1;
			
			$myStatusURL = $CFG->wwwroot.'/local/ubonattend/my_status.php?id='.$courseInfo->id.'&userid=';
            foreach ($users as $u) {
                $idnumber = (!empty($u->idnumber)) ? $u->idnumber : '&nbsp;';
                
			    echo '<tr>';
			    echo     '<td class="text-center">'.$number.'</td>';
			    echo     '<td class="text-center">';
			    echo         '<img src="'.$CUser->getPicture($u).'" alt="" />';
			    echo     '</td>';
			    
			    echo     '<td class="text-center">';
			    // 교수자만 볼수 있기 때문에 학번을 그대로 노출
			    echo         $idnumber;
			    echo     '</td>';
			    echo     '<td class="text-center">';
			    echo         '<a href="'.$myStatusURL.$u->id.'">'.fullname($u).'</a>';
			    echo     '</td>';
			    echo     '<td class="text-center">';
			    echo         $CUser->getDepartment($u, true);
			    echo     '</td>';
			    
			    if ($courseGroup->is) {
			        $groupName = $CGroup->getUserGroupBadge($courseInfo->id, $u->id, null, true);
			        echo '<td class="text-center">'.$groupName.'</td>';
			    }
			    
			    
			    foreach ($courseSections as $cs) {
			        if (!empty($cs->allowModules)) {
			            foreach ($cs->allowModules as $cmid) {
        			        echo '<td class="text-center">';
        			        echo     $userModuleProgress[$u->id][$cmid] ?? '&nbsp;';
        			        echo '</td>';
			            }
			        } else {
			            echo '<td class="text-center">&nbsp;</td>';
			        }
			    }
			    
			    echo '</tr>';
			    
			    $number++;
			}
			?>
		</tbody>
	</table>
</div>
<?php
    echo Paging::printHTML($totalCount, $page, $ls);
} else {
    echo $OUTPUT->notification(get_string('not_found_users', 'local_ubion'));
}
?>