<?php
use \local_ubion\base\Common;
use local_ubion\base\Paging;

// 진도처리를 진행하는 주차 정보
$courseSections = $COnAttendance->getCourseSections();

$userSectionStatus = [];
foreach ($courseSections as $section) {
    if (!empty($section->allowModules)) {
        $userSectionStatus[$section->id] = \local_ubonattend\Attendance::CODE_ABSENCE;
    }
}


// userProgress는 아직 시작되지 않은 주차에 대해서는 출석 정보를 가져오지 않음
// [userid][sectionid] = 출석 상태값
// userProgress는 사용자가 뷰어를 열람한 사용자들에 대해서만 기록이 남기 때문에
// 뷰어를 열람하지 않는 사용자에 대한 예외처리는 따로 진행해야됨.
$userSectionProgress = array();
if ($userProgress = $COnAttendance->getSectionUserStatus($userInQuery)) {
    foreach ($userProgress as $up) {
        $userSectionProgress[$up->userid][$up->sectionid] = $up->status;
    }
}

// 주차별 출석 정보를 담아두는 배열
$defaultStatus = array(
    \local_ubonattend\Attendance::CODE_ATTENDANCE => 0
    ,\local_ubonattend\Attendance::CODE_LATE => 0
    ,\local_ubonattend\Attendance::CODE_ABSENCE => 0
);

// 사용자별로 반복을 돌면서 강좌내 출석 정보를 셋팅해줘야됨.
foreach ($users as $u) {
    $u->status = $defaultStatus;
    $u->sectionStatus = $userSectionStatus;
    
    // sectionStatus를 $userSectionProgress[$up->userid][$up->sectionid]값으로 통체로 바꾸면 안됩니다.
    // 이유는 $userSectionProgress에 특정 주차 정보가 존재하지 않을수 있기 때문에 (특정 주차에 존재하는 영상에 대해서 한번도 시청하지 않은 경우가 있음)
    // 별도의 분기처리를 진행해야됨
    foreach ($u->sectionStatus as $sectionid => $status) {
        // local_ubonattend_section에 데이터가 존재하는 경우 local_ubonattend_section에서 조회된 값을 그대로 사용하고, 
        // 존재하지 않는다면 기본값(결석)을 그대로 사용
        $status = $userSectionProgress[$u->id][$sectionid] ?? $status;
        $u->sectionStatus[$sectionid] = $status;
        $u->status[$status]++;
    }
}



if ($totalCount > 0) {
?>

<div class="table-responsive">
	<table class="table table-bordered table-coursemos table-online-attendance mb-0">
		<thead>
			<tr>
				<th class="fixedcolumn min-wp-50"><?= $i8n->number; ?></th>
				<th class="fixedcolumn min-wp-60"><?= $i8n->userpic; ?></th>
				<th class="fixedcolumn min-wp-100"><?= Common::getSortLink($i8n->idnumber, 'idnumber'); ?></th>
				<th class="fixedcolumn min-wp-120 leftlastfixedcolumn"><?= Common::getSortLink($i8n->fullnameuser, 'fullname'); ?></th>
				<th class="min-wp-120"><?= $i8n->department; ?></th>
				<?php
				if ($courseGroup->is) {
					echo '<th class="min-wp-80">'.$i8n->group.'</th>';
				}
				
				// 강좌에 존재하는 주차 출력
				foreach ($courseSections as $cs) {
			        $sectionName = $CCourse->getSectionName($courseInfo, $cs->section);
			        
				    echo '<th class="text-truncate max-wp-50" title="'.$sectionName.'" data-toggle="tooltip" data-container="body">';
				    echo    $sectionName;
				    echo '</th>';
				}
				
				
				echo '<th class="borderline min-wp-50">'.$i8n->attendance.'</th>';
				
				// 지각 기능을 사용하는 경우 표시
				if ($isLate) {
				    echo '<th class="borderline min-wp-50">'.$i8n->late.'</th>';
				}
				echo '<th class="borderline min-wp-50">'.$i8n->absence.'</th>';
				?>
			</tr>
		</thead>
		<tbody>
			<?php 
			$page = empty($page) ? 1 : $page;
			$number = (($page-1) * $ls) + 1;
			
			$myStatusURL = $CFG->wwwroot.'/local/ubonattend/my_status.php?id='.$courseInfo->id.'&userid=';
            foreach ($users as $u) {
                $idnumber = (!empty($u->idnumber)) ? $u->idnumber : '&nbsp;';
                
			    echo '<tr>';
			    echo     '<td class="text-center">'.$number.'</td>';
			    echo     '<td class="text-center">';
			    echo         '<img src="'.$CUser->getPicture($u).'" alt="" />';
			    echo     '</td>';
			    
			    echo     '<td class="text-center">';
			    // 교수자만 볼수 있기 때문에 학번을 그대로 노출
			    echo         $idnumber;
			    echo     '</td>';
			    echo     '<td class="text-center">';
			    echo         '<a href="'.$myStatusURL.$u->id.'">'.fullname($u).'</a>';
			    echo     '</td>';
			    echo     '<td class="text-center">';
			    echo         $CUser->getDepartment($u, true);
			    echo     '</td>';
			    
			    if ($courseGroup->is) {
			        $groupName = $CGroup->getUserGroupBadge($courseInfo->id, $u->id, null, true);
			        echo '<td class="text-center">'.$groupName.'</td>';
			    }
			    
			    
			    foreach ($courseSections as $cs) {
			        $status = $u->sectionStatus[$cs->id] ?? null;
			        $sectionName = $CCourse->getSectionName($courseInfo, $cs->section);
			        
			        echo '<td class="text-center">';
			        echo     $COnAttendance->getStatusSymbol($status, true);
			        echo '</td>';
			    }
			    
			    echo '<td class="text-center borderline">';
			    echo     $u->status[$COnAttendance::CODE_ATTENDANCE] ?? 0;
			    echo '</td>';
			    
			    if ($isLate) {
			        echo '<td class="text-center borderline">';
			        echo     $u->status[$COnAttendance::CODE_LATE] ?? 0;
			        echo '</td>';
			    }
			    echo '<td class="text-center borderline">';
			    echo     $u->status[$COnAttendance::CODE_ABSENCE] ?? 0;
			    echo '</td>';
			    
			    echo '</tr>';
			    
			    $number++;
			}
			?>
		</tbody>
	</table>
</div>
<?php 
    echo Paging::printHTML($totalCount, $page, $ls);
} else {
    echo $OUTPUT->notification(get_string('not_found_users', 'local_ubion'));
}
?>