<?php
namespace local_ubmenu\core;

/**
 * isMenuActive메뉴명
 * - 메뉴 active 여부를 결정할 때 사용
 *
 * isMenuView메뉴명
 * - 교수/학생 이외의 값으로 메뉴 표시 여부를 결정하는 경우 사용
 *
 * settingMenuAfter메뉴명
 * - settingMenu에서 설정된 메뉴에 대해서 변경이 필요한 경우 (특별한 경우가 아니라면 사용금지)
 */
use stdClass;
use local_ubion\course\Course;
use local_ubion\base\ {
    Parameter,
    Javascript,
    Str,
    Common
};

class Menu extends \local_ubion\controller\Controller
{

    protected $pluginname = 'local_ubmenu';

    // 아래 $CCourse, $course, $courseUbion은 $this->getUserMenus() 함수에서 사용됩니다.
    protected $CCourse = null;

    protected $course = null;

    protected $courseUbion = null;

    /**
     * 메뉴 추가
     */
    function doInsert()
    {
        $courseid = Parameter::request('courseid', 0, PARAM_INT);

        // 0보다 작으면 무조건 0으로 고정
        if ($courseid < 0) {
            $courseid = 0;
        }

        // 권한 체크
        $this->authCheck($courseid);

        // ajax로 처리하다보니 공백이 + 로 치환될수 있기 때문에 예외처리해줘야됨.
        $stringid = Parameter::request('stringid', null, PARAM_ALPHA);
        $url = Parameter::request('url', null, PARAM_NOTAGS);
        $class = Parameter::request('class', null, PARAM_ALPHANUMEXT);
        $blank = Parameter::request('blank', 0, PARAM_INT);
        $viewlevel = Parameter::request('viewlevel', 0, PARAM_INT);
        $visible = Parameter::request('visible', 1, PARAM_INT);
        $notedit = Parameter::request('notedit', 0, PARAM_INT);
        $collapse = Parameter::request('collapse', 0, PARAM_INT);
        $depth = 1;
        $parentid = Parameter::request('parentid', 0, PARAM_INT);

        // 상위 메뉴 id값이 전달이 되었다면 depth는 2단계로 변경되어야 함.
        if (! empty($parentid)) {
            $depth = 2;
        }

        // 사이트 메뉴 또는 강좌 공통 메뉴인 경우
        $isSiteCourseMenu = ($courseid <= SITEID) ? true : false;

        // 메뉴명
        if ($isSiteCourseMenu) {
            $isSiteCourseMenu = true;
            $defaultLang = Common::getRequiredLanguage();
            $defaultMenuName = Parameter::request('lang-' . $defaultLang, null, PARAM_NOTAGS);
        } else {
            $defaultMenuName = Parameter::request('name', null, PARAM_NOTAGS);
        }

        // 기본적으로 검사되어야 할 항목
        $validateList = array(
            'name' => array(
                'required',
                $this->validate()::PLACEHOLDER => get_string('name', $this->pluginname),
                $this->validate()::PARAMVALUE => $defaultMenuName
            ),
            'url' => array(
                'required',
                $this->validate()::PLACEHOLDER => get_string('url', $this->pluginname),
                $this->validate()::PARAMVALUE => $url
            )
        );

        // 사이트 전체 또는 강좌 메뉴인 경우에는 $stringid값이 필수임
        if ($isSiteCourseMenu) {
            $validateList['stringid'] = array(
                'required',
                $this->validate()::PLACEHOLDER => get_string('stringid', $this->pluginname),
                $this->validate()::PARAMVALUE => $stringid
            );
        }

        $validate = $this->validate()->make($validateList);

        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {

            // 해당 메뉴가 최상위 메뉴인지 확인이 필요함.
            // 만약 최상위 메뉴가 아닌 경우에는 메뉴 추가가 불가능해야됨.
            if (! empty($parentid)) {
                if ($menu = $this->getView($parentid)) {
                    // parentid가 0 또는 null아 아니거나 depth가 1보다 크면 최상위 메뉴가 아니기 때문에 메뉴 추가 불가
                    if (! empty($menu->parentid) || $menu->depth > 1) {
                        Javascript::printJSON(array(
                            'code' => Javascript::getFailureCode(),
                            'msg' => get_string('notsubmenu2', $this->pluginname, $menu->name)
                        ));
                    }
                } else {
                    Javascript::printJSON(array(
                        'code' => Javascript::getFailureCode(),
                        'msg' => get_string('notparent', $this->pluginname, $parentid)
                    ));
                }
            }

            // 사이트 및 강좌 기본 메뉴인 경우에는
            // 메뉴 stringid값이 중복된게 있는지 확인해봐야됨.
            if ($isSiteCourseMenu) {
                if ($this->isStringIDOverlap($stringid)) {
                    Javascript::printAlert(get_string('registed_menu', $this->pluginname, $stringid));
                }
            }

            // 사이트 및 강좌 공통 메뉴이면 다국어 지원 때문에 serialize 해서 저장해야됨.
            if ($isSiteCourseMenu) {

                // 언어 명칭 serialize 시켜서 저장시켜야됨.
                $name = array();
                if ($langs = Common::getLangs()) {
                    foreach ($langs as $l) {
                        $name[$l] = Parameter::request('lang-' . $l, null, PARAM_NOTAGS);
                    }
                }

                $name = serialize($name);
            } else {
                $name = $defaultMenuName;
            }

            $menu = $this->setInsert($courseid, $stringid, $name, $url, $class, $blank, $viewlevel, $visible, $collapse, $depth, $parentid, $notedit);

            Javascript::printJSON(array(
                'code' => Javascript::getSuccessCode(),
                'msg' => get_string('save_complete', 'local_ubion')
            ));
        }
    }

    /**
     * 메뉴 추가
     *
     * @param number $courseid
     * @param string $stringid
     * @param string $name
     * @param string $url
     * @param string $class
     * @param number $blank
     * @param number $viewlevel
     * @param number $visible
     * @param number $collapse
     * @param number $depth
     * @param number $parentid
     * @param number $notedit
     * @return \stdClass
     */
    function setInsert($courseid, $stringid, $name, $url, $class = null, $blank = 0, $viewlevel = 0, $visible = 1, $collapse = 0, $depth = 1, $parentid = null, $notedit = 0)
    {
        global $DB;

        $menu = new \stdClass();
        $menu->courseid = $courseid;
        $menu->stringid = $stringid;
        $menu->name = $name;
        $menu->url = $url;
        $menu->class = $class;
        $menu->blank = $blank;
        $menu->viewlevel = $viewlevel;
        $menu->visible = $visible;
        $menu->collapse = $collapse;
        $menu->depth = $depth;
        $menu->parentid = $parentid;
        $menu->notedit = $notedit;
        $menu->id = $DB->insert_record('local_ubmenu', $menu);

        // 캐시 초기화 (특정 메뉴만 변경하지 말고 전체를 초기화하는게 좋을듯 싶음)
        $this->setCacheDelete($courseid);

        return $menu;
    }

    /**
     * 메뉴 stringid값이 중복되었는지 확인해줍니다.
     *
     * @param string $stringid
     * @return mixed|boolean
     */
    function isStringIDOverlap($stringid)
    {
        global $DB;

        return $DB->get_field_sql("SELECT COUNT(1) FROM {local_ubmenu} WHERE courseid IN (:courseid1, :courseid2) AND stringid = :stringid", array(
            'courseid1' => SITEID,
            'courseid2' => 0,
            'stringid' => $stringid
        ));
    }

    /**
     * MENU 수정 (post)
     */
    function doUpdate()
    {
        global $DB;

        $id = Parameter::request('id', null, PARAM_INT);
        $courseid = Parameter::request('courseid', - 1, PARAM_INT);

        $stringid = Parameter::request('stringid', null, PARAM_ALPHA);
        $url = Parameter::request('url', null, PARAM_NOTAGS);
        $class = Parameter::request('class', null, PARAM_ALPHANUMEXT);
        $blank = Parameter::request('blank', 0, PARAM_INT);
        $viewlevel = Parameter::request('viewlevel', 0, PARAM_INT);
        $visible = Parameter::request('visible', 1, PARAM_INT);
        $notedit = Parameter::request('notedit', 0, PARAM_INT);
        $collapse = Parameter::request('collapse', 0, PARAM_INT);

        // 사이트 메뉴 또는 강좌 공통 메뉴인 경우
        $isSiteCourseMenu = ($courseid <= SITEID) ? true : false;

        // 메뉴명
        if ($isSiteCourseMenu) {
            $isSiteCourseMenu = true;
            $defaultLang = Common::getRequiredLanguage();
            $defaultMenuName = Parameter::request('lang-' . $defaultLang, null, PARAM_NOTAGS);
        } else {
            $defaultMenuName = Parameter::request('name', null, PARAM_NOTAGS);
        }

        // 기본적으로 검사되어야 할 항목
        $validateList = array(
            'id' => array(
                'required',
                'number',
                $this->validate()::PLACEHOLDER => 'menuid',
                $this->validate()::PARAMVALUE => $id
            ),
            'name' => array(
                'required',
                $this->validate()::PLACEHOLDER => get_string('name', $this->pluginname),
                $this->validate()::PARAMVALUE => $defaultMenuName
            ),
            'url' => array(
                'required',
                $this->validate()::PLACEHOLDER => get_string('url', $this->pluginname),
                $this->validate()::PARAMVALUE => $url
            )
        );

        // 사이트 전체 또는 강좌 메뉴인 경우에는 $stringid값이 필수임
        if ($isSiteCourseMenu) {
            $validateList['stringid'] = array(
                'required',
                $this->validate()::PLACEHOLDER => get_string('stringid', $this->pluginname),
                $this->validate()::PARAMVALUE => $stringid
            );
        }

        $validate = $this->validate()->make($validateList);
        if ($validate->fails()) {
            Javascript::printAlert($validate->getErrorMessages());
        } else {
            if ($menu = $this->getView($id)) {

                // 파라메터로 전달된 강좌 번호가 없다면 조회한 메뉴의 강좌 번호가 셋팅되어야 함.
                if ($courseid < 0) {
                    $courseid = $menu->courseid;
                }

                // 권한 체크 => 전달된 강좌 번호로 체크 해야됨
                $this->authCheck($courseid);

                // 사이트 및 강좌 기본 메뉴인 경우에는
                // 메뉴 stringid값이 중복된게 있는지 확인해봐야됨.
                if ($isSiteCourseMenu) {
                    if ($menu->stringid != $stringid) {
                        if ($this->isStringIDOverlap($stringid)) {
                            Javascript::printAlert(get_string('registed_menu', $this->pluginname, $stringid));
                        }
                    }
                }

                // 강좌에서 메뉴 수정하는 경우에는 조회된 메뉴가 강좌 기본 메뉴인지 확인해야됨.
                if ($courseid > SITEID && empty($menu->courseid)) {
                    // 수정 불가인 경우에는 업데이트 구문을 실행하면 안됨.
                    if (! empty($menu->notedit)) {
                        Javascript::printAlert(get_string('notedit_msg', $this->pluginname));
                    } else {
                        // 기본 메뉴인 경우에는 visible를 제외하고는 수정이 될수 없음.
                        if ($menuUserID = $DB->get_field_sql("SELECT id FROM {local_ubmenu_user} WHERE courseid = :courseid AND menuid = :menuid", array(
                            'courseid' => $courseid,
                            'menuid' => $menu->id
                        ))) {
                            $this->setUserUpdate($courseid, $menuUserID, $visible);
                        } else {
                            $this->setUserInsert($courseid, $menu->id, $menu->depth, $menu->sortorder, $visible, $menu->parentid);
                        }
                    }
                } else {
                    // 사이트 및 강좌 공통 메뉴이면 다국어 지원 때문에 serialize 해서 저장해야됨.
                    if ($isSiteCourseMenu) {

                        // 언어 명칭 serialize 시켜서 저장시켜야됨.
                        $name = array();
                        if ($langs = Common::getLangs()) {
                            foreach ($langs as $l) {
                                $name[$l] = Parameter::request('lang-' . $l, null, PARAM_NOTAGS);
                            }
                        }

                        $name = serialize($name);
                    } else {
                        $name = $defaultMenuName;
                    }

                    $menu = $this->setUpdate($id, $courseid, $stringid, $name, $url, $class, $blank, $viewlevel, $visible, $collapse, $notedit);
                }

                Javascript::printJSON(array(
                    'code' => Javascript::getSuccessCode(),
                    'msg' => get_string('update_complete', 'local_ubion')
                ));
            } else {
                Javascript::printJSON(array(
                    'code' => Javascript::getFailureCode(),
                    'msg' => get_string('nomenu', $this->pluginname, $id)
                ));
            }
        }
    }

    /**
     * MENU 수정
     *
     * @param number $id
     * @param number $courseid
     * @param string $stringid
     * @param string $name
     * @param string $url
     * @param string $class
     * @param number $blank
     * @param number $viewlevel
     * @param number $visible
     * @param number $collapse
     * @param number $notedit
     * @return \stdClass
     */
    function setUpdate($id, $courseid, $stringid, $name, $url, $class = null, $blank = 0, $viewlevel = 0, $visible = 1, $collapse = 0, $notedit = 0)
    {
        global $DB;

        $menu = new \stdClass();
        $menu->id = $id;
        $menu->stringid = $stringid;
        $menu->name = $name;
        $menu->url = $url;
        $menu->class = $class;
        $menu->blank = $blank;
        $menu->viewlevel = $viewlevel;
        $menu->visible = $visible;
        $menu->collapse = $collapse;
        $menu->notedit = $notedit;
        $DB->update_record('local_ubmenu', $menu);

        // 캐시 초기화 (특정 메뉴만 변경하지 말고 전체를 초기화하는게 좋을듯 싶음)
        $this->setCacheDelete($courseid);

        return $menu;
    }

    /**
     * MENU 삭제(post)
     *
     * @return mixed
     */
    function doDelete()
    {
        global $DB;

        $id = Parameter::request('id', null, PARAM_INT);
        $courseid = Parameter::request('courseid', 0, PARAM_INT);

        // 메뉴가 존재하는지 확인
        if ($menu = $this->getView($id)) {

            // $courseid = $menu->courseid;

            // 교수 권한 체크
            $this->authCheck($courseid);

            if ($courseid > SITEID && (empty($menu->courseid) || $menu->courseid == SITEID)) {
                Javascript::printAlert(get_string('notdelete', $this->pluginname));
            } else {
                // 메뉴 삭제
                $this->setDelete($id, $menu->courseid);

                return Javascript::printJSON(array(
                    'code' => Javascript::getSuccessCode(),
                    'msg' => get_string('delete_complete', 'local_ubion')
                ));
            }
        } else {
            Javascript::printJSON(array(
                'code' => Javascript::getFailureCode(),
                'msg' => get_string('nomenu', $this->pluginname, $id)
            ));
        }
    }

    /**
     * MENU 삭제
     *
     * @param number $id
     * @param number $courseid
     * @param string $isChildDelete
     */
    function setDelete($id, $cousreid, $isChildDelete = true)
    {
        global $DB;

        $DB->delete_records('local_ubmenu', array(
            'id' => $id
        ));

        if ($isChildDelete) {
            // 사용자가 별도로 설정한 메뉴 설정값도 삭제되어야 함.
            if ($submenus = $DB->get_records_sql("SELECT id FROM {local_ubmenu} WHERE parentid = :parentid", array(
                'parentid' => $id
            ))) {
                foreach ($submenus as $ss) {
                    $DB->delete_records('local_ubmenu_user', array(
                        'menuid' => $ss->id
                    ));
                }
            }

            // 상위, 하위 메뉴와 상관 없이 삭제하려는 id값으로 parentid 조건에 걸리는 메뉴가 있는지확인
            $DB->delete_records('local_ubmenu', array(
                'parentid' => $id
            ));
        }

        // 상위 메뉴 정보 삭제
        $DB->delete_records('local_ubmenu_user', array(
            'menuid' => $id
        ));

        // 캐시 초기화 (특정 메뉴만 변경하지 말고 전체를 초기화하는게 좋을듯 싶음)
        $this->setCacheDelete($cousreid);
    }

    function setUserInsert($courseid, $menuid, $depth, $sortorder, $visible, $parentid)
    {
        global $DB;

        $menuUser = new \stdClass();
        $menuUser->courseid = $courseid;
        $menuUser->menuid = $menuid;
        $menuUser->depth = $depth;
        $menuUser->sortorder = $sortorder;
        $menuUser->visible = $visible;
        $menuUser->parentid = $parentid;
        $menuUser->id = $DB->insert_record('local_ubmenu_user', $menuUser);

        // 캐시 초기화 (특정 메뉴만 변경하지 말고 전체를 초기화하는게 좋을듯 싶음)
        $this->setCacheDelete($courseid);

        return $menuUser;
    }

    function setUserUpdate($courseid, $id, $visible)
    {
        global $DB;

        $menuUser = new \stdClass();
        $menuUser->id = $id;
        $menuUser->visible = $visible;
        $DB->update_record('local_ubmenu_user', $menuUser);

        // 캐시 초기화 (특정 메뉴만 변경하지 말고 전체를 초기화하는게 좋을듯 싶음)
        $this->setCacheDelete($courseid);
    }

    /**
     * 메뉴 순서 변경 (post)
     */
    function doSortorder()
    {
        global $DB;

        $courseid = Parameter::request('courseid', 0, PARAM_INT);

        if ($courseid < 0) {
            $courseid = 0;
        }

        // 관리자 권한체크
        $this->authCheck($courseid);

        $json = Parameter::request('json', null, PARAM_NOTAGS);

        if (! empty($json)) {
            $json = json_decode($json);

            // 사이트 메뉴이거나, 강좌 기본 메뉴인 경우에는 곧바로 local_ubmenu에 적용하면 됨.
            if (empty($courseid) || $courseid == SITEID) {
                $topOrder = 1;
                foreach ($json[0] as $topMenu) {

                    $updateMenu = new \stdClass();
                    $updateMenu->id = $topMenu->id;
                    $updateMenu->sortorder = $topOrder;
                    $updateMenu->depth = 1;
                    $updateMenu->parentid = null;
                    $DB->update_record('local_ubmenu', $updateMenu);

                    if (isset($topMenu->children[0]) && ! empty($topMenu->children[0])) {
                        $subOrder = 1;

                        foreach ($topMenu->children[0] as $subMenu) {
                            $updateMenu = new \stdClass();
                            $updateMenu->id = $subMenu->id;
                            $updateMenu->sortorder = $subOrder;
                            $updateMenu->depth = 2;
                            $updateMenu->parentid = $topMenu->id;
                            $DB->update_record('local_ubmenu', $updateMenu);

                            $subOrder ++;
                        }
                    }

                    $topOrder ++;
                }
            } else {
                // 강좌에서 정렬을 진행하는 경우
                // 기본 메뉴에 대해서는 local_ubmenu_user에 기록이 되어야 함.
                $topOrder = 1;
                foreach ($json[0] as $topMenu) {
                    // 메뉴가 존재할 경우에만 진행
                    if ($topMenuInfo = $this->getView($topMenu->id)) {

                        // 메뉴 순서 변경
                        $this->setMenuSortorder($topMenuInfo, $courseid, $topOrder, 1, null);

                        if (isset($topMenu->children[0]) && ! empty($topMenu->children[0])) {
                            $subOrder = 1;

                            foreach ($topMenu->children[0] as $subMenu) {

                                if ($subMenuInfo = $this->getView($subMenu->id)) {

                                    // 메뉴 순서변경
                                    $this->setMenuSortorder($subMenuInfo, $courseid, $subOrder, 2, $topMenuInfo->id);
                                    $subOrder ++;
                                }
                            }
                        }

                        $topOrder ++;
                    }
                }
            }

            Javascript::printJSON(array(
                'code' => Javascript::getSuccessCode(),
                'msg' => get_string('save_complete', 'local_ubion')
            ));
        } else {
            Javascript::printJSON(array(
                'code' => Javascript::getFailureCode(),
                'msg' => get_string('notsend_sort_param', $this->pluginname)
            ));
        }
    }

    /**
     * 메뉴 순서 변경
     *
     * @param object $menuInfo
     * @param number $courseid
     * @param number $order
     * @param number $depth
     * @param number $parentid
     */
    protected function setMenuSortorder($menuInfo, $courseid, $order, $depth, $parentid)
    {
        global $DB;

        // 사이트, 강좌기본메뉴, 수정하려는 강좌와 메뉴정보의 강좌번호가 일치하는 경우에는
        // local_ubmenu에 직접 변경
        if ($courseid > SITEID && (empty($menuInfo->courseid) || $menuInfo->courseid == SITEID)) {
            if ($menuUserID = $DB->get_field_sql("SELECT id FROM {local_ubmenu_user} WHERE courseid = :courseid AND menuid = :menuid", array(
                'courseid' => $courseid,
                'menuid' => $menuInfo->id
            ))) {
                // order 값만 변경
                $updateMenuUser = new \stdClass();
                $updateMenuUser->id = $menuUserID;
                $updateMenuUser->sortorder = $order;
                $DB->update_record('local_ubmenu_user', $updateMenuUser);
            } else {
                // 최상위 메뉴이기 때문에 일부 파라메터는 강제로 고정됨
                $this->setUserInsert($courseid, $menuInfo->id, $depth, $order, $menuInfo->visible, $parentid);
            }
        } else {
            $updateMenu = new \stdClass();
            $updateMenu->id = $menuInfo->id;
            $updateMenu->sortorder = $order;
            $updateMenu->depth = $depth;
            $updateMenu->parentid = $parentid;
            $DB->update_record('local_ubmenu', $updateMenu);
        }
    }

    /**
     * 권한 체크
     *
     * @param number $courseid
     */
    protected function authCheck($courseid)
    {
        if ($courseid <= SITEID) {
            $this->authSiteAdmin();
        } else {
            $this->authProfessor($courseid);
        }
    }

    /**
     * SITE관리자 여부
     *
     * siteadmin이 아니면 alert 출력
     */
    private function authSiteAdmin()
    {
        if (! is_siteadmin()) {
            Javascript::printAlert(get_string('no_permissions', 'local_ubion'));
        }
    }

    /**
     * 교수 여부
     *
     * @param number $courseid
     * @return boolean
     */
    private function authProfessor($courseid)
    {
        if (! Course::getInstance()->isProfessor($courseid)) {
            Javascript::printAlert(get_string('no_permissions', 'local_ubion'));
        }
    }

    /**
     * 관리자페이지 - 사이트 메뉴 (숨김메뉴도 모두 가져옴)
     *
     * @return array[]
     */
    function getAdminSiteMenus()
    {
        return $this->getAdminMenus(SITEID);
    }

    /**
     *
     * 관리자페이지 - 강좌 기본 메뉴 (숨김메뉴도 모두 가져옴)
     *
     * @return array[]
     */
    function getAdminCourseMenus()
    {
        return $this->getAdminMenus(0);
    }

    /**
     * 관리자페이지용 메뉴정보를 가져옵니다.
     * (숨김메뉴도 모두 가져옴)
     *
     * @param number $courseid
     * @return array[]
     */
    private function getAdminMenus($courseid)
    {
        global $DB;

        $menuSelect = $DB->get_records_sql('SELECT * FROM {local_ubmenu} WHERE courseid = :courseid ORDER BY depth, sortorder', array(
            'courseid' => $courseid
        ));

        // 메뉴는 최대 2단계까지 구성이 가능
        // 2단계 메뉴에서 상위 메뉴가 존재하지 않는 경우를 대비해서 서로 단계별로 배열에 담아놓고서 상위 메뉴가 없는항목이 있으면
        // 1단계에 표시되도록 처리해야됨.
        $depth1Menus = array();
        $depth2Menus = array();

        $menus = array();

        // 곧바로 화면에 출력할수 있도록 depth형태로 배열 구성해주기
        if (! empty($menuSelect)) {
            foreach ($menuSelect as $ms) {
                if ($ms->depth == 1) {
                    $depth1Menus[] = $ms;
                } else {
                    $depth2Menus[] = $ms;
                }
            }

            // sortorder를 진행해놨기 때문에 별도로 순서를 재정의 할 필요는 없음.
            if (! empty($depth1Menus)) {
                foreach ($depth1Menus as $dm) {
                    $dm->submenu = array();

                    $menus[$dm->id] = $dm;
                }
            }
            unset($depth1Menus);

            if (! empty($depth2Menus)) {
                foreach ($depth2Menus as $d2) {
                    if (isset($menus[$d2->parentid])) {
                        $menus[$d2->parentid]->submenu[$d2->id] = $d2;
                    } else {
                        $menus[$d2->id] = $d2;
                    }
                }
            }
            unset($depth2Menus);
        }

        return $menus;
    }

    /**
     * 메뉴 가져오기 (교수자가 메뉴 관리 할떄 사용됨)
     *
     * @param number $courseid
     * @param boolean $reset
     * @return array[]
     */
    function getProfMenus($courseid)
    {
        // 메뉴 수정 페이지에서는 캐시 정보를 가져다가 쓰면 안되고, 디비 정보값을 그대로 활용
        return $this->getMenus($courseid, true, true);
    }

    /**
     * 메뉴 가져오기
     *
     * 캐시와 연동되기 때문에 사용자별 권한에 따른 메뉴설정값을 가져오기 원한다면 getUserMenus() 함수를 호출해주시기 바랍니다.
     * (사용자별 권한 및 다른 요인에 의해서 메뉴 표시여부가 결정되는 부분도 있기 이곳에서 처리할수가 없음)
     *
     * @param number $courseid
     * @param boolean $reset
     * @return array[]
     */
    protected function getMenus($courseid, $reset = false, $isShowVisible = false)
    {
        global $DB;

        $cache = \cache::make('local_ubmenu', 'menus');

        if ($reset) {
            $cache->delete($courseid);
        }

        $menus = $cache->get($courseid);

        if (empty($menus)) {
            // 사이트메뉴(1) 또는 강좌 기본 메뉴(0)인 경우에는 사용자별로 설정이 다를수가 없음.
            if ($courseid <= SITEID) {
                $menuSelect = $DB->get_records_sql('SELECT * FROM {local_ubmenu} WHERE courseid = :courseid ORDER BY depth, sortorder', array(
                    'courseid' => $courseid
                ));
            } else {

                // 사용자별로 메뉴 설정이 따라 붙기 때문에 예외처리가 필요함.
                $menuSelect = $DB->get_records_sql('SELECT * FROM {local_ubmenu} WHERE courseid IN (:defaultcourseid, :courseid) ORDER BY depth, sortorder', array(
                    'defaultcourseid' => 0,
                    'courseid' => $courseid
                ));

                if ($userMenus = $DB->get_records_sql("SELECT * FROM {local_ubmenu_user} WHERE courseid = :courseid", array(
                    'courseid' => $courseid
                ))) {
                    // 기본 메뉴에 사용자가 설정한 값이 존재하면 사용자가 설정한 값으로 override되도록 처리
                    // DB상에서 처리하면 쿼리문이 특정 DB에 종속되기 때문에 프로그램에서 처리함.
                    foreach ($userMenus as $um) {
                        // 최상위 메뉴에서 사용안함으로 설정하면 강좌별 개별설정한 값도 셋팅되면 안됨.
                        if (isset($menuSelect[$um->menuid]) && $menuSelect[$um->menuid]->visible) {
                            $menuSelect[$um->menuid]->depth = $um->depth;
                            $menuSelect[$um->menuid]->sortorder = $um->sortorder;
                            $menuSelect[$um->menuid]->visible = $um->visible;
                            $menuSelect[$um->menuid]->original_visible = $menuSelect[$um->menuid]->visible; // 원본 visible 저장해놔야됨.
                            $menuSelect[$um->menuid]->parentid = $um->parentid;
                        }
                    }
                }
            }

            // 사용자가 정렬을 변경했을수도 있기 때문에 usort를 진행해줘야됨.
            // sortorder 형태로 정렬
            usort($menuSelect, function ($a, $b) {
                return $a->sortorder > $b->sortorder;
            });

            // 메뉴는 최대 2단계까지 구성이 가능
            // 2단계 메뉴에서 상위 메뉴가 존재하지 않는 경우를 대비해서 서로 단계별로 배열에 담아놓고서 상위 메뉴가 없는항목이 있으면
            // 1단계에 표시되도록 처리해야됨.
            $depth1Menus = array();
            $depth2Menus = array();

            $hideMenus = array();

            $menus = array();

            // 곧바로 화면에 출력할수 있도록 depth형태로 배열 구성해주기
            if (! empty($menuSelect)) {
                foreach ($menuSelect as $ms) {
                    // 메뉴 활성화된 경우
                    // 관리자 페이지에서 추가된 메뉴인 경우
                    // 강좌내에서 추가된 메뉴 숨김여부와 상관 없이 모든 메뉴 표시
                    if ($ms->visible || isset($ms->original_visible) || ($isShowVisible && $ms->courseid > SITEID)) {
                        if ($ms->depth == 1) {
                            $depth1Menus[] = $ms;
                        } else {
                            $depth2Menus[] = $ms;
                        }
                    } else {
                        // 숨김 메뉴
                        // 상위 메뉴에서 숨길 때 하위 메뉴도 표시되면 안되기 때문에 별도의 배열에 담아둠
                        // 아래에서 상위 메뉴 숨김여부를 확인할때 사용됨.
                        $hideMenus[$ms->id] = $ms;
                    }
                }

                // sortorder를 진행해놨기 때문에 별도로 순서를 재정의 할 필요는 없음.
                if (! empty($depth1Menus)) {
                    foreach ($depth1Menus as $dm) {
                        $dm->submenu = array();

                        $menus[$dm->id] = $dm;
                    }
                }
                unset($depth1Menus);

                if (! empty($depth2Menus)) {
                    foreach ($depth2Menus as $d2) {
                        // 상위 메뉴가 숨김메뉴로 선택되지 않았을 때에만 진행되어야 함.
                        if (! isset($hideMenus[$d2->parentid])) {
                            // 상위 메뉴가 존재하면 하위메뉴로, 존재하지 않으면 상위 메뉴로 강제로 이동시켜줘야됨.
                            if (isset($menus[$d2->parentid])) {
                                $menus[$d2->parentid]->submenu[$d2->id] = $d2;
                            } else {
                                $menus[$d2->id] = $d2;
                            }
                        }
                    }
                }
                unset($depth2Menus);
                unset($hideMenus);
            }
            $cache->set($courseid, $menus);
        }

        return $menus;
    }

    /**
     * 사이트 메뉴
     *
     * @param boolean $reset
     * @return array
     */
    public function getSiteMenus($reset = false)
    {
        return $this->getUserMenus(SITEID, $reset);
    }

    /**
     * 강좌 메뉴
     *
     * @param number $courseid
     * @param boolean $reset
     * @return array
     */
    public function getCourseMenus($courseid, $reset = false)
    {
        return $this->getUserMenus($courseid, $reset);
    }

    /**
     * 사용자별 권한에 맞는 메뉴 리턴
     *
     * @param number $courseid
     * @param boolean $reset
     * @return array[]
     */
    protected function getUserMenus($courseid, $reset = false)
    {

        // !!!!!! 주의 !!!!!!
        // mustache를 사용하는 경우에는 배열에 key값을 강제로 부여하면 foreach가 되지 않기 때문에 key값은 부여하면 안됩니다!!!!
        // mustache로인해 해당 함수가 좀 복잡하게 꼬여버렸습니다.
        $this->CCourse = Course::getInstance();

        // SITEID보다 큰 경우에만 course, courseUbion을 반영해줘야됨.
        if ($courseid > SITEID) {
            $this->course = $this->CCourse->getCourse($courseid);
            $this->courseUbion = $this->CCourse->getUbion($courseid);
        }

        $finalMenus = [];
        $isProfessor = Course::getInstance()->isProfessor($courseid);

        if ($menus = $this->getMenus($courseid, $reset)) {

            foreach ($menus as $ms) {
                // mustcache로 인해 submenu key값을 제거해줘야됨.
                // 임시 변수에 submenu를 담아뒀다가 다시 재 설정해줘야됨.
                $submenu = $ms->submenu ?? null;
                unset($ms->submenu);
                $ms->submenu = array();

                $ms = $this->settingMenu($ms, $courseid);

                // courseid값이 SITEID이거나 0값이면 프로그램에 의해서 메뉴 표시여부를 결정함.
                $isView = $this->isMenuView($ms->courseid, $ms->stringid);

                // 모두, 교수/조교인 경우만 구별되기 때문에 viewlevel이 0이 아니면 무조건 교수전용 메뉴로 판단하면됨.
                // 메뉴를 볼 권한이 없으면 continue
                if (! $isView || (! empty($ms->viewlevel) && ! $isProfessor) || empty($ms->visible)) {
                    continue;
                } else {
                    if (! empty($submenu)) {
                        foreach ($submenu as $mss) {
                            $mss = $this->settingMenu($mss, $courseid);

                            // courseid값이 SITEID이거나 0값이면 프로그램에 의해서 메뉴 표시여부를 결정함.
                            $isSubView = $this->isMenuView($mss->courseid, $mss->stringid);

                            // 서브메뉴에서 active가 되었다면 상위 메뉴도 active 되어야 함.
                            if ($mss->active) {
                                $ms->active = true;
                            }

                            // 메뉴를 볼 권한이 없으면 continue
                            if (! $isSubView || (! empty($mss->viewlevel) && ! $isProfessor) || empty($mss->visible)) {
                                continue;
                            }

                            $ms->submenu[] = $mss;
                        }
                    }
                }

                $ms->isSubmenu = count($ms->submenu) >= 1 ? true : false;

                $finalMenus[] = $ms;
            }
        }

        return $finalMenus;
    }

    public function settingMenu($menu, $courseid, $isAfter = true)
    {
        $menu->realUrl = $menu->url;

        // active 여부 확인
        $menu->active = false;

        // 사이트 메뉴이거나, 강좌 기본 메뉴인 경우에는 강좌명 및 주소를 변경해줘야됨.
        if (empty($menu->courseid) || $menu->courseid == SITEID) {

            // 사이트 및 강좌 기본 메뉴이면 다국어 때문에 serilize되어 있기 때문에 unserialize 해야됨.
            $unserialize = unserialize($menu->name);
            $menu->name = $this->getMenuName($unserialize);
            $menu->nameUnSerialize = $unserialize;
            $menu->currentCourseid = $courseid;

            /*
             * // 주소가 /로 시작되면 url을 풀로 셋팅해줘야됨.
             * if (\local_ubion\base\Str::startsWith($menu->url, '/')) {
             * $menu->url = $CFG->wwwroot.$menu->url;
             * }
             */

            if (strpos($menu->url, '{courseid}') !== false) {
                $menu->url = str_replace('{courseid}', $courseid, $menu->url);
            }

            // 강좌에서 교수님이 등록한 메뉴에 대해서는 active될 필요가 없기 때문에 사이트 및 강좌 공통 메뉴에 대해서만 검사가 되어야 함.
            $menu->active = $this->isMenuActive($menu);
        }

        // 문자열 시작이 /로 시작되지 않고 http로 시작되지 않는다면 http를 붙여줘야됨.
        if (! Str::startsWith($menu->url, '/') && ! Str::startsWith($menu->url, 'http')) {
            $menu->url = 'http://' . $menu->url;
        }

        if ($isAfter) {
            // 메뉴 기본설정이 완료된 후 각 강좌마다 특정 조건에 의해서 표시되어야 하는 경우가 발생될수 있음
            $menu = $this->settingMenuAfter($menu);
        }

        return $menu;
    }

    /**
     * 강좌마다 특정 조건에 의해서 메뉴가 표시되거나 링크 주소가 변경되어야 할 필요성이 있음
     * 예 : 강의계획서
     *
     * @param stdClass $menu
     * @return stdClass
     */
    public function settingMenuAfter($menu)
    {

        // 사이트 메뉴 및 강좌 공통 메뉴인 경우에만 제공됨.
        // 강좌별로 생성된 메뉴에 대해서는 검사하지 않음.
        if (empty($menu->courseid) || $menu->courseid == SITEID) {
            $functionName = 'settingMenuAfter' . ucfirst($menu->stringid);
            if (method_exists($this, $functionName)) {
                $menu = $this->$functionName($menu);
            }
        }

        return $menu;
    }

    /**
     * 메뉴 active 효과 여부
     *
     * @param stdClass $menu
     * @return boolean
     */
    protected function isMenuActive($menu)
    {
        $isActive = false;

        // stringid값이 존재할 경우에만 검사해야됨.
        if (! empty($menu->stringid)) {
            $functionName = 'isMenuActive' . ucfirst($menu->stringid);

            if (method_exists($this, $functionName)) {
                $isActive = $this->$functionName($menu);
            } else {

                /*
                 * $scriptname = $_SERVER['SCRIPT_NAME'];
                 *
                 * // url에 파라메터가 존재하면 파라메터는 제거하고 확인해봐야됨.
                 * $parseurl = parse_url($menu->url);
                 *
                 * //error_log($scriptname);
                 * //error_log($menu->url);
                 * //error_log(strpos($scriptname, $parseurl['path']) !== false);
                 * //error_log($scriptname == $parseurl['path']);
                 * // 페이지명이 동일할 경우 active 처리
                 * if (isset($parseurl['path'])) {
                 * if (($menu->url != "/" && strpos($scriptname, $parseurl['path']) !== false) || $scriptname == $parseurl['path']) {
                 * $isActive = true;
                 * }
                 * }
                 */

                $scriptname = $_SERVER['SCRIPT_NAME'];
                $parseurl = parse_url($menu->url);

                if (isset($parseurl['path'])) {
                    $filename = basename($parseurl['path']);

                    $path = str_replace($filename, '', $parseurl['path']);

                    // 파일명까지 일치해야되는 경로
                    $fileNameEqual = array(
                        '/course/',
                        '/local/ubion/course/',
                        '/local/ubion/user/'
                    );

                    // 최상위 주소가 아닌 경우 디렉토리 하위에 대해서는 active 시켜줌.
                    if ($path != '/') {
                        // /course/ 인 경우에는 파일명까지 일치 해야됨.
                        if (in_array($path, $fileNameEqual)) {
                            if ($scriptname == $parseurl['path']) {
                                $isActive = true;
                            }
                        } else {

                            // 게시판에 대해서 링크를 건 경우 파라메터로 전달된 ID값고 메뉴로 등록된 주소의 ID값을 서로 비교해야됨.
                            if ($parseurl['path'] == '/mod/ubboard/view.php' && strpos($scriptname, '/mod/ubboard') !== false) {
                                // 파라메터가 존재하는 경우
                                if (isset($parseurl['query'])) {
                                    parse_str($parseurl['query'], $parseUrlArray);

                                    // 파라메터로 전달된 id값고, db에 저장된 id값이 일치하는지 확인
                                    $paramID = optional_param('id', null, PARAM_INT);
                                    $menuID = $parseUrlArray['id'] ?? null;

                                    if (! empty($paramID) && ! empty($menuID)) {
                                        $isActive = $paramID == $menuID;
                                    }
                                }
                            } else {
                                if (! empty($path) && strpos($scriptname, $path) === 0) {
                                    $isActive = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $isActive;
    }

    /**
     * 상황에 따라 메뉴 표시여부를 프로그램적으로 처리해야되는 경우가 있음
     * 이럴때에 isAuth메뉴명 함수를 만들어두면 해당 함수를 실행해서 메뉴 view 여부를 리턴해줌
     *
     * @param number $courseid
     * @param string $stringid
     * @return boolean
     */
    private function isMenuView($courseid, $stringid)
    {
        $isView = true;

        // courseid가 SITEID이거나, 0이면 프로그램에 의해서 메뉴 표시여부를 결정할수 있음.
        if ($courseid == SITEID || empty($courseid)) {
            $functionName = 'isMenuView' . ucfirst($stringid);
            // 함수가 존재하면 실행
            if (method_exists($this, $functionName)) {
                $isView = $this->$functionName();
            }
        }

        return $isView;
    }

    /**
     * menu 조회 (post)
     *
     * @return mixed|boolean
     */
    function doView()
    {
        global $DB;

        $id = Parameter::request('id', 0, PARAM_INT);
        $courseid = Parameter::request('courseid', 0, PARAM_INT);

        if (! empty($id)) {
            $menu = $this->getView($id);

            // 강좌에서 추가된 메뉴인 경우
            $isCourseAddMenu = ($courseid > SITEID) ? true : false;

            // 사용자가 별도로 설정한 메뉴 설정이 존재하는 경우
            // 사용자 설정값이 더 우선시 되도록 조치해야됨.
            if ($isCourseAddMenu) {
                if ($menuUser = $DB->get_record_sql("SELECT * FROM {local_ubmenu_user} WHERE courseid = :courseid AND menuid = :menuid", array(
                    'courseid' => $courseid,
                    'menuid' => $menu->id
                ))) {
                    $menu->visible = $menuUser->visible;
                }
            }

            // 다국어가 적용되어 있으므로 기본언어값이 사용자한테 보여지도록 처리
            if ($isCourseAddMenu) {
                $menu->fullname = $menu->name;
                $menu->nameUnserialize = null;
            } else {
                $unserialize = unserialize($menu->name);
                $menu->fullname = $this->getMenuName($unserialize);

                // name값에 대해서 unserialize 전달해줘야지 수정이 가능함.
                $menu->nameUnserialize = $unserialize;
            }

            if (Parameter::isAajx()) {
                Javascript::printJSON(array(
                    'code' => Javascript::getSuccessCode(),
                    'msg' => get_string('view_complete', 'local_ubion'),
                    'menu' => $menu
                ));
            } else {
                return $menu;
            }
        }
    }

    /**
     * menu 조회
     *
     * @param number $id
     * @return mixed|boolean
     */
    function getView($id)
    {
        global $DB;

        return $DB->get_record_sql("SELECT * FROM {local_ubmenu} WHERE id = :id", array(
            'id' => $id
        ));
    }

    /**
     * 특정 강좌 캐시 삭제
     *
     * @param number $courseid
     */
    function setCacheDelete($courseid)
    {
        $cache = \cache::make('local_ubmenu', 'menus');

        // 사이트 관리에서 강좌 기본 메뉴 추가시, 강좌내에서는 강좌 메뉴가 이미 캐시화 되어 있어서 최대 1일동안 신규로 추가된 메뉴가 안보일수 있음.
        // 이를 방지 하기 위해서 강좌 전체 메뉴 변경(추가,수정,삭제)가 이루어지면 전체 캐시를 삭제 해야됨.
        if (empty($courseid)) {
            $cache->purge();
        } else {
            $cache->delete($courseid);
        }
    }

    /**
     * 사용자 언어에 맞는 패밀리 사이트 명칭이 리턴됨.
     *
     * @param array $unserialize
     * @param string $currentLang
     * @return string
     */
    protected function getMenuName(array $unserialize, $currentLang = null)
    {
        // 사용자가 선택한 언어
        if (empty($currentLang)) {
            $currentLang = current_language();
        }

        // $unserialize값에 사용자가 선택한 언어가 없다면
        // 기본적으로 영어가 표시
        // 만약 영어도 빈값이라면 한글로 표시해줘야됨.
        $menuName = $unserialize[$currentLang] ?? '';

        // 사용자가 선택한 언어가 없거나, 메뉴명을 따로지정하지 않은 경우 영어로 설정
        if (empty($menuName)) {
            $menuName = $unserialize['en'] ?? '';
        }

        // 영어도 빈값이라면 getRequiredLanguage값으로 표시해줌
        if (empty($menuName)) {
            $menuName = $unserialize[Common::getRequiredLanguage()] ?? '';
        }

        // 그래도 빈값이면 빈값 그대로 출력
        return $menuName;
    }

    /*
     * 생각보다 호출이 많이 발생될거 같아서 보류함.
     * protected function isMenuActiveSiteRegular()
     * {
     * return $this->isCourseTypeCheck(Course::TYPE_REGULAR);
     * }
     *
     *
     * protected function isMenuActiveSiteIrregular()
     * {
     * return $this->isCourseTypeCheck(Course::TYPE_IRREGULAR);
     * }
     *
     *
     * protected function isMenuActiveSiteEClass()
     * {
     * return $this->isCourseTypeCheck(Course::TYPE_ECLASS);
     * }
     *
     *
     * protected function isMenuActiveSiteOnline()
     * {
     * return $this->isCourseTypeCheck(Course::TYPE_ONLINE);
     * }
     *
     *
     * protected function isCourseTypeCheck($coursetype)
     * {
     * global $PAGE;
     *
     * if ($PAGE->course->id > SITEID) {
     * $CCourse = Course::getInstance();
     * $courseUbion = $CCourse->getUbion($PAGE->course->id);
     *
     * if ($courseUbion->course_type == $coursetype) {
     * return true;
     * }
     * }
     *
     * return false;
     * }
     */
    protected function isMenuActiveSiteRegularAssistant()
    {
        $scriptname = $_SERVER['SCRIPT_NAME'];

        // 조교청강신청 자체가 정규 과정만 이루어지기 떄문에 조교청강신청 페이지 접근하면 정규 강좌로 판단시켜줌
        if (strpos($scriptname, '/local/ubassistant/') !== false) {
            return true;
        }

        return false;
    }

    protected function isMenuActiveCourseGradeGrade()
    {
        $scriptname = $_SERVER['SCRIPT_NAME'];

        if (strpos($scriptname, '/grade/') !== false) {
            return true;
        }

        return false;
    }

    /**
     * 강의 계획서 메뉴 표시 여부
     *
     * @return boolean
     */
    protected function settingMenuAfterCourseInfoSyllabus($menu)
    {
        // 강의 계획서는 합반이 된 강좌인 경우에는
        // syllabus.php를 통해서 합반된 강좌 목록이 표시되고, 사용자가 원하는 강좌의 강의계획서를 클릭할수 있어야됨.
        // 그렇기 때문에 합반된 강좌는 새창으로 열리면 안됨.
        // 단, 일반 강좌인 경우에는 새창으로 열려야됨.
        // 이를 위해서 이곳에서 예외처리를 진행해줘야됨.
        $isSyllabusView = 0;

        // courseid값이 따로 존재하지 않으면 메뉴 자체가 표시될 필요가 없음.
        if (isset($this->course) && $this->course->id > SITEID) {
            // 메뉴자체가 비활성화 된 경우에는 무시 (정규 강좌 구분을 위해 courseUbion이 없는 강좌는 검사하지 않음)
            if (! empty($this->courseUbion) && $menu->visible) {
                // 접속한 강좌가 정규강좌이고 강의계획서가 존재하는 경우에만 표시되어야 함.
                if ($this->courseUbion->course_type == $this->CCourse::TYPE_REGULAR && $this->courseUbion->is_syllabus) {
                    $isSyllabusView = 1;
                }
            }
        } else {
            $isSyllabusView = 1;
        }

        $menu->visible = $isSyllabusView;

        // 합반 강좌이면 현재창에서 열려야됨. (합반된 강좌 목록을 표시해서 원하는 강좌를 선택할수 있어야됨)
        if ($this->courseUbion->is_combined) {
            $menu->blank = 0;
        }

        return $menu;
    }

    protected function isMenuActiveSiteRegularMy()
    {
        $scriptname = $_SERVER['SCRIPT_NAME'];

        if (strpos($scriptname, '/local/ubion/user/index.php') !== false) {
            return true;
        }

        return false;
    }

    protected function isMenuActiveSiteMypageFiles()
    {
        $scriptname = $_SERVER['SCRIPT_NAME'];

        if (strpos($scriptname, '/user/files.php') !== false) {
            return true;
        }

        return false;
    }


    /**
     * 온라인 출석부 표시 여부
     *
     * @return boolean
     */
    protected function isMenuViewCourseGradeOnline()
    {
        return $this->isCourseOnlineAttend();
    }

    /**
     * 온라인 출석부 메뉴 활성화 여부
     *
     * @return boolean
     */
    protected function isMenuActiveCourseGradeOnline()
    {
        $scriptname = $_SERVER['SCRIPT_NAME'];

        if (strpos($scriptname, '/local/ubonattend/index.php') !== false || strpos($scriptname, '/local/ubonattend/report.php') !== false) {
            return true;
        }

        return false;
    }

    /**
     * 진도현황 페이지 표시 여부
     *
     * @return boolean
     */
    protected function isMenuViewCourseGradeProgress()
    {
        if ($this->isCourseProgress() && ! $this->isCourseOnlineAttend()) {
            return true;
        }

        return false;
    }

    /**
     * 온라인 출석부 셋팅 메뉴 표시 여부
     *
     * @return boolean
     */
    protected function isMenuViewCourseOtherOnlineSetting()
    {
        return $this->isCourseOnlineAttend();
    }

    /**
     * 온라인 출석부 셋팅 메뉴 활성화 여부
     *
     * @return boolean
     */
    protected function isMenuActiveCourseOtherOnlineSetting()
    {
        $scriptname = $_SERVER['SCRIPT_NAME'];

        if (strpos($scriptname, '/local/ubonattend/setting.php') !== false || strpos($scriptname, '/local/ubonattend/grade.php') !== false) {
            return true;
        }

        return false;
    }

    /**
     * 강좌 진도처리 기능 사용여부 리턴
     *
     * @return boolean
     */
    protected function isCourseProgress()
    {
        global $PAGE;

        $courseid = $PAGE->course->id ?? SITEID;

        $return = false;
        if ($courseid > SITEID) {
            $return = $this->courseUbion->progress ?? false;
        }

        return $return;
    }

    /**
     * 강좌 온라인 출석부 사용여부 리턴
     *
     * @return boolean
     */
    protected function isCourseOnlineAttend()
    {
        global $PAGE;

        $courseid = $PAGE->course->id ?? SITEID;

        if ($courseid > SITEID && $this->isCourseProgress()) {
            // return \local_ubonattend\Attendance::getInstance($courseid)->isAttendanceCourse();
            return false;
        }

        return false;
    }
}