<?php
namespace local_ubmenu;


class Menu extends \local_ubmenu\core\Menu {
    
    private static $instance;
    
    /**
     * 강좌 Class
     * @return Menu
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
    
    function isAuthCourseMenu1()
    {
        /*
        global $USER;
        if ($USER->username == 'costu4') {
            return true;
        } else {
            return false;
        }
        */
        return true;
    }
    
    public function settingMenuAfterCourseGradeCompletion($menu)
    {
        // 베트남에서만 사용됨.
        // 교수자가 아니라면 개인 페이지로 이동
        if (!\local_ubion\course\Course::getInstance()->isProfessor($menu->currentCourseid)) {
            $menu->url = str_replace('/report/ubcompletion/index.php', '/local/vn/report.php', $menu->url);
        }
        return $menu;
    }
    
    protected function isMenuActiveCourseGradeCompletion()
    {
        $scriptname = $_SERVER['SCRIPT_NAME'];
        
        if (strpos($scriptname, '/local/vn/report.php') !== false || strpos($scriptname, '/report/ubcompletion/index.php') !== false) {
            return true;
        }
        
        return false;
    }
    
}