define(['jquery', 'theme_coursemos/bootstrap', 'theme_coursemos/jquery.validate', 'theme_coursemos/jquery-sortable'], function($) {
	var menu = {};
	
	menu.pluginname = 'local_ubmenu';
	
	menu.management = function(courseid) {
		
		menus = $(".local-ubmenu-management ul.menu").sortable({
			handle : '.moveicon'
			,onDrop: function  ($item, container, _super) {
			    _super($item, container);
	
	
			    if(container.el.closest('ul').hasClass('menu')) {
					// item에 ul이 없다면 ul구문 추가
					if($item.find('ul').length == 0) {
						$item.append('<ul></ul>');
					}
			    } else {
					if($item.find('ul').length > 0) {
						$item.find('ul').remove();
					}
			    }
	
			    // 저장 버튼 표시
			    $(".local-ubmenu-management .div-save").show();
			},
	
			isValidTarget: function  ($item, container) {
				if($item.find('li').length > 0) {
					if(container.target.hasClass('menu') || container.target.hasClass('menu-container')) {
						return true;
					} else {
						return false;
					}
				} 
	
				return true;
			}
		});
	
		$(".btn-menu-add").click(function() {
	
			$("#modal-menu .modal-title").text(M.util.get_string('add', menu.pluginname));
			$("#modal-menu form").each(function() { this.reset(); })
			$("#modal-menu form input[name='parentid']").val('');
			$("#modal-menu form input[name='"+coursemostype+"']").val('insert');
			
			// disable 한 항목 모두 제거해줘야됨.
			elDisableRemove();

			elShow();
			
			// 탑메뉴에서만 허용되는 메뉴 숨기기
			topMenuElHide();
			
			$("#modal-menu .form-group-collapseyn").show();
			
			$("#modal-menu").modal('show');
		});
	
	
		$("form.form-validate").validate({
			submitHandler : function(form) {
				$(form).find(":disabled").removeAttr('disabled');
				
				$.ajax({
					 data: $(form).serialize()
					,url : M.cfg.wwwroot + '/local/ubmenu/action.php'
					,success:function(data){
						if(data.code == mesg.success) {
							location.reload();
						} else {
							alert(data.msg);
						}
					}
					,error: function(xhr, option, error){
				      alert(xhr.status + ' : ' + error);
				  	}
				});
			}
		});
	
	
		$(".local-ubmenu-management .btn-menu-insert").click(function() {
			if($(this).closest('ul').hasClass('menu')) {
	    		id = $(this).closest('li').data('id');
	    
	    		$("#modal-menu form").each(function() { this.reset(); })
	    		$("#modal-menu form input[name='"+coursemostype+"']").val('insert');
	    		$("#modal-menu form input[name='parentid']").val(id);
	    
	    		// disable 한 항목 모두 제거해줘야됨.
	    		// elDisableRemove();
	    		elShow();
	    		
	    		// 탑메뉴에서만 허용되는 메뉴 숨기기
	    		topMenuElHide();
	    		
	    		$("#modal-menu").modal('show');
			} else {
				alert(M.util.get_string('notsubmenu', menu.pluginname));
			}
		});
	
		
		$(".local-ubmenu-management .btn-menu-update").click(function() {
			id = $(this).closest('li').data('id');
			isTopMenu = $(this).closest('ul').hasClass('menu');
	
			$.ajax({
				 data: param = coursemostype+'=view&id='+id+'&courseid='+courseid
				,url : M.cfg.wwwroot + '/local/ubmenu/action.php'
				,success:function(data){
					if(data.code == mesg.success) {
						$("#modal-menu .modal-title").text(M.util.get_string('modify_title', menu.pluginname, data.menu.fullname));
	
						$("#modal-menu form input[name='"+coursemostype+"']").val('update');
				
						// disable 한 항목 모두 제거해줘야됨.
			    		// elDisableRemove();
						elShow();
						
						if (isTopMenu) {
							topMenuElShow();
						} else {
							topMenuElHide();
						}
			    		
						coursemosBindElement($("#modal-menu form"), data.menu, ['courseid']);
						

						// value값은 serialize되어 있기 때문에 unserialize해서 수동으로 바인딩 해줘야됨.
						$.each(data.menu.nameUnserialize, function(langKey, langValue) {
							$("#modal-menu form [name='lang-"+langKey+"']").val(langValue);
						});
						
						
						if (courseid > 1) {
							if (data.menu.courseid == 0) {
								
								// elDisable();
								elHide();
							}
						}
					} else {
						alert(data.msg);
					}
				}
				,error: function(xhr, option, error){
			      alert(xhr.status + ' : ' + error);
			  	}
			});
	
			$("#modal-menu").modal('show');
		});
	
	
		$(".local-ubmenu-management .btn-menu-delete").click(function() {
			li = $(this).closest('li');
	
			if(confirm(M.util.get_string('delete_confirm', menu.pluginname, li.find('> .menu-name').text()))) {
				$.ajax({
					 data: param = coursemostype+'=delete&id='+li.data('id')+'&courseid='+courseid
					,url : M.cfg.wwwroot + '/local/ubmenu/action.php'
					,success:function(data){
						if(data.code == mesg.success) {
							li.remove();
						} else {
							alert(data.msg);
						}
					}
					,error: function(xhr, option, error){
				      alert(xhr.status + ' : ' + error);
				  	}
				});
			}
		});
	
	
		$(".local-ubmenu-management .btn-menu-save").click(function() {
	
		    data = menus.sortable("serialize").get();
	
		    jsonString = JSON.stringify(data, null, ' ');
		    $.ajax({
				 data: param = coursemostype+'=sortorder&courseid='+courseid+'&json='+jsonString
				,url : M.cfg.wwwroot + '/local/ubmenu/action.php'
				,success:function(data){
					if(data.code == mesg.success) {
						location.reload();
					} else {
						alert(data.msg);
					}
				}
				,error: function(xhr, option, error){
			      alert(xhr.status + ' : ' + error);
			  	}
			});
		});
		
		function elDisable() {
			$("#modal-menu form input[name='name']").attr('disabled', 'disabled');
			$("#modal-menu form input[name='url']").attr('disabled', 'disabled');
			$("#modal-menu form input[name='blank']").attr('disabled', 'disabled');
			$("#modal-menu form input[name='viewlevel']").attr('disabled', 'disabled');
			$("#modal-menu form input[name='collapse']").attr('disabled', 'disabled');
		}
		
		function elDisableRemove() {
			$("#modal-menu form input[name='name']").removeAttr('disabled');
			$("#modal-menu form input[name='url']").removeAttr('disabled');
			$("#modal-menu form input[name='blank']").removeAttr('disabled');
			$("#modal-menu form input[name='viewlevel']").removeAttr('disabled');
			$("#modal-menu form input[name='collapse']").removeAttr('disabled');
		}
		
		
		function elHide() {
			$("#modal-menu .form-group-name").hide();
			$("#modal-menu .form-group-url").hide();
			$("#modal-menu .form-group-viewlevel").hide();
			$("#modal-menu .form-group-collapseyn").hide();
		}
		
		function elShow() {
			$("#modal-menu .form-group-name").show();
			$("#modal-menu .form-group-url").show();
			$("#modal-menu .form-group-viewlevel").show();
			$("#modal-menu .form-group-useyn").show();
			$("#modal-menu .form-group-collapseyn").show();
		}
	
		function topMenuElShow() {
			$("#modal-menu .form-group-collapseyn").show();
		}
		
		function topMenuElHide() {
			$("#modal-menu .form-group-collapseyn").hide();
		}
	};
	
	menu.syllabus = function(width, height) {
		
		$(".block-coursemos-menu a.syllabus").click(function() {
			options = 'width='+width+', height='+height+', scrollbars=yes';
			window.open($(this).attr('href'), 'syllabus', options);
			return false;
		});
	}
	
	
	return menu;
});