<?php
require_once '../../config.php';


$CMenu = \local_ubmenu\Menu::getInstance();

$courseid = required_param('id', PARAM_INT);
$pluginname = 'local_ubmenu';

$CCourse = \local_ubion\course\Course::getInstance();
if (!$CCourse->isProfessor($courseid)) {
    \local_ubion\base\Common::printError(get_string('no_permissions', 'local_ubion'));
}

$PAGE->set_url('/local/ubmenu/index.php', array('id'=>$courseid));
$context = context_course::instance($courseid);
$PAGE->set_context($context);
$PAGE->set_course($CCourse->getCourse($courseid));
$PAGE->set_title(get_string('management', $pluginname));
$PAGE->set_heading(get_string('management', $pluginname));
$PAGE->set_pagelayout('incourse');


$PAGE->requires->strings_for_js(array(
    'add'
    ,'notsubmenu'
    ,'modify_title'
    ,'delete_confirm'
), $pluginname);

$PAGE->requires->js_call_amd('local_ubmenu/menu', 'management', array(
    'courseid' => $courseid
));

$menus = $CMenu->getProfMenus($courseid);

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('management', $pluginname), 2);
?>
<div class="local-ubmenu-management">
	<?php 
	$moveIcon = $OUTPUT->image_url('i/move_2d');
	if (empty($menus)) {
	    echo '<div class="well">'.get_string('nomenus', $pluginname).'</div>';
	} else {
	    echo '<ul class="menu">';
	    foreach ($menus as $ms) {
	        // 관리페이지에서는 강좌별 환경에 따른 설정은 실행되면 안되고, 디비값 그대로 출력되어야 함.
	        $ms = $CMenu->settingMenu($ms, $courseid, false);
	        
	        $isDefaultMenu = empty($ms->courseid) ? true : false;
	        $hideClass = (empty($ms->visible)) ? 'menuHide' : '';
	        
	        echo '<li data-id="'.$ms->id.'" class="'.$hideClass.'">';
	        echo     '<img src="'.$moveIcon.'" alt="move" class="moveicon" />';
	        echo     '<span class="menu-name">'.$ms->name.'</span>';
			echo     '<div class="menu-url">['.$ms->url.']</div>';
	        echo     '<div class="menu-label">';
	        if (empty($ms->visible)) {
	            echo     '<span class="label label-danger">'.get_string('use_n', $pluginname).'</span>';
	        }
	        
	        if (!empty($ms->blank)) {
	            echo      '<span class="label label-info">'.get_string('new_window', $pluginname).'</span>';
	        }
	        echo     '</div>';

			echo     '<div class="menu-actions">';
	        echo             '<button type="button" class="btn btn-sm btn-info btn-menu-insert"> '.get_string('add').' </button>';
	        if (!$isDefaultMenu || ($isDefaultMenu && empty($ms->notedit))) {
	           echo          '<button type="button" class="btn btn-sm btn-default btn-menu-update"> '.get_string('modify', 'local_ubion').' </button>';
	        }
	        // 기본 메뉴에 대해서는 삭제가 불가능함.
	        if (!$isDefaultMenu) {
	           echo          '<button type="button" class="btn btn-sm btn-danger btn-menu-delete"> '.get_string('delete').' </button>';
	        }
	        echo     '</div>';
	        
	        
	        // 2단계 시작
	        echo     '<ul>';
	        if (!empty($ms->submenu)) {
	            foreach ($ms->submenu as $mss) {
	                // 관리페이지에서는 강좌별 환경에 따른 설정은 실행되면 안되고, 디비값 그대로 출력되어야 함.
	                $mss = $CMenu->settingMenu($mss, $courseid, false);
	                
	                $isSubDefaultMenu = empty($mss->courseid) ? true : false;
	                $subHideClass = (empty($mss->visible)) ? 'menuHide' : '';
	                
	                echo '<li data-id="'.$mss->id.'" class="'.$hideClass.' '.$subHideClass.'">';
	                echo     '<img src="'.$moveIcon.'" alt="move" class="moveicon" />';
	                echo     '<span class="menu-name">'.$mss->name.'</span>';
	                echo     '<div class="menu-url">['.$mss->url.']</div>';
					echo     '<div class="menu-label">';
	                if (empty($mss->visible)) {
	                    echo     '<span class="label label-danger">'.get_string('use_n', $pluginname).'</span>';
	                }
	                
	                if (!empty($mss->blank)) {
	                    echo         '<span class="label label-info">'.get_string('new_window', $pluginname).'</span>';
	                }
	                echo     '</div>';

					echo     '<div class="menu-actions">';
	                echo         '<button type="button" class="btn btn-sm btn-info btn-menu-insert"> '.get_string('add').' </button>';
	                if (!$isSubDefaultMenu|| ($isSubDefaultMenu&& empty($mss->notedit))) {
	                   echo      '<button type="button" class="btn btn-sm btn-default btn-menu-update"> '.get_string('modify', 'local_ubion').' </button>';
	                }
	                if (!$isSubDefaultMenu) {
	                   echo      '<button type="button" class="btn btn-sm btn-danger btn-menu-delete"> '.get_string('delete').' </button>';
	                }
	                echo     '</div>';
	                
	                echo '</li>';
	            }
	        }
	        echo     '</ul>';
	        echo '</li>';
	    }
	    echo '</ul>';
	}
	?>
	
    
    <div class="actions text-right">
    	<div class="pull-left text-left div-save">
    		<button type="button" class="btn btn-success btn-menu-save"><?php print_string('move_save', $pluginname)?></button>
    		<span class="text-danger">
    			<?php print_string('move_save_help', $pluginname)?>
    		</span>
    	</div>
    	<button type="button" class="btn btn-primary btn-menu-add"><?php print_string('add', $pluginname); ?></button>
    </div>
    
    
    <div id="modal-menu" class="modal fade" tabindex="-1" role="dialog">
    	<div class="modal-dialog modal-lg" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    				<h5 class="modal-title"><?php print_string('add', $pluginname); ?></h5>
    			</div>
    			<div class="modal-body">
    				<form method="post" action="action.php" class="form-horizontal form-validate" id="modal-form">
                    	<div class="form-group row form-group-name">
                    		<label class="col-md-3 control-label"><?php print_string('name', $pluginname); ?></label>
                    		<div class="col-md-9">
                    			<input type="hidden" name="<?php echo $CMenu::TYPENAME; ?>" value="insert" />
                    			<input type="hidden" name="id" />
                    			<input type="hidden" name="courseid" value="<?php echo $courseid; ?>" />
                    			<input type="hidden" name="parentid" />
                    			<input type="text" name="name" class="required" placeholder="<?php print_string('name', $pluginname); ?>" />
                    		</div>
                    	</div>
                    	
                    	<div class="form-group row form-group-url">
                    		<label class="col-md-3 control-label"><?php print_string('url', $pluginname); ?></label>
                    		<div class="col-md-9">
                    			<div class="form-inline">
                        			<input type="text" name="url" class="required form-control w-75" placeholder="<?php print_string('url', $pluginname); ?>" />
                        			&nbsp;
                        			<div class="checkbox">
                            			<label>
                            				<input type="checkbox" name="blank" value="1" class="form-check-input" /> <?php print_string('new_window', $pluginname); ?>
                            			</label>
                        			</div>
                    			</div>
                    		</div>
                    	</div>
                    	
                    	<div class="form-group row form-group-viewlevel">
                    		<label class="col-md-3 control-label"><?php print_string('accesslevel', $pluginname); ?></label>
                    		<div class="col-md-9 form-inline">
								<label class="radio-inline">
									<input type="radio" name="viewlevel" value="0" checked="checked" class="form-check-input" /> <?php print_string('accesslevel_all', $pluginname); ?>
								</label>
							
								<label class="radio-inline">
									<input type="radio" name="viewlevel" value="<?php echo \local_ubion\user\User::getInstance()::TYPE_PROFESSOR; ?>" class="form-check-input" /> <?php print_string('accesslevel_prof', $pluginname); ?>
								</label>
                    		</div>
                    	</div>
                    	
                    	<div class="form-group row form-group-useyn">
                    		<label class="col-md-3 control-label"><?php print_string('use_yn', $pluginname); ?></label>
                    		<div class="col-md-9 form-inline">
								<label class="radio-inline">
									<input type="radio" name="visible" value="1" checked="checked" class="form-check-input" /> <?php print_string('use_y', $pluginname); ?>
								</label>
							
								<label class="radio-inline">
									<input type="radio" name="visible" value="0" class="form-check-input" /> <?php print_string('use_n', $pluginname); ?>
								</label>
                    		</div>
                    	</div>
                    	<div class="form-group row form-group-collapseyn">
                    		<label class="col-md-3 control-label"><?php print_string('collapse_yn', $pluginname); ?></label>
                    		<div class="col-md-9 form-inline">
                    			<label class="radio-inline">
                    				<input type="radio" name="collapse" value="0" checked="checked" class="form-check-input" /> <?php print_string('collapse_n', $pluginname); ?>
                    			</label>
                    			<label class="radio-inline">
                    				<input type="radio" name="collapse" value="1" class="form-check-input" /> <?php print_string('collapse_y', $pluginname); ?>
                    			</label>
                    			<div class="help-block text-info">
                    				<?php print_string('collapse_help', $pluginname); ?>
                    			</div>
                    		</div>
                    	</div>
                    
                    	<div class="form-group">
                    		<div class="col-md-offset-3 col-md-9">
                    			<button type="submit" class="btn btn-primary"><?php print_string('save', 'local_ubion')?></button>
                    			<button type="button" class="btn btn-default" data-dismiss="modal"><?= get_string('close', 'local_ubion'); ?></button>
                    		</div>
                    	</div>
                    </form>
    			</div>
    		</div><!-- /.modal-content -->
    	</div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<?php 
echo $OUTPUT->footer();