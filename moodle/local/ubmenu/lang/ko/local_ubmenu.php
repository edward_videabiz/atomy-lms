<?php
$string['accesslevel'] = '접근권한';
$string['accesslevel_all'] = '전체 허용';
$string['accesslevel_prof'] = '교수(조교) 허용';
$string['add'] = '메뉴 추가';
$string['cache_message'] = '메뉴는 cache로 관리 되기 때문에 DB에서 직접 추가 및 수정하시면 화면상에 곧바로 반영되지 않을수 있습니다.';
$string['cachedef_menus'] = '메뉴 캐시';
$string['class'] = 'class명';
$string['class_help'] = '메뉴에 class명이 필요한 경우 입력해주시기 바랍니다.';
$string['collapse_help'] = '"펼침"으로 선택시 하위 메뉴가 존재시 상위 메뉴가 기본적으로 펼쳐져 있습니다.';
$string['collapse_n'] = '기본 (닫힘)';
$string['collapse_y'] = '펼침';
$string['collapse_yn'] = '펼침여부';
$string['delete_confirm'] = '[{$a}] 메뉴를 삭제하시겠습니까?\n하위 메뉴 존재시 하위메뉴도 모두 삭제처리됩니다.\n\n정말로 삭제하시겠습니까?';
$string['management'] = '메뉴 관리';
$string['modify_title'] = '[{$a}] 수정';
$string['move_help'] = '화살표 아이콘을 드래그 하시면 메뉴 순서를 변경하실수 있습니다.';
$string['move_save'] = '정렬 순서 저장';
$string['move_save_help'] = '정렬 순서는 반드시 저장버튼을 클릭하셔야 저장됩니다.';
$string['name'] = '메뉴명';
$string['new_window'] = '새창';
$string['nomenus'] = '등록된 메뉴가 없습니다.';
$string['notdelete'] = '삭제 불가능한 메뉴입니다. 관리자에게 문의하여주시기 바랍니다.';
$string['notedit_help'] = '수정 가능으로 선택시 교수자가 해당 메뉴에 대해 사용여부를 변경할수 있습니다. ';
$string['notedit_msg'] = '수정 불가능한 메뉴입니다.';
$string['notedit_yn'] = '수정 여부';
$string['notedit_n'] = '수정 불가';
$string['notedit_y'] = '수정 가능';
$string['notsubmenu'] = '하위 메뉴에서는 메뉴를 추가하실수 없습니다.';
$string['notsubmenu2'] = '[{$a}] 메뉴는 최상위 메뉴가 아니기 때문에 하위 메뉴 추가가 불가능합니다.';
$string['notsend_sort_param'] = '정렬관련 파라메터가 전달되지 않았습니다.';
$string['notparent'] = '상위 메뉴가 존재하지 않습니다. [{$a}]';
$string['nomenu'] = '메뉴가 존재하지 않습니다. [{$a}]';
$string['pluginname'] = '메뉴 관리';
$string['registed_menu'] = '[{$a}] 은(는) 이미 등록된 stringid 입니다.';
$string['stringid'] = 'stringid';
$string['stringid_help'] = '<p>알파벳만 입력가능</p><p class="mb-0">중복된 stringid 사용불가</p>';
$string['url'] = '주소';
$string['url_help'] = '강좌번호가 필요한 경우에는 {courseid} 를 입력해주시면 됩니다.';
$string['use_yn'] = '사용여부';
$string['use_n'] = '미사용';
$string['use_y'] = '사용';