<?php

function xmldb_local_ubmenu_upgrade($oldversion) {
    global $DB;
    
    $dbman = $DB->get_manager();
    
    if ($oldversion < 2018031201) {
        upgrade_plugin_savepoint(true, 2018031201, 'local', 'ubmenu');
    }
    
    
    if ($oldversion < 2018031202) {
        
        // Define field stringid to be added to local_ubmenu.
        $table = new xmldb_table('local_ubmenu');
        
        $field = new xmldb_field('stringid', XMLDB_TYPE_CHAR, '100', null, null, null, null, 'courseid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        
        $field = new xmldb_field('name', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null, 'stringid');
        $dbman->change_field_type($table, $field);
        
        
        // Ubmenu savepoint reached.
        upgrade_plugin_savepoint(true, 2018031202, 'local', 'ubmenu');
    }
    
    
    return true;
}