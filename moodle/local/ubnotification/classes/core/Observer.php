<?php
namespace local_ubnotification\core;

use \local_ubion\course\Course;

class Observer
{
    
    /**
     * 강좌 복구
     * @param \core\event\course_restored $event
     */
    public static function course_restored(\core\event\course_restored $event)
    {
        global $DB, $CFG;
        
        // 복구
        // error_log('course restore');
        //error_log(print_r($event, true));
        
        // 복구된 강좌 정보 알아오기
        $courseid = $event->objectid;
        
        self::setRestoreEvent($courseid);
        
    }
    
    
    /**
     * 강좌 및 학습자원/활동시 강좌에 등록/삭제된 모듈에 대해서 알림 메시지 보내기
     * 
     * @param int $courseid
     */
    private static function setRestoreEvent($courseid)
    {
        // 강좌에 존재하는 학습자원/활동 모두 가져오기
        $modinfo = get_fast_modinfo($courseid);
        
        $CNotification = \local_ubnotification\Notification::getInstance();
        
        // 현재 등록되어 있는 학습자원/활동 목록 가져오기
        $oldNotifications = $CNotification->getCourseNotifications($courseid);
        
        $cms = $modinfo->get_cms();
        
        // action값은 고정
        $action = $CNotification::ACTION_ADD;
        
        // 학습자원/활동 복구이기 때문에 etcid값은 null로 처리
        $etcid = null;
        
        // 복구된 강좌에 등록된 학습을 반복하면서 oldNotifications 배열과 비교해서 삭제된 모듈에 대해서 isdelete값을 1로 변경시켜줘야됨.
        if (!empty($cms)) {
            foreach ($cms as $cm) {
                $cmid = $cm->id;
                $pluginname = $cm->modname;
                $instanceid = $cm->instance;
                
                // label은 알림으로 전송되지 않습니다.
                if (!in_array($pluginname, self::getNoSendPlugins())) {
                    // 등록된 항목이 없으면 추가
                    if (!isset($oldNotifications[$cmid])) {
                        $CNotification->setInsert($courseid, $pluginname, $action, $cmid, $instanceid, $etcid);
                    } else {
                        // 빽업 파일에 대해 기존 강좌에 병합하는 경우
                        // 기존에 등록되어 있던 학습자원/활동에 대해서 삭제처리 되면 안되기 때문에 oldNotification 배열에서 제거해줘야됨.
                        unset($oldNotifications[$cmid]);
                    }
                }
            }
        }
        
        if (!empty($oldNotifications)) {
            foreach ($oldNotifications as $on) {
                $CNotification->setDeleteModule($courseid, $on->cmid);
            }
        }
    }
    
    private static function getNoSendPlugins()
    {
        return array('label');
    }
    
    /**
     * 학습자원/활동 복구
     * 해당 event는 무들에서 공식적으로 제공되지 않아서 /backup/util/plan/restore_plan.class.php => execute() 함수를 수정했습니다. 
     * 
     * @param \local_ubion\event\course_module_restored $event
     */
    public static function course_module_restored(\local_ubion\event\course_module_restored $event)
    {
        global $DB, $CFG;
        
        // 학습자원/활동 복구
        // error_log('activity restore');
        
        // 복구된 강좌 정보 알아오기
        $courseid = $event->objectid;
        
        
        // 등록된 cmid값을 알수가 없기 때문에 강좌 전체에 대해서 검사를 진행해봐야됨.
        self::setRestoreEvent($courseid);
    }
    
    
    /**
     * Triggered via course_module_created event.
     *
     * @param \core\event\course_deleted $event
     */
    public static function course_deleted(\core\event\course_deleted $event)
    {
        global $DB, $CFG;
        
        // 강좌 번호
        $courseid = $event->courseid;
        
        // 알림 항목 모두 삭제
        // error_log('course_delete');
        
        \local_ubnotification\Notification::getInstance()->setCourseNotificationDelete($courseid);
    }
    
    
    /**
     * Triggered via course_module_created event.
     *
     * @param \core\event\course_module_created $event
     */
    public static function course_module_created(\core\event\course_module_created $event)
    {
        
        $courseid = $event->courseid;
        $cmid = $event->objectid;
        $other = $event->other;
        
        $pluginname = $other['modulename'];
        $instanceid = $other['instanceid'];
        $etcid = null;
        
        // course_module추가(삭제)는 add로 고정
        $action = \local_ubnotification\Notification::ACTION_ADD;
        
        \local_ubnotification\Notification::getInstance()->setInsert($courseid, $pluginname, $action, $cmid, $instanceid, $etcid);
        
    }
    
    /**
     * Triggered via user_enrolment_deleted event.
     *
     * @param \core\event\course_module_deleted $event
     */
    public static function course_module_deleted(\core\event\course_module_deleted $event)
    {
        global $DB, $CFG;
        
        $CNotification = \local_ubnotification\Notification::getInstance();
     
        $courseid = $event->courseid;
        $cmid = $event->objectid;
        $other = $event->other;
        
        $pluginname = $other['modulename'];
        $instanceid = $other['instanceid'];
        $etcid = null;
        
        // course_module추가(삭제)는 add로 고정
        $action = $CNotification::ACTION_ADD;
        
        //error_log('delete');
        //error_log(print_r($event, true));
        
        $CNotification->setInsert($courseid, $pluginname, $action, $cmid, $instanceid, $etcid, 1);
        
        // 게시판이나 포럼 같은 경우에는 모듈이 삭제 되면 하위 게시글에 대해서도 모두 삭제 처리를 진행해줘야됨.
        // 게시판, 포럼을 떠나서 모듈 삭제시 course, cmid와 관련된 항목 모두 삭제 처리하는 방식으로 변경함. 
        // if ($pluginname == 'ubboard' || $pluginname == 'forum') {
        $CNotification->setDeleteModule($courseid, $cmid);
        // }
    }
    
    
    public static function discussion_created(\mod_forum\event\discussion_created $event)
    {
        global $DB;
        
        $courseid = $event->courseid;
        $pluginname = $event->component;
        $action = \local_ubnotification\Notification::ACTION_FORUM_DISCUSSION;
        $cmid = $event->contextinstanceid;
        $instanceid = $event->other['forumid'];
        $etcid = $event->objectid;
        
        // pin 항목에 대해서만 전달해야되는 경우 
        // 게시기간에 따른 처리 여부는 로직이 복잡할거 같아서 힘들지 않을까 싶습니다?
        /*
        if ($forumDiscussions = $event->get_record_snapshot('forum_discussions', $etcid)) {  
            // 고정 여부 : $forumDiscussions->pinned
            // 게시 기간을 지정한 경우
            // 시작 : $forumDiscussions->timestart;
            // 종료 : $forumDiscussions->timeend;
        }
        */
        
        \local_ubnotification\Notification::getInstance()->setInsert($courseid, $pluginname, $action, $cmid, $instanceid, $etcid);
    }
    
    
    public static function discussion_deleted(\mod_forum\event\discussion_deleted $event)
    {
        $courseid = $event->courseid;
        $pluginname = $event->component;
        $action = \local_ubnotification\Notification::ACTION_FORUM_DISCUSSION;
        $cmid = $event->contextinstanceid;
        $instanceid = $event->other['forumid'];
        $etcid = $event->objectid;
        
        // 포럼 주제글 삭제
        \local_ubnotification\Notification::getInstance()->setInsert($courseid, $pluginname, $action, $cmid, $instanceid, $etcid, 1);
    }
}