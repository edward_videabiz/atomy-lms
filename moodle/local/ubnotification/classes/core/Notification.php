<?php
namespace local_ubnotification\core;

use course_modinfo;
use local_ubion\core\traits\TraitCourseUser;
use local_ubion\core\traits\TraitPush;

class Notification
{
    use TraitCourseUser;
    use TraitPush;

    protected $pluginname = 'local_ubnotification';

    const DEFAULT_KEPT_PERIOD = 7;

    const ACTION_ADD = 'add';

    const ACTION_UBBOARD_NOTICE = 'addAnnouncement';

    const ACTION_UBBOARD_ARTICLE = 'addArticle';

    const ACTION_FORUM_DISCUSSION = 'addDiscussion';

    const ACTION_ASSISTANT_ADD = 'add';

    const ACTION_ASSISTANT_APPROVE = 'addApprove';

    /**
     * 알림 보관 기간
     *
     * @return int
     */
    public function getKeptPeriod()
    {
        // 일단위 입력임
        $period = get_config($this->pluginname, 'kept_period');

        if (empty($period)) {
            $period = DEFAULT_KEPT_PERIOD;
        }

        return $period;
    }

    /**
     * 알림 보관 기간을 텍스트 형태로 리턴
     * 예 : 1일, 2일, 7일 등
     *
     * @return string
     */
    public function getKeptPeriodText()
    {
        $period = $this->getKeptPeriod();

        // 무조건 일로 표시
        // 주단위로 끊어지면 1주, 2주처럼 보기 좋은데
        // 13일, 15일, 30일, 31일 등 구별하기가 어려운 문구가 나왔을 때 표시할 텍스트가 애매해지므로
        // 일로 통일시키는게 좋을듯 함.
        return get_string('period_day', $this->pluginname, $period);
    }

    /**
     * 사용자에게 전달된 notification 갯수
     *
     * @param int $userid
     * @return mixed
     */
    function getCount($isNoRead = true, $userid = null)
    {
        global $USER, $DB;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $query = "SELECT
                            COUNT(1)
                  FROM		{local_ubnotification} lund
    			  JOIN		{local_ubnotification_user} lunu ON lund.id = lunu.nnid
                  WHERE     userid = :userid";

        $param = array(
            'userid' => $userid
        );

        if ($isNoRead) {
            $query .= " AND timereaded IS NULL";
        }

        return $DB->get_field_sql($query, $param);
    }

    /**
     * 읽지 않은 알림을 가져옵니다.
     *
     * @param int $userid
     * @return array
     */
    function getNewNotifications($userid = null)
    {
        global $USER, $DB;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        // 알림 내역 가져오기
        $query = "SELECT
    						lund.*
    			  FROM		{local_ubnotification} lund
    			  JOIN		{local_ubnotification_user} lunu ON lund.id = lunu.nnid
    			  WHERE		userid = :userid
    			  AND		timereaded IS NULL";

        $param = array(
            'userid' => $userid
        );

        $orderby = " ORDER BY lunu.timecreated";

        return $DB->get_records_sql($query . $orderby, $param);
    }

    /**
     * /**
     * 사용자에게 전달된 알림 내역
     *
     * @param int $page
     * @param int $ls
     * @param int $userid
     * @return array
     */
    function getNotifications($page = 1, $ls = 20, $userid = null)
    {
        global $USER, $DB;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        // 알림 내역 가져오기
        $query = "SELECT
    						  lund.*
    						  ,lunu.timereaded
    						  ,cm.id AS cmid
    			  FROM		  {local_ubnotification} lund
    			  JOIN		  {local_ubnotification_user} lunu ON lund.id = lunu.nnid
    			  LEFT JOIN	  {course_modules} cm ON cm.id = lund.cmid
    			  WHERE		  userid = :userid";

        $orderby = " ORDER BY 	lunu.timecreated DESC";
        $param = array(
            'userid' => $userid
        );

        // error_log($query);
        // error_log(print_r($param, true));

        // 페이징을 위한 변수 설정
        $limitfrom = $ls * ($page - 1);

        if ($notifications = $DB->get_records_sql($query . $orderby, $param, $limitfrom, $ls)) {
            $notifications = $this->getNotificationSetting($notifications);
        }

        return $notifications;
    }

    function getNotificationSetting($notifications)
    {
        global $CFG, $OUTPUT, $DB;

        if ($notifications) {

            $article_modules = array(
                'announcement',
                'discussion'
            );


            foreach ($notifications as $ns) {

                if ($ns->pluginname == 'mod_ubboard') {

                    $url = $CFG->wwwroot . '/mod/ubboard/article.php?id=' . $ns->cmid . '&bwid=' . $ns->etcid;
                    // 공지사항만 출력되기 때문에 따로 예외처리 없이 notice로 출력합니다.
                    $icon = $OUTPUT->image_url('timeline/ubboard_notice', $this->pluginname);
                    $modulename = get_string('article_notice', $this->pluginname);
                    $message = get_string('new_article', $this->pluginname, $modulename);
                } else if ($ns->pluginname == 'mod_forum') {

                    $url = $CFG->wwwroot . '/mod/forum/discuss.php?d=' . $ns->etcid;
                    $icon = $OUTPUT->image_url('timeline/forum', $this->pluginname);
                    $modulename = get_string('article_forum', $this->pluginname);
                    $message = get_string('new_article', $this->pluginname, $modulename);
                    
                } else {

                    $url = $CFG->wwwroot . '/mod/' . $ns->pluginname . '/view.php?id=' . $ns->cmid;
                    $modulename = get_string('pluginname', $ns->pluginname);
                    $message = get_string('new_activity', $this->pluginname, $modulename);
                    $icon = $OUTPUT->image_url('timeline/' . $ns->pluginname, $this->pluginname);

                    // 게시판인 경우에는 게시판 타입에 따른 아이콘을 표시해줘야됨.
                    if ($ns->pluginname == 'ubboard') {
                        $ubboard_type = $DB->get_field_sql('SELECT type FROM {ubboard} WHERE id = :id', array(
                            'id' => $ns->instanceid
                        ));
                        if ($ubboard_type == 'notice') {
                            $icon = $OUTPUT->image_url('timeline/ubboard_notice', $this->pluginname);
                        } else if ($ubboard_type == 'qna') {
                            $icon = $OUTPUT->image_url('timeline/ubboard_qna', $this->pluginname);
                        }
                    }
                }

                $ns->url = $url;
                $ns->icon = $icon->out();
                $ns->modulename = $modulename;
                $ns->message = $message;
            }
        }

        return $notifications;
    }

    /**
     * 알림 추가
     *
     * @param int $courseid
     * @param string $pluginname
     * @param string $action
     * @param int $cmid
     * @param int $instanceid
     * @param int $etcid
     * @param number $isdelete
     * @param array $users
     */
    public function setInsert($courseid, $pluginname, $action, $cmid, $instanceid, $etcid, $isdelete = 0, array $users = null)
    {
        global $DB;

        // 강좌에서 알림 전송을 허용한 경우에만 진행

        if (\local_ubion\course\Course::getInstance()->isNotification($courseid)) {

            // 전달된 사용자가 없으면 알림 항목 자체를 등록할 필요가 없음.
            // null값이 전달되었으면 모든 사용자에게 전달해주는걸로 판단.
            if (is_null($users)) {
                $users = $this->getCourseUserAll($courseid, null, null, null, null, - 1);
            }

            // 사용자가 존재할 경우에만 추가
            // 사용자가 존재하지 않으면 알림 항목을 추가해도 볼수 있는 사용자가 없기 때문에 레코드 추가 자체를 안함.
            if (! empty($users)) {
                // 삭제 값이 전달된 경우에는 기존에 등록된 레코드가 존재하는지 확인
                // 추가 모드라면 별다른 예외처리 하지 않아도 됨.
                if ($isdelete) {

                    $this->setUpdate($courseid, $cmid, $instanceid, $etcid, $action, $isdelete);

                    // isdelete값만 변경했다면 따로 추가적인 활동은 안해도됨.
                    // 참고로 레코드를 삭제하지 않는 이유는,
                    // 교수 또는 학생이 알림항목을 받았는데 갑자기 특정 알림 항목이 삭제가되면 시스템 오류라고 판단할 가망성이 높기 때문에
                    // 알림 항목에 line-through 처리하는게 문의가 안들어옴.
                } else {
                    $time = time();

                    $notification = new \stdClass();
                    $notification->courseid = $courseid;
                    $notification->pluginname = $pluginname;
                    $notification->action = $action;
                    $notification->cmid = $cmid;
                    $notification->instanceid = $instanceid;
                    $notification->etcid = $etcid;
                    $notification->timecreated = $time;
                    $notification->isdelete = $isdelete;

                    // error_log(print_r($notification, true));
                    // 대형 강의실인 경우 사용자가 많기 때문에 transaction을 걸어둠
                    $transaction = $DB->start_delegated_transaction();
                    if ($notification->id = $DB->insert_record('local_ubnotification', $notification)) {
                        // 사용자 존재여부는 상단에서 진행했기 때문에 따로 예외처리 없음.
                        foreach ($users as $u) {
                            $this->setInsertUser($notification->id, $u->id, $courseid, $time);
                        }
                    }
                    $transaction->allow_commit();
                }
            }
        }
    }

    /**
     * 알림 isDelete값 변경
     *
     * @param int $courseid
     * @param int $cmid
     * @param int $instanceid
     * @param int $etcid
     * @param string $action
     * @param int $isdelete
     */
    public function setUpdate($courseid, $cmid, $instanceid, $etcid, $action, $isdelete = 0)
    {
        global $DB;

        if (\local_ubion\course\Course::getInstance()->isNotification($courseid)) {
            $cmidQuery = (empty($cmid)) ? ' IS NULL' : ' = :etcid';
            $etcidQuery = (empty($etcid)) ? ' IS NULL' : ' = :etcid';

            $query = "UPDATE {local_ubnotification} SET
                                  isdelete = :isdelete
                          WHERE  courseid = :courseid
                          AND    cmid " . $cmidQuery . "
                          AND    instanceid = :instanceid
                          AND    etcid " . $etcidQuery . "
                          AND    action = :action";

            $param = array(
                'courseid' => $courseid,
                'cmid' => $cmid,
                'instanceid' => $instanceid,
                'etcid' => $etcid,
                'action' => $action,
                'isdelete' => $isdelete
            );

            // error_log($query);
            // error_log(print_r($param, true));

            $DB->execute($query, $param);
        }
    }

    /**
     * 알림 항목 존재여부
     *
     * @param int $courseid
     * @param int $cmid
     * @param int $instanceid
     * @param int $etcid
     * @param string $action
     * @return mixed|boolean
     */
    public function isNotificationExist($courseid, $cmid, $instanceid, $etcid, $action)
    {
        global $DB;

        $cmidQuery = (empty($cmid)) ? ' IS NULL' : ' = :cmid';
        $etcidQuery = (empty($etcid)) ? ' IS NULL' : ' = :etcid';

        $query = "SELECT
                            COUNT(1)
                  FROM      {local_ubnotification}
                  WHERE     courseid = :courseid
                  AND       cmid " . $cmidQuery . "
                  AND       instanceid = :instanceid
                  AND       etcid " . $etcidQuery . "
                  AND       action = :action";

        $param = array(
            'courseid' => $courseid,
            'cmid' => $cmid,
            'instanceid' => $instanceid,
            'etcid' => $etcid,
            'action' => $action
        );

        // error_log($query);
        // error_log(print_r($param, true));

        return $DB->get_field_sql($query, $param);
    }

    /**
     * 알림 항목을 사용자 테이블(local_ubnotification_user)에 추가
     *
     * @param int $nnid
     * @param int $userid
     * @param int $courseid
     * @param int $timecreated
     */
    public function setInsertUser($nnid, $userid, $courseid, $timecreated = null)
    {
        global $DB;

        if (empty($timecreated)) {
            $timecreated = time();
        }

        $noti = new \stdClass();
        $noti->nnid = $nnid;
        $noti->userid = $userid;
        $noti->courseid = $courseid;
        $noti->timecreated = $timecreated;
        $noti->timereaded = null;
        $noti->timepushed = null;
        $noti->id = $DB->insert_record('local_ubnotification_user', $noti);
    }

    /**
     * 사용자에게 전달된 notification 갯수
     *
     * @param int $userid
     * @return mixed
     */
    function getCourseCount($courseid, $isNoRead = true, $userid = null)
    {
        global $USER, $DB;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        $query = "SELECT
                            COUNT(1)
                  FROM      {local_ubnotification_user}
                  WHERE     userid = :userid
                  AND       courseid = :courseid";

        $param = array(
            'userid' => $userid,
            'courseid' => $courseid
        );

        if ($isNoRead) {
            $query .= " AND timereaded IS NULL";
        }

        return $DB->get_field_sql($query, $param);
    }

    /**
     * 모든 항목 읽기
     *
     * @param int $userid
     */
    function setAllRead($userid = null)
    {
        global $USER, $DB;

        if (empty($userid)) {
            $userid = $USER->id;
        }

        // 알림 내역을 가져온 후 알림 내역 모두 읽음으로 표시
        $query = "UPDATE {local_ubnotification_user} SET
                            timereaded = :timereaded
                  WHERE     userid = :userid
                  AND       timereaded IS NULL";

        $DB->execute($query, array(
            'timereaded' => time(),
            'userid' => $userid
        ));
    }

    /**
     * 강좌내에 존재하는 모든 알림 항목 삭제
     *
     * @param int $courseid
     */
    function setCourseNotificationDelete($courseid)
    {
        global $DB;

        // JOIN 문으로 삭제 처리 할려다가 다중 DBMS 때문에 테이블 별로 따로 삭제 처리함.

        // local_ubnotification_user삭제
        $DB->delete_records('local_ubnotification_user', array(
            'courseid' => $courseid
        ));

        // local_ubnotification 삭제
        $DB->delete_records('local_ubnotification', array(
            'courseid' => $courseid
        ));
    }

    /**
     * 메인페이지(dashboard)내에 표시될 알림 블록 내용
     *
     * @param number $printNum
     * @return string
     */
    public function getBlockHTML($printNum = 5)
    {
        global $CFG, $OUTPUT;

        // 최근 알림 항목을 가져와야되므로 page는 1로 고정됨.
        $page = 1;

        $blockContent = '';

        $count = $this->getCount();

        if ($count > 0) {
            $CCourse = \local_ubion\course\Course::getInstance();

            $notifications = $this->getNotifications($page, $printNum);
            $newIconUrl = $OUTPUT->image_url('new', $this->pluginname);

            $number = count($notifications);
            foreach ($notifications as $ns) {
                // last-child가 안먹혀서 직접 class로 잡아줌
                $mediaClass = ($number == 1) ? 'last-media' : '';
                $blockContent .= '<div class="media ' . $mediaClass . '">';

                // 삭제 (또는 취소)된 경우에는 링크가 걸려있으면 안됨.
                if ($ns->isdelete) {
                    $blockContent .= '<div class="text-through media-link">';
                } else {
                    $blockContent .= '<a href="' . $ns->url . '" class="media-link">';
                }

                $blockContent .= '<div class="media-left">';
                $blockContent .= '<img class="media-object" src="' . $ns->icon . '" alt="" />';
                $blockContent .= '</div>';
                $blockContent .= '<div class="media-body">';

                // 강좌명 출력
                $newicon = empty($ns->timereaded) ? '<img src="' . $newIconUrl . '" alt="new" /> ' : '';
                $coursename = $CCourse->getName($ns->courseid);
                $blockContent .= '<h5 class="media-heading">' . $newicon . $coursename . '</h5>';
                $blockContent .= '<p class="media-desc">' . $ns->message . '</p>';
                $blockContent .= '</div>';

                if ($ns->isdelete) {
                    $blockContent .= '</div>';
                } else {
                    $blockContent .= '</a>';
                }
                $blockContent .= '</div>';
                $number --;
            }
        } else {
            // echo '<div class="nodata">'.get_string('no_new_notification', 'local_ubnotification').'</div>';
            $blockContent .= '<div class="nodata">' . get_string('no_new_notification', $this->pluginname) . '</div>';
        }

        $blockContent .= '<div class="more-button">';
        $blockContent .= '<a href="' . $CFG->wwwroot . '/local/ubnotification/" class="btn btn-xs btn-default">' . get_string('notification_more', $this->pluginname) . '</a>';
        $blockContent .= '</div>';

        return array(
            $count,
            $blockContent
        );
    }

    /**
     * 특정 학습자원/활동 알림 삭제
     *
     * @param int $courseid
     * @param int $cmid
     */
    public function setDeleteModule($courseid, $cmid)
    {
        global $DB;

        // cmid만 가지고도 처리할수 있는데 courseid, cmid를 사용한 이유는 인덱스 때문임.
        $query = "UPDATE {local_ubnotification} SET isdelete = 1 WHERE courseid = :courseid AND cmid = :cmid";
        $DB->execute($query, array(
            'courseid' => $courseid,
            'cmid' => $cmid
        ));
    }

    /**
     * 강좌내에 존재하는 알림 항목
     *
     * @param int $courseid
     * @return array
     */
    public function getCourseNotifications($courseid)
    {
        global $DB;

        return $DB->get_records_sql("SELECT cmid, courseid FROM {local_ubnotification} WHERE courseid = :courseid GROUP BY courseid, cmid", array(
            'courseid' => $courseid
        ));
    }

    public function cron()
    {
        global $DB;

        mtrace('UBNotification cron start');

        $now = time();

        // 마지막에 전송된 시간 가져오기
        $lastrun = $DB->get_field_sql("SELECT lastruntime FROM {task_scheduled} WHERE classname = :classname", array(
            'classname' => '\local_ubnotification\task\notification_task'
        ));

        // 만약 lastrun값이 빈값이면 현재 시간으로 설정
        // 과거 데이터가 전달되면 안됨.
        if (empty($lastrun)) {
            $lastrun = $now;
        }
        // $lastrun = 1525846814;

        $query = "SELECT
                				*
                  FROM			{local_ubnotification}
                  WHERE			timecreated >= :timecreated
                  AND			courseid > :courseid
                  AND			timepushed = 0
                  AND           isdelete = 0";

        $param = array(
            'timecreated' => $lastrun,
            'courseid' => SITEID
        );

        if ($notifications = $DB->get_records_sql($query, $param)) {
            $CCourse = \local_ubion\course\Course::getInstance();

            // 중복된 데이터 조회방지를 위해 강좌 정보 담아두기
            // 배열키 => courseid
            // name, nameEng, users
            $courses = array();

            $notiUserQuery = "SELECT userid FROM {local_ubnotification_user} WHERE nnid = :nnid";

            $boardQuery = "SELECT subject FROM {ubboard_write} WHERE id = :id AND isdelete = 0";
            $forumQuery = "SELECT name FROM {forum_discussions} WHERE id = :id";

            foreach ($notifications as $ns) {

                if (isset($courses[$ns->courseid])) {
                    $courseInfo = $courses[$ns->courseid];
                } else {
                    $courseInfo = new \stdClass();
                    $courseInfo->users = '';
                    $courseInfo->ubion = null;
                    $courseInfo->name = $CCourse->getName($ns->courseid);
                    $courseInfo->nameEng = null;
                    $courseInfo->cms = course_modinfo::instance($ns->courseid)->cms;

                    // courseubion이 존재하면 영문 강좌값 셋팅
                    if ($courseUbion = $CCourse->getUbion($ns->courseid)) {
                        $courseInfo->nameEng = $courseUbion->ename;
                    }

                    // 영문 강좌명이 따로 설정되어 있지 않으면 한글 강좌명으로 표시
                    if (empty($courseInfo->nameEng)) {
                        $courseInfo->nameEng = $courseInfo->name;
                    }

                    // 해당 알림에 대해서 푸시 알림을 받아야되는 사용자 목록
                    if ($users = $DB->get_records_sql($notiUserQuery, array(
                        'nnid' => $ns->id
                    ))) {
                        $comma = '';
                        foreach ($users as $u) {
                            $courseInfo->users .= $comma . $u->userid;
                            $comma = ',';
                        }
                    }

                    $courses[$ns->courseid] = $courseInfo;
                }

                $messageCode = $ns->pluginname . $ns->action;
                $mcode = get_string('pluginname', $ns->pluginname);
                $msubject = null;

                // 푸시 전송 여부
                $isPush = true;

                // cmid가 존재하는지 확인
                // 조교/청강생 신청 등의 모듈에서는 cmid값이 존재하지 않음.
                if (! empty($ns->cmid)) {

                    // 실제 모듈이 존재하지 않으면 푸시가 전달될 필요가 없음.
                    if (! isset($courseInfo->cms[$ns->cmid])) {
                        $isPush = false;
                    } else {
                        $msubject = $courseInfo->cms[$ns->cmid]->name;
                    }

                    // 게시판이나 포럼 같은 경우에는 게시글의 제목이 전달되어야 함.
                    if ($ns->pluginname == 'mod_ubboard') {
                        if ($ns->action == self::ACTION_UBBOARD_NOTICE || $ns->action == self::ACTION_UBBOARD_ARTICLE) {
                            // 게시물 제목 가져오기
                            if ($articleSubject = $DB->get_field_sql($boardQuery, array(
                                'id' => $ns->etcid
                            ))) {
                                $msubject = $articleSubject;
                            } else {
                                // 조회된 레코드가 없다면 게시물 삭제라고 봐도 무관할듯함.
                                $isPush = false;
                            }
                        }
                    } else if ($ns->pluginname == 'mod_forum') {
                        if ($ns->action == self::ACTION_FORUM_DISCUSSION) {
                            // 포럼 주제글 제목 가져오기
                            if ($articleSubject = $DB->get_field_sql($forumQuery, array(
                                'id' => $ns->etcid
                            ))) {
                                $msubject = $articleSubject;
                            } else {
                                // 조회된 레코드가 없다면 게시물 삭제라고 봐도 무관할듯함.
                                $isPush = false;
                            }
                        }
                    }
                }

                // 사용자가 실제로 존재하는 경우에만 진행
                if ($isPush && ! empty($courseInfo->users)) {
                    // 반복을 돌면서 푸시 메시지 보내줘야됨.
                    $parameter = $this->pushParameter($courseInfo->users, $courseInfo->name, $courseInfo->nameEng, $messageCode, $mcode, $msubject);

                    list ($isSuccess, $result) = $this->push($parameter);

                    if ($isSuccess) {
                        // 푸시 전송 시간 업데이트
                        $ns->timepushed = $now;
                        $DB->update_record('local_ubnotification', $ns);
                    }
                }
            }
        }

        // old 알림 삭제 처리
        $keptPeriod = get_config('local_ubnotification', 'kept_period');
        if (empty($keptPeriod)) {
            $keptPeriod = self::DEFAULT_KEPT_PERIOD;
        }

        $deleteTime = $now - ($keptPeriod * DAYSECS);

        // 삭제대상 항목 조회
        // 다중 DBMS로 때문에 DELETE JOIN은 사용하지 않습니다.
        $query = "SELECT * FROM {local_ubnotification} WHERE timecreated <= :timecreated";
        $param = array(
            'timecreated' => $deleteTime
        );

        if ($notis = $DB->get_records_sql($query, $param)) {
            foreach ($notis as $ns) {
                $DB->delete_records('local_ubnotification', array(
                    'id' => $ns->id
                ));
                $DB->delete_records('local_ubnotification_user', array(
                    'nnid' => $ns->id
                ));
            }
        }

        mtrace('UBNotification cron end');
    }

    public function pushParameter($userids, $coursename, $coursenameEng, $messagecode, $mcode, $msubject)
    {
        // 알림은 4로 고정됨.
        $parameter = "type=" . $this->getPushNotificationCode();
        $parameter .= "&userids=" . urlencode($userids);
        $parameter .= "&coursename=" . urlencode($coursename);
        $parameter .= '&ename=' . urlencode($coursenameEng);
        $parameter .= '&messagecode=' . $messagecode;
        $parameter .= '&mcode=' . urlencode($mcode);
        $parameter .= '&msubject=' . urlencode($msubject);

        return $parameter;
    }
}