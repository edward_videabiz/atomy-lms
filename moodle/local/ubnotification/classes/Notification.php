<?php
namespace local_ubnotification;

class Notification extends \local_ubnotification\core\Notification 
{
    private static $instance;
    
    /**
     * 강좌 Class
     * @return Notification
     */
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
}