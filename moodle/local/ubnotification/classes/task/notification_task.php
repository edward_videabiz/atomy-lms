<?php
namespace local_ubnotification\task;


class notification_task extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('task_notification', 'local_ubnotification');
    }

    /**
     * Do the job.
     * Throw exceptions on errors (the job will be retried).
     */
    public function execute() {
        global $DB;
        
        
        // 복구된 강좌 및 모듈에 대한 처리가 진행되어야 할듯?
        // 상황에 따라 불필요할수도 있음.
        // \local_ubnotification_notification::getInstance()->cron();
        \local_ubnotification\Notification::getInstance()->cron();
    }
}
