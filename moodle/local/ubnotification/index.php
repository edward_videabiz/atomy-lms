<?php
require_once '../../config.php';

use local_ubion\base\Common;
use local_ubion\base\Paging;
use local_ubion\base\Parameter;

$CCourse = \local_ubion\course\Course::getInstance();

$pluginname = 'local_ubnotification';

// 파라메터
$page = optional_param('page', 1, PARAM_INT);
$ls = optional_param('ls', Parameter::getDefaultListSize(), PARAM_INT);

// 반드시 로그인 되어야 함.
require_login(SITEID, false);

$i8n = new stdClass();
$i8n->notification = get_string('notification_message', $pluginname);

$url = new moodle_url('/local/ubnotification/index.php');
if (count($_GET) > 0) {
    foreach ($_GET as $getKey => $getValue) {
        if (! empty($getValue)) {
            $url->param($getKey, $getValue);
        }
    }
}
$PAGE->set_url($url);
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_pagetype('site-notification');
$PAGE->set_title($i8n->notification);

$PAGE->navbar->add($i8n->notification, $url);
// $PAGE->navigation->extend_for_user($USER);

$CNotification = \local_ubnotification\Notification::getInstance();

// 알림 전체 갯수 및 리스트 가져오기
$totalCount = $CNotification->getCount(false);
$notifications = $CNotification->getNotifications($page, $ls);

// 목록을 가져온 뒤에 모든 알림에 대한 읽음 처리를 진행해줘야됨.
// $OUTPUT->header()전에 읽음 처리해줘야 header 영역의 알림 갯수가 제대로 표시됩니다.
// (해당 페이지 접근시 알림을 모두 읽음 처리하기 때문에 알림 항목에 대해서는 count가 되면 안됨)
$CNotification->setAllRead();

if ($totalCount > 0) {
    $time = time();
    $iconNew = '<img src="' . $OUTPUT->image_url('new', $pluginname) . '" alt="new" />';

    foreach ($notifications as $ns) {
        $course = $CCourse->getCourse($ns->courseid);

        $ns->courseName = $CCourse->getName($course);
        $ns->sectionName = null; // 현재는 미사용

        $ns->twitterTime = Common::getDateDiffTwitter($time, $ns->timecreated);
        $ns->newIcon = null;
        if (empty($ns->timereaded)) {
            $ns->newIcon = $iconNew;
        }
    }

    $notifications = array_values($notifications);
}

$ctx = new stdClass();
$ctx->keptPeriodText = $CNotification->getKeptPeriodText();
$ctx->totalCount = $totalCount;
$ctx->notifications = $notifications;
$ctx->paging = Paging::getPaging($totalCount, $page, $ls);

echo $OUTPUT->header();
echo $OUTPUT->heading($i8n->notification, 2, 'pagetitle');
echo $OUTPUT->render_from_template('local_ubnotification/index', $ctx);
echo $OUTPUT->footer();