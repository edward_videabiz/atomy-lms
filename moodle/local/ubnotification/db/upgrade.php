<?php
function xmldb_local_ubnotification_upgrade($oldversion) {
	global $CFG, $DB, $OUTPUT;

	$dbman = $DB->get_manager();

	if ($oldversion < 2018050300) {
	    
	    // Define field courseid to be added to local_ubnotification_user.
	    $table = new xmldb_table('local_ubnotification_user');
	    $field = new xmldb_field('courseid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null, 'userid');
	    if (!$dbman->field_exists($table, $field)) {
	        $dbman->add_field($table, $field);
	    }
	    
	    $index = new xmldb_index('courseid', XMLDB_INDEX_NOTUNIQUE, array('courseid'));
	    if (!$dbman->index_exists($table, $index)) {
	        $dbman->add_index($table, $index);
	    }
	    
	    // Ubnotification savepoint reached.
	    upgrade_plugin_savepoint(true, 2018050300, 'local', 'ubnotification');
	}
	
	if ($oldversion < 2018050301) {
	    
	    // Define field isdelete to be added to local_ubnotification.
	    $table = new xmldb_table('local_ubnotification');
	    $field = new xmldb_field('isdelete', XMLDB_TYPE_INTEGER, '1', null, null, null, '0', 'timecreated');
	    
	    // Conditionally launch add field isdelete.
	    if (!$dbman->field_exists($table, $field)) {
	        $dbman->add_field($table, $field);
	    }
	    
	    // Ubnotification savepoint reached.
	    upgrade_plugin_savepoint(true, 2018050301, 'local', 'ubnotification');
	}

	
	if ($oldversion < 2018050302) {
	    
	    // Define field section to be dropped from local_ubnotification.
	    $table = new xmldb_table('local_ubnotification');
	    
	    $field = new xmldb_field('section');
	    if ($dbman->field_exists($table, $field)) {
	        $dbman->drop_field($table, $field);
	    }
	    
	    $field = new xmldb_field('sectionid');
	    if ($dbman->field_exists($table, $field)) {
	        $dbman->drop_field($table, $field);
	    }
	    
	    // Ubnotification savepoint reached.
	    upgrade_plugin_savepoint(true, 2018050302, 'local', 'ubnotification');
	}
	
	
	if ($oldversion < 2018050303) {
	    
	    // Define index course (not unique) to be added to local_ubnotification.
	    $table = new xmldb_table('local_ubnotification');
	    $index = new xmldb_index('course', XMLDB_INDEX_NOTUNIQUE, array('courseid', 'cmid', 'instanceid', 'etcid', 'action'));
	    
	    // Conditionally launch add index course.
	    if (!$dbman->index_exists($table, $index)) {
	        $dbman->add_index($table, $index);
	    }
	    
	    // Ubnotification savepoint reached.
	    upgrade_plugin_savepoint(true, 2018050303, 'local', 'ubnotification');
	}
	
	
	if ($oldversion < 2018050304) {
	    
	    // Define field timepushed to be added to local_ubnotification.
	    $table = new xmldb_table('local_ubnotification');
	    
	    $field = new xmldb_field('timepushed', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0', 'isdelete');
	    if (!$dbman->field_exists($table, $field)) {
	        $dbman->add_field($table, $field);
	    }
	    
	    $index = new xmldb_index('timepushed', XMLDB_INDEX_NOTUNIQUE, array('timepushed'));
	    if (!$dbman->index_exists($table, $index)) {
	        $dbman->add_index($table, $index);
	    }
	    
	    
	    $table = new xmldb_table('local_ubnotification_user');
	    $field = new xmldb_field('timepushed');
	    if ($dbman->field_exists($table, $field)) {
	        $dbman->drop_field($table, $field);
	    }
	    
	    // Ubnotification savepoint reached.
	    upgrade_plugin_savepoint(true, 2018050304, 'local', 'ubnotification');
	}
	
}