<?php
$observers = array(
        // 강좌 복구
        array(
            'eventname' => '\core\event\course_restored',
            'callback'    => '\local_ubnotification\Observer::course_restored',
        ),
    
        // 학습자원/활동 복구
        array(
            'eventname' => '\local_ubion\event\course_module_restored',
            'callback'    => '\local_ubnotification\Observer::course_module_restored',
        ),
    
		// 강좌 삭제 ubnotification 데이터 삭제용
		array(
				'eventname'   => '\core\event\course_deleted',
				'callback'    => '\local_ubnotification\Observer::course_deleted',
		),

		// 학습 자원/활동 관련(추가, 삭제 등)
		array(
				'eventname'   => '\core\event\course_module_created',
				'callback'    => '\local_ubnotification\Observer::course_module_created',
		),
		array(
				'eventname'   => '\core\event\course_module_deleted',
				'callback'    => '\local_ubnotification\Observer::course_module_deleted',
		),
    
        // 포럼
        array(
            'eventname'   => '\mod_forum\event\discussion_created',
            'callback'    => '\local_ubnotification\Observer::discussion_created',
        ),
        array(
            'eventname'   => '\mod_forum\event\discussion_deleted',
            'callback'    => '\local_ubnotification\Observer::discussion_deleted',
        ),
    
    
        // 조교 / 청강생 신청/승인
        
);
