<?php
defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $pluginname = 'local_ubnotification';

    $settings = new admin_settingpage('local_ubnotification', get_string('pluginname', $pluginname));

    // ##############
    // # 기본 설정 ##
    // ##############
    $settings->add(new admin_setting_heading('local_ubnotification_default', get_string('setting_heading_default', $pluginname), ''));

    $options = array();
    for ($i = 1; $i <= 31; $i ++) {
        $options[$i] = get_string('period_day', $pluginname, $i);
    }

    $name = $pluginname . '/kept_period';
    $title = get_string('setting_kept_period', $pluginname);
    $description = get_string('setting_kept_period_help', $pluginname);
    $default = 7;
    $setting = new admin_setting_configselect($name, $title, $description, $default, $options);
    $settings->add($setting);

    $ADMIN->add('localplugins', $settings);
}