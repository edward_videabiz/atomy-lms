<?php
if (isloggedin()) {

    $CHtml = \theme_coursemos\Html::getInstance();

    $mContext = new stdClass();
    // 화면에 표시될 html
    $mContext->html = $CHtml->getMyCourses();

    echo $OUTPUT->render_from_template('theme_coursemos/coursemos-index', $mContext);
}